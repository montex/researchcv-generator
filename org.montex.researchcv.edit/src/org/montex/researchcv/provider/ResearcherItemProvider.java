/**
 */
package org.montex.researchcv.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.montex.researchcv.ResearchCVFactory;
import org.montex.researchcv.ResearchCVPackage;
import org.montex.researchcv.Researcher;

/**
 * This is the item provider adapter for a {@link org.montex.researchcv.Researcher} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ResearcherItemProvider extends PersonItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResearcherItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addCoauthorsPropertyDescriptor(object);
			addSupervisionsRegistryPropertyDescriptor(object);
			addSupervisionsPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Coauthors feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCoauthorsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Researcher_coauthors_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Researcher_coauthors_feature", "_UI_Researcher_type"),
				 ResearchCVPackage.Literals.RESEARCHER__COAUTHORS,
				 true,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Supervisions Registry feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSupervisionsRegistryPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Researcher_supervisionsRegistry_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Researcher_supervisionsRegistry_feature", "_UI_Researcher_type"),
				 ResearchCVPackage.Literals.RESEARCHER__SUPERVISIONS_REGISTRY,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Supervisions feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSupervisionsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Researcher_supervisions_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Researcher_supervisions_feature", "_UI_Researcher_type"),
				 ResearchCVPackage.Literals.RESEARCHER__SUPERVISIONS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ResearchCVPackage.Literals.RESEARCHER__CONTACTS);
			childrenFeatures.add(ResearchCVPackage.Literals.RESEARCHER__SOCIALS);
			childrenFeatures.add(ResearchCVPackage.Literals.RESEARCHER__PUBLICATIONS);
			childrenFeatures.add(ResearchCVPackage.Literals.RESEARCHER__TEACHING_MATERIAL);
			childrenFeatures.add(ResearchCVPackage.Literals.RESEARCHER__OTHER_MATERIAL);
			childrenFeatures.add(ResearchCVPackage.Literals.RESEARCHER__RESEARCH_GROUP);
			childrenFeatures.add(ResearchCVPackage.Literals.RESEARCHER__TOPICS);
			childrenFeatures.add(ResearchCVPackage.Literals.RESEARCHER__GRANTS);
			childrenFeatures.add(ResearchCVPackage.Literals.RESEARCHER__COURSES);
			childrenFeatures.add(ResearchCVPackage.Literals.RESEARCHER__TAUGHT_COURSES);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Researcher.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Researcher"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getText(Object object) {
		Researcher r = (Researcher)object;
		return getString("_UI_Researcher_type") +
				": " +
				String.join(" ", r.getFirstNames()) +
				" " +
				String.join(" ", r.getLastNames());
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Researcher.class)) {
			case ResearchCVPackage.RESEARCHER__CONTACTS:
			case ResearchCVPackage.RESEARCHER__SOCIALS:
			case ResearchCVPackage.RESEARCHER__PUBLICATIONS:
			case ResearchCVPackage.RESEARCHER__TEACHING_MATERIAL:
			case ResearchCVPackage.RESEARCHER__OTHER_MATERIAL:
			case ResearchCVPackage.RESEARCHER__RESEARCH_GROUP:
			case ResearchCVPackage.RESEARCHER__TOPICS:
			case ResearchCVPackage.RESEARCHER__GRANTS:
			case ResearchCVPackage.RESEARCHER__COURSES:
			case ResearchCVPackage.RESEARCHER__TAUGHT_COURSES:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ResearchCVPackage.Literals.RESEARCHER__CONTACTS,
				 ResearchCVFactory.eINSTANCE.createAddress()));

		newChildDescriptors.add
			(createChildParameter
				(ResearchCVPackage.Literals.RESEARCHER__CONTACTS,
				 ResearchCVFactory.eINSTANCE.createEmail()));

		newChildDescriptors.add
			(createChildParameter
				(ResearchCVPackage.Literals.RESEARCHER__CONTACTS,
				 ResearchCVFactory.eINSTANCE.createPhone()));

		newChildDescriptors.add
			(createChildParameter
				(ResearchCVPackage.Literals.RESEARCHER__CONTACTS,
				 ResearchCVFactory.eINSTANCE.createWebsite()));

		newChildDescriptors.add
			(createChildParameter
				(ResearchCVPackage.Literals.RESEARCHER__SOCIALS,
				 ResearchCVFactory.eINSTANCE.createSocialProfile()));

		newChildDescriptors.add
			(createChildParameter
				(ResearchCVPackage.Literals.RESEARCHER__PUBLICATIONS,
				 ResearchCVFactory.eINSTANCE.createJournal()));

		newChildDescriptors.add
			(createChildParameter
				(ResearchCVPackage.Literals.RESEARCHER__PUBLICATIONS,
				 ResearchCVFactory.eINSTANCE.createConference()));

		newChildDescriptors.add
			(createChildParameter
				(ResearchCVPackage.Literals.RESEARCHER__PUBLICATIONS,
				 ResearchCVFactory.eINSTANCE.createWorkshop()));

		newChildDescriptors.add
			(createChildParameter
				(ResearchCVPackage.Literals.RESEARCHER__PUBLICATIONS,
				 ResearchCVFactory.eINSTANCE.createBook()));

		newChildDescriptors.add
			(createChildParameter
				(ResearchCVPackage.Literals.RESEARCHER__PUBLICATIONS,
				 ResearchCVFactory.eINSTANCE.createBookChapter()));

		newChildDescriptors.add
			(createChildParameter
				(ResearchCVPackage.Literals.RESEARCHER__PUBLICATIONS,
				 ResearchCVFactory.eINSTANCE.createReport()));

		newChildDescriptors.add
			(createChildParameter
				(ResearchCVPackage.Literals.RESEARCHER__PUBLICATIONS,
				 ResearchCVFactory.eINSTANCE.createMiscellaneous()));

		newChildDescriptors.add
			(createChildParameter
				(ResearchCVPackage.Literals.RESEARCHER__TEACHING_MATERIAL,
				 ResearchCVFactory.eINSTANCE.createJournal()));

		newChildDescriptors.add
			(createChildParameter
				(ResearchCVPackage.Literals.RESEARCHER__TEACHING_MATERIAL,
				 ResearchCVFactory.eINSTANCE.createConference()));

		newChildDescriptors.add
			(createChildParameter
				(ResearchCVPackage.Literals.RESEARCHER__TEACHING_MATERIAL,
				 ResearchCVFactory.eINSTANCE.createWorkshop()));

		newChildDescriptors.add
			(createChildParameter
				(ResearchCVPackage.Literals.RESEARCHER__TEACHING_MATERIAL,
				 ResearchCVFactory.eINSTANCE.createBook()));

		newChildDescriptors.add
			(createChildParameter
				(ResearchCVPackage.Literals.RESEARCHER__TEACHING_MATERIAL,
				 ResearchCVFactory.eINSTANCE.createBookChapter()));

		newChildDescriptors.add
			(createChildParameter
				(ResearchCVPackage.Literals.RESEARCHER__TEACHING_MATERIAL,
				 ResearchCVFactory.eINSTANCE.createReport()));

		newChildDescriptors.add
			(createChildParameter
				(ResearchCVPackage.Literals.RESEARCHER__TEACHING_MATERIAL,
				 ResearchCVFactory.eINSTANCE.createMiscellaneous()));

		newChildDescriptors.add
			(createChildParameter
				(ResearchCVPackage.Literals.RESEARCHER__OTHER_MATERIAL,
				 ResearchCVFactory.eINSTANCE.createJournal()));

		newChildDescriptors.add
			(createChildParameter
				(ResearchCVPackage.Literals.RESEARCHER__OTHER_MATERIAL,
				 ResearchCVFactory.eINSTANCE.createConference()));

		newChildDescriptors.add
			(createChildParameter
				(ResearchCVPackage.Literals.RESEARCHER__OTHER_MATERIAL,
				 ResearchCVFactory.eINSTANCE.createWorkshop()));

		newChildDescriptors.add
			(createChildParameter
				(ResearchCVPackage.Literals.RESEARCHER__OTHER_MATERIAL,
				 ResearchCVFactory.eINSTANCE.createBook()));

		newChildDescriptors.add
			(createChildParameter
				(ResearchCVPackage.Literals.RESEARCHER__OTHER_MATERIAL,
				 ResearchCVFactory.eINSTANCE.createBookChapter()));

		newChildDescriptors.add
			(createChildParameter
				(ResearchCVPackage.Literals.RESEARCHER__OTHER_MATERIAL,
				 ResearchCVFactory.eINSTANCE.createReport()));

		newChildDescriptors.add
			(createChildParameter
				(ResearchCVPackage.Literals.RESEARCHER__OTHER_MATERIAL,
				 ResearchCVFactory.eINSTANCE.createMiscellaneous()));

		newChildDescriptors.add
			(createChildParameter
				(ResearchCVPackage.Literals.RESEARCHER__RESEARCH_GROUP,
				 ResearchCVFactory.eINSTANCE.createResearchGroup()));

		newChildDescriptors.add
			(createChildParameter
				(ResearchCVPackage.Literals.RESEARCHER__TOPICS,
				 ResearchCVFactory.eINSTANCE.createResearchTopic()));

		newChildDescriptors.add
			(createChildParameter
				(ResearchCVPackage.Literals.RESEARCHER__GRANTS,
				 ResearchCVFactory.eINSTANCE.createProject()));

		newChildDescriptors.add
			(createChildParameter
				(ResearchCVPackage.Literals.RESEARCHER__COURSES,
				 ResearchCVFactory.eINSTANCE.createCourse()));

		newChildDescriptors.add
			(createChildParameter
				(ResearchCVPackage.Literals.RESEARCHER__TAUGHT_COURSES,
				 ResearchCVFactory.eINSTANCE.createTaughtCourse()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == ResearchCVPackage.Literals.RESEARCHER__PUBLICATIONS ||
			childFeature == ResearchCVPackage.Literals.RESEARCHER__TEACHING_MATERIAL ||
			childFeature == ResearchCVPackage.Literals.RESEARCHER__OTHER_MATERIAL;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
