package org.montex.researchcv.ui.handlers;

import java.io.IOException;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.common.util.URI;
import org.montex.researchcv.ui.utils.Translator;

public class ToXtextHandler extends AbstractTransformationHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		super.execute(event);

//        URI fileXMICV = URI.createFileURI("/home/montex/Projects/montex.org/leonardo/runtime/leonardo/leonardo.rcv");
//        URI fileXMICoauthors = URI.createFileURI("/home/montex/Projects/montex.org/leonardo/runtime/leonardo/coauthors.rcv");
//        URI fileXtext = URI.createFileURI("/home/montex/Projects/montex.org/leonardo/runtime/leonardo/leonardo.cv");

        URI fileXMICV = URI.createPlatformResourceURI("/leonardo/leonardo.rcv", true);
        URI fileXMICoauthors = URI.createPlatformResourceURI("/leonardo/coauthors.rcv", true);
        URI fileXtext = URI.createPlatformResourceURI("/leonardo/leonardo.cv", true);


        try {
			Translator.toXtext(new URI[] { fileXMICV, fileXMICoauthors });
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.err.println(e.getStackTrace());
		}


		return null;
		
	}

}
