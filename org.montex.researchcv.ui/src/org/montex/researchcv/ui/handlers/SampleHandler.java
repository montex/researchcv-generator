package org.montex.researchcv.ui.handlers;

import com.google.inject.Injector;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceFactoryImpl;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.montex.researchcv.ui.utils.Translator;
import org.montex.researchcv.xtext.ui.internal.XtextActivator;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;

public class SampleHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		String message = "";
		
        ISelection selection = HandlerUtil.getActiveWorkbenchWindow(event)
                .getActivePage().getSelection();
        if (selection != null & selection instanceof IStructuredSelection) {
            IStructuredSelection strucSelection = (IStructuredSelection) selection;
            for (Iterator<Object> iterator = strucSelection.iterator(); iterator
                    .hasNext();) {
                Object element = iterator.next();
                System.out.println(element.toString());
            }
        }

//        URI fileXMICV = URI.createFileURI("/home/montex/Projects/montex.org/leonardo/runtime/leonardo/leonardo.rcv");
//        URI fileXMICoauthors = URI.createFileURI("/home/montex/Projects/montex.org/leonardo/runtime/leonardo/coauthors.rcv");
//        URI fileXtext = URI.createFileURI("/home/montex/Projects/montex.org/leonardo/runtime/leonardo/leonardo.cv");

        URI fileXMICV = URI.createPlatformResourceURI("/leonardo/leonardo.rcv", true);
        URI fileXMICoauthors = URI.createPlatformResourceURI("/leonardo/coauthors.rcv", true);
        URI fileXtext = URI.createPlatformResourceURI("/leonardo/leonardo.cv", true);

        try {
			Translator.toXtext(new URI[] { fileXMICV, fileXMICoauthors });
		} catch (IOException e) {
			// TODO Auto-generated catch block
			message += e.getStackTrace().toString();
		}

		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		MessageDialog.openInformation(
				window.getShell(),
				"ResearchCV Menu Extensions",
				message);
		
		
		return null;
		
		
	}
	

}
