package org.montex.researchcv.ui.handlers;

import java.util.LinkedList;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.ui.handlers.HandlerUtil;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;

public abstract class AbstractTransformationHandler extends AbstractHandler {
	
	protected LinkedList<IFile> inputFiles = new LinkedList<IFile>();
	protected IFile outputFile;
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		acquireSelection(event);
		return null;
	}
	
	protected void acquireSelection(ExecutionEvent event) {

	    ISelection selection = HandlerUtil.getActiveWorkbenchWindow(event)
                .getActivePage().getSelection();
        if (selection != null & selection instanceof IStructuredSelection) {
            IStructuredSelection strucSelection = (IStructuredSelection) selection;
            for(Object obj : strucSelection) {
            	IFile selFile = (IFile)obj;
            	inputFiles.add(selFile);
            }
        }    
        
        addFilesInSameDirectory(inputFiles, "rcv");
	}

	protected void addFilesInSameDirectory(LinkedList<IFile> files, String targetExtension) {
		
		IFile first = files.getFirst();
		
		IContainer folder = first.getParent();
		try {
			for(IResource r : folder.members()) {
				if(targetExtension != null && targetExtension.equalsIgnoreCase(r.getFileExtension())) {
					if(r.getType() == IResource.FILE && !files.contains(r)) {
						files.add((IFile)r);	
					}
				}
			}
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
