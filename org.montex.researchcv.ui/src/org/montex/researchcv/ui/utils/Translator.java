package org.montex.researchcv.ui.utils;

import java.io.IOException;
import java.util.LinkedList;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.montex.researchcv.xtext.ui.internal.XtextActivator;

import com.google.inject.Injector;

public class Translator {

	public static void toXtext(URI[] input) throws IOException {
		XtextActivator activator = XtextActivator.getInstance();
		Injector injector = activator.getInjector(XtextActivator.ORG_MONTEX_RESEARCHCV_XTEXT_TEXTUALRCV);
		
		XtextResourceSet rsXtext = injector.getInstance(XtextResourceSet.class);
		ResourceSet rs = new ResourceSetImpl();


		Resource[] rInResources = new Resource[input.length];
		Resource[] rOutResources = new Resource[input.length];

		EObject root;
		for(int i=0; i<rInResources.length; i++) {
			rInResources[i] = rs.getResource(input[i], true);
			rOutResources[i] = rsXtext.createResource(input[i].trimFileExtension().appendFileExtension("cv"));

			root = rInResources[i].getContents().get(0);
			rOutResources[i].getContents().add(root);
		}
		
		EcoreUtil.resolveAll(rs);
		EcoreUtil.resolveAll(rsXtext);
		
		for(Resource r : rOutResources) {
			r.save(null);
		}
	}

	public static void toXMI(URI[] input, URI output) throws IOException {
		XtextActivator activator = XtextActivator.getInstance();
		Injector injector = activator.getInjector(XtextActivator.ORG_MONTEX_RESEARCHCV_XTEXT_TEXTUALRCV);

		XtextResourceSet rsXtext = injector.getInstance(XtextResourceSet.class);
		ResourceSet rs = new ResourceSetImpl();
		
		Resource[] rInResources = new Resource[input.length];
		for(int i=0; i<rInResources.length; i++) {
			rInResources[i] = rsXtext.getResource(input[i], true);
		}
		
		Resource rOutResource = rs.createResource(output);
		
		EObject root = rInResources[0].getContents().get(0);
		EcoreUtil.resolveAll(rs);
		
		rOutResource.getContents().add(root);
		
		rOutResource.save(null);
	}

}
