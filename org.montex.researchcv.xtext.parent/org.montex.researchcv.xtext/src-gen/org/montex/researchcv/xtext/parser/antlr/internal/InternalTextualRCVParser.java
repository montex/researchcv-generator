package org.montex.researchcv.xtext.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.montex.researchcv.xtext.services.TextualRCVGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalTextualRCVParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_URL", "RULE_INT", "RULE_COUNTRY_CODE", "RULE_DIGIT", "RULE_LETTER_LOWERCASE", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'persons'", "'{'", "';'", "'}'", "'Institutions:'", "'Supervisions:'", "'.'", "'>'", "'@'", "','", "'<-'", "'+'", "'['", "']'", "'doi:'", "'/'", "'researcher'", "'contacts'", "'-'", "'socials'", "'publications'", "'teachingMaterial'", "'otherMaterial'", "'researchGroup'", "'topics'", "'grants'", "'courses'", "'taughtCourses'", "'SocialProfile'", "'url'", "'ResearchGroup'", "'acronym'", "'URL'", "'ResearchTopic'", "'title'", "'description'", "'relatedPapers'", "'('", "')'", "'Course'", "'name'", "'level'", "'hours'", "'credits'", "'canceled'", "'TaughtCourse'", "'webinfo'", "'website'", "'language'", "'responsibility'", "'offering'", "'address'", "'room'", "'email:'", "'phone:'", "'website:'", "'true'", "'false'", "'openAccess'", "'Journal'", "'published'", "'DOI'", "'abstract'", "'withAuthorVersion'", "'notes'", "'journalName'", "'publisher'", "'firstPage'", "'lastPage'", "'volume'", "'issue'", "'authors'", "'relatedProjects'", "'date'", "'ISSN'", "'shortPaper'", "'toolPaper'", "'invitedPaper'", "'Conference'", "'booktitle'", "'series'", "'eventName'", "'venue'", "'eventShortName'", "'trackName'", "'trackShortName'", "'bookeditors'", "'ISBN'", "'eventEndDate'", "'Workshop'", "'mainEventName'", "'mainEventShortName'", "'Book'", "'BookChapter'", "'chapterNumber'", "'Report'", "'reportNumber'", "'institution'", "'Miscellaneous'", "'SerialNumber'", "'value'", "'kind'", "'Budget'", "'amount'", "'currency'", "'Project'", "'agency'", "'program'", "'call'", "'number'", "'relatedPublications'", "'budget'", "'startDate'", "'endDate'", "'E'", "'e'", "'Master'", "'Bachelor'", "'PhD'", "'M.Quali'", "'private'", "'work'", "'UNSPECIFIED'", "'PRINT'", "'ELECTRONIC'", "'CDROM'", "'ONDEMAND'", "'USB'", "'UNDERGRADUATE'", "'GRADUATE'", "'CONTINUING_EDUCATION'"
    };
    public static final int T__144=144;
    public static final int T__143=143;
    public static final int T__50=50;
    public static final int T__145=145;
    public static final int T__140=140;
    public static final int T__142=142;
    public static final int T__141=141;
    public static final int T__59=59;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__137=137;
    public static final int T__52=52;
    public static final int T__136=136;
    public static final int T__53=53;
    public static final int T__139=139;
    public static final int T__54=54;
    public static final int T__138=138;
    public static final int T__133=133;
    public static final int T__132=132;
    public static final int T__60=60;
    public static final int T__135=135;
    public static final int T__61=61;
    public static final int T__134=134;
    public static final int RULE_ID=4;
    public static final int T__131=131;
    public static final int T__130=130;
    public static final int RULE_DIGIT=9;
    public static final int RULE_LETTER_LOWERCASE=10;
    public static final int RULE_INT=7;
    public static final int T__66=66;
    public static final int RULE_ML_COMMENT=11;
    public static final int T__67=67;
    public static final int T__129=129;
    public static final int T__68=68;
    public static final int T__69=69;
    public static final int T__62=62;
    public static final int T__126=126;
    public static final int T__63=63;
    public static final int T__125=125;
    public static final int T__64=64;
    public static final int T__128=128;
    public static final int T__65=65;
    public static final int T__127=127;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__91=91;
    public static final int T__100=100;
    public static final int T__92=92;
    public static final int T__93=93;
    public static final int T__102=102;
    public static final int T__94=94;
    public static final int T__101=101;
    public static final int T__90=90;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__99=99;
    public static final int T__95=95;
    public static final int T__96=96;
    public static final int T__97=97;
    public static final int T__98=98;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int RULE_URL=6;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__122=122;
    public static final int T__70=70;
    public static final int T__121=121;
    public static final int T__71=71;
    public static final int T__124=124;
    public static final int T__72=72;
    public static final int T__123=123;
    public static final int T__120=120;
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=12;
    public static final int T__77=77;
    public static final int T__119=119;
    public static final int T__78=78;
    public static final int T__118=118;
    public static final int T__79=79;
    public static final int T__73=73;
    public static final int T__115=115;
    public static final int EOF=-1;
    public static final int T__74=74;
    public static final int T__114=114;
    public static final int T__75=75;
    public static final int T__117=117;
    public static final int T__76=76;
    public static final int T__116=116;
    public static final int T__80=80;
    public static final int T__111=111;
    public static final int T__81=81;
    public static final int T__110=110;
    public static final int T__82=82;
    public static final int T__113=113;
    public static final int T__83=83;
    public static final int T__112=112;
    public static final int RULE_WS=13;
    public static final int RULE_ANY_OTHER=14;
    public static final int RULE_COUNTRY_CODE=8;
    public static final int T__88=88;
    public static final int T__108=108;
    public static final int T__89=89;
    public static final int T__107=107;
    public static final int T__109=109;
    public static final int T__84=84;
    public static final int T__104=104;
    public static final int T__85=85;
    public static final int T__103=103;
    public static final int T__86=86;
    public static final int T__106=106;
    public static final int T__87=87;
    public static final int T__105=105;

    // delegates
    // delegators


        public InternalTextualRCVParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalTextualRCVParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalTextualRCVParser.tokenNames; }
    public String getGrammarFileName() { return "InternalTextualRCV.g"; }



     	private TextualRCVGrammarAccess grammarAccess;

        public InternalTextualRCVParser(TokenStream input, TextualRCVGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "RootElement";
       	}

       	@Override
       	protected TextualRCVGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleRootElement"
    // InternalTextualRCV.g:65:1: entryRuleRootElement returns [EObject current=null] : iv_ruleRootElement= ruleRootElement EOF ;
    public final EObject entryRuleRootElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRootElement = null;


        try {
            // InternalTextualRCV.g:65:52: (iv_ruleRootElement= ruleRootElement EOF )
            // InternalTextualRCV.g:66:2: iv_ruleRootElement= ruleRootElement EOF
            {
             newCompositeNode(grammarAccess.getRootElementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRootElement=ruleRootElement();

            state._fsp--;

             current =iv_ruleRootElement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRootElement"


    // $ANTLR start "ruleRootElement"
    // InternalTextualRCV.g:72:1: ruleRootElement returns [EObject current=null] : (this_Researcher_0= ruleResearcher | this_PersonRegistry_1= rulePersonRegistry | this_InstitutionRegistry_2= ruleInstitutionRegistry | this_SupervisionsRegistry_3= ruleSupervisionsRegistry ) ;
    public final EObject ruleRootElement() throws RecognitionException {
        EObject current = null;

        EObject this_Researcher_0 = null;

        EObject this_PersonRegistry_1 = null;

        EObject this_InstitutionRegistry_2 = null;

        EObject this_SupervisionsRegistry_3 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:78:2: ( (this_Researcher_0= ruleResearcher | this_PersonRegistry_1= rulePersonRegistry | this_InstitutionRegistry_2= ruleInstitutionRegistry | this_SupervisionsRegistry_3= ruleSupervisionsRegistry ) )
            // InternalTextualRCV.g:79:2: (this_Researcher_0= ruleResearcher | this_PersonRegistry_1= rulePersonRegistry | this_InstitutionRegistry_2= ruleInstitutionRegistry | this_SupervisionsRegistry_3= ruleSupervisionsRegistry )
            {
            // InternalTextualRCV.g:79:2: (this_Researcher_0= ruleResearcher | this_PersonRegistry_1= rulePersonRegistry | this_InstitutionRegistry_2= ruleInstitutionRegistry | this_SupervisionsRegistry_3= ruleSupervisionsRegistry )
            int alt1=4;
            switch ( input.LA(1) ) {
            case 31:
                {
                alt1=1;
                }
                break;
            case 15:
                {
                alt1=2;
                }
                break;
            case 19:
                {
                alt1=3;
                }
                break;
            case 20:
                {
                alt1=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // InternalTextualRCV.g:80:3: this_Researcher_0= ruleResearcher
                    {

                    			newCompositeNode(grammarAccess.getRootElementAccess().getResearcherParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Researcher_0=ruleResearcher();

                    state._fsp--;


                    			current = this_Researcher_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalTextualRCV.g:89:3: this_PersonRegistry_1= rulePersonRegistry
                    {

                    			newCompositeNode(grammarAccess.getRootElementAccess().getPersonRegistryParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_PersonRegistry_1=rulePersonRegistry();

                    state._fsp--;


                    			current = this_PersonRegistry_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalTextualRCV.g:98:3: this_InstitutionRegistry_2= ruleInstitutionRegistry
                    {

                    			newCompositeNode(grammarAccess.getRootElementAccess().getInstitutionRegistryParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_InstitutionRegistry_2=ruleInstitutionRegistry();

                    state._fsp--;


                    			current = this_InstitutionRegistry_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalTextualRCV.g:107:3: this_SupervisionsRegistry_3= ruleSupervisionsRegistry
                    {

                    			newCompositeNode(grammarAccess.getRootElementAccess().getSupervisionsRegistryParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_SupervisionsRegistry_3=ruleSupervisionsRegistry();

                    state._fsp--;


                    			current = this_SupervisionsRegistry_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRootElement"


    // $ANTLR start "entryRulePersonRegistry"
    // InternalTextualRCV.g:119:1: entryRulePersonRegistry returns [EObject current=null] : iv_rulePersonRegistry= rulePersonRegistry EOF ;
    public final EObject entryRulePersonRegistry() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePersonRegistry = null;


        try {
            // InternalTextualRCV.g:119:55: (iv_rulePersonRegistry= rulePersonRegistry EOF )
            // InternalTextualRCV.g:120:2: iv_rulePersonRegistry= rulePersonRegistry EOF
            {
             newCompositeNode(grammarAccess.getPersonRegistryRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePersonRegistry=rulePersonRegistry();

            state._fsp--;

             current =iv_rulePersonRegistry; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePersonRegistry"


    // $ANTLR start "rulePersonRegistry"
    // InternalTextualRCV.g:126:1: rulePersonRegistry returns [EObject current=null] : (otherlv_0= 'persons' otherlv_1= '{' ( ( (lv_person_2_0= rulePerson ) ) otherlv_3= ';' )+ otherlv_4= '}' ) ;
    public final EObject rulePersonRegistry() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        EObject lv_person_2_0 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:132:2: ( (otherlv_0= 'persons' otherlv_1= '{' ( ( (lv_person_2_0= rulePerson ) ) otherlv_3= ';' )+ otherlv_4= '}' ) )
            // InternalTextualRCV.g:133:2: (otherlv_0= 'persons' otherlv_1= '{' ( ( (lv_person_2_0= rulePerson ) ) otherlv_3= ';' )+ otherlv_4= '}' )
            {
            // InternalTextualRCV.g:133:2: (otherlv_0= 'persons' otherlv_1= '{' ( ( (lv_person_2_0= rulePerson ) ) otherlv_3= ';' )+ otherlv_4= '}' )
            // InternalTextualRCV.g:134:3: otherlv_0= 'persons' otherlv_1= '{' ( ( (lv_person_2_0= rulePerson ) ) otherlv_3= ';' )+ otherlv_4= '}'
            {
            otherlv_0=(Token)match(input,15,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getPersonRegistryAccess().getPersonsKeyword_0());
            		
            otherlv_1=(Token)match(input,16,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getPersonRegistryAccess().getLeftCurlyBracketKeyword_1());
            		
            // InternalTextualRCV.g:142:3: ( ( (lv_person_2_0= rulePerson ) ) otherlv_3= ';' )+
            int cnt2=0;
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0>=RULE_ID && LA2_0<=RULE_STRING)||LA2_0==31) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalTextualRCV.g:143:4: ( (lv_person_2_0= rulePerson ) ) otherlv_3= ';'
            	    {
            	    // InternalTextualRCV.g:143:4: ( (lv_person_2_0= rulePerson ) )
            	    // InternalTextualRCV.g:144:5: (lv_person_2_0= rulePerson )
            	    {
            	    // InternalTextualRCV.g:144:5: (lv_person_2_0= rulePerson )
            	    // InternalTextualRCV.g:145:6: lv_person_2_0= rulePerson
            	    {

            	    						newCompositeNode(grammarAccess.getPersonRegistryAccess().getPersonPersonParserRuleCall_2_0_0());
            	    					
            	    pushFollow(FOLLOW_5);
            	    lv_person_2_0=rulePerson();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getPersonRegistryRule());
            	    						}
            	    						add(
            	    							current,
            	    							"person",
            	    							lv_person_2_0,
            	    							"org.montex.researchcv.xtext.TextualRCV.Person");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }

            	    otherlv_3=(Token)match(input,17,FOLLOW_6); 

            	    				newLeafNode(otherlv_3, grammarAccess.getPersonRegistryAccess().getSemicolonKeyword_2_1());
            	    			

            	    }
            	    break;

            	default :
            	    if ( cnt2 >= 1 ) break loop2;
                        EarlyExitException eee =
                            new EarlyExitException(2, input);
                        throw eee;
                }
                cnt2++;
            } while (true);

            otherlv_4=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_4, grammarAccess.getPersonRegistryAccess().getRightCurlyBracketKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePersonRegistry"


    // $ANTLR start "entryRuleInstitutionRegistry"
    // InternalTextualRCV.g:175:1: entryRuleInstitutionRegistry returns [EObject current=null] : iv_ruleInstitutionRegistry= ruleInstitutionRegistry EOF ;
    public final EObject entryRuleInstitutionRegistry() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInstitutionRegistry = null;


        try {
            // InternalTextualRCV.g:175:60: (iv_ruleInstitutionRegistry= ruleInstitutionRegistry EOF )
            // InternalTextualRCV.g:176:2: iv_ruleInstitutionRegistry= ruleInstitutionRegistry EOF
            {
             newCompositeNode(grammarAccess.getInstitutionRegistryRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInstitutionRegistry=ruleInstitutionRegistry();

            state._fsp--;

             current =iv_ruleInstitutionRegistry; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInstitutionRegistry"


    // $ANTLR start "ruleInstitutionRegistry"
    // InternalTextualRCV.g:182:1: ruleInstitutionRegistry returns [EObject current=null] : (otherlv_0= 'Institutions:' ( (lv_institutions_1_0= ruleInstitution ) )+ ) ;
    public final EObject ruleInstitutionRegistry() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_institutions_1_0 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:188:2: ( (otherlv_0= 'Institutions:' ( (lv_institutions_1_0= ruleInstitution ) )+ ) )
            // InternalTextualRCV.g:189:2: (otherlv_0= 'Institutions:' ( (lv_institutions_1_0= ruleInstitution ) )+ )
            {
            // InternalTextualRCV.g:189:2: (otherlv_0= 'Institutions:' ( (lv_institutions_1_0= ruleInstitution ) )+ )
            // InternalTextualRCV.g:190:3: otherlv_0= 'Institutions:' ( (lv_institutions_1_0= ruleInstitution ) )+
            {
            otherlv_0=(Token)match(input,19,FOLLOW_7); 

            			newLeafNode(otherlv_0, grammarAccess.getInstitutionRegistryAccess().getInstitutionsKeyword_0());
            		
            // InternalTextualRCV.g:194:3: ( (lv_institutions_1_0= ruleInstitution ) )+
            int cnt3=0;
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==RULE_ID) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalTextualRCV.g:195:4: (lv_institutions_1_0= ruleInstitution )
            	    {
            	    // InternalTextualRCV.g:195:4: (lv_institutions_1_0= ruleInstitution )
            	    // InternalTextualRCV.g:196:5: lv_institutions_1_0= ruleInstitution
            	    {

            	    					newCompositeNode(grammarAccess.getInstitutionRegistryAccess().getInstitutionsInstitutionParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_8);
            	    lv_institutions_1_0=ruleInstitution();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getInstitutionRegistryRule());
            	    					}
            	    					add(
            	    						current,
            	    						"institutions",
            	    						lv_institutions_1_0,
            	    						"org.montex.researchcv.xtext.TextualRCV.Institution");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt3 >= 1 ) break loop3;
                        EarlyExitException eee =
                            new EarlyExitException(3, input);
                        throw eee;
                }
                cnt3++;
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInstitutionRegistry"


    // $ANTLR start "entryRuleSupervisionsRegistry"
    // InternalTextualRCV.g:217:1: entryRuleSupervisionsRegistry returns [EObject current=null] : iv_ruleSupervisionsRegistry= ruleSupervisionsRegistry EOF ;
    public final EObject entryRuleSupervisionsRegistry() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSupervisionsRegistry = null;


        try {
            // InternalTextualRCV.g:217:61: (iv_ruleSupervisionsRegistry= ruleSupervisionsRegistry EOF )
            // InternalTextualRCV.g:218:2: iv_ruleSupervisionsRegistry= ruleSupervisionsRegistry EOF
            {
             newCompositeNode(grammarAccess.getSupervisionsRegistryRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSupervisionsRegistry=ruleSupervisionsRegistry();

            state._fsp--;

             current =iv_ruleSupervisionsRegistry; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSupervisionsRegistry"


    // $ANTLR start "ruleSupervisionsRegistry"
    // InternalTextualRCV.g:224:1: ruleSupervisionsRegistry returns [EObject current=null] : (otherlv_0= 'Supervisions:' ( (lv_supervisions_1_0= ruleSupervision ) )+ ) ;
    public final EObject ruleSupervisionsRegistry() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_supervisions_1_0 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:230:2: ( (otherlv_0= 'Supervisions:' ( (lv_supervisions_1_0= ruleSupervision ) )+ ) )
            // InternalTextualRCV.g:231:2: (otherlv_0= 'Supervisions:' ( (lv_supervisions_1_0= ruleSupervision ) )+ )
            {
            // InternalTextualRCV.g:231:2: (otherlv_0= 'Supervisions:' ( (lv_supervisions_1_0= ruleSupervision ) )+ )
            // InternalTextualRCV.g:232:3: otherlv_0= 'Supervisions:' ( (lv_supervisions_1_0= ruleSupervision ) )+
            {
            otherlv_0=(Token)match(input,20,FOLLOW_9); 

            			newLeafNode(otherlv_0, grammarAccess.getSupervisionsRegistryAccess().getSupervisionsKeyword_0());
            		
            // InternalTextualRCV.g:236:3: ( (lv_supervisions_1_0= ruleSupervision ) )+
            int cnt4=0;
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==RULE_INT||LA4_0==33) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalTextualRCV.g:237:4: (lv_supervisions_1_0= ruleSupervision )
            	    {
            	    // InternalTextualRCV.g:237:4: (lv_supervisions_1_0= ruleSupervision )
            	    // InternalTextualRCV.g:238:5: lv_supervisions_1_0= ruleSupervision
            	    {

            	    					newCompositeNode(grammarAccess.getSupervisionsRegistryAccess().getSupervisionsSupervisionParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_10);
            	    lv_supervisions_1_0=ruleSupervision();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getSupervisionsRegistryRule());
            	    					}
            	    					add(
            	    						current,
            	    						"supervisions",
            	    						lv_supervisions_1_0,
            	    						"org.montex.researchcv.xtext.TextualRCV.Supervision");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt4 >= 1 ) break loop4;
                        EarlyExitException eee =
                            new EarlyExitException(4, input);
                        throw eee;
                }
                cnt4++;
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSupervisionsRegistry"


    // $ANTLR start "entryRuleInstitution"
    // InternalTextualRCV.g:259:1: entryRuleInstitution returns [EObject current=null] : iv_ruleInstitution= ruleInstitution EOF ;
    public final EObject entryRuleInstitution() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInstitution = null;


        try {
            // InternalTextualRCV.g:259:52: (iv_ruleInstitution= ruleInstitution EOF )
            // InternalTextualRCV.g:260:2: iv_ruleInstitution= ruleInstitution EOF
            {
             newCompositeNode(grammarAccess.getInstitutionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInstitution=ruleInstitution();

            state._fsp--;

             current =iv_ruleInstitution; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInstitution"


    // $ANTLR start "ruleInstitution"
    // InternalTextualRCV.g:266:1: ruleInstitution returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '.' ( (lv_country_2_0= ruleCountryCode ) ) )? otherlv_3= '{' ( (lv_fullName_4_0= ruleMultiLanguageString ) ) ( (lv_units_5_0= ruleInstitution ) )* otherlv_6= '}' ) ;
    public final EObject ruleInstitution() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_6=null;
        AntlrDatatypeRuleToken lv_country_2_0 = null;

        EObject lv_fullName_4_0 = null;

        EObject lv_units_5_0 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:272:2: ( ( ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '.' ( (lv_country_2_0= ruleCountryCode ) ) )? otherlv_3= '{' ( (lv_fullName_4_0= ruleMultiLanguageString ) ) ( (lv_units_5_0= ruleInstitution ) )* otherlv_6= '}' ) )
            // InternalTextualRCV.g:273:2: ( ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '.' ( (lv_country_2_0= ruleCountryCode ) ) )? otherlv_3= '{' ( (lv_fullName_4_0= ruleMultiLanguageString ) ) ( (lv_units_5_0= ruleInstitution ) )* otherlv_6= '}' )
            {
            // InternalTextualRCV.g:273:2: ( ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '.' ( (lv_country_2_0= ruleCountryCode ) ) )? otherlv_3= '{' ( (lv_fullName_4_0= ruleMultiLanguageString ) ) ( (lv_units_5_0= ruleInstitution ) )* otherlv_6= '}' )
            // InternalTextualRCV.g:274:3: ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '.' ( (lv_country_2_0= ruleCountryCode ) ) )? otherlv_3= '{' ( (lv_fullName_4_0= ruleMultiLanguageString ) ) ( (lv_units_5_0= ruleInstitution ) )* otherlv_6= '}'
            {
            // InternalTextualRCV.g:274:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalTextualRCV.g:275:4: (lv_name_0_0= RULE_ID )
            {
            // InternalTextualRCV.g:275:4: (lv_name_0_0= RULE_ID )
            // InternalTextualRCV.g:276:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_11); 

            					newLeafNode(lv_name_0_0, grammarAccess.getInstitutionAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getInstitutionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalTextualRCV.g:292:3: (otherlv_1= '.' ( (lv_country_2_0= ruleCountryCode ) ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==21) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalTextualRCV.g:293:4: otherlv_1= '.' ( (lv_country_2_0= ruleCountryCode ) )
                    {
                    otherlv_1=(Token)match(input,21,FOLLOW_12); 

                    				newLeafNode(otherlv_1, grammarAccess.getInstitutionAccess().getFullStopKeyword_1_0());
                    			
                    // InternalTextualRCV.g:297:4: ( (lv_country_2_0= ruleCountryCode ) )
                    // InternalTextualRCV.g:298:5: (lv_country_2_0= ruleCountryCode )
                    {
                    // InternalTextualRCV.g:298:5: (lv_country_2_0= ruleCountryCode )
                    // InternalTextualRCV.g:299:6: lv_country_2_0= ruleCountryCode
                    {

                    						newCompositeNode(grammarAccess.getInstitutionAccess().getCountryCountryCodeParserRuleCall_1_1_0());
                    					
                    pushFollow(FOLLOW_3);
                    lv_country_2_0=ruleCountryCode();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getInstitutionRule());
                    						}
                    						set(
                    							current,
                    							"country",
                    							lv_country_2_0,
                    							"org.montex.researchcv.xtext.TextualRCV.CountryCode");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_3=(Token)match(input,16,FOLLOW_13); 

            			newLeafNode(otherlv_3, grammarAccess.getInstitutionAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalTextualRCV.g:321:3: ( (lv_fullName_4_0= ruleMultiLanguageString ) )
            // InternalTextualRCV.g:322:4: (lv_fullName_4_0= ruleMultiLanguageString )
            {
            // InternalTextualRCV.g:322:4: (lv_fullName_4_0= ruleMultiLanguageString )
            // InternalTextualRCV.g:323:5: lv_fullName_4_0= ruleMultiLanguageString
            {

            					newCompositeNode(grammarAccess.getInstitutionAccess().getFullNameMultiLanguageStringParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_14);
            lv_fullName_4_0=ruleMultiLanguageString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getInstitutionRule());
            					}
            					set(
            						current,
            						"fullName",
            						lv_fullName_4_0,
            						"org.montex.researchcv.xtext.TextualRCV.MultiLanguageString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:340:3: ( (lv_units_5_0= ruleInstitution ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==RULE_ID) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalTextualRCV.g:341:4: (lv_units_5_0= ruleInstitution )
            	    {
            	    // InternalTextualRCV.g:341:4: (lv_units_5_0= ruleInstitution )
            	    // InternalTextualRCV.g:342:5: lv_units_5_0= ruleInstitution
            	    {

            	    					newCompositeNode(grammarAccess.getInstitutionAccess().getUnitsInstitutionParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_14);
            	    lv_units_5_0=ruleInstitution();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getInstitutionRule());
            	    					}
            	    					add(
            	    						current,
            	    						"units",
            	    						lv_units_5_0,
            	    						"org.montex.researchcv.xtext.TextualRCV.Institution");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            otherlv_6=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getInstitutionAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInstitution"


    // $ANTLR start "entryRuleSupervision"
    // InternalTextualRCV.g:367:1: entryRuleSupervision returns [EObject current=null] : iv_ruleSupervision= ruleSupervision EOF ;
    public final EObject entryRuleSupervision() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSupervision = null;


        try {
            // InternalTextualRCV.g:367:52: (iv_ruleSupervision= ruleSupervision EOF )
            // InternalTextualRCV.g:368:2: iv_ruleSupervision= ruleSupervision EOF
            {
             newCompositeNode(grammarAccess.getSupervisionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSupervision=ruleSupervision();

            state._fsp--;

             current =iv_ruleSupervision; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSupervision"


    // $ANTLR start "ruleSupervision"
    // InternalTextualRCV.g:374:1: ruleSupervision returns [EObject current=null] : ( ( ( (lv_dateEnd_0_0= rulePublishedDate ) ) | ( ( (lv_dasteStart_1_0= rulePublishedDate ) ) otherlv_2= '>' ( (lv_dateEnd_3_0= rulePublishedDate ) )? ) ) ( (lv_level_4_0= ruleSupervisionKind ) ) otherlv_5= '@' ( ( ruleFQN ) ) (otherlv_7= ',' ( ( ruleFQN ) ) )* otherlv_9= '{' ( ( ruleEString ) ) otherlv_11= '<-' ( ( ruleEString ) ) (otherlv_13= '+' ( ( ruleEString ) ) )* (otherlv_15= '[' ( (lv_projectTitle_16_0= RULE_STRING ) ) otherlv_17= ']' )? ( (lv_finalWorkURL_18_0= RULE_URL ) )? (otherlv_19= 'doi:' ( (lv_finalWorkDOI_20_0= ruleDOI ) ) )? otherlv_21= '}' ) ;
    public final EObject ruleSupervision() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token lv_projectTitle_16_0=null;
        Token otherlv_17=null;
        Token lv_finalWorkURL_18_0=null;
        Token otherlv_19=null;
        Token otherlv_21=null;
        EObject lv_dateEnd_0_0 = null;

        EObject lv_dasteStart_1_0 = null;

        EObject lv_dateEnd_3_0 = null;

        Enumerator lv_level_4_0 = null;

        AntlrDatatypeRuleToken lv_finalWorkDOI_20_0 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:380:2: ( ( ( ( (lv_dateEnd_0_0= rulePublishedDate ) ) | ( ( (lv_dasteStart_1_0= rulePublishedDate ) ) otherlv_2= '>' ( (lv_dateEnd_3_0= rulePublishedDate ) )? ) ) ( (lv_level_4_0= ruleSupervisionKind ) ) otherlv_5= '@' ( ( ruleFQN ) ) (otherlv_7= ',' ( ( ruleFQN ) ) )* otherlv_9= '{' ( ( ruleEString ) ) otherlv_11= '<-' ( ( ruleEString ) ) (otherlv_13= '+' ( ( ruleEString ) ) )* (otherlv_15= '[' ( (lv_projectTitle_16_0= RULE_STRING ) ) otherlv_17= ']' )? ( (lv_finalWorkURL_18_0= RULE_URL ) )? (otherlv_19= 'doi:' ( (lv_finalWorkDOI_20_0= ruleDOI ) ) )? otherlv_21= '}' ) )
            // InternalTextualRCV.g:381:2: ( ( ( (lv_dateEnd_0_0= rulePublishedDate ) ) | ( ( (lv_dasteStart_1_0= rulePublishedDate ) ) otherlv_2= '>' ( (lv_dateEnd_3_0= rulePublishedDate ) )? ) ) ( (lv_level_4_0= ruleSupervisionKind ) ) otherlv_5= '@' ( ( ruleFQN ) ) (otherlv_7= ',' ( ( ruleFQN ) ) )* otherlv_9= '{' ( ( ruleEString ) ) otherlv_11= '<-' ( ( ruleEString ) ) (otherlv_13= '+' ( ( ruleEString ) ) )* (otherlv_15= '[' ( (lv_projectTitle_16_0= RULE_STRING ) ) otherlv_17= ']' )? ( (lv_finalWorkURL_18_0= RULE_URL ) )? (otherlv_19= 'doi:' ( (lv_finalWorkDOI_20_0= ruleDOI ) ) )? otherlv_21= '}' )
            {
            // InternalTextualRCV.g:381:2: ( ( ( (lv_dateEnd_0_0= rulePublishedDate ) ) | ( ( (lv_dasteStart_1_0= rulePublishedDate ) ) otherlv_2= '>' ( (lv_dateEnd_3_0= rulePublishedDate ) )? ) ) ( (lv_level_4_0= ruleSupervisionKind ) ) otherlv_5= '@' ( ( ruleFQN ) ) (otherlv_7= ',' ( ( ruleFQN ) ) )* otherlv_9= '{' ( ( ruleEString ) ) otherlv_11= '<-' ( ( ruleEString ) ) (otherlv_13= '+' ( ( ruleEString ) ) )* (otherlv_15= '[' ( (lv_projectTitle_16_0= RULE_STRING ) ) otherlv_17= ']' )? ( (lv_finalWorkURL_18_0= RULE_URL ) )? (otherlv_19= 'doi:' ( (lv_finalWorkDOI_20_0= ruleDOI ) ) )? otherlv_21= '}' )
            // InternalTextualRCV.g:382:3: ( ( (lv_dateEnd_0_0= rulePublishedDate ) ) | ( ( (lv_dasteStart_1_0= rulePublishedDate ) ) otherlv_2= '>' ( (lv_dateEnd_3_0= rulePublishedDate ) )? ) ) ( (lv_level_4_0= ruleSupervisionKind ) ) otherlv_5= '@' ( ( ruleFQN ) ) (otherlv_7= ',' ( ( ruleFQN ) ) )* otherlv_9= '{' ( ( ruleEString ) ) otherlv_11= '<-' ( ( ruleEString ) ) (otherlv_13= '+' ( ( ruleEString ) ) )* (otherlv_15= '[' ( (lv_projectTitle_16_0= RULE_STRING ) ) otherlv_17= ']' )? ( (lv_finalWorkURL_18_0= RULE_URL ) )? (otherlv_19= 'doi:' ( (lv_finalWorkDOI_20_0= ruleDOI ) ) )? otherlv_21= '}'
            {
            // InternalTextualRCV.g:382:3: ( ( (lv_dateEnd_0_0= rulePublishedDate ) ) | ( ( (lv_dasteStart_1_0= rulePublishedDate ) ) otherlv_2= '>' ( (lv_dateEnd_3_0= rulePublishedDate ) )? ) )
            int alt8=2;
            alt8 = dfa8.predict(input);
            switch (alt8) {
                case 1 :
                    // InternalTextualRCV.g:383:4: ( (lv_dateEnd_0_0= rulePublishedDate ) )
                    {
                    // InternalTextualRCV.g:383:4: ( (lv_dateEnd_0_0= rulePublishedDate ) )
                    // InternalTextualRCV.g:384:5: (lv_dateEnd_0_0= rulePublishedDate )
                    {
                    // InternalTextualRCV.g:384:5: (lv_dateEnd_0_0= rulePublishedDate )
                    // InternalTextualRCV.g:385:6: lv_dateEnd_0_0= rulePublishedDate
                    {

                    						newCompositeNode(grammarAccess.getSupervisionAccess().getDateEndPublishedDateParserRuleCall_0_0_0());
                    					
                    pushFollow(FOLLOW_15);
                    lv_dateEnd_0_0=rulePublishedDate();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getSupervisionRule());
                    						}
                    						set(
                    							current,
                    							"dateEnd",
                    							lv_dateEnd_0_0,
                    							"org.montex.researchcv.xtext.TextualRCV.PublishedDate");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalTextualRCV.g:403:4: ( ( (lv_dasteStart_1_0= rulePublishedDate ) ) otherlv_2= '>' ( (lv_dateEnd_3_0= rulePublishedDate ) )? )
                    {
                    // InternalTextualRCV.g:403:4: ( ( (lv_dasteStart_1_0= rulePublishedDate ) ) otherlv_2= '>' ( (lv_dateEnd_3_0= rulePublishedDate ) )? )
                    // InternalTextualRCV.g:404:5: ( (lv_dasteStart_1_0= rulePublishedDate ) ) otherlv_2= '>' ( (lv_dateEnd_3_0= rulePublishedDate ) )?
                    {
                    // InternalTextualRCV.g:404:5: ( (lv_dasteStart_1_0= rulePublishedDate ) )
                    // InternalTextualRCV.g:405:6: (lv_dasteStart_1_0= rulePublishedDate )
                    {
                    // InternalTextualRCV.g:405:6: (lv_dasteStart_1_0= rulePublishedDate )
                    // InternalTextualRCV.g:406:7: lv_dasteStart_1_0= rulePublishedDate
                    {

                    							newCompositeNode(grammarAccess.getSupervisionAccess().getDasteStartPublishedDateParserRuleCall_0_1_0_0());
                    						
                    pushFollow(FOLLOW_16);
                    lv_dasteStart_1_0=rulePublishedDate();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getSupervisionRule());
                    							}
                    							set(
                    								current,
                    								"dasteStart",
                    								lv_dasteStart_1_0,
                    								"org.montex.researchcv.xtext.TextualRCV.PublishedDate");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }

                    otherlv_2=(Token)match(input,22,FOLLOW_17); 

                    					newLeafNode(otherlv_2, grammarAccess.getSupervisionAccess().getGreaterThanSignKeyword_0_1_1());
                    				
                    // InternalTextualRCV.g:427:5: ( (lv_dateEnd_3_0= rulePublishedDate ) )?
                    int alt7=2;
                    int LA7_0 = input.LA(1);

                    if ( (LA7_0==RULE_INT||LA7_0==33) ) {
                        alt7=1;
                    }
                    switch (alt7) {
                        case 1 :
                            // InternalTextualRCV.g:428:6: (lv_dateEnd_3_0= rulePublishedDate )
                            {
                            // InternalTextualRCV.g:428:6: (lv_dateEnd_3_0= rulePublishedDate )
                            // InternalTextualRCV.g:429:7: lv_dateEnd_3_0= rulePublishedDate
                            {

                            							newCompositeNode(grammarAccess.getSupervisionAccess().getDateEndPublishedDateParserRuleCall_0_1_2_0());
                            						
                            pushFollow(FOLLOW_15);
                            lv_dateEnd_3_0=rulePublishedDate();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getSupervisionRule());
                            							}
                            							set(
                            								current,
                            								"dateEnd",
                            								lv_dateEnd_3_0,
                            								"org.montex.researchcv.xtext.TextualRCV.PublishedDate");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }
                            break;

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:448:3: ( (lv_level_4_0= ruleSupervisionKind ) )
            // InternalTextualRCV.g:449:4: (lv_level_4_0= ruleSupervisionKind )
            {
            // InternalTextualRCV.g:449:4: (lv_level_4_0= ruleSupervisionKind )
            // InternalTextualRCV.g:450:5: lv_level_4_0= ruleSupervisionKind
            {

            					newCompositeNode(grammarAccess.getSupervisionAccess().getLevelSupervisionKindEnumRuleCall_1_0());
            				
            pushFollow(FOLLOW_18);
            lv_level_4_0=ruleSupervisionKind();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSupervisionRule());
            					}
            					set(
            						current,
            						"level",
            						lv_level_4_0,
            						"org.montex.researchcv.xtext.TextualRCV.SupervisionKind");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,23,FOLLOW_7); 

            			newLeafNode(otherlv_5, grammarAccess.getSupervisionAccess().getCommercialAtKeyword_2());
            		
            // InternalTextualRCV.g:471:3: ( ( ruleFQN ) )
            // InternalTextualRCV.g:472:4: ( ruleFQN )
            {
            // InternalTextualRCV.g:472:4: ( ruleFQN )
            // InternalTextualRCV.g:473:5: ruleFQN
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getSupervisionRule());
            					}
            				

            					newCompositeNode(grammarAccess.getSupervisionAccess().getInstitutionsInstitutionCrossReference_3_0());
            				
            pushFollow(FOLLOW_19);
            ruleFQN();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:487:3: (otherlv_7= ',' ( ( ruleFQN ) ) )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==24) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalTextualRCV.g:488:4: otherlv_7= ',' ( ( ruleFQN ) )
            	    {
            	    otherlv_7=(Token)match(input,24,FOLLOW_7); 

            	    				newLeafNode(otherlv_7, grammarAccess.getSupervisionAccess().getCommaKeyword_4_0());
            	    			
            	    // InternalTextualRCV.g:492:4: ( ( ruleFQN ) )
            	    // InternalTextualRCV.g:493:5: ( ruleFQN )
            	    {
            	    // InternalTextualRCV.g:493:5: ( ruleFQN )
            	    // InternalTextualRCV.g:494:6: ruleFQN
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getSupervisionRule());
            	    						}
            	    					

            	    						newCompositeNode(grammarAccess.getSupervisionAccess().getInstitutionsInstitutionCrossReference_4_1_0());
            	    					
            	    pushFollow(FOLLOW_19);
            	    ruleFQN();

            	    state._fsp--;


            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

            otherlv_9=(Token)match(input,16,FOLLOW_20); 

            			newLeafNode(otherlv_9, grammarAccess.getSupervisionAccess().getLeftCurlyBracketKeyword_5());
            		
            // InternalTextualRCV.g:513:3: ( ( ruleEString ) )
            // InternalTextualRCV.g:514:4: ( ruleEString )
            {
            // InternalTextualRCV.g:514:4: ( ruleEString )
            // InternalTextualRCV.g:515:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getSupervisionRule());
            					}
            				

            					newCompositeNode(grammarAccess.getSupervisionAccess().getStudentPersonCrossReference_6_0());
            				
            pushFollow(FOLLOW_21);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_11=(Token)match(input,25,FOLLOW_20); 

            			newLeafNode(otherlv_11, grammarAccess.getSupervisionAccess().getLessThanSignHyphenMinusKeyword_7());
            		
            // InternalTextualRCV.g:533:3: ( ( ruleEString ) )
            // InternalTextualRCV.g:534:4: ( ruleEString )
            {
            // InternalTextualRCV.g:534:4: ( ruleEString )
            // InternalTextualRCV.g:535:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getSupervisionRule());
            					}
            				

            					newCompositeNode(grammarAccess.getSupervisionAccess().getSupervisorsPersonCrossReference_8_0());
            				
            pushFollow(FOLLOW_22);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:549:3: (otherlv_13= '+' ( ( ruleEString ) ) )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==26) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalTextualRCV.g:550:4: otherlv_13= '+' ( ( ruleEString ) )
            	    {
            	    otherlv_13=(Token)match(input,26,FOLLOW_20); 

            	    				newLeafNode(otherlv_13, grammarAccess.getSupervisionAccess().getPlusSignKeyword_9_0());
            	    			
            	    // InternalTextualRCV.g:554:4: ( ( ruleEString ) )
            	    // InternalTextualRCV.g:555:5: ( ruleEString )
            	    {
            	    // InternalTextualRCV.g:555:5: ( ruleEString )
            	    // InternalTextualRCV.g:556:6: ruleEString
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getSupervisionRule());
            	    						}
            	    					

            	    						newCompositeNode(grammarAccess.getSupervisionAccess().getSupervisorsPersonCrossReference_9_1_0());
            	    					
            	    pushFollow(FOLLOW_22);
            	    ruleEString();

            	    state._fsp--;


            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

            // InternalTextualRCV.g:571:3: (otherlv_15= '[' ( (lv_projectTitle_16_0= RULE_STRING ) ) otherlv_17= ']' )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==27) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalTextualRCV.g:572:4: otherlv_15= '[' ( (lv_projectTitle_16_0= RULE_STRING ) ) otherlv_17= ']'
                    {
                    otherlv_15=(Token)match(input,27,FOLLOW_13); 

                    				newLeafNode(otherlv_15, grammarAccess.getSupervisionAccess().getLeftSquareBracketKeyword_10_0());
                    			
                    // InternalTextualRCV.g:576:4: ( (lv_projectTitle_16_0= RULE_STRING ) )
                    // InternalTextualRCV.g:577:5: (lv_projectTitle_16_0= RULE_STRING )
                    {
                    // InternalTextualRCV.g:577:5: (lv_projectTitle_16_0= RULE_STRING )
                    // InternalTextualRCV.g:578:6: lv_projectTitle_16_0= RULE_STRING
                    {
                    lv_projectTitle_16_0=(Token)match(input,RULE_STRING,FOLLOW_23); 

                    						newLeafNode(lv_projectTitle_16_0, grammarAccess.getSupervisionAccess().getProjectTitleSTRINGTerminalRuleCall_10_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getSupervisionRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"projectTitle",
                    							lv_projectTitle_16_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }

                    otherlv_17=(Token)match(input,28,FOLLOW_24); 

                    				newLeafNode(otherlv_17, grammarAccess.getSupervisionAccess().getRightSquareBracketKeyword_10_2());
                    			

                    }
                    break;

            }

            // InternalTextualRCV.g:599:3: ( (lv_finalWorkURL_18_0= RULE_URL ) )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==RULE_URL) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalTextualRCV.g:600:4: (lv_finalWorkURL_18_0= RULE_URL )
                    {
                    // InternalTextualRCV.g:600:4: (lv_finalWorkURL_18_0= RULE_URL )
                    // InternalTextualRCV.g:601:5: lv_finalWorkURL_18_0= RULE_URL
                    {
                    lv_finalWorkURL_18_0=(Token)match(input,RULE_URL,FOLLOW_25); 

                    					newLeafNode(lv_finalWorkURL_18_0, grammarAccess.getSupervisionAccess().getFinalWorkURLURLTerminalRuleCall_11_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getSupervisionRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"finalWorkURL",
                    						lv_finalWorkURL_18_0,
                    						"org.montex.researchcv.xtext.TextualRCV.URL");
                    				

                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:617:3: (otherlv_19= 'doi:' ( (lv_finalWorkDOI_20_0= ruleDOI ) ) )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==29) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalTextualRCV.g:618:4: otherlv_19= 'doi:' ( (lv_finalWorkDOI_20_0= ruleDOI ) )
                    {
                    otherlv_19=(Token)match(input,29,FOLLOW_26); 

                    				newLeafNode(otherlv_19, grammarAccess.getSupervisionAccess().getDoiKeyword_12_0());
                    			
                    // InternalTextualRCV.g:622:4: ( (lv_finalWorkDOI_20_0= ruleDOI ) )
                    // InternalTextualRCV.g:623:5: (lv_finalWorkDOI_20_0= ruleDOI )
                    {
                    // InternalTextualRCV.g:623:5: (lv_finalWorkDOI_20_0= ruleDOI )
                    // InternalTextualRCV.g:624:6: lv_finalWorkDOI_20_0= ruleDOI
                    {

                    						newCompositeNode(grammarAccess.getSupervisionAccess().getFinalWorkDOIDOIParserRuleCall_12_1_0());
                    					
                    pushFollow(FOLLOW_27);
                    lv_finalWorkDOI_20_0=ruleDOI();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getSupervisionRule());
                    						}
                    						set(
                    							current,
                    							"finalWorkDOI",
                    							lv_finalWorkDOI_20_0,
                    							"org.montex.researchcv.xtext.TextualRCV.DOI");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_21=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_21, grammarAccess.getSupervisionAccess().getRightCurlyBracketKeyword_13());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSupervision"


    // $ANTLR start "entryRuleDOI"
    // InternalTextualRCV.g:650:1: entryRuleDOI returns [String current=null] : iv_ruleDOI= ruleDOI EOF ;
    public final String entryRuleDOI() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleDOI = null;


        try {
            // InternalTextualRCV.g:650:43: (iv_ruleDOI= ruleDOI EOF )
            // InternalTextualRCV.g:651:2: iv_ruleDOI= ruleDOI EOF
            {
             newCompositeNode(grammarAccess.getDOIRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDOI=ruleDOI();

            state._fsp--;

             current =iv_ruleDOI.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDOI"


    // $ANTLR start "ruleDOI"
    // InternalTextualRCV.g:657:1: ruleDOI returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_INT_0= RULE_INT | this_ID_1= RULE_ID | kw= '.' | kw= '/' )+ ;
    public final AntlrDatatypeRuleToken ruleDOI() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_INT_0=null;
        Token this_ID_1=null;
        Token kw=null;


        	enterRule();

        try {
            // InternalTextualRCV.g:663:2: ( (this_INT_0= RULE_INT | this_ID_1= RULE_ID | kw= '.' | kw= '/' )+ )
            // InternalTextualRCV.g:664:2: (this_INT_0= RULE_INT | this_ID_1= RULE_ID | kw= '.' | kw= '/' )+
            {
            // InternalTextualRCV.g:664:2: (this_INT_0= RULE_INT | this_ID_1= RULE_ID | kw= '.' | kw= '/' )+
            int cnt14=0;
            loop14:
            do {
                int alt14=5;
                switch ( input.LA(1) ) {
                case RULE_INT:
                    {
                    alt14=1;
                    }
                    break;
                case RULE_ID:
                    {
                    alt14=2;
                    }
                    break;
                case 21:
                    {
                    alt14=3;
                    }
                    break;
                case 30:
                    {
                    alt14=4;
                    }
                    break;

                }

                switch (alt14) {
            	case 1 :
            	    // InternalTextualRCV.g:665:3: this_INT_0= RULE_INT
            	    {
            	    this_INT_0=(Token)match(input,RULE_INT,FOLLOW_28); 

            	    			current.merge(this_INT_0);
            	    		

            	    			newLeafNode(this_INT_0, grammarAccess.getDOIAccess().getINTTerminalRuleCall_0());
            	    		

            	    }
            	    break;
            	case 2 :
            	    // InternalTextualRCV.g:673:3: this_ID_1= RULE_ID
            	    {
            	    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_28); 

            	    			current.merge(this_ID_1);
            	    		

            	    			newLeafNode(this_ID_1, grammarAccess.getDOIAccess().getIDTerminalRuleCall_1());
            	    		

            	    }
            	    break;
            	case 3 :
            	    // InternalTextualRCV.g:681:3: kw= '.'
            	    {
            	    kw=(Token)match(input,21,FOLLOW_28); 

            	    			current.merge(kw);
            	    			newLeafNode(kw, grammarAccess.getDOIAccess().getFullStopKeyword_2());
            	    		

            	    }
            	    break;
            	case 4 :
            	    // InternalTextualRCV.g:687:3: kw= '/'
            	    {
            	    kw=(Token)match(input,30,FOLLOW_28); 

            	    			current.merge(kw);
            	    			newLeafNode(kw, grammarAccess.getDOIAccess().getSolidusKeyword_3());
            	    		

            	    }
            	    break;

            	default :
            	    if ( cnt14 >= 1 ) break loop14;
                        EarlyExitException eee =
                            new EarlyExitException(14, input);
                        throw eee;
                }
                cnt14++;
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDOI"


    // $ANTLR start "entryRuleFQN"
    // InternalTextualRCV.g:696:1: entryRuleFQN returns [String current=null] : iv_ruleFQN= ruleFQN EOF ;
    public final String entryRuleFQN() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleFQN = null;


        try {
            // InternalTextualRCV.g:696:43: (iv_ruleFQN= ruleFQN EOF )
            // InternalTextualRCV.g:697:2: iv_ruleFQN= ruleFQN EOF
            {
             newCompositeNode(grammarAccess.getFQNRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFQN=ruleFQN();

            state._fsp--;

             current =iv_ruleFQN.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFQN"


    // $ANTLR start "ruleFQN"
    // InternalTextualRCV.g:703:1: ruleFQN returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleFQN() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;


        	enterRule();

        try {
            // InternalTextualRCV.g:709:2: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // InternalTextualRCV.g:710:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // InternalTextualRCV.g:710:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // InternalTextualRCV.g:711:3: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_29); 

            			current.merge(this_ID_0);
            		

            			newLeafNode(this_ID_0, grammarAccess.getFQNAccess().getIDTerminalRuleCall_0());
            		
            // InternalTextualRCV.g:718:3: (kw= '.' this_ID_2= RULE_ID )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==21) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalTextualRCV.g:719:4: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,21,FOLLOW_7); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getFQNAccess().getFullStopKeyword_1_0());
            	    			
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_29); 

            	    				current.merge(this_ID_2);
            	    			

            	    				newLeafNode(this_ID_2, grammarAccess.getFQNAccess().getIDTerminalRuleCall_1_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFQN"


    // $ANTLR start "entryRuleMultiLanguageString"
    // InternalTextualRCV.g:736:1: entryRuleMultiLanguageString returns [EObject current=null] : iv_ruleMultiLanguageString= ruleMultiLanguageString EOF ;
    public final EObject entryRuleMultiLanguageString() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMultiLanguageString = null;


        try {
            // InternalTextualRCV.g:736:60: (iv_ruleMultiLanguageString= ruleMultiLanguageString EOF )
            // InternalTextualRCV.g:737:2: iv_ruleMultiLanguageString= ruleMultiLanguageString EOF
            {
             newCompositeNode(grammarAccess.getMultiLanguageStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMultiLanguageString=ruleMultiLanguageString();

            state._fsp--;

             current =iv_ruleMultiLanguageString; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMultiLanguageString"


    // $ANTLR start "ruleMultiLanguageString"
    // InternalTextualRCV.g:743:1: ruleMultiLanguageString returns [EObject current=null] : ( (lv_localizations_0_0= ruleLocalizedString ) )+ ;
    public final EObject ruleMultiLanguageString() throws RecognitionException {
        EObject current = null;

        EObject lv_localizations_0_0 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:749:2: ( ( (lv_localizations_0_0= ruleLocalizedString ) )+ )
            // InternalTextualRCV.g:750:2: ( (lv_localizations_0_0= ruleLocalizedString ) )+
            {
            // InternalTextualRCV.g:750:2: ( (lv_localizations_0_0= ruleLocalizedString ) )+
            int cnt16=0;
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==RULE_STRING) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalTextualRCV.g:751:3: (lv_localizations_0_0= ruleLocalizedString )
            	    {
            	    // InternalTextualRCV.g:751:3: (lv_localizations_0_0= ruleLocalizedString )
            	    // InternalTextualRCV.g:752:4: lv_localizations_0_0= ruleLocalizedString
            	    {

            	    				newCompositeNode(grammarAccess.getMultiLanguageStringAccess().getLocalizationsLocalizedStringParserRuleCall_0());
            	    			
            	    pushFollow(FOLLOW_30);
            	    lv_localizations_0_0=ruleLocalizedString();

            	    state._fsp--;


            	    				if (current==null) {
            	    					current = createModelElementForParent(grammarAccess.getMultiLanguageStringRule());
            	    				}
            	    				add(
            	    					current,
            	    					"localizations",
            	    					lv_localizations_0_0,
            	    					"org.montex.researchcv.xtext.TextualRCV.LocalizedString");
            	    				afterParserOrEnumRuleCall();
            	    			

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt16 >= 1 ) break loop16;
                        EarlyExitException eee =
                            new EarlyExitException(16, input);
                        throw eee;
                }
                cnt16++;
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMultiLanguageString"


    // $ANTLR start "entryRuleLocalizedString"
    // InternalTextualRCV.g:772:1: entryRuleLocalizedString returns [EObject current=null] : iv_ruleLocalizedString= ruleLocalizedString EOF ;
    public final EObject entryRuleLocalizedString() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLocalizedString = null;



        	HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens();

        try {
            // InternalTextualRCV.g:774:2: (iv_ruleLocalizedString= ruleLocalizedString EOF )
            // InternalTextualRCV.g:775:2: iv_ruleLocalizedString= ruleLocalizedString EOF
            {
             newCompositeNode(grammarAccess.getLocalizedStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLocalizedString=ruleLocalizedString();

            state._fsp--;

             current =iv_ruleLocalizedString; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "entryRuleLocalizedString"


    // $ANTLR start "ruleLocalizedString"
    // InternalTextualRCV.g:784:1: ruleLocalizedString returns [EObject current=null] : ( ( (lv_text_0_0= RULE_STRING ) ) otherlv_1= '/' ( (lv_language_2_0= ruleLanguageCode ) ) ) ;
    public final EObject ruleLocalizedString() throws RecognitionException {
        EObject current = null;

        Token lv_text_0_0=null;
        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_language_2_0 = null;



        	enterRule();
        	HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens();

        try {
            // InternalTextualRCV.g:791:2: ( ( ( (lv_text_0_0= RULE_STRING ) ) otherlv_1= '/' ( (lv_language_2_0= ruleLanguageCode ) ) ) )
            // InternalTextualRCV.g:792:2: ( ( (lv_text_0_0= RULE_STRING ) ) otherlv_1= '/' ( (lv_language_2_0= ruleLanguageCode ) ) )
            {
            // InternalTextualRCV.g:792:2: ( ( (lv_text_0_0= RULE_STRING ) ) otherlv_1= '/' ( (lv_language_2_0= ruleLanguageCode ) ) )
            // InternalTextualRCV.g:793:3: ( (lv_text_0_0= RULE_STRING ) ) otherlv_1= '/' ( (lv_language_2_0= ruleLanguageCode ) )
            {
            // InternalTextualRCV.g:793:3: ( (lv_text_0_0= RULE_STRING ) )
            // InternalTextualRCV.g:794:4: (lv_text_0_0= RULE_STRING )
            {
            // InternalTextualRCV.g:794:4: (lv_text_0_0= RULE_STRING )
            // InternalTextualRCV.g:795:5: lv_text_0_0= RULE_STRING
            {
            lv_text_0_0=(Token)match(input,RULE_STRING,FOLLOW_31); 

            					newLeafNode(lv_text_0_0, grammarAccess.getLocalizedStringAccess().getTextSTRINGTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getLocalizedStringRule());
            					}
            					setWithLastConsumed(
            						current,
            						"text",
            						lv_text_0_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }

            otherlv_1=(Token)match(input,30,FOLLOW_12); 

            			newLeafNode(otherlv_1, grammarAccess.getLocalizedStringAccess().getSolidusKeyword_1());
            		
            // InternalTextualRCV.g:815:3: ( (lv_language_2_0= ruleLanguageCode ) )
            // InternalTextualRCV.g:816:4: (lv_language_2_0= ruleLanguageCode )
            {
            // InternalTextualRCV.g:816:4: (lv_language_2_0= ruleLanguageCode )
            // InternalTextualRCV.g:817:5: lv_language_2_0= ruleLanguageCode
            {

            					newCompositeNode(grammarAccess.getLocalizedStringAccess().getLanguageLanguageCodeParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_language_2_0=ruleLanguageCode();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getLocalizedStringRule());
            					}
            					set(
            						current,
            						"language",
            						lv_language_2_0,
            						"org.montex.researchcv.xtext.TextualRCV.LanguageCode");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "ruleLocalizedString"


    // $ANTLR start "entryRuleCountryCode"
    // InternalTextualRCV.g:841:1: entryRuleCountryCode returns [String current=null] : iv_ruleCountryCode= ruleCountryCode EOF ;
    public final String entryRuleCountryCode() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleCountryCode = null;


        try {
            // InternalTextualRCV.g:841:51: (iv_ruleCountryCode= ruleCountryCode EOF )
            // InternalTextualRCV.g:842:2: iv_ruleCountryCode= ruleCountryCode EOF
            {
             newCompositeNode(grammarAccess.getCountryCodeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCountryCode=ruleCountryCode();

            state._fsp--;

             current =iv_ruleCountryCode.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCountryCode"


    // $ANTLR start "ruleCountryCode"
    // InternalTextualRCV.g:848:1: ruleCountryCode returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_COUNTRY_CODE_0= RULE_COUNTRY_CODE ;
    public final AntlrDatatypeRuleToken ruleCountryCode() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_COUNTRY_CODE_0=null;


        	enterRule();

        try {
            // InternalTextualRCV.g:854:2: (this_COUNTRY_CODE_0= RULE_COUNTRY_CODE )
            // InternalTextualRCV.g:855:2: this_COUNTRY_CODE_0= RULE_COUNTRY_CODE
            {
            this_COUNTRY_CODE_0=(Token)match(input,RULE_COUNTRY_CODE,FOLLOW_2); 

            		current.merge(this_COUNTRY_CODE_0);
            	

            		newLeafNode(this_COUNTRY_CODE_0, grammarAccess.getCountryCodeAccess().getCOUNTRY_CODETerminalRuleCall());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCountryCode"


    // $ANTLR start "entryRuleLanguageCode"
    // InternalTextualRCV.g:865:1: entryRuleLanguageCode returns [String current=null] : iv_ruleLanguageCode= ruleLanguageCode EOF ;
    public final String entryRuleLanguageCode() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleLanguageCode = null;


        try {
            // InternalTextualRCV.g:865:52: (iv_ruleLanguageCode= ruleLanguageCode EOF )
            // InternalTextualRCV.g:866:2: iv_ruleLanguageCode= ruleLanguageCode EOF
            {
             newCompositeNode(grammarAccess.getLanguageCodeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLanguageCode=ruleLanguageCode();

            state._fsp--;

             current =iv_ruleLanguageCode.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLanguageCode"


    // $ANTLR start "ruleLanguageCode"
    // InternalTextualRCV.g:872:1: ruleLanguageCode returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_COUNTRY_CODE_0= RULE_COUNTRY_CODE ;
    public final AntlrDatatypeRuleToken ruleLanguageCode() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_COUNTRY_CODE_0=null;


        	enterRule();

        try {
            // InternalTextualRCV.g:878:2: (this_COUNTRY_CODE_0= RULE_COUNTRY_CODE )
            // InternalTextualRCV.g:879:2: this_COUNTRY_CODE_0= RULE_COUNTRY_CODE
            {
            this_COUNTRY_CODE_0=(Token)match(input,RULE_COUNTRY_CODE,FOLLOW_2); 

            		current.merge(this_COUNTRY_CODE_0);
            	

            		newLeafNode(this_COUNTRY_CODE_0, grammarAccess.getLanguageCodeAccess().getCOUNTRY_CODETerminalRuleCall());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLanguageCode"


    // $ANTLR start "entryRuleResearcher"
    // InternalTextualRCV.g:889:1: entryRuleResearcher returns [EObject current=null] : iv_ruleResearcher= ruleResearcher EOF ;
    public final EObject entryRuleResearcher() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleResearcher = null;


        try {
            // InternalTextualRCV.g:889:51: (iv_ruleResearcher= ruleResearcher EOF )
            // InternalTextualRCV.g:890:2: iv_ruleResearcher= ruleResearcher EOF
            {
             newCompositeNode(grammarAccess.getResearcherRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleResearcher=ruleResearcher();

            state._fsp--;

             current =iv_ruleResearcher; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleResearcher"


    // $ANTLR start "ruleResearcher"
    // InternalTextualRCV.g:896:1: ruleResearcher returns [EObject current=null] : (otherlv_0= 'researcher' ( (lv_lastNames_1_0= ruleEString ) )+ otherlv_2= ',' ( (lv_firstNames_3_0= ruleEString ) )+ otherlv_4= '{' (otherlv_5= 'contacts' otherlv_6= '{' otherlv_7= '-' ( (lv_contacts_8_0= ruleContact ) ) (otherlv_9= '-' ( (lv_contacts_10_0= ruleContact ) ) )* otherlv_11= '}' )? (otherlv_12= 'socials' otherlv_13= '{' ( (lv_socials_14_0= ruleSocialProfile ) ) (otherlv_15= ',' ( (lv_socials_16_0= ruleSocialProfile ) ) )* otherlv_17= '}' )? (otherlv_18= 'publications' otherlv_19= '{' ( (lv_publications_20_0= rulePublication ) ) (otherlv_21= ',' ( (lv_publications_22_0= rulePublication ) ) )* otherlv_23= '}' )? (otherlv_24= 'teachingMaterial' otherlv_25= '{' ( (lv_teachingMaterial_26_0= rulePublication ) ) (otherlv_27= ',' ( (lv_teachingMaterial_28_0= rulePublication ) ) )* otherlv_29= '}' )? (otherlv_30= 'otherMaterial' otherlv_31= '{' ( (lv_otherMaterial_32_0= rulePublication ) ) (otherlv_33= ',' ( (lv_otherMaterial_34_0= rulePublication ) ) )* otherlv_35= '}' )? (otherlv_36= 'researchGroup' ( (lv_researchGroup_37_0= ruleResearchGroup ) ) )? (otherlv_38= 'topics' otherlv_39= '{' ( (lv_topics_40_0= ruleResearchTopic ) ) (otherlv_41= ',' ( (lv_topics_42_0= ruleResearchTopic ) ) )* otherlv_43= '}' )? (otherlv_44= 'grants' otherlv_45= '{' ( (lv_grants_46_0= ruleGrant ) ) (otherlv_47= ',' ( (lv_grants_48_0= ruleGrant ) ) )* otherlv_49= '}' )? (otherlv_50= 'courses' otherlv_51= '{' ( (lv_courses_52_0= ruleCourse ) ) (otherlv_53= ',' ( (lv_courses_54_0= ruleCourse ) ) )* otherlv_55= '}' )? (otherlv_56= 'taughtCourses' otherlv_57= '{' ( (lv_taughtCourses_58_0= ruleTaughtCourse ) ) (otherlv_59= ',' ( (lv_taughtCourses_60_0= ruleTaughtCourse ) ) )* otherlv_61= '}' )? otherlv_62= '}' ) ;
    public final EObject ruleResearcher() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        Token otherlv_19=null;
        Token otherlv_21=null;
        Token otherlv_23=null;
        Token otherlv_24=null;
        Token otherlv_25=null;
        Token otherlv_27=null;
        Token otherlv_29=null;
        Token otherlv_30=null;
        Token otherlv_31=null;
        Token otherlv_33=null;
        Token otherlv_35=null;
        Token otherlv_36=null;
        Token otherlv_38=null;
        Token otherlv_39=null;
        Token otherlv_41=null;
        Token otherlv_43=null;
        Token otherlv_44=null;
        Token otherlv_45=null;
        Token otherlv_47=null;
        Token otherlv_49=null;
        Token otherlv_50=null;
        Token otherlv_51=null;
        Token otherlv_53=null;
        Token otherlv_55=null;
        Token otherlv_56=null;
        Token otherlv_57=null;
        Token otherlv_59=null;
        Token otherlv_61=null;
        Token otherlv_62=null;
        AntlrDatatypeRuleToken lv_lastNames_1_0 = null;

        AntlrDatatypeRuleToken lv_firstNames_3_0 = null;

        EObject lv_contacts_8_0 = null;

        EObject lv_contacts_10_0 = null;

        EObject lv_socials_14_0 = null;

        EObject lv_socials_16_0 = null;

        EObject lv_publications_20_0 = null;

        EObject lv_publications_22_0 = null;

        EObject lv_teachingMaterial_26_0 = null;

        EObject lv_teachingMaterial_28_0 = null;

        EObject lv_otherMaterial_32_0 = null;

        EObject lv_otherMaterial_34_0 = null;

        EObject lv_researchGroup_37_0 = null;

        EObject lv_topics_40_0 = null;

        EObject lv_topics_42_0 = null;

        EObject lv_grants_46_0 = null;

        EObject lv_grants_48_0 = null;

        EObject lv_courses_52_0 = null;

        EObject lv_courses_54_0 = null;

        EObject lv_taughtCourses_58_0 = null;

        EObject lv_taughtCourses_60_0 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:902:2: ( (otherlv_0= 'researcher' ( (lv_lastNames_1_0= ruleEString ) )+ otherlv_2= ',' ( (lv_firstNames_3_0= ruleEString ) )+ otherlv_4= '{' (otherlv_5= 'contacts' otherlv_6= '{' otherlv_7= '-' ( (lv_contacts_8_0= ruleContact ) ) (otherlv_9= '-' ( (lv_contacts_10_0= ruleContact ) ) )* otherlv_11= '}' )? (otherlv_12= 'socials' otherlv_13= '{' ( (lv_socials_14_0= ruleSocialProfile ) ) (otherlv_15= ',' ( (lv_socials_16_0= ruleSocialProfile ) ) )* otherlv_17= '}' )? (otherlv_18= 'publications' otherlv_19= '{' ( (lv_publications_20_0= rulePublication ) ) (otherlv_21= ',' ( (lv_publications_22_0= rulePublication ) ) )* otherlv_23= '}' )? (otherlv_24= 'teachingMaterial' otherlv_25= '{' ( (lv_teachingMaterial_26_0= rulePublication ) ) (otherlv_27= ',' ( (lv_teachingMaterial_28_0= rulePublication ) ) )* otherlv_29= '}' )? (otherlv_30= 'otherMaterial' otherlv_31= '{' ( (lv_otherMaterial_32_0= rulePublication ) ) (otherlv_33= ',' ( (lv_otherMaterial_34_0= rulePublication ) ) )* otherlv_35= '}' )? (otherlv_36= 'researchGroup' ( (lv_researchGroup_37_0= ruleResearchGroup ) ) )? (otherlv_38= 'topics' otherlv_39= '{' ( (lv_topics_40_0= ruleResearchTopic ) ) (otherlv_41= ',' ( (lv_topics_42_0= ruleResearchTopic ) ) )* otherlv_43= '}' )? (otherlv_44= 'grants' otherlv_45= '{' ( (lv_grants_46_0= ruleGrant ) ) (otherlv_47= ',' ( (lv_grants_48_0= ruleGrant ) ) )* otherlv_49= '}' )? (otherlv_50= 'courses' otherlv_51= '{' ( (lv_courses_52_0= ruleCourse ) ) (otherlv_53= ',' ( (lv_courses_54_0= ruleCourse ) ) )* otherlv_55= '}' )? (otherlv_56= 'taughtCourses' otherlv_57= '{' ( (lv_taughtCourses_58_0= ruleTaughtCourse ) ) (otherlv_59= ',' ( (lv_taughtCourses_60_0= ruleTaughtCourse ) ) )* otherlv_61= '}' )? otherlv_62= '}' ) )
            // InternalTextualRCV.g:903:2: (otherlv_0= 'researcher' ( (lv_lastNames_1_0= ruleEString ) )+ otherlv_2= ',' ( (lv_firstNames_3_0= ruleEString ) )+ otherlv_4= '{' (otherlv_5= 'contacts' otherlv_6= '{' otherlv_7= '-' ( (lv_contacts_8_0= ruleContact ) ) (otherlv_9= '-' ( (lv_contacts_10_0= ruleContact ) ) )* otherlv_11= '}' )? (otherlv_12= 'socials' otherlv_13= '{' ( (lv_socials_14_0= ruleSocialProfile ) ) (otherlv_15= ',' ( (lv_socials_16_0= ruleSocialProfile ) ) )* otherlv_17= '}' )? (otherlv_18= 'publications' otherlv_19= '{' ( (lv_publications_20_0= rulePublication ) ) (otherlv_21= ',' ( (lv_publications_22_0= rulePublication ) ) )* otherlv_23= '}' )? (otherlv_24= 'teachingMaterial' otherlv_25= '{' ( (lv_teachingMaterial_26_0= rulePublication ) ) (otherlv_27= ',' ( (lv_teachingMaterial_28_0= rulePublication ) ) )* otherlv_29= '}' )? (otherlv_30= 'otherMaterial' otherlv_31= '{' ( (lv_otherMaterial_32_0= rulePublication ) ) (otherlv_33= ',' ( (lv_otherMaterial_34_0= rulePublication ) ) )* otherlv_35= '}' )? (otherlv_36= 'researchGroup' ( (lv_researchGroup_37_0= ruleResearchGroup ) ) )? (otherlv_38= 'topics' otherlv_39= '{' ( (lv_topics_40_0= ruleResearchTopic ) ) (otherlv_41= ',' ( (lv_topics_42_0= ruleResearchTopic ) ) )* otherlv_43= '}' )? (otherlv_44= 'grants' otherlv_45= '{' ( (lv_grants_46_0= ruleGrant ) ) (otherlv_47= ',' ( (lv_grants_48_0= ruleGrant ) ) )* otherlv_49= '}' )? (otherlv_50= 'courses' otherlv_51= '{' ( (lv_courses_52_0= ruleCourse ) ) (otherlv_53= ',' ( (lv_courses_54_0= ruleCourse ) ) )* otherlv_55= '}' )? (otherlv_56= 'taughtCourses' otherlv_57= '{' ( (lv_taughtCourses_58_0= ruleTaughtCourse ) ) (otherlv_59= ',' ( (lv_taughtCourses_60_0= ruleTaughtCourse ) ) )* otherlv_61= '}' )? otherlv_62= '}' )
            {
            // InternalTextualRCV.g:903:2: (otherlv_0= 'researcher' ( (lv_lastNames_1_0= ruleEString ) )+ otherlv_2= ',' ( (lv_firstNames_3_0= ruleEString ) )+ otherlv_4= '{' (otherlv_5= 'contacts' otherlv_6= '{' otherlv_7= '-' ( (lv_contacts_8_0= ruleContact ) ) (otherlv_9= '-' ( (lv_contacts_10_0= ruleContact ) ) )* otherlv_11= '}' )? (otherlv_12= 'socials' otherlv_13= '{' ( (lv_socials_14_0= ruleSocialProfile ) ) (otherlv_15= ',' ( (lv_socials_16_0= ruleSocialProfile ) ) )* otherlv_17= '}' )? (otherlv_18= 'publications' otherlv_19= '{' ( (lv_publications_20_0= rulePublication ) ) (otherlv_21= ',' ( (lv_publications_22_0= rulePublication ) ) )* otherlv_23= '}' )? (otherlv_24= 'teachingMaterial' otherlv_25= '{' ( (lv_teachingMaterial_26_0= rulePublication ) ) (otherlv_27= ',' ( (lv_teachingMaterial_28_0= rulePublication ) ) )* otherlv_29= '}' )? (otherlv_30= 'otherMaterial' otherlv_31= '{' ( (lv_otherMaterial_32_0= rulePublication ) ) (otherlv_33= ',' ( (lv_otherMaterial_34_0= rulePublication ) ) )* otherlv_35= '}' )? (otherlv_36= 'researchGroup' ( (lv_researchGroup_37_0= ruleResearchGroup ) ) )? (otherlv_38= 'topics' otherlv_39= '{' ( (lv_topics_40_0= ruleResearchTopic ) ) (otherlv_41= ',' ( (lv_topics_42_0= ruleResearchTopic ) ) )* otherlv_43= '}' )? (otherlv_44= 'grants' otherlv_45= '{' ( (lv_grants_46_0= ruleGrant ) ) (otherlv_47= ',' ( (lv_grants_48_0= ruleGrant ) ) )* otherlv_49= '}' )? (otherlv_50= 'courses' otherlv_51= '{' ( (lv_courses_52_0= ruleCourse ) ) (otherlv_53= ',' ( (lv_courses_54_0= ruleCourse ) ) )* otherlv_55= '}' )? (otherlv_56= 'taughtCourses' otherlv_57= '{' ( (lv_taughtCourses_58_0= ruleTaughtCourse ) ) (otherlv_59= ',' ( (lv_taughtCourses_60_0= ruleTaughtCourse ) ) )* otherlv_61= '}' )? otherlv_62= '}' )
            // InternalTextualRCV.g:904:3: otherlv_0= 'researcher' ( (lv_lastNames_1_0= ruleEString ) )+ otherlv_2= ',' ( (lv_firstNames_3_0= ruleEString ) )+ otherlv_4= '{' (otherlv_5= 'contacts' otherlv_6= '{' otherlv_7= '-' ( (lv_contacts_8_0= ruleContact ) ) (otherlv_9= '-' ( (lv_contacts_10_0= ruleContact ) ) )* otherlv_11= '}' )? (otherlv_12= 'socials' otherlv_13= '{' ( (lv_socials_14_0= ruleSocialProfile ) ) (otherlv_15= ',' ( (lv_socials_16_0= ruleSocialProfile ) ) )* otherlv_17= '}' )? (otherlv_18= 'publications' otherlv_19= '{' ( (lv_publications_20_0= rulePublication ) ) (otherlv_21= ',' ( (lv_publications_22_0= rulePublication ) ) )* otherlv_23= '}' )? (otherlv_24= 'teachingMaterial' otherlv_25= '{' ( (lv_teachingMaterial_26_0= rulePublication ) ) (otherlv_27= ',' ( (lv_teachingMaterial_28_0= rulePublication ) ) )* otherlv_29= '}' )? (otherlv_30= 'otherMaterial' otherlv_31= '{' ( (lv_otherMaterial_32_0= rulePublication ) ) (otherlv_33= ',' ( (lv_otherMaterial_34_0= rulePublication ) ) )* otherlv_35= '}' )? (otherlv_36= 'researchGroup' ( (lv_researchGroup_37_0= ruleResearchGroup ) ) )? (otherlv_38= 'topics' otherlv_39= '{' ( (lv_topics_40_0= ruleResearchTopic ) ) (otherlv_41= ',' ( (lv_topics_42_0= ruleResearchTopic ) ) )* otherlv_43= '}' )? (otherlv_44= 'grants' otherlv_45= '{' ( (lv_grants_46_0= ruleGrant ) ) (otherlv_47= ',' ( (lv_grants_48_0= ruleGrant ) ) )* otherlv_49= '}' )? (otherlv_50= 'courses' otherlv_51= '{' ( (lv_courses_52_0= ruleCourse ) ) (otherlv_53= ',' ( (lv_courses_54_0= ruleCourse ) ) )* otherlv_55= '}' )? (otherlv_56= 'taughtCourses' otherlv_57= '{' ( (lv_taughtCourses_58_0= ruleTaughtCourse ) ) (otherlv_59= ',' ( (lv_taughtCourses_60_0= ruleTaughtCourse ) ) )* otherlv_61= '}' )? otherlv_62= '}'
            {
            otherlv_0=(Token)match(input,31,FOLLOW_20); 

            			newLeafNode(otherlv_0, grammarAccess.getResearcherAccess().getResearcherKeyword_0());
            		
            // InternalTextualRCV.g:908:3: ( (lv_lastNames_1_0= ruleEString ) )+
            int cnt17=0;
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( ((LA17_0>=RULE_ID && LA17_0<=RULE_STRING)) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // InternalTextualRCV.g:909:4: (lv_lastNames_1_0= ruleEString )
            	    {
            	    // InternalTextualRCV.g:909:4: (lv_lastNames_1_0= ruleEString )
            	    // InternalTextualRCV.g:910:5: lv_lastNames_1_0= ruleEString
            	    {

            	    					newCompositeNode(grammarAccess.getResearcherAccess().getLastNamesEStringParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_32);
            	    lv_lastNames_1_0=ruleEString();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getResearcherRule());
            	    					}
            	    					add(
            	    						current,
            	    						"lastNames",
            	    						lv_lastNames_1_0,
            	    						"org.montex.researchcv.xtext.TextualRCV.EString");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt17 >= 1 ) break loop17;
                        EarlyExitException eee =
                            new EarlyExitException(17, input);
                        throw eee;
                }
                cnt17++;
            } while (true);

            otherlv_2=(Token)match(input,24,FOLLOW_20); 

            			newLeafNode(otherlv_2, grammarAccess.getResearcherAccess().getCommaKeyword_2());
            		
            // InternalTextualRCV.g:931:3: ( (lv_firstNames_3_0= ruleEString ) )+
            int cnt18=0;
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( ((LA18_0>=RULE_ID && LA18_0<=RULE_STRING)) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // InternalTextualRCV.g:932:4: (lv_firstNames_3_0= ruleEString )
            	    {
            	    // InternalTextualRCV.g:932:4: (lv_firstNames_3_0= ruleEString )
            	    // InternalTextualRCV.g:933:5: lv_firstNames_3_0= ruleEString
            	    {

            	    					newCompositeNode(grammarAccess.getResearcherAccess().getFirstNamesEStringParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_33);
            	    lv_firstNames_3_0=ruleEString();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getResearcherRule());
            	    					}
            	    					add(
            	    						current,
            	    						"firstNames",
            	    						lv_firstNames_3_0,
            	    						"org.montex.researchcv.xtext.TextualRCV.EString");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt18 >= 1 ) break loop18;
                        EarlyExitException eee =
                            new EarlyExitException(18, input);
                        throw eee;
                }
                cnt18++;
            } while (true);

            otherlv_4=(Token)match(input,16,FOLLOW_34); 

            			newLeafNode(otherlv_4, grammarAccess.getResearcherAccess().getLeftCurlyBracketKeyword_4());
            		
            // InternalTextualRCV.g:954:3: (otherlv_5= 'contacts' otherlv_6= '{' otherlv_7= '-' ( (lv_contacts_8_0= ruleContact ) ) (otherlv_9= '-' ( (lv_contacts_10_0= ruleContact ) ) )* otherlv_11= '}' )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==32) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // InternalTextualRCV.g:955:4: otherlv_5= 'contacts' otherlv_6= '{' otherlv_7= '-' ( (lv_contacts_8_0= ruleContact ) ) (otherlv_9= '-' ( (lv_contacts_10_0= ruleContact ) ) )* otherlv_11= '}'
                    {
                    otherlv_5=(Token)match(input,32,FOLLOW_3); 

                    				newLeafNode(otherlv_5, grammarAccess.getResearcherAccess().getContactsKeyword_5_0());
                    			
                    otherlv_6=(Token)match(input,16,FOLLOW_35); 

                    				newLeafNode(otherlv_6, grammarAccess.getResearcherAccess().getLeftCurlyBracketKeyword_5_1());
                    			
                    otherlv_7=(Token)match(input,33,FOLLOW_36); 

                    				newLeafNode(otherlv_7, grammarAccess.getResearcherAccess().getHyphenMinusKeyword_5_2());
                    			
                    // InternalTextualRCV.g:967:4: ( (lv_contacts_8_0= ruleContact ) )
                    // InternalTextualRCV.g:968:5: (lv_contacts_8_0= ruleContact )
                    {
                    // InternalTextualRCV.g:968:5: (lv_contacts_8_0= ruleContact )
                    // InternalTextualRCV.g:969:6: lv_contacts_8_0= ruleContact
                    {

                    						newCompositeNode(grammarAccess.getResearcherAccess().getContactsContactParserRuleCall_5_3_0());
                    					
                    pushFollow(FOLLOW_37);
                    lv_contacts_8_0=ruleContact();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getResearcherRule());
                    						}
                    						add(
                    							current,
                    							"contacts",
                    							lv_contacts_8_0,
                    							"org.montex.researchcv.xtext.TextualRCV.Contact");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:986:4: (otherlv_9= '-' ( (lv_contacts_10_0= ruleContact ) ) )*
                    loop19:
                    do {
                        int alt19=2;
                        int LA19_0 = input.LA(1);

                        if ( (LA19_0==33) ) {
                            alt19=1;
                        }


                        switch (alt19) {
                    	case 1 :
                    	    // InternalTextualRCV.g:987:5: otherlv_9= '-' ( (lv_contacts_10_0= ruleContact ) )
                    	    {
                    	    otherlv_9=(Token)match(input,33,FOLLOW_36); 

                    	    					newLeafNode(otherlv_9, grammarAccess.getResearcherAccess().getHyphenMinusKeyword_5_4_0());
                    	    				
                    	    // InternalTextualRCV.g:991:5: ( (lv_contacts_10_0= ruleContact ) )
                    	    // InternalTextualRCV.g:992:6: (lv_contacts_10_0= ruleContact )
                    	    {
                    	    // InternalTextualRCV.g:992:6: (lv_contacts_10_0= ruleContact )
                    	    // InternalTextualRCV.g:993:7: lv_contacts_10_0= ruleContact
                    	    {

                    	    							newCompositeNode(grammarAccess.getResearcherAccess().getContactsContactParserRuleCall_5_4_1_0());
                    	    						
                    	    pushFollow(FOLLOW_37);
                    	    lv_contacts_10_0=ruleContact();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getResearcherRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"contacts",
                    	    								lv_contacts_10_0,
                    	    								"org.montex.researchcv.xtext.TextualRCV.Contact");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop19;
                        }
                    } while (true);

                    otherlv_11=(Token)match(input,18,FOLLOW_38); 

                    				newLeafNode(otherlv_11, grammarAccess.getResearcherAccess().getRightCurlyBracketKeyword_5_5());
                    			

                    }
                    break;

            }

            // InternalTextualRCV.g:1016:3: (otherlv_12= 'socials' otherlv_13= '{' ( (lv_socials_14_0= ruleSocialProfile ) ) (otherlv_15= ',' ( (lv_socials_16_0= ruleSocialProfile ) ) )* otherlv_17= '}' )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==34) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // InternalTextualRCV.g:1017:4: otherlv_12= 'socials' otherlv_13= '{' ( (lv_socials_14_0= ruleSocialProfile ) ) (otherlv_15= ',' ( (lv_socials_16_0= ruleSocialProfile ) ) )* otherlv_17= '}'
                    {
                    otherlv_12=(Token)match(input,34,FOLLOW_3); 

                    				newLeafNode(otherlv_12, grammarAccess.getResearcherAccess().getSocialsKeyword_6_0());
                    			
                    otherlv_13=(Token)match(input,16,FOLLOW_39); 

                    				newLeafNode(otherlv_13, grammarAccess.getResearcherAccess().getLeftCurlyBracketKeyword_6_1());
                    			
                    // InternalTextualRCV.g:1025:4: ( (lv_socials_14_0= ruleSocialProfile ) )
                    // InternalTextualRCV.g:1026:5: (lv_socials_14_0= ruleSocialProfile )
                    {
                    // InternalTextualRCV.g:1026:5: (lv_socials_14_0= ruleSocialProfile )
                    // InternalTextualRCV.g:1027:6: lv_socials_14_0= ruleSocialProfile
                    {

                    						newCompositeNode(grammarAccess.getResearcherAccess().getSocialsSocialProfileParserRuleCall_6_2_0());
                    					
                    pushFollow(FOLLOW_40);
                    lv_socials_14_0=ruleSocialProfile();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getResearcherRule());
                    						}
                    						add(
                    							current,
                    							"socials",
                    							lv_socials_14_0,
                    							"org.montex.researchcv.xtext.TextualRCV.SocialProfile");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:1044:4: (otherlv_15= ',' ( (lv_socials_16_0= ruleSocialProfile ) ) )*
                    loop21:
                    do {
                        int alt21=2;
                        int LA21_0 = input.LA(1);

                        if ( (LA21_0==24) ) {
                            alt21=1;
                        }


                        switch (alt21) {
                    	case 1 :
                    	    // InternalTextualRCV.g:1045:5: otherlv_15= ',' ( (lv_socials_16_0= ruleSocialProfile ) )
                    	    {
                    	    otherlv_15=(Token)match(input,24,FOLLOW_39); 

                    	    					newLeafNode(otherlv_15, grammarAccess.getResearcherAccess().getCommaKeyword_6_3_0());
                    	    				
                    	    // InternalTextualRCV.g:1049:5: ( (lv_socials_16_0= ruleSocialProfile ) )
                    	    // InternalTextualRCV.g:1050:6: (lv_socials_16_0= ruleSocialProfile )
                    	    {
                    	    // InternalTextualRCV.g:1050:6: (lv_socials_16_0= ruleSocialProfile )
                    	    // InternalTextualRCV.g:1051:7: lv_socials_16_0= ruleSocialProfile
                    	    {

                    	    							newCompositeNode(grammarAccess.getResearcherAccess().getSocialsSocialProfileParserRuleCall_6_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_40);
                    	    lv_socials_16_0=ruleSocialProfile();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getResearcherRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"socials",
                    	    								lv_socials_16_0,
                    	    								"org.montex.researchcv.xtext.TextualRCV.SocialProfile");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop21;
                        }
                    } while (true);

                    otherlv_17=(Token)match(input,18,FOLLOW_41); 

                    				newLeafNode(otherlv_17, grammarAccess.getResearcherAccess().getRightCurlyBracketKeyword_6_4());
                    			

                    }
                    break;

            }

            // InternalTextualRCV.g:1074:3: (otherlv_18= 'publications' otherlv_19= '{' ( (lv_publications_20_0= rulePublication ) ) (otherlv_21= ',' ( (lv_publications_22_0= rulePublication ) ) )* otherlv_23= '}' )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==35) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // InternalTextualRCV.g:1075:4: otherlv_18= 'publications' otherlv_19= '{' ( (lv_publications_20_0= rulePublication ) ) (otherlv_21= ',' ( (lv_publications_22_0= rulePublication ) ) )* otherlv_23= '}'
                    {
                    otherlv_18=(Token)match(input,35,FOLLOW_3); 

                    				newLeafNode(otherlv_18, grammarAccess.getResearcherAccess().getPublicationsKeyword_7_0());
                    			
                    otherlv_19=(Token)match(input,16,FOLLOW_42); 

                    				newLeafNode(otherlv_19, grammarAccess.getResearcherAccess().getLeftCurlyBracketKeyword_7_1());
                    			
                    // InternalTextualRCV.g:1083:4: ( (lv_publications_20_0= rulePublication ) )
                    // InternalTextualRCV.g:1084:5: (lv_publications_20_0= rulePublication )
                    {
                    // InternalTextualRCV.g:1084:5: (lv_publications_20_0= rulePublication )
                    // InternalTextualRCV.g:1085:6: lv_publications_20_0= rulePublication
                    {

                    						newCompositeNode(grammarAccess.getResearcherAccess().getPublicationsPublicationParserRuleCall_7_2_0());
                    					
                    pushFollow(FOLLOW_40);
                    lv_publications_20_0=rulePublication();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getResearcherRule());
                    						}
                    						add(
                    							current,
                    							"publications",
                    							lv_publications_20_0,
                    							"org.montex.researchcv.xtext.TextualRCV.Publication");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:1102:4: (otherlv_21= ',' ( (lv_publications_22_0= rulePublication ) ) )*
                    loop23:
                    do {
                        int alt23=2;
                        int LA23_0 = input.LA(1);

                        if ( (LA23_0==24) ) {
                            alt23=1;
                        }


                        switch (alt23) {
                    	case 1 :
                    	    // InternalTextualRCV.g:1103:5: otherlv_21= ',' ( (lv_publications_22_0= rulePublication ) )
                    	    {
                    	    otherlv_21=(Token)match(input,24,FOLLOW_42); 

                    	    					newLeafNode(otherlv_21, grammarAccess.getResearcherAccess().getCommaKeyword_7_3_0());
                    	    				
                    	    // InternalTextualRCV.g:1107:5: ( (lv_publications_22_0= rulePublication ) )
                    	    // InternalTextualRCV.g:1108:6: (lv_publications_22_0= rulePublication )
                    	    {
                    	    // InternalTextualRCV.g:1108:6: (lv_publications_22_0= rulePublication )
                    	    // InternalTextualRCV.g:1109:7: lv_publications_22_0= rulePublication
                    	    {

                    	    							newCompositeNode(grammarAccess.getResearcherAccess().getPublicationsPublicationParserRuleCall_7_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_40);
                    	    lv_publications_22_0=rulePublication();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getResearcherRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"publications",
                    	    								lv_publications_22_0,
                    	    								"org.montex.researchcv.xtext.TextualRCV.Publication");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop23;
                        }
                    } while (true);

                    otherlv_23=(Token)match(input,18,FOLLOW_43); 

                    				newLeafNode(otherlv_23, grammarAccess.getResearcherAccess().getRightCurlyBracketKeyword_7_4());
                    			

                    }
                    break;

            }

            // InternalTextualRCV.g:1132:3: (otherlv_24= 'teachingMaterial' otherlv_25= '{' ( (lv_teachingMaterial_26_0= rulePublication ) ) (otherlv_27= ',' ( (lv_teachingMaterial_28_0= rulePublication ) ) )* otherlv_29= '}' )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==36) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // InternalTextualRCV.g:1133:4: otherlv_24= 'teachingMaterial' otherlv_25= '{' ( (lv_teachingMaterial_26_0= rulePublication ) ) (otherlv_27= ',' ( (lv_teachingMaterial_28_0= rulePublication ) ) )* otherlv_29= '}'
                    {
                    otherlv_24=(Token)match(input,36,FOLLOW_3); 

                    				newLeafNode(otherlv_24, grammarAccess.getResearcherAccess().getTeachingMaterialKeyword_8_0());
                    			
                    otherlv_25=(Token)match(input,16,FOLLOW_42); 

                    				newLeafNode(otherlv_25, grammarAccess.getResearcherAccess().getLeftCurlyBracketKeyword_8_1());
                    			
                    // InternalTextualRCV.g:1141:4: ( (lv_teachingMaterial_26_0= rulePublication ) )
                    // InternalTextualRCV.g:1142:5: (lv_teachingMaterial_26_0= rulePublication )
                    {
                    // InternalTextualRCV.g:1142:5: (lv_teachingMaterial_26_0= rulePublication )
                    // InternalTextualRCV.g:1143:6: lv_teachingMaterial_26_0= rulePublication
                    {

                    						newCompositeNode(grammarAccess.getResearcherAccess().getTeachingMaterialPublicationParserRuleCall_8_2_0());
                    					
                    pushFollow(FOLLOW_40);
                    lv_teachingMaterial_26_0=rulePublication();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getResearcherRule());
                    						}
                    						add(
                    							current,
                    							"teachingMaterial",
                    							lv_teachingMaterial_26_0,
                    							"org.montex.researchcv.xtext.TextualRCV.Publication");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:1160:4: (otherlv_27= ',' ( (lv_teachingMaterial_28_0= rulePublication ) ) )*
                    loop25:
                    do {
                        int alt25=2;
                        int LA25_0 = input.LA(1);

                        if ( (LA25_0==24) ) {
                            alt25=1;
                        }


                        switch (alt25) {
                    	case 1 :
                    	    // InternalTextualRCV.g:1161:5: otherlv_27= ',' ( (lv_teachingMaterial_28_0= rulePublication ) )
                    	    {
                    	    otherlv_27=(Token)match(input,24,FOLLOW_42); 

                    	    					newLeafNode(otherlv_27, grammarAccess.getResearcherAccess().getCommaKeyword_8_3_0());
                    	    				
                    	    // InternalTextualRCV.g:1165:5: ( (lv_teachingMaterial_28_0= rulePublication ) )
                    	    // InternalTextualRCV.g:1166:6: (lv_teachingMaterial_28_0= rulePublication )
                    	    {
                    	    // InternalTextualRCV.g:1166:6: (lv_teachingMaterial_28_0= rulePublication )
                    	    // InternalTextualRCV.g:1167:7: lv_teachingMaterial_28_0= rulePublication
                    	    {

                    	    							newCompositeNode(grammarAccess.getResearcherAccess().getTeachingMaterialPublicationParserRuleCall_8_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_40);
                    	    lv_teachingMaterial_28_0=rulePublication();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getResearcherRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"teachingMaterial",
                    	    								lv_teachingMaterial_28_0,
                    	    								"org.montex.researchcv.xtext.TextualRCV.Publication");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop25;
                        }
                    } while (true);

                    otherlv_29=(Token)match(input,18,FOLLOW_44); 

                    				newLeafNode(otherlv_29, grammarAccess.getResearcherAccess().getRightCurlyBracketKeyword_8_4());
                    			

                    }
                    break;

            }

            // InternalTextualRCV.g:1190:3: (otherlv_30= 'otherMaterial' otherlv_31= '{' ( (lv_otherMaterial_32_0= rulePublication ) ) (otherlv_33= ',' ( (lv_otherMaterial_34_0= rulePublication ) ) )* otherlv_35= '}' )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==37) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // InternalTextualRCV.g:1191:4: otherlv_30= 'otherMaterial' otherlv_31= '{' ( (lv_otherMaterial_32_0= rulePublication ) ) (otherlv_33= ',' ( (lv_otherMaterial_34_0= rulePublication ) ) )* otherlv_35= '}'
                    {
                    otherlv_30=(Token)match(input,37,FOLLOW_3); 

                    				newLeafNode(otherlv_30, grammarAccess.getResearcherAccess().getOtherMaterialKeyword_9_0());
                    			
                    otherlv_31=(Token)match(input,16,FOLLOW_42); 

                    				newLeafNode(otherlv_31, grammarAccess.getResearcherAccess().getLeftCurlyBracketKeyword_9_1());
                    			
                    // InternalTextualRCV.g:1199:4: ( (lv_otherMaterial_32_0= rulePublication ) )
                    // InternalTextualRCV.g:1200:5: (lv_otherMaterial_32_0= rulePublication )
                    {
                    // InternalTextualRCV.g:1200:5: (lv_otherMaterial_32_0= rulePublication )
                    // InternalTextualRCV.g:1201:6: lv_otherMaterial_32_0= rulePublication
                    {

                    						newCompositeNode(grammarAccess.getResearcherAccess().getOtherMaterialPublicationParserRuleCall_9_2_0());
                    					
                    pushFollow(FOLLOW_40);
                    lv_otherMaterial_32_0=rulePublication();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getResearcherRule());
                    						}
                    						add(
                    							current,
                    							"otherMaterial",
                    							lv_otherMaterial_32_0,
                    							"org.montex.researchcv.xtext.TextualRCV.Publication");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:1218:4: (otherlv_33= ',' ( (lv_otherMaterial_34_0= rulePublication ) ) )*
                    loop27:
                    do {
                        int alt27=2;
                        int LA27_0 = input.LA(1);

                        if ( (LA27_0==24) ) {
                            alt27=1;
                        }


                        switch (alt27) {
                    	case 1 :
                    	    // InternalTextualRCV.g:1219:5: otherlv_33= ',' ( (lv_otherMaterial_34_0= rulePublication ) )
                    	    {
                    	    otherlv_33=(Token)match(input,24,FOLLOW_42); 

                    	    					newLeafNode(otherlv_33, grammarAccess.getResearcherAccess().getCommaKeyword_9_3_0());
                    	    				
                    	    // InternalTextualRCV.g:1223:5: ( (lv_otherMaterial_34_0= rulePublication ) )
                    	    // InternalTextualRCV.g:1224:6: (lv_otherMaterial_34_0= rulePublication )
                    	    {
                    	    // InternalTextualRCV.g:1224:6: (lv_otherMaterial_34_0= rulePublication )
                    	    // InternalTextualRCV.g:1225:7: lv_otherMaterial_34_0= rulePublication
                    	    {

                    	    							newCompositeNode(grammarAccess.getResearcherAccess().getOtherMaterialPublicationParserRuleCall_9_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_40);
                    	    lv_otherMaterial_34_0=rulePublication();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getResearcherRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"otherMaterial",
                    	    								lv_otherMaterial_34_0,
                    	    								"org.montex.researchcv.xtext.TextualRCV.Publication");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop27;
                        }
                    } while (true);

                    otherlv_35=(Token)match(input,18,FOLLOW_45); 

                    				newLeafNode(otherlv_35, grammarAccess.getResearcherAccess().getRightCurlyBracketKeyword_9_4());
                    			

                    }
                    break;

            }

            // InternalTextualRCV.g:1248:3: (otherlv_36= 'researchGroup' ( (lv_researchGroup_37_0= ruleResearchGroup ) ) )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==38) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // InternalTextualRCV.g:1249:4: otherlv_36= 'researchGroup' ( (lv_researchGroup_37_0= ruleResearchGroup ) )
                    {
                    otherlv_36=(Token)match(input,38,FOLLOW_46); 

                    				newLeafNode(otherlv_36, grammarAccess.getResearcherAccess().getResearchGroupKeyword_10_0());
                    			
                    // InternalTextualRCV.g:1253:4: ( (lv_researchGroup_37_0= ruleResearchGroup ) )
                    // InternalTextualRCV.g:1254:5: (lv_researchGroup_37_0= ruleResearchGroup )
                    {
                    // InternalTextualRCV.g:1254:5: (lv_researchGroup_37_0= ruleResearchGroup )
                    // InternalTextualRCV.g:1255:6: lv_researchGroup_37_0= ruleResearchGroup
                    {

                    						newCompositeNode(grammarAccess.getResearcherAccess().getResearchGroupResearchGroupParserRuleCall_10_1_0());
                    					
                    pushFollow(FOLLOW_47);
                    lv_researchGroup_37_0=ruleResearchGroup();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getResearcherRule());
                    						}
                    						set(
                    							current,
                    							"researchGroup",
                    							lv_researchGroup_37_0,
                    							"org.montex.researchcv.xtext.TextualRCV.ResearchGroup");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:1273:3: (otherlv_38= 'topics' otherlv_39= '{' ( (lv_topics_40_0= ruleResearchTopic ) ) (otherlv_41= ',' ( (lv_topics_42_0= ruleResearchTopic ) ) )* otherlv_43= '}' )?
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==39) ) {
                alt31=1;
            }
            switch (alt31) {
                case 1 :
                    // InternalTextualRCV.g:1274:4: otherlv_38= 'topics' otherlv_39= '{' ( (lv_topics_40_0= ruleResearchTopic ) ) (otherlv_41= ',' ( (lv_topics_42_0= ruleResearchTopic ) ) )* otherlv_43= '}'
                    {
                    otherlv_38=(Token)match(input,39,FOLLOW_3); 

                    				newLeafNode(otherlv_38, grammarAccess.getResearcherAccess().getTopicsKeyword_11_0());
                    			
                    otherlv_39=(Token)match(input,16,FOLLOW_48); 

                    				newLeafNode(otherlv_39, grammarAccess.getResearcherAccess().getLeftCurlyBracketKeyword_11_1());
                    			
                    // InternalTextualRCV.g:1282:4: ( (lv_topics_40_0= ruleResearchTopic ) )
                    // InternalTextualRCV.g:1283:5: (lv_topics_40_0= ruleResearchTopic )
                    {
                    // InternalTextualRCV.g:1283:5: (lv_topics_40_0= ruleResearchTopic )
                    // InternalTextualRCV.g:1284:6: lv_topics_40_0= ruleResearchTopic
                    {

                    						newCompositeNode(grammarAccess.getResearcherAccess().getTopicsResearchTopicParserRuleCall_11_2_0());
                    					
                    pushFollow(FOLLOW_40);
                    lv_topics_40_0=ruleResearchTopic();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getResearcherRule());
                    						}
                    						add(
                    							current,
                    							"topics",
                    							lv_topics_40_0,
                    							"org.montex.researchcv.xtext.TextualRCV.ResearchTopic");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:1301:4: (otherlv_41= ',' ( (lv_topics_42_0= ruleResearchTopic ) ) )*
                    loop30:
                    do {
                        int alt30=2;
                        int LA30_0 = input.LA(1);

                        if ( (LA30_0==24) ) {
                            alt30=1;
                        }


                        switch (alt30) {
                    	case 1 :
                    	    // InternalTextualRCV.g:1302:5: otherlv_41= ',' ( (lv_topics_42_0= ruleResearchTopic ) )
                    	    {
                    	    otherlv_41=(Token)match(input,24,FOLLOW_48); 

                    	    					newLeafNode(otherlv_41, grammarAccess.getResearcherAccess().getCommaKeyword_11_3_0());
                    	    				
                    	    // InternalTextualRCV.g:1306:5: ( (lv_topics_42_0= ruleResearchTopic ) )
                    	    // InternalTextualRCV.g:1307:6: (lv_topics_42_0= ruleResearchTopic )
                    	    {
                    	    // InternalTextualRCV.g:1307:6: (lv_topics_42_0= ruleResearchTopic )
                    	    // InternalTextualRCV.g:1308:7: lv_topics_42_0= ruleResearchTopic
                    	    {

                    	    							newCompositeNode(grammarAccess.getResearcherAccess().getTopicsResearchTopicParserRuleCall_11_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_40);
                    	    lv_topics_42_0=ruleResearchTopic();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getResearcherRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"topics",
                    	    								lv_topics_42_0,
                    	    								"org.montex.researchcv.xtext.TextualRCV.ResearchTopic");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop30;
                        }
                    } while (true);

                    otherlv_43=(Token)match(input,18,FOLLOW_49); 

                    				newLeafNode(otherlv_43, grammarAccess.getResearcherAccess().getRightCurlyBracketKeyword_11_4());
                    			

                    }
                    break;

            }

            // InternalTextualRCV.g:1331:3: (otherlv_44= 'grants' otherlv_45= '{' ( (lv_grants_46_0= ruleGrant ) ) (otherlv_47= ',' ( (lv_grants_48_0= ruleGrant ) ) )* otherlv_49= '}' )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==40) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // InternalTextualRCV.g:1332:4: otherlv_44= 'grants' otherlv_45= '{' ( (lv_grants_46_0= ruleGrant ) ) (otherlv_47= ',' ( (lv_grants_48_0= ruleGrant ) ) )* otherlv_49= '}'
                    {
                    otherlv_44=(Token)match(input,40,FOLLOW_3); 

                    				newLeafNode(otherlv_44, grammarAccess.getResearcherAccess().getGrantsKeyword_12_0());
                    			
                    otherlv_45=(Token)match(input,16,FOLLOW_50); 

                    				newLeafNode(otherlv_45, grammarAccess.getResearcherAccess().getLeftCurlyBracketKeyword_12_1());
                    			
                    // InternalTextualRCV.g:1340:4: ( (lv_grants_46_0= ruleGrant ) )
                    // InternalTextualRCV.g:1341:5: (lv_grants_46_0= ruleGrant )
                    {
                    // InternalTextualRCV.g:1341:5: (lv_grants_46_0= ruleGrant )
                    // InternalTextualRCV.g:1342:6: lv_grants_46_0= ruleGrant
                    {

                    						newCompositeNode(grammarAccess.getResearcherAccess().getGrantsGrantParserRuleCall_12_2_0());
                    					
                    pushFollow(FOLLOW_40);
                    lv_grants_46_0=ruleGrant();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getResearcherRule());
                    						}
                    						add(
                    							current,
                    							"grants",
                    							lv_grants_46_0,
                    							"org.montex.researchcv.xtext.TextualRCV.Grant");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:1359:4: (otherlv_47= ',' ( (lv_grants_48_0= ruleGrant ) ) )*
                    loop32:
                    do {
                        int alt32=2;
                        int LA32_0 = input.LA(1);

                        if ( (LA32_0==24) ) {
                            alt32=1;
                        }


                        switch (alt32) {
                    	case 1 :
                    	    // InternalTextualRCV.g:1360:5: otherlv_47= ',' ( (lv_grants_48_0= ruleGrant ) )
                    	    {
                    	    otherlv_47=(Token)match(input,24,FOLLOW_50); 

                    	    					newLeafNode(otherlv_47, grammarAccess.getResearcherAccess().getCommaKeyword_12_3_0());
                    	    				
                    	    // InternalTextualRCV.g:1364:5: ( (lv_grants_48_0= ruleGrant ) )
                    	    // InternalTextualRCV.g:1365:6: (lv_grants_48_0= ruleGrant )
                    	    {
                    	    // InternalTextualRCV.g:1365:6: (lv_grants_48_0= ruleGrant )
                    	    // InternalTextualRCV.g:1366:7: lv_grants_48_0= ruleGrant
                    	    {

                    	    							newCompositeNode(grammarAccess.getResearcherAccess().getGrantsGrantParserRuleCall_12_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_40);
                    	    lv_grants_48_0=ruleGrant();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getResearcherRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"grants",
                    	    								lv_grants_48_0,
                    	    								"org.montex.researchcv.xtext.TextualRCV.Grant");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop32;
                        }
                    } while (true);

                    otherlv_49=(Token)match(input,18,FOLLOW_51); 

                    				newLeafNode(otherlv_49, grammarAccess.getResearcherAccess().getRightCurlyBracketKeyword_12_4());
                    			

                    }
                    break;

            }

            // InternalTextualRCV.g:1389:3: (otherlv_50= 'courses' otherlv_51= '{' ( (lv_courses_52_0= ruleCourse ) ) (otherlv_53= ',' ( (lv_courses_54_0= ruleCourse ) ) )* otherlv_55= '}' )?
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==41) ) {
                alt35=1;
            }
            switch (alt35) {
                case 1 :
                    // InternalTextualRCV.g:1390:4: otherlv_50= 'courses' otherlv_51= '{' ( (lv_courses_52_0= ruleCourse ) ) (otherlv_53= ',' ( (lv_courses_54_0= ruleCourse ) ) )* otherlv_55= '}'
                    {
                    otherlv_50=(Token)match(input,41,FOLLOW_3); 

                    				newLeafNode(otherlv_50, grammarAccess.getResearcherAccess().getCoursesKeyword_13_0());
                    			
                    otherlv_51=(Token)match(input,16,FOLLOW_52); 

                    				newLeafNode(otherlv_51, grammarAccess.getResearcherAccess().getLeftCurlyBracketKeyword_13_1());
                    			
                    // InternalTextualRCV.g:1398:4: ( (lv_courses_52_0= ruleCourse ) )
                    // InternalTextualRCV.g:1399:5: (lv_courses_52_0= ruleCourse )
                    {
                    // InternalTextualRCV.g:1399:5: (lv_courses_52_0= ruleCourse )
                    // InternalTextualRCV.g:1400:6: lv_courses_52_0= ruleCourse
                    {

                    						newCompositeNode(grammarAccess.getResearcherAccess().getCoursesCourseParserRuleCall_13_2_0());
                    					
                    pushFollow(FOLLOW_40);
                    lv_courses_52_0=ruleCourse();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getResearcherRule());
                    						}
                    						add(
                    							current,
                    							"courses",
                    							lv_courses_52_0,
                    							"org.montex.researchcv.xtext.TextualRCV.Course");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:1417:4: (otherlv_53= ',' ( (lv_courses_54_0= ruleCourse ) ) )*
                    loop34:
                    do {
                        int alt34=2;
                        int LA34_0 = input.LA(1);

                        if ( (LA34_0==24) ) {
                            alt34=1;
                        }


                        switch (alt34) {
                    	case 1 :
                    	    // InternalTextualRCV.g:1418:5: otherlv_53= ',' ( (lv_courses_54_0= ruleCourse ) )
                    	    {
                    	    otherlv_53=(Token)match(input,24,FOLLOW_52); 

                    	    					newLeafNode(otherlv_53, grammarAccess.getResearcherAccess().getCommaKeyword_13_3_0());
                    	    				
                    	    // InternalTextualRCV.g:1422:5: ( (lv_courses_54_0= ruleCourse ) )
                    	    // InternalTextualRCV.g:1423:6: (lv_courses_54_0= ruleCourse )
                    	    {
                    	    // InternalTextualRCV.g:1423:6: (lv_courses_54_0= ruleCourse )
                    	    // InternalTextualRCV.g:1424:7: lv_courses_54_0= ruleCourse
                    	    {

                    	    							newCompositeNode(grammarAccess.getResearcherAccess().getCoursesCourseParserRuleCall_13_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_40);
                    	    lv_courses_54_0=ruleCourse();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getResearcherRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"courses",
                    	    								lv_courses_54_0,
                    	    								"org.montex.researchcv.xtext.TextualRCV.Course");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop34;
                        }
                    } while (true);

                    otherlv_55=(Token)match(input,18,FOLLOW_53); 

                    				newLeafNode(otherlv_55, grammarAccess.getResearcherAccess().getRightCurlyBracketKeyword_13_4());
                    			

                    }
                    break;

            }

            // InternalTextualRCV.g:1447:3: (otherlv_56= 'taughtCourses' otherlv_57= '{' ( (lv_taughtCourses_58_0= ruleTaughtCourse ) ) (otherlv_59= ',' ( (lv_taughtCourses_60_0= ruleTaughtCourse ) ) )* otherlv_61= '}' )?
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==42) ) {
                alt37=1;
            }
            switch (alt37) {
                case 1 :
                    // InternalTextualRCV.g:1448:4: otherlv_56= 'taughtCourses' otherlv_57= '{' ( (lv_taughtCourses_58_0= ruleTaughtCourse ) ) (otherlv_59= ',' ( (lv_taughtCourses_60_0= ruleTaughtCourse ) ) )* otherlv_61= '}'
                    {
                    otherlv_56=(Token)match(input,42,FOLLOW_3); 

                    				newLeafNode(otherlv_56, grammarAccess.getResearcherAccess().getTaughtCoursesKeyword_14_0());
                    			
                    otherlv_57=(Token)match(input,16,FOLLOW_54); 

                    				newLeafNode(otherlv_57, grammarAccess.getResearcherAccess().getLeftCurlyBracketKeyword_14_1());
                    			
                    // InternalTextualRCV.g:1456:4: ( (lv_taughtCourses_58_0= ruleTaughtCourse ) )
                    // InternalTextualRCV.g:1457:5: (lv_taughtCourses_58_0= ruleTaughtCourse )
                    {
                    // InternalTextualRCV.g:1457:5: (lv_taughtCourses_58_0= ruleTaughtCourse )
                    // InternalTextualRCV.g:1458:6: lv_taughtCourses_58_0= ruleTaughtCourse
                    {

                    						newCompositeNode(grammarAccess.getResearcherAccess().getTaughtCoursesTaughtCourseParserRuleCall_14_2_0());
                    					
                    pushFollow(FOLLOW_40);
                    lv_taughtCourses_58_0=ruleTaughtCourse();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getResearcherRule());
                    						}
                    						add(
                    							current,
                    							"taughtCourses",
                    							lv_taughtCourses_58_0,
                    							"org.montex.researchcv.xtext.TextualRCV.TaughtCourse");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:1475:4: (otherlv_59= ',' ( (lv_taughtCourses_60_0= ruleTaughtCourse ) ) )*
                    loop36:
                    do {
                        int alt36=2;
                        int LA36_0 = input.LA(1);

                        if ( (LA36_0==24) ) {
                            alt36=1;
                        }


                        switch (alt36) {
                    	case 1 :
                    	    // InternalTextualRCV.g:1476:5: otherlv_59= ',' ( (lv_taughtCourses_60_0= ruleTaughtCourse ) )
                    	    {
                    	    otherlv_59=(Token)match(input,24,FOLLOW_54); 

                    	    					newLeafNode(otherlv_59, grammarAccess.getResearcherAccess().getCommaKeyword_14_3_0());
                    	    				
                    	    // InternalTextualRCV.g:1480:5: ( (lv_taughtCourses_60_0= ruleTaughtCourse ) )
                    	    // InternalTextualRCV.g:1481:6: (lv_taughtCourses_60_0= ruleTaughtCourse )
                    	    {
                    	    // InternalTextualRCV.g:1481:6: (lv_taughtCourses_60_0= ruleTaughtCourse )
                    	    // InternalTextualRCV.g:1482:7: lv_taughtCourses_60_0= ruleTaughtCourse
                    	    {

                    	    							newCompositeNode(grammarAccess.getResearcherAccess().getTaughtCoursesTaughtCourseParserRuleCall_14_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_40);
                    	    lv_taughtCourses_60_0=ruleTaughtCourse();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getResearcherRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"taughtCourses",
                    	    								lv_taughtCourses_60_0,
                    	    								"org.montex.researchcv.xtext.TextualRCV.TaughtCourse");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop36;
                        }
                    } while (true);

                    otherlv_61=(Token)match(input,18,FOLLOW_27); 

                    				newLeafNode(otherlv_61, grammarAccess.getResearcherAccess().getRightCurlyBracketKeyword_14_4());
                    			

                    }
                    break;

            }

            otherlv_62=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_62, grammarAccess.getResearcherAccess().getRightCurlyBracketKeyword_15());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleResearcher"


    // $ANTLR start "entryRuleContact"
    // InternalTextualRCV.g:1513:1: entryRuleContact returns [EObject current=null] : iv_ruleContact= ruleContact EOF ;
    public final EObject entryRuleContact() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleContact = null;


        try {
            // InternalTextualRCV.g:1513:48: (iv_ruleContact= ruleContact EOF )
            // InternalTextualRCV.g:1514:2: iv_ruleContact= ruleContact EOF
            {
             newCompositeNode(grammarAccess.getContactRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleContact=ruleContact();

            state._fsp--;

             current =iv_ruleContact; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleContact"


    // $ANTLR start "ruleContact"
    // InternalTextualRCV.g:1520:1: ruleContact returns [EObject current=null] : (this_Address_0= ruleAddress | this_Email_1= ruleEmail | this_Phone_2= rulePhone | this_Website_3= ruleWebsite ) ;
    public final EObject ruleContact() throws RecognitionException {
        EObject current = null;

        EObject this_Address_0 = null;

        EObject this_Email_1 = null;

        EObject this_Phone_2 = null;

        EObject this_Website_3 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:1526:2: ( (this_Address_0= ruleAddress | this_Email_1= ruleEmail | this_Phone_2= rulePhone | this_Website_3= ruleWebsite ) )
            // InternalTextualRCV.g:1527:2: (this_Address_0= ruleAddress | this_Email_1= ruleEmail | this_Phone_2= rulePhone | this_Website_3= ruleWebsite )
            {
            // InternalTextualRCV.g:1527:2: (this_Address_0= ruleAddress | this_Email_1= ruleEmail | this_Phone_2= rulePhone | this_Website_3= ruleWebsite )
            int alt38=4;
            switch ( input.LA(1) ) {
            case 135:
                {
                switch ( input.LA(2) ) {
                case 70:
                    {
                    alt38=4;
                    }
                    break;
                case 66:
                    {
                    alt38=1;
                    }
                    break;
                case 69:
                    {
                    alt38=3;
                    }
                    break;
                case 68:
                    {
                    alt38=2;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 38, 1, input);

                    throw nvae;
                }

                }
                break;
            case 136:
                {
                switch ( input.LA(2) ) {
                case 68:
                    {
                    alt38=2;
                    }
                    break;
                case 70:
                    {
                    alt38=4;
                    }
                    break;
                case 66:
                    {
                    alt38=1;
                    }
                    break;
                case 69:
                    {
                    alt38=3;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 38, 2, input);

                    throw nvae;
                }

                }
                break;
            case 66:
                {
                alt38=1;
                }
                break;
            case 68:
                {
                alt38=2;
                }
                break;
            case 69:
                {
                alt38=3;
                }
                break;
            case 70:
                {
                alt38=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 38, 0, input);

                throw nvae;
            }

            switch (alt38) {
                case 1 :
                    // InternalTextualRCV.g:1528:3: this_Address_0= ruleAddress
                    {

                    			newCompositeNode(grammarAccess.getContactAccess().getAddressParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Address_0=ruleAddress();

                    state._fsp--;


                    			current = this_Address_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalTextualRCV.g:1537:3: this_Email_1= ruleEmail
                    {

                    			newCompositeNode(grammarAccess.getContactAccess().getEmailParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_Email_1=ruleEmail();

                    state._fsp--;


                    			current = this_Email_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalTextualRCV.g:1546:3: this_Phone_2= rulePhone
                    {

                    			newCompositeNode(grammarAccess.getContactAccess().getPhoneParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_Phone_2=rulePhone();

                    state._fsp--;


                    			current = this_Phone_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalTextualRCV.g:1555:3: this_Website_3= ruleWebsite
                    {

                    			newCompositeNode(grammarAccess.getContactAccess().getWebsiteParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_Website_3=ruleWebsite();

                    state._fsp--;


                    			current = this_Website_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleContact"


    // $ANTLR start "entryRulePublication"
    // InternalTextualRCV.g:1567:1: entryRulePublication returns [EObject current=null] : iv_rulePublication= rulePublication EOF ;
    public final EObject entryRulePublication() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePublication = null;


        try {
            // InternalTextualRCV.g:1567:52: (iv_rulePublication= rulePublication EOF )
            // InternalTextualRCV.g:1568:2: iv_rulePublication= rulePublication EOF
            {
             newCompositeNode(grammarAccess.getPublicationRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePublication=rulePublication();

            state._fsp--;

             current =iv_rulePublication; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePublication"


    // $ANTLR start "rulePublication"
    // InternalTextualRCV.g:1574:1: rulePublication returns [EObject current=null] : (this_Journal_0= ruleJournal | this_Conference_1= ruleConference | this_Workshop_2= ruleWorkshop | this_Book_3= ruleBook | this_BookChapter_4= ruleBookChapter | this_Report_5= ruleReport | this_Miscellaneous_6= ruleMiscellaneous ) ;
    public final EObject rulePublication() throws RecognitionException {
        EObject current = null;

        EObject this_Journal_0 = null;

        EObject this_Conference_1 = null;

        EObject this_Workshop_2 = null;

        EObject this_Book_3 = null;

        EObject this_BookChapter_4 = null;

        EObject this_Report_5 = null;

        EObject this_Miscellaneous_6 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:1580:2: ( (this_Journal_0= ruleJournal | this_Conference_1= ruleConference | this_Workshop_2= ruleWorkshop | this_Book_3= ruleBook | this_BookChapter_4= ruleBookChapter | this_Report_5= ruleReport | this_Miscellaneous_6= ruleMiscellaneous ) )
            // InternalTextualRCV.g:1581:2: (this_Journal_0= ruleJournal | this_Conference_1= ruleConference | this_Workshop_2= ruleWorkshop | this_Book_3= ruleBook | this_BookChapter_4= ruleBookChapter | this_Report_5= ruleReport | this_Miscellaneous_6= ruleMiscellaneous )
            {
            // InternalTextualRCV.g:1581:2: (this_Journal_0= ruleJournal | this_Conference_1= ruleConference | this_Workshop_2= ruleWorkshop | this_Book_3= ruleBook | this_BookChapter_4= ruleBookChapter | this_Report_5= ruleReport | this_Miscellaneous_6= ruleMiscellaneous )
            int alt39=7;
            alt39 = dfa39.predict(input);
            switch (alt39) {
                case 1 :
                    // InternalTextualRCV.g:1582:3: this_Journal_0= ruleJournal
                    {

                    			newCompositeNode(grammarAccess.getPublicationAccess().getJournalParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Journal_0=ruleJournal();

                    state._fsp--;


                    			current = this_Journal_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalTextualRCV.g:1591:3: this_Conference_1= ruleConference
                    {

                    			newCompositeNode(grammarAccess.getPublicationAccess().getConferenceParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_Conference_1=ruleConference();

                    state._fsp--;


                    			current = this_Conference_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalTextualRCV.g:1600:3: this_Workshop_2= ruleWorkshop
                    {

                    			newCompositeNode(grammarAccess.getPublicationAccess().getWorkshopParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_Workshop_2=ruleWorkshop();

                    state._fsp--;


                    			current = this_Workshop_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalTextualRCV.g:1609:3: this_Book_3= ruleBook
                    {

                    			newCompositeNode(grammarAccess.getPublicationAccess().getBookParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_Book_3=ruleBook();

                    state._fsp--;


                    			current = this_Book_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 5 :
                    // InternalTextualRCV.g:1618:3: this_BookChapter_4= ruleBookChapter
                    {

                    			newCompositeNode(grammarAccess.getPublicationAccess().getBookChapterParserRuleCall_4());
                    		
                    pushFollow(FOLLOW_2);
                    this_BookChapter_4=ruleBookChapter();

                    state._fsp--;


                    			current = this_BookChapter_4;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 6 :
                    // InternalTextualRCV.g:1627:3: this_Report_5= ruleReport
                    {

                    			newCompositeNode(grammarAccess.getPublicationAccess().getReportParserRuleCall_5());
                    		
                    pushFollow(FOLLOW_2);
                    this_Report_5=ruleReport();

                    state._fsp--;


                    			current = this_Report_5;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 7 :
                    // InternalTextualRCV.g:1636:3: this_Miscellaneous_6= ruleMiscellaneous
                    {

                    			newCompositeNode(grammarAccess.getPublicationAccess().getMiscellaneousParserRuleCall_6());
                    		
                    pushFollow(FOLLOW_2);
                    this_Miscellaneous_6=ruleMiscellaneous();

                    state._fsp--;


                    			current = this_Miscellaneous_6;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePublication"


    // $ANTLR start "entryRuleGrant"
    // InternalTextualRCV.g:1648:1: entryRuleGrant returns [EObject current=null] : iv_ruleGrant= ruleGrant EOF ;
    public final EObject entryRuleGrant() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGrant = null;


        try {
            // InternalTextualRCV.g:1648:46: (iv_ruleGrant= ruleGrant EOF )
            // InternalTextualRCV.g:1649:2: iv_ruleGrant= ruleGrant EOF
            {
             newCompositeNode(grammarAccess.getGrantRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleGrant=ruleGrant();

            state._fsp--;

             current =iv_ruleGrant; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGrant"


    // $ANTLR start "ruleGrant"
    // InternalTextualRCV.g:1655:1: ruleGrant returns [EObject current=null] : this_Project_0= ruleProject ;
    public final EObject ruleGrant() throws RecognitionException {
        EObject current = null;

        EObject this_Project_0 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:1661:2: (this_Project_0= ruleProject )
            // InternalTextualRCV.g:1662:2: this_Project_0= ruleProject
            {

            		newCompositeNode(grammarAccess.getGrantAccess().getProjectParserRuleCall());
            	
            pushFollow(FOLLOW_2);
            this_Project_0=ruleProject();

            state._fsp--;


            		current = this_Project_0;
            		afterParserOrEnumRuleCall();
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGrant"


    // $ANTLR start "entryRulePerson"
    // InternalTextualRCV.g:1673:1: entryRulePerson returns [EObject current=null] : iv_rulePerson= rulePerson EOF ;
    public final EObject entryRulePerson() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePerson = null;


        try {
            // InternalTextualRCV.g:1673:47: (iv_rulePerson= rulePerson EOF )
            // InternalTextualRCV.g:1674:2: iv_rulePerson= rulePerson EOF
            {
             newCompositeNode(grammarAccess.getPersonRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePerson=rulePerson();

            state._fsp--;

             current =iv_rulePerson; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePerson"


    // $ANTLR start "rulePerson"
    // InternalTextualRCV.g:1680:1: rulePerson returns [EObject current=null] : (this_Person_Impl_0= rulePerson_Impl | this_Researcher_1= ruleResearcher ) ;
    public final EObject rulePerson() throws RecognitionException {
        EObject current = null;

        EObject this_Person_Impl_0 = null;

        EObject this_Researcher_1 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:1686:2: ( (this_Person_Impl_0= rulePerson_Impl | this_Researcher_1= ruleResearcher ) )
            // InternalTextualRCV.g:1687:2: (this_Person_Impl_0= rulePerson_Impl | this_Researcher_1= ruleResearcher )
            {
            // InternalTextualRCV.g:1687:2: (this_Person_Impl_0= rulePerson_Impl | this_Researcher_1= ruleResearcher )
            int alt40=2;
            int LA40_0 = input.LA(1);

            if ( ((LA40_0>=RULE_ID && LA40_0<=RULE_STRING)) ) {
                alt40=1;
            }
            else if ( (LA40_0==31) ) {
                alt40=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 40, 0, input);

                throw nvae;
            }
            switch (alt40) {
                case 1 :
                    // InternalTextualRCV.g:1688:3: this_Person_Impl_0= rulePerson_Impl
                    {

                    			newCompositeNode(grammarAccess.getPersonAccess().getPerson_ImplParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Person_Impl_0=rulePerson_Impl();

                    state._fsp--;


                    			current = this_Person_Impl_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalTextualRCV.g:1697:3: this_Researcher_1= ruleResearcher
                    {

                    			newCompositeNode(grammarAccess.getPersonAccess().getResearcherParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_Researcher_1=ruleResearcher();

                    state._fsp--;


                    			current = this_Researcher_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePerson"


    // $ANTLR start "entryRuleEString"
    // InternalTextualRCV.g:1709:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalTextualRCV.g:1709:47: (iv_ruleEString= ruleEString EOF )
            // InternalTextualRCV.g:1710:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalTextualRCV.g:1716:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;


        	enterRule();

        try {
            // InternalTextualRCV.g:1722:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // InternalTextualRCV.g:1723:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // InternalTextualRCV.g:1723:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt41=2;
            int LA41_0 = input.LA(1);

            if ( (LA41_0==RULE_STRING) ) {
                alt41=1;
            }
            else if ( (LA41_0==RULE_ID) ) {
                alt41=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 41, 0, input);

                throw nvae;
            }
            switch (alt41) {
                case 1 :
                    // InternalTextualRCV.g:1724:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_0);
                    		

                    			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalTextualRCV.g:1732:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_1);
                    		

                    			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleSocialProfile"
    // InternalTextualRCV.g:1743:1: entryRuleSocialProfile returns [EObject current=null] : iv_ruleSocialProfile= ruleSocialProfile EOF ;
    public final EObject entryRuleSocialProfile() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSocialProfile = null;


        try {
            // InternalTextualRCV.g:1743:54: (iv_ruleSocialProfile= ruleSocialProfile EOF )
            // InternalTextualRCV.g:1744:2: iv_ruleSocialProfile= ruleSocialProfile EOF
            {
             newCompositeNode(grammarAccess.getSocialProfileRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSocialProfile=ruleSocialProfile();

            state._fsp--;

             current =iv_ruleSocialProfile; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSocialProfile"


    // $ANTLR start "ruleSocialProfile"
    // InternalTextualRCV.g:1750:1: ruleSocialProfile returns [EObject current=null] : (otherlv_0= 'SocialProfile' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' otherlv_3= 'url' ( (lv_url_4_0= ruleEString ) ) otherlv_5= '}' ) ;
    public final EObject ruleSocialProfile() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        AntlrDatatypeRuleToken lv_url_4_0 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:1756:2: ( (otherlv_0= 'SocialProfile' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' otherlv_3= 'url' ( (lv_url_4_0= ruleEString ) ) otherlv_5= '}' ) )
            // InternalTextualRCV.g:1757:2: (otherlv_0= 'SocialProfile' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' otherlv_3= 'url' ( (lv_url_4_0= ruleEString ) ) otherlv_5= '}' )
            {
            // InternalTextualRCV.g:1757:2: (otherlv_0= 'SocialProfile' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' otherlv_3= 'url' ( (lv_url_4_0= ruleEString ) ) otherlv_5= '}' )
            // InternalTextualRCV.g:1758:3: otherlv_0= 'SocialProfile' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' otherlv_3= 'url' ( (lv_url_4_0= ruleEString ) ) otherlv_5= '}'
            {
            otherlv_0=(Token)match(input,43,FOLLOW_20); 

            			newLeafNode(otherlv_0, grammarAccess.getSocialProfileAccess().getSocialProfileKeyword_0());
            		
            // InternalTextualRCV.g:1762:3: ( (lv_name_1_0= ruleEString ) )
            // InternalTextualRCV.g:1763:4: (lv_name_1_0= ruleEString )
            {
            // InternalTextualRCV.g:1763:4: (lv_name_1_0= ruleEString )
            // InternalTextualRCV.g:1764:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getSocialProfileAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_3);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSocialProfileRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,16,FOLLOW_55); 

            			newLeafNode(otherlv_2, grammarAccess.getSocialProfileAccess().getLeftCurlyBracketKeyword_2());
            		
            otherlv_3=(Token)match(input,44,FOLLOW_20); 

            			newLeafNode(otherlv_3, grammarAccess.getSocialProfileAccess().getUrlKeyword_3());
            		
            // InternalTextualRCV.g:1789:3: ( (lv_url_4_0= ruleEString ) )
            // InternalTextualRCV.g:1790:4: (lv_url_4_0= ruleEString )
            {
            // InternalTextualRCV.g:1790:4: (lv_url_4_0= ruleEString )
            // InternalTextualRCV.g:1791:5: lv_url_4_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getSocialProfileAccess().getUrlEStringParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_27);
            lv_url_4_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSocialProfileRule());
            					}
            					set(
            						current,
            						"url",
            						lv_url_4_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getSocialProfileAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSocialProfile"


    // $ANTLR start "entryRuleResearchGroup"
    // InternalTextualRCV.g:1816:1: entryRuleResearchGroup returns [EObject current=null] : iv_ruleResearchGroup= ruleResearchGroup EOF ;
    public final EObject entryRuleResearchGroup() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleResearchGroup = null;


        try {
            // InternalTextualRCV.g:1816:54: (iv_ruleResearchGroup= ruleResearchGroup EOF )
            // InternalTextualRCV.g:1817:2: iv_ruleResearchGroup= ruleResearchGroup EOF
            {
             newCompositeNode(grammarAccess.getResearchGroupRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleResearchGroup=ruleResearchGroup();

            state._fsp--;

             current =iv_ruleResearchGroup; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleResearchGroup"


    // $ANTLR start "ruleResearchGroup"
    // InternalTextualRCV.g:1823:1: ruleResearchGroup returns [EObject current=null] : (otherlv_0= 'ResearchGroup' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' otherlv_3= 'acronym' ( (lv_acronym_4_0= ruleEString ) ) (otherlv_5= 'URL' ( (lv_URL_6_0= ruleEString ) ) )? otherlv_7= '}' ) ;
    public final EObject ruleResearchGroup() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        AntlrDatatypeRuleToken lv_acronym_4_0 = null;

        AntlrDatatypeRuleToken lv_URL_6_0 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:1829:2: ( (otherlv_0= 'ResearchGroup' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' otherlv_3= 'acronym' ( (lv_acronym_4_0= ruleEString ) ) (otherlv_5= 'URL' ( (lv_URL_6_0= ruleEString ) ) )? otherlv_7= '}' ) )
            // InternalTextualRCV.g:1830:2: (otherlv_0= 'ResearchGroup' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' otherlv_3= 'acronym' ( (lv_acronym_4_0= ruleEString ) ) (otherlv_5= 'URL' ( (lv_URL_6_0= ruleEString ) ) )? otherlv_7= '}' )
            {
            // InternalTextualRCV.g:1830:2: (otherlv_0= 'ResearchGroup' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' otherlv_3= 'acronym' ( (lv_acronym_4_0= ruleEString ) ) (otherlv_5= 'URL' ( (lv_URL_6_0= ruleEString ) ) )? otherlv_7= '}' )
            // InternalTextualRCV.g:1831:3: otherlv_0= 'ResearchGroup' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' otherlv_3= 'acronym' ( (lv_acronym_4_0= ruleEString ) ) (otherlv_5= 'URL' ( (lv_URL_6_0= ruleEString ) ) )? otherlv_7= '}'
            {
            otherlv_0=(Token)match(input,45,FOLLOW_20); 

            			newLeafNode(otherlv_0, grammarAccess.getResearchGroupAccess().getResearchGroupKeyword_0());
            		
            // InternalTextualRCV.g:1835:3: ( (lv_name_1_0= ruleEString ) )
            // InternalTextualRCV.g:1836:4: (lv_name_1_0= ruleEString )
            {
            // InternalTextualRCV.g:1836:4: (lv_name_1_0= ruleEString )
            // InternalTextualRCV.g:1837:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getResearchGroupAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_3);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getResearchGroupRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,16,FOLLOW_56); 

            			newLeafNode(otherlv_2, grammarAccess.getResearchGroupAccess().getLeftCurlyBracketKeyword_2());
            		
            otherlv_3=(Token)match(input,46,FOLLOW_20); 

            			newLeafNode(otherlv_3, grammarAccess.getResearchGroupAccess().getAcronymKeyword_3());
            		
            // InternalTextualRCV.g:1862:3: ( (lv_acronym_4_0= ruleEString ) )
            // InternalTextualRCV.g:1863:4: (lv_acronym_4_0= ruleEString )
            {
            // InternalTextualRCV.g:1863:4: (lv_acronym_4_0= ruleEString )
            // InternalTextualRCV.g:1864:5: lv_acronym_4_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getResearchGroupAccess().getAcronymEStringParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_57);
            lv_acronym_4_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getResearchGroupRule());
            					}
            					set(
            						current,
            						"acronym",
            						lv_acronym_4_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:1881:3: (otherlv_5= 'URL' ( (lv_URL_6_0= ruleEString ) ) )?
            int alt42=2;
            int LA42_0 = input.LA(1);

            if ( (LA42_0==47) ) {
                alt42=1;
            }
            switch (alt42) {
                case 1 :
                    // InternalTextualRCV.g:1882:4: otherlv_5= 'URL' ( (lv_URL_6_0= ruleEString ) )
                    {
                    otherlv_5=(Token)match(input,47,FOLLOW_20); 

                    				newLeafNode(otherlv_5, grammarAccess.getResearchGroupAccess().getURLKeyword_5_0());
                    			
                    // InternalTextualRCV.g:1886:4: ( (lv_URL_6_0= ruleEString ) )
                    // InternalTextualRCV.g:1887:5: (lv_URL_6_0= ruleEString )
                    {
                    // InternalTextualRCV.g:1887:5: (lv_URL_6_0= ruleEString )
                    // InternalTextualRCV.g:1888:6: lv_URL_6_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getResearchGroupAccess().getURLEStringParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_27);
                    lv_URL_6_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getResearchGroupRule());
                    						}
                    						set(
                    							current,
                    							"URL",
                    							lv_URL_6_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_7=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_7, grammarAccess.getResearchGroupAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleResearchGroup"


    // $ANTLR start "entryRuleResearchTopic"
    // InternalTextualRCV.g:1914:1: entryRuleResearchTopic returns [EObject current=null] : iv_ruleResearchTopic= ruleResearchTopic EOF ;
    public final EObject entryRuleResearchTopic() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleResearchTopic = null;


        try {
            // InternalTextualRCV.g:1914:54: (iv_ruleResearchTopic= ruleResearchTopic EOF )
            // InternalTextualRCV.g:1915:2: iv_ruleResearchTopic= ruleResearchTopic EOF
            {
             newCompositeNode(grammarAccess.getResearchTopicRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleResearchTopic=ruleResearchTopic();

            state._fsp--;

             current =iv_ruleResearchTopic; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleResearchTopic"


    // $ANTLR start "ruleResearchTopic"
    // InternalTextualRCV.g:1921:1: ruleResearchTopic returns [EObject current=null] : (otherlv_0= 'ResearchTopic' otherlv_1= '{' otherlv_2= 'title' ( (lv_title_3_0= ruleEString ) ) (otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) ) )? (otherlv_6= 'relatedPapers' otherlv_7= '(' ( ( ruleEString ) ) (otherlv_9= ',' ( ( ruleEString ) ) )* otherlv_11= ')' )? otherlv_12= '}' ) ;
    public final EObject ruleResearchTopic() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        AntlrDatatypeRuleToken lv_title_3_0 = null;

        AntlrDatatypeRuleToken lv_description_5_0 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:1927:2: ( (otherlv_0= 'ResearchTopic' otherlv_1= '{' otherlv_2= 'title' ( (lv_title_3_0= ruleEString ) ) (otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) ) )? (otherlv_6= 'relatedPapers' otherlv_7= '(' ( ( ruleEString ) ) (otherlv_9= ',' ( ( ruleEString ) ) )* otherlv_11= ')' )? otherlv_12= '}' ) )
            // InternalTextualRCV.g:1928:2: (otherlv_0= 'ResearchTopic' otherlv_1= '{' otherlv_2= 'title' ( (lv_title_3_0= ruleEString ) ) (otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) ) )? (otherlv_6= 'relatedPapers' otherlv_7= '(' ( ( ruleEString ) ) (otherlv_9= ',' ( ( ruleEString ) ) )* otherlv_11= ')' )? otherlv_12= '}' )
            {
            // InternalTextualRCV.g:1928:2: (otherlv_0= 'ResearchTopic' otherlv_1= '{' otherlv_2= 'title' ( (lv_title_3_0= ruleEString ) ) (otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) ) )? (otherlv_6= 'relatedPapers' otherlv_7= '(' ( ( ruleEString ) ) (otherlv_9= ',' ( ( ruleEString ) ) )* otherlv_11= ')' )? otherlv_12= '}' )
            // InternalTextualRCV.g:1929:3: otherlv_0= 'ResearchTopic' otherlv_1= '{' otherlv_2= 'title' ( (lv_title_3_0= ruleEString ) ) (otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) ) )? (otherlv_6= 'relatedPapers' otherlv_7= '(' ( ( ruleEString ) ) (otherlv_9= ',' ( ( ruleEString ) ) )* otherlv_11= ')' )? otherlv_12= '}'
            {
            otherlv_0=(Token)match(input,48,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getResearchTopicAccess().getResearchTopicKeyword_0());
            		
            otherlv_1=(Token)match(input,16,FOLLOW_58); 

            			newLeafNode(otherlv_1, grammarAccess.getResearchTopicAccess().getLeftCurlyBracketKeyword_1());
            		
            otherlv_2=(Token)match(input,49,FOLLOW_20); 

            			newLeafNode(otherlv_2, grammarAccess.getResearchTopicAccess().getTitleKeyword_2());
            		
            // InternalTextualRCV.g:1941:3: ( (lv_title_3_0= ruleEString ) )
            // InternalTextualRCV.g:1942:4: (lv_title_3_0= ruleEString )
            {
            // InternalTextualRCV.g:1942:4: (lv_title_3_0= ruleEString )
            // InternalTextualRCV.g:1943:5: lv_title_3_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getResearchTopicAccess().getTitleEStringParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_59);
            lv_title_3_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getResearchTopicRule());
            					}
            					set(
            						current,
            						"title",
            						lv_title_3_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:1960:3: (otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) ) )?
            int alt43=2;
            int LA43_0 = input.LA(1);

            if ( (LA43_0==50) ) {
                alt43=1;
            }
            switch (alt43) {
                case 1 :
                    // InternalTextualRCV.g:1961:4: otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) )
                    {
                    otherlv_4=(Token)match(input,50,FOLLOW_20); 

                    				newLeafNode(otherlv_4, grammarAccess.getResearchTopicAccess().getDescriptionKeyword_4_0());
                    			
                    // InternalTextualRCV.g:1965:4: ( (lv_description_5_0= ruleEString ) )
                    // InternalTextualRCV.g:1966:5: (lv_description_5_0= ruleEString )
                    {
                    // InternalTextualRCV.g:1966:5: (lv_description_5_0= ruleEString )
                    // InternalTextualRCV.g:1967:6: lv_description_5_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getResearchTopicAccess().getDescriptionEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_60);
                    lv_description_5_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getResearchTopicRule());
                    						}
                    						set(
                    							current,
                    							"description",
                    							lv_description_5_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:1985:3: (otherlv_6= 'relatedPapers' otherlv_7= '(' ( ( ruleEString ) ) (otherlv_9= ',' ( ( ruleEString ) ) )* otherlv_11= ')' )?
            int alt45=2;
            int LA45_0 = input.LA(1);

            if ( (LA45_0==51) ) {
                alt45=1;
            }
            switch (alt45) {
                case 1 :
                    // InternalTextualRCV.g:1986:4: otherlv_6= 'relatedPapers' otherlv_7= '(' ( ( ruleEString ) ) (otherlv_9= ',' ( ( ruleEString ) ) )* otherlv_11= ')'
                    {
                    otherlv_6=(Token)match(input,51,FOLLOW_61); 

                    				newLeafNode(otherlv_6, grammarAccess.getResearchTopicAccess().getRelatedPapersKeyword_5_0());
                    			
                    otherlv_7=(Token)match(input,52,FOLLOW_20); 

                    				newLeafNode(otherlv_7, grammarAccess.getResearchTopicAccess().getLeftParenthesisKeyword_5_1());
                    			
                    // InternalTextualRCV.g:1994:4: ( ( ruleEString ) )
                    // InternalTextualRCV.g:1995:5: ( ruleEString )
                    {
                    // InternalTextualRCV.g:1995:5: ( ruleEString )
                    // InternalTextualRCV.g:1996:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getResearchTopicRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getResearchTopicAccess().getRelatedPapersPublicationCrossReference_5_2_0());
                    					
                    pushFollow(FOLLOW_62);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:2010:4: (otherlv_9= ',' ( ( ruleEString ) ) )*
                    loop44:
                    do {
                        int alt44=2;
                        int LA44_0 = input.LA(1);

                        if ( (LA44_0==24) ) {
                            alt44=1;
                        }


                        switch (alt44) {
                    	case 1 :
                    	    // InternalTextualRCV.g:2011:5: otherlv_9= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_9=(Token)match(input,24,FOLLOW_20); 

                    	    					newLeafNode(otherlv_9, grammarAccess.getResearchTopicAccess().getCommaKeyword_5_3_0());
                    	    				
                    	    // InternalTextualRCV.g:2015:5: ( ( ruleEString ) )
                    	    // InternalTextualRCV.g:2016:6: ( ruleEString )
                    	    {
                    	    // InternalTextualRCV.g:2016:6: ( ruleEString )
                    	    // InternalTextualRCV.g:2017:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getResearchTopicRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getResearchTopicAccess().getRelatedPapersPublicationCrossReference_5_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_62);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop44;
                        }
                    } while (true);

                    otherlv_11=(Token)match(input,53,FOLLOW_27); 

                    				newLeafNode(otherlv_11, grammarAccess.getResearchTopicAccess().getRightParenthesisKeyword_5_4());
                    			

                    }
                    break;

            }

            otherlv_12=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_12, grammarAccess.getResearchTopicAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleResearchTopic"


    // $ANTLR start "entryRuleCourse"
    // InternalTextualRCV.g:2045:1: entryRuleCourse returns [EObject current=null] : iv_ruleCourse= ruleCourse EOF ;
    public final EObject entryRuleCourse() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCourse = null;


        try {
            // InternalTextualRCV.g:2045:47: (iv_ruleCourse= ruleCourse EOF )
            // InternalTextualRCV.g:2046:2: iv_ruleCourse= ruleCourse EOF
            {
             newCompositeNode(grammarAccess.getCourseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCourse=ruleCourse();

            state._fsp--;

             current =iv_ruleCourse; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCourse"


    // $ANTLR start "ruleCourse"
    // InternalTextualRCV.g:2052:1: ruleCourse returns [EObject current=null] : (otherlv_0= 'Course' ( (lv_code_1_0= ruleEString ) ) otherlv_2= '{' otherlv_3= 'name' ( (lv_name_4_0= ruleEString ) ) (otherlv_5= 'level' ( (lv_level_6_0= ruleCourseKind ) ) )? otherlv_7= 'hours' ( (lv_hours_8_0= ruleEInt ) ) otherlv_9= 'credits' ( (lv_credits_10_0= ruleECTS ) ) otherlv_11= '}' ) ;
    public final EObject ruleCourse() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        AntlrDatatypeRuleToken lv_code_1_0 = null;

        AntlrDatatypeRuleToken lv_name_4_0 = null;

        Enumerator lv_level_6_0 = null;

        AntlrDatatypeRuleToken lv_hours_8_0 = null;

        AntlrDatatypeRuleToken lv_credits_10_0 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:2058:2: ( (otherlv_0= 'Course' ( (lv_code_1_0= ruleEString ) ) otherlv_2= '{' otherlv_3= 'name' ( (lv_name_4_0= ruleEString ) ) (otherlv_5= 'level' ( (lv_level_6_0= ruleCourseKind ) ) )? otherlv_7= 'hours' ( (lv_hours_8_0= ruleEInt ) ) otherlv_9= 'credits' ( (lv_credits_10_0= ruleECTS ) ) otherlv_11= '}' ) )
            // InternalTextualRCV.g:2059:2: (otherlv_0= 'Course' ( (lv_code_1_0= ruleEString ) ) otherlv_2= '{' otherlv_3= 'name' ( (lv_name_4_0= ruleEString ) ) (otherlv_5= 'level' ( (lv_level_6_0= ruleCourseKind ) ) )? otherlv_7= 'hours' ( (lv_hours_8_0= ruleEInt ) ) otherlv_9= 'credits' ( (lv_credits_10_0= ruleECTS ) ) otherlv_11= '}' )
            {
            // InternalTextualRCV.g:2059:2: (otherlv_0= 'Course' ( (lv_code_1_0= ruleEString ) ) otherlv_2= '{' otherlv_3= 'name' ( (lv_name_4_0= ruleEString ) ) (otherlv_5= 'level' ( (lv_level_6_0= ruleCourseKind ) ) )? otherlv_7= 'hours' ( (lv_hours_8_0= ruleEInt ) ) otherlv_9= 'credits' ( (lv_credits_10_0= ruleECTS ) ) otherlv_11= '}' )
            // InternalTextualRCV.g:2060:3: otherlv_0= 'Course' ( (lv_code_1_0= ruleEString ) ) otherlv_2= '{' otherlv_3= 'name' ( (lv_name_4_0= ruleEString ) ) (otherlv_5= 'level' ( (lv_level_6_0= ruleCourseKind ) ) )? otherlv_7= 'hours' ( (lv_hours_8_0= ruleEInt ) ) otherlv_9= 'credits' ( (lv_credits_10_0= ruleECTS ) ) otherlv_11= '}'
            {
            otherlv_0=(Token)match(input,54,FOLLOW_20); 

            			newLeafNode(otherlv_0, grammarAccess.getCourseAccess().getCourseKeyword_0());
            		
            // InternalTextualRCV.g:2064:3: ( (lv_code_1_0= ruleEString ) )
            // InternalTextualRCV.g:2065:4: (lv_code_1_0= ruleEString )
            {
            // InternalTextualRCV.g:2065:4: (lv_code_1_0= ruleEString )
            // InternalTextualRCV.g:2066:5: lv_code_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getCourseAccess().getCodeEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_3);
            lv_code_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCourseRule());
            					}
            					set(
            						current,
            						"code",
            						lv_code_1_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,16,FOLLOW_63); 

            			newLeafNode(otherlv_2, grammarAccess.getCourseAccess().getLeftCurlyBracketKeyword_2());
            		
            otherlv_3=(Token)match(input,55,FOLLOW_20); 

            			newLeafNode(otherlv_3, grammarAccess.getCourseAccess().getNameKeyword_3());
            		
            // InternalTextualRCV.g:2091:3: ( (lv_name_4_0= ruleEString ) )
            // InternalTextualRCV.g:2092:4: (lv_name_4_0= ruleEString )
            {
            // InternalTextualRCV.g:2092:4: (lv_name_4_0= ruleEString )
            // InternalTextualRCV.g:2093:5: lv_name_4_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getCourseAccess().getNameEStringParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_64);
            lv_name_4_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCourseRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_4_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:2110:3: (otherlv_5= 'level' ( (lv_level_6_0= ruleCourseKind ) ) )?
            int alt46=2;
            int LA46_0 = input.LA(1);

            if ( (LA46_0==56) ) {
                alt46=1;
            }
            switch (alt46) {
                case 1 :
                    // InternalTextualRCV.g:2111:4: otherlv_5= 'level' ( (lv_level_6_0= ruleCourseKind ) )
                    {
                    otherlv_5=(Token)match(input,56,FOLLOW_65); 

                    				newLeafNode(otherlv_5, grammarAccess.getCourseAccess().getLevelKeyword_5_0());
                    			
                    // InternalTextualRCV.g:2115:4: ( (lv_level_6_0= ruleCourseKind ) )
                    // InternalTextualRCV.g:2116:5: (lv_level_6_0= ruleCourseKind )
                    {
                    // InternalTextualRCV.g:2116:5: (lv_level_6_0= ruleCourseKind )
                    // InternalTextualRCV.g:2117:6: lv_level_6_0= ruleCourseKind
                    {

                    						newCompositeNode(grammarAccess.getCourseAccess().getLevelCourseKindEnumRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_66);
                    lv_level_6_0=ruleCourseKind();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCourseRule());
                    						}
                    						set(
                    							current,
                    							"level",
                    							lv_level_6_0,
                    							"org.montex.researchcv.xtext.TextualRCV.CourseKind");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_7=(Token)match(input,57,FOLLOW_9); 

            			newLeafNode(otherlv_7, grammarAccess.getCourseAccess().getHoursKeyword_6());
            		
            // InternalTextualRCV.g:2139:3: ( (lv_hours_8_0= ruleEInt ) )
            // InternalTextualRCV.g:2140:4: (lv_hours_8_0= ruleEInt )
            {
            // InternalTextualRCV.g:2140:4: (lv_hours_8_0= ruleEInt )
            // InternalTextualRCV.g:2141:5: lv_hours_8_0= ruleEInt
            {

            					newCompositeNode(grammarAccess.getCourseAccess().getHoursEIntParserRuleCall_7_0());
            				
            pushFollow(FOLLOW_67);
            lv_hours_8_0=ruleEInt();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCourseRule());
            					}
            					set(
            						current,
            						"hours",
            						lv_hours_8_0,
            						"org.montex.researchcv.xtext.TextualRCV.EInt");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_9=(Token)match(input,58,FOLLOW_68); 

            			newLeafNode(otherlv_9, grammarAccess.getCourseAccess().getCreditsKeyword_8());
            		
            // InternalTextualRCV.g:2162:3: ( (lv_credits_10_0= ruleECTS ) )
            // InternalTextualRCV.g:2163:4: (lv_credits_10_0= ruleECTS )
            {
            // InternalTextualRCV.g:2163:4: (lv_credits_10_0= ruleECTS )
            // InternalTextualRCV.g:2164:5: lv_credits_10_0= ruleECTS
            {

            					newCompositeNode(grammarAccess.getCourseAccess().getCreditsECTSParserRuleCall_9_0());
            				
            pushFollow(FOLLOW_27);
            lv_credits_10_0=ruleECTS();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCourseRule());
            					}
            					set(
            						current,
            						"credits",
            						lv_credits_10_0,
            						"org.montex.researchcv.xtext.TextualRCV.ECTS");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_11=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_11, grammarAccess.getCourseAccess().getRightCurlyBracketKeyword_10());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCourse"


    // $ANTLR start "entryRuleTaughtCourse"
    // InternalTextualRCV.g:2189:1: entryRuleTaughtCourse returns [EObject current=null] : iv_ruleTaughtCourse= ruleTaughtCourse EOF ;
    public final EObject entryRuleTaughtCourse() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTaughtCourse = null;


        try {
            // InternalTextualRCV.g:2189:53: (iv_ruleTaughtCourse= ruleTaughtCourse EOF )
            // InternalTextualRCV.g:2190:2: iv_ruleTaughtCourse= ruleTaughtCourse EOF
            {
             newCompositeNode(grammarAccess.getTaughtCourseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTaughtCourse=ruleTaughtCourse();

            state._fsp--;

             current =iv_ruleTaughtCourse; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTaughtCourse"


    // $ANTLR start "ruleTaughtCourse"
    // InternalTextualRCV.g:2196:1: ruleTaughtCourse returns [EObject current=null] : ( ( (lv_canceled_0_0= 'canceled' ) )? otherlv_1= 'TaughtCourse' otherlv_2= '{' ( (lv_year_3_0= ruleEInt ) ) otherlv_4= '/' ( (lv_period_5_0= ruleEInt ) ) (otherlv_6= 'webinfo' ( (lv_webinfo_7_0= ruleEString ) ) )? (otherlv_8= 'website' ( (lv_website_9_0= ruleEString ) ) )? otherlv_10= 'language' ( (lv_language_11_0= ruleLanguageCode ) ) otherlv_12= 'responsibility' ( (lv_responsibility_13_0= ruleEDouble ) ) otherlv_14= 'offering' otherlv_15= '(' ( (lv_offering_16_0= ruleCourseOffering ) ) (otherlv_17= ',' ( (lv_offering_18_0= ruleCourseOffering ) ) )* otherlv_19= ')' otherlv_20= '}' ) ;
    public final EObject ruleTaughtCourse() throws RecognitionException {
        EObject current = null;

        Token lv_canceled_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_20=null;
        AntlrDatatypeRuleToken lv_year_3_0 = null;

        AntlrDatatypeRuleToken lv_period_5_0 = null;

        AntlrDatatypeRuleToken lv_webinfo_7_0 = null;

        AntlrDatatypeRuleToken lv_website_9_0 = null;

        AntlrDatatypeRuleToken lv_language_11_0 = null;

        AntlrDatatypeRuleToken lv_responsibility_13_0 = null;

        EObject lv_offering_16_0 = null;

        EObject lv_offering_18_0 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:2202:2: ( ( ( (lv_canceled_0_0= 'canceled' ) )? otherlv_1= 'TaughtCourse' otherlv_2= '{' ( (lv_year_3_0= ruleEInt ) ) otherlv_4= '/' ( (lv_period_5_0= ruleEInt ) ) (otherlv_6= 'webinfo' ( (lv_webinfo_7_0= ruleEString ) ) )? (otherlv_8= 'website' ( (lv_website_9_0= ruleEString ) ) )? otherlv_10= 'language' ( (lv_language_11_0= ruleLanguageCode ) ) otherlv_12= 'responsibility' ( (lv_responsibility_13_0= ruleEDouble ) ) otherlv_14= 'offering' otherlv_15= '(' ( (lv_offering_16_0= ruleCourseOffering ) ) (otherlv_17= ',' ( (lv_offering_18_0= ruleCourseOffering ) ) )* otherlv_19= ')' otherlv_20= '}' ) )
            // InternalTextualRCV.g:2203:2: ( ( (lv_canceled_0_0= 'canceled' ) )? otherlv_1= 'TaughtCourse' otherlv_2= '{' ( (lv_year_3_0= ruleEInt ) ) otherlv_4= '/' ( (lv_period_5_0= ruleEInt ) ) (otherlv_6= 'webinfo' ( (lv_webinfo_7_0= ruleEString ) ) )? (otherlv_8= 'website' ( (lv_website_9_0= ruleEString ) ) )? otherlv_10= 'language' ( (lv_language_11_0= ruleLanguageCode ) ) otherlv_12= 'responsibility' ( (lv_responsibility_13_0= ruleEDouble ) ) otherlv_14= 'offering' otherlv_15= '(' ( (lv_offering_16_0= ruleCourseOffering ) ) (otherlv_17= ',' ( (lv_offering_18_0= ruleCourseOffering ) ) )* otherlv_19= ')' otherlv_20= '}' )
            {
            // InternalTextualRCV.g:2203:2: ( ( (lv_canceled_0_0= 'canceled' ) )? otherlv_1= 'TaughtCourse' otherlv_2= '{' ( (lv_year_3_0= ruleEInt ) ) otherlv_4= '/' ( (lv_period_5_0= ruleEInt ) ) (otherlv_6= 'webinfo' ( (lv_webinfo_7_0= ruleEString ) ) )? (otherlv_8= 'website' ( (lv_website_9_0= ruleEString ) ) )? otherlv_10= 'language' ( (lv_language_11_0= ruleLanguageCode ) ) otherlv_12= 'responsibility' ( (lv_responsibility_13_0= ruleEDouble ) ) otherlv_14= 'offering' otherlv_15= '(' ( (lv_offering_16_0= ruleCourseOffering ) ) (otherlv_17= ',' ( (lv_offering_18_0= ruleCourseOffering ) ) )* otherlv_19= ')' otherlv_20= '}' )
            // InternalTextualRCV.g:2204:3: ( (lv_canceled_0_0= 'canceled' ) )? otherlv_1= 'TaughtCourse' otherlv_2= '{' ( (lv_year_3_0= ruleEInt ) ) otherlv_4= '/' ( (lv_period_5_0= ruleEInt ) ) (otherlv_6= 'webinfo' ( (lv_webinfo_7_0= ruleEString ) ) )? (otherlv_8= 'website' ( (lv_website_9_0= ruleEString ) ) )? otherlv_10= 'language' ( (lv_language_11_0= ruleLanguageCode ) ) otherlv_12= 'responsibility' ( (lv_responsibility_13_0= ruleEDouble ) ) otherlv_14= 'offering' otherlv_15= '(' ( (lv_offering_16_0= ruleCourseOffering ) ) (otherlv_17= ',' ( (lv_offering_18_0= ruleCourseOffering ) ) )* otherlv_19= ')' otherlv_20= '}'
            {
            // InternalTextualRCV.g:2204:3: ( (lv_canceled_0_0= 'canceled' ) )?
            int alt47=2;
            int LA47_0 = input.LA(1);

            if ( (LA47_0==59) ) {
                alt47=1;
            }
            switch (alt47) {
                case 1 :
                    // InternalTextualRCV.g:2205:4: (lv_canceled_0_0= 'canceled' )
                    {
                    // InternalTextualRCV.g:2205:4: (lv_canceled_0_0= 'canceled' )
                    // InternalTextualRCV.g:2206:5: lv_canceled_0_0= 'canceled'
                    {
                    lv_canceled_0_0=(Token)match(input,59,FOLLOW_69); 

                    					newLeafNode(lv_canceled_0_0, grammarAccess.getTaughtCourseAccess().getCanceledCanceledKeyword_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getTaughtCourseRule());
                    					}
                    					setWithLastConsumed(current, "canceled", lv_canceled_0_0 != null, "canceled");
                    				

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,60,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getTaughtCourseAccess().getTaughtCourseKeyword_1());
            		
            otherlv_2=(Token)match(input,16,FOLLOW_9); 

            			newLeafNode(otherlv_2, grammarAccess.getTaughtCourseAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalTextualRCV.g:2226:3: ( (lv_year_3_0= ruleEInt ) )
            // InternalTextualRCV.g:2227:4: (lv_year_3_0= ruleEInt )
            {
            // InternalTextualRCV.g:2227:4: (lv_year_3_0= ruleEInt )
            // InternalTextualRCV.g:2228:5: lv_year_3_0= ruleEInt
            {

            					newCompositeNode(grammarAccess.getTaughtCourseAccess().getYearEIntParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_31);
            lv_year_3_0=ruleEInt();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTaughtCourseRule());
            					}
            					set(
            						current,
            						"year",
            						lv_year_3_0,
            						"org.montex.researchcv.xtext.TextualRCV.EInt");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,30,FOLLOW_9); 

            			newLeafNode(otherlv_4, grammarAccess.getTaughtCourseAccess().getSolidusKeyword_4());
            		
            // InternalTextualRCV.g:2249:3: ( (lv_period_5_0= ruleEInt ) )
            // InternalTextualRCV.g:2250:4: (lv_period_5_0= ruleEInt )
            {
            // InternalTextualRCV.g:2250:4: (lv_period_5_0= ruleEInt )
            // InternalTextualRCV.g:2251:5: lv_period_5_0= ruleEInt
            {

            					newCompositeNode(grammarAccess.getTaughtCourseAccess().getPeriodEIntParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_70);
            lv_period_5_0=ruleEInt();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTaughtCourseRule());
            					}
            					set(
            						current,
            						"period",
            						lv_period_5_0,
            						"org.montex.researchcv.xtext.TextualRCV.EInt");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:2268:3: (otherlv_6= 'webinfo' ( (lv_webinfo_7_0= ruleEString ) ) )?
            int alt48=2;
            int LA48_0 = input.LA(1);

            if ( (LA48_0==61) ) {
                alt48=1;
            }
            switch (alt48) {
                case 1 :
                    // InternalTextualRCV.g:2269:4: otherlv_6= 'webinfo' ( (lv_webinfo_7_0= ruleEString ) )
                    {
                    otherlv_6=(Token)match(input,61,FOLLOW_20); 

                    				newLeafNode(otherlv_6, grammarAccess.getTaughtCourseAccess().getWebinfoKeyword_6_0());
                    			
                    // InternalTextualRCV.g:2273:4: ( (lv_webinfo_7_0= ruleEString ) )
                    // InternalTextualRCV.g:2274:5: (lv_webinfo_7_0= ruleEString )
                    {
                    // InternalTextualRCV.g:2274:5: (lv_webinfo_7_0= ruleEString )
                    // InternalTextualRCV.g:2275:6: lv_webinfo_7_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getTaughtCourseAccess().getWebinfoEStringParserRuleCall_6_1_0());
                    					
                    pushFollow(FOLLOW_71);
                    lv_webinfo_7_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getTaughtCourseRule());
                    						}
                    						set(
                    							current,
                    							"webinfo",
                    							lv_webinfo_7_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:2293:3: (otherlv_8= 'website' ( (lv_website_9_0= ruleEString ) ) )?
            int alt49=2;
            int LA49_0 = input.LA(1);

            if ( (LA49_0==62) ) {
                alt49=1;
            }
            switch (alt49) {
                case 1 :
                    // InternalTextualRCV.g:2294:4: otherlv_8= 'website' ( (lv_website_9_0= ruleEString ) )
                    {
                    otherlv_8=(Token)match(input,62,FOLLOW_20); 

                    				newLeafNode(otherlv_8, grammarAccess.getTaughtCourseAccess().getWebsiteKeyword_7_0());
                    			
                    // InternalTextualRCV.g:2298:4: ( (lv_website_9_0= ruleEString ) )
                    // InternalTextualRCV.g:2299:5: (lv_website_9_0= ruleEString )
                    {
                    // InternalTextualRCV.g:2299:5: (lv_website_9_0= ruleEString )
                    // InternalTextualRCV.g:2300:6: lv_website_9_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getTaughtCourseAccess().getWebsiteEStringParserRuleCall_7_1_0());
                    					
                    pushFollow(FOLLOW_72);
                    lv_website_9_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getTaughtCourseRule());
                    						}
                    						set(
                    							current,
                    							"website",
                    							lv_website_9_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_10=(Token)match(input,63,FOLLOW_12); 

            			newLeafNode(otherlv_10, grammarAccess.getTaughtCourseAccess().getLanguageKeyword_8());
            		
            // InternalTextualRCV.g:2322:3: ( (lv_language_11_0= ruleLanguageCode ) )
            // InternalTextualRCV.g:2323:4: (lv_language_11_0= ruleLanguageCode )
            {
            // InternalTextualRCV.g:2323:4: (lv_language_11_0= ruleLanguageCode )
            // InternalTextualRCV.g:2324:5: lv_language_11_0= ruleLanguageCode
            {

            					newCompositeNode(grammarAccess.getTaughtCourseAccess().getLanguageLanguageCodeParserRuleCall_9_0());
            				
            pushFollow(FOLLOW_73);
            lv_language_11_0=ruleLanguageCode();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTaughtCourseRule());
            					}
            					set(
            						current,
            						"language",
            						lv_language_11_0,
            						"org.montex.researchcv.xtext.TextualRCV.LanguageCode");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_12=(Token)match(input,64,FOLLOW_74); 

            			newLeafNode(otherlv_12, grammarAccess.getTaughtCourseAccess().getResponsibilityKeyword_10());
            		
            // InternalTextualRCV.g:2345:3: ( (lv_responsibility_13_0= ruleEDouble ) )
            // InternalTextualRCV.g:2346:4: (lv_responsibility_13_0= ruleEDouble )
            {
            // InternalTextualRCV.g:2346:4: (lv_responsibility_13_0= ruleEDouble )
            // InternalTextualRCV.g:2347:5: lv_responsibility_13_0= ruleEDouble
            {

            					newCompositeNode(grammarAccess.getTaughtCourseAccess().getResponsibilityEDoubleParserRuleCall_11_0());
            				
            pushFollow(FOLLOW_75);
            lv_responsibility_13_0=ruleEDouble();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTaughtCourseRule());
            					}
            					set(
            						current,
            						"responsibility",
            						lv_responsibility_13_0,
            						"org.montex.researchcv.xtext.TextualRCV.EDouble");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_14=(Token)match(input,65,FOLLOW_61); 

            			newLeafNode(otherlv_14, grammarAccess.getTaughtCourseAccess().getOfferingKeyword_12());
            		
            otherlv_15=(Token)match(input,52,FOLLOW_3); 

            			newLeafNode(otherlv_15, grammarAccess.getTaughtCourseAccess().getLeftParenthesisKeyword_13());
            		
            // InternalTextualRCV.g:2372:3: ( (lv_offering_16_0= ruleCourseOffering ) )
            // InternalTextualRCV.g:2373:4: (lv_offering_16_0= ruleCourseOffering )
            {
            // InternalTextualRCV.g:2373:4: (lv_offering_16_0= ruleCourseOffering )
            // InternalTextualRCV.g:2374:5: lv_offering_16_0= ruleCourseOffering
            {

            					newCompositeNode(grammarAccess.getTaughtCourseAccess().getOfferingCourseOfferingParserRuleCall_14_0());
            				
            pushFollow(FOLLOW_62);
            lv_offering_16_0=ruleCourseOffering();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTaughtCourseRule());
            					}
            					add(
            						current,
            						"offering",
            						lv_offering_16_0,
            						"org.montex.researchcv.xtext.TextualRCV.CourseOffering");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:2391:3: (otherlv_17= ',' ( (lv_offering_18_0= ruleCourseOffering ) ) )*
            loop50:
            do {
                int alt50=2;
                int LA50_0 = input.LA(1);

                if ( (LA50_0==24) ) {
                    alt50=1;
                }


                switch (alt50) {
            	case 1 :
            	    // InternalTextualRCV.g:2392:4: otherlv_17= ',' ( (lv_offering_18_0= ruleCourseOffering ) )
            	    {
            	    otherlv_17=(Token)match(input,24,FOLLOW_3); 

            	    				newLeafNode(otherlv_17, grammarAccess.getTaughtCourseAccess().getCommaKeyword_15_0());
            	    			
            	    // InternalTextualRCV.g:2396:4: ( (lv_offering_18_0= ruleCourseOffering ) )
            	    // InternalTextualRCV.g:2397:5: (lv_offering_18_0= ruleCourseOffering )
            	    {
            	    // InternalTextualRCV.g:2397:5: (lv_offering_18_0= ruleCourseOffering )
            	    // InternalTextualRCV.g:2398:6: lv_offering_18_0= ruleCourseOffering
            	    {

            	    						newCompositeNode(grammarAccess.getTaughtCourseAccess().getOfferingCourseOfferingParserRuleCall_15_1_0());
            	    					
            	    pushFollow(FOLLOW_62);
            	    lv_offering_18_0=ruleCourseOffering();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getTaughtCourseRule());
            	    						}
            	    						add(
            	    							current,
            	    							"offering",
            	    							lv_offering_18_0,
            	    							"org.montex.researchcv.xtext.TextualRCV.CourseOffering");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop50;
                }
            } while (true);

            otherlv_19=(Token)match(input,53,FOLLOW_27); 

            			newLeafNode(otherlv_19, grammarAccess.getTaughtCourseAccess().getRightParenthesisKeyword_16());
            		
            otherlv_20=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_20, grammarAccess.getTaughtCourseAccess().getRightCurlyBracketKeyword_17());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTaughtCourse"


    // $ANTLR start "entryRuleCourseOffering"
    // InternalTextualRCV.g:2428:1: entryRuleCourseOffering returns [EObject current=null] : iv_ruleCourseOffering= ruleCourseOffering EOF ;
    public final EObject entryRuleCourseOffering() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCourseOffering = null;


        try {
            // InternalTextualRCV.g:2428:55: (iv_ruleCourseOffering= ruleCourseOffering EOF )
            // InternalTextualRCV.g:2429:2: iv_ruleCourseOffering= ruleCourseOffering EOF
            {
             newCompositeNode(grammarAccess.getCourseOfferingRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCourseOffering=ruleCourseOffering();

            state._fsp--;

             current =iv_ruleCourseOffering; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCourseOffering"


    // $ANTLR start "ruleCourseOffering"
    // InternalTextualRCV.g:2435:1: ruleCourseOffering returns [EObject current=null] : (otherlv_0= '{' otherlv_1= '}' ) ;
    public final EObject ruleCourseOffering() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalTextualRCV.g:2441:2: ( (otherlv_0= '{' otherlv_1= '}' ) )
            // InternalTextualRCV.g:2442:2: (otherlv_0= '{' otherlv_1= '}' )
            {
            // InternalTextualRCV.g:2442:2: (otherlv_0= '{' otherlv_1= '}' )
            // InternalTextualRCV.g:2443:3: otherlv_0= '{' otherlv_1= '}'
            {
            otherlv_0=(Token)match(input,16,FOLLOW_27); 

            			newLeafNode(otherlv_0, grammarAccess.getCourseOfferingAccess().getLeftCurlyBracketKeyword_0());
            		
            otherlv_1=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getCourseOfferingAccess().getRightCurlyBracketKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCourseOffering"


    // $ANTLR start "entryRuleAddress"
    // InternalTextualRCV.g:2455:1: entryRuleAddress returns [EObject current=null] : iv_ruleAddress= ruleAddress EOF ;
    public final EObject entryRuleAddress() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAddress = null;


        try {
            // InternalTextualRCV.g:2455:48: (iv_ruleAddress= ruleAddress EOF )
            // InternalTextualRCV.g:2456:2: iv_ruleAddress= ruleAddress EOF
            {
             newCompositeNode(grammarAccess.getAddressRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAddress=ruleAddress();

            state._fsp--;

             current =iv_ruleAddress; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAddress"


    // $ANTLR start "ruleAddress"
    // InternalTextualRCV.g:2462:1: ruleAddress returns [EObject current=null] : ( ( (lv_kind_0_0= ruleContactKind ) )? otherlv_1= 'address' otherlv_2= '{' ( (lv_zip_3_0= ruleEString ) ) ( (lv_city_4_0= ruleEString ) ) (otherlv_5= ',' ( (lv_region_6_0= ruleEString ) ) )? otherlv_7= '[' ( (lv_country_8_0= ruleEString ) ) otherlv_9= ']' ( (lv_lines_10_0= ruleEString ) )+ (otherlv_11= '(' ( ( ( (lv_building_12_0= ruleEString ) ) otherlv_13= ',' otherlv_14= 'room' ( (lv_room_15_0= ruleEString ) ) ) | (otherlv_16= 'room' ( (lv_room_17_0= ruleEString ) ) ) | ( (lv_building_18_0= ruleEString ) ) ) otherlv_19= ')' )? otherlv_20= '}' ) ;
    public final EObject ruleAddress() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_19=null;
        Token otherlv_20=null;
        Enumerator lv_kind_0_0 = null;

        AntlrDatatypeRuleToken lv_zip_3_0 = null;

        AntlrDatatypeRuleToken lv_city_4_0 = null;

        AntlrDatatypeRuleToken lv_region_6_0 = null;

        AntlrDatatypeRuleToken lv_country_8_0 = null;

        AntlrDatatypeRuleToken lv_lines_10_0 = null;

        AntlrDatatypeRuleToken lv_building_12_0 = null;

        AntlrDatatypeRuleToken lv_room_15_0 = null;

        AntlrDatatypeRuleToken lv_room_17_0 = null;

        AntlrDatatypeRuleToken lv_building_18_0 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:2468:2: ( ( ( (lv_kind_0_0= ruleContactKind ) )? otherlv_1= 'address' otherlv_2= '{' ( (lv_zip_3_0= ruleEString ) ) ( (lv_city_4_0= ruleEString ) ) (otherlv_5= ',' ( (lv_region_6_0= ruleEString ) ) )? otherlv_7= '[' ( (lv_country_8_0= ruleEString ) ) otherlv_9= ']' ( (lv_lines_10_0= ruleEString ) )+ (otherlv_11= '(' ( ( ( (lv_building_12_0= ruleEString ) ) otherlv_13= ',' otherlv_14= 'room' ( (lv_room_15_0= ruleEString ) ) ) | (otherlv_16= 'room' ( (lv_room_17_0= ruleEString ) ) ) | ( (lv_building_18_0= ruleEString ) ) ) otherlv_19= ')' )? otherlv_20= '}' ) )
            // InternalTextualRCV.g:2469:2: ( ( (lv_kind_0_0= ruleContactKind ) )? otherlv_1= 'address' otherlv_2= '{' ( (lv_zip_3_0= ruleEString ) ) ( (lv_city_4_0= ruleEString ) ) (otherlv_5= ',' ( (lv_region_6_0= ruleEString ) ) )? otherlv_7= '[' ( (lv_country_8_0= ruleEString ) ) otherlv_9= ']' ( (lv_lines_10_0= ruleEString ) )+ (otherlv_11= '(' ( ( ( (lv_building_12_0= ruleEString ) ) otherlv_13= ',' otherlv_14= 'room' ( (lv_room_15_0= ruleEString ) ) ) | (otherlv_16= 'room' ( (lv_room_17_0= ruleEString ) ) ) | ( (lv_building_18_0= ruleEString ) ) ) otherlv_19= ')' )? otherlv_20= '}' )
            {
            // InternalTextualRCV.g:2469:2: ( ( (lv_kind_0_0= ruleContactKind ) )? otherlv_1= 'address' otherlv_2= '{' ( (lv_zip_3_0= ruleEString ) ) ( (lv_city_4_0= ruleEString ) ) (otherlv_5= ',' ( (lv_region_6_0= ruleEString ) ) )? otherlv_7= '[' ( (lv_country_8_0= ruleEString ) ) otherlv_9= ']' ( (lv_lines_10_0= ruleEString ) )+ (otherlv_11= '(' ( ( ( (lv_building_12_0= ruleEString ) ) otherlv_13= ',' otherlv_14= 'room' ( (lv_room_15_0= ruleEString ) ) ) | (otherlv_16= 'room' ( (lv_room_17_0= ruleEString ) ) ) | ( (lv_building_18_0= ruleEString ) ) ) otherlv_19= ')' )? otherlv_20= '}' )
            // InternalTextualRCV.g:2470:3: ( (lv_kind_0_0= ruleContactKind ) )? otherlv_1= 'address' otherlv_2= '{' ( (lv_zip_3_0= ruleEString ) ) ( (lv_city_4_0= ruleEString ) ) (otherlv_5= ',' ( (lv_region_6_0= ruleEString ) ) )? otherlv_7= '[' ( (lv_country_8_0= ruleEString ) ) otherlv_9= ']' ( (lv_lines_10_0= ruleEString ) )+ (otherlv_11= '(' ( ( ( (lv_building_12_0= ruleEString ) ) otherlv_13= ',' otherlv_14= 'room' ( (lv_room_15_0= ruleEString ) ) ) | (otherlv_16= 'room' ( (lv_room_17_0= ruleEString ) ) ) | ( (lv_building_18_0= ruleEString ) ) ) otherlv_19= ')' )? otherlv_20= '}'
            {
            // InternalTextualRCV.g:2470:3: ( (lv_kind_0_0= ruleContactKind ) )?
            int alt51=2;
            int LA51_0 = input.LA(1);

            if ( ((LA51_0>=135 && LA51_0<=136)) ) {
                alt51=1;
            }
            switch (alt51) {
                case 1 :
                    // InternalTextualRCV.g:2471:4: (lv_kind_0_0= ruleContactKind )
                    {
                    // InternalTextualRCV.g:2471:4: (lv_kind_0_0= ruleContactKind )
                    // InternalTextualRCV.g:2472:5: lv_kind_0_0= ruleContactKind
                    {

                    					newCompositeNode(grammarAccess.getAddressAccess().getKindContactKindEnumRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_76);
                    lv_kind_0_0=ruleContactKind();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getAddressRule());
                    					}
                    					set(
                    						current,
                    						"kind",
                    						lv_kind_0_0,
                    						"org.montex.researchcv.xtext.TextualRCV.ContactKind");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,66,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getAddressAccess().getAddressKeyword_1());
            		
            otherlv_2=(Token)match(input,16,FOLLOW_20); 

            			newLeafNode(otherlv_2, grammarAccess.getAddressAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalTextualRCV.g:2497:3: ( (lv_zip_3_0= ruleEString ) )
            // InternalTextualRCV.g:2498:4: (lv_zip_3_0= ruleEString )
            {
            // InternalTextualRCV.g:2498:4: (lv_zip_3_0= ruleEString )
            // InternalTextualRCV.g:2499:5: lv_zip_3_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getAddressAccess().getZipEStringParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_20);
            lv_zip_3_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAddressRule());
            					}
            					set(
            						current,
            						"zip",
            						lv_zip_3_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:2516:3: ( (lv_city_4_0= ruleEString ) )
            // InternalTextualRCV.g:2517:4: (lv_city_4_0= ruleEString )
            {
            // InternalTextualRCV.g:2517:4: (lv_city_4_0= ruleEString )
            // InternalTextualRCV.g:2518:5: lv_city_4_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getAddressAccess().getCityEStringParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_77);
            lv_city_4_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAddressRule());
            					}
            					set(
            						current,
            						"city",
            						lv_city_4_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:2535:3: (otherlv_5= ',' ( (lv_region_6_0= ruleEString ) ) )?
            int alt52=2;
            int LA52_0 = input.LA(1);

            if ( (LA52_0==24) ) {
                alt52=1;
            }
            switch (alt52) {
                case 1 :
                    // InternalTextualRCV.g:2536:4: otherlv_5= ',' ( (lv_region_6_0= ruleEString ) )
                    {
                    otherlv_5=(Token)match(input,24,FOLLOW_20); 

                    				newLeafNode(otherlv_5, grammarAccess.getAddressAccess().getCommaKeyword_5_0());
                    			
                    // InternalTextualRCV.g:2540:4: ( (lv_region_6_0= ruleEString ) )
                    // InternalTextualRCV.g:2541:5: (lv_region_6_0= ruleEString )
                    {
                    // InternalTextualRCV.g:2541:5: (lv_region_6_0= ruleEString )
                    // InternalTextualRCV.g:2542:6: lv_region_6_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getAddressAccess().getRegionEStringParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_78);
                    lv_region_6_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAddressRule());
                    						}
                    						set(
                    							current,
                    							"region",
                    							lv_region_6_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_7=(Token)match(input,27,FOLLOW_20); 

            			newLeafNode(otherlv_7, grammarAccess.getAddressAccess().getLeftSquareBracketKeyword_6());
            		
            // InternalTextualRCV.g:2564:3: ( (lv_country_8_0= ruleEString ) )
            // InternalTextualRCV.g:2565:4: (lv_country_8_0= ruleEString )
            {
            // InternalTextualRCV.g:2565:4: (lv_country_8_0= ruleEString )
            // InternalTextualRCV.g:2566:5: lv_country_8_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getAddressAccess().getCountryEStringParserRuleCall_7_0());
            				
            pushFollow(FOLLOW_23);
            lv_country_8_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAddressRule());
            					}
            					set(
            						current,
            						"country",
            						lv_country_8_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_9=(Token)match(input,28,FOLLOW_20); 

            			newLeafNode(otherlv_9, grammarAccess.getAddressAccess().getRightSquareBracketKeyword_8());
            		
            // InternalTextualRCV.g:2587:3: ( (lv_lines_10_0= ruleEString ) )+
            int cnt53=0;
            loop53:
            do {
                int alt53=2;
                int LA53_0 = input.LA(1);

                if ( ((LA53_0>=RULE_ID && LA53_0<=RULE_STRING)) ) {
                    alt53=1;
                }


                switch (alt53) {
            	case 1 :
            	    // InternalTextualRCV.g:2588:4: (lv_lines_10_0= ruleEString )
            	    {
            	    // InternalTextualRCV.g:2588:4: (lv_lines_10_0= ruleEString )
            	    // InternalTextualRCV.g:2589:5: lv_lines_10_0= ruleEString
            	    {

            	    					newCompositeNode(grammarAccess.getAddressAccess().getLinesEStringParserRuleCall_9_0());
            	    				
            	    pushFollow(FOLLOW_79);
            	    lv_lines_10_0=ruleEString();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getAddressRule());
            	    					}
            	    					add(
            	    						current,
            	    						"lines",
            	    						lv_lines_10_0,
            	    						"org.montex.researchcv.xtext.TextualRCV.EString");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt53 >= 1 ) break loop53;
                        EarlyExitException eee =
                            new EarlyExitException(53, input);
                        throw eee;
                }
                cnt53++;
            } while (true);

            // InternalTextualRCV.g:2606:3: (otherlv_11= '(' ( ( ( (lv_building_12_0= ruleEString ) ) otherlv_13= ',' otherlv_14= 'room' ( (lv_room_15_0= ruleEString ) ) ) | (otherlv_16= 'room' ( (lv_room_17_0= ruleEString ) ) ) | ( (lv_building_18_0= ruleEString ) ) ) otherlv_19= ')' )?
            int alt55=2;
            int LA55_0 = input.LA(1);

            if ( (LA55_0==52) ) {
                alt55=1;
            }
            switch (alt55) {
                case 1 :
                    // InternalTextualRCV.g:2607:4: otherlv_11= '(' ( ( ( (lv_building_12_0= ruleEString ) ) otherlv_13= ',' otherlv_14= 'room' ( (lv_room_15_0= ruleEString ) ) ) | (otherlv_16= 'room' ( (lv_room_17_0= ruleEString ) ) ) | ( (lv_building_18_0= ruleEString ) ) ) otherlv_19= ')'
                    {
                    otherlv_11=(Token)match(input,52,FOLLOW_80); 

                    				newLeafNode(otherlv_11, grammarAccess.getAddressAccess().getLeftParenthesisKeyword_10_0());
                    			
                    // InternalTextualRCV.g:2611:4: ( ( ( (lv_building_12_0= ruleEString ) ) otherlv_13= ',' otherlv_14= 'room' ( (lv_room_15_0= ruleEString ) ) ) | (otherlv_16= 'room' ( (lv_room_17_0= ruleEString ) ) ) | ( (lv_building_18_0= ruleEString ) ) )
                    int alt54=3;
                    switch ( input.LA(1) ) {
                    case RULE_STRING:
                        {
                        int LA54_1 = input.LA(2);

                        if ( (LA54_1==53) ) {
                            alt54=3;
                        }
                        else if ( (LA54_1==24) ) {
                            alt54=1;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 54, 1, input);

                            throw nvae;
                        }
                        }
                        break;
                    case RULE_ID:
                        {
                        int LA54_2 = input.LA(2);

                        if ( (LA54_2==53) ) {
                            alt54=3;
                        }
                        else if ( (LA54_2==24) ) {
                            alt54=1;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 54, 2, input);

                            throw nvae;
                        }
                        }
                        break;
                    case 67:
                        {
                        alt54=2;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 54, 0, input);

                        throw nvae;
                    }

                    switch (alt54) {
                        case 1 :
                            // InternalTextualRCV.g:2612:5: ( ( (lv_building_12_0= ruleEString ) ) otherlv_13= ',' otherlv_14= 'room' ( (lv_room_15_0= ruleEString ) ) )
                            {
                            // InternalTextualRCV.g:2612:5: ( ( (lv_building_12_0= ruleEString ) ) otherlv_13= ',' otherlv_14= 'room' ( (lv_room_15_0= ruleEString ) ) )
                            // InternalTextualRCV.g:2613:6: ( (lv_building_12_0= ruleEString ) ) otherlv_13= ',' otherlv_14= 'room' ( (lv_room_15_0= ruleEString ) )
                            {
                            // InternalTextualRCV.g:2613:6: ( (lv_building_12_0= ruleEString ) )
                            // InternalTextualRCV.g:2614:7: (lv_building_12_0= ruleEString )
                            {
                            // InternalTextualRCV.g:2614:7: (lv_building_12_0= ruleEString )
                            // InternalTextualRCV.g:2615:8: lv_building_12_0= ruleEString
                            {

                            								newCompositeNode(grammarAccess.getAddressAccess().getBuildingEStringParserRuleCall_10_1_0_0_0());
                            							
                            pushFollow(FOLLOW_81);
                            lv_building_12_0=ruleEString();

                            state._fsp--;


                            								if (current==null) {
                            									current = createModelElementForParent(grammarAccess.getAddressRule());
                            								}
                            								set(
                            									current,
                            									"building",
                            									lv_building_12_0,
                            									"org.montex.researchcv.xtext.TextualRCV.EString");
                            								afterParserOrEnumRuleCall();
                            							

                            }


                            }

                            otherlv_13=(Token)match(input,24,FOLLOW_82); 

                            						newLeafNode(otherlv_13, grammarAccess.getAddressAccess().getCommaKeyword_10_1_0_1());
                            					
                            otherlv_14=(Token)match(input,67,FOLLOW_20); 

                            						newLeafNode(otherlv_14, grammarAccess.getAddressAccess().getRoomKeyword_10_1_0_2());
                            					
                            // InternalTextualRCV.g:2640:6: ( (lv_room_15_0= ruleEString ) )
                            // InternalTextualRCV.g:2641:7: (lv_room_15_0= ruleEString )
                            {
                            // InternalTextualRCV.g:2641:7: (lv_room_15_0= ruleEString )
                            // InternalTextualRCV.g:2642:8: lv_room_15_0= ruleEString
                            {

                            								newCompositeNode(grammarAccess.getAddressAccess().getRoomEStringParserRuleCall_10_1_0_3_0());
                            							
                            pushFollow(FOLLOW_83);
                            lv_room_15_0=ruleEString();

                            state._fsp--;


                            								if (current==null) {
                            									current = createModelElementForParent(grammarAccess.getAddressRule());
                            								}
                            								set(
                            									current,
                            									"room",
                            									lv_room_15_0,
                            									"org.montex.researchcv.xtext.TextualRCV.EString");
                            								afterParserOrEnumRuleCall();
                            							

                            }


                            }


                            }


                            }
                            break;
                        case 2 :
                            // InternalTextualRCV.g:2661:5: (otherlv_16= 'room' ( (lv_room_17_0= ruleEString ) ) )
                            {
                            // InternalTextualRCV.g:2661:5: (otherlv_16= 'room' ( (lv_room_17_0= ruleEString ) ) )
                            // InternalTextualRCV.g:2662:6: otherlv_16= 'room' ( (lv_room_17_0= ruleEString ) )
                            {
                            otherlv_16=(Token)match(input,67,FOLLOW_20); 

                            						newLeafNode(otherlv_16, grammarAccess.getAddressAccess().getRoomKeyword_10_1_1_0());
                            					
                            // InternalTextualRCV.g:2666:6: ( (lv_room_17_0= ruleEString ) )
                            // InternalTextualRCV.g:2667:7: (lv_room_17_0= ruleEString )
                            {
                            // InternalTextualRCV.g:2667:7: (lv_room_17_0= ruleEString )
                            // InternalTextualRCV.g:2668:8: lv_room_17_0= ruleEString
                            {

                            								newCompositeNode(grammarAccess.getAddressAccess().getRoomEStringParserRuleCall_10_1_1_1_0());
                            							
                            pushFollow(FOLLOW_83);
                            lv_room_17_0=ruleEString();

                            state._fsp--;


                            								if (current==null) {
                            									current = createModelElementForParent(grammarAccess.getAddressRule());
                            								}
                            								set(
                            									current,
                            									"room",
                            									lv_room_17_0,
                            									"org.montex.researchcv.xtext.TextualRCV.EString");
                            								afterParserOrEnumRuleCall();
                            							

                            }


                            }


                            }


                            }
                            break;
                        case 3 :
                            // InternalTextualRCV.g:2687:5: ( (lv_building_18_0= ruleEString ) )
                            {
                            // InternalTextualRCV.g:2687:5: ( (lv_building_18_0= ruleEString ) )
                            // InternalTextualRCV.g:2688:6: (lv_building_18_0= ruleEString )
                            {
                            // InternalTextualRCV.g:2688:6: (lv_building_18_0= ruleEString )
                            // InternalTextualRCV.g:2689:7: lv_building_18_0= ruleEString
                            {

                            							newCompositeNode(grammarAccess.getAddressAccess().getBuildingEStringParserRuleCall_10_1_2_0());
                            						
                            pushFollow(FOLLOW_83);
                            lv_building_18_0=ruleEString();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getAddressRule());
                            							}
                            							set(
                            								current,
                            								"building",
                            								lv_building_18_0,
                            								"org.montex.researchcv.xtext.TextualRCV.EString");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }


                            }
                            break;

                    }

                    otherlv_19=(Token)match(input,53,FOLLOW_27); 

                    				newLeafNode(otherlv_19, grammarAccess.getAddressAccess().getRightParenthesisKeyword_10_2());
                    			

                    }
                    break;

            }

            otherlv_20=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_20, grammarAccess.getAddressAccess().getRightCurlyBracketKeyword_11());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAddress"


    // $ANTLR start "entryRuleEmail"
    // InternalTextualRCV.g:2720:1: entryRuleEmail returns [EObject current=null] : iv_ruleEmail= ruleEmail EOF ;
    public final EObject entryRuleEmail() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEmail = null;


        try {
            // InternalTextualRCV.g:2720:46: (iv_ruleEmail= ruleEmail EOF )
            // InternalTextualRCV.g:2721:2: iv_ruleEmail= ruleEmail EOF
            {
             newCompositeNode(grammarAccess.getEmailRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEmail=ruleEmail();

            state._fsp--;

             current =iv_ruleEmail; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEmail"


    // $ANTLR start "ruleEmail"
    // InternalTextualRCV.g:2727:1: ruleEmail returns [EObject current=null] : ( ( (lv_kind_0_0= ruleContactKind ) )? otherlv_1= 'email:' ( (lv_address_2_0= ruleEString ) ) ) ;
    public final EObject ruleEmail() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Enumerator lv_kind_0_0 = null;

        AntlrDatatypeRuleToken lv_address_2_0 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:2733:2: ( ( ( (lv_kind_0_0= ruleContactKind ) )? otherlv_1= 'email:' ( (lv_address_2_0= ruleEString ) ) ) )
            // InternalTextualRCV.g:2734:2: ( ( (lv_kind_0_0= ruleContactKind ) )? otherlv_1= 'email:' ( (lv_address_2_0= ruleEString ) ) )
            {
            // InternalTextualRCV.g:2734:2: ( ( (lv_kind_0_0= ruleContactKind ) )? otherlv_1= 'email:' ( (lv_address_2_0= ruleEString ) ) )
            // InternalTextualRCV.g:2735:3: ( (lv_kind_0_0= ruleContactKind ) )? otherlv_1= 'email:' ( (lv_address_2_0= ruleEString ) )
            {
            // InternalTextualRCV.g:2735:3: ( (lv_kind_0_0= ruleContactKind ) )?
            int alt56=2;
            int LA56_0 = input.LA(1);

            if ( ((LA56_0>=135 && LA56_0<=136)) ) {
                alt56=1;
            }
            switch (alt56) {
                case 1 :
                    // InternalTextualRCV.g:2736:4: (lv_kind_0_0= ruleContactKind )
                    {
                    // InternalTextualRCV.g:2736:4: (lv_kind_0_0= ruleContactKind )
                    // InternalTextualRCV.g:2737:5: lv_kind_0_0= ruleContactKind
                    {

                    					newCompositeNode(grammarAccess.getEmailAccess().getKindContactKindEnumRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_84);
                    lv_kind_0_0=ruleContactKind();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getEmailRule());
                    					}
                    					set(
                    						current,
                    						"kind",
                    						lv_kind_0_0,
                    						"org.montex.researchcv.xtext.TextualRCV.ContactKind");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,68,FOLLOW_20); 

            			newLeafNode(otherlv_1, grammarAccess.getEmailAccess().getEmailKeyword_1());
            		
            // InternalTextualRCV.g:2758:3: ( (lv_address_2_0= ruleEString ) )
            // InternalTextualRCV.g:2759:4: (lv_address_2_0= ruleEString )
            {
            // InternalTextualRCV.g:2759:4: (lv_address_2_0= ruleEString )
            // InternalTextualRCV.g:2760:5: lv_address_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getEmailAccess().getAddressEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_address_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getEmailRule());
            					}
            					set(
            						current,
            						"address",
            						lv_address_2_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEmail"


    // $ANTLR start "entryRulePhone"
    // InternalTextualRCV.g:2781:1: entryRulePhone returns [EObject current=null] : iv_rulePhone= rulePhone EOF ;
    public final EObject entryRulePhone() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePhone = null;


        try {
            // InternalTextualRCV.g:2781:46: (iv_rulePhone= rulePhone EOF )
            // InternalTextualRCV.g:2782:2: iv_rulePhone= rulePhone EOF
            {
             newCompositeNode(grammarAccess.getPhoneRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePhone=rulePhone();

            state._fsp--;

             current =iv_rulePhone; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePhone"


    // $ANTLR start "rulePhone"
    // InternalTextualRCV.g:2788:1: rulePhone returns [EObject current=null] : ( ( (lv_kind_0_0= ruleContactKind ) )? otherlv_1= 'phone:' ( (lv_number_2_0= ruleEString ) ) ) ;
    public final EObject rulePhone() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Enumerator lv_kind_0_0 = null;

        AntlrDatatypeRuleToken lv_number_2_0 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:2794:2: ( ( ( (lv_kind_0_0= ruleContactKind ) )? otherlv_1= 'phone:' ( (lv_number_2_0= ruleEString ) ) ) )
            // InternalTextualRCV.g:2795:2: ( ( (lv_kind_0_0= ruleContactKind ) )? otherlv_1= 'phone:' ( (lv_number_2_0= ruleEString ) ) )
            {
            // InternalTextualRCV.g:2795:2: ( ( (lv_kind_0_0= ruleContactKind ) )? otherlv_1= 'phone:' ( (lv_number_2_0= ruleEString ) ) )
            // InternalTextualRCV.g:2796:3: ( (lv_kind_0_0= ruleContactKind ) )? otherlv_1= 'phone:' ( (lv_number_2_0= ruleEString ) )
            {
            // InternalTextualRCV.g:2796:3: ( (lv_kind_0_0= ruleContactKind ) )?
            int alt57=2;
            int LA57_0 = input.LA(1);

            if ( ((LA57_0>=135 && LA57_0<=136)) ) {
                alt57=1;
            }
            switch (alt57) {
                case 1 :
                    // InternalTextualRCV.g:2797:4: (lv_kind_0_0= ruleContactKind )
                    {
                    // InternalTextualRCV.g:2797:4: (lv_kind_0_0= ruleContactKind )
                    // InternalTextualRCV.g:2798:5: lv_kind_0_0= ruleContactKind
                    {

                    					newCompositeNode(grammarAccess.getPhoneAccess().getKindContactKindEnumRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_85);
                    lv_kind_0_0=ruleContactKind();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getPhoneRule());
                    					}
                    					set(
                    						current,
                    						"kind",
                    						lv_kind_0_0,
                    						"org.montex.researchcv.xtext.TextualRCV.ContactKind");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,69,FOLLOW_20); 

            			newLeafNode(otherlv_1, grammarAccess.getPhoneAccess().getPhoneKeyword_1());
            		
            // InternalTextualRCV.g:2819:3: ( (lv_number_2_0= ruleEString ) )
            // InternalTextualRCV.g:2820:4: (lv_number_2_0= ruleEString )
            {
            // InternalTextualRCV.g:2820:4: (lv_number_2_0= ruleEString )
            // InternalTextualRCV.g:2821:5: lv_number_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getPhoneAccess().getNumberEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_number_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPhoneRule());
            					}
            					set(
            						current,
            						"number",
            						lv_number_2_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePhone"


    // $ANTLR start "entryRuleWebsite"
    // InternalTextualRCV.g:2842:1: entryRuleWebsite returns [EObject current=null] : iv_ruleWebsite= ruleWebsite EOF ;
    public final EObject entryRuleWebsite() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWebsite = null;


        try {
            // InternalTextualRCV.g:2842:48: (iv_ruleWebsite= ruleWebsite EOF )
            // InternalTextualRCV.g:2843:2: iv_ruleWebsite= ruleWebsite EOF
            {
             newCompositeNode(grammarAccess.getWebsiteRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleWebsite=ruleWebsite();

            state._fsp--;

             current =iv_ruleWebsite; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWebsite"


    // $ANTLR start "ruleWebsite"
    // InternalTextualRCV.g:2849:1: ruleWebsite returns [EObject current=null] : ( ( (lv_kind_0_0= ruleContactKind ) )? otherlv_1= 'website:' ( (lv_url_2_0= ruleEString ) ) ) ;
    public final EObject ruleWebsite() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Enumerator lv_kind_0_0 = null;

        AntlrDatatypeRuleToken lv_url_2_0 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:2855:2: ( ( ( (lv_kind_0_0= ruleContactKind ) )? otherlv_1= 'website:' ( (lv_url_2_0= ruleEString ) ) ) )
            // InternalTextualRCV.g:2856:2: ( ( (lv_kind_0_0= ruleContactKind ) )? otherlv_1= 'website:' ( (lv_url_2_0= ruleEString ) ) )
            {
            // InternalTextualRCV.g:2856:2: ( ( (lv_kind_0_0= ruleContactKind ) )? otherlv_1= 'website:' ( (lv_url_2_0= ruleEString ) ) )
            // InternalTextualRCV.g:2857:3: ( (lv_kind_0_0= ruleContactKind ) )? otherlv_1= 'website:' ( (lv_url_2_0= ruleEString ) )
            {
            // InternalTextualRCV.g:2857:3: ( (lv_kind_0_0= ruleContactKind ) )?
            int alt58=2;
            int LA58_0 = input.LA(1);

            if ( ((LA58_0>=135 && LA58_0<=136)) ) {
                alt58=1;
            }
            switch (alt58) {
                case 1 :
                    // InternalTextualRCV.g:2858:4: (lv_kind_0_0= ruleContactKind )
                    {
                    // InternalTextualRCV.g:2858:4: (lv_kind_0_0= ruleContactKind )
                    // InternalTextualRCV.g:2859:5: lv_kind_0_0= ruleContactKind
                    {

                    					newCompositeNode(grammarAccess.getWebsiteAccess().getKindContactKindEnumRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_86);
                    lv_kind_0_0=ruleContactKind();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getWebsiteRule());
                    					}
                    					set(
                    						current,
                    						"kind",
                    						lv_kind_0_0,
                    						"org.montex.researchcv.xtext.TextualRCV.ContactKind");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            otherlv_1=(Token)match(input,70,FOLLOW_20); 

            			newLeafNode(otherlv_1, grammarAccess.getWebsiteAccess().getWebsiteKeyword_1());
            		
            // InternalTextualRCV.g:2880:3: ( (lv_url_2_0= ruleEString ) )
            // InternalTextualRCV.g:2881:4: (lv_url_2_0= ruleEString )
            {
            // InternalTextualRCV.g:2881:4: (lv_url_2_0= ruleEString )
            // InternalTextualRCV.g:2882:5: lv_url_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getWebsiteAccess().getUrlEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_url_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getWebsiteRule());
            					}
            					set(
            						current,
            						"url",
            						lv_url_2_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWebsite"


    // $ANTLR start "entryRulePerson_Impl"
    // InternalTextualRCV.g:2903:1: entryRulePerson_Impl returns [EObject current=null] : iv_rulePerson_Impl= rulePerson_Impl EOF ;
    public final EObject entryRulePerson_Impl() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePerson_Impl = null;


        try {
            // InternalTextualRCV.g:2903:52: (iv_rulePerson_Impl= rulePerson_Impl EOF )
            // InternalTextualRCV.g:2904:2: iv_rulePerson_Impl= rulePerson_Impl EOF
            {
             newCompositeNode(grammarAccess.getPerson_ImplRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePerson_Impl=rulePerson_Impl();

            state._fsp--;

             current =iv_rulePerson_Impl; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePerson_Impl"


    // $ANTLR start "rulePerson_Impl"
    // InternalTextualRCV.g:2910:1: rulePerson_Impl returns [EObject current=null] : ( ( (lv_lastNames_0_0= ruleEString ) )+ otherlv_1= ',' ( (lv_firstNames_2_0= ruleEString ) )+ ) ;
    public final EObject rulePerson_Impl() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_lastNames_0_0 = null;

        AntlrDatatypeRuleToken lv_firstNames_2_0 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:2916:2: ( ( ( (lv_lastNames_0_0= ruleEString ) )+ otherlv_1= ',' ( (lv_firstNames_2_0= ruleEString ) )+ ) )
            // InternalTextualRCV.g:2917:2: ( ( (lv_lastNames_0_0= ruleEString ) )+ otherlv_1= ',' ( (lv_firstNames_2_0= ruleEString ) )+ )
            {
            // InternalTextualRCV.g:2917:2: ( ( (lv_lastNames_0_0= ruleEString ) )+ otherlv_1= ',' ( (lv_firstNames_2_0= ruleEString ) )+ )
            // InternalTextualRCV.g:2918:3: ( (lv_lastNames_0_0= ruleEString ) )+ otherlv_1= ',' ( (lv_firstNames_2_0= ruleEString ) )+
            {
            // InternalTextualRCV.g:2918:3: ( (lv_lastNames_0_0= ruleEString ) )+
            int cnt59=0;
            loop59:
            do {
                int alt59=2;
                int LA59_0 = input.LA(1);

                if ( ((LA59_0>=RULE_ID && LA59_0<=RULE_STRING)) ) {
                    alt59=1;
                }


                switch (alt59) {
            	case 1 :
            	    // InternalTextualRCV.g:2919:4: (lv_lastNames_0_0= ruleEString )
            	    {
            	    // InternalTextualRCV.g:2919:4: (lv_lastNames_0_0= ruleEString )
            	    // InternalTextualRCV.g:2920:5: lv_lastNames_0_0= ruleEString
            	    {

            	    					newCompositeNode(grammarAccess.getPerson_ImplAccess().getLastNamesEStringParserRuleCall_0_0());
            	    				
            	    pushFollow(FOLLOW_32);
            	    lv_lastNames_0_0=ruleEString();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getPerson_ImplRule());
            	    					}
            	    					add(
            	    						current,
            	    						"lastNames",
            	    						lv_lastNames_0_0,
            	    						"org.montex.researchcv.xtext.TextualRCV.EString");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt59 >= 1 ) break loop59;
                        EarlyExitException eee =
                            new EarlyExitException(59, input);
                        throw eee;
                }
                cnt59++;
            } while (true);

            otherlv_1=(Token)match(input,24,FOLLOW_20); 

            			newLeafNode(otherlv_1, grammarAccess.getPerson_ImplAccess().getCommaKeyword_1());
            		
            // InternalTextualRCV.g:2941:3: ( (lv_firstNames_2_0= ruleEString ) )+
            int cnt60=0;
            loop60:
            do {
                int alt60=2;
                int LA60_0 = input.LA(1);

                if ( ((LA60_0>=RULE_ID && LA60_0<=RULE_STRING)) ) {
                    alt60=1;
                }


                switch (alt60) {
            	case 1 :
            	    // InternalTextualRCV.g:2942:4: (lv_firstNames_2_0= ruleEString )
            	    {
            	    // InternalTextualRCV.g:2942:4: (lv_firstNames_2_0= ruleEString )
            	    // InternalTextualRCV.g:2943:5: lv_firstNames_2_0= ruleEString
            	    {

            	    					newCompositeNode(grammarAccess.getPerson_ImplAccess().getFirstNamesEStringParserRuleCall_2_0());
            	    				
            	    pushFollow(FOLLOW_87);
            	    lv_firstNames_2_0=ruleEString();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getPerson_ImplRule());
            	    					}
            	    					add(
            	    						current,
            	    						"firstNames",
            	    						lv_firstNames_2_0,
            	    						"org.montex.researchcv.xtext.TextualRCV.EString");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt60 >= 1 ) break loop60;
                        EarlyExitException eee =
                            new EarlyExitException(60, input);
                        throw eee;
                }
                cnt60++;
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePerson_Impl"


    // $ANTLR start "entryRuleEBoolean"
    // InternalTextualRCV.g:2964:1: entryRuleEBoolean returns [String current=null] : iv_ruleEBoolean= ruleEBoolean EOF ;
    public final String entryRuleEBoolean() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEBoolean = null;


        try {
            // InternalTextualRCV.g:2964:48: (iv_ruleEBoolean= ruleEBoolean EOF )
            // InternalTextualRCV.g:2965:2: iv_ruleEBoolean= ruleEBoolean EOF
            {
             newCompositeNode(grammarAccess.getEBooleanRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEBoolean=ruleEBoolean();

            state._fsp--;

             current =iv_ruleEBoolean.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEBoolean"


    // $ANTLR start "ruleEBoolean"
    // InternalTextualRCV.g:2971:1: ruleEBoolean returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'true' | kw= 'false' ) ;
    public final AntlrDatatypeRuleToken ruleEBoolean() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalTextualRCV.g:2977:2: ( (kw= 'true' | kw= 'false' ) )
            // InternalTextualRCV.g:2978:2: (kw= 'true' | kw= 'false' )
            {
            // InternalTextualRCV.g:2978:2: (kw= 'true' | kw= 'false' )
            int alt61=2;
            int LA61_0 = input.LA(1);

            if ( (LA61_0==71) ) {
                alt61=1;
            }
            else if ( (LA61_0==72) ) {
                alt61=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 61, 0, input);

                throw nvae;
            }
            switch (alt61) {
                case 1 :
                    // InternalTextualRCV.g:2979:3: kw= 'true'
                    {
                    kw=(Token)match(input,71,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getEBooleanAccess().getTrueKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalTextualRCV.g:2985:3: kw= 'false'
                    {
                    kw=(Token)match(input,72,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getEBooleanAccess().getFalseKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEBoolean"


    // $ANTLR start "entryRulePublishedDate"
    // InternalTextualRCV.g:2994:1: entryRulePublishedDate returns [EObject current=null] : iv_rulePublishedDate= rulePublishedDate EOF ;
    public final EObject entryRulePublishedDate() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePublishedDate = null;



        	HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens();

        try {
            // InternalTextualRCV.g:2996:2: (iv_rulePublishedDate= rulePublishedDate EOF )
            // InternalTextualRCV.g:2997:2: iv_rulePublishedDate= rulePublishedDate EOF
            {
             newCompositeNode(grammarAccess.getPublishedDateRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePublishedDate=rulePublishedDate();

            state._fsp--;

             current =iv_rulePublishedDate; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "entryRulePublishedDate"


    // $ANTLR start "rulePublishedDate"
    // InternalTextualRCV.g:3006:1: rulePublishedDate returns [EObject current=null] : ( ( (lv_year_0_0= ruleEInt ) ) (otherlv_1= '.' ( (lv_month_2_0= ruleEInt ) ) (otherlv_3= '.' ( (lv_day_4_0= ruleEInt ) ) )? )? ) ;
    public final EObject rulePublishedDate() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        AntlrDatatypeRuleToken lv_year_0_0 = null;

        AntlrDatatypeRuleToken lv_month_2_0 = null;

        AntlrDatatypeRuleToken lv_day_4_0 = null;



        	enterRule();
        	HiddenTokens myHiddenTokenState = ((XtextTokenStream)input).setHiddenTokens();

        try {
            // InternalTextualRCV.g:3013:2: ( ( ( (lv_year_0_0= ruleEInt ) ) (otherlv_1= '.' ( (lv_month_2_0= ruleEInt ) ) (otherlv_3= '.' ( (lv_day_4_0= ruleEInt ) ) )? )? ) )
            // InternalTextualRCV.g:3014:2: ( ( (lv_year_0_0= ruleEInt ) ) (otherlv_1= '.' ( (lv_month_2_0= ruleEInt ) ) (otherlv_3= '.' ( (lv_day_4_0= ruleEInt ) ) )? )? )
            {
            // InternalTextualRCV.g:3014:2: ( ( (lv_year_0_0= ruleEInt ) ) (otherlv_1= '.' ( (lv_month_2_0= ruleEInt ) ) (otherlv_3= '.' ( (lv_day_4_0= ruleEInt ) ) )? )? )
            // InternalTextualRCV.g:3015:3: ( (lv_year_0_0= ruleEInt ) ) (otherlv_1= '.' ( (lv_month_2_0= ruleEInt ) ) (otherlv_3= '.' ( (lv_day_4_0= ruleEInt ) ) )? )?
            {
            // InternalTextualRCV.g:3015:3: ( (lv_year_0_0= ruleEInt ) )
            // InternalTextualRCV.g:3016:4: (lv_year_0_0= ruleEInt )
            {
            // InternalTextualRCV.g:3016:4: (lv_year_0_0= ruleEInt )
            // InternalTextualRCV.g:3017:5: lv_year_0_0= ruleEInt
            {

            					newCompositeNode(grammarAccess.getPublishedDateAccess().getYearEIntParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_29);
            lv_year_0_0=ruleEInt();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPublishedDateRule());
            					}
            					set(
            						current,
            						"year",
            						lv_year_0_0,
            						"org.montex.researchcv.xtext.TextualRCV.EInt");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:3034:3: (otherlv_1= '.' ( (lv_month_2_0= ruleEInt ) ) (otherlv_3= '.' ( (lv_day_4_0= ruleEInt ) ) )? )?
            int alt63=2;
            int LA63_0 = input.LA(1);

            if ( (LA63_0==21) ) {
                alt63=1;
            }
            switch (alt63) {
                case 1 :
                    // InternalTextualRCV.g:3035:4: otherlv_1= '.' ( (lv_month_2_0= ruleEInt ) ) (otherlv_3= '.' ( (lv_day_4_0= ruleEInt ) ) )?
                    {
                    otherlv_1=(Token)match(input,21,FOLLOW_9); 

                    				newLeafNode(otherlv_1, grammarAccess.getPublishedDateAccess().getFullStopKeyword_1_0());
                    			
                    // InternalTextualRCV.g:3039:4: ( (lv_month_2_0= ruleEInt ) )
                    // InternalTextualRCV.g:3040:5: (lv_month_2_0= ruleEInt )
                    {
                    // InternalTextualRCV.g:3040:5: (lv_month_2_0= ruleEInt )
                    // InternalTextualRCV.g:3041:6: lv_month_2_0= ruleEInt
                    {

                    						newCompositeNode(grammarAccess.getPublishedDateAccess().getMonthEIntParserRuleCall_1_1_0());
                    					
                    pushFollow(FOLLOW_29);
                    lv_month_2_0=ruleEInt();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPublishedDateRule());
                    						}
                    						set(
                    							current,
                    							"month",
                    							lv_month_2_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EInt");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:3058:4: (otherlv_3= '.' ( (lv_day_4_0= ruleEInt ) ) )?
                    int alt62=2;
                    int LA62_0 = input.LA(1);

                    if ( (LA62_0==21) ) {
                        alt62=1;
                    }
                    switch (alt62) {
                        case 1 :
                            // InternalTextualRCV.g:3059:5: otherlv_3= '.' ( (lv_day_4_0= ruleEInt ) )
                            {
                            otherlv_3=(Token)match(input,21,FOLLOW_9); 

                            					newLeafNode(otherlv_3, grammarAccess.getPublishedDateAccess().getFullStopKeyword_1_2_0());
                            				
                            // InternalTextualRCV.g:3063:5: ( (lv_day_4_0= ruleEInt ) )
                            // InternalTextualRCV.g:3064:6: (lv_day_4_0= ruleEInt )
                            {
                            // InternalTextualRCV.g:3064:6: (lv_day_4_0= ruleEInt )
                            // InternalTextualRCV.g:3065:7: lv_day_4_0= ruleEInt
                            {

                            							newCompositeNode(grammarAccess.getPublishedDateAccess().getDayEIntParserRuleCall_1_2_1_0());
                            						
                            pushFollow(FOLLOW_2);
                            lv_day_4_0=ruleEInt();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getPublishedDateRule());
                            							}
                            							set(
                            								current,
                            								"day",
                            								lv_day_4_0,
                            								"org.montex.researchcv.xtext.TextualRCV.EInt");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }


                            }
                            break;

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {

            	myHiddenTokenState.restore();

        }
        return current;
    }
    // $ANTLR end "rulePublishedDate"


    // $ANTLR start "entryRuleJournal"
    // InternalTextualRCV.g:3091:1: entryRuleJournal returns [EObject current=null] : iv_ruleJournal= ruleJournal EOF ;
    public final EObject entryRuleJournal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleJournal = null;


        try {
            // InternalTextualRCV.g:3091:48: (iv_ruleJournal= ruleJournal EOF )
            // InternalTextualRCV.g:3092:2: iv_ruleJournal= ruleJournal EOF
            {
             newCompositeNode(grammarAccess.getJournalRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleJournal=ruleJournal();

            state._fsp--;

             current =iv_ruleJournal; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleJournal"


    // $ANTLR start "ruleJournal"
    // InternalTextualRCV.g:3098:1: ruleJournal returns [EObject current=null] : ( ( (lv_openAccess_0_0= 'openAccess' ) ) otherlv_1= 'Journal' ( (lv_citekey_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'title' ( (lv_title_5_0= ruleEString ) ) (otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) ) )? otherlv_8= 'published' ( (lv_published_9_0= ruleEBoolean ) ) (otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) ) )? (otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) ) )? otherlv_14= 'withAuthorVersion' ( (lv_withAuthorVersion_15_0= ruleEBoolean ) ) (otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}' )? otherlv_22= 'journalName' ( (lv_journalName_23_0= ruleEString ) ) (otherlv_24= 'publisher' ( (lv_publisher_25_0= ruleEString ) ) )? (otherlv_26= 'firstPage' ( (lv_firstPage_27_0= ruleEString ) ) )? (otherlv_28= 'lastPage' ( (lv_lastPage_29_0= ruleEString ) ) )? otherlv_30= 'volume' ( (lv_volume_31_0= ruleEInt ) ) otherlv_32= 'issue' ( (lv_issue_33_0= ruleEInt ) ) otherlv_34= 'authors' otherlv_35= '(' ( ( ruleEString ) ) (otherlv_37= ',' ( ( ruleEString ) ) )* otherlv_39= ')' (otherlv_40= 'relatedProjects' otherlv_41= '(' ( ( ruleEString ) ) (otherlv_43= ',' ( ( ruleEString ) ) )* otherlv_45= ')' )? otherlv_46= 'date' ( (lv_date_47_0= rulePublishedDate ) ) (otherlv_48= 'ISSN' otherlv_49= '{' ( (lv_ISSN_50_0= ruleSerialNumber ) ) (otherlv_51= ',' ( (lv_ISSN_52_0= ruleSerialNumber ) ) )* otherlv_53= '}' )? otherlv_54= '}' ) ;
    public final EObject ruleJournal() throws RecognitionException {
        EObject current = null;

        Token lv_openAccess_0_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_21=null;
        Token otherlv_22=null;
        Token otherlv_24=null;
        Token otherlv_26=null;
        Token otherlv_28=null;
        Token otherlv_30=null;
        Token otherlv_32=null;
        Token otherlv_34=null;
        Token otherlv_35=null;
        Token otherlv_37=null;
        Token otherlv_39=null;
        Token otherlv_40=null;
        Token otherlv_41=null;
        Token otherlv_43=null;
        Token otherlv_45=null;
        Token otherlv_46=null;
        Token otherlv_48=null;
        Token otherlv_49=null;
        Token otherlv_51=null;
        Token otherlv_53=null;
        Token otherlv_54=null;
        AntlrDatatypeRuleToken lv_citekey_2_0 = null;

        AntlrDatatypeRuleToken lv_title_5_0 = null;

        AntlrDatatypeRuleToken lv_URL_7_0 = null;

        AntlrDatatypeRuleToken lv_published_9_0 = null;

        AntlrDatatypeRuleToken lv_DOI_11_0 = null;

        AntlrDatatypeRuleToken lv_abstract_13_0 = null;

        AntlrDatatypeRuleToken lv_withAuthorVersion_15_0 = null;

        AntlrDatatypeRuleToken lv_notes_18_0 = null;

        AntlrDatatypeRuleToken lv_notes_20_0 = null;

        AntlrDatatypeRuleToken lv_journalName_23_0 = null;

        AntlrDatatypeRuleToken lv_publisher_25_0 = null;

        AntlrDatatypeRuleToken lv_firstPage_27_0 = null;

        AntlrDatatypeRuleToken lv_lastPage_29_0 = null;

        AntlrDatatypeRuleToken lv_volume_31_0 = null;

        AntlrDatatypeRuleToken lv_issue_33_0 = null;

        EObject lv_date_47_0 = null;

        EObject lv_ISSN_50_0 = null;

        EObject lv_ISSN_52_0 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:3104:2: ( ( ( (lv_openAccess_0_0= 'openAccess' ) ) otherlv_1= 'Journal' ( (lv_citekey_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'title' ( (lv_title_5_0= ruleEString ) ) (otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) ) )? otherlv_8= 'published' ( (lv_published_9_0= ruleEBoolean ) ) (otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) ) )? (otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) ) )? otherlv_14= 'withAuthorVersion' ( (lv_withAuthorVersion_15_0= ruleEBoolean ) ) (otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}' )? otherlv_22= 'journalName' ( (lv_journalName_23_0= ruleEString ) ) (otherlv_24= 'publisher' ( (lv_publisher_25_0= ruleEString ) ) )? (otherlv_26= 'firstPage' ( (lv_firstPage_27_0= ruleEString ) ) )? (otherlv_28= 'lastPage' ( (lv_lastPage_29_0= ruleEString ) ) )? otherlv_30= 'volume' ( (lv_volume_31_0= ruleEInt ) ) otherlv_32= 'issue' ( (lv_issue_33_0= ruleEInt ) ) otherlv_34= 'authors' otherlv_35= '(' ( ( ruleEString ) ) (otherlv_37= ',' ( ( ruleEString ) ) )* otherlv_39= ')' (otherlv_40= 'relatedProjects' otherlv_41= '(' ( ( ruleEString ) ) (otherlv_43= ',' ( ( ruleEString ) ) )* otherlv_45= ')' )? otherlv_46= 'date' ( (lv_date_47_0= rulePublishedDate ) ) (otherlv_48= 'ISSN' otherlv_49= '{' ( (lv_ISSN_50_0= ruleSerialNumber ) ) (otherlv_51= ',' ( (lv_ISSN_52_0= ruleSerialNumber ) ) )* otherlv_53= '}' )? otherlv_54= '}' ) )
            // InternalTextualRCV.g:3105:2: ( ( (lv_openAccess_0_0= 'openAccess' ) ) otherlv_1= 'Journal' ( (lv_citekey_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'title' ( (lv_title_5_0= ruleEString ) ) (otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) ) )? otherlv_8= 'published' ( (lv_published_9_0= ruleEBoolean ) ) (otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) ) )? (otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) ) )? otherlv_14= 'withAuthorVersion' ( (lv_withAuthorVersion_15_0= ruleEBoolean ) ) (otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}' )? otherlv_22= 'journalName' ( (lv_journalName_23_0= ruleEString ) ) (otherlv_24= 'publisher' ( (lv_publisher_25_0= ruleEString ) ) )? (otherlv_26= 'firstPage' ( (lv_firstPage_27_0= ruleEString ) ) )? (otherlv_28= 'lastPage' ( (lv_lastPage_29_0= ruleEString ) ) )? otherlv_30= 'volume' ( (lv_volume_31_0= ruleEInt ) ) otherlv_32= 'issue' ( (lv_issue_33_0= ruleEInt ) ) otherlv_34= 'authors' otherlv_35= '(' ( ( ruleEString ) ) (otherlv_37= ',' ( ( ruleEString ) ) )* otherlv_39= ')' (otherlv_40= 'relatedProjects' otherlv_41= '(' ( ( ruleEString ) ) (otherlv_43= ',' ( ( ruleEString ) ) )* otherlv_45= ')' )? otherlv_46= 'date' ( (lv_date_47_0= rulePublishedDate ) ) (otherlv_48= 'ISSN' otherlv_49= '{' ( (lv_ISSN_50_0= ruleSerialNumber ) ) (otherlv_51= ',' ( (lv_ISSN_52_0= ruleSerialNumber ) ) )* otherlv_53= '}' )? otherlv_54= '}' )
            {
            // InternalTextualRCV.g:3105:2: ( ( (lv_openAccess_0_0= 'openAccess' ) ) otherlv_1= 'Journal' ( (lv_citekey_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'title' ( (lv_title_5_0= ruleEString ) ) (otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) ) )? otherlv_8= 'published' ( (lv_published_9_0= ruleEBoolean ) ) (otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) ) )? (otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) ) )? otherlv_14= 'withAuthorVersion' ( (lv_withAuthorVersion_15_0= ruleEBoolean ) ) (otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}' )? otherlv_22= 'journalName' ( (lv_journalName_23_0= ruleEString ) ) (otherlv_24= 'publisher' ( (lv_publisher_25_0= ruleEString ) ) )? (otherlv_26= 'firstPage' ( (lv_firstPage_27_0= ruleEString ) ) )? (otherlv_28= 'lastPage' ( (lv_lastPage_29_0= ruleEString ) ) )? otherlv_30= 'volume' ( (lv_volume_31_0= ruleEInt ) ) otherlv_32= 'issue' ( (lv_issue_33_0= ruleEInt ) ) otherlv_34= 'authors' otherlv_35= '(' ( ( ruleEString ) ) (otherlv_37= ',' ( ( ruleEString ) ) )* otherlv_39= ')' (otherlv_40= 'relatedProjects' otherlv_41= '(' ( ( ruleEString ) ) (otherlv_43= ',' ( ( ruleEString ) ) )* otherlv_45= ')' )? otherlv_46= 'date' ( (lv_date_47_0= rulePublishedDate ) ) (otherlv_48= 'ISSN' otherlv_49= '{' ( (lv_ISSN_50_0= ruleSerialNumber ) ) (otherlv_51= ',' ( (lv_ISSN_52_0= ruleSerialNumber ) ) )* otherlv_53= '}' )? otherlv_54= '}' )
            // InternalTextualRCV.g:3106:3: ( (lv_openAccess_0_0= 'openAccess' ) ) otherlv_1= 'Journal' ( (lv_citekey_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'title' ( (lv_title_5_0= ruleEString ) ) (otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) ) )? otherlv_8= 'published' ( (lv_published_9_0= ruleEBoolean ) ) (otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) ) )? (otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) ) )? otherlv_14= 'withAuthorVersion' ( (lv_withAuthorVersion_15_0= ruleEBoolean ) ) (otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}' )? otherlv_22= 'journalName' ( (lv_journalName_23_0= ruleEString ) ) (otherlv_24= 'publisher' ( (lv_publisher_25_0= ruleEString ) ) )? (otherlv_26= 'firstPage' ( (lv_firstPage_27_0= ruleEString ) ) )? (otherlv_28= 'lastPage' ( (lv_lastPage_29_0= ruleEString ) ) )? otherlv_30= 'volume' ( (lv_volume_31_0= ruleEInt ) ) otherlv_32= 'issue' ( (lv_issue_33_0= ruleEInt ) ) otherlv_34= 'authors' otherlv_35= '(' ( ( ruleEString ) ) (otherlv_37= ',' ( ( ruleEString ) ) )* otherlv_39= ')' (otherlv_40= 'relatedProjects' otherlv_41= '(' ( ( ruleEString ) ) (otherlv_43= ',' ( ( ruleEString ) ) )* otherlv_45= ')' )? otherlv_46= 'date' ( (lv_date_47_0= rulePublishedDate ) ) (otherlv_48= 'ISSN' otherlv_49= '{' ( (lv_ISSN_50_0= ruleSerialNumber ) ) (otherlv_51= ',' ( (lv_ISSN_52_0= ruleSerialNumber ) ) )* otherlv_53= '}' )? otherlv_54= '}'
            {
            // InternalTextualRCV.g:3106:3: ( (lv_openAccess_0_0= 'openAccess' ) )
            // InternalTextualRCV.g:3107:4: (lv_openAccess_0_0= 'openAccess' )
            {
            // InternalTextualRCV.g:3107:4: (lv_openAccess_0_0= 'openAccess' )
            // InternalTextualRCV.g:3108:5: lv_openAccess_0_0= 'openAccess'
            {
            lv_openAccess_0_0=(Token)match(input,73,FOLLOW_88); 

            					newLeafNode(lv_openAccess_0_0, grammarAccess.getJournalAccess().getOpenAccessOpenAccessKeyword_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getJournalRule());
            					}
            					setWithLastConsumed(current, "openAccess", lv_openAccess_0_0 != null, "openAccess");
            				

            }


            }

            otherlv_1=(Token)match(input,74,FOLLOW_20); 

            			newLeafNode(otherlv_1, grammarAccess.getJournalAccess().getJournalKeyword_1());
            		
            // InternalTextualRCV.g:3124:3: ( (lv_citekey_2_0= ruleEString ) )
            // InternalTextualRCV.g:3125:4: (lv_citekey_2_0= ruleEString )
            {
            // InternalTextualRCV.g:3125:4: (lv_citekey_2_0= ruleEString )
            // InternalTextualRCV.g:3126:5: lv_citekey_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getJournalAccess().getCitekeyEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_3);
            lv_citekey_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getJournalRule());
            					}
            					set(
            						current,
            						"citekey",
            						lv_citekey_2_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,16,FOLLOW_58); 

            			newLeafNode(otherlv_3, grammarAccess.getJournalAccess().getLeftCurlyBracketKeyword_3());
            		
            otherlv_4=(Token)match(input,49,FOLLOW_20); 

            			newLeafNode(otherlv_4, grammarAccess.getJournalAccess().getTitleKeyword_4());
            		
            // InternalTextualRCV.g:3151:3: ( (lv_title_5_0= ruleEString ) )
            // InternalTextualRCV.g:3152:4: (lv_title_5_0= ruleEString )
            {
            // InternalTextualRCV.g:3152:4: (lv_title_5_0= ruleEString )
            // InternalTextualRCV.g:3153:5: lv_title_5_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getJournalAccess().getTitleEStringParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_89);
            lv_title_5_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getJournalRule());
            					}
            					set(
            						current,
            						"title",
            						lv_title_5_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:3170:3: (otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) ) )?
            int alt64=2;
            int LA64_0 = input.LA(1);

            if ( (LA64_0==47) ) {
                alt64=1;
            }
            switch (alt64) {
                case 1 :
                    // InternalTextualRCV.g:3171:4: otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) )
                    {
                    otherlv_6=(Token)match(input,47,FOLLOW_20); 

                    				newLeafNode(otherlv_6, grammarAccess.getJournalAccess().getURLKeyword_6_0());
                    			
                    // InternalTextualRCV.g:3175:4: ( (lv_URL_7_0= ruleEString ) )
                    // InternalTextualRCV.g:3176:5: (lv_URL_7_0= ruleEString )
                    {
                    // InternalTextualRCV.g:3176:5: (lv_URL_7_0= ruleEString )
                    // InternalTextualRCV.g:3177:6: lv_URL_7_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getJournalAccess().getURLEStringParserRuleCall_6_1_0());
                    					
                    pushFollow(FOLLOW_90);
                    lv_URL_7_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getJournalRule());
                    						}
                    						set(
                    							current,
                    							"URL",
                    							lv_URL_7_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_8=(Token)match(input,75,FOLLOW_91); 

            			newLeafNode(otherlv_8, grammarAccess.getJournalAccess().getPublishedKeyword_7());
            		
            // InternalTextualRCV.g:3199:3: ( (lv_published_9_0= ruleEBoolean ) )
            // InternalTextualRCV.g:3200:4: (lv_published_9_0= ruleEBoolean )
            {
            // InternalTextualRCV.g:3200:4: (lv_published_9_0= ruleEBoolean )
            // InternalTextualRCV.g:3201:5: lv_published_9_0= ruleEBoolean
            {

            					newCompositeNode(grammarAccess.getJournalAccess().getPublishedEBooleanParserRuleCall_8_0());
            				
            pushFollow(FOLLOW_92);
            lv_published_9_0=ruleEBoolean();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getJournalRule());
            					}
            					set(
            						current,
            						"published",
            						lv_published_9_0,
            						"org.montex.researchcv.xtext.TextualRCV.EBoolean");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:3218:3: (otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) ) )?
            int alt65=2;
            int LA65_0 = input.LA(1);

            if ( (LA65_0==76) ) {
                alt65=1;
            }
            switch (alt65) {
                case 1 :
                    // InternalTextualRCV.g:3219:4: otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) )
                    {
                    otherlv_10=(Token)match(input,76,FOLLOW_20); 

                    				newLeafNode(otherlv_10, grammarAccess.getJournalAccess().getDOIKeyword_9_0());
                    			
                    // InternalTextualRCV.g:3223:4: ( (lv_DOI_11_0= ruleEString ) )
                    // InternalTextualRCV.g:3224:5: (lv_DOI_11_0= ruleEString )
                    {
                    // InternalTextualRCV.g:3224:5: (lv_DOI_11_0= ruleEString )
                    // InternalTextualRCV.g:3225:6: lv_DOI_11_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getJournalAccess().getDOIEStringParserRuleCall_9_1_0());
                    					
                    pushFollow(FOLLOW_93);
                    lv_DOI_11_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getJournalRule());
                    						}
                    						set(
                    							current,
                    							"DOI",
                    							lv_DOI_11_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:3243:3: (otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) ) )?
            int alt66=2;
            int LA66_0 = input.LA(1);

            if ( (LA66_0==77) ) {
                alt66=1;
            }
            switch (alt66) {
                case 1 :
                    // InternalTextualRCV.g:3244:4: otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) )
                    {
                    otherlv_12=(Token)match(input,77,FOLLOW_20); 

                    				newLeafNode(otherlv_12, grammarAccess.getJournalAccess().getAbstractKeyword_10_0());
                    			
                    // InternalTextualRCV.g:3248:4: ( (lv_abstract_13_0= ruleEString ) )
                    // InternalTextualRCV.g:3249:5: (lv_abstract_13_0= ruleEString )
                    {
                    // InternalTextualRCV.g:3249:5: (lv_abstract_13_0= ruleEString )
                    // InternalTextualRCV.g:3250:6: lv_abstract_13_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getJournalAccess().getAbstractEStringParserRuleCall_10_1_0());
                    					
                    pushFollow(FOLLOW_94);
                    lv_abstract_13_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getJournalRule());
                    						}
                    						set(
                    							current,
                    							"abstract",
                    							lv_abstract_13_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_14=(Token)match(input,78,FOLLOW_91); 

            			newLeafNode(otherlv_14, grammarAccess.getJournalAccess().getWithAuthorVersionKeyword_11());
            		
            // InternalTextualRCV.g:3272:3: ( (lv_withAuthorVersion_15_0= ruleEBoolean ) )
            // InternalTextualRCV.g:3273:4: (lv_withAuthorVersion_15_0= ruleEBoolean )
            {
            // InternalTextualRCV.g:3273:4: (lv_withAuthorVersion_15_0= ruleEBoolean )
            // InternalTextualRCV.g:3274:5: lv_withAuthorVersion_15_0= ruleEBoolean
            {

            					newCompositeNode(grammarAccess.getJournalAccess().getWithAuthorVersionEBooleanParserRuleCall_12_0());
            				
            pushFollow(FOLLOW_95);
            lv_withAuthorVersion_15_0=ruleEBoolean();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getJournalRule());
            					}
            					set(
            						current,
            						"withAuthorVersion",
            						lv_withAuthorVersion_15_0,
            						"org.montex.researchcv.xtext.TextualRCV.EBoolean");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:3291:3: (otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}' )?
            int alt68=2;
            int LA68_0 = input.LA(1);

            if ( (LA68_0==79) ) {
                alt68=1;
            }
            switch (alt68) {
                case 1 :
                    // InternalTextualRCV.g:3292:4: otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}'
                    {
                    otherlv_16=(Token)match(input,79,FOLLOW_3); 

                    				newLeafNode(otherlv_16, grammarAccess.getJournalAccess().getNotesKeyword_13_0());
                    			
                    otherlv_17=(Token)match(input,16,FOLLOW_20); 

                    				newLeafNode(otherlv_17, grammarAccess.getJournalAccess().getLeftCurlyBracketKeyword_13_1());
                    			
                    // InternalTextualRCV.g:3300:4: ( (lv_notes_18_0= ruleEString ) )
                    // InternalTextualRCV.g:3301:5: (lv_notes_18_0= ruleEString )
                    {
                    // InternalTextualRCV.g:3301:5: (lv_notes_18_0= ruleEString )
                    // InternalTextualRCV.g:3302:6: lv_notes_18_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getJournalAccess().getNotesEStringParserRuleCall_13_2_0());
                    					
                    pushFollow(FOLLOW_40);
                    lv_notes_18_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getJournalRule());
                    						}
                    						add(
                    							current,
                    							"notes",
                    							lv_notes_18_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:3319:4: (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )*
                    loop67:
                    do {
                        int alt67=2;
                        int LA67_0 = input.LA(1);

                        if ( (LA67_0==24) ) {
                            alt67=1;
                        }


                        switch (alt67) {
                    	case 1 :
                    	    // InternalTextualRCV.g:3320:5: otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) )
                    	    {
                    	    otherlv_19=(Token)match(input,24,FOLLOW_20); 

                    	    					newLeafNode(otherlv_19, grammarAccess.getJournalAccess().getCommaKeyword_13_3_0());
                    	    				
                    	    // InternalTextualRCV.g:3324:5: ( (lv_notes_20_0= ruleEString ) )
                    	    // InternalTextualRCV.g:3325:6: (lv_notes_20_0= ruleEString )
                    	    {
                    	    // InternalTextualRCV.g:3325:6: (lv_notes_20_0= ruleEString )
                    	    // InternalTextualRCV.g:3326:7: lv_notes_20_0= ruleEString
                    	    {

                    	    							newCompositeNode(grammarAccess.getJournalAccess().getNotesEStringParserRuleCall_13_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_40);
                    	    lv_notes_20_0=ruleEString();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getJournalRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"notes",
                    	    								lv_notes_20_0,
                    	    								"org.montex.researchcv.xtext.TextualRCV.EString");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop67;
                        }
                    } while (true);

                    otherlv_21=(Token)match(input,18,FOLLOW_96); 

                    				newLeafNode(otherlv_21, grammarAccess.getJournalAccess().getRightCurlyBracketKeyword_13_4());
                    			

                    }
                    break;

            }

            otherlv_22=(Token)match(input,80,FOLLOW_20); 

            			newLeafNode(otherlv_22, grammarAccess.getJournalAccess().getJournalNameKeyword_14());
            		
            // InternalTextualRCV.g:3353:3: ( (lv_journalName_23_0= ruleEString ) )
            // InternalTextualRCV.g:3354:4: (lv_journalName_23_0= ruleEString )
            {
            // InternalTextualRCV.g:3354:4: (lv_journalName_23_0= ruleEString )
            // InternalTextualRCV.g:3355:5: lv_journalName_23_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getJournalAccess().getJournalNameEStringParserRuleCall_15_0());
            				
            pushFollow(FOLLOW_97);
            lv_journalName_23_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getJournalRule());
            					}
            					set(
            						current,
            						"journalName",
            						lv_journalName_23_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:3372:3: (otherlv_24= 'publisher' ( (lv_publisher_25_0= ruleEString ) ) )?
            int alt69=2;
            int LA69_0 = input.LA(1);

            if ( (LA69_0==81) ) {
                alt69=1;
            }
            switch (alt69) {
                case 1 :
                    // InternalTextualRCV.g:3373:4: otherlv_24= 'publisher' ( (lv_publisher_25_0= ruleEString ) )
                    {
                    otherlv_24=(Token)match(input,81,FOLLOW_20); 

                    				newLeafNode(otherlv_24, grammarAccess.getJournalAccess().getPublisherKeyword_16_0());
                    			
                    // InternalTextualRCV.g:3377:4: ( (lv_publisher_25_0= ruleEString ) )
                    // InternalTextualRCV.g:3378:5: (lv_publisher_25_0= ruleEString )
                    {
                    // InternalTextualRCV.g:3378:5: (lv_publisher_25_0= ruleEString )
                    // InternalTextualRCV.g:3379:6: lv_publisher_25_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getJournalAccess().getPublisherEStringParserRuleCall_16_1_0());
                    					
                    pushFollow(FOLLOW_98);
                    lv_publisher_25_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getJournalRule());
                    						}
                    						set(
                    							current,
                    							"publisher",
                    							lv_publisher_25_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:3397:3: (otherlv_26= 'firstPage' ( (lv_firstPage_27_0= ruleEString ) ) )?
            int alt70=2;
            int LA70_0 = input.LA(1);

            if ( (LA70_0==82) ) {
                alt70=1;
            }
            switch (alt70) {
                case 1 :
                    // InternalTextualRCV.g:3398:4: otherlv_26= 'firstPage' ( (lv_firstPage_27_0= ruleEString ) )
                    {
                    otherlv_26=(Token)match(input,82,FOLLOW_20); 

                    				newLeafNode(otherlv_26, grammarAccess.getJournalAccess().getFirstPageKeyword_17_0());
                    			
                    // InternalTextualRCV.g:3402:4: ( (lv_firstPage_27_0= ruleEString ) )
                    // InternalTextualRCV.g:3403:5: (lv_firstPage_27_0= ruleEString )
                    {
                    // InternalTextualRCV.g:3403:5: (lv_firstPage_27_0= ruleEString )
                    // InternalTextualRCV.g:3404:6: lv_firstPage_27_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getJournalAccess().getFirstPageEStringParserRuleCall_17_1_0());
                    					
                    pushFollow(FOLLOW_99);
                    lv_firstPage_27_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getJournalRule());
                    						}
                    						set(
                    							current,
                    							"firstPage",
                    							lv_firstPage_27_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:3422:3: (otherlv_28= 'lastPage' ( (lv_lastPage_29_0= ruleEString ) ) )?
            int alt71=2;
            int LA71_0 = input.LA(1);

            if ( (LA71_0==83) ) {
                alt71=1;
            }
            switch (alt71) {
                case 1 :
                    // InternalTextualRCV.g:3423:4: otherlv_28= 'lastPage' ( (lv_lastPage_29_0= ruleEString ) )
                    {
                    otherlv_28=(Token)match(input,83,FOLLOW_20); 

                    				newLeafNode(otherlv_28, grammarAccess.getJournalAccess().getLastPageKeyword_18_0());
                    			
                    // InternalTextualRCV.g:3427:4: ( (lv_lastPage_29_0= ruleEString ) )
                    // InternalTextualRCV.g:3428:5: (lv_lastPage_29_0= ruleEString )
                    {
                    // InternalTextualRCV.g:3428:5: (lv_lastPage_29_0= ruleEString )
                    // InternalTextualRCV.g:3429:6: lv_lastPage_29_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getJournalAccess().getLastPageEStringParserRuleCall_18_1_0());
                    					
                    pushFollow(FOLLOW_100);
                    lv_lastPage_29_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getJournalRule());
                    						}
                    						set(
                    							current,
                    							"lastPage",
                    							lv_lastPage_29_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_30=(Token)match(input,84,FOLLOW_9); 

            			newLeafNode(otherlv_30, grammarAccess.getJournalAccess().getVolumeKeyword_19());
            		
            // InternalTextualRCV.g:3451:3: ( (lv_volume_31_0= ruleEInt ) )
            // InternalTextualRCV.g:3452:4: (lv_volume_31_0= ruleEInt )
            {
            // InternalTextualRCV.g:3452:4: (lv_volume_31_0= ruleEInt )
            // InternalTextualRCV.g:3453:5: lv_volume_31_0= ruleEInt
            {

            					newCompositeNode(grammarAccess.getJournalAccess().getVolumeEIntParserRuleCall_20_0());
            				
            pushFollow(FOLLOW_101);
            lv_volume_31_0=ruleEInt();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getJournalRule());
            					}
            					set(
            						current,
            						"volume",
            						lv_volume_31_0,
            						"org.montex.researchcv.xtext.TextualRCV.EInt");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_32=(Token)match(input,85,FOLLOW_9); 

            			newLeafNode(otherlv_32, grammarAccess.getJournalAccess().getIssueKeyword_21());
            		
            // InternalTextualRCV.g:3474:3: ( (lv_issue_33_0= ruleEInt ) )
            // InternalTextualRCV.g:3475:4: (lv_issue_33_0= ruleEInt )
            {
            // InternalTextualRCV.g:3475:4: (lv_issue_33_0= ruleEInt )
            // InternalTextualRCV.g:3476:5: lv_issue_33_0= ruleEInt
            {

            					newCompositeNode(grammarAccess.getJournalAccess().getIssueEIntParserRuleCall_22_0());
            				
            pushFollow(FOLLOW_102);
            lv_issue_33_0=ruleEInt();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getJournalRule());
            					}
            					set(
            						current,
            						"issue",
            						lv_issue_33_0,
            						"org.montex.researchcv.xtext.TextualRCV.EInt");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_34=(Token)match(input,86,FOLLOW_61); 

            			newLeafNode(otherlv_34, grammarAccess.getJournalAccess().getAuthorsKeyword_23());
            		
            otherlv_35=(Token)match(input,52,FOLLOW_20); 

            			newLeafNode(otherlv_35, grammarAccess.getJournalAccess().getLeftParenthesisKeyword_24());
            		
            // InternalTextualRCV.g:3501:3: ( ( ruleEString ) )
            // InternalTextualRCV.g:3502:4: ( ruleEString )
            {
            // InternalTextualRCV.g:3502:4: ( ruleEString )
            // InternalTextualRCV.g:3503:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getJournalRule());
            					}
            				

            					newCompositeNode(grammarAccess.getJournalAccess().getAuthorsPersonCrossReference_25_0());
            				
            pushFollow(FOLLOW_62);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:3517:3: (otherlv_37= ',' ( ( ruleEString ) ) )*
            loop72:
            do {
                int alt72=2;
                int LA72_0 = input.LA(1);

                if ( (LA72_0==24) ) {
                    alt72=1;
                }


                switch (alt72) {
            	case 1 :
            	    // InternalTextualRCV.g:3518:4: otherlv_37= ',' ( ( ruleEString ) )
            	    {
            	    otherlv_37=(Token)match(input,24,FOLLOW_20); 

            	    				newLeafNode(otherlv_37, grammarAccess.getJournalAccess().getCommaKeyword_26_0());
            	    			
            	    // InternalTextualRCV.g:3522:4: ( ( ruleEString ) )
            	    // InternalTextualRCV.g:3523:5: ( ruleEString )
            	    {
            	    // InternalTextualRCV.g:3523:5: ( ruleEString )
            	    // InternalTextualRCV.g:3524:6: ruleEString
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getJournalRule());
            	    						}
            	    					

            	    						newCompositeNode(grammarAccess.getJournalAccess().getAuthorsPersonCrossReference_26_1_0());
            	    					
            	    pushFollow(FOLLOW_62);
            	    ruleEString();

            	    state._fsp--;


            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop72;
                }
            } while (true);

            otherlv_39=(Token)match(input,53,FOLLOW_103); 

            			newLeafNode(otherlv_39, grammarAccess.getJournalAccess().getRightParenthesisKeyword_27());
            		
            // InternalTextualRCV.g:3543:3: (otherlv_40= 'relatedProjects' otherlv_41= '(' ( ( ruleEString ) ) (otherlv_43= ',' ( ( ruleEString ) ) )* otherlv_45= ')' )?
            int alt74=2;
            int LA74_0 = input.LA(1);

            if ( (LA74_0==87) ) {
                alt74=1;
            }
            switch (alt74) {
                case 1 :
                    // InternalTextualRCV.g:3544:4: otherlv_40= 'relatedProjects' otherlv_41= '(' ( ( ruleEString ) ) (otherlv_43= ',' ( ( ruleEString ) ) )* otherlv_45= ')'
                    {
                    otherlv_40=(Token)match(input,87,FOLLOW_61); 

                    				newLeafNode(otherlv_40, grammarAccess.getJournalAccess().getRelatedProjectsKeyword_28_0());
                    			
                    otherlv_41=(Token)match(input,52,FOLLOW_20); 

                    				newLeafNode(otherlv_41, grammarAccess.getJournalAccess().getLeftParenthesisKeyword_28_1());
                    			
                    // InternalTextualRCV.g:3552:4: ( ( ruleEString ) )
                    // InternalTextualRCV.g:3553:5: ( ruleEString )
                    {
                    // InternalTextualRCV.g:3553:5: ( ruleEString )
                    // InternalTextualRCV.g:3554:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getJournalRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getJournalAccess().getRelatedProjectsGrantCrossReference_28_2_0());
                    					
                    pushFollow(FOLLOW_62);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:3568:4: (otherlv_43= ',' ( ( ruleEString ) ) )*
                    loop73:
                    do {
                        int alt73=2;
                        int LA73_0 = input.LA(1);

                        if ( (LA73_0==24) ) {
                            alt73=1;
                        }


                        switch (alt73) {
                    	case 1 :
                    	    // InternalTextualRCV.g:3569:5: otherlv_43= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_43=(Token)match(input,24,FOLLOW_20); 

                    	    					newLeafNode(otherlv_43, grammarAccess.getJournalAccess().getCommaKeyword_28_3_0());
                    	    				
                    	    // InternalTextualRCV.g:3573:5: ( ( ruleEString ) )
                    	    // InternalTextualRCV.g:3574:6: ( ruleEString )
                    	    {
                    	    // InternalTextualRCV.g:3574:6: ( ruleEString )
                    	    // InternalTextualRCV.g:3575:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getJournalRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getJournalAccess().getRelatedProjectsGrantCrossReference_28_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_62);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop73;
                        }
                    } while (true);

                    otherlv_45=(Token)match(input,53,FOLLOW_104); 

                    				newLeafNode(otherlv_45, grammarAccess.getJournalAccess().getRightParenthesisKeyword_28_4());
                    			

                    }
                    break;

            }

            otherlv_46=(Token)match(input,88,FOLLOW_9); 

            			newLeafNode(otherlv_46, grammarAccess.getJournalAccess().getDateKeyword_29());
            		
            // InternalTextualRCV.g:3599:3: ( (lv_date_47_0= rulePublishedDate ) )
            // InternalTextualRCV.g:3600:4: (lv_date_47_0= rulePublishedDate )
            {
            // InternalTextualRCV.g:3600:4: (lv_date_47_0= rulePublishedDate )
            // InternalTextualRCV.g:3601:5: lv_date_47_0= rulePublishedDate
            {

            					newCompositeNode(grammarAccess.getJournalAccess().getDatePublishedDateParserRuleCall_30_0());
            				
            pushFollow(FOLLOW_105);
            lv_date_47_0=rulePublishedDate();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getJournalRule());
            					}
            					set(
            						current,
            						"date",
            						lv_date_47_0,
            						"org.montex.researchcv.xtext.TextualRCV.PublishedDate");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:3618:3: (otherlv_48= 'ISSN' otherlv_49= '{' ( (lv_ISSN_50_0= ruleSerialNumber ) ) (otherlv_51= ',' ( (lv_ISSN_52_0= ruleSerialNumber ) ) )* otherlv_53= '}' )?
            int alt76=2;
            int LA76_0 = input.LA(1);

            if ( (LA76_0==89) ) {
                alt76=1;
            }
            switch (alt76) {
                case 1 :
                    // InternalTextualRCV.g:3619:4: otherlv_48= 'ISSN' otherlv_49= '{' ( (lv_ISSN_50_0= ruleSerialNumber ) ) (otherlv_51= ',' ( (lv_ISSN_52_0= ruleSerialNumber ) ) )* otherlv_53= '}'
                    {
                    otherlv_48=(Token)match(input,89,FOLLOW_3); 

                    				newLeafNode(otherlv_48, grammarAccess.getJournalAccess().getISSNKeyword_31_0());
                    			
                    otherlv_49=(Token)match(input,16,FOLLOW_106); 

                    				newLeafNode(otherlv_49, grammarAccess.getJournalAccess().getLeftCurlyBracketKeyword_31_1());
                    			
                    // InternalTextualRCV.g:3627:4: ( (lv_ISSN_50_0= ruleSerialNumber ) )
                    // InternalTextualRCV.g:3628:5: (lv_ISSN_50_0= ruleSerialNumber )
                    {
                    // InternalTextualRCV.g:3628:5: (lv_ISSN_50_0= ruleSerialNumber )
                    // InternalTextualRCV.g:3629:6: lv_ISSN_50_0= ruleSerialNumber
                    {

                    						newCompositeNode(grammarAccess.getJournalAccess().getISSNSerialNumberParserRuleCall_31_2_0());
                    					
                    pushFollow(FOLLOW_40);
                    lv_ISSN_50_0=ruleSerialNumber();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getJournalRule());
                    						}
                    						add(
                    							current,
                    							"ISSN",
                    							lv_ISSN_50_0,
                    							"org.montex.researchcv.xtext.TextualRCV.SerialNumber");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:3646:4: (otherlv_51= ',' ( (lv_ISSN_52_0= ruleSerialNumber ) ) )*
                    loop75:
                    do {
                        int alt75=2;
                        int LA75_0 = input.LA(1);

                        if ( (LA75_0==24) ) {
                            alt75=1;
                        }


                        switch (alt75) {
                    	case 1 :
                    	    // InternalTextualRCV.g:3647:5: otherlv_51= ',' ( (lv_ISSN_52_0= ruleSerialNumber ) )
                    	    {
                    	    otherlv_51=(Token)match(input,24,FOLLOW_106); 

                    	    					newLeafNode(otherlv_51, grammarAccess.getJournalAccess().getCommaKeyword_31_3_0());
                    	    				
                    	    // InternalTextualRCV.g:3651:5: ( (lv_ISSN_52_0= ruleSerialNumber ) )
                    	    // InternalTextualRCV.g:3652:6: (lv_ISSN_52_0= ruleSerialNumber )
                    	    {
                    	    // InternalTextualRCV.g:3652:6: (lv_ISSN_52_0= ruleSerialNumber )
                    	    // InternalTextualRCV.g:3653:7: lv_ISSN_52_0= ruleSerialNumber
                    	    {

                    	    							newCompositeNode(grammarAccess.getJournalAccess().getISSNSerialNumberParserRuleCall_31_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_40);
                    	    lv_ISSN_52_0=ruleSerialNumber();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getJournalRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"ISSN",
                    	    								lv_ISSN_52_0,
                    	    								"org.montex.researchcv.xtext.TextualRCV.SerialNumber");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop75;
                        }
                    } while (true);

                    otherlv_53=(Token)match(input,18,FOLLOW_27); 

                    				newLeafNode(otherlv_53, grammarAccess.getJournalAccess().getRightCurlyBracketKeyword_31_4());
                    			

                    }
                    break;

            }

            otherlv_54=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_54, grammarAccess.getJournalAccess().getRightCurlyBracketKeyword_32());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleJournal"


    // $ANTLR start "entryRuleConference"
    // InternalTextualRCV.g:3684:1: entryRuleConference returns [EObject current=null] : iv_ruleConference= ruleConference EOF ;
    public final EObject entryRuleConference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConference = null;


        try {
            // InternalTextualRCV.g:3684:51: (iv_ruleConference= ruleConference EOF )
            // InternalTextualRCV.g:3685:2: iv_ruleConference= ruleConference EOF
            {
             newCompositeNode(grammarAccess.getConferenceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleConference=ruleConference();

            state._fsp--;

             current =iv_ruleConference; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConference"


    // $ANTLR start "ruleConference"
    // InternalTextualRCV.g:3691:1: ruleConference returns [EObject current=null] : ( ( (lv_openAccess_0_0= 'openAccess' ) ) ( (lv_shortPaper_1_0= 'shortPaper' ) ) ( (lv_toolPaper_2_0= 'toolPaper' ) ) ( (lv_invitedPaper_3_0= 'invitedPaper' ) ) otherlv_4= 'Conference' ( (lv_citekey_5_0= ruleEString ) ) otherlv_6= '{' otherlv_7= 'title' ( (lv_title_8_0= ruleEString ) ) (otherlv_9= 'URL' ( (lv_URL_10_0= ruleEString ) ) )? otherlv_11= 'published' ( (lv_published_12_0= ruleEBoolean ) ) (otherlv_13= 'DOI' ( (lv_DOI_14_0= ruleEString ) ) )? (otherlv_15= 'abstract' ( (lv_abstract_16_0= ruleEString ) ) )? otherlv_17= 'withAuthorVersion' ( (lv_withAuthorVersion_18_0= ruleEBoolean ) ) (otherlv_19= 'notes' otherlv_20= '{' ( (lv_notes_21_0= ruleEString ) ) (otherlv_22= ',' ( (lv_notes_23_0= ruleEString ) ) )* otherlv_24= '}' )? otherlv_25= 'booktitle' ( (lv_booktitle_26_0= ruleEString ) ) (otherlv_27= 'publisher' ( (lv_publisher_28_0= ruleEString ) ) )? (otherlv_29= 'series' ( (lv_series_30_0= ruleEString ) ) )? (otherlv_31= 'volume' ( (lv_volume_32_0= ruleEString ) ) )? (otherlv_33= 'firstPage' ( (lv_firstPage_34_0= ruleEString ) ) )? (otherlv_35= 'lastPage' ( (lv_lastPage_36_0= ruleEString ) ) )? otherlv_37= 'eventName' ( (lv_eventName_38_0= ruleEString ) ) (otherlv_39= 'venue' ( (lv_venue_40_0= ruleEString ) ) )? otherlv_41= 'eventShortName' ( (lv_eventShortName_42_0= ruleEString ) ) (otherlv_43= 'trackName' ( (lv_trackName_44_0= ruleEString ) ) )? (otherlv_45= 'trackShortName' ( (lv_trackShortName_46_0= ruleEString ) ) )? otherlv_47= 'authors' otherlv_48= '(' ( ( ruleEString ) ) (otherlv_50= ',' ( ( ruleEString ) ) )* otherlv_52= ')' (otherlv_53= 'relatedProjects' otherlv_54= '(' ( ( ruleEString ) ) (otherlv_56= ',' ( ( ruleEString ) ) )* otherlv_58= ')' )? (otherlv_59= 'bookeditors' otherlv_60= '(' ( ( ruleEString ) ) (otherlv_62= ',' ( ( ruleEString ) ) )* otherlv_64= ')' )? otherlv_65= 'date' ( (lv_date_66_0= rulePublishedDate ) ) (otherlv_67= 'ISBN' otherlv_68= '{' ( (lv_ISBN_69_0= ruleSerialNumber ) ) (otherlv_70= ',' ( (lv_ISBN_71_0= ruleSerialNumber ) ) )* otherlv_72= '}' )? (otherlv_73= 'eventEndDate' ( (lv_eventEndDate_74_0= rulePublishedDate ) ) )? otherlv_75= '}' ) ;
    public final EObject ruleConference() throws RecognitionException {
        EObject current = null;

        Token lv_openAccess_0_0=null;
        Token lv_shortPaper_1_0=null;
        Token lv_toolPaper_2_0=null;
        Token lv_invitedPaper_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_20=null;
        Token otherlv_22=null;
        Token otherlv_24=null;
        Token otherlv_25=null;
        Token otherlv_27=null;
        Token otherlv_29=null;
        Token otherlv_31=null;
        Token otherlv_33=null;
        Token otherlv_35=null;
        Token otherlv_37=null;
        Token otherlv_39=null;
        Token otherlv_41=null;
        Token otherlv_43=null;
        Token otherlv_45=null;
        Token otherlv_47=null;
        Token otherlv_48=null;
        Token otherlv_50=null;
        Token otherlv_52=null;
        Token otherlv_53=null;
        Token otherlv_54=null;
        Token otherlv_56=null;
        Token otherlv_58=null;
        Token otherlv_59=null;
        Token otherlv_60=null;
        Token otherlv_62=null;
        Token otherlv_64=null;
        Token otherlv_65=null;
        Token otherlv_67=null;
        Token otherlv_68=null;
        Token otherlv_70=null;
        Token otherlv_72=null;
        Token otherlv_73=null;
        Token otherlv_75=null;
        AntlrDatatypeRuleToken lv_citekey_5_0 = null;

        AntlrDatatypeRuleToken lv_title_8_0 = null;

        AntlrDatatypeRuleToken lv_URL_10_0 = null;

        AntlrDatatypeRuleToken lv_published_12_0 = null;

        AntlrDatatypeRuleToken lv_DOI_14_0 = null;

        AntlrDatatypeRuleToken lv_abstract_16_0 = null;

        AntlrDatatypeRuleToken lv_withAuthorVersion_18_0 = null;

        AntlrDatatypeRuleToken lv_notes_21_0 = null;

        AntlrDatatypeRuleToken lv_notes_23_0 = null;

        AntlrDatatypeRuleToken lv_booktitle_26_0 = null;

        AntlrDatatypeRuleToken lv_publisher_28_0 = null;

        AntlrDatatypeRuleToken lv_series_30_0 = null;

        AntlrDatatypeRuleToken lv_volume_32_0 = null;

        AntlrDatatypeRuleToken lv_firstPage_34_0 = null;

        AntlrDatatypeRuleToken lv_lastPage_36_0 = null;

        AntlrDatatypeRuleToken lv_eventName_38_0 = null;

        AntlrDatatypeRuleToken lv_venue_40_0 = null;

        AntlrDatatypeRuleToken lv_eventShortName_42_0 = null;

        AntlrDatatypeRuleToken lv_trackName_44_0 = null;

        AntlrDatatypeRuleToken lv_trackShortName_46_0 = null;

        EObject lv_date_66_0 = null;

        EObject lv_ISBN_69_0 = null;

        EObject lv_ISBN_71_0 = null;

        EObject lv_eventEndDate_74_0 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:3697:2: ( ( ( (lv_openAccess_0_0= 'openAccess' ) ) ( (lv_shortPaper_1_0= 'shortPaper' ) ) ( (lv_toolPaper_2_0= 'toolPaper' ) ) ( (lv_invitedPaper_3_0= 'invitedPaper' ) ) otherlv_4= 'Conference' ( (lv_citekey_5_0= ruleEString ) ) otherlv_6= '{' otherlv_7= 'title' ( (lv_title_8_0= ruleEString ) ) (otherlv_9= 'URL' ( (lv_URL_10_0= ruleEString ) ) )? otherlv_11= 'published' ( (lv_published_12_0= ruleEBoolean ) ) (otherlv_13= 'DOI' ( (lv_DOI_14_0= ruleEString ) ) )? (otherlv_15= 'abstract' ( (lv_abstract_16_0= ruleEString ) ) )? otherlv_17= 'withAuthorVersion' ( (lv_withAuthorVersion_18_0= ruleEBoolean ) ) (otherlv_19= 'notes' otherlv_20= '{' ( (lv_notes_21_0= ruleEString ) ) (otherlv_22= ',' ( (lv_notes_23_0= ruleEString ) ) )* otherlv_24= '}' )? otherlv_25= 'booktitle' ( (lv_booktitle_26_0= ruleEString ) ) (otherlv_27= 'publisher' ( (lv_publisher_28_0= ruleEString ) ) )? (otherlv_29= 'series' ( (lv_series_30_0= ruleEString ) ) )? (otherlv_31= 'volume' ( (lv_volume_32_0= ruleEString ) ) )? (otherlv_33= 'firstPage' ( (lv_firstPage_34_0= ruleEString ) ) )? (otherlv_35= 'lastPage' ( (lv_lastPage_36_0= ruleEString ) ) )? otherlv_37= 'eventName' ( (lv_eventName_38_0= ruleEString ) ) (otherlv_39= 'venue' ( (lv_venue_40_0= ruleEString ) ) )? otherlv_41= 'eventShortName' ( (lv_eventShortName_42_0= ruleEString ) ) (otherlv_43= 'trackName' ( (lv_trackName_44_0= ruleEString ) ) )? (otherlv_45= 'trackShortName' ( (lv_trackShortName_46_0= ruleEString ) ) )? otherlv_47= 'authors' otherlv_48= '(' ( ( ruleEString ) ) (otherlv_50= ',' ( ( ruleEString ) ) )* otherlv_52= ')' (otherlv_53= 'relatedProjects' otherlv_54= '(' ( ( ruleEString ) ) (otherlv_56= ',' ( ( ruleEString ) ) )* otherlv_58= ')' )? (otherlv_59= 'bookeditors' otherlv_60= '(' ( ( ruleEString ) ) (otherlv_62= ',' ( ( ruleEString ) ) )* otherlv_64= ')' )? otherlv_65= 'date' ( (lv_date_66_0= rulePublishedDate ) ) (otherlv_67= 'ISBN' otherlv_68= '{' ( (lv_ISBN_69_0= ruleSerialNumber ) ) (otherlv_70= ',' ( (lv_ISBN_71_0= ruleSerialNumber ) ) )* otherlv_72= '}' )? (otherlv_73= 'eventEndDate' ( (lv_eventEndDate_74_0= rulePublishedDate ) ) )? otherlv_75= '}' ) )
            // InternalTextualRCV.g:3698:2: ( ( (lv_openAccess_0_0= 'openAccess' ) ) ( (lv_shortPaper_1_0= 'shortPaper' ) ) ( (lv_toolPaper_2_0= 'toolPaper' ) ) ( (lv_invitedPaper_3_0= 'invitedPaper' ) ) otherlv_4= 'Conference' ( (lv_citekey_5_0= ruleEString ) ) otherlv_6= '{' otherlv_7= 'title' ( (lv_title_8_0= ruleEString ) ) (otherlv_9= 'URL' ( (lv_URL_10_0= ruleEString ) ) )? otherlv_11= 'published' ( (lv_published_12_0= ruleEBoolean ) ) (otherlv_13= 'DOI' ( (lv_DOI_14_0= ruleEString ) ) )? (otherlv_15= 'abstract' ( (lv_abstract_16_0= ruleEString ) ) )? otherlv_17= 'withAuthorVersion' ( (lv_withAuthorVersion_18_0= ruleEBoolean ) ) (otherlv_19= 'notes' otherlv_20= '{' ( (lv_notes_21_0= ruleEString ) ) (otherlv_22= ',' ( (lv_notes_23_0= ruleEString ) ) )* otherlv_24= '}' )? otherlv_25= 'booktitle' ( (lv_booktitle_26_0= ruleEString ) ) (otherlv_27= 'publisher' ( (lv_publisher_28_0= ruleEString ) ) )? (otherlv_29= 'series' ( (lv_series_30_0= ruleEString ) ) )? (otherlv_31= 'volume' ( (lv_volume_32_0= ruleEString ) ) )? (otherlv_33= 'firstPage' ( (lv_firstPage_34_0= ruleEString ) ) )? (otherlv_35= 'lastPage' ( (lv_lastPage_36_0= ruleEString ) ) )? otherlv_37= 'eventName' ( (lv_eventName_38_0= ruleEString ) ) (otherlv_39= 'venue' ( (lv_venue_40_0= ruleEString ) ) )? otherlv_41= 'eventShortName' ( (lv_eventShortName_42_0= ruleEString ) ) (otherlv_43= 'trackName' ( (lv_trackName_44_0= ruleEString ) ) )? (otherlv_45= 'trackShortName' ( (lv_trackShortName_46_0= ruleEString ) ) )? otherlv_47= 'authors' otherlv_48= '(' ( ( ruleEString ) ) (otherlv_50= ',' ( ( ruleEString ) ) )* otherlv_52= ')' (otherlv_53= 'relatedProjects' otherlv_54= '(' ( ( ruleEString ) ) (otherlv_56= ',' ( ( ruleEString ) ) )* otherlv_58= ')' )? (otherlv_59= 'bookeditors' otherlv_60= '(' ( ( ruleEString ) ) (otherlv_62= ',' ( ( ruleEString ) ) )* otherlv_64= ')' )? otherlv_65= 'date' ( (lv_date_66_0= rulePublishedDate ) ) (otherlv_67= 'ISBN' otherlv_68= '{' ( (lv_ISBN_69_0= ruleSerialNumber ) ) (otherlv_70= ',' ( (lv_ISBN_71_0= ruleSerialNumber ) ) )* otherlv_72= '}' )? (otherlv_73= 'eventEndDate' ( (lv_eventEndDate_74_0= rulePublishedDate ) ) )? otherlv_75= '}' )
            {
            // InternalTextualRCV.g:3698:2: ( ( (lv_openAccess_0_0= 'openAccess' ) ) ( (lv_shortPaper_1_0= 'shortPaper' ) ) ( (lv_toolPaper_2_0= 'toolPaper' ) ) ( (lv_invitedPaper_3_0= 'invitedPaper' ) ) otherlv_4= 'Conference' ( (lv_citekey_5_0= ruleEString ) ) otherlv_6= '{' otherlv_7= 'title' ( (lv_title_8_0= ruleEString ) ) (otherlv_9= 'URL' ( (lv_URL_10_0= ruleEString ) ) )? otherlv_11= 'published' ( (lv_published_12_0= ruleEBoolean ) ) (otherlv_13= 'DOI' ( (lv_DOI_14_0= ruleEString ) ) )? (otherlv_15= 'abstract' ( (lv_abstract_16_0= ruleEString ) ) )? otherlv_17= 'withAuthorVersion' ( (lv_withAuthorVersion_18_0= ruleEBoolean ) ) (otherlv_19= 'notes' otherlv_20= '{' ( (lv_notes_21_0= ruleEString ) ) (otherlv_22= ',' ( (lv_notes_23_0= ruleEString ) ) )* otherlv_24= '}' )? otherlv_25= 'booktitle' ( (lv_booktitle_26_0= ruleEString ) ) (otherlv_27= 'publisher' ( (lv_publisher_28_0= ruleEString ) ) )? (otherlv_29= 'series' ( (lv_series_30_0= ruleEString ) ) )? (otherlv_31= 'volume' ( (lv_volume_32_0= ruleEString ) ) )? (otherlv_33= 'firstPage' ( (lv_firstPage_34_0= ruleEString ) ) )? (otherlv_35= 'lastPage' ( (lv_lastPage_36_0= ruleEString ) ) )? otherlv_37= 'eventName' ( (lv_eventName_38_0= ruleEString ) ) (otherlv_39= 'venue' ( (lv_venue_40_0= ruleEString ) ) )? otherlv_41= 'eventShortName' ( (lv_eventShortName_42_0= ruleEString ) ) (otherlv_43= 'trackName' ( (lv_trackName_44_0= ruleEString ) ) )? (otherlv_45= 'trackShortName' ( (lv_trackShortName_46_0= ruleEString ) ) )? otherlv_47= 'authors' otherlv_48= '(' ( ( ruleEString ) ) (otherlv_50= ',' ( ( ruleEString ) ) )* otherlv_52= ')' (otherlv_53= 'relatedProjects' otherlv_54= '(' ( ( ruleEString ) ) (otherlv_56= ',' ( ( ruleEString ) ) )* otherlv_58= ')' )? (otherlv_59= 'bookeditors' otherlv_60= '(' ( ( ruleEString ) ) (otherlv_62= ',' ( ( ruleEString ) ) )* otherlv_64= ')' )? otherlv_65= 'date' ( (lv_date_66_0= rulePublishedDate ) ) (otherlv_67= 'ISBN' otherlv_68= '{' ( (lv_ISBN_69_0= ruleSerialNumber ) ) (otherlv_70= ',' ( (lv_ISBN_71_0= ruleSerialNumber ) ) )* otherlv_72= '}' )? (otherlv_73= 'eventEndDate' ( (lv_eventEndDate_74_0= rulePublishedDate ) ) )? otherlv_75= '}' )
            // InternalTextualRCV.g:3699:3: ( (lv_openAccess_0_0= 'openAccess' ) ) ( (lv_shortPaper_1_0= 'shortPaper' ) ) ( (lv_toolPaper_2_0= 'toolPaper' ) ) ( (lv_invitedPaper_3_0= 'invitedPaper' ) ) otherlv_4= 'Conference' ( (lv_citekey_5_0= ruleEString ) ) otherlv_6= '{' otherlv_7= 'title' ( (lv_title_8_0= ruleEString ) ) (otherlv_9= 'URL' ( (lv_URL_10_0= ruleEString ) ) )? otherlv_11= 'published' ( (lv_published_12_0= ruleEBoolean ) ) (otherlv_13= 'DOI' ( (lv_DOI_14_0= ruleEString ) ) )? (otherlv_15= 'abstract' ( (lv_abstract_16_0= ruleEString ) ) )? otherlv_17= 'withAuthorVersion' ( (lv_withAuthorVersion_18_0= ruleEBoolean ) ) (otherlv_19= 'notes' otherlv_20= '{' ( (lv_notes_21_0= ruleEString ) ) (otherlv_22= ',' ( (lv_notes_23_0= ruleEString ) ) )* otherlv_24= '}' )? otherlv_25= 'booktitle' ( (lv_booktitle_26_0= ruleEString ) ) (otherlv_27= 'publisher' ( (lv_publisher_28_0= ruleEString ) ) )? (otherlv_29= 'series' ( (lv_series_30_0= ruleEString ) ) )? (otherlv_31= 'volume' ( (lv_volume_32_0= ruleEString ) ) )? (otherlv_33= 'firstPage' ( (lv_firstPage_34_0= ruleEString ) ) )? (otherlv_35= 'lastPage' ( (lv_lastPage_36_0= ruleEString ) ) )? otherlv_37= 'eventName' ( (lv_eventName_38_0= ruleEString ) ) (otherlv_39= 'venue' ( (lv_venue_40_0= ruleEString ) ) )? otherlv_41= 'eventShortName' ( (lv_eventShortName_42_0= ruleEString ) ) (otherlv_43= 'trackName' ( (lv_trackName_44_0= ruleEString ) ) )? (otherlv_45= 'trackShortName' ( (lv_trackShortName_46_0= ruleEString ) ) )? otherlv_47= 'authors' otherlv_48= '(' ( ( ruleEString ) ) (otherlv_50= ',' ( ( ruleEString ) ) )* otherlv_52= ')' (otherlv_53= 'relatedProjects' otherlv_54= '(' ( ( ruleEString ) ) (otherlv_56= ',' ( ( ruleEString ) ) )* otherlv_58= ')' )? (otherlv_59= 'bookeditors' otherlv_60= '(' ( ( ruleEString ) ) (otherlv_62= ',' ( ( ruleEString ) ) )* otherlv_64= ')' )? otherlv_65= 'date' ( (lv_date_66_0= rulePublishedDate ) ) (otherlv_67= 'ISBN' otherlv_68= '{' ( (lv_ISBN_69_0= ruleSerialNumber ) ) (otherlv_70= ',' ( (lv_ISBN_71_0= ruleSerialNumber ) ) )* otherlv_72= '}' )? (otherlv_73= 'eventEndDate' ( (lv_eventEndDate_74_0= rulePublishedDate ) ) )? otherlv_75= '}'
            {
            // InternalTextualRCV.g:3699:3: ( (lv_openAccess_0_0= 'openAccess' ) )
            // InternalTextualRCV.g:3700:4: (lv_openAccess_0_0= 'openAccess' )
            {
            // InternalTextualRCV.g:3700:4: (lv_openAccess_0_0= 'openAccess' )
            // InternalTextualRCV.g:3701:5: lv_openAccess_0_0= 'openAccess'
            {
            lv_openAccess_0_0=(Token)match(input,73,FOLLOW_107); 

            					newLeafNode(lv_openAccess_0_0, grammarAccess.getConferenceAccess().getOpenAccessOpenAccessKeyword_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getConferenceRule());
            					}
            					setWithLastConsumed(current, "openAccess", lv_openAccess_0_0 != null, "openAccess");
            				

            }


            }

            // InternalTextualRCV.g:3713:3: ( (lv_shortPaper_1_0= 'shortPaper' ) )
            // InternalTextualRCV.g:3714:4: (lv_shortPaper_1_0= 'shortPaper' )
            {
            // InternalTextualRCV.g:3714:4: (lv_shortPaper_1_0= 'shortPaper' )
            // InternalTextualRCV.g:3715:5: lv_shortPaper_1_0= 'shortPaper'
            {
            lv_shortPaper_1_0=(Token)match(input,90,FOLLOW_108); 

            					newLeafNode(lv_shortPaper_1_0, grammarAccess.getConferenceAccess().getShortPaperShortPaperKeyword_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getConferenceRule());
            					}
            					setWithLastConsumed(current, "shortPaper", lv_shortPaper_1_0 != null, "shortPaper");
            				

            }


            }

            // InternalTextualRCV.g:3727:3: ( (lv_toolPaper_2_0= 'toolPaper' ) )
            // InternalTextualRCV.g:3728:4: (lv_toolPaper_2_0= 'toolPaper' )
            {
            // InternalTextualRCV.g:3728:4: (lv_toolPaper_2_0= 'toolPaper' )
            // InternalTextualRCV.g:3729:5: lv_toolPaper_2_0= 'toolPaper'
            {
            lv_toolPaper_2_0=(Token)match(input,91,FOLLOW_109); 

            					newLeafNode(lv_toolPaper_2_0, grammarAccess.getConferenceAccess().getToolPaperToolPaperKeyword_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getConferenceRule());
            					}
            					setWithLastConsumed(current, "toolPaper", lv_toolPaper_2_0 != null, "toolPaper");
            				

            }


            }

            // InternalTextualRCV.g:3741:3: ( (lv_invitedPaper_3_0= 'invitedPaper' ) )
            // InternalTextualRCV.g:3742:4: (lv_invitedPaper_3_0= 'invitedPaper' )
            {
            // InternalTextualRCV.g:3742:4: (lv_invitedPaper_3_0= 'invitedPaper' )
            // InternalTextualRCV.g:3743:5: lv_invitedPaper_3_0= 'invitedPaper'
            {
            lv_invitedPaper_3_0=(Token)match(input,92,FOLLOW_110); 

            					newLeafNode(lv_invitedPaper_3_0, grammarAccess.getConferenceAccess().getInvitedPaperInvitedPaperKeyword_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getConferenceRule());
            					}
            					setWithLastConsumed(current, "invitedPaper", lv_invitedPaper_3_0 != null, "invitedPaper");
            				

            }


            }

            otherlv_4=(Token)match(input,93,FOLLOW_20); 

            			newLeafNode(otherlv_4, grammarAccess.getConferenceAccess().getConferenceKeyword_4());
            		
            // InternalTextualRCV.g:3759:3: ( (lv_citekey_5_0= ruleEString ) )
            // InternalTextualRCV.g:3760:4: (lv_citekey_5_0= ruleEString )
            {
            // InternalTextualRCV.g:3760:4: (lv_citekey_5_0= ruleEString )
            // InternalTextualRCV.g:3761:5: lv_citekey_5_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getConferenceAccess().getCitekeyEStringParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_3);
            lv_citekey_5_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConferenceRule());
            					}
            					set(
            						current,
            						"citekey",
            						lv_citekey_5_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_6=(Token)match(input,16,FOLLOW_58); 

            			newLeafNode(otherlv_6, grammarAccess.getConferenceAccess().getLeftCurlyBracketKeyword_6());
            		
            otherlv_7=(Token)match(input,49,FOLLOW_20); 

            			newLeafNode(otherlv_7, grammarAccess.getConferenceAccess().getTitleKeyword_7());
            		
            // InternalTextualRCV.g:3786:3: ( (lv_title_8_0= ruleEString ) )
            // InternalTextualRCV.g:3787:4: (lv_title_8_0= ruleEString )
            {
            // InternalTextualRCV.g:3787:4: (lv_title_8_0= ruleEString )
            // InternalTextualRCV.g:3788:5: lv_title_8_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getConferenceAccess().getTitleEStringParserRuleCall_8_0());
            				
            pushFollow(FOLLOW_89);
            lv_title_8_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConferenceRule());
            					}
            					set(
            						current,
            						"title",
            						lv_title_8_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:3805:3: (otherlv_9= 'URL' ( (lv_URL_10_0= ruleEString ) ) )?
            int alt77=2;
            int LA77_0 = input.LA(1);

            if ( (LA77_0==47) ) {
                alt77=1;
            }
            switch (alt77) {
                case 1 :
                    // InternalTextualRCV.g:3806:4: otherlv_9= 'URL' ( (lv_URL_10_0= ruleEString ) )
                    {
                    otherlv_9=(Token)match(input,47,FOLLOW_20); 

                    				newLeafNode(otherlv_9, grammarAccess.getConferenceAccess().getURLKeyword_9_0());
                    			
                    // InternalTextualRCV.g:3810:4: ( (lv_URL_10_0= ruleEString ) )
                    // InternalTextualRCV.g:3811:5: (lv_URL_10_0= ruleEString )
                    {
                    // InternalTextualRCV.g:3811:5: (lv_URL_10_0= ruleEString )
                    // InternalTextualRCV.g:3812:6: lv_URL_10_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getConferenceAccess().getURLEStringParserRuleCall_9_1_0());
                    					
                    pushFollow(FOLLOW_90);
                    lv_URL_10_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getConferenceRule());
                    						}
                    						set(
                    							current,
                    							"URL",
                    							lv_URL_10_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_11=(Token)match(input,75,FOLLOW_91); 

            			newLeafNode(otherlv_11, grammarAccess.getConferenceAccess().getPublishedKeyword_10());
            		
            // InternalTextualRCV.g:3834:3: ( (lv_published_12_0= ruleEBoolean ) )
            // InternalTextualRCV.g:3835:4: (lv_published_12_0= ruleEBoolean )
            {
            // InternalTextualRCV.g:3835:4: (lv_published_12_0= ruleEBoolean )
            // InternalTextualRCV.g:3836:5: lv_published_12_0= ruleEBoolean
            {

            					newCompositeNode(grammarAccess.getConferenceAccess().getPublishedEBooleanParserRuleCall_11_0());
            				
            pushFollow(FOLLOW_92);
            lv_published_12_0=ruleEBoolean();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConferenceRule());
            					}
            					set(
            						current,
            						"published",
            						lv_published_12_0,
            						"org.montex.researchcv.xtext.TextualRCV.EBoolean");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:3853:3: (otherlv_13= 'DOI' ( (lv_DOI_14_0= ruleEString ) ) )?
            int alt78=2;
            int LA78_0 = input.LA(1);

            if ( (LA78_0==76) ) {
                alt78=1;
            }
            switch (alt78) {
                case 1 :
                    // InternalTextualRCV.g:3854:4: otherlv_13= 'DOI' ( (lv_DOI_14_0= ruleEString ) )
                    {
                    otherlv_13=(Token)match(input,76,FOLLOW_20); 

                    				newLeafNode(otherlv_13, grammarAccess.getConferenceAccess().getDOIKeyword_12_0());
                    			
                    // InternalTextualRCV.g:3858:4: ( (lv_DOI_14_0= ruleEString ) )
                    // InternalTextualRCV.g:3859:5: (lv_DOI_14_0= ruleEString )
                    {
                    // InternalTextualRCV.g:3859:5: (lv_DOI_14_0= ruleEString )
                    // InternalTextualRCV.g:3860:6: lv_DOI_14_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getConferenceAccess().getDOIEStringParserRuleCall_12_1_0());
                    					
                    pushFollow(FOLLOW_93);
                    lv_DOI_14_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getConferenceRule());
                    						}
                    						set(
                    							current,
                    							"DOI",
                    							lv_DOI_14_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:3878:3: (otherlv_15= 'abstract' ( (lv_abstract_16_0= ruleEString ) ) )?
            int alt79=2;
            int LA79_0 = input.LA(1);

            if ( (LA79_0==77) ) {
                alt79=1;
            }
            switch (alt79) {
                case 1 :
                    // InternalTextualRCV.g:3879:4: otherlv_15= 'abstract' ( (lv_abstract_16_0= ruleEString ) )
                    {
                    otherlv_15=(Token)match(input,77,FOLLOW_20); 

                    				newLeafNode(otherlv_15, grammarAccess.getConferenceAccess().getAbstractKeyword_13_0());
                    			
                    // InternalTextualRCV.g:3883:4: ( (lv_abstract_16_0= ruleEString ) )
                    // InternalTextualRCV.g:3884:5: (lv_abstract_16_0= ruleEString )
                    {
                    // InternalTextualRCV.g:3884:5: (lv_abstract_16_0= ruleEString )
                    // InternalTextualRCV.g:3885:6: lv_abstract_16_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getConferenceAccess().getAbstractEStringParserRuleCall_13_1_0());
                    					
                    pushFollow(FOLLOW_94);
                    lv_abstract_16_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getConferenceRule());
                    						}
                    						set(
                    							current,
                    							"abstract",
                    							lv_abstract_16_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_17=(Token)match(input,78,FOLLOW_91); 

            			newLeafNode(otherlv_17, grammarAccess.getConferenceAccess().getWithAuthorVersionKeyword_14());
            		
            // InternalTextualRCV.g:3907:3: ( (lv_withAuthorVersion_18_0= ruleEBoolean ) )
            // InternalTextualRCV.g:3908:4: (lv_withAuthorVersion_18_0= ruleEBoolean )
            {
            // InternalTextualRCV.g:3908:4: (lv_withAuthorVersion_18_0= ruleEBoolean )
            // InternalTextualRCV.g:3909:5: lv_withAuthorVersion_18_0= ruleEBoolean
            {

            					newCompositeNode(grammarAccess.getConferenceAccess().getWithAuthorVersionEBooleanParserRuleCall_15_0());
            				
            pushFollow(FOLLOW_111);
            lv_withAuthorVersion_18_0=ruleEBoolean();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConferenceRule());
            					}
            					set(
            						current,
            						"withAuthorVersion",
            						lv_withAuthorVersion_18_0,
            						"org.montex.researchcv.xtext.TextualRCV.EBoolean");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:3926:3: (otherlv_19= 'notes' otherlv_20= '{' ( (lv_notes_21_0= ruleEString ) ) (otherlv_22= ',' ( (lv_notes_23_0= ruleEString ) ) )* otherlv_24= '}' )?
            int alt81=2;
            int LA81_0 = input.LA(1);

            if ( (LA81_0==79) ) {
                alt81=1;
            }
            switch (alt81) {
                case 1 :
                    // InternalTextualRCV.g:3927:4: otherlv_19= 'notes' otherlv_20= '{' ( (lv_notes_21_0= ruleEString ) ) (otherlv_22= ',' ( (lv_notes_23_0= ruleEString ) ) )* otherlv_24= '}'
                    {
                    otherlv_19=(Token)match(input,79,FOLLOW_3); 

                    				newLeafNode(otherlv_19, grammarAccess.getConferenceAccess().getNotesKeyword_16_0());
                    			
                    otherlv_20=(Token)match(input,16,FOLLOW_20); 

                    				newLeafNode(otherlv_20, grammarAccess.getConferenceAccess().getLeftCurlyBracketKeyword_16_1());
                    			
                    // InternalTextualRCV.g:3935:4: ( (lv_notes_21_0= ruleEString ) )
                    // InternalTextualRCV.g:3936:5: (lv_notes_21_0= ruleEString )
                    {
                    // InternalTextualRCV.g:3936:5: (lv_notes_21_0= ruleEString )
                    // InternalTextualRCV.g:3937:6: lv_notes_21_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getConferenceAccess().getNotesEStringParserRuleCall_16_2_0());
                    					
                    pushFollow(FOLLOW_40);
                    lv_notes_21_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getConferenceRule());
                    						}
                    						add(
                    							current,
                    							"notes",
                    							lv_notes_21_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:3954:4: (otherlv_22= ',' ( (lv_notes_23_0= ruleEString ) ) )*
                    loop80:
                    do {
                        int alt80=2;
                        int LA80_0 = input.LA(1);

                        if ( (LA80_0==24) ) {
                            alt80=1;
                        }


                        switch (alt80) {
                    	case 1 :
                    	    // InternalTextualRCV.g:3955:5: otherlv_22= ',' ( (lv_notes_23_0= ruleEString ) )
                    	    {
                    	    otherlv_22=(Token)match(input,24,FOLLOW_20); 

                    	    					newLeafNode(otherlv_22, grammarAccess.getConferenceAccess().getCommaKeyword_16_3_0());
                    	    				
                    	    // InternalTextualRCV.g:3959:5: ( (lv_notes_23_0= ruleEString ) )
                    	    // InternalTextualRCV.g:3960:6: (lv_notes_23_0= ruleEString )
                    	    {
                    	    // InternalTextualRCV.g:3960:6: (lv_notes_23_0= ruleEString )
                    	    // InternalTextualRCV.g:3961:7: lv_notes_23_0= ruleEString
                    	    {

                    	    							newCompositeNode(grammarAccess.getConferenceAccess().getNotesEStringParserRuleCall_16_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_40);
                    	    lv_notes_23_0=ruleEString();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getConferenceRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"notes",
                    	    								lv_notes_23_0,
                    	    								"org.montex.researchcv.xtext.TextualRCV.EString");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop80;
                        }
                    } while (true);

                    otherlv_24=(Token)match(input,18,FOLLOW_112); 

                    				newLeafNode(otherlv_24, grammarAccess.getConferenceAccess().getRightCurlyBracketKeyword_16_4());
                    			

                    }
                    break;

            }

            otherlv_25=(Token)match(input,94,FOLLOW_20); 

            			newLeafNode(otherlv_25, grammarAccess.getConferenceAccess().getBooktitleKeyword_17());
            		
            // InternalTextualRCV.g:3988:3: ( (lv_booktitle_26_0= ruleEString ) )
            // InternalTextualRCV.g:3989:4: (lv_booktitle_26_0= ruleEString )
            {
            // InternalTextualRCV.g:3989:4: (lv_booktitle_26_0= ruleEString )
            // InternalTextualRCV.g:3990:5: lv_booktitle_26_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getConferenceAccess().getBooktitleEStringParserRuleCall_18_0());
            				
            pushFollow(FOLLOW_113);
            lv_booktitle_26_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConferenceRule());
            					}
            					set(
            						current,
            						"booktitle",
            						lv_booktitle_26_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:4007:3: (otherlv_27= 'publisher' ( (lv_publisher_28_0= ruleEString ) ) )?
            int alt82=2;
            int LA82_0 = input.LA(1);

            if ( (LA82_0==81) ) {
                alt82=1;
            }
            switch (alt82) {
                case 1 :
                    // InternalTextualRCV.g:4008:4: otherlv_27= 'publisher' ( (lv_publisher_28_0= ruleEString ) )
                    {
                    otherlv_27=(Token)match(input,81,FOLLOW_20); 

                    				newLeafNode(otherlv_27, grammarAccess.getConferenceAccess().getPublisherKeyword_19_0());
                    			
                    // InternalTextualRCV.g:4012:4: ( (lv_publisher_28_0= ruleEString ) )
                    // InternalTextualRCV.g:4013:5: (lv_publisher_28_0= ruleEString )
                    {
                    // InternalTextualRCV.g:4013:5: (lv_publisher_28_0= ruleEString )
                    // InternalTextualRCV.g:4014:6: lv_publisher_28_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getConferenceAccess().getPublisherEStringParserRuleCall_19_1_0());
                    					
                    pushFollow(FOLLOW_114);
                    lv_publisher_28_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getConferenceRule());
                    						}
                    						set(
                    							current,
                    							"publisher",
                    							lv_publisher_28_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:4032:3: (otherlv_29= 'series' ( (lv_series_30_0= ruleEString ) ) )?
            int alt83=2;
            int LA83_0 = input.LA(1);

            if ( (LA83_0==95) ) {
                alt83=1;
            }
            switch (alt83) {
                case 1 :
                    // InternalTextualRCV.g:4033:4: otherlv_29= 'series' ( (lv_series_30_0= ruleEString ) )
                    {
                    otherlv_29=(Token)match(input,95,FOLLOW_20); 

                    				newLeafNode(otherlv_29, grammarAccess.getConferenceAccess().getSeriesKeyword_20_0());
                    			
                    // InternalTextualRCV.g:4037:4: ( (lv_series_30_0= ruleEString ) )
                    // InternalTextualRCV.g:4038:5: (lv_series_30_0= ruleEString )
                    {
                    // InternalTextualRCV.g:4038:5: (lv_series_30_0= ruleEString )
                    // InternalTextualRCV.g:4039:6: lv_series_30_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getConferenceAccess().getSeriesEStringParserRuleCall_20_1_0());
                    					
                    pushFollow(FOLLOW_115);
                    lv_series_30_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getConferenceRule());
                    						}
                    						set(
                    							current,
                    							"series",
                    							lv_series_30_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:4057:3: (otherlv_31= 'volume' ( (lv_volume_32_0= ruleEString ) ) )?
            int alt84=2;
            int LA84_0 = input.LA(1);

            if ( (LA84_0==84) ) {
                alt84=1;
            }
            switch (alt84) {
                case 1 :
                    // InternalTextualRCV.g:4058:4: otherlv_31= 'volume' ( (lv_volume_32_0= ruleEString ) )
                    {
                    otherlv_31=(Token)match(input,84,FOLLOW_20); 

                    				newLeafNode(otherlv_31, grammarAccess.getConferenceAccess().getVolumeKeyword_21_0());
                    			
                    // InternalTextualRCV.g:4062:4: ( (lv_volume_32_0= ruleEString ) )
                    // InternalTextualRCV.g:4063:5: (lv_volume_32_0= ruleEString )
                    {
                    // InternalTextualRCV.g:4063:5: (lv_volume_32_0= ruleEString )
                    // InternalTextualRCV.g:4064:6: lv_volume_32_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getConferenceAccess().getVolumeEStringParserRuleCall_21_1_0());
                    					
                    pushFollow(FOLLOW_116);
                    lv_volume_32_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getConferenceRule());
                    						}
                    						set(
                    							current,
                    							"volume",
                    							lv_volume_32_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:4082:3: (otherlv_33= 'firstPage' ( (lv_firstPage_34_0= ruleEString ) ) )?
            int alt85=2;
            int LA85_0 = input.LA(1);

            if ( (LA85_0==82) ) {
                alt85=1;
            }
            switch (alt85) {
                case 1 :
                    // InternalTextualRCV.g:4083:4: otherlv_33= 'firstPage' ( (lv_firstPage_34_0= ruleEString ) )
                    {
                    otherlv_33=(Token)match(input,82,FOLLOW_20); 

                    				newLeafNode(otherlv_33, grammarAccess.getConferenceAccess().getFirstPageKeyword_22_0());
                    			
                    // InternalTextualRCV.g:4087:4: ( (lv_firstPage_34_0= ruleEString ) )
                    // InternalTextualRCV.g:4088:5: (lv_firstPage_34_0= ruleEString )
                    {
                    // InternalTextualRCV.g:4088:5: (lv_firstPage_34_0= ruleEString )
                    // InternalTextualRCV.g:4089:6: lv_firstPage_34_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getConferenceAccess().getFirstPageEStringParserRuleCall_22_1_0());
                    					
                    pushFollow(FOLLOW_117);
                    lv_firstPage_34_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getConferenceRule());
                    						}
                    						set(
                    							current,
                    							"firstPage",
                    							lv_firstPage_34_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:4107:3: (otherlv_35= 'lastPage' ( (lv_lastPage_36_0= ruleEString ) ) )?
            int alt86=2;
            int LA86_0 = input.LA(1);

            if ( (LA86_0==83) ) {
                alt86=1;
            }
            switch (alt86) {
                case 1 :
                    // InternalTextualRCV.g:4108:4: otherlv_35= 'lastPage' ( (lv_lastPage_36_0= ruleEString ) )
                    {
                    otherlv_35=(Token)match(input,83,FOLLOW_20); 

                    				newLeafNode(otherlv_35, grammarAccess.getConferenceAccess().getLastPageKeyword_23_0());
                    			
                    // InternalTextualRCV.g:4112:4: ( (lv_lastPage_36_0= ruleEString ) )
                    // InternalTextualRCV.g:4113:5: (lv_lastPage_36_0= ruleEString )
                    {
                    // InternalTextualRCV.g:4113:5: (lv_lastPage_36_0= ruleEString )
                    // InternalTextualRCV.g:4114:6: lv_lastPage_36_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getConferenceAccess().getLastPageEStringParserRuleCall_23_1_0());
                    					
                    pushFollow(FOLLOW_118);
                    lv_lastPage_36_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getConferenceRule());
                    						}
                    						set(
                    							current,
                    							"lastPage",
                    							lv_lastPage_36_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_37=(Token)match(input,96,FOLLOW_20); 

            			newLeafNode(otherlv_37, grammarAccess.getConferenceAccess().getEventNameKeyword_24());
            		
            // InternalTextualRCV.g:4136:3: ( (lv_eventName_38_0= ruleEString ) )
            // InternalTextualRCV.g:4137:4: (lv_eventName_38_0= ruleEString )
            {
            // InternalTextualRCV.g:4137:4: (lv_eventName_38_0= ruleEString )
            // InternalTextualRCV.g:4138:5: lv_eventName_38_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getConferenceAccess().getEventNameEStringParserRuleCall_25_0());
            				
            pushFollow(FOLLOW_119);
            lv_eventName_38_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConferenceRule());
            					}
            					set(
            						current,
            						"eventName",
            						lv_eventName_38_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:4155:3: (otherlv_39= 'venue' ( (lv_venue_40_0= ruleEString ) ) )?
            int alt87=2;
            int LA87_0 = input.LA(1);

            if ( (LA87_0==97) ) {
                alt87=1;
            }
            switch (alt87) {
                case 1 :
                    // InternalTextualRCV.g:4156:4: otherlv_39= 'venue' ( (lv_venue_40_0= ruleEString ) )
                    {
                    otherlv_39=(Token)match(input,97,FOLLOW_20); 

                    				newLeafNode(otherlv_39, grammarAccess.getConferenceAccess().getVenueKeyword_26_0());
                    			
                    // InternalTextualRCV.g:4160:4: ( (lv_venue_40_0= ruleEString ) )
                    // InternalTextualRCV.g:4161:5: (lv_venue_40_0= ruleEString )
                    {
                    // InternalTextualRCV.g:4161:5: (lv_venue_40_0= ruleEString )
                    // InternalTextualRCV.g:4162:6: lv_venue_40_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getConferenceAccess().getVenueEStringParserRuleCall_26_1_0());
                    					
                    pushFollow(FOLLOW_120);
                    lv_venue_40_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getConferenceRule());
                    						}
                    						set(
                    							current,
                    							"venue",
                    							lv_venue_40_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_41=(Token)match(input,98,FOLLOW_20); 

            			newLeafNode(otherlv_41, grammarAccess.getConferenceAccess().getEventShortNameKeyword_27());
            		
            // InternalTextualRCV.g:4184:3: ( (lv_eventShortName_42_0= ruleEString ) )
            // InternalTextualRCV.g:4185:4: (lv_eventShortName_42_0= ruleEString )
            {
            // InternalTextualRCV.g:4185:4: (lv_eventShortName_42_0= ruleEString )
            // InternalTextualRCV.g:4186:5: lv_eventShortName_42_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getConferenceAccess().getEventShortNameEStringParserRuleCall_28_0());
            				
            pushFollow(FOLLOW_121);
            lv_eventShortName_42_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConferenceRule());
            					}
            					set(
            						current,
            						"eventShortName",
            						lv_eventShortName_42_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:4203:3: (otherlv_43= 'trackName' ( (lv_trackName_44_0= ruleEString ) ) )?
            int alt88=2;
            int LA88_0 = input.LA(1);

            if ( (LA88_0==99) ) {
                alt88=1;
            }
            switch (alt88) {
                case 1 :
                    // InternalTextualRCV.g:4204:4: otherlv_43= 'trackName' ( (lv_trackName_44_0= ruleEString ) )
                    {
                    otherlv_43=(Token)match(input,99,FOLLOW_20); 

                    				newLeafNode(otherlv_43, grammarAccess.getConferenceAccess().getTrackNameKeyword_29_0());
                    			
                    // InternalTextualRCV.g:4208:4: ( (lv_trackName_44_0= ruleEString ) )
                    // InternalTextualRCV.g:4209:5: (lv_trackName_44_0= ruleEString )
                    {
                    // InternalTextualRCV.g:4209:5: (lv_trackName_44_0= ruleEString )
                    // InternalTextualRCV.g:4210:6: lv_trackName_44_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getConferenceAccess().getTrackNameEStringParserRuleCall_29_1_0());
                    					
                    pushFollow(FOLLOW_122);
                    lv_trackName_44_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getConferenceRule());
                    						}
                    						set(
                    							current,
                    							"trackName",
                    							lv_trackName_44_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:4228:3: (otherlv_45= 'trackShortName' ( (lv_trackShortName_46_0= ruleEString ) ) )?
            int alt89=2;
            int LA89_0 = input.LA(1);

            if ( (LA89_0==100) ) {
                alt89=1;
            }
            switch (alt89) {
                case 1 :
                    // InternalTextualRCV.g:4229:4: otherlv_45= 'trackShortName' ( (lv_trackShortName_46_0= ruleEString ) )
                    {
                    otherlv_45=(Token)match(input,100,FOLLOW_20); 

                    				newLeafNode(otherlv_45, grammarAccess.getConferenceAccess().getTrackShortNameKeyword_30_0());
                    			
                    // InternalTextualRCV.g:4233:4: ( (lv_trackShortName_46_0= ruleEString ) )
                    // InternalTextualRCV.g:4234:5: (lv_trackShortName_46_0= ruleEString )
                    {
                    // InternalTextualRCV.g:4234:5: (lv_trackShortName_46_0= ruleEString )
                    // InternalTextualRCV.g:4235:6: lv_trackShortName_46_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getConferenceAccess().getTrackShortNameEStringParserRuleCall_30_1_0());
                    					
                    pushFollow(FOLLOW_102);
                    lv_trackShortName_46_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getConferenceRule());
                    						}
                    						set(
                    							current,
                    							"trackShortName",
                    							lv_trackShortName_46_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_47=(Token)match(input,86,FOLLOW_61); 

            			newLeafNode(otherlv_47, grammarAccess.getConferenceAccess().getAuthorsKeyword_31());
            		
            otherlv_48=(Token)match(input,52,FOLLOW_20); 

            			newLeafNode(otherlv_48, grammarAccess.getConferenceAccess().getLeftParenthesisKeyword_32());
            		
            // InternalTextualRCV.g:4261:3: ( ( ruleEString ) )
            // InternalTextualRCV.g:4262:4: ( ruleEString )
            {
            // InternalTextualRCV.g:4262:4: ( ruleEString )
            // InternalTextualRCV.g:4263:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getConferenceRule());
            					}
            				

            					newCompositeNode(grammarAccess.getConferenceAccess().getAuthorsPersonCrossReference_33_0());
            				
            pushFollow(FOLLOW_62);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:4277:3: (otherlv_50= ',' ( ( ruleEString ) ) )*
            loop90:
            do {
                int alt90=2;
                int LA90_0 = input.LA(1);

                if ( (LA90_0==24) ) {
                    alt90=1;
                }


                switch (alt90) {
            	case 1 :
            	    // InternalTextualRCV.g:4278:4: otherlv_50= ',' ( ( ruleEString ) )
            	    {
            	    otherlv_50=(Token)match(input,24,FOLLOW_20); 

            	    				newLeafNode(otherlv_50, grammarAccess.getConferenceAccess().getCommaKeyword_34_0());
            	    			
            	    // InternalTextualRCV.g:4282:4: ( ( ruleEString ) )
            	    // InternalTextualRCV.g:4283:5: ( ruleEString )
            	    {
            	    // InternalTextualRCV.g:4283:5: ( ruleEString )
            	    // InternalTextualRCV.g:4284:6: ruleEString
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getConferenceRule());
            	    						}
            	    					

            	    						newCompositeNode(grammarAccess.getConferenceAccess().getAuthorsPersonCrossReference_34_1_0());
            	    					
            	    pushFollow(FOLLOW_62);
            	    ruleEString();

            	    state._fsp--;


            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop90;
                }
            } while (true);

            otherlv_52=(Token)match(input,53,FOLLOW_123); 

            			newLeafNode(otherlv_52, grammarAccess.getConferenceAccess().getRightParenthesisKeyword_35());
            		
            // InternalTextualRCV.g:4303:3: (otherlv_53= 'relatedProjects' otherlv_54= '(' ( ( ruleEString ) ) (otherlv_56= ',' ( ( ruleEString ) ) )* otherlv_58= ')' )?
            int alt92=2;
            int LA92_0 = input.LA(1);

            if ( (LA92_0==87) ) {
                alt92=1;
            }
            switch (alt92) {
                case 1 :
                    // InternalTextualRCV.g:4304:4: otherlv_53= 'relatedProjects' otherlv_54= '(' ( ( ruleEString ) ) (otherlv_56= ',' ( ( ruleEString ) ) )* otherlv_58= ')'
                    {
                    otherlv_53=(Token)match(input,87,FOLLOW_61); 

                    				newLeafNode(otherlv_53, grammarAccess.getConferenceAccess().getRelatedProjectsKeyword_36_0());
                    			
                    otherlv_54=(Token)match(input,52,FOLLOW_20); 

                    				newLeafNode(otherlv_54, grammarAccess.getConferenceAccess().getLeftParenthesisKeyword_36_1());
                    			
                    // InternalTextualRCV.g:4312:4: ( ( ruleEString ) )
                    // InternalTextualRCV.g:4313:5: ( ruleEString )
                    {
                    // InternalTextualRCV.g:4313:5: ( ruleEString )
                    // InternalTextualRCV.g:4314:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getConferenceRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getConferenceAccess().getRelatedProjectsGrantCrossReference_36_2_0());
                    					
                    pushFollow(FOLLOW_62);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:4328:4: (otherlv_56= ',' ( ( ruleEString ) ) )*
                    loop91:
                    do {
                        int alt91=2;
                        int LA91_0 = input.LA(1);

                        if ( (LA91_0==24) ) {
                            alt91=1;
                        }


                        switch (alt91) {
                    	case 1 :
                    	    // InternalTextualRCV.g:4329:5: otherlv_56= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_56=(Token)match(input,24,FOLLOW_20); 

                    	    					newLeafNode(otherlv_56, grammarAccess.getConferenceAccess().getCommaKeyword_36_3_0());
                    	    				
                    	    // InternalTextualRCV.g:4333:5: ( ( ruleEString ) )
                    	    // InternalTextualRCV.g:4334:6: ( ruleEString )
                    	    {
                    	    // InternalTextualRCV.g:4334:6: ( ruleEString )
                    	    // InternalTextualRCV.g:4335:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getConferenceRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getConferenceAccess().getRelatedProjectsGrantCrossReference_36_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_62);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop91;
                        }
                    } while (true);

                    otherlv_58=(Token)match(input,53,FOLLOW_124); 

                    				newLeafNode(otherlv_58, grammarAccess.getConferenceAccess().getRightParenthesisKeyword_36_4());
                    			

                    }
                    break;

            }

            // InternalTextualRCV.g:4355:3: (otherlv_59= 'bookeditors' otherlv_60= '(' ( ( ruleEString ) ) (otherlv_62= ',' ( ( ruleEString ) ) )* otherlv_64= ')' )?
            int alt94=2;
            int LA94_0 = input.LA(1);

            if ( (LA94_0==101) ) {
                alt94=1;
            }
            switch (alt94) {
                case 1 :
                    // InternalTextualRCV.g:4356:4: otherlv_59= 'bookeditors' otherlv_60= '(' ( ( ruleEString ) ) (otherlv_62= ',' ( ( ruleEString ) ) )* otherlv_64= ')'
                    {
                    otherlv_59=(Token)match(input,101,FOLLOW_61); 

                    				newLeafNode(otherlv_59, grammarAccess.getConferenceAccess().getBookeditorsKeyword_37_0());
                    			
                    otherlv_60=(Token)match(input,52,FOLLOW_20); 

                    				newLeafNode(otherlv_60, grammarAccess.getConferenceAccess().getLeftParenthesisKeyword_37_1());
                    			
                    // InternalTextualRCV.g:4364:4: ( ( ruleEString ) )
                    // InternalTextualRCV.g:4365:5: ( ruleEString )
                    {
                    // InternalTextualRCV.g:4365:5: ( ruleEString )
                    // InternalTextualRCV.g:4366:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getConferenceRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getConferenceAccess().getBookeditorsPersonCrossReference_37_2_0());
                    					
                    pushFollow(FOLLOW_62);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:4380:4: (otherlv_62= ',' ( ( ruleEString ) ) )*
                    loop93:
                    do {
                        int alt93=2;
                        int LA93_0 = input.LA(1);

                        if ( (LA93_0==24) ) {
                            alt93=1;
                        }


                        switch (alt93) {
                    	case 1 :
                    	    // InternalTextualRCV.g:4381:5: otherlv_62= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_62=(Token)match(input,24,FOLLOW_20); 

                    	    					newLeafNode(otherlv_62, grammarAccess.getConferenceAccess().getCommaKeyword_37_3_0());
                    	    				
                    	    // InternalTextualRCV.g:4385:5: ( ( ruleEString ) )
                    	    // InternalTextualRCV.g:4386:6: ( ruleEString )
                    	    {
                    	    // InternalTextualRCV.g:4386:6: ( ruleEString )
                    	    // InternalTextualRCV.g:4387:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getConferenceRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getConferenceAccess().getBookeditorsPersonCrossReference_37_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_62);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop93;
                        }
                    } while (true);

                    otherlv_64=(Token)match(input,53,FOLLOW_104); 

                    				newLeafNode(otherlv_64, grammarAccess.getConferenceAccess().getRightParenthesisKeyword_37_4());
                    			

                    }
                    break;

            }

            otherlv_65=(Token)match(input,88,FOLLOW_9); 

            			newLeafNode(otherlv_65, grammarAccess.getConferenceAccess().getDateKeyword_38());
            		
            // InternalTextualRCV.g:4411:3: ( (lv_date_66_0= rulePublishedDate ) )
            // InternalTextualRCV.g:4412:4: (lv_date_66_0= rulePublishedDate )
            {
            // InternalTextualRCV.g:4412:4: (lv_date_66_0= rulePublishedDate )
            // InternalTextualRCV.g:4413:5: lv_date_66_0= rulePublishedDate
            {

            					newCompositeNode(grammarAccess.getConferenceAccess().getDatePublishedDateParserRuleCall_39_0());
            				
            pushFollow(FOLLOW_125);
            lv_date_66_0=rulePublishedDate();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConferenceRule());
            					}
            					set(
            						current,
            						"date",
            						lv_date_66_0,
            						"org.montex.researchcv.xtext.TextualRCV.PublishedDate");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:4430:3: (otherlv_67= 'ISBN' otherlv_68= '{' ( (lv_ISBN_69_0= ruleSerialNumber ) ) (otherlv_70= ',' ( (lv_ISBN_71_0= ruleSerialNumber ) ) )* otherlv_72= '}' )?
            int alt96=2;
            int LA96_0 = input.LA(1);

            if ( (LA96_0==102) ) {
                alt96=1;
            }
            switch (alt96) {
                case 1 :
                    // InternalTextualRCV.g:4431:4: otherlv_67= 'ISBN' otherlv_68= '{' ( (lv_ISBN_69_0= ruleSerialNumber ) ) (otherlv_70= ',' ( (lv_ISBN_71_0= ruleSerialNumber ) ) )* otherlv_72= '}'
                    {
                    otherlv_67=(Token)match(input,102,FOLLOW_3); 

                    				newLeafNode(otherlv_67, grammarAccess.getConferenceAccess().getISBNKeyword_40_0());
                    			
                    otherlv_68=(Token)match(input,16,FOLLOW_106); 

                    				newLeafNode(otherlv_68, grammarAccess.getConferenceAccess().getLeftCurlyBracketKeyword_40_1());
                    			
                    // InternalTextualRCV.g:4439:4: ( (lv_ISBN_69_0= ruleSerialNumber ) )
                    // InternalTextualRCV.g:4440:5: (lv_ISBN_69_0= ruleSerialNumber )
                    {
                    // InternalTextualRCV.g:4440:5: (lv_ISBN_69_0= ruleSerialNumber )
                    // InternalTextualRCV.g:4441:6: lv_ISBN_69_0= ruleSerialNumber
                    {

                    						newCompositeNode(grammarAccess.getConferenceAccess().getISBNSerialNumberParserRuleCall_40_2_0());
                    					
                    pushFollow(FOLLOW_40);
                    lv_ISBN_69_0=ruleSerialNumber();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getConferenceRule());
                    						}
                    						add(
                    							current,
                    							"ISBN",
                    							lv_ISBN_69_0,
                    							"org.montex.researchcv.xtext.TextualRCV.SerialNumber");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:4458:4: (otherlv_70= ',' ( (lv_ISBN_71_0= ruleSerialNumber ) ) )*
                    loop95:
                    do {
                        int alt95=2;
                        int LA95_0 = input.LA(1);

                        if ( (LA95_0==24) ) {
                            alt95=1;
                        }


                        switch (alt95) {
                    	case 1 :
                    	    // InternalTextualRCV.g:4459:5: otherlv_70= ',' ( (lv_ISBN_71_0= ruleSerialNumber ) )
                    	    {
                    	    otherlv_70=(Token)match(input,24,FOLLOW_106); 

                    	    					newLeafNode(otherlv_70, grammarAccess.getConferenceAccess().getCommaKeyword_40_3_0());
                    	    				
                    	    // InternalTextualRCV.g:4463:5: ( (lv_ISBN_71_0= ruleSerialNumber ) )
                    	    // InternalTextualRCV.g:4464:6: (lv_ISBN_71_0= ruleSerialNumber )
                    	    {
                    	    // InternalTextualRCV.g:4464:6: (lv_ISBN_71_0= ruleSerialNumber )
                    	    // InternalTextualRCV.g:4465:7: lv_ISBN_71_0= ruleSerialNumber
                    	    {

                    	    							newCompositeNode(grammarAccess.getConferenceAccess().getISBNSerialNumberParserRuleCall_40_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_40);
                    	    lv_ISBN_71_0=ruleSerialNumber();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getConferenceRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"ISBN",
                    	    								lv_ISBN_71_0,
                    	    								"org.montex.researchcv.xtext.TextualRCV.SerialNumber");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop95;
                        }
                    } while (true);

                    otherlv_72=(Token)match(input,18,FOLLOW_126); 

                    				newLeafNode(otherlv_72, grammarAccess.getConferenceAccess().getRightCurlyBracketKeyword_40_4());
                    			

                    }
                    break;

            }

            // InternalTextualRCV.g:4488:3: (otherlv_73= 'eventEndDate' ( (lv_eventEndDate_74_0= rulePublishedDate ) ) )?
            int alt97=2;
            int LA97_0 = input.LA(1);

            if ( (LA97_0==103) ) {
                alt97=1;
            }
            switch (alt97) {
                case 1 :
                    // InternalTextualRCV.g:4489:4: otherlv_73= 'eventEndDate' ( (lv_eventEndDate_74_0= rulePublishedDate ) )
                    {
                    otherlv_73=(Token)match(input,103,FOLLOW_9); 

                    				newLeafNode(otherlv_73, grammarAccess.getConferenceAccess().getEventEndDateKeyword_41_0());
                    			
                    // InternalTextualRCV.g:4493:4: ( (lv_eventEndDate_74_0= rulePublishedDate ) )
                    // InternalTextualRCV.g:4494:5: (lv_eventEndDate_74_0= rulePublishedDate )
                    {
                    // InternalTextualRCV.g:4494:5: (lv_eventEndDate_74_0= rulePublishedDate )
                    // InternalTextualRCV.g:4495:6: lv_eventEndDate_74_0= rulePublishedDate
                    {

                    						newCompositeNode(grammarAccess.getConferenceAccess().getEventEndDatePublishedDateParserRuleCall_41_1_0());
                    					
                    pushFollow(FOLLOW_27);
                    lv_eventEndDate_74_0=rulePublishedDate();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getConferenceRule());
                    						}
                    						set(
                    							current,
                    							"eventEndDate",
                    							lv_eventEndDate_74_0,
                    							"org.montex.researchcv.xtext.TextualRCV.PublishedDate");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_75=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_75, grammarAccess.getConferenceAccess().getRightCurlyBracketKeyword_42());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConference"


    // $ANTLR start "entryRuleWorkshop"
    // InternalTextualRCV.g:4521:1: entryRuleWorkshop returns [EObject current=null] : iv_ruleWorkshop= ruleWorkshop EOF ;
    public final EObject entryRuleWorkshop() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWorkshop = null;


        try {
            // InternalTextualRCV.g:4521:49: (iv_ruleWorkshop= ruleWorkshop EOF )
            // InternalTextualRCV.g:4522:2: iv_ruleWorkshop= ruleWorkshop EOF
            {
             newCompositeNode(grammarAccess.getWorkshopRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleWorkshop=ruleWorkshop();

            state._fsp--;

             current =iv_ruleWorkshop; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWorkshop"


    // $ANTLR start "ruleWorkshop"
    // InternalTextualRCV.g:4528:1: ruleWorkshop returns [EObject current=null] : ( ( (lv_openAccess_0_0= 'openAccess' ) ) ( (lv_shortPaper_1_0= 'shortPaper' ) ) ( (lv_toolPaper_2_0= 'toolPaper' ) ) ( (lv_invitedPaper_3_0= 'invitedPaper' ) ) otherlv_4= 'Workshop' ( (lv_citekey_5_0= ruleEString ) ) otherlv_6= '{' otherlv_7= 'title' ( (lv_title_8_0= ruleEString ) ) (otherlv_9= 'URL' ( (lv_URL_10_0= ruleEString ) ) )? otherlv_11= 'published' ( (lv_published_12_0= ruleEBoolean ) ) (otherlv_13= 'DOI' ( (lv_DOI_14_0= ruleEString ) ) )? (otherlv_15= 'abstract' ( (lv_abstract_16_0= ruleEString ) ) )? otherlv_17= 'withAuthorVersion' ( (lv_withAuthorVersion_18_0= ruleEBoolean ) ) (otherlv_19= 'notes' otherlv_20= '{' ( (lv_notes_21_0= ruleEString ) ) (otherlv_22= ',' ( (lv_notes_23_0= ruleEString ) ) )* otherlv_24= '}' )? otherlv_25= 'booktitle' ( (lv_booktitle_26_0= ruleEString ) ) (otherlv_27= 'publisher' ( (lv_publisher_28_0= ruleEString ) ) )? (otherlv_29= 'series' ( (lv_series_30_0= ruleEString ) ) )? (otherlv_31= 'volume' ( (lv_volume_32_0= ruleEString ) ) )? (otherlv_33= 'firstPage' ( (lv_firstPage_34_0= ruleEString ) ) )? (otherlv_35= 'lastPage' ( (lv_lastPage_36_0= ruleEString ) ) )? otherlv_37= 'eventName' ( (lv_eventName_38_0= ruleEString ) ) (otherlv_39= 'venue' ( (lv_venue_40_0= ruleEString ) ) )? otherlv_41= 'eventShortName' ( (lv_eventShortName_42_0= ruleEString ) ) (otherlv_43= 'trackName' ( (lv_trackName_44_0= ruleEString ) ) )? (otherlv_45= 'trackShortName' ( (lv_trackShortName_46_0= ruleEString ) ) )? (otherlv_47= 'mainEventName' ( (lv_mainEventName_48_0= ruleEString ) ) )? (otherlv_49= 'mainEventShortName' ( (lv_mainEventShortName_50_0= ruleEString ) ) )? otherlv_51= 'authors' otherlv_52= '(' ( ( ruleEString ) ) (otherlv_54= ',' ( ( ruleEString ) ) )* otherlv_56= ')' (otherlv_57= 'relatedProjects' otherlv_58= '(' ( ( ruleEString ) ) (otherlv_60= ',' ( ( ruleEString ) ) )* otherlv_62= ')' )? (otherlv_63= 'bookeditors' otherlv_64= '(' ( ( ruleEString ) ) (otherlv_66= ',' ( ( ruleEString ) ) )* otherlv_68= ')' )? otherlv_69= 'date' ( (lv_date_70_0= rulePublishedDate ) ) (otherlv_71= 'ISBN' otherlv_72= '{' ( (lv_ISBN_73_0= ruleSerialNumber ) ) (otherlv_74= ',' ( (lv_ISBN_75_0= ruleSerialNumber ) ) )* otherlv_76= '}' )? (otherlv_77= 'eventEndDate' ( (lv_eventEndDate_78_0= rulePublishedDate ) ) )? otherlv_79= '}' ) ;
    public final EObject ruleWorkshop() throws RecognitionException {
        EObject current = null;

        Token lv_openAccess_0_0=null;
        Token lv_shortPaper_1_0=null;
        Token lv_toolPaper_2_0=null;
        Token lv_invitedPaper_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_20=null;
        Token otherlv_22=null;
        Token otherlv_24=null;
        Token otherlv_25=null;
        Token otherlv_27=null;
        Token otherlv_29=null;
        Token otherlv_31=null;
        Token otherlv_33=null;
        Token otherlv_35=null;
        Token otherlv_37=null;
        Token otherlv_39=null;
        Token otherlv_41=null;
        Token otherlv_43=null;
        Token otherlv_45=null;
        Token otherlv_47=null;
        Token otherlv_49=null;
        Token otherlv_51=null;
        Token otherlv_52=null;
        Token otherlv_54=null;
        Token otherlv_56=null;
        Token otherlv_57=null;
        Token otherlv_58=null;
        Token otherlv_60=null;
        Token otherlv_62=null;
        Token otherlv_63=null;
        Token otherlv_64=null;
        Token otherlv_66=null;
        Token otherlv_68=null;
        Token otherlv_69=null;
        Token otherlv_71=null;
        Token otherlv_72=null;
        Token otherlv_74=null;
        Token otherlv_76=null;
        Token otherlv_77=null;
        Token otherlv_79=null;
        AntlrDatatypeRuleToken lv_citekey_5_0 = null;

        AntlrDatatypeRuleToken lv_title_8_0 = null;

        AntlrDatatypeRuleToken lv_URL_10_0 = null;

        AntlrDatatypeRuleToken lv_published_12_0 = null;

        AntlrDatatypeRuleToken lv_DOI_14_0 = null;

        AntlrDatatypeRuleToken lv_abstract_16_0 = null;

        AntlrDatatypeRuleToken lv_withAuthorVersion_18_0 = null;

        AntlrDatatypeRuleToken lv_notes_21_0 = null;

        AntlrDatatypeRuleToken lv_notes_23_0 = null;

        AntlrDatatypeRuleToken lv_booktitle_26_0 = null;

        AntlrDatatypeRuleToken lv_publisher_28_0 = null;

        AntlrDatatypeRuleToken lv_series_30_0 = null;

        AntlrDatatypeRuleToken lv_volume_32_0 = null;

        AntlrDatatypeRuleToken lv_firstPage_34_0 = null;

        AntlrDatatypeRuleToken lv_lastPage_36_0 = null;

        AntlrDatatypeRuleToken lv_eventName_38_0 = null;

        AntlrDatatypeRuleToken lv_venue_40_0 = null;

        AntlrDatatypeRuleToken lv_eventShortName_42_0 = null;

        AntlrDatatypeRuleToken lv_trackName_44_0 = null;

        AntlrDatatypeRuleToken lv_trackShortName_46_0 = null;

        AntlrDatatypeRuleToken lv_mainEventName_48_0 = null;

        AntlrDatatypeRuleToken lv_mainEventShortName_50_0 = null;

        EObject lv_date_70_0 = null;

        EObject lv_ISBN_73_0 = null;

        EObject lv_ISBN_75_0 = null;

        EObject lv_eventEndDate_78_0 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:4534:2: ( ( ( (lv_openAccess_0_0= 'openAccess' ) ) ( (lv_shortPaper_1_0= 'shortPaper' ) ) ( (lv_toolPaper_2_0= 'toolPaper' ) ) ( (lv_invitedPaper_3_0= 'invitedPaper' ) ) otherlv_4= 'Workshop' ( (lv_citekey_5_0= ruleEString ) ) otherlv_6= '{' otherlv_7= 'title' ( (lv_title_8_0= ruleEString ) ) (otherlv_9= 'URL' ( (lv_URL_10_0= ruleEString ) ) )? otherlv_11= 'published' ( (lv_published_12_0= ruleEBoolean ) ) (otherlv_13= 'DOI' ( (lv_DOI_14_0= ruleEString ) ) )? (otherlv_15= 'abstract' ( (lv_abstract_16_0= ruleEString ) ) )? otherlv_17= 'withAuthorVersion' ( (lv_withAuthorVersion_18_0= ruleEBoolean ) ) (otherlv_19= 'notes' otherlv_20= '{' ( (lv_notes_21_0= ruleEString ) ) (otherlv_22= ',' ( (lv_notes_23_0= ruleEString ) ) )* otherlv_24= '}' )? otherlv_25= 'booktitle' ( (lv_booktitle_26_0= ruleEString ) ) (otherlv_27= 'publisher' ( (lv_publisher_28_0= ruleEString ) ) )? (otherlv_29= 'series' ( (lv_series_30_0= ruleEString ) ) )? (otherlv_31= 'volume' ( (lv_volume_32_0= ruleEString ) ) )? (otherlv_33= 'firstPage' ( (lv_firstPage_34_0= ruleEString ) ) )? (otherlv_35= 'lastPage' ( (lv_lastPage_36_0= ruleEString ) ) )? otherlv_37= 'eventName' ( (lv_eventName_38_0= ruleEString ) ) (otherlv_39= 'venue' ( (lv_venue_40_0= ruleEString ) ) )? otherlv_41= 'eventShortName' ( (lv_eventShortName_42_0= ruleEString ) ) (otherlv_43= 'trackName' ( (lv_trackName_44_0= ruleEString ) ) )? (otherlv_45= 'trackShortName' ( (lv_trackShortName_46_0= ruleEString ) ) )? (otherlv_47= 'mainEventName' ( (lv_mainEventName_48_0= ruleEString ) ) )? (otherlv_49= 'mainEventShortName' ( (lv_mainEventShortName_50_0= ruleEString ) ) )? otherlv_51= 'authors' otherlv_52= '(' ( ( ruleEString ) ) (otherlv_54= ',' ( ( ruleEString ) ) )* otherlv_56= ')' (otherlv_57= 'relatedProjects' otherlv_58= '(' ( ( ruleEString ) ) (otherlv_60= ',' ( ( ruleEString ) ) )* otherlv_62= ')' )? (otherlv_63= 'bookeditors' otherlv_64= '(' ( ( ruleEString ) ) (otherlv_66= ',' ( ( ruleEString ) ) )* otherlv_68= ')' )? otherlv_69= 'date' ( (lv_date_70_0= rulePublishedDate ) ) (otherlv_71= 'ISBN' otherlv_72= '{' ( (lv_ISBN_73_0= ruleSerialNumber ) ) (otherlv_74= ',' ( (lv_ISBN_75_0= ruleSerialNumber ) ) )* otherlv_76= '}' )? (otherlv_77= 'eventEndDate' ( (lv_eventEndDate_78_0= rulePublishedDate ) ) )? otherlv_79= '}' ) )
            // InternalTextualRCV.g:4535:2: ( ( (lv_openAccess_0_0= 'openAccess' ) ) ( (lv_shortPaper_1_0= 'shortPaper' ) ) ( (lv_toolPaper_2_0= 'toolPaper' ) ) ( (lv_invitedPaper_3_0= 'invitedPaper' ) ) otherlv_4= 'Workshop' ( (lv_citekey_5_0= ruleEString ) ) otherlv_6= '{' otherlv_7= 'title' ( (lv_title_8_0= ruleEString ) ) (otherlv_9= 'URL' ( (lv_URL_10_0= ruleEString ) ) )? otherlv_11= 'published' ( (lv_published_12_0= ruleEBoolean ) ) (otherlv_13= 'DOI' ( (lv_DOI_14_0= ruleEString ) ) )? (otherlv_15= 'abstract' ( (lv_abstract_16_0= ruleEString ) ) )? otherlv_17= 'withAuthorVersion' ( (lv_withAuthorVersion_18_0= ruleEBoolean ) ) (otherlv_19= 'notes' otherlv_20= '{' ( (lv_notes_21_0= ruleEString ) ) (otherlv_22= ',' ( (lv_notes_23_0= ruleEString ) ) )* otherlv_24= '}' )? otherlv_25= 'booktitle' ( (lv_booktitle_26_0= ruleEString ) ) (otherlv_27= 'publisher' ( (lv_publisher_28_0= ruleEString ) ) )? (otherlv_29= 'series' ( (lv_series_30_0= ruleEString ) ) )? (otherlv_31= 'volume' ( (lv_volume_32_0= ruleEString ) ) )? (otherlv_33= 'firstPage' ( (lv_firstPage_34_0= ruleEString ) ) )? (otherlv_35= 'lastPage' ( (lv_lastPage_36_0= ruleEString ) ) )? otherlv_37= 'eventName' ( (lv_eventName_38_0= ruleEString ) ) (otherlv_39= 'venue' ( (lv_venue_40_0= ruleEString ) ) )? otherlv_41= 'eventShortName' ( (lv_eventShortName_42_0= ruleEString ) ) (otherlv_43= 'trackName' ( (lv_trackName_44_0= ruleEString ) ) )? (otherlv_45= 'trackShortName' ( (lv_trackShortName_46_0= ruleEString ) ) )? (otherlv_47= 'mainEventName' ( (lv_mainEventName_48_0= ruleEString ) ) )? (otherlv_49= 'mainEventShortName' ( (lv_mainEventShortName_50_0= ruleEString ) ) )? otherlv_51= 'authors' otherlv_52= '(' ( ( ruleEString ) ) (otherlv_54= ',' ( ( ruleEString ) ) )* otherlv_56= ')' (otherlv_57= 'relatedProjects' otherlv_58= '(' ( ( ruleEString ) ) (otherlv_60= ',' ( ( ruleEString ) ) )* otherlv_62= ')' )? (otherlv_63= 'bookeditors' otherlv_64= '(' ( ( ruleEString ) ) (otherlv_66= ',' ( ( ruleEString ) ) )* otherlv_68= ')' )? otherlv_69= 'date' ( (lv_date_70_0= rulePublishedDate ) ) (otherlv_71= 'ISBN' otherlv_72= '{' ( (lv_ISBN_73_0= ruleSerialNumber ) ) (otherlv_74= ',' ( (lv_ISBN_75_0= ruleSerialNumber ) ) )* otherlv_76= '}' )? (otherlv_77= 'eventEndDate' ( (lv_eventEndDate_78_0= rulePublishedDate ) ) )? otherlv_79= '}' )
            {
            // InternalTextualRCV.g:4535:2: ( ( (lv_openAccess_0_0= 'openAccess' ) ) ( (lv_shortPaper_1_0= 'shortPaper' ) ) ( (lv_toolPaper_2_0= 'toolPaper' ) ) ( (lv_invitedPaper_3_0= 'invitedPaper' ) ) otherlv_4= 'Workshop' ( (lv_citekey_5_0= ruleEString ) ) otherlv_6= '{' otherlv_7= 'title' ( (lv_title_8_0= ruleEString ) ) (otherlv_9= 'URL' ( (lv_URL_10_0= ruleEString ) ) )? otherlv_11= 'published' ( (lv_published_12_0= ruleEBoolean ) ) (otherlv_13= 'DOI' ( (lv_DOI_14_0= ruleEString ) ) )? (otherlv_15= 'abstract' ( (lv_abstract_16_0= ruleEString ) ) )? otherlv_17= 'withAuthorVersion' ( (lv_withAuthorVersion_18_0= ruleEBoolean ) ) (otherlv_19= 'notes' otherlv_20= '{' ( (lv_notes_21_0= ruleEString ) ) (otherlv_22= ',' ( (lv_notes_23_0= ruleEString ) ) )* otherlv_24= '}' )? otherlv_25= 'booktitle' ( (lv_booktitle_26_0= ruleEString ) ) (otherlv_27= 'publisher' ( (lv_publisher_28_0= ruleEString ) ) )? (otherlv_29= 'series' ( (lv_series_30_0= ruleEString ) ) )? (otherlv_31= 'volume' ( (lv_volume_32_0= ruleEString ) ) )? (otherlv_33= 'firstPage' ( (lv_firstPage_34_0= ruleEString ) ) )? (otherlv_35= 'lastPage' ( (lv_lastPage_36_0= ruleEString ) ) )? otherlv_37= 'eventName' ( (lv_eventName_38_0= ruleEString ) ) (otherlv_39= 'venue' ( (lv_venue_40_0= ruleEString ) ) )? otherlv_41= 'eventShortName' ( (lv_eventShortName_42_0= ruleEString ) ) (otherlv_43= 'trackName' ( (lv_trackName_44_0= ruleEString ) ) )? (otherlv_45= 'trackShortName' ( (lv_trackShortName_46_0= ruleEString ) ) )? (otherlv_47= 'mainEventName' ( (lv_mainEventName_48_0= ruleEString ) ) )? (otherlv_49= 'mainEventShortName' ( (lv_mainEventShortName_50_0= ruleEString ) ) )? otherlv_51= 'authors' otherlv_52= '(' ( ( ruleEString ) ) (otherlv_54= ',' ( ( ruleEString ) ) )* otherlv_56= ')' (otherlv_57= 'relatedProjects' otherlv_58= '(' ( ( ruleEString ) ) (otherlv_60= ',' ( ( ruleEString ) ) )* otherlv_62= ')' )? (otherlv_63= 'bookeditors' otherlv_64= '(' ( ( ruleEString ) ) (otherlv_66= ',' ( ( ruleEString ) ) )* otherlv_68= ')' )? otherlv_69= 'date' ( (lv_date_70_0= rulePublishedDate ) ) (otherlv_71= 'ISBN' otherlv_72= '{' ( (lv_ISBN_73_0= ruleSerialNumber ) ) (otherlv_74= ',' ( (lv_ISBN_75_0= ruleSerialNumber ) ) )* otherlv_76= '}' )? (otherlv_77= 'eventEndDate' ( (lv_eventEndDate_78_0= rulePublishedDate ) ) )? otherlv_79= '}' )
            // InternalTextualRCV.g:4536:3: ( (lv_openAccess_0_0= 'openAccess' ) ) ( (lv_shortPaper_1_0= 'shortPaper' ) ) ( (lv_toolPaper_2_0= 'toolPaper' ) ) ( (lv_invitedPaper_3_0= 'invitedPaper' ) ) otherlv_4= 'Workshop' ( (lv_citekey_5_0= ruleEString ) ) otherlv_6= '{' otherlv_7= 'title' ( (lv_title_8_0= ruleEString ) ) (otherlv_9= 'URL' ( (lv_URL_10_0= ruleEString ) ) )? otherlv_11= 'published' ( (lv_published_12_0= ruleEBoolean ) ) (otherlv_13= 'DOI' ( (lv_DOI_14_0= ruleEString ) ) )? (otherlv_15= 'abstract' ( (lv_abstract_16_0= ruleEString ) ) )? otherlv_17= 'withAuthorVersion' ( (lv_withAuthorVersion_18_0= ruleEBoolean ) ) (otherlv_19= 'notes' otherlv_20= '{' ( (lv_notes_21_0= ruleEString ) ) (otherlv_22= ',' ( (lv_notes_23_0= ruleEString ) ) )* otherlv_24= '}' )? otherlv_25= 'booktitle' ( (lv_booktitle_26_0= ruleEString ) ) (otherlv_27= 'publisher' ( (lv_publisher_28_0= ruleEString ) ) )? (otherlv_29= 'series' ( (lv_series_30_0= ruleEString ) ) )? (otherlv_31= 'volume' ( (lv_volume_32_0= ruleEString ) ) )? (otherlv_33= 'firstPage' ( (lv_firstPage_34_0= ruleEString ) ) )? (otherlv_35= 'lastPage' ( (lv_lastPage_36_0= ruleEString ) ) )? otherlv_37= 'eventName' ( (lv_eventName_38_0= ruleEString ) ) (otherlv_39= 'venue' ( (lv_venue_40_0= ruleEString ) ) )? otherlv_41= 'eventShortName' ( (lv_eventShortName_42_0= ruleEString ) ) (otherlv_43= 'trackName' ( (lv_trackName_44_0= ruleEString ) ) )? (otherlv_45= 'trackShortName' ( (lv_trackShortName_46_0= ruleEString ) ) )? (otherlv_47= 'mainEventName' ( (lv_mainEventName_48_0= ruleEString ) ) )? (otherlv_49= 'mainEventShortName' ( (lv_mainEventShortName_50_0= ruleEString ) ) )? otherlv_51= 'authors' otherlv_52= '(' ( ( ruleEString ) ) (otherlv_54= ',' ( ( ruleEString ) ) )* otherlv_56= ')' (otherlv_57= 'relatedProjects' otherlv_58= '(' ( ( ruleEString ) ) (otherlv_60= ',' ( ( ruleEString ) ) )* otherlv_62= ')' )? (otherlv_63= 'bookeditors' otherlv_64= '(' ( ( ruleEString ) ) (otherlv_66= ',' ( ( ruleEString ) ) )* otherlv_68= ')' )? otherlv_69= 'date' ( (lv_date_70_0= rulePublishedDate ) ) (otherlv_71= 'ISBN' otherlv_72= '{' ( (lv_ISBN_73_0= ruleSerialNumber ) ) (otherlv_74= ',' ( (lv_ISBN_75_0= ruleSerialNumber ) ) )* otherlv_76= '}' )? (otherlv_77= 'eventEndDate' ( (lv_eventEndDate_78_0= rulePublishedDate ) ) )? otherlv_79= '}'
            {
            // InternalTextualRCV.g:4536:3: ( (lv_openAccess_0_0= 'openAccess' ) )
            // InternalTextualRCV.g:4537:4: (lv_openAccess_0_0= 'openAccess' )
            {
            // InternalTextualRCV.g:4537:4: (lv_openAccess_0_0= 'openAccess' )
            // InternalTextualRCV.g:4538:5: lv_openAccess_0_0= 'openAccess'
            {
            lv_openAccess_0_0=(Token)match(input,73,FOLLOW_107); 

            					newLeafNode(lv_openAccess_0_0, grammarAccess.getWorkshopAccess().getOpenAccessOpenAccessKeyword_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getWorkshopRule());
            					}
            					setWithLastConsumed(current, "openAccess", lv_openAccess_0_0 != null, "openAccess");
            				

            }


            }

            // InternalTextualRCV.g:4550:3: ( (lv_shortPaper_1_0= 'shortPaper' ) )
            // InternalTextualRCV.g:4551:4: (lv_shortPaper_1_0= 'shortPaper' )
            {
            // InternalTextualRCV.g:4551:4: (lv_shortPaper_1_0= 'shortPaper' )
            // InternalTextualRCV.g:4552:5: lv_shortPaper_1_0= 'shortPaper'
            {
            lv_shortPaper_1_0=(Token)match(input,90,FOLLOW_108); 

            					newLeafNode(lv_shortPaper_1_0, grammarAccess.getWorkshopAccess().getShortPaperShortPaperKeyword_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getWorkshopRule());
            					}
            					setWithLastConsumed(current, "shortPaper", lv_shortPaper_1_0 != null, "shortPaper");
            				

            }


            }

            // InternalTextualRCV.g:4564:3: ( (lv_toolPaper_2_0= 'toolPaper' ) )
            // InternalTextualRCV.g:4565:4: (lv_toolPaper_2_0= 'toolPaper' )
            {
            // InternalTextualRCV.g:4565:4: (lv_toolPaper_2_0= 'toolPaper' )
            // InternalTextualRCV.g:4566:5: lv_toolPaper_2_0= 'toolPaper'
            {
            lv_toolPaper_2_0=(Token)match(input,91,FOLLOW_109); 

            					newLeafNode(lv_toolPaper_2_0, grammarAccess.getWorkshopAccess().getToolPaperToolPaperKeyword_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getWorkshopRule());
            					}
            					setWithLastConsumed(current, "toolPaper", lv_toolPaper_2_0 != null, "toolPaper");
            				

            }


            }

            // InternalTextualRCV.g:4578:3: ( (lv_invitedPaper_3_0= 'invitedPaper' ) )
            // InternalTextualRCV.g:4579:4: (lv_invitedPaper_3_0= 'invitedPaper' )
            {
            // InternalTextualRCV.g:4579:4: (lv_invitedPaper_3_0= 'invitedPaper' )
            // InternalTextualRCV.g:4580:5: lv_invitedPaper_3_0= 'invitedPaper'
            {
            lv_invitedPaper_3_0=(Token)match(input,92,FOLLOW_127); 

            					newLeafNode(lv_invitedPaper_3_0, grammarAccess.getWorkshopAccess().getInvitedPaperInvitedPaperKeyword_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getWorkshopRule());
            					}
            					setWithLastConsumed(current, "invitedPaper", lv_invitedPaper_3_0 != null, "invitedPaper");
            				

            }


            }

            otherlv_4=(Token)match(input,104,FOLLOW_20); 

            			newLeafNode(otherlv_4, grammarAccess.getWorkshopAccess().getWorkshopKeyword_4());
            		
            // InternalTextualRCV.g:4596:3: ( (lv_citekey_5_0= ruleEString ) )
            // InternalTextualRCV.g:4597:4: (lv_citekey_5_0= ruleEString )
            {
            // InternalTextualRCV.g:4597:4: (lv_citekey_5_0= ruleEString )
            // InternalTextualRCV.g:4598:5: lv_citekey_5_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getWorkshopAccess().getCitekeyEStringParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_3);
            lv_citekey_5_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getWorkshopRule());
            					}
            					set(
            						current,
            						"citekey",
            						lv_citekey_5_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_6=(Token)match(input,16,FOLLOW_58); 

            			newLeafNode(otherlv_6, grammarAccess.getWorkshopAccess().getLeftCurlyBracketKeyword_6());
            		
            otherlv_7=(Token)match(input,49,FOLLOW_20); 

            			newLeafNode(otherlv_7, grammarAccess.getWorkshopAccess().getTitleKeyword_7());
            		
            // InternalTextualRCV.g:4623:3: ( (lv_title_8_0= ruleEString ) )
            // InternalTextualRCV.g:4624:4: (lv_title_8_0= ruleEString )
            {
            // InternalTextualRCV.g:4624:4: (lv_title_8_0= ruleEString )
            // InternalTextualRCV.g:4625:5: lv_title_8_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getWorkshopAccess().getTitleEStringParserRuleCall_8_0());
            				
            pushFollow(FOLLOW_89);
            lv_title_8_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getWorkshopRule());
            					}
            					set(
            						current,
            						"title",
            						lv_title_8_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:4642:3: (otherlv_9= 'URL' ( (lv_URL_10_0= ruleEString ) ) )?
            int alt98=2;
            int LA98_0 = input.LA(1);

            if ( (LA98_0==47) ) {
                alt98=1;
            }
            switch (alt98) {
                case 1 :
                    // InternalTextualRCV.g:4643:4: otherlv_9= 'URL' ( (lv_URL_10_0= ruleEString ) )
                    {
                    otherlv_9=(Token)match(input,47,FOLLOW_20); 

                    				newLeafNode(otherlv_9, grammarAccess.getWorkshopAccess().getURLKeyword_9_0());
                    			
                    // InternalTextualRCV.g:4647:4: ( (lv_URL_10_0= ruleEString ) )
                    // InternalTextualRCV.g:4648:5: (lv_URL_10_0= ruleEString )
                    {
                    // InternalTextualRCV.g:4648:5: (lv_URL_10_0= ruleEString )
                    // InternalTextualRCV.g:4649:6: lv_URL_10_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getWorkshopAccess().getURLEStringParserRuleCall_9_1_0());
                    					
                    pushFollow(FOLLOW_90);
                    lv_URL_10_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getWorkshopRule());
                    						}
                    						set(
                    							current,
                    							"URL",
                    							lv_URL_10_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_11=(Token)match(input,75,FOLLOW_91); 

            			newLeafNode(otherlv_11, grammarAccess.getWorkshopAccess().getPublishedKeyword_10());
            		
            // InternalTextualRCV.g:4671:3: ( (lv_published_12_0= ruleEBoolean ) )
            // InternalTextualRCV.g:4672:4: (lv_published_12_0= ruleEBoolean )
            {
            // InternalTextualRCV.g:4672:4: (lv_published_12_0= ruleEBoolean )
            // InternalTextualRCV.g:4673:5: lv_published_12_0= ruleEBoolean
            {

            					newCompositeNode(grammarAccess.getWorkshopAccess().getPublishedEBooleanParserRuleCall_11_0());
            				
            pushFollow(FOLLOW_92);
            lv_published_12_0=ruleEBoolean();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getWorkshopRule());
            					}
            					set(
            						current,
            						"published",
            						lv_published_12_0,
            						"org.montex.researchcv.xtext.TextualRCV.EBoolean");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:4690:3: (otherlv_13= 'DOI' ( (lv_DOI_14_0= ruleEString ) ) )?
            int alt99=2;
            int LA99_0 = input.LA(1);

            if ( (LA99_0==76) ) {
                alt99=1;
            }
            switch (alt99) {
                case 1 :
                    // InternalTextualRCV.g:4691:4: otherlv_13= 'DOI' ( (lv_DOI_14_0= ruleEString ) )
                    {
                    otherlv_13=(Token)match(input,76,FOLLOW_20); 

                    				newLeafNode(otherlv_13, grammarAccess.getWorkshopAccess().getDOIKeyword_12_0());
                    			
                    // InternalTextualRCV.g:4695:4: ( (lv_DOI_14_0= ruleEString ) )
                    // InternalTextualRCV.g:4696:5: (lv_DOI_14_0= ruleEString )
                    {
                    // InternalTextualRCV.g:4696:5: (lv_DOI_14_0= ruleEString )
                    // InternalTextualRCV.g:4697:6: lv_DOI_14_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getWorkshopAccess().getDOIEStringParserRuleCall_12_1_0());
                    					
                    pushFollow(FOLLOW_93);
                    lv_DOI_14_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getWorkshopRule());
                    						}
                    						set(
                    							current,
                    							"DOI",
                    							lv_DOI_14_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:4715:3: (otherlv_15= 'abstract' ( (lv_abstract_16_0= ruleEString ) ) )?
            int alt100=2;
            int LA100_0 = input.LA(1);

            if ( (LA100_0==77) ) {
                alt100=1;
            }
            switch (alt100) {
                case 1 :
                    // InternalTextualRCV.g:4716:4: otherlv_15= 'abstract' ( (lv_abstract_16_0= ruleEString ) )
                    {
                    otherlv_15=(Token)match(input,77,FOLLOW_20); 

                    				newLeafNode(otherlv_15, grammarAccess.getWorkshopAccess().getAbstractKeyword_13_0());
                    			
                    // InternalTextualRCV.g:4720:4: ( (lv_abstract_16_0= ruleEString ) )
                    // InternalTextualRCV.g:4721:5: (lv_abstract_16_0= ruleEString )
                    {
                    // InternalTextualRCV.g:4721:5: (lv_abstract_16_0= ruleEString )
                    // InternalTextualRCV.g:4722:6: lv_abstract_16_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getWorkshopAccess().getAbstractEStringParserRuleCall_13_1_0());
                    					
                    pushFollow(FOLLOW_94);
                    lv_abstract_16_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getWorkshopRule());
                    						}
                    						set(
                    							current,
                    							"abstract",
                    							lv_abstract_16_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_17=(Token)match(input,78,FOLLOW_91); 

            			newLeafNode(otherlv_17, grammarAccess.getWorkshopAccess().getWithAuthorVersionKeyword_14());
            		
            // InternalTextualRCV.g:4744:3: ( (lv_withAuthorVersion_18_0= ruleEBoolean ) )
            // InternalTextualRCV.g:4745:4: (lv_withAuthorVersion_18_0= ruleEBoolean )
            {
            // InternalTextualRCV.g:4745:4: (lv_withAuthorVersion_18_0= ruleEBoolean )
            // InternalTextualRCV.g:4746:5: lv_withAuthorVersion_18_0= ruleEBoolean
            {

            					newCompositeNode(grammarAccess.getWorkshopAccess().getWithAuthorVersionEBooleanParserRuleCall_15_0());
            				
            pushFollow(FOLLOW_111);
            lv_withAuthorVersion_18_0=ruleEBoolean();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getWorkshopRule());
            					}
            					set(
            						current,
            						"withAuthorVersion",
            						lv_withAuthorVersion_18_0,
            						"org.montex.researchcv.xtext.TextualRCV.EBoolean");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:4763:3: (otherlv_19= 'notes' otherlv_20= '{' ( (lv_notes_21_0= ruleEString ) ) (otherlv_22= ',' ( (lv_notes_23_0= ruleEString ) ) )* otherlv_24= '}' )?
            int alt102=2;
            int LA102_0 = input.LA(1);

            if ( (LA102_0==79) ) {
                alt102=1;
            }
            switch (alt102) {
                case 1 :
                    // InternalTextualRCV.g:4764:4: otherlv_19= 'notes' otherlv_20= '{' ( (lv_notes_21_0= ruleEString ) ) (otherlv_22= ',' ( (lv_notes_23_0= ruleEString ) ) )* otherlv_24= '}'
                    {
                    otherlv_19=(Token)match(input,79,FOLLOW_3); 

                    				newLeafNode(otherlv_19, grammarAccess.getWorkshopAccess().getNotesKeyword_16_0());
                    			
                    otherlv_20=(Token)match(input,16,FOLLOW_20); 

                    				newLeafNode(otherlv_20, grammarAccess.getWorkshopAccess().getLeftCurlyBracketKeyword_16_1());
                    			
                    // InternalTextualRCV.g:4772:4: ( (lv_notes_21_0= ruleEString ) )
                    // InternalTextualRCV.g:4773:5: (lv_notes_21_0= ruleEString )
                    {
                    // InternalTextualRCV.g:4773:5: (lv_notes_21_0= ruleEString )
                    // InternalTextualRCV.g:4774:6: lv_notes_21_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getWorkshopAccess().getNotesEStringParserRuleCall_16_2_0());
                    					
                    pushFollow(FOLLOW_40);
                    lv_notes_21_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getWorkshopRule());
                    						}
                    						add(
                    							current,
                    							"notes",
                    							lv_notes_21_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:4791:4: (otherlv_22= ',' ( (lv_notes_23_0= ruleEString ) ) )*
                    loop101:
                    do {
                        int alt101=2;
                        int LA101_0 = input.LA(1);

                        if ( (LA101_0==24) ) {
                            alt101=1;
                        }


                        switch (alt101) {
                    	case 1 :
                    	    // InternalTextualRCV.g:4792:5: otherlv_22= ',' ( (lv_notes_23_0= ruleEString ) )
                    	    {
                    	    otherlv_22=(Token)match(input,24,FOLLOW_20); 

                    	    					newLeafNode(otherlv_22, grammarAccess.getWorkshopAccess().getCommaKeyword_16_3_0());
                    	    				
                    	    // InternalTextualRCV.g:4796:5: ( (lv_notes_23_0= ruleEString ) )
                    	    // InternalTextualRCV.g:4797:6: (lv_notes_23_0= ruleEString )
                    	    {
                    	    // InternalTextualRCV.g:4797:6: (lv_notes_23_0= ruleEString )
                    	    // InternalTextualRCV.g:4798:7: lv_notes_23_0= ruleEString
                    	    {

                    	    							newCompositeNode(grammarAccess.getWorkshopAccess().getNotesEStringParserRuleCall_16_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_40);
                    	    lv_notes_23_0=ruleEString();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getWorkshopRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"notes",
                    	    								lv_notes_23_0,
                    	    								"org.montex.researchcv.xtext.TextualRCV.EString");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop101;
                        }
                    } while (true);

                    otherlv_24=(Token)match(input,18,FOLLOW_112); 

                    				newLeafNode(otherlv_24, grammarAccess.getWorkshopAccess().getRightCurlyBracketKeyword_16_4());
                    			

                    }
                    break;

            }

            otherlv_25=(Token)match(input,94,FOLLOW_20); 

            			newLeafNode(otherlv_25, grammarAccess.getWorkshopAccess().getBooktitleKeyword_17());
            		
            // InternalTextualRCV.g:4825:3: ( (lv_booktitle_26_0= ruleEString ) )
            // InternalTextualRCV.g:4826:4: (lv_booktitle_26_0= ruleEString )
            {
            // InternalTextualRCV.g:4826:4: (lv_booktitle_26_0= ruleEString )
            // InternalTextualRCV.g:4827:5: lv_booktitle_26_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getWorkshopAccess().getBooktitleEStringParserRuleCall_18_0());
            				
            pushFollow(FOLLOW_113);
            lv_booktitle_26_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getWorkshopRule());
            					}
            					set(
            						current,
            						"booktitle",
            						lv_booktitle_26_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:4844:3: (otherlv_27= 'publisher' ( (lv_publisher_28_0= ruleEString ) ) )?
            int alt103=2;
            int LA103_0 = input.LA(1);

            if ( (LA103_0==81) ) {
                alt103=1;
            }
            switch (alt103) {
                case 1 :
                    // InternalTextualRCV.g:4845:4: otherlv_27= 'publisher' ( (lv_publisher_28_0= ruleEString ) )
                    {
                    otherlv_27=(Token)match(input,81,FOLLOW_20); 

                    				newLeafNode(otherlv_27, grammarAccess.getWorkshopAccess().getPublisherKeyword_19_0());
                    			
                    // InternalTextualRCV.g:4849:4: ( (lv_publisher_28_0= ruleEString ) )
                    // InternalTextualRCV.g:4850:5: (lv_publisher_28_0= ruleEString )
                    {
                    // InternalTextualRCV.g:4850:5: (lv_publisher_28_0= ruleEString )
                    // InternalTextualRCV.g:4851:6: lv_publisher_28_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getWorkshopAccess().getPublisherEStringParserRuleCall_19_1_0());
                    					
                    pushFollow(FOLLOW_114);
                    lv_publisher_28_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getWorkshopRule());
                    						}
                    						set(
                    							current,
                    							"publisher",
                    							lv_publisher_28_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:4869:3: (otherlv_29= 'series' ( (lv_series_30_0= ruleEString ) ) )?
            int alt104=2;
            int LA104_0 = input.LA(1);

            if ( (LA104_0==95) ) {
                alt104=1;
            }
            switch (alt104) {
                case 1 :
                    // InternalTextualRCV.g:4870:4: otherlv_29= 'series' ( (lv_series_30_0= ruleEString ) )
                    {
                    otherlv_29=(Token)match(input,95,FOLLOW_20); 

                    				newLeafNode(otherlv_29, grammarAccess.getWorkshopAccess().getSeriesKeyword_20_0());
                    			
                    // InternalTextualRCV.g:4874:4: ( (lv_series_30_0= ruleEString ) )
                    // InternalTextualRCV.g:4875:5: (lv_series_30_0= ruleEString )
                    {
                    // InternalTextualRCV.g:4875:5: (lv_series_30_0= ruleEString )
                    // InternalTextualRCV.g:4876:6: lv_series_30_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getWorkshopAccess().getSeriesEStringParserRuleCall_20_1_0());
                    					
                    pushFollow(FOLLOW_115);
                    lv_series_30_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getWorkshopRule());
                    						}
                    						set(
                    							current,
                    							"series",
                    							lv_series_30_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:4894:3: (otherlv_31= 'volume' ( (lv_volume_32_0= ruleEString ) ) )?
            int alt105=2;
            int LA105_0 = input.LA(1);

            if ( (LA105_0==84) ) {
                alt105=1;
            }
            switch (alt105) {
                case 1 :
                    // InternalTextualRCV.g:4895:4: otherlv_31= 'volume' ( (lv_volume_32_0= ruleEString ) )
                    {
                    otherlv_31=(Token)match(input,84,FOLLOW_20); 

                    				newLeafNode(otherlv_31, grammarAccess.getWorkshopAccess().getVolumeKeyword_21_0());
                    			
                    // InternalTextualRCV.g:4899:4: ( (lv_volume_32_0= ruleEString ) )
                    // InternalTextualRCV.g:4900:5: (lv_volume_32_0= ruleEString )
                    {
                    // InternalTextualRCV.g:4900:5: (lv_volume_32_0= ruleEString )
                    // InternalTextualRCV.g:4901:6: lv_volume_32_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getWorkshopAccess().getVolumeEStringParserRuleCall_21_1_0());
                    					
                    pushFollow(FOLLOW_116);
                    lv_volume_32_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getWorkshopRule());
                    						}
                    						set(
                    							current,
                    							"volume",
                    							lv_volume_32_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:4919:3: (otherlv_33= 'firstPage' ( (lv_firstPage_34_0= ruleEString ) ) )?
            int alt106=2;
            int LA106_0 = input.LA(1);

            if ( (LA106_0==82) ) {
                alt106=1;
            }
            switch (alt106) {
                case 1 :
                    // InternalTextualRCV.g:4920:4: otherlv_33= 'firstPage' ( (lv_firstPage_34_0= ruleEString ) )
                    {
                    otherlv_33=(Token)match(input,82,FOLLOW_20); 

                    				newLeafNode(otherlv_33, grammarAccess.getWorkshopAccess().getFirstPageKeyword_22_0());
                    			
                    // InternalTextualRCV.g:4924:4: ( (lv_firstPage_34_0= ruleEString ) )
                    // InternalTextualRCV.g:4925:5: (lv_firstPage_34_0= ruleEString )
                    {
                    // InternalTextualRCV.g:4925:5: (lv_firstPage_34_0= ruleEString )
                    // InternalTextualRCV.g:4926:6: lv_firstPage_34_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getWorkshopAccess().getFirstPageEStringParserRuleCall_22_1_0());
                    					
                    pushFollow(FOLLOW_117);
                    lv_firstPage_34_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getWorkshopRule());
                    						}
                    						set(
                    							current,
                    							"firstPage",
                    							lv_firstPage_34_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:4944:3: (otherlv_35= 'lastPage' ( (lv_lastPage_36_0= ruleEString ) ) )?
            int alt107=2;
            int LA107_0 = input.LA(1);

            if ( (LA107_0==83) ) {
                alt107=1;
            }
            switch (alt107) {
                case 1 :
                    // InternalTextualRCV.g:4945:4: otherlv_35= 'lastPage' ( (lv_lastPage_36_0= ruleEString ) )
                    {
                    otherlv_35=(Token)match(input,83,FOLLOW_20); 

                    				newLeafNode(otherlv_35, grammarAccess.getWorkshopAccess().getLastPageKeyword_23_0());
                    			
                    // InternalTextualRCV.g:4949:4: ( (lv_lastPage_36_0= ruleEString ) )
                    // InternalTextualRCV.g:4950:5: (lv_lastPage_36_0= ruleEString )
                    {
                    // InternalTextualRCV.g:4950:5: (lv_lastPage_36_0= ruleEString )
                    // InternalTextualRCV.g:4951:6: lv_lastPage_36_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getWorkshopAccess().getLastPageEStringParserRuleCall_23_1_0());
                    					
                    pushFollow(FOLLOW_118);
                    lv_lastPage_36_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getWorkshopRule());
                    						}
                    						set(
                    							current,
                    							"lastPage",
                    							lv_lastPage_36_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_37=(Token)match(input,96,FOLLOW_20); 

            			newLeafNode(otherlv_37, grammarAccess.getWorkshopAccess().getEventNameKeyword_24());
            		
            // InternalTextualRCV.g:4973:3: ( (lv_eventName_38_0= ruleEString ) )
            // InternalTextualRCV.g:4974:4: (lv_eventName_38_0= ruleEString )
            {
            // InternalTextualRCV.g:4974:4: (lv_eventName_38_0= ruleEString )
            // InternalTextualRCV.g:4975:5: lv_eventName_38_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getWorkshopAccess().getEventNameEStringParserRuleCall_25_0());
            				
            pushFollow(FOLLOW_119);
            lv_eventName_38_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getWorkshopRule());
            					}
            					set(
            						current,
            						"eventName",
            						lv_eventName_38_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:4992:3: (otherlv_39= 'venue' ( (lv_venue_40_0= ruleEString ) ) )?
            int alt108=2;
            int LA108_0 = input.LA(1);

            if ( (LA108_0==97) ) {
                alt108=1;
            }
            switch (alt108) {
                case 1 :
                    // InternalTextualRCV.g:4993:4: otherlv_39= 'venue' ( (lv_venue_40_0= ruleEString ) )
                    {
                    otherlv_39=(Token)match(input,97,FOLLOW_20); 

                    				newLeafNode(otherlv_39, grammarAccess.getWorkshopAccess().getVenueKeyword_26_0());
                    			
                    // InternalTextualRCV.g:4997:4: ( (lv_venue_40_0= ruleEString ) )
                    // InternalTextualRCV.g:4998:5: (lv_venue_40_0= ruleEString )
                    {
                    // InternalTextualRCV.g:4998:5: (lv_venue_40_0= ruleEString )
                    // InternalTextualRCV.g:4999:6: lv_venue_40_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getWorkshopAccess().getVenueEStringParserRuleCall_26_1_0());
                    					
                    pushFollow(FOLLOW_120);
                    lv_venue_40_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getWorkshopRule());
                    						}
                    						set(
                    							current,
                    							"venue",
                    							lv_venue_40_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_41=(Token)match(input,98,FOLLOW_20); 

            			newLeafNode(otherlv_41, grammarAccess.getWorkshopAccess().getEventShortNameKeyword_27());
            		
            // InternalTextualRCV.g:5021:3: ( (lv_eventShortName_42_0= ruleEString ) )
            // InternalTextualRCV.g:5022:4: (lv_eventShortName_42_0= ruleEString )
            {
            // InternalTextualRCV.g:5022:4: (lv_eventShortName_42_0= ruleEString )
            // InternalTextualRCV.g:5023:5: lv_eventShortName_42_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getWorkshopAccess().getEventShortNameEStringParserRuleCall_28_0());
            				
            pushFollow(FOLLOW_128);
            lv_eventShortName_42_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getWorkshopRule());
            					}
            					set(
            						current,
            						"eventShortName",
            						lv_eventShortName_42_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:5040:3: (otherlv_43= 'trackName' ( (lv_trackName_44_0= ruleEString ) ) )?
            int alt109=2;
            int LA109_0 = input.LA(1);

            if ( (LA109_0==99) ) {
                alt109=1;
            }
            switch (alt109) {
                case 1 :
                    // InternalTextualRCV.g:5041:4: otherlv_43= 'trackName' ( (lv_trackName_44_0= ruleEString ) )
                    {
                    otherlv_43=(Token)match(input,99,FOLLOW_20); 

                    				newLeafNode(otherlv_43, grammarAccess.getWorkshopAccess().getTrackNameKeyword_29_0());
                    			
                    // InternalTextualRCV.g:5045:4: ( (lv_trackName_44_0= ruleEString ) )
                    // InternalTextualRCV.g:5046:5: (lv_trackName_44_0= ruleEString )
                    {
                    // InternalTextualRCV.g:5046:5: (lv_trackName_44_0= ruleEString )
                    // InternalTextualRCV.g:5047:6: lv_trackName_44_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getWorkshopAccess().getTrackNameEStringParserRuleCall_29_1_0());
                    					
                    pushFollow(FOLLOW_129);
                    lv_trackName_44_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getWorkshopRule());
                    						}
                    						set(
                    							current,
                    							"trackName",
                    							lv_trackName_44_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:5065:3: (otherlv_45= 'trackShortName' ( (lv_trackShortName_46_0= ruleEString ) ) )?
            int alt110=2;
            int LA110_0 = input.LA(1);

            if ( (LA110_0==100) ) {
                alt110=1;
            }
            switch (alt110) {
                case 1 :
                    // InternalTextualRCV.g:5066:4: otherlv_45= 'trackShortName' ( (lv_trackShortName_46_0= ruleEString ) )
                    {
                    otherlv_45=(Token)match(input,100,FOLLOW_20); 

                    				newLeafNode(otherlv_45, grammarAccess.getWorkshopAccess().getTrackShortNameKeyword_30_0());
                    			
                    // InternalTextualRCV.g:5070:4: ( (lv_trackShortName_46_0= ruleEString ) )
                    // InternalTextualRCV.g:5071:5: (lv_trackShortName_46_0= ruleEString )
                    {
                    // InternalTextualRCV.g:5071:5: (lv_trackShortName_46_0= ruleEString )
                    // InternalTextualRCV.g:5072:6: lv_trackShortName_46_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getWorkshopAccess().getTrackShortNameEStringParserRuleCall_30_1_0());
                    					
                    pushFollow(FOLLOW_130);
                    lv_trackShortName_46_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getWorkshopRule());
                    						}
                    						set(
                    							current,
                    							"trackShortName",
                    							lv_trackShortName_46_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:5090:3: (otherlv_47= 'mainEventName' ( (lv_mainEventName_48_0= ruleEString ) ) )?
            int alt111=2;
            int LA111_0 = input.LA(1);

            if ( (LA111_0==105) ) {
                alt111=1;
            }
            switch (alt111) {
                case 1 :
                    // InternalTextualRCV.g:5091:4: otherlv_47= 'mainEventName' ( (lv_mainEventName_48_0= ruleEString ) )
                    {
                    otherlv_47=(Token)match(input,105,FOLLOW_20); 

                    				newLeafNode(otherlv_47, grammarAccess.getWorkshopAccess().getMainEventNameKeyword_31_0());
                    			
                    // InternalTextualRCV.g:5095:4: ( (lv_mainEventName_48_0= ruleEString ) )
                    // InternalTextualRCV.g:5096:5: (lv_mainEventName_48_0= ruleEString )
                    {
                    // InternalTextualRCV.g:5096:5: (lv_mainEventName_48_0= ruleEString )
                    // InternalTextualRCV.g:5097:6: lv_mainEventName_48_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getWorkshopAccess().getMainEventNameEStringParserRuleCall_31_1_0());
                    					
                    pushFollow(FOLLOW_131);
                    lv_mainEventName_48_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getWorkshopRule());
                    						}
                    						set(
                    							current,
                    							"mainEventName",
                    							lv_mainEventName_48_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:5115:3: (otherlv_49= 'mainEventShortName' ( (lv_mainEventShortName_50_0= ruleEString ) ) )?
            int alt112=2;
            int LA112_0 = input.LA(1);

            if ( (LA112_0==106) ) {
                alt112=1;
            }
            switch (alt112) {
                case 1 :
                    // InternalTextualRCV.g:5116:4: otherlv_49= 'mainEventShortName' ( (lv_mainEventShortName_50_0= ruleEString ) )
                    {
                    otherlv_49=(Token)match(input,106,FOLLOW_20); 

                    				newLeafNode(otherlv_49, grammarAccess.getWorkshopAccess().getMainEventShortNameKeyword_32_0());
                    			
                    // InternalTextualRCV.g:5120:4: ( (lv_mainEventShortName_50_0= ruleEString ) )
                    // InternalTextualRCV.g:5121:5: (lv_mainEventShortName_50_0= ruleEString )
                    {
                    // InternalTextualRCV.g:5121:5: (lv_mainEventShortName_50_0= ruleEString )
                    // InternalTextualRCV.g:5122:6: lv_mainEventShortName_50_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getWorkshopAccess().getMainEventShortNameEStringParserRuleCall_32_1_0());
                    					
                    pushFollow(FOLLOW_102);
                    lv_mainEventShortName_50_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getWorkshopRule());
                    						}
                    						set(
                    							current,
                    							"mainEventShortName",
                    							lv_mainEventShortName_50_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_51=(Token)match(input,86,FOLLOW_61); 

            			newLeafNode(otherlv_51, grammarAccess.getWorkshopAccess().getAuthorsKeyword_33());
            		
            otherlv_52=(Token)match(input,52,FOLLOW_20); 

            			newLeafNode(otherlv_52, grammarAccess.getWorkshopAccess().getLeftParenthesisKeyword_34());
            		
            // InternalTextualRCV.g:5148:3: ( ( ruleEString ) )
            // InternalTextualRCV.g:5149:4: ( ruleEString )
            {
            // InternalTextualRCV.g:5149:4: ( ruleEString )
            // InternalTextualRCV.g:5150:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getWorkshopRule());
            					}
            				

            					newCompositeNode(grammarAccess.getWorkshopAccess().getAuthorsPersonCrossReference_35_0());
            				
            pushFollow(FOLLOW_62);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:5164:3: (otherlv_54= ',' ( ( ruleEString ) ) )*
            loop113:
            do {
                int alt113=2;
                int LA113_0 = input.LA(1);

                if ( (LA113_0==24) ) {
                    alt113=1;
                }


                switch (alt113) {
            	case 1 :
            	    // InternalTextualRCV.g:5165:4: otherlv_54= ',' ( ( ruleEString ) )
            	    {
            	    otherlv_54=(Token)match(input,24,FOLLOW_20); 

            	    				newLeafNode(otherlv_54, grammarAccess.getWorkshopAccess().getCommaKeyword_36_0());
            	    			
            	    // InternalTextualRCV.g:5169:4: ( ( ruleEString ) )
            	    // InternalTextualRCV.g:5170:5: ( ruleEString )
            	    {
            	    // InternalTextualRCV.g:5170:5: ( ruleEString )
            	    // InternalTextualRCV.g:5171:6: ruleEString
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getWorkshopRule());
            	    						}
            	    					

            	    						newCompositeNode(grammarAccess.getWorkshopAccess().getAuthorsPersonCrossReference_36_1_0());
            	    					
            	    pushFollow(FOLLOW_62);
            	    ruleEString();

            	    state._fsp--;


            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop113;
                }
            } while (true);

            otherlv_56=(Token)match(input,53,FOLLOW_123); 

            			newLeafNode(otherlv_56, grammarAccess.getWorkshopAccess().getRightParenthesisKeyword_37());
            		
            // InternalTextualRCV.g:5190:3: (otherlv_57= 'relatedProjects' otherlv_58= '(' ( ( ruleEString ) ) (otherlv_60= ',' ( ( ruleEString ) ) )* otherlv_62= ')' )?
            int alt115=2;
            int LA115_0 = input.LA(1);

            if ( (LA115_0==87) ) {
                alt115=1;
            }
            switch (alt115) {
                case 1 :
                    // InternalTextualRCV.g:5191:4: otherlv_57= 'relatedProjects' otherlv_58= '(' ( ( ruleEString ) ) (otherlv_60= ',' ( ( ruleEString ) ) )* otherlv_62= ')'
                    {
                    otherlv_57=(Token)match(input,87,FOLLOW_61); 

                    				newLeafNode(otherlv_57, grammarAccess.getWorkshopAccess().getRelatedProjectsKeyword_38_0());
                    			
                    otherlv_58=(Token)match(input,52,FOLLOW_20); 

                    				newLeafNode(otherlv_58, grammarAccess.getWorkshopAccess().getLeftParenthesisKeyword_38_1());
                    			
                    // InternalTextualRCV.g:5199:4: ( ( ruleEString ) )
                    // InternalTextualRCV.g:5200:5: ( ruleEString )
                    {
                    // InternalTextualRCV.g:5200:5: ( ruleEString )
                    // InternalTextualRCV.g:5201:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getWorkshopRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getWorkshopAccess().getRelatedProjectsGrantCrossReference_38_2_0());
                    					
                    pushFollow(FOLLOW_62);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:5215:4: (otherlv_60= ',' ( ( ruleEString ) ) )*
                    loop114:
                    do {
                        int alt114=2;
                        int LA114_0 = input.LA(1);

                        if ( (LA114_0==24) ) {
                            alt114=1;
                        }


                        switch (alt114) {
                    	case 1 :
                    	    // InternalTextualRCV.g:5216:5: otherlv_60= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_60=(Token)match(input,24,FOLLOW_20); 

                    	    					newLeafNode(otherlv_60, grammarAccess.getWorkshopAccess().getCommaKeyword_38_3_0());
                    	    				
                    	    // InternalTextualRCV.g:5220:5: ( ( ruleEString ) )
                    	    // InternalTextualRCV.g:5221:6: ( ruleEString )
                    	    {
                    	    // InternalTextualRCV.g:5221:6: ( ruleEString )
                    	    // InternalTextualRCV.g:5222:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getWorkshopRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getWorkshopAccess().getRelatedProjectsGrantCrossReference_38_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_62);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop114;
                        }
                    } while (true);

                    otherlv_62=(Token)match(input,53,FOLLOW_124); 

                    				newLeafNode(otherlv_62, grammarAccess.getWorkshopAccess().getRightParenthesisKeyword_38_4());
                    			

                    }
                    break;

            }

            // InternalTextualRCV.g:5242:3: (otherlv_63= 'bookeditors' otherlv_64= '(' ( ( ruleEString ) ) (otherlv_66= ',' ( ( ruleEString ) ) )* otherlv_68= ')' )?
            int alt117=2;
            int LA117_0 = input.LA(1);

            if ( (LA117_0==101) ) {
                alt117=1;
            }
            switch (alt117) {
                case 1 :
                    // InternalTextualRCV.g:5243:4: otherlv_63= 'bookeditors' otherlv_64= '(' ( ( ruleEString ) ) (otherlv_66= ',' ( ( ruleEString ) ) )* otherlv_68= ')'
                    {
                    otherlv_63=(Token)match(input,101,FOLLOW_61); 

                    				newLeafNode(otherlv_63, grammarAccess.getWorkshopAccess().getBookeditorsKeyword_39_0());
                    			
                    otherlv_64=(Token)match(input,52,FOLLOW_20); 

                    				newLeafNode(otherlv_64, grammarAccess.getWorkshopAccess().getLeftParenthesisKeyword_39_1());
                    			
                    // InternalTextualRCV.g:5251:4: ( ( ruleEString ) )
                    // InternalTextualRCV.g:5252:5: ( ruleEString )
                    {
                    // InternalTextualRCV.g:5252:5: ( ruleEString )
                    // InternalTextualRCV.g:5253:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getWorkshopRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getWorkshopAccess().getBookeditorsPersonCrossReference_39_2_0());
                    					
                    pushFollow(FOLLOW_62);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:5267:4: (otherlv_66= ',' ( ( ruleEString ) ) )*
                    loop116:
                    do {
                        int alt116=2;
                        int LA116_0 = input.LA(1);

                        if ( (LA116_0==24) ) {
                            alt116=1;
                        }


                        switch (alt116) {
                    	case 1 :
                    	    // InternalTextualRCV.g:5268:5: otherlv_66= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_66=(Token)match(input,24,FOLLOW_20); 

                    	    					newLeafNode(otherlv_66, grammarAccess.getWorkshopAccess().getCommaKeyword_39_3_0());
                    	    				
                    	    // InternalTextualRCV.g:5272:5: ( ( ruleEString ) )
                    	    // InternalTextualRCV.g:5273:6: ( ruleEString )
                    	    {
                    	    // InternalTextualRCV.g:5273:6: ( ruleEString )
                    	    // InternalTextualRCV.g:5274:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getWorkshopRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getWorkshopAccess().getBookeditorsPersonCrossReference_39_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_62);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop116;
                        }
                    } while (true);

                    otherlv_68=(Token)match(input,53,FOLLOW_104); 

                    				newLeafNode(otherlv_68, grammarAccess.getWorkshopAccess().getRightParenthesisKeyword_39_4());
                    			

                    }
                    break;

            }

            otherlv_69=(Token)match(input,88,FOLLOW_9); 

            			newLeafNode(otherlv_69, grammarAccess.getWorkshopAccess().getDateKeyword_40());
            		
            // InternalTextualRCV.g:5298:3: ( (lv_date_70_0= rulePublishedDate ) )
            // InternalTextualRCV.g:5299:4: (lv_date_70_0= rulePublishedDate )
            {
            // InternalTextualRCV.g:5299:4: (lv_date_70_0= rulePublishedDate )
            // InternalTextualRCV.g:5300:5: lv_date_70_0= rulePublishedDate
            {

            					newCompositeNode(grammarAccess.getWorkshopAccess().getDatePublishedDateParserRuleCall_41_0());
            				
            pushFollow(FOLLOW_125);
            lv_date_70_0=rulePublishedDate();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getWorkshopRule());
            					}
            					set(
            						current,
            						"date",
            						lv_date_70_0,
            						"org.montex.researchcv.xtext.TextualRCV.PublishedDate");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:5317:3: (otherlv_71= 'ISBN' otherlv_72= '{' ( (lv_ISBN_73_0= ruleSerialNumber ) ) (otherlv_74= ',' ( (lv_ISBN_75_0= ruleSerialNumber ) ) )* otherlv_76= '}' )?
            int alt119=2;
            int LA119_0 = input.LA(1);

            if ( (LA119_0==102) ) {
                alt119=1;
            }
            switch (alt119) {
                case 1 :
                    // InternalTextualRCV.g:5318:4: otherlv_71= 'ISBN' otherlv_72= '{' ( (lv_ISBN_73_0= ruleSerialNumber ) ) (otherlv_74= ',' ( (lv_ISBN_75_0= ruleSerialNumber ) ) )* otherlv_76= '}'
                    {
                    otherlv_71=(Token)match(input,102,FOLLOW_3); 

                    				newLeafNode(otherlv_71, grammarAccess.getWorkshopAccess().getISBNKeyword_42_0());
                    			
                    otherlv_72=(Token)match(input,16,FOLLOW_106); 

                    				newLeafNode(otherlv_72, grammarAccess.getWorkshopAccess().getLeftCurlyBracketKeyword_42_1());
                    			
                    // InternalTextualRCV.g:5326:4: ( (lv_ISBN_73_0= ruleSerialNumber ) )
                    // InternalTextualRCV.g:5327:5: (lv_ISBN_73_0= ruleSerialNumber )
                    {
                    // InternalTextualRCV.g:5327:5: (lv_ISBN_73_0= ruleSerialNumber )
                    // InternalTextualRCV.g:5328:6: lv_ISBN_73_0= ruleSerialNumber
                    {

                    						newCompositeNode(grammarAccess.getWorkshopAccess().getISBNSerialNumberParserRuleCall_42_2_0());
                    					
                    pushFollow(FOLLOW_40);
                    lv_ISBN_73_0=ruleSerialNumber();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getWorkshopRule());
                    						}
                    						add(
                    							current,
                    							"ISBN",
                    							lv_ISBN_73_0,
                    							"org.montex.researchcv.xtext.TextualRCV.SerialNumber");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:5345:4: (otherlv_74= ',' ( (lv_ISBN_75_0= ruleSerialNumber ) ) )*
                    loop118:
                    do {
                        int alt118=2;
                        int LA118_0 = input.LA(1);

                        if ( (LA118_0==24) ) {
                            alt118=1;
                        }


                        switch (alt118) {
                    	case 1 :
                    	    // InternalTextualRCV.g:5346:5: otherlv_74= ',' ( (lv_ISBN_75_0= ruleSerialNumber ) )
                    	    {
                    	    otherlv_74=(Token)match(input,24,FOLLOW_106); 

                    	    					newLeafNode(otherlv_74, grammarAccess.getWorkshopAccess().getCommaKeyword_42_3_0());
                    	    				
                    	    // InternalTextualRCV.g:5350:5: ( (lv_ISBN_75_0= ruleSerialNumber ) )
                    	    // InternalTextualRCV.g:5351:6: (lv_ISBN_75_0= ruleSerialNumber )
                    	    {
                    	    // InternalTextualRCV.g:5351:6: (lv_ISBN_75_0= ruleSerialNumber )
                    	    // InternalTextualRCV.g:5352:7: lv_ISBN_75_0= ruleSerialNumber
                    	    {

                    	    							newCompositeNode(grammarAccess.getWorkshopAccess().getISBNSerialNumberParserRuleCall_42_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_40);
                    	    lv_ISBN_75_0=ruleSerialNumber();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getWorkshopRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"ISBN",
                    	    								lv_ISBN_75_0,
                    	    								"org.montex.researchcv.xtext.TextualRCV.SerialNumber");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop118;
                        }
                    } while (true);

                    otherlv_76=(Token)match(input,18,FOLLOW_126); 

                    				newLeafNode(otherlv_76, grammarAccess.getWorkshopAccess().getRightCurlyBracketKeyword_42_4());
                    			

                    }
                    break;

            }

            // InternalTextualRCV.g:5375:3: (otherlv_77= 'eventEndDate' ( (lv_eventEndDate_78_0= rulePublishedDate ) ) )?
            int alt120=2;
            int LA120_0 = input.LA(1);

            if ( (LA120_0==103) ) {
                alt120=1;
            }
            switch (alt120) {
                case 1 :
                    // InternalTextualRCV.g:5376:4: otherlv_77= 'eventEndDate' ( (lv_eventEndDate_78_0= rulePublishedDate ) )
                    {
                    otherlv_77=(Token)match(input,103,FOLLOW_9); 

                    				newLeafNode(otherlv_77, grammarAccess.getWorkshopAccess().getEventEndDateKeyword_43_0());
                    			
                    // InternalTextualRCV.g:5380:4: ( (lv_eventEndDate_78_0= rulePublishedDate ) )
                    // InternalTextualRCV.g:5381:5: (lv_eventEndDate_78_0= rulePublishedDate )
                    {
                    // InternalTextualRCV.g:5381:5: (lv_eventEndDate_78_0= rulePublishedDate )
                    // InternalTextualRCV.g:5382:6: lv_eventEndDate_78_0= rulePublishedDate
                    {

                    						newCompositeNode(grammarAccess.getWorkshopAccess().getEventEndDatePublishedDateParserRuleCall_43_1_0());
                    					
                    pushFollow(FOLLOW_27);
                    lv_eventEndDate_78_0=rulePublishedDate();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getWorkshopRule());
                    						}
                    						set(
                    							current,
                    							"eventEndDate",
                    							lv_eventEndDate_78_0,
                    							"org.montex.researchcv.xtext.TextualRCV.PublishedDate");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_79=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_79, grammarAccess.getWorkshopAccess().getRightCurlyBracketKeyword_44());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWorkshop"


    // $ANTLR start "entryRuleBook"
    // InternalTextualRCV.g:5408:1: entryRuleBook returns [EObject current=null] : iv_ruleBook= ruleBook EOF ;
    public final EObject entryRuleBook() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBook = null;


        try {
            // InternalTextualRCV.g:5408:45: (iv_ruleBook= ruleBook EOF )
            // InternalTextualRCV.g:5409:2: iv_ruleBook= ruleBook EOF
            {
             newCompositeNode(grammarAccess.getBookRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBook=ruleBook();

            state._fsp--;

             current =iv_ruleBook; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBook"


    // $ANTLR start "ruleBook"
    // InternalTextualRCV.g:5415:1: ruleBook returns [EObject current=null] : ( ( (lv_openAccess_0_0= 'openAccess' ) ) otherlv_1= 'Book' ( (lv_citekey_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'title' ( (lv_title_5_0= ruleEString ) ) (otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) ) )? otherlv_8= 'published' ( (lv_published_9_0= ruleEBoolean ) ) (otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) ) )? (otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) ) )? otherlv_14= 'withAuthorVersion' ( (lv_withAuthorVersion_15_0= ruleEBoolean ) ) (otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}' )? (otherlv_22= 'publisher' ( (lv_publisher_23_0= ruleEString ) ) )? (otherlv_24= 'series' ( (lv_series_25_0= ruleEString ) ) )? (otherlv_26= 'volume' ( (lv_volume_27_0= ruleEString ) ) )? otherlv_28= 'authors' otherlv_29= '(' ( ( ruleEString ) ) (otherlv_31= ',' ( ( ruleEString ) ) )* otherlv_33= ')' (otherlv_34= 'relatedProjects' otherlv_35= '(' ( ( ruleEString ) ) (otherlv_37= ',' ( ( ruleEString ) ) )* otherlv_39= ')' )? otherlv_40= 'date' ( (lv_date_41_0= rulePublishedDate ) ) (otherlv_42= 'ISBN' otherlv_43= '{' ( (lv_ISBN_44_0= ruleSerialNumber ) ) (otherlv_45= ',' ( (lv_ISBN_46_0= ruleSerialNumber ) ) )* otherlv_47= '}' )? otherlv_48= '}' ) ;
    public final EObject ruleBook() throws RecognitionException {
        EObject current = null;

        Token lv_openAccess_0_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_21=null;
        Token otherlv_22=null;
        Token otherlv_24=null;
        Token otherlv_26=null;
        Token otherlv_28=null;
        Token otherlv_29=null;
        Token otherlv_31=null;
        Token otherlv_33=null;
        Token otherlv_34=null;
        Token otherlv_35=null;
        Token otherlv_37=null;
        Token otherlv_39=null;
        Token otherlv_40=null;
        Token otherlv_42=null;
        Token otherlv_43=null;
        Token otherlv_45=null;
        Token otherlv_47=null;
        Token otherlv_48=null;
        AntlrDatatypeRuleToken lv_citekey_2_0 = null;

        AntlrDatatypeRuleToken lv_title_5_0 = null;

        AntlrDatatypeRuleToken lv_URL_7_0 = null;

        AntlrDatatypeRuleToken lv_published_9_0 = null;

        AntlrDatatypeRuleToken lv_DOI_11_0 = null;

        AntlrDatatypeRuleToken lv_abstract_13_0 = null;

        AntlrDatatypeRuleToken lv_withAuthorVersion_15_0 = null;

        AntlrDatatypeRuleToken lv_notes_18_0 = null;

        AntlrDatatypeRuleToken lv_notes_20_0 = null;

        AntlrDatatypeRuleToken lv_publisher_23_0 = null;

        AntlrDatatypeRuleToken lv_series_25_0 = null;

        AntlrDatatypeRuleToken lv_volume_27_0 = null;

        EObject lv_date_41_0 = null;

        EObject lv_ISBN_44_0 = null;

        EObject lv_ISBN_46_0 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:5421:2: ( ( ( (lv_openAccess_0_0= 'openAccess' ) ) otherlv_1= 'Book' ( (lv_citekey_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'title' ( (lv_title_5_0= ruleEString ) ) (otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) ) )? otherlv_8= 'published' ( (lv_published_9_0= ruleEBoolean ) ) (otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) ) )? (otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) ) )? otherlv_14= 'withAuthorVersion' ( (lv_withAuthorVersion_15_0= ruleEBoolean ) ) (otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}' )? (otherlv_22= 'publisher' ( (lv_publisher_23_0= ruleEString ) ) )? (otherlv_24= 'series' ( (lv_series_25_0= ruleEString ) ) )? (otherlv_26= 'volume' ( (lv_volume_27_0= ruleEString ) ) )? otherlv_28= 'authors' otherlv_29= '(' ( ( ruleEString ) ) (otherlv_31= ',' ( ( ruleEString ) ) )* otherlv_33= ')' (otherlv_34= 'relatedProjects' otherlv_35= '(' ( ( ruleEString ) ) (otherlv_37= ',' ( ( ruleEString ) ) )* otherlv_39= ')' )? otherlv_40= 'date' ( (lv_date_41_0= rulePublishedDate ) ) (otherlv_42= 'ISBN' otherlv_43= '{' ( (lv_ISBN_44_0= ruleSerialNumber ) ) (otherlv_45= ',' ( (lv_ISBN_46_0= ruleSerialNumber ) ) )* otherlv_47= '}' )? otherlv_48= '}' ) )
            // InternalTextualRCV.g:5422:2: ( ( (lv_openAccess_0_0= 'openAccess' ) ) otherlv_1= 'Book' ( (lv_citekey_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'title' ( (lv_title_5_0= ruleEString ) ) (otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) ) )? otherlv_8= 'published' ( (lv_published_9_0= ruleEBoolean ) ) (otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) ) )? (otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) ) )? otherlv_14= 'withAuthorVersion' ( (lv_withAuthorVersion_15_0= ruleEBoolean ) ) (otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}' )? (otherlv_22= 'publisher' ( (lv_publisher_23_0= ruleEString ) ) )? (otherlv_24= 'series' ( (lv_series_25_0= ruleEString ) ) )? (otherlv_26= 'volume' ( (lv_volume_27_0= ruleEString ) ) )? otherlv_28= 'authors' otherlv_29= '(' ( ( ruleEString ) ) (otherlv_31= ',' ( ( ruleEString ) ) )* otherlv_33= ')' (otherlv_34= 'relatedProjects' otherlv_35= '(' ( ( ruleEString ) ) (otherlv_37= ',' ( ( ruleEString ) ) )* otherlv_39= ')' )? otherlv_40= 'date' ( (lv_date_41_0= rulePublishedDate ) ) (otherlv_42= 'ISBN' otherlv_43= '{' ( (lv_ISBN_44_0= ruleSerialNumber ) ) (otherlv_45= ',' ( (lv_ISBN_46_0= ruleSerialNumber ) ) )* otherlv_47= '}' )? otherlv_48= '}' )
            {
            // InternalTextualRCV.g:5422:2: ( ( (lv_openAccess_0_0= 'openAccess' ) ) otherlv_1= 'Book' ( (lv_citekey_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'title' ( (lv_title_5_0= ruleEString ) ) (otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) ) )? otherlv_8= 'published' ( (lv_published_9_0= ruleEBoolean ) ) (otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) ) )? (otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) ) )? otherlv_14= 'withAuthorVersion' ( (lv_withAuthorVersion_15_0= ruleEBoolean ) ) (otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}' )? (otherlv_22= 'publisher' ( (lv_publisher_23_0= ruleEString ) ) )? (otherlv_24= 'series' ( (lv_series_25_0= ruleEString ) ) )? (otherlv_26= 'volume' ( (lv_volume_27_0= ruleEString ) ) )? otherlv_28= 'authors' otherlv_29= '(' ( ( ruleEString ) ) (otherlv_31= ',' ( ( ruleEString ) ) )* otherlv_33= ')' (otherlv_34= 'relatedProjects' otherlv_35= '(' ( ( ruleEString ) ) (otherlv_37= ',' ( ( ruleEString ) ) )* otherlv_39= ')' )? otherlv_40= 'date' ( (lv_date_41_0= rulePublishedDate ) ) (otherlv_42= 'ISBN' otherlv_43= '{' ( (lv_ISBN_44_0= ruleSerialNumber ) ) (otherlv_45= ',' ( (lv_ISBN_46_0= ruleSerialNumber ) ) )* otherlv_47= '}' )? otherlv_48= '}' )
            // InternalTextualRCV.g:5423:3: ( (lv_openAccess_0_0= 'openAccess' ) ) otherlv_1= 'Book' ( (lv_citekey_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'title' ( (lv_title_5_0= ruleEString ) ) (otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) ) )? otherlv_8= 'published' ( (lv_published_9_0= ruleEBoolean ) ) (otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) ) )? (otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) ) )? otherlv_14= 'withAuthorVersion' ( (lv_withAuthorVersion_15_0= ruleEBoolean ) ) (otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}' )? (otherlv_22= 'publisher' ( (lv_publisher_23_0= ruleEString ) ) )? (otherlv_24= 'series' ( (lv_series_25_0= ruleEString ) ) )? (otherlv_26= 'volume' ( (lv_volume_27_0= ruleEString ) ) )? otherlv_28= 'authors' otherlv_29= '(' ( ( ruleEString ) ) (otherlv_31= ',' ( ( ruleEString ) ) )* otherlv_33= ')' (otherlv_34= 'relatedProjects' otherlv_35= '(' ( ( ruleEString ) ) (otherlv_37= ',' ( ( ruleEString ) ) )* otherlv_39= ')' )? otherlv_40= 'date' ( (lv_date_41_0= rulePublishedDate ) ) (otherlv_42= 'ISBN' otherlv_43= '{' ( (lv_ISBN_44_0= ruleSerialNumber ) ) (otherlv_45= ',' ( (lv_ISBN_46_0= ruleSerialNumber ) ) )* otherlv_47= '}' )? otherlv_48= '}'
            {
            // InternalTextualRCV.g:5423:3: ( (lv_openAccess_0_0= 'openAccess' ) )
            // InternalTextualRCV.g:5424:4: (lv_openAccess_0_0= 'openAccess' )
            {
            // InternalTextualRCV.g:5424:4: (lv_openAccess_0_0= 'openAccess' )
            // InternalTextualRCV.g:5425:5: lv_openAccess_0_0= 'openAccess'
            {
            lv_openAccess_0_0=(Token)match(input,73,FOLLOW_132); 

            					newLeafNode(lv_openAccess_0_0, grammarAccess.getBookAccess().getOpenAccessOpenAccessKeyword_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getBookRule());
            					}
            					setWithLastConsumed(current, "openAccess", lv_openAccess_0_0 != null, "openAccess");
            				

            }


            }

            otherlv_1=(Token)match(input,107,FOLLOW_20); 

            			newLeafNode(otherlv_1, grammarAccess.getBookAccess().getBookKeyword_1());
            		
            // InternalTextualRCV.g:5441:3: ( (lv_citekey_2_0= ruleEString ) )
            // InternalTextualRCV.g:5442:4: (lv_citekey_2_0= ruleEString )
            {
            // InternalTextualRCV.g:5442:4: (lv_citekey_2_0= ruleEString )
            // InternalTextualRCV.g:5443:5: lv_citekey_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getBookAccess().getCitekeyEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_3);
            lv_citekey_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getBookRule());
            					}
            					set(
            						current,
            						"citekey",
            						lv_citekey_2_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,16,FOLLOW_58); 

            			newLeafNode(otherlv_3, grammarAccess.getBookAccess().getLeftCurlyBracketKeyword_3());
            		
            otherlv_4=(Token)match(input,49,FOLLOW_20); 

            			newLeafNode(otherlv_4, grammarAccess.getBookAccess().getTitleKeyword_4());
            		
            // InternalTextualRCV.g:5468:3: ( (lv_title_5_0= ruleEString ) )
            // InternalTextualRCV.g:5469:4: (lv_title_5_0= ruleEString )
            {
            // InternalTextualRCV.g:5469:4: (lv_title_5_0= ruleEString )
            // InternalTextualRCV.g:5470:5: lv_title_5_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getBookAccess().getTitleEStringParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_89);
            lv_title_5_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getBookRule());
            					}
            					set(
            						current,
            						"title",
            						lv_title_5_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:5487:3: (otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) ) )?
            int alt121=2;
            int LA121_0 = input.LA(1);

            if ( (LA121_0==47) ) {
                alt121=1;
            }
            switch (alt121) {
                case 1 :
                    // InternalTextualRCV.g:5488:4: otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) )
                    {
                    otherlv_6=(Token)match(input,47,FOLLOW_20); 

                    				newLeafNode(otherlv_6, grammarAccess.getBookAccess().getURLKeyword_6_0());
                    			
                    // InternalTextualRCV.g:5492:4: ( (lv_URL_7_0= ruleEString ) )
                    // InternalTextualRCV.g:5493:5: (lv_URL_7_0= ruleEString )
                    {
                    // InternalTextualRCV.g:5493:5: (lv_URL_7_0= ruleEString )
                    // InternalTextualRCV.g:5494:6: lv_URL_7_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getBookAccess().getURLEStringParserRuleCall_6_1_0());
                    					
                    pushFollow(FOLLOW_90);
                    lv_URL_7_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getBookRule());
                    						}
                    						set(
                    							current,
                    							"URL",
                    							lv_URL_7_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_8=(Token)match(input,75,FOLLOW_91); 

            			newLeafNode(otherlv_8, grammarAccess.getBookAccess().getPublishedKeyword_7());
            		
            // InternalTextualRCV.g:5516:3: ( (lv_published_9_0= ruleEBoolean ) )
            // InternalTextualRCV.g:5517:4: (lv_published_9_0= ruleEBoolean )
            {
            // InternalTextualRCV.g:5517:4: (lv_published_9_0= ruleEBoolean )
            // InternalTextualRCV.g:5518:5: lv_published_9_0= ruleEBoolean
            {

            					newCompositeNode(grammarAccess.getBookAccess().getPublishedEBooleanParserRuleCall_8_0());
            				
            pushFollow(FOLLOW_92);
            lv_published_9_0=ruleEBoolean();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getBookRule());
            					}
            					set(
            						current,
            						"published",
            						lv_published_9_0,
            						"org.montex.researchcv.xtext.TextualRCV.EBoolean");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:5535:3: (otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) ) )?
            int alt122=2;
            int LA122_0 = input.LA(1);

            if ( (LA122_0==76) ) {
                alt122=1;
            }
            switch (alt122) {
                case 1 :
                    // InternalTextualRCV.g:5536:4: otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) )
                    {
                    otherlv_10=(Token)match(input,76,FOLLOW_20); 

                    				newLeafNode(otherlv_10, grammarAccess.getBookAccess().getDOIKeyword_9_0());
                    			
                    // InternalTextualRCV.g:5540:4: ( (lv_DOI_11_0= ruleEString ) )
                    // InternalTextualRCV.g:5541:5: (lv_DOI_11_0= ruleEString )
                    {
                    // InternalTextualRCV.g:5541:5: (lv_DOI_11_0= ruleEString )
                    // InternalTextualRCV.g:5542:6: lv_DOI_11_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getBookAccess().getDOIEStringParserRuleCall_9_1_0());
                    					
                    pushFollow(FOLLOW_93);
                    lv_DOI_11_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getBookRule());
                    						}
                    						set(
                    							current,
                    							"DOI",
                    							lv_DOI_11_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:5560:3: (otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) ) )?
            int alt123=2;
            int LA123_0 = input.LA(1);

            if ( (LA123_0==77) ) {
                alt123=1;
            }
            switch (alt123) {
                case 1 :
                    // InternalTextualRCV.g:5561:4: otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) )
                    {
                    otherlv_12=(Token)match(input,77,FOLLOW_20); 

                    				newLeafNode(otherlv_12, grammarAccess.getBookAccess().getAbstractKeyword_10_0());
                    			
                    // InternalTextualRCV.g:5565:4: ( (lv_abstract_13_0= ruleEString ) )
                    // InternalTextualRCV.g:5566:5: (lv_abstract_13_0= ruleEString )
                    {
                    // InternalTextualRCV.g:5566:5: (lv_abstract_13_0= ruleEString )
                    // InternalTextualRCV.g:5567:6: lv_abstract_13_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getBookAccess().getAbstractEStringParserRuleCall_10_1_0());
                    					
                    pushFollow(FOLLOW_94);
                    lv_abstract_13_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getBookRule());
                    						}
                    						set(
                    							current,
                    							"abstract",
                    							lv_abstract_13_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_14=(Token)match(input,78,FOLLOW_91); 

            			newLeafNode(otherlv_14, grammarAccess.getBookAccess().getWithAuthorVersionKeyword_11());
            		
            // InternalTextualRCV.g:5589:3: ( (lv_withAuthorVersion_15_0= ruleEBoolean ) )
            // InternalTextualRCV.g:5590:4: (lv_withAuthorVersion_15_0= ruleEBoolean )
            {
            // InternalTextualRCV.g:5590:4: (lv_withAuthorVersion_15_0= ruleEBoolean )
            // InternalTextualRCV.g:5591:5: lv_withAuthorVersion_15_0= ruleEBoolean
            {

            					newCompositeNode(grammarAccess.getBookAccess().getWithAuthorVersionEBooleanParserRuleCall_12_0());
            				
            pushFollow(FOLLOW_133);
            lv_withAuthorVersion_15_0=ruleEBoolean();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getBookRule());
            					}
            					set(
            						current,
            						"withAuthorVersion",
            						lv_withAuthorVersion_15_0,
            						"org.montex.researchcv.xtext.TextualRCV.EBoolean");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:5608:3: (otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}' )?
            int alt125=2;
            int LA125_0 = input.LA(1);

            if ( (LA125_0==79) ) {
                alt125=1;
            }
            switch (alt125) {
                case 1 :
                    // InternalTextualRCV.g:5609:4: otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}'
                    {
                    otherlv_16=(Token)match(input,79,FOLLOW_3); 

                    				newLeafNode(otherlv_16, grammarAccess.getBookAccess().getNotesKeyword_13_0());
                    			
                    otherlv_17=(Token)match(input,16,FOLLOW_20); 

                    				newLeafNode(otherlv_17, grammarAccess.getBookAccess().getLeftCurlyBracketKeyword_13_1());
                    			
                    // InternalTextualRCV.g:5617:4: ( (lv_notes_18_0= ruleEString ) )
                    // InternalTextualRCV.g:5618:5: (lv_notes_18_0= ruleEString )
                    {
                    // InternalTextualRCV.g:5618:5: (lv_notes_18_0= ruleEString )
                    // InternalTextualRCV.g:5619:6: lv_notes_18_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getBookAccess().getNotesEStringParserRuleCall_13_2_0());
                    					
                    pushFollow(FOLLOW_40);
                    lv_notes_18_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getBookRule());
                    						}
                    						add(
                    							current,
                    							"notes",
                    							lv_notes_18_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:5636:4: (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )*
                    loop124:
                    do {
                        int alt124=2;
                        int LA124_0 = input.LA(1);

                        if ( (LA124_0==24) ) {
                            alt124=1;
                        }


                        switch (alt124) {
                    	case 1 :
                    	    // InternalTextualRCV.g:5637:5: otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) )
                    	    {
                    	    otherlv_19=(Token)match(input,24,FOLLOW_20); 

                    	    					newLeafNode(otherlv_19, grammarAccess.getBookAccess().getCommaKeyword_13_3_0());
                    	    				
                    	    // InternalTextualRCV.g:5641:5: ( (lv_notes_20_0= ruleEString ) )
                    	    // InternalTextualRCV.g:5642:6: (lv_notes_20_0= ruleEString )
                    	    {
                    	    // InternalTextualRCV.g:5642:6: (lv_notes_20_0= ruleEString )
                    	    // InternalTextualRCV.g:5643:7: lv_notes_20_0= ruleEString
                    	    {

                    	    							newCompositeNode(grammarAccess.getBookAccess().getNotesEStringParserRuleCall_13_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_40);
                    	    lv_notes_20_0=ruleEString();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getBookRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"notes",
                    	    								lv_notes_20_0,
                    	    								"org.montex.researchcv.xtext.TextualRCV.EString");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop124;
                        }
                    } while (true);

                    otherlv_21=(Token)match(input,18,FOLLOW_134); 

                    				newLeafNode(otherlv_21, grammarAccess.getBookAccess().getRightCurlyBracketKeyword_13_4());
                    			

                    }
                    break;

            }

            // InternalTextualRCV.g:5666:3: (otherlv_22= 'publisher' ( (lv_publisher_23_0= ruleEString ) ) )?
            int alt126=2;
            int LA126_0 = input.LA(1);

            if ( (LA126_0==81) ) {
                alt126=1;
            }
            switch (alt126) {
                case 1 :
                    // InternalTextualRCV.g:5667:4: otherlv_22= 'publisher' ( (lv_publisher_23_0= ruleEString ) )
                    {
                    otherlv_22=(Token)match(input,81,FOLLOW_20); 

                    				newLeafNode(otherlv_22, grammarAccess.getBookAccess().getPublisherKeyword_14_0());
                    			
                    // InternalTextualRCV.g:5671:4: ( (lv_publisher_23_0= ruleEString ) )
                    // InternalTextualRCV.g:5672:5: (lv_publisher_23_0= ruleEString )
                    {
                    // InternalTextualRCV.g:5672:5: (lv_publisher_23_0= ruleEString )
                    // InternalTextualRCV.g:5673:6: lv_publisher_23_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getBookAccess().getPublisherEStringParserRuleCall_14_1_0());
                    					
                    pushFollow(FOLLOW_135);
                    lv_publisher_23_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getBookRule());
                    						}
                    						set(
                    							current,
                    							"publisher",
                    							lv_publisher_23_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:5691:3: (otherlv_24= 'series' ( (lv_series_25_0= ruleEString ) ) )?
            int alt127=2;
            int LA127_0 = input.LA(1);

            if ( (LA127_0==95) ) {
                alt127=1;
            }
            switch (alt127) {
                case 1 :
                    // InternalTextualRCV.g:5692:4: otherlv_24= 'series' ( (lv_series_25_0= ruleEString ) )
                    {
                    otherlv_24=(Token)match(input,95,FOLLOW_20); 

                    				newLeafNode(otherlv_24, grammarAccess.getBookAccess().getSeriesKeyword_15_0());
                    			
                    // InternalTextualRCV.g:5696:4: ( (lv_series_25_0= ruleEString ) )
                    // InternalTextualRCV.g:5697:5: (lv_series_25_0= ruleEString )
                    {
                    // InternalTextualRCV.g:5697:5: (lv_series_25_0= ruleEString )
                    // InternalTextualRCV.g:5698:6: lv_series_25_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getBookAccess().getSeriesEStringParserRuleCall_15_1_0());
                    					
                    pushFollow(FOLLOW_136);
                    lv_series_25_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getBookRule());
                    						}
                    						set(
                    							current,
                    							"series",
                    							lv_series_25_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:5716:3: (otherlv_26= 'volume' ( (lv_volume_27_0= ruleEString ) ) )?
            int alt128=2;
            int LA128_0 = input.LA(1);

            if ( (LA128_0==84) ) {
                alt128=1;
            }
            switch (alt128) {
                case 1 :
                    // InternalTextualRCV.g:5717:4: otherlv_26= 'volume' ( (lv_volume_27_0= ruleEString ) )
                    {
                    otherlv_26=(Token)match(input,84,FOLLOW_20); 

                    				newLeafNode(otherlv_26, grammarAccess.getBookAccess().getVolumeKeyword_16_0());
                    			
                    // InternalTextualRCV.g:5721:4: ( (lv_volume_27_0= ruleEString ) )
                    // InternalTextualRCV.g:5722:5: (lv_volume_27_0= ruleEString )
                    {
                    // InternalTextualRCV.g:5722:5: (lv_volume_27_0= ruleEString )
                    // InternalTextualRCV.g:5723:6: lv_volume_27_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getBookAccess().getVolumeEStringParserRuleCall_16_1_0());
                    					
                    pushFollow(FOLLOW_102);
                    lv_volume_27_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getBookRule());
                    						}
                    						set(
                    							current,
                    							"volume",
                    							lv_volume_27_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_28=(Token)match(input,86,FOLLOW_61); 

            			newLeafNode(otherlv_28, grammarAccess.getBookAccess().getAuthorsKeyword_17());
            		
            otherlv_29=(Token)match(input,52,FOLLOW_20); 

            			newLeafNode(otherlv_29, grammarAccess.getBookAccess().getLeftParenthesisKeyword_18());
            		
            // InternalTextualRCV.g:5749:3: ( ( ruleEString ) )
            // InternalTextualRCV.g:5750:4: ( ruleEString )
            {
            // InternalTextualRCV.g:5750:4: ( ruleEString )
            // InternalTextualRCV.g:5751:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getBookRule());
            					}
            				

            					newCompositeNode(grammarAccess.getBookAccess().getAuthorsPersonCrossReference_19_0());
            				
            pushFollow(FOLLOW_62);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:5765:3: (otherlv_31= ',' ( ( ruleEString ) ) )*
            loop129:
            do {
                int alt129=2;
                int LA129_0 = input.LA(1);

                if ( (LA129_0==24) ) {
                    alt129=1;
                }


                switch (alt129) {
            	case 1 :
            	    // InternalTextualRCV.g:5766:4: otherlv_31= ',' ( ( ruleEString ) )
            	    {
            	    otherlv_31=(Token)match(input,24,FOLLOW_20); 

            	    				newLeafNode(otherlv_31, grammarAccess.getBookAccess().getCommaKeyword_20_0());
            	    			
            	    // InternalTextualRCV.g:5770:4: ( ( ruleEString ) )
            	    // InternalTextualRCV.g:5771:5: ( ruleEString )
            	    {
            	    // InternalTextualRCV.g:5771:5: ( ruleEString )
            	    // InternalTextualRCV.g:5772:6: ruleEString
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getBookRule());
            	    						}
            	    					

            	    						newCompositeNode(grammarAccess.getBookAccess().getAuthorsPersonCrossReference_20_1_0());
            	    					
            	    pushFollow(FOLLOW_62);
            	    ruleEString();

            	    state._fsp--;


            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop129;
                }
            } while (true);

            otherlv_33=(Token)match(input,53,FOLLOW_103); 

            			newLeafNode(otherlv_33, grammarAccess.getBookAccess().getRightParenthesisKeyword_21());
            		
            // InternalTextualRCV.g:5791:3: (otherlv_34= 'relatedProjects' otherlv_35= '(' ( ( ruleEString ) ) (otherlv_37= ',' ( ( ruleEString ) ) )* otherlv_39= ')' )?
            int alt131=2;
            int LA131_0 = input.LA(1);

            if ( (LA131_0==87) ) {
                alt131=1;
            }
            switch (alt131) {
                case 1 :
                    // InternalTextualRCV.g:5792:4: otherlv_34= 'relatedProjects' otherlv_35= '(' ( ( ruleEString ) ) (otherlv_37= ',' ( ( ruleEString ) ) )* otherlv_39= ')'
                    {
                    otherlv_34=(Token)match(input,87,FOLLOW_61); 

                    				newLeafNode(otherlv_34, grammarAccess.getBookAccess().getRelatedProjectsKeyword_22_0());
                    			
                    otherlv_35=(Token)match(input,52,FOLLOW_20); 

                    				newLeafNode(otherlv_35, grammarAccess.getBookAccess().getLeftParenthesisKeyword_22_1());
                    			
                    // InternalTextualRCV.g:5800:4: ( ( ruleEString ) )
                    // InternalTextualRCV.g:5801:5: ( ruleEString )
                    {
                    // InternalTextualRCV.g:5801:5: ( ruleEString )
                    // InternalTextualRCV.g:5802:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getBookRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getBookAccess().getRelatedProjectsGrantCrossReference_22_2_0());
                    					
                    pushFollow(FOLLOW_62);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:5816:4: (otherlv_37= ',' ( ( ruleEString ) ) )*
                    loop130:
                    do {
                        int alt130=2;
                        int LA130_0 = input.LA(1);

                        if ( (LA130_0==24) ) {
                            alt130=1;
                        }


                        switch (alt130) {
                    	case 1 :
                    	    // InternalTextualRCV.g:5817:5: otherlv_37= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_37=(Token)match(input,24,FOLLOW_20); 

                    	    					newLeafNode(otherlv_37, grammarAccess.getBookAccess().getCommaKeyword_22_3_0());
                    	    				
                    	    // InternalTextualRCV.g:5821:5: ( ( ruleEString ) )
                    	    // InternalTextualRCV.g:5822:6: ( ruleEString )
                    	    {
                    	    // InternalTextualRCV.g:5822:6: ( ruleEString )
                    	    // InternalTextualRCV.g:5823:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getBookRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getBookAccess().getRelatedProjectsGrantCrossReference_22_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_62);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop130;
                        }
                    } while (true);

                    otherlv_39=(Token)match(input,53,FOLLOW_104); 

                    				newLeafNode(otherlv_39, grammarAccess.getBookAccess().getRightParenthesisKeyword_22_4());
                    			

                    }
                    break;

            }

            otherlv_40=(Token)match(input,88,FOLLOW_9); 

            			newLeafNode(otherlv_40, grammarAccess.getBookAccess().getDateKeyword_23());
            		
            // InternalTextualRCV.g:5847:3: ( (lv_date_41_0= rulePublishedDate ) )
            // InternalTextualRCV.g:5848:4: (lv_date_41_0= rulePublishedDate )
            {
            // InternalTextualRCV.g:5848:4: (lv_date_41_0= rulePublishedDate )
            // InternalTextualRCV.g:5849:5: lv_date_41_0= rulePublishedDate
            {

            					newCompositeNode(grammarAccess.getBookAccess().getDatePublishedDateParserRuleCall_24_0());
            				
            pushFollow(FOLLOW_137);
            lv_date_41_0=rulePublishedDate();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getBookRule());
            					}
            					set(
            						current,
            						"date",
            						lv_date_41_0,
            						"org.montex.researchcv.xtext.TextualRCV.PublishedDate");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:5866:3: (otherlv_42= 'ISBN' otherlv_43= '{' ( (lv_ISBN_44_0= ruleSerialNumber ) ) (otherlv_45= ',' ( (lv_ISBN_46_0= ruleSerialNumber ) ) )* otherlv_47= '}' )?
            int alt133=2;
            int LA133_0 = input.LA(1);

            if ( (LA133_0==102) ) {
                alt133=1;
            }
            switch (alt133) {
                case 1 :
                    // InternalTextualRCV.g:5867:4: otherlv_42= 'ISBN' otherlv_43= '{' ( (lv_ISBN_44_0= ruleSerialNumber ) ) (otherlv_45= ',' ( (lv_ISBN_46_0= ruleSerialNumber ) ) )* otherlv_47= '}'
                    {
                    otherlv_42=(Token)match(input,102,FOLLOW_3); 

                    				newLeafNode(otherlv_42, grammarAccess.getBookAccess().getISBNKeyword_25_0());
                    			
                    otherlv_43=(Token)match(input,16,FOLLOW_106); 

                    				newLeafNode(otherlv_43, grammarAccess.getBookAccess().getLeftCurlyBracketKeyword_25_1());
                    			
                    // InternalTextualRCV.g:5875:4: ( (lv_ISBN_44_0= ruleSerialNumber ) )
                    // InternalTextualRCV.g:5876:5: (lv_ISBN_44_0= ruleSerialNumber )
                    {
                    // InternalTextualRCV.g:5876:5: (lv_ISBN_44_0= ruleSerialNumber )
                    // InternalTextualRCV.g:5877:6: lv_ISBN_44_0= ruleSerialNumber
                    {

                    						newCompositeNode(grammarAccess.getBookAccess().getISBNSerialNumberParserRuleCall_25_2_0());
                    					
                    pushFollow(FOLLOW_40);
                    lv_ISBN_44_0=ruleSerialNumber();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getBookRule());
                    						}
                    						add(
                    							current,
                    							"ISBN",
                    							lv_ISBN_44_0,
                    							"org.montex.researchcv.xtext.TextualRCV.SerialNumber");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:5894:4: (otherlv_45= ',' ( (lv_ISBN_46_0= ruleSerialNumber ) ) )*
                    loop132:
                    do {
                        int alt132=2;
                        int LA132_0 = input.LA(1);

                        if ( (LA132_0==24) ) {
                            alt132=1;
                        }


                        switch (alt132) {
                    	case 1 :
                    	    // InternalTextualRCV.g:5895:5: otherlv_45= ',' ( (lv_ISBN_46_0= ruleSerialNumber ) )
                    	    {
                    	    otherlv_45=(Token)match(input,24,FOLLOW_106); 

                    	    					newLeafNode(otherlv_45, grammarAccess.getBookAccess().getCommaKeyword_25_3_0());
                    	    				
                    	    // InternalTextualRCV.g:5899:5: ( (lv_ISBN_46_0= ruleSerialNumber ) )
                    	    // InternalTextualRCV.g:5900:6: (lv_ISBN_46_0= ruleSerialNumber )
                    	    {
                    	    // InternalTextualRCV.g:5900:6: (lv_ISBN_46_0= ruleSerialNumber )
                    	    // InternalTextualRCV.g:5901:7: lv_ISBN_46_0= ruleSerialNumber
                    	    {

                    	    							newCompositeNode(grammarAccess.getBookAccess().getISBNSerialNumberParserRuleCall_25_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_40);
                    	    lv_ISBN_46_0=ruleSerialNumber();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getBookRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"ISBN",
                    	    								lv_ISBN_46_0,
                    	    								"org.montex.researchcv.xtext.TextualRCV.SerialNumber");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop132;
                        }
                    } while (true);

                    otherlv_47=(Token)match(input,18,FOLLOW_27); 

                    				newLeafNode(otherlv_47, grammarAccess.getBookAccess().getRightCurlyBracketKeyword_25_4());
                    			

                    }
                    break;

            }

            otherlv_48=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_48, grammarAccess.getBookAccess().getRightCurlyBracketKeyword_26());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBook"


    // $ANTLR start "entryRuleBookChapter"
    // InternalTextualRCV.g:5932:1: entryRuleBookChapter returns [EObject current=null] : iv_ruleBookChapter= ruleBookChapter EOF ;
    public final EObject entryRuleBookChapter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBookChapter = null;


        try {
            // InternalTextualRCV.g:5932:52: (iv_ruleBookChapter= ruleBookChapter EOF )
            // InternalTextualRCV.g:5933:2: iv_ruleBookChapter= ruleBookChapter EOF
            {
             newCompositeNode(grammarAccess.getBookChapterRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBookChapter=ruleBookChapter();

            state._fsp--;

             current =iv_ruleBookChapter; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBookChapter"


    // $ANTLR start "ruleBookChapter"
    // InternalTextualRCV.g:5939:1: ruleBookChapter returns [EObject current=null] : ( ( (lv_openAccess_0_0= 'openAccess' ) ) otherlv_1= 'BookChapter' ( (lv_citekey_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'title' ( (lv_title_5_0= ruleEString ) ) (otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) ) )? otherlv_8= 'published' ( (lv_published_9_0= ruleEBoolean ) ) (otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) ) )? (otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) ) )? otherlv_14= 'withAuthorVersion' ( (lv_withAuthorVersion_15_0= ruleEBoolean ) ) (otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}' )? otherlv_22= 'booktitle' ( (lv_booktitle_23_0= ruleEString ) ) (otherlv_24= 'publisher' ( (lv_publisher_25_0= ruleEString ) ) )? (otherlv_26= 'series' ( (lv_series_27_0= ruleEString ) ) )? (otherlv_28= 'volume' ( (lv_volume_29_0= ruleEString ) ) )? (otherlv_30= 'firstPage' ( (lv_firstPage_31_0= ruleEString ) ) )? (otherlv_32= 'lastPage' ( (lv_lastPage_33_0= ruleEString ) ) )? (otherlv_34= 'chapterNumber' ( (lv_chapterNumber_35_0= ruleEString ) ) )? otherlv_36= 'authors' otherlv_37= '(' ( ( ruleEString ) ) (otherlv_39= ',' ( ( ruleEString ) ) )* otherlv_41= ')' (otherlv_42= 'relatedProjects' otherlv_43= '(' ( ( ruleEString ) ) (otherlv_45= ',' ( ( ruleEString ) ) )* otherlv_47= ')' )? (otherlv_48= 'bookeditors' otherlv_49= '(' ( ( ruleEString ) ) (otherlv_51= ',' ( ( ruleEString ) ) )* otherlv_53= ')' )? otherlv_54= 'date' ( (lv_date_55_0= rulePublishedDate ) ) (otherlv_56= 'ISBN' otherlv_57= '{' ( (lv_ISBN_58_0= ruleSerialNumber ) ) (otherlv_59= ',' ( (lv_ISBN_60_0= ruleSerialNumber ) ) )* otherlv_61= '}' )? otherlv_62= '}' ) ;
    public final EObject ruleBookChapter() throws RecognitionException {
        EObject current = null;

        Token lv_openAccess_0_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_21=null;
        Token otherlv_22=null;
        Token otherlv_24=null;
        Token otherlv_26=null;
        Token otherlv_28=null;
        Token otherlv_30=null;
        Token otherlv_32=null;
        Token otherlv_34=null;
        Token otherlv_36=null;
        Token otherlv_37=null;
        Token otherlv_39=null;
        Token otherlv_41=null;
        Token otherlv_42=null;
        Token otherlv_43=null;
        Token otherlv_45=null;
        Token otherlv_47=null;
        Token otherlv_48=null;
        Token otherlv_49=null;
        Token otherlv_51=null;
        Token otherlv_53=null;
        Token otherlv_54=null;
        Token otherlv_56=null;
        Token otherlv_57=null;
        Token otherlv_59=null;
        Token otherlv_61=null;
        Token otherlv_62=null;
        AntlrDatatypeRuleToken lv_citekey_2_0 = null;

        AntlrDatatypeRuleToken lv_title_5_0 = null;

        AntlrDatatypeRuleToken lv_URL_7_0 = null;

        AntlrDatatypeRuleToken lv_published_9_0 = null;

        AntlrDatatypeRuleToken lv_DOI_11_0 = null;

        AntlrDatatypeRuleToken lv_abstract_13_0 = null;

        AntlrDatatypeRuleToken lv_withAuthorVersion_15_0 = null;

        AntlrDatatypeRuleToken lv_notes_18_0 = null;

        AntlrDatatypeRuleToken lv_notes_20_0 = null;

        AntlrDatatypeRuleToken lv_booktitle_23_0 = null;

        AntlrDatatypeRuleToken lv_publisher_25_0 = null;

        AntlrDatatypeRuleToken lv_series_27_0 = null;

        AntlrDatatypeRuleToken lv_volume_29_0 = null;

        AntlrDatatypeRuleToken lv_firstPage_31_0 = null;

        AntlrDatatypeRuleToken lv_lastPage_33_0 = null;

        AntlrDatatypeRuleToken lv_chapterNumber_35_0 = null;

        EObject lv_date_55_0 = null;

        EObject lv_ISBN_58_0 = null;

        EObject lv_ISBN_60_0 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:5945:2: ( ( ( (lv_openAccess_0_0= 'openAccess' ) ) otherlv_1= 'BookChapter' ( (lv_citekey_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'title' ( (lv_title_5_0= ruleEString ) ) (otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) ) )? otherlv_8= 'published' ( (lv_published_9_0= ruleEBoolean ) ) (otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) ) )? (otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) ) )? otherlv_14= 'withAuthorVersion' ( (lv_withAuthorVersion_15_0= ruleEBoolean ) ) (otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}' )? otherlv_22= 'booktitle' ( (lv_booktitle_23_0= ruleEString ) ) (otherlv_24= 'publisher' ( (lv_publisher_25_0= ruleEString ) ) )? (otherlv_26= 'series' ( (lv_series_27_0= ruleEString ) ) )? (otherlv_28= 'volume' ( (lv_volume_29_0= ruleEString ) ) )? (otherlv_30= 'firstPage' ( (lv_firstPage_31_0= ruleEString ) ) )? (otherlv_32= 'lastPage' ( (lv_lastPage_33_0= ruleEString ) ) )? (otherlv_34= 'chapterNumber' ( (lv_chapterNumber_35_0= ruleEString ) ) )? otherlv_36= 'authors' otherlv_37= '(' ( ( ruleEString ) ) (otherlv_39= ',' ( ( ruleEString ) ) )* otherlv_41= ')' (otherlv_42= 'relatedProjects' otherlv_43= '(' ( ( ruleEString ) ) (otherlv_45= ',' ( ( ruleEString ) ) )* otherlv_47= ')' )? (otherlv_48= 'bookeditors' otherlv_49= '(' ( ( ruleEString ) ) (otherlv_51= ',' ( ( ruleEString ) ) )* otherlv_53= ')' )? otherlv_54= 'date' ( (lv_date_55_0= rulePublishedDate ) ) (otherlv_56= 'ISBN' otherlv_57= '{' ( (lv_ISBN_58_0= ruleSerialNumber ) ) (otherlv_59= ',' ( (lv_ISBN_60_0= ruleSerialNumber ) ) )* otherlv_61= '}' )? otherlv_62= '}' ) )
            // InternalTextualRCV.g:5946:2: ( ( (lv_openAccess_0_0= 'openAccess' ) ) otherlv_1= 'BookChapter' ( (lv_citekey_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'title' ( (lv_title_5_0= ruleEString ) ) (otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) ) )? otherlv_8= 'published' ( (lv_published_9_0= ruleEBoolean ) ) (otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) ) )? (otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) ) )? otherlv_14= 'withAuthorVersion' ( (lv_withAuthorVersion_15_0= ruleEBoolean ) ) (otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}' )? otherlv_22= 'booktitle' ( (lv_booktitle_23_0= ruleEString ) ) (otherlv_24= 'publisher' ( (lv_publisher_25_0= ruleEString ) ) )? (otherlv_26= 'series' ( (lv_series_27_0= ruleEString ) ) )? (otherlv_28= 'volume' ( (lv_volume_29_0= ruleEString ) ) )? (otherlv_30= 'firstPage' ( (lv_firstPage_31_0= ruleEString ) ) )? (otherlv_32= 'lastPage' ( (lv_lastPage_33_0= ruleEString ) ) )? (otherlv_34= 'chapterNumber' ( (lv_chapterNumber_35_0= ruleEString ) ) )? otherlv_36= 'authors' otherlv_37= '(' ( ( ruleEString ) ) (otherlv_39= ',' ( ( ruleEString ) ) )* otherlv_41= ')' (otherlv_42= 'relatedProjects' otherlv_43= '(' ( ( ruleEString ) ) (otherlv_45= ',' ( ( ruleEString ) ) )* otherlv_47= ')' )? (otherlv_48= 'bookeditors' otherlv_49= '(' ( ( ruleEString ) ) (otherlv_51= ',' ( ( ruleEString ) ) )* otherlv_53= ')' )? otherlv_54= 'date' ( (lv_date_55_0= rulePublishedDate ) ) (otherlv_56= 'ISBN' otherlv_57= '{' ( (lv_ISBN_58_0= ruleSerialNumber ) ) (otherlv_59= ',' ( (lv_ISBN_60_0= ruleSerialNumber ) ) )* otherlv_61= '}' )? otherlv_62= '}' )
            {
            // InternalTextualRCV.g:5946:2: ( ( (lv_openAccess_0_0= 'openAccess' ) ) otherlv_1= 'BookChapter' ( (lv_citekey_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'title' ( (lv_title_5_0= ruleEString ) ) (otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) ) )? otherlv_8= 'published' ( (lv_published_9_0= ruleEBoolean ) ) (otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) ) )? (otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) ) )? otherlv_14= 'withAuthorVersion' ( (lv_withAuthorVersion_15_0= ruleEBoolean ) ) (otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}' )? otherlv_22= 'booktitle' ( (lv_booktitle_23_0= ruleEString ) ) (otherlv_24= 'publisher' ( (lv_publisher_25_0= ruleEString ) ) )? (otherlv_26= 'series' ( (lv_series_27_0= ruleEString ) ) )? (otherlv_28= 'volume' ( (lv_volume_29_0= ruleEString ) ) )? (otherlv_30= 'firstPage' ( (lv_firstPage_31_0= ruleEString ) ) )? (otherlv_32= 'lastPage' ( (lv_lastPage_33_0= ruleEString ) ) )? (otherlv_34= 'chapterNumber' ( (lv_chapterNumber_35_0= ruleEString ) ) )? otherlv_36= 'authors' otherlv_37= '(' ( ( ruleEString ) ) (otherlv_39= ',' ( ( ruleEString ) ) )* otherlv_41= ')' (otherlv_42= 'relatedProjects' otherlv_43= '(' ( ( ruleEString ) ) (otherlv_45= ',' ( ( ruleEString ) ) )* otherlv_47= ')' )? (otherlv_48= 'bookeditors' otherlv_49= '(' ( ( ruleEString ) ) (otherlv_51= ',' ( ( ruleEString ) ) )* otherlv_53= ')' )? otherlv_54= 'date' ( (lv_date_55_0= rulePublishedDate ) ) (otherlv_56= 'ISBN' otherlv_57= '{' ( (lv_ISBN_58_0= ruleSerialNumber ) ) (otherlv_59= ',' ( (lv_ISBN_60_0= ruleSerialNumber ) ) )* otherlv_61= '}' )? otherlv_62= '}' )
            // InternalTextualRCV.g:5947:3: ( (lv_openAccess_0_0= 'openAccess' ) ) otherlv_1= 'BookChapter' ( (lv_citekey_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'title' ( (lv_title_5_0= ruleEString ) ) (otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) ) )? otherlv_8= 'published' ( (lv_published_9_0= ruleEBoolean ) ) (otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) ) )? (otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) ) )? otherlv_14= 'withAuthorVersion' ( (lv_withAuthorVersion_15_0= ruleEBoolean ) ) (otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}' )? otherlv_22= 'booktitle' ( (lv_booktitle_23_0= ruleEString ) ) (otherlv_24= 'publisher' ( (lv_publisher_25_0= ruleEString ) ) )? (otherlv_26= 'series' ( (lv_series_27_0= ruleEString ) ) )? (otherlv_28= 'volume' ( (lv_volume_29_0= ruleEString ) ) )? (otherlv_30= 'firstPage' ( (lv_firstPage_31_0= ruleEString ) ) )? (otherlv_32= 'lastPage' ( (lv_lastPage_33_0= ruleEString ) ) )? (otherlv_34= 'chapterNumber' ( (lv_chapterNumber_35_0= ruleEString ) ) )? otherlv_36= 'authors' otherlv_37= '(' ( ( ruleEString ) ) (otherlv_39= ',' ( ( ruleEString ) ) )* otherlv_41= ')' (otherlv_42= 'relatedProjects' otherlv_43= '(' ( ( ruleEString ) ) (otherlv_45= ',' ( ( ruleEString ) ) )* otherlv_47= ')' )? (otherlv_48= 'bookeditors' otherlv_49= '(' ( ( ruleEString ) ) (otherlv_51= ',' ( ( ruleEString ) ) )* otherlv_53= ')' )? otherlv_54= 'date' ( (lv_date_55_0= rulePublishedDate ) ) (otherlv_56= 'ISBN' otherlv_57= '{' ( (lv_ISBN_58_0= ruleSerialNumber ) ) (otherlv_59= ',' ( (lv_ISBN_60_0= ruleSerialNumber ) ) )* otherlv_61= '}' )? otherlv_62= '}'
            {
            // InternalTextualRCV.g:5947:3: ( (lv_openAccess_0_0= 'openAccess' ) )
            // InternalTextualRCV.g:5948:4: (lv_openAccess_0_0= 'openAccess' )
            {
            // InternalTextualRCV.g:5948:4: (lv_openAccess_0_0= 'openAccess' )
            // InternalTextualRCV.g:5949:5: lv_openAccess_0_0= 'openAccess'
            {
            lv_openAccess_0_0=(Token)match(input,73,FOLLOW_138); 

            					newLeafNode(lv_openAccess_0_0, grammarAccess.getBookChapterAccess().getOpenAccessOpenAccessKeyword_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getBookChapterRule());
            					}
            					setWithLastConsumed(current, "openAccess", lv_openAccess_0_0 != null, "openAccess");
            				

            }


            }

            otherlv_1=(Token)match(input,108,FOLLOW_20); 

            			newLeafNode(otherlv_1, grammarAccess.getBookChapterAccess().getBookChapterKeyword_1());
            		
            // InternalTextualRCV.g:5965:3: ( (lv_citekey_2_0= ruleEString ) )
            // InternalTextualRCV.g:5966:4: (lv_citekey_2_0= ruleEString )
            {
            // InternalTextualRCV.g:5966:4: (lv_citekey_2_0= ruleEString )
            // InternalTextualRCV.g:5967:5: lv_citekey_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getBookChapterAccess().getCitekeyEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_3);
            lv_citekey_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getBookChapterRule());
            					}
            					set(
            						current,
            						"citekey",
            						lv_citekey_2_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,16,FOLLOW_58); 

            			newLeafNode(otherlv_3, grammarAccess.getBookChapterAccess().getLeftCurlyBracketKeyword_3());
            		
            otherlv_4=(Token)match(input,49,FOLLOW_20); 

            			newLeafNode(otherlv_4, grammarAccess.getBookChapterAccess().getTitleKeyword_4());
            		
            // InternalTextualRCV.g:5992:3: ( (lv_title_5_0= ruleEString ) )
            // InternalTextualRCV.g:5993:4: (lv_title_5_0= ruleEString )
            {
            // InternalTextualRCV.g:5993:4: (lv_title_5_0= ruleEString )
            // InternalTextualRCV.g:5994:5: lv_title_5_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getBookChapterAccess().getTitleEStringParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_89);
            lv_title_5_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getBookChapterRule());
            					}
            					set(
            						current,
            						"title",
            						lv_title_5_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:6011:3: (otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) ) )?
            int alt134=2;
            int LA134_0 = input.LA(1);

            if ( (LA134_0==47) ) {
                alt134=1;
            }
            switch (alt134) {
                case 1 :
                    // InternalTextualRCV.g:6012:4: otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) )
                    {
                    otherlv_6=(Token)match(input,47,FOLLOW_20); 

                    				newLeafNode(otherlv_6, grammarAccess.getBookChapterAccess().getURLKeyword_6_0());
                    			
                    // InternalTextualRCV.g:6016:4: ( (lv_URL_7_0= ruleEString ) )
                    // InternalTextualRCV.g:6017:5: (lv_URL_7_0= ruleEString )
                    {
                    // InternalTextualRCV.g:6017:5: (lv_URL_7_0= ruleEString )
                    // InternalTextualRCV.g:6018:6: lv_URL_7_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getBookChapterAccess().getURLEStringParserRuleCall_6_1_0());
                    					
                    pushFollow(FOLLOW_90);
                    lv_URL_7_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getBookChapterRule());
                    						}
                    						set(
                    							current,
                    							"URL",
                    							lv_URL_7_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_8=(Token)match(input,75,FOLLOW_91); 

            			newLeafNode(otherlv_8, grammarAccess.getBookChapterAccess().getPublishedKeyword_7());
            		
            // InternalTextualRCV.g:6040:3: ( (lv_published_9_0= ruleEBoolean ) )
            // InternalTextualRCV.g:6041:4: (lv_published_9_0= ruleEBoolean )
            {
            // InternalTextualRCV.g:6041:4: (lv_published_9_0= ruleEBoolean )
            // InternalTextualRCV.g:6042:5: lv_published_9_0= ruleEBoolean
            {

            					newCompositeNode(grammarAccess.getBookChapterAccess().getPublishedEBooleanParserRuleCall_8_0());
            				
            pushFollow(FOLLOW_92);
            lv_published_9_0=ruleEBoolean();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getBookChapterRule());
            					}
            					set(
            						current,
            						"published",
            						lv_published_9_0,
            						"org.montex.researchcv.xtext.TextualRCV.EBoolean");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:6059:3: (otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) ) )?
            int alt135=2;
            int LA135_0 = input.LA(1);

            if ( (LA135_0==76) ) {
                alt135=1;
            }
            switch (alt135) {
                case 1 :
                    // InternalTextualRCV.g:6060:4: otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) )
                    {
                    otherlv_10=(Token)match(input,76,FOLLOW_20); 

                    				newLeafNode(otherlv_10, grammarAccess.getBookChapterAccess().getDOIKeyword_9_0());
                    			
                    // InternalTextualRCV.g:6064:4: ( (lv_DOI_11_0= ruleEString ) )
                    // InternalTextualRCV.g:6065:5: (lv_DOI_11_0= ruleEString )
                    {
                    // InternalTextualRCV.g:6065:5: (lv_DOI_11_0= ruleEString )
                    // InternalTextualRCV.g:6066:6: lv_DOI_11_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getBookChapterAccess().getDOIEStringParserRuleCall_9_1_0());
                    					
                    pushFollow(FOLLOW_93);
                    lv_DOI_11_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getBookChapterRule());
                    						}
                    						set(
                    							current,
                    							"DOI",
                    							lv_DOI_11_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:6084:3: (otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) ) )?
            int alt136=2;
            int LA136_0 = input.LA(1);

            if ( (LA136_0==77) ) {
                alt136=1;
            }
            switch (alt136) {
                case 1 :
                    // InternalTextualRCV.g:6085:4: otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) )
                    {
                    otherlv_12=(Token)match(input,77,FOLLOW_20); 

                    				newLeafNode(otherlv_12, grammarAccess.getBookChapterAccess().getAbstractKeyword_10_0());
                    			
                    // InternalTextualRCV.g:6089:4: ( (lv_abstract_13_0= ruleEString ) )
                    // InternalTextualRCV.g:6090:5: (lv_abstract_13_0= ruleEString )
                    {
                    // InternalTextualRCV.g:6090:5: (lv_abstract_13_0= ruleEString )
                    // InternalTextualRCV.g:6091:6: lv_abstract_13_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getBookChapterAccess().getAbstractEStringParserRuleCall_10_1_0());
                    					
                    pushFollow(FOLLOW_94);
                    lv_abstract_13_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getBookChapterRule());
                    						}
                    						set(
                    							current,
                    							"abstract",
                    							lv_abstract_13_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_14=(Token)match(input,78,FOLLOW_91); 

            			newLeafNode(otherlv_14, grammarAccess.getBookChapterAccess().getWithAuthorVersionKeyword_11());
            		
            // InternalTextualRCV.g:6113:3: ( (lv_withAuthorVersion_15_0= ruleEBoolean ) )
            // InternalTextualRCV.g:6114:4: (lv_withAuthorVersion_15_0= ruleEBoolean )
            {
            // InternalTextualRCV.g:6114:4: (lv_withAuthorVersion_15_0= ruleEBoolean )
            // InternalTextualRCV.g:6115:5: lv_withAuthorVersion_15_0= ruleEBoolean
            {

            					newCompositeNode(grammarAccess.getBookChapterAccess().getWithAuthorVersionEBooleanParserRuleCall_12_0());
            				
            pushFollow(FOLLOW_111);
            lv_withAuthorVersion_15_0=ruleEBoolean();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getBookChapterRule());
            					}
            					set(
            						current,
            						"withAuthorVersion",
            						lv_withAuthorVersion_15_0,
            						"org.montex.researchcv.xtext.TextualRCV.EBoolean");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:6132:3: (otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}' )?
            int alt138=2;
            int LA138_0 = input.LA(1);

            if ( (LA138_0==79) ) {
                alt138=1;
            }
            switch (alt138) {
                case 1 :
                    // InternalTextualRCV.g:6133:4: otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}'
                    {
                    otherlv_16=(Token)match(input,79,FOLLOW_3); 

                    				newLeafNode(otherlv_16, grammarAccess.getBookChapterAccess().getNotesKeyword_13_0());
                    			
                    otherlv_17=(Token)match(input,16,FOLLOW_20); 

                    				newLeafNode(otherlv_17, grammarAccess.getBookChapterAccess().getLeftCurlyBracketKeyword_13_1());
                    			
                    // InternalTextualRCV.g:6141:4: ( (lv_notes_18_0= ruleEString ) )
                    // InternalTextualRCV.g:6142:5: (lv_notes_18_0= ruleEString )
                    {
                    // InternalTextualRCV.g:6142:5: (lv_notes_18_0= ruleEString )
                    // InternalTextualRCV.g:6143:6: lv_notes_18_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getBookChapterAccess().getNotesEStringParserRuleCall_13_2_0());
                    					
                    pushFollow(FOLLOW_40);
                    lv_notes_18_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getBookChapterRule());
                    						}
                    						add(
                    							current,
                    							"notes",
                    							lv_notes_18_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:6160:4: (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )*
                    loop137:
                    do {
                        int alt137=2;
                        int LA137_0 = input.LA(1);

                        if ( (LA137_0==24) ) {
                            alt137=1;
                        }


                        switch (alt137) {
                    	case 1 :
                    	    // InternalTextualRCV.g:6161:5: otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) )
                    	    {
                    	    otherlv_19=(Token)match(input,24,FOLLOW_20); 

                    	    					newLeafNode(otherlv_19, grammarAccess.getBookChapterAccess().getCommaKeyword_13_3_0());
                    	    				
                    	    // InternalTextualRCV.g:6165:5: ( (lv_notes_20_0= ruleEString ) )
                    	    // InternalTextualRCV.g:6166:6: (lv_notes_20_0= ruleEString )
                    	    {
                    	    // InternalTextualRCV.g:6166:6: (lv_notes_20_0= ruleEString )
                    	    // InternalTextualRCV.g:6167:7: lv_notes_20_0= ruleEString
                    	    {

                    	    							newCompositeNode(grammarAccess.getBookChapterAccess().getNotesEStringParserRuleCall_13_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_40);
                    	    lv_notes_20_0=ruleEString();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getBookChapterRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"notes",
                    	    								lv_notes_20_0,
                    	    								"org.montex.researchcv.xtext.TextualRCV.EString");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop137;
                        }
                    } while (true);

                    otherlv_21=(Token)match(input,18,FOLLOW_112); 

                    				newLeafNode(otherlv_21, grammarAccess.getBookChapterAccess().getRightCurlyBracketKeyword_13_4());
                    			

                    }
                    break;

            }

            otherlv_22=(Token)match(input,94,FOLLOW_20); 

            			newLeafNode(otherlv_22, grammarAccess.getBookChapterAccess().getBooktitleKeyword_14());
            		
            // InternalTextualRCV.g:6194:3: ( (lv_booktitle_23_0= ruleEString ) )
            // InternalTextualRCV.g:6195:4: (lv_booktitle_23_0= ruleEString )
            {
            // InternalTextualRCV.g:6195:4: (lv_booktitle_23_0= ruleEString )
            // InternalTextualRCV.g:6196:5: lv_booktitle_23_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getBookChapterAccess().getBooktitleEStringParserRuleCall_15_0());
            				
            pushFollow(FOLLOW_139);
            lv_booktitle_23_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getBookChapterRule());
            					}
            					set(
            						current,
            						"booktitle",
            						lv_booktitle_23_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:6213:3: (otherlv_24= 'publisher' ( (lv_publisher_25_0= ruleEString ) ) )?
            int alt139=2;
            int LA139_0 = input.LA(1);

            if ( (LA139_0==81) ) {
                alt139=1;
            }
            switch (alt139) {
                case 1 :
                    // InternalTextualRCV.g:6214:4: otherlv_24= 'publisher' ( (lv_publisher_25_0= ruleEString ) )
                    {
                    otherlv_24=(Token)match(input,81,FOLLOW_20); 

                    				newLeafNode(otherlv_24, grammarAccess.getBookChapterAccess().getPublisherKeyword_16_0());
                    			
                    // InternalTextualRCV.g:6218:4: ( (lv_publisher_25_0= ruleEString ) )
                    // InternalTextualRCV.g:6219:5: (lv_publisher_25_0= ruleEString )
                    {
                    // InternalTextualRCV.g:6219:5: (lv_publisher_25_0= ruleEString )
                    // InternalTextualRCV.g:6220:6: lv_publisher_25_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getBookChapterAccess().getPublisherEStringParserRuleCall_16_1_0());
                    					
                    pushFollow(FOLLOW_140);
                    lv_publisher_25_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getBookChapterRule());
                    						}
                    						set(
                    							current,
                    							"publisher",
                    							lv_publisher_25_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:6238:3: (otherlv_26= 'series' ( (lv_series_27_0= ruleEString ) ) )?
            int alt140=2;
            int LA140_0 = input.LA(1);

            if ( (LA140_0==95) ) {
                alt140=1;
            }
            switch (alt140) {
                case 1 :
                    // InternalTextualRCV.g:6239:4: otherlv_26= 'series' ( (lv_series_27_0= ruleEString ) )
                    {
                    otherlv_26=(Token)match(input,95,FOLLOW_20); 

                    				newLeafNode(otherlv_26, grammarAccess.getBookChapterAccess().getSeriesKeyword_17_0());
                    			
                    // InternalTextualRCV.g:6243:4: ( (lv_series_27_0= ruleEString ) )
                    // InternalTextualRCV.g:6244:5: (lv_series_27_0= ruleEString )
                    {
                    // InternalTextualRCV.g:6244:5: (lv_series_27_0= ruleEString )
                    // InternalTextualRCV.g:6245:6: lv_series_27_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getBookChapterAccess().getSeriesEStringParserRuleCall_17_1_0());
                    					
                    pushFollow(FOLLOW_141);
                    lv_series_27_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getBookChapterRule());
                    						}
                    						set(
                    							current,
                    							"series",
                    							lv_series_27_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:6263:3: (otherlv_28= 'volume' ( (lv_volume_29_0= ruleEString ) ) )?
            int alt141=2;
            int LA141_0 = input.LA(1);

            if ( (LA141_0==84) ) {
                alt141=1;
            }
            switch (alt141) {
                case 1 :
                    // InternalTextualRCV.g:6264:4: otherlv_28= 'volume' ( (lv_volume_29_0= ruleEString ) )
                    {
                    otherlv_28=(Token)match(input,84,FOLLOW_20); 

                    				newLeafNode(otherlv_28, grammarAccess.getBookChapterAccess().getVolumeKeyword_18_0());
                    			
                    // InternalTextualRCV.g:6268:4: ( (lv_volume_29_0= ruleEString ) )
                    // InternalTextualRCV.g:6269:5: (lv_volume_29_0= ruleEString )
                    {
                    // InternalTextualRCV.g:6269:5: (lv_volume_29_0= ruleEString )
                    // InternalTextualRCV.g:6270:6: lv_volume_29_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getBookChapterAccess().getVolumeEStringParserRuleCall_18_1_0());
                    					
                    pushFollow(FOLLOW_142);
                    lv_volume_29_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getBookChapterRule());
                    						}
                    						set(
                    							current,
                    							"volume",
                    							lv_volume_29_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:6288:3: (otherlv_30= 'firstPage' ( (lv_firstPage_31_0= ruleEString ) ) )?
            int alt142=2;
            int LA142_0 = input.LA(1);

            if ( (LA142_0==82) ) {
                alt142=1;
            }
            switch (alt142) {
                case 1 :
                    // InternalTextualRCV.g:6289:4: otherlv_30= 'firstPage' ( (lv_firstPage_31_0= ruleEString ) )
                    {
                    otherlv_30=(Token)match(input,82,FOLLOW_20); 

                    				newLeafNode(otherlv_30, grammarAccess.getBookChapterAccess().getFirstPageKeyword_19_0());
                    			
                    // InternalTextualRCV.g:6293:4: ( (lv_firstPage_31_0= ruleEString ) )
                    // InternalTextualRCV.g:6294:5: (lv_firstPage_31_0= ruleEString )
                    {
                    // InternalTextualRCV.g:6294:5: (lv_firstPage_31_0= ruleEString )
                    // InternalTextualRCV.g:6295:6: lv_firstPage_31_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getBookChapterAccess().getFirstPageEStringParserRuleCall_19_1_0());
                    					
                    pushFollow(FOLLOW_143);
                    lv_firstPage_31_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getBookChapterRule());
                    						}
                    						set(
                    							current,
                    							"firstPage",
                    							lv_firstPage_31_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:6313:3: (otherlv_32= 'lastPage' ( (lv_lastPage_33_0= ruleEString ) ) )?
            int alt143=2;
            int LA143_0 = input.LA(1);

            if ( (LA143_0==83) ) {
                alt143=1;
            }
            switch (alt143) {
                case 1 :
                    // InternalTextualRCV.g:6314:4: otherlv_32= 'lastPage' ( (lv_lastPage_33_0= ruleEString ) )
                    {
                    otherlv_32=(Token)match(input,83,FOLLOW_20); 

                    				newLeafNode(otherlv_32, grammarAccess.getBookChapterAccess().getLastPageKeyword_20_0());
                    			
                    // InternalTextualRCV.g:6318:4: ( (lv_lastPage_33_0= ruleEString ) )
                    // InternalTextualRCV.g:6319:5: (lv_lastPage_33_0= ruleEString )
                    {
                    // InternalTextualRCV.g:6319:5: (lv_lastPage_33_0= ruleEString )
                    // InternalTextualRCV.g:6320:6: lv_lastPage_33_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getBookChapterAccess().getLastPageEStringParserRuleCall_20_1_0());
                    					
                    pushFollow(FOLLOW_144);
                    lv_lastPage_33_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getBookChapterRule());
                    						}
                    						set(
                    							current,
                    							"lastPage",
                    							lv_lastPage_33_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:6338:3: (otherlv_34= 'chapterNumber' ( (lv_chapterNumber_35_0= ruleEString ) ) )?
            int alt144=2;
            int LA144_0 = input.LA(1);

            if ( (LA144_0==109) ) {
                alt144=1;
            }
            switch (alt144) {
                case 1 :
                    // InternalTextualRCV.g:6339:4: otherlv_34= 'chapterNumber' ( (lv_chapterNumber_35_0= ruleEString ) )
                    {
                    otherlv_34=(Token)match(input,109,FOLLOW_20); 

                    				newLeafNode(otherlv_34, grammarAccess.getBookChapterAccess().getChapterNumberKeyword_21_0());
                    			
                    // InternalTextualRCV.g:6343:4: ( (lv_chapterNumber_35_0= ruleEString ) )
                    // InternalTextualRCV.g:6344:5: (lv_chapterNumber_35_0= ruleEString )
                    {
                    // InternalTextualRCV.g:6344:5: (lv_chapterNumber_35_0= ruleEString )
                    // InternalTextualRCV.g:6345:6: lv_chapterNumber_35_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getBookChapterAccess().getChapterNumberEStringParserRuleCall_21_1_0());
                    					
                    pushFollow(FOLLOW_102);
                    lv_chapterNumber_35_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getBookChapterRule());
                    						}
                    						set(
                    							current,
                    							"chapterNumber",
                    							lv_chapterNumber_35_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_36=(Token)match(input,86,FOLLOW_61); 

            			newLeafNode(otherlv_36, grammarAccess.getBookChapterAccess().getAuthorsKeyword_22());
            		
            otherlv_37=(Token)match(input,52,FOLLOW_20); 

            			newLeafNode(otherlv_37, grammarAccess.getBookChapterAccess().getLeftParenthesisKeyword_23());
            		
            // InternalTextualRCV.g:6371:3: ( ( ruleEString ) )
            // InternalTextualRCV.g:6372:4: ( ruleEString )
            {
            // InternalTextualRCV.g:6372:4: ( ruleEString )
            // InternalTextualRCV.g:6373:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getBookChapterRule());
            					}
            				

            					newCompositeNode(grammarAccess.getBookChapterAccess().getAuthorsPersonCrossReference_24_0());
            				
            pushFollow(FOLLOW_62);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:6387:3: (otherlv_39= ',' ( ( ruleEString ) ) )*
            loop145:
            do {
                int alt145=2;
                int LA145_0 = input.LA(1);

                if ( (LA145_0==24) ) {
                    alt145=1;
                }


                switch (alt145) {
            	case 1 :
            	    // InternalTextualRCV.g:6388:4: otherlv_39= ',' ( ( ruleEString ) )
            	    {
            	    otherlv_39=(Token)match(input,24,FOLLOW_20); 

            	    				newLeafNode(otherlv_39, grammarAccess.getBookChapterAccess().getCommaKeyword_25_0());
            	    			
            	    // InternalTextualRCV.g:6392:4: ( ( ruleEString ) )
            	    // InternalTextualRCV.g:6393:5: ( ruleEString )
            	    {
            	    // InternalTextualRCV.g:6393:5: ( ruleEString )
            	    // InternalTextualRCV.g:6394:6: ruleEString
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getBookChapterRule());
            	    						}
            	    					

            	    						newCompositeNode(grammarAccess.getBookChapterAccess().getAuthorsPersonCrossReference_25_1_0());
            	    					
            	    pushFollow(FOLLOW_62);
            	    ruleEString();

            	    state._fsp--;


            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop145;
                }
            } while (true);

            otherlv_41=(Token)match(input,53,FOLLOW_123); 

            			newLeafNode(otherlv_41, grammarAccess.getBookChapterAccess().getRightParenthesisKeyword_26());
            		
            // InternalTextualRCV.g:6413:3: (otherlv_42= 'relatedProjects' otherlv_43= '(' ( ( ruleEString ) ) (otherlv_45= ',' ( ( ruleEString ) ) )* otherlv_47= ')' )?
            int alt147=2;
            int LA147_0 = input.LA(1);

            if ( (LA147_0==87) ) {
                alt147=1;
            }
            switch (alt147) {
                case 1 :
                    // InternalTextualRCV.g:6414:4: otherlv_42= 'relatedProjects' otherlv_43= '(' ( ( ruleEString ) ) (otherlv_45= ',' ( ( ruleEString ) ) )* otherlv_47= ')'
                    {
                    otherlv_42=(Token)match(input,87,FOLLOW_61); 

                    				newLeafNode(otherlv_42, grammarAccess.getBookChapterAccess().getRelatedProjectsKeyword_27_0());
                    			
                    otherlv_43=(Token)match(input,52,FOLLOW_20); 

                    				newLeafNode(otherlv_43, grammarAccess.getBookChapterAccess().getLeftParenthesisKeyword_27_1());
                    			
                    // InternalTextualRCV.g:6422:4: ( ( ruleEString ) )
                    // InternalTextualRCV.g:6423:5: ( ruleEString )
                    {
                    // InternalTextualRCV.g:6423:5: ( ruleEString )
                    // InternalTextualRCV.g:6424:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getBookChapterRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getBookChapterAccess().getRelatedProjectsGrantCrossReference_27_2_0());
                    					
                    pushFollow(FOLLOW_62);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:6438:4: (otherlv_45= ',' ( ( ruleEString ) ) )*
                    loop146:
                    do {
                        int alt146=2;
                        int LA146_0 = input.LA(1);

                        if ( (LA146_0==24) ) {
                            alt146=1;
                        }


                        switch (alt146) {
                    	case 1 :
                    	    // InternalTextualRCV.g:6439:5: otherlv_45= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_45=(Token)match(input,24,FOLLOW_20); 

                    	    					newLeafNode(otherlv_45, grammarAccess.getBookChapterAccess().getCommaKeyword_27_3_0());
                    	    				
                    	    // InternalTextualRCV.g:6443:5: ( ( ruleEString ) )
                    	    // InternalTextualRCV.g:6444:6: ( ruleEString )
                    	    {
                    	    // InternalTextualRCV.g:6444:6: ( ruleEString )
                    	    // InternalTextualRCV.g:6445:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getBookChapterRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getBookChapterAccess().getRelatedProjectsGrantCrossReference_27_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_62);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop146;
                        }
                    } while (true);

                    otherlv_47=(Token)match(input,53,FOLLOW_124); 

                    				newLeafNode(otherlv_47, grammarAccess.getBookChapterAccess().getRightParenthesisKeyword_27_4());
                    			

                    }
                    break;

            }

            // InternalTextualRCV.g:6465:3: (otherlv_48= 'bookeditors' otherlv_49= '(' ( ( ruleEString ) ) (otherlv_51= ',' ( ( ruleEString ) ) )* otherlv_53= ')' )?
            int alt149=2;
            int LA149_0 = input.LA(1);

            if ( (LA149_0==101) ) {
                alt149=1;
            }
            switch (alt149) {
                case 1 :
                    // InternalTextualRCV.g:6466:4: otherlv_48= 'bookeditors' otherlv_49= '(' ( ( ruleEString ) ) (otherlv_51= ',' ( ( ruleEString ) ) )* otherlv_53= ')'
                    {
                    otherlv_48=(Token)match(input,101,FOLLOW_61); 

                    				newLeafNode(otherlv_48, grammarAccess.getBookChapterAccess().getBookeditorsKeyword_28_0());
                    			
                    otherlv_49=(Token)match(input,52,FOLLOW_20); 

                    				newLeafNode(otherlv_49, grammarAccess.getBookChapterAccess().getLeftParenthesisKeyword_28_1());
                    			
                    // InternalTextualRCV.g:6474:4: ( ( ruleEString ) )
                    // InternalTextualRCV.g:6475:5: ( ruleEString )
                    {
                    // InternalTextualRCV.g:6475:5: ( ruleEString )
                    // InternalTextualRCV.g:6476:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getBookChapterRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getBookChapterAccess().getBookeditorsPersonCrossReference_28_2_0());
                    					
                    pushFollow(FOLLOW_62);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:6490:4: (otherlv_51= ',' ( ( ruleEString ) ) )*
                    loop148:
                    do {
                        int alt148=2;
                        int LA148_0 = input.LA(1);

                        if ( (LA148_0==24) ) {
                            alt148=1;
                        }


                        switch (alt148) {
                    	case 1 :
                    	    // InternalTextualRCV.g:6491:5: otherlv_51= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_51=(Token)match(input,24,FOLLOW_20); 

                    	    					newLeafNode(otherlv_51, grammarAccess.getBookChapterAccess().getCommaKeyword_28_3_0());
                    	    				
                    	    // InternalTextualRCV.g:6495:5: ( ( ruleEString ) )
                    	    // InternalTextualRCV.g:6496:6: ( ruleEString )
                    	    {
                    	    // InternalTextualRCV.g:6496:6: ( ruleEString )
                    	    // InternalTextualRCV.g:6497:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getBookChapterRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getBookChapterAccess().getBookeditorsPersonCrossReference_28_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_62);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop148;
                        }
                    } while (true);

                    otherlv_53=(Token)match(input,53,FOLLOW_104); 

                    				newLeafNode(otherlv_53, grammarAccess.getBookChapterAccess().getRightParenthesisKeyword_28_4());
                    			

                    }
                    break;

            }

            otherlv_54=(Token)match(input,88,FOLLOW_9); 

            			newLeafNode(otherlv_54, grammarAccess.getBookChapterAccess().getDateKeyword_29());
            		
            // InternalTextualRCV.g:6521:3: ( (lv_date_55_0= rulePublishedDate ) )
            // InternalTextualRCV.g:6522:4: (lv_date_55_0= rulePublishedDate )
            {
            // InternalTextualRCV.g:6522:4: (lv_date_55_0= rulePublishedDate )
            // InternalTextualRCV.g:6523:5: lv_date_55_0= rulePublishedDate
            {

            					newCompositeNode(grammarAccess.getBookChapterAccess().getDatePublishedDateParserRuleCall_30_0());
            				
            pushFollow(FOLLOW_137);
            lv_date_55_0=rulePublishedDate();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getBookChapterRule());
            					}
            					set(
            						current,
            						"date",
            						lv_date_55_0,
            						"org.montex.researchcv.xtext.TextualRCV.PublishedDate");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:6540:3: (otherlv_56= 'ISBN' otherlv_57= '{' ( (lv_ISBN_58_0= ruleSerialNumber ) ) (otherlv_59= ',' ( (lv_ISBN_60_0= ruleSerialNumber ) ) )* otherlv_61= '}' )?
            int alt151=2;
            int LA151_0 = input.LA(1);

            if ( (LA151_0==102) ) {
                alt151=1;
            }
            switch (alt151) {
                case 1 :
                    // InternalTextualRCV.g:6541:4: otherlv_56= 'ISBN' otherlv_57= '{' ( (lv_ISBN_58_0= ruleSerialNumber ) ) (otherlv_59= ',' ( (lv_ISBN_60_0= ruleSerialNumber ) ) )* otherlv_61= '}'
                    {
                    otherlv_56=(Token)match(input,102,FOLLOW_3); 

                    				newLeafNode(otherlv_56, grammarAccess.getBookChapterAccess().getISBNKeyword_31_0());
                    			
                    otherlv_57=(Token)match(input,16,FOLLOW_106); 

                    				newLeafNode(otherlv_57, grammarAccess.getBookChapterAccess().getLeftCurlyBracketKeyword_31_1());
                    			
                    // InternalTextualRCV.g:6549:4: ( (lv_ISBN_58_0= ruleSerialNumber ) )
                    // InternalTextualRCV.g:6550:5: (lv_ISBN_58_0= ruleSerialNumber )
                    {
                    // InternalTextualRCV.g:6550:5: (lv_ISBN_58_0= ruleSerialNumber )
                    // InternalTextualRCV.g:6551:6: lv_ISBN_58_0= ruleSerialNumber
                    {

                    						newCompositeNode(grammarAccess.getBookChapterAccess().getISBNSerialNumberParserRuleCall_31_2_0());
                    					
                    pushFollow(FOLLOW_40);
                    lv_ISBN_58_0=ruleSerialNumber();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getBookChapterRule());
                    						}
                    						add(
                    							current,
                    							"ISBN",
                    							lv_ISBN_58_0,
                    							"org.montex.researchcv.xtext.TextualRCV.SerialNumber");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:6568:4: (otherlv_59= ',' ( (lv_ISBN_60_0= ruleSerialNumber ) ) )*
                    loop150:
                    do {
                        int alt150=2;
                        int LA150_0 = input.LA(1);

                        if ( (LA150_0==24) ) {
                            alt150=1;
                        }


                        switch (alt150) {
                    	case 1 :
                    	    // InternalTextualRCV.g:6569:5: otherlv_59= ',' ( (lv_ISBN_60_0= ruleSerialNumber ) )
                    	    {
                    	    otherlv_59=(Token)match(input,24,FOLLOW_106); 

                    	    					newLeafNode(otherlv_59, grammarAccess.getBookChapterAccess().getCommaKeyword_31_3_0());
                    	    				
                    	    // InternalTextualRCV.g:6573:5: ( (lv_ISBN_60_0= ruleSerialNumber ) )
                    	    // InternalTextualRCV.g:6574:6: (lv_ISBN_60_0= ruleSerialNumber )
                    	    {
                    	    // InternalTextualRCV.g:6574:6: (lv_ISBN_60_0= ruleSerialNumber )
                    	    // InternalTextualRCV.g:6575:7: lv_ISBN_60_0= ruleSerialNumber
                    	    {

                    	    							newCompositeNode(grammarAccess.getBookChapterAccess().getISBNSerialNumberParserRuleCall_31_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_40);
                    	    lv_ISBN_60_0=ruleSerialNumber();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getBookChapterRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"ISBN",
                    	    								lv_ISBN_60_0,
                    	    								"org.montex.researchcv.xtext.TextualRCV.SerialNumber");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop150;
                        }
                    } while (true);

                    otherlv_61=(Token)match(input,18,FOLLOW_27); 

                    				newLeafNode(otherlv_61, grammarAccess.getBookChapterAccess().getRightCurlyBracketKeyword_31_4());
                    			

                    }
                    break;

            }

            otherlv_62=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_62, grammarAccess.getBookChapterAccess().getRightCurlyBracketKeyword_32());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBookChapter"


    // $ANTLR start "entryRuleReport"
    // InternalTextualRCV.g:6606:1: entryRuleReport returns [EObject current=null] : iv_ruleReport= ruleReport EOF ;
    public final EObject entryRuleReport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReport = null;


        try {
            // InternalTextualRCV.g:6606:47: (iv_ruleReport= ruleReport EOF )
            // InternalTextualRCV.g:6607:2: iv_ruleReport= ruleReport EOF
            {
             newCompositeNode(grammarAccess.getReportRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleReport=ruleReport();

            state._fsp--;

             current =iv_ruleReport; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReport"


    // $ANTLR start "ruleReport"
    // InternalTextualRCV.g:6613:1: ruleReport returns [EObject current=null] : ( ( (lv_openAccess_0_0= 'openAccess' ) ) otherlv_1= 'Report' ( (lv_citekey_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'title' ( (lv_title_5_0= ruleEString ) ) (otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) ) )? otherlv_8= 'published' ( (lv_published_9_0= ruleEBoolean ) ) (otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) ) )? (otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) ) )? otherlv_14= 'withAuthorVersion' ( (lv_withAuthorVersion_15_0= ruleEBoolean ) ) (otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}' )? (otherlv_22= 'reportNumber' ( (lv_reportNumber_23_0= ruleEString ) ) )? (otherlv_24= 'institution' ( (lv_institution_25_0= ruleEString ) ) )? otherlv_26= 'authors' otherlv_27= '(' ( ( ruleEString ) ) (otherlv_29= ',' ( ( ruleEString ) ) )* otherlv_31= ')' (otherlv_32= 'relatedProjects' otherlv_33= '(' ( ( ruleEString ) ) (otherlv_35= ',' ( ( ruleEString ) ) )* otherlv_37= ')' )? otherlv_38= 'date' ( (lv_date_39_0= rulePublishedDate ) ) otherlv_40= '}' ) ;
    public final EObject ruleReport() throws RecognitionException {
        EObject current = null;

        Token lv_openAccess_0_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_21=null;
        Token otherlv_22=null;
        Token otherlv_24=null;
        Token otherlv_26=null;
        Token otherlv_27=null;
        Token otherlv_29=null;
        Token otherlv_31=null;
        Token otherlv_32=null;
        Token otherlv_33=null;
        Token otherlv_35=null;
        Token otherlv_37=null;
        Token otherlv_38=null;
        Token otherlv_40=null;
        AntlrDatatypeRuleToken lv_citekey_2_0 = null;

        AntlrDatatypeRuleToken lv_title_5_0 = null;

        AntlrDatatypeRuleToken lv_URL_7_0 = null;

        AntlrDatatypeRuleToken lv_published_9_0 = null;

        AntlrDatatypeRuleToken lv_DOI_11_0 = null;

        AntlrDatatypeRuleToken lv_abstract_13_0 = null;

        AntlrDatatypeRuleToken lv_withAuthorVersion_15_0 = null;

        AntlrDatatypeRuleToken lv_notes_18_0 = null;

        AntlrDatatypeRuleToken lv_notes_20_0 = null;

        AntlrDatatypeRuleToken lv_reportNumber_23_0 = null;

        AntlrDatatypeRuleToken lv_institution_25_0 = null;

        EObject lv_date_39_0 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:6619:2: ( ( ( (lv_openAccess_0_0= 'openAccess' ) ) otherlv_1= 'Report' ( (lv_citekey_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'title' ( (lv_title_5_0= ruleEString ) ) (otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) ) )? otherlv_8= 'published' ( (lv_published_9_0= ruleEBoolean ) ) (otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) ) )? (otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) ) )? otherlv_14= 'withAuthorVersion' ( (lv_withAuthorVersion_15_0= ruleEBoolean ) ) (otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}' )? (otherlv_22= 'reportNumber' ( (lv_reportNumber_23_0= ruleEString ) ) )? (otherlv_24= 'institution' ( (lv_institution_25_0= ruleEString ) ) )? otherlv_26= 'authors' otherlv_27= '(' ( ( ruleEString ) ) (otherlv_29= ',' ( ( ruleEString ) ) )* otherlv_31= ')' (otherlv_32= 'relatedProjects' otherlv_33= '(' ( ( ruleEString ) ) (otherlv_35= ',' ( ( ruleEString ) ) )* otherlv_37= ')' )? otherlv_38= 'date' ( (lv_date_39_0= rulePublishedDate ) ) otherlv_40= '}' ) )
            // InternalTextualRCV.g:6620:2: ( ( (lv_openAccess_0_0= 'openAccess' ) ) otherlv_1= 'Report' ( (lv_citekey_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'title' ( (lv_title_5_0= ruleEString ) ) (otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) ) )? otherlv_8= 'published' ( (lv_published_9_0= ruleEBoolean ) ) (otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) ) )? (otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) ) )? otherlv_14= 'withAuthorVersion' ( (lv_withAuthorVersion_15_0= ruleEBoolean ) ) (otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}' )? (otherlv_22= 'reportNumber' ( (lv_reportNumber_23_0= ruleEString ) ) )? (otherlv_24= 'institution' ( (lv_institution_25_0= ruleEString ) ) )? otherlv_26= 'authors' otherlv_27= '(' ( ( ruleEString ) ) (otherlv_29= ',' ( ( ruleEString ) ) )* otherlv_31= ')' (otherlv_32= 'relatedProjects' otherlv_33= '(' ( ( ruleEString ) ) (otherlv_35= ',' ( ( ruleEString ) ) )* otherlv_37= ')' )? otherlv_38= 'date' ( (lv_date_39_0= rulePublishedDate ) ) otherlv_40= '}' )
            {
            // InternalTextualRCV.g:6620:2: ( ( (lv_openAccess_0_0= 'openAccess' ) ) otherlv_1= 'Report' ( (lv_citekey_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'title' ( (lv_title_5_0= ruleEString ) ) (otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) ) )? otherlv_8= 'published' ( (lv_published_9_0= ruleEBoolean ) ) (otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) ) )? (otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) ) )? otherlv_14= 'withAuthorVersion' ( (lv_withAuthorVersion_15_0= ruleEBoolean ) ) (otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}' )? (otherlv_22= 'reportNumber' ( (lv_reportNumber_23_0= ruleEString ) ) )? (otherlv_24= 'institution' ( (lv_institution_25_0= ruleEString ) ) )? otherlv_26= 'authors' otherlv_27= '(' ( ( ruleEString ) ) (otherlv_29= ',' ( ( ruleEString ) ) )* otherlv_31= ')' (otherlv_32= 'relatedProjects' otherlv_33= '(' ( ( ruleEString ) ) (otherlv_35= ',' ( ( ruleEString ) ) )* otherlv_37= ')' )? otherlv_38= 'date' ( (lv_date_39_0= rulePublishedDate ) ) otherlv_40= '}' )
            // InternalTextualRCV.g:6621:3: ( (lv_openAccess_0_0= 'openAccess' ) ) otherlv_1= 'Report' ( (lv_citekey_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'title' ( (lv_title_5_0= ruleEString ) ) (otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) ) )? otherlv_8= 'published' ( (lv_published_9_0= ruleEBoolean ) ) (otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) ) )? (otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) ) )? otherlv_14= 'withAuthorVersion' ( (lv_withAuthorVersion_15_0= ruleEBoolean ) ) (otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}' )? (otherlv_22= 'reportNumber' ( (lv_reportNumber_23_0= ruleEString ) ) )? (otherlv_24= 'institution' ( (lv_institution_25_0= ruleEString ) ) )? otherlv_26= 'authors' otherlv_27= '(' ( ( ruleEString ) ) (otherlv_29= ',' ( ( ruleEString ) ) )* otherlv_31= ')' (otherlv_32= 'relatedProjects' otherlv_33= '(' ( ( ruleEString ) ) (otherlv_35= ',' ( ( ruleEString ) ) )* otherlv_37= ')' )? otherlv_38= 'date' ( (lv_date_39_0= rulePublishedDate ) ) otherlv_40= '}'
            {
            // InternalTextualRCV.g:6621:3: ( (lv_openAccess_0_0= 'openAccess' ) )
            // InternalTextualRCV.g:6622:4: (lv_openAccess_0_0= 'openAccess' )
            {
            // InternalTextualRCV.g:6622:4: (lv_openAccess_0_0= 'openAccess' )
            // InternalTextualRCV.g:6623:5: lv_openAccess_0_0= 'openAccess'
            {
            lv_openAccess_0_0=(Token)match(input,73,FOLLOW_145); 

            					newLeafNode(lv_openAccess_0_0, grammarAccess.getReportAccess().getOpenAccessOpenAccessKeyword_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getReportRule());
            					}
            					setWithLastConsumed(current, "openAccess", lv_openAccess_0_0 != null, "openAccess");
            				

            }


            }

            otherlv_1=(Token)match(input,110,FOLLOW_20); 

            			newLeafNode(otherlv_1, grammarAccess.getReportAccess().getReportKeyword_1());
            		
            // InternalTextualRCV.g:6639:3: ( (lv_citekey_2_0= ruleEString ) )
            // InternalTextualRCV.g:6640:4: (lv_citekey_2_0= ruleEString )
            {
            // InternalTextualRCV.g:6640:4: (lv_citekey_2_0= ruleEString )
            // InternalTextualRCV.g:6641:5: lv_citekey_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getReportAccess().getCitekeyEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_3);
            lv_citekey_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getReportRule());
            					}
            					set(
            						current,
            						"citekey",
            						lv_citekey_2_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,16,FOLLOW_58); 

            			newLeafNode(otherlv_3, grammarAccess.getReportAccess().getLeftCurlyBracketKeyword_3());
            		
            otherlv_4=(Token)match(input,49,FOLLOW_20); 

            			newLeafNode(otherlv_4, grammarAccess.getReportAccess().getTitleKeyword_4());
            		
            // InternalTextualRCV.g:6666:3: ( (lv_title_5_0= ruleEString ) )
            // InternalTextualRCV.g:6667:4: (lv_title_5_0= ruleEString )
            {
            // InternalTextualRCV.g:6667:4: (lv_title_5_0= ruleEString )
            // InternalTextualRCV.g:6668:5: lv_title_5_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getReportAccess().getTitleEStringParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_89);
            lv_title_5_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getReportRule());
            					}
            					set(
            						current,
            						"title",
            						lv_title_5_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:6685:3: (otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) ) )?
            int alt152=2;
            int LA152_0 = input.LA(1);

            if ( (LA152_0==47) ) {
                alt152=1;
            }
            switch (alt152) {
                case 1 :
                    // InternalTextualRCV.g:6686:4: otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) )
                    {
                    otherlv_6=(Token)match(input,47,FOLLOW_20); 

                    				newLeafNode(otherlv_6, grammarAccess.getReportAccess().getURLKeyword_6_0());
                    			
                    // InternalTextualRCV.g:6690:4: ( (lv_URL_7_0= ruleEString ) )
                    // InternalTextualRCV.g:6691:5: (lv_URL_7_0= ruleEString )
                    {
                    // InternalTextualRCV.g:6691:5: (lv_URL_7_0= ruleEString )
                    // InternalTextualRCV.g:6692:6: lv_URL_7_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getReportAccess().getURLEStringParserRuleCall_6_1_0());
                    					
                    pushFollow(FOLLOW_90);
                    lv_URL_7_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getReportRule());
                    						}
                    						set(
                    							current,
                    							"URL",
                    							lv_URL_7_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_8=(Token)match(input,75,FOLLOW_91); 

            			newLeafNode(otherlv_8, grammarAccess.getReportAccess().getPublishedKeyword_7());
            		
            // InternalTextualRCV.g:6714:3: ( (lv_published_9_0= ruleEBoolean ) )
            // InternalTextualRCV.g:6715:4: (lv_published_9_0= ruleEBoolean )
            {
            // InternalTextualRCV.g:6715:4: (lv_published_9_0= ruleEBoolean )
            // InternalTextualRCV.g:6716:5: lv_published_9_0= ruleEBoolean
            {

            					newCompositeNode(grammarAccess.getReportAccess().getPublishedEBooleanParserRuleCall_8_0());
            				
            pushFollow(FOLLOW_92);
            lv_published_9_0=ruleEBoolean();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getReportRule());
            					}
            					set(
            						current,
            						"published",
            						lv_published_9_0,
            						"org.montex.researchcv.xtext.TextualRCV.EBoolean");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:6733:3: (otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) ) )?
            int alt153=2;
            int LA153_0 = input.LA(1);

            if ( (LA153_0==76) ) {
                alt153=1;
            }
            switch (alt153) {
                case 1 :
                    // InternalTextualRCV.g:6734:4: otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) )
                    {
                    otherlv_10=(Token)match(input,76,FOLLOW_20); 

                    				newLeafNode(otherlv_10, grammarAccess.getReportAccess().getDOIKeyword_9_0());
                    			
                    // InternalTextualRCV.g:6738:4: ( (lv_DOI_11_0= ruleEString ) )
                    // InternalTextualRCV.g:6739:5: (lv_DOI_11_0= ruleEString )
                    {
                    // InternalTextualRCV.g:6739:5: (lv_DOI_11_0= ruleEString )
                    // InternalTextualRCV.g:6740:6: lv_DOI_11_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getReportAccess().getDOIEStringParserRuleCall_9_1_0());
                    					
                    pushFollow(FOLLOW_93);
                    lv_DOI_11_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getReportRule());
                    						}
                    						set(
                    							current,
                    							"DOI",
                    							lv_DOI_11_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:6758:3: (otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) ) )?
            int alt154=2;
            int LA154_0 = input.LA(1);

            if ( (LA154_0==77) ) {
                alt154=1;
            }
            switch (alt154) {
                case 1 :
                    // InternalTextualRCV.g:6759:4: otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) )
                    {
                    otherlv_12=(Token)match(input,77,FOLLOW_20); 

                    				newLeafNode(otherlv_12, grammarAccess.getReportAccess().getAbstractKeyword_10_0());
                    			
                    // InternalTextualRCV.g:6763:4: ( (lv_abstract_13_0= ruleEString ) )
                    // InternalTextualRCV.g:6764:5: (lv_abstract_13_0= ruleEString )
                    {
                    // InternalTextualRCV.g:6764:5: (lv_abstract_13_0= ruleEString )
                    // InternalTextualRCV.g:6765:6: lv_abstract_13_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getReportAccess().getAbstractEStringParserRuleCall_10_1_0());
                    					
                    pushFollow(FOLLOW_94);
                    lv_abstract_13_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getReportRule());
                    						}
                    						set(
                    							current,
                    							"abstract",
                    							lv_abstract_13_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_14=(Token)match(input,78,FOLLOW_91); 

            			newLeafNode(otherlv_14, grammarAccess.getReportAccess().getWithAuthorVersionKeyword_11());
            		
            // InternalTextualRCV.g:6787:3: ( (lv_withAuthorVersion_15_0= ruleEBoolean ) )
            // InternalTextualRCV.g:6788:4: (lv_withAuthorVersion_15_0= ruleEBoolean )
            {
            // InternalTextualRCV.g:6788:4: (lv_withAuthorVersion_15_0= ruleEBoolean )
            // InternalTextualRCV.g:6789:5: lv_withAuthorVersion_15_0= ruleEBoolean
            {

            					newCompositeNode(grammarAccess.getReportAccess().getWithAuthorVersionEBooleanParserRuleCall_12_0());
            				
            pushFollow(FOLLOW_146);
            lv_withAuthorVersion_15_0=ruleEBoolean();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getReportRule());
            					}
            					set(
            						current,
            						"withAuthorVersion",
            						lv_withAuthorVersion_15_0,
            						"org.montex.researchcv.xtext.TextualRCV.EBoolean");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:6806:3: (otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}' )?
            int alt156=2;
            int LA156_0 = input.LA(1);

            if ( (LA156_0==79) ) {
                alt156=1;
            }
            switch (alt156) {
                case 1 :
                    // InternalTextualRCV.g:6807:4: otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}'
                    {
                    otherlv_16=(Token)match(input,79,FOLLOW_3); 

                    				newLeafNode(otherlv_16, grammarAccess.getReportAccess().getNotesKeyword_13_0());
                    			
                    otherlv_17=(Token)match(input,16,FOLLOW_20); 

                    				newLeafNode(otherlv_17, grammarAccess.getReportAccess().getLeftCurlyBracketKeyword_13_1());
                    			
                    // InternalTextualRCV.g:6815:4: ( (lv_notes_18_0= ruleEString ) )
                    // InternalTextualRCV.g:6816:5: (lv_notes_18_0= ruleEString )
                    {
                    // InternalTextualRCV.g:6816:5: (lv_notes_18_0= ruleEString )
                    // InternalTextualRCV.g:6817:6: lv_notes_18_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getReportAccess().getNotesEStringParserRuleCall_13_2_0());
                    					
                    pushFollow(FOLLOW_40);
                    lv_notes_18_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getReportRule());
                    						}
                    						add(
                    							current,
                    							"notes",
                    							lv_notes_18_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:6834:4: (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )*
                    loop155:
                    do {
                        int alt155=2;
                        int LA155_0 = input.LA(1);

                        if ( (LA155_0==24) ) {
                            alt155=1;
                        }


                        switch (alt155) {
                    	case 1 :
                    	    // InternalTextualRCV.g:6835:5: otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) )
                    	    {
                    	    otherlv_19=(Token)match(input,24,FOLLOW_20); 

                    	    					newLeafNode(otherlv_19, grammarAccess.getReportAccess().getCommaKeyword_13_3_0());
                    	    				
                    	    // InternalTextualRCV.g:6839:5: ( (lv_notes_20_0= ruleEString ) )
                    	    // InternalTextualRCV.g:6840:6: (lv_notes_20_0= ruleEString )
                    	    {
                    	    // InternalTextualRCV.g:6840:6: (lv_notes_20_0= ruleEString )
                    	    // InternalTextualRCV.g:6841:7: lv_notes_20_0= ruleEString
                    	    {

                    	    							newCompositeNode(grammarAccess.getReportAccess().getNotesEStringParserRuleCall_13_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_40);
                    	    lv_notes_20_0=ruleEString();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getReportRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"notes",
                    	    								lv_notes_20_0,
                    	    								"org.montex.researchcv.xtext.TextualRCV.EString");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop155;
                        }
                    } while (true);

                    otherlv_21=(Token)match(input,18,FOLLOW_147); 

                    				newLeafNode(otherlv_21, grammarAccess.getReportAccess().getRightCurlyBracketKeyword_13_4());
                    			

                    }
                    break;

            }

            // InternalTextualRCV.g:6864:3: (otherlv_22= 'reportNumber' ( (lv_reportNumber_23_0= ruleEString ) ) )?
            int alt157=2;
            int LA157_0 = input.LA(1);

            if ( (LA157_0==111) ) {
                alt157=1;
            }
            switch (alt157) {
                case 1 :
                    // InternalTextualRCV.g:6865:4: otherlv_22= 'reportNumber' ( (lv_reportNumber_23_0= ruleEString ) )
                    {
                    otherlv_22=(Token)match(input,111,FOLLOW_20); 

                    				newLeafNode(otherlv_22, grammarAccess.getReportAccess().getReportNumberKeyword_14_0());
                    			
                    // InternalTextualRCV.g:6869:4: ( (lv_reportNumber_23_0= ruleEString ) )
                    // InternalTextualRCV.g:6870:5: (lv_reportNumber_23_0= ruleEString )
                    {
                    // InternalTextualRCV.g:6870:5: (lv_reportNumber_23_0= ruleEString )
                    // InternalTextualRCV.g:6871:6: lv_reportNumber_23_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getReportAccess().getReportNumberEStringParserRuleCall_14_1_0());
                    					
                    pushFollow(FOLLOW_148);
                    lv_reportNumber_23_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getReportRule());
                    						}
                    						set(
                    							current,
                    							"reportNumber",
                    							lv_reportNumber_23_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:6889:3: (otherlv_24= 'institution' ( (lv_institution_25_0= ruleEString ) ) )?
            int alt158=2;
            int LA158_0 = input.LA(1);

            if ( (LA158_0==112) ) {
                alt158=1;
            }
            switch (alt158) {
                case 1 :
                    // InternalTextualRCV.g:6890:4: otherlv_24= 'institution' ( (lv_institution_25_0= ruleEString ) )
                    {
                    otherlv_24=(Token)match(input,112,FOLLOW_20); 

                    				newLeafNode(otherlv_24, grammarAccess.getReportAccess().getInstitutionKeyword_15_0());
                    			
                    // InternalTextualRCV.g:6894:4: ( (lv_institution_25_0= ruleEString ) )
                    // InternalTextualRCV.g:6895:5: (lv_institution_25_0= ruleEString )
                    {
                    // InternalTextualRCV.g:6895:5: (lv_institution_25_0= ruleEString )
                    // InternalTextualRCV.g:6896:6: lv_institution_25_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getReportAccess().getInstitutionEStringParserRuleCall_15_1_0());
                    					
                    pushFollow(FOLLOW_102);
                    lv_institution_25_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getReportRule());
                    						}
                    						set(
                    							current,
                    							"institution",
                    							lv_institution_25_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_26=(Token)match(input,86,FOLLOW_61); 

            			newLeafNode(otherlv_26, grammarAccess.getReportAccess().getAuthorsKeyword_16());
            		
            otherlv_27=(Token)match(input,52,FOLLOW_20); 

            			newLeafNode(otherlv_27, grammarAccess.getReportAccess().getLeftParenthesisKeyword_17());
            		
            // InternalTextualRCV.g:6922:3: ( ( ruleEString ) )
            // InternalTextualRCV.g:6923:4: ( ruleEString )
            {
            // InternalTextualRCV.g:6923:4: ( ruleEString )
            // InternalTextualRCV.g:6924:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getReportRule());
            					}
            				

            					newCompositeNode(grammarAccess.getReportAccess().getAuthorsPersonCrossReference_18_0());
            				
            pushFollow(FOLLOW_62);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:6938:3: (otherlv_29= ',' ( ( ruleEString ) ) )*
            loop159:
            do {
                int alt159=2;
                int LA159_0 = input.LA(1);

                if ( (LA159_0==24) ) {
                    alt159=1;
                }


                switch (alt159) {
            	case 1 :
            	    // InternalTextualRCV.g:6939:4: otherlv_29= ',' ( ( ruleEString ) )
            	    {
            	    otherlv_29=(Token)match(input,24,FOLLOW_20); 

            	    				newLeafNode(otherlv_29, grammarAccess.getReportAccess().getCommaKeyword_19_0());
            	    			
            	    // InternalTextualRCV.g:6943:4: ( ( ruleEString ) )
            	    // InternalTextualRCV.g:6944:5: ( ruleEString )
            	    {
            	    // InternalTextualRCV.g:6944:5: ( ruleEString )
            	    // InternalTextualRCV.g:6945:6: ruleEString
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getReportRule());
            	    						}
            	    					

            	    						newCompositeNode(grammarAccess.getReportAccess().getAuthorsPersonCrossReference_19_1_0());
            	    					
            	    pushFollow(FOLLOW_62);
            	    ruleEString();

            	    state._fsp--;


            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop159;
                }
            } while (true);

            otherlv_31=(Token)match(input,53,FOLLOW_103); 

            			newLeafNode(otherlv_31, grammarAccess.getReportAccess().getRightParenthesisKeyword_20());
            		
            // InternalTextualRCV.g:6964:3: (otherlv_32= 'relatedProjects' otherlv_33= '(' ( ( ruleEString ) ) (otherlv_35= ',' ( ( ruleEString ) ) )* otherlv_37= ')' )?
            int alt161=2;
            int LA161_0 = input.LA(1);

            if ( (LA161_0==87) ) {
                alt161=1;
            }
            switch (alt161) {
                case 1 :
                    // InternalTextualRCV.g:6965:4: otherlv_32= 'relatedProjects' otherlv_33= '(' ( ( ruleEString ) ) (otherlv_35= ',' ( ( ruleEString ) ) )* otherlv_37= ')'
                    {
                    otherlv_32=(Token)match(input,87,FOLLOW_61); 

                    				newLeafNode(otherlv_32, grammarAccess.getReportAccess().getRelatedProjectsKeyword_21_0());
                    			
                    otherlv_33=(Token)match(input,52,FOLLOW_20); 

                    				newLeafNode(otherlv_33, grammarAccess.getReportAccess().getLeftParenthesisKeyword_21_1());
                    			
                    // InternalTextualRCV.g:6973:4: ( ( ruleEString ) )
                    // InternalTextualRCV.g:6974:5: ( ruleEString )
                    {
                    // InternalTextualRCV.g:6974:5: ( ruleEString )
                    // InternalTextualRCV.g:6975:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getReportRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getReportAccess().getRelatedProjectsGrantCrossReference_21_2_0());
                    					
                    pushFollow(FOLLOW_62);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:6989:4: (otherlv_35= ',' ( ( ruleEString ) ) )*
                    loop160:
                    do {
                        int alt160=2;
                        int LA160_0 = input.LA(1);

                        if ( (LA160_0==24) ) {
                            alt160=1;
                        }


                        switch (alt160) {
                    	case 1 :
                    	    // InternalTextualRCV.g:6990:5: otherlv_35= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_35=(Token)match(input,24,FOLLOW_20); 

                    	    					newLeafNode(otherlv_35, grammarAccess.getReportAccess().getCommaKeyword_21_3_0());
                    	    				
                    	    // InternalTextualRCV.g:6994:5: ( ( ruleEString ) )
                    	    // InternalTextualRCV.g:6995:6: ( ruleEString )
                    	    {
                    	    // InternalTextualRCV.g:6995:6: ( ruleEString )
                    	    // InternalTextualRCV.g:6996:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getReportRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getReportAccess().getRelatedProjectsGrantCrossReference_21_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_62);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop160;
                        }
                    } while (true);

                    otherlv_37=(Token)match(input,53,FOLLOW_104); 

                    				newLeafNode(otherlv_37, grammarAccess.getReportAccess().getRightParenthesisKeyword_21_4());
                    			

                    }
                    break;

            }

            otherlv_38=(Token)match(input,88,FOLLOW_9); 

            			newLeafNode(otherlv_38, grammarAccess.getReportAccess().getDateKeyword_22());
            		
            // InternalTextualRCV.g:7020:3: ( (lv_date_39_0= rulePublishedDate ) )
            // InternalTextualRCV.g:7021:4: (lv_date_39_0= rulePublishedDate )
            {
            // InternalTextualRCV.g:7021:4: (lv_date_39_0= rulePublishedDate )
            // InternalTextualRCV.g:7022:5: lv_date_39_0= rulePublishedDate
            {

            					newCompositeNode(grammarAccess.getReportAccess().getDatePublishedDateParserRuleCall_23_0());
            				
            pushFollow(FOLLOW_27);
            lv_date_39_0=rulePublishedDate();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getReportRule());
            					}
            					set(
            						current,
            						"date",
            						lv_date_39_0,
            						"org.montex.researchcv.xtext.TextualRCV.PublishedDate");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_40=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_40, grammarAccess.getReportAccess().getRightCurlyBracketKeyword_24());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReport"


    // $ANTLR start "entryRuleMiscellaneous"
    // InternalTextualRCV.g:7047:1: entryRuleMiscellaneous returns [EObject current=null] : iv_ruleMiscellaneous= ruleMiscellaneous EOF ;
    public final EObject entryRuleMiscellaneous() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMiscellaneous = null;


        try {
            // InternalTextualRCV.g:7047:54: (iv_ruleMiscellaneous= ruleMiscellaneous EOF )
            // InternalTextualRCV.g:7048:2: iv_ruleMiscellaneous= ruleMiscellaneous EOF
            {
             newCompositeNode(grammarAccess.getMiscellaneousRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMiscellaneous=ruleMiscellaneous();

            state._fsp--;

             current =iv_ruleMiscellaneous; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMiscellaneous"


    // $ANTLR start "ruleMiscellaneous"
    // InternalTextualRCV.g:7054:1: ruleMiscellaneous returns [EObject current=null] : ( ( (lv_openAccess_0_0= 'openAccess' ) ) otherlv_1= 'Miscellaneous' ( (lv_citekey_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'title' ( (lv_title_5_0= ruleEString ) ) (otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) ) )? otherlv_8= 'published' ( (lv_published_9_0= ruleEBoolean ) ) (otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) ) )? (otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) ) )? otherlv_14= 'withAuthorVersion' ( (lv_withAuthorVersion_15_0= ruleEBoolean ) ) (otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}' )? otherlv_22= 'authors' otherlv_23= '(' ( ( ruleEString ) ) (otherlv_25= ',' ( ( ruleEString ) ) )* otherlv_27= ')' (otherlv_28= 'relatedProjects' otherlv_29= '(' ( ( ruleEString ) ) (otherlv_31= ',' ( ( ruleEString ) ) )* otherlv_33= ')' )? otherlv_34= 'date' ( (lv_date_35_0= rulePublishedDate ) ) otherlv_36= '}' ) ;
    public final EObject ruleMiscellaneous() throws RecognitionException {
        EObject current = null;

        Token lv_openAccess_0_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_21=null;
        Token otherlv_22=null;
        Token otherlv_23=null;
        Token otherlv_25=null;
        Token otherlv_27=null;
        Token otherlv_28=null;
        Token otherlv_29=null;
        Token otherlv_31=null;
        Token otherlv_33=null;
        Token otherlv_34=null;
        Token otherlv_36=null;
        AntlrDatatypeRuleToken lv_citekey_2_0 = null;

        AntlrDatatypeRuleToken lv_title_5_0 = null;

        AntlrDatatypeRuleToken lv_URL_7_0 = null;

        AntlrDatatypeRuleToken lv_published_9_0 = null;

        AntlrDatatypeRuleToken lv_DOI_11_0 = null;

        AntlrDatatypeRuleToken lv_abstract_13_0 = null;

        AntlrDatatypeRuleToken lv_withAuthorVersion_15_0 = null;

        AntlrDatatypeRuleToken lv_notes_18_0 = null;

        AntlrDatatypeRuleToken lv_notes_20_0 = null;

        EObject lv_date_35_0 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:7060:2: ( ( ( (lv_openAccess_0_0= 'openAccess' ) ) otherlv_1= 'Miscellaneous' ( (lv_citekey_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'title' ( (lv_title_5_0= ruleEString ) ) (otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) ) )? otherlv_8= 'published' ( (lv_published_9_0= ruleEBoolean ) ) (otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) ) )? (otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) ) )? otherlv_14= 'withAuthorVersion' ( (lv_withAuthorVersion_15_0= ruleEBoolean ) ) (otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}' )? otherlv_22= 'authors' otherlv_23= '(' ( ( ruleEString ) ) (otherlv_25= ',' ( ( ruleEString ) ) )* otherlv_27= ')' (otherlv_28= 'relatedProjects' otherlv_29= '(' ( ( ruleEString ) ) (otherlv_31= ',' ( ( ruleEString ) ) )* otherlv_33= ')' )? otherlv_34= 'date' ( (lv_date_35_0= rulePublishedDate ) ) otherlv_36= '}' ) )
            // InternalTextualRCV.g:7061:2: ( ( (lv_openAccess_0_0= 'openAccess' ) ) otherlv_1= 'Miscellaneous' ( (lv_citekey_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'title' ( (lv_title_5_0= ruleEString ) ) (otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) ) )? otherlv_8= 'published' ( (lv_published_9_0= ruleEBoolean ) ) (otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) ) )? (otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) ) )? otherlv_14= 'withAuthorVersion' ( (lv_withAuthorVersion_15_0= ruleEBoolean ) ) (otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}' )? otherlv_22= 'authors' otherlv_23= '(' ( ( ruleEString ) ) (otherlv_25= ',' ( ( ruleEString ) ) )* otherlv_27= ')' (otherlv_28= 'relatedProjects' otherlv_29= '(' ( ( ruleEString ) ) (otherlv_31= ',' ( ( ruleEString ) ) )* otherlv_33= ')' )? otherlv_34= 'date' ( (lv_date_35_0= rulePublishedDate ) ) otherlv_36= '}' )
            {
            // InternalTextualRCV.g:7061:2: ( ( (lv_openAccess_0_0= 'openAccess' ) ) otherlv_1= 'Miscellaneous' ( (lv_citekey_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'title' ( (lv_title_5_0= ruleEString ) ) (otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) ) )? otherlv_8= 'published' ( (lv_published_9_0= ruleEBoolean ) ) (otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) ) )? (otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) ) )? otherlv_14= 'withAuthorVersion' ( (lv_withAuthorVersion_15_0= ruleEBoolean ) ) (otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}' )? otherlv_22= 'authors' otherlv_23= '(' ( ( ruleEString ) ) (otherlv_25= ',' ( ( ruleEString ) ) )* otherlv_27= ')' (otherlv_28= 'relatedProjects' otherlv_29= '(' ( ( ruleEString ) ) (otherlv_31= ',' ( ( ruleEString ) ) )* otherlv_33= ')' )? otherlv_34= 'date' ( (lv_date_35_0= rulePublishedDate ) ) otherlv_36= '}' )
            // InternalTextualRCV.g:7062:3: ( (lv_openAccess_0_0= 'openAccess' ) ) otherlv_1= 'Miscellaneous' ( (lv_citekey_2_0= ruleEString ) ) otherlv_3= '{' otherlv_4= 'title' ( (lv_title_5_0= ruleEString ) ) (otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) ) )? otherlv_8= 'published' ( (lv_published_9_0= ruleEBoolean ) ) (otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) ) )? (otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) ) )? otherlv_14= 'withAuthorVersion' ( (lv_withAuthorVersion_15_0= ruleEBoolean ) ) (otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}' )? otherlv_22= 'authors' otherlv_23= '(' ( ( ruleEString ) ) (otherlv_25= ',' ( ( ruleEString ) ) )* otherlv_27= ')' (otherlv_28= 'relatedProjects' otherlv_29= '(' ( ( ruleEString ) ) (otherlv_31= ',' ( ( ruleEString ) ) )* otherlv_33= ')' )? otherlv_34= 'date' ( (lv_date_35_0= rulePublishedDate ) ) otherlv_36= '}'
            {
            // InternalTextualRCV.g:7062:3: ( (lv_openAccess_0_0= 'openAccess' ) )
            // InternalTextualRCV.g:7063:4: (lv_openAccess_0_0= 'openAccess' )
            {
            // InternalTextualRCV.g:7063:4: (lv_openAccess_0_0= 'openAccess' )
            // InternalTextualRCV.g:7064:5: lv_openAccess_0_0= 'openAccess'
            {
            lv_openAccess_0_0=(Token)match(input,73,FOLLOW_149); 

            					newLeafNode(lv_openAccess_0_0, grammarAccess.getMiscellaneousAccess().getOpenAccessOpenAccessKeyword_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getMiscellaneousRule());
            					}
            					setWithLastConsumed(current, "openAccess", lv_openAccess_0_0 != null, "openAccess");
            				

            }


            }

            otherlv_1=(Token)match(input,113,FOLLOW_20); 

            			newLeafNode(otherlv_1, grammarAccess.getMiscellaneousAccess().getMiscellaneousKeyword_1());
            		
            // InternalTextualRCV.g:7080:3: ( (lv_citekey_2_0= ruleEString ) )
            // InternalTextualRCV.g:7081:4: (lv_citekey_2_0= ruleEString )
            {
            // InternalTextualRCV.g:7081:4: (lv_citekey_2_0= ruleEString )
            // InternalTextualRCV.g:7082:5: lv_citekey_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getMiscellaneousAccess().getCitekeyEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_3);
            lv_citekey_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getMiscellaneousRule());
            					}
            					set(
            						current,
            						"citekey",
            						lv_citekey_2_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,16,FOLLOW_58); 

            			newLeafNode(otherlv_3, grammarAccess.getMiscellaneousAccess().getLeftCurlyBracketKeyword_3());
            		
            otherlv_4=(Token)match(input,49,FOLLOW_20); 

            			newLeafNode(otherlv_4, grammarAccess.getMiscellaneousAccess().getTitleKeyword_4());
            		
            // InternalTextualRCV.g:7107:3: ( (lv_title_5_0= ruleEString ) )
            // InternalTextualRCV.g:7108:4: (lv_title_5_0= ruleEString )
            {
            // InternalTextualRCV.g:7108:4: (lv_title_5_0= ruleEString )
            // InternalTextualRCV.g:7109:5: lv_title_5_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getMiscellaneousAccess().getTitleEStringParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_89);
            lv_title_5_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getMiscellaneousRule());
            					}
            					set(
            						current,
            						"title",
            						lv_title_5_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:7126:3: (otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) ) )?
            int alt162=2;
            int LA162_0 = input.LA(1);

            if ( (LA162_0==47) ) {
                alt162=1;
            }
            switch (alt162) {
                case 1 :
                    // InternalTextualRCV.g:7127:4: otherlv_6= 'URL' ( (lv_URL_7_0= ruleEString ) )
                    {
                    otherlv_6=(Token)match(input,47,FOLLOW_20); 

                    				newLeafNode(otherlv_6, grammarAccess.getMiscellaneousAccess().getURLKeyword_6_0());
                    			
                    // InternalTextualRCV.g:7131:4: ( (lv_URL_7_0= ruleEString ) )
                    // InternalTextualRCV.g:7132:5: (lv_URL_7_0= ruleEString )
                    {
                    // InternalTextualRCV.g:7132:5: (lv_URL_7_0= ruleEString )
                    // InternalTextualRCV.g:7133:6: lv_URL_7_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getMiscellaneousAccess().getURLEStringParserRuleCall_6_1_0());
                    					
                    pushFollow(FOLLOW_90);
                    lv_URL_7_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getMiscellaneousRule());
                    						}
                    						set(
                    							current,
                    							"URL",
                    							lv_URL_7_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_8=(Token)match(input,75,FOLLOW_91); 

            			newLeafNode(otherlv_8, grammarAccess.getMiscellaneousAccess().getPublishedKeyword_7());
            		
            // InternalTextualRCV.g:7155:3: ( (lv_published_9_0= ruleEBoolean ) )
            // InternalTextualRCV.g:7156:4: (lv_published_9_0= ruleEBoolean )
            {
            // InternalTextualRCV.g:7156:4: (lv_published_9_0= ruleEBoolean )
            // InternalTextualRCV.g:7157:5: lv_published_9_0= ruleEBoolean
            {

            					newCompositeNode(grammarAccess.getMiscellaneousAccess().getPublishedEBooleanParserRuleCall_8_0());
            				
            pushFollow(FOLLOW_92);
            lv_published_9_0=ruleEBoolean();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getMiscellaneousRule());
            					}
            					set(
            						current,
            						"published",
            						lv_published_9_0,
            						"org.montex.researchcv.xtext.TextualRCV.EBoolean");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:7174:3: (otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) ) )?
            int alt163=2;
            int LA163_0 = input.LA(1);

            if ( (LA163_0==76) ) {
                alt163=1;
            }
            switch (alt163) {
                case 1 :
                    // InternalTextualRCV.g:7175:4: otherlv_10= 'DOI' ( (lv_DOI_11_0= ruleEString ) )
                    {
                    otherlv_10=(Token)match(input,76,FOLLOW_20); 

                    				newLeafNode(otherlv_10, grammarAccess.getMiscellaneousAccess().getDOIKeyword_9_0());
                    			
                    // InternalTextualRCV.g:7179:4: ( (lv_DOI_11_0= ruleEString ) )
                    // InternalTextualRCV.g:7180:5: (lv_DOI_11_0= ruleEString )
                    {
                    // InternalTextualRCV.g:7180:5: (lv_DOI_11_0= ruleEString )
                    // InternalTextualRCV.g:7181:6: lv_DOI_11_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getMiscellaneousAccess().getDOIEStringParserRuleCall_9_1_0());
                    					
                    pushFollow(FOLLOW_93);
                    lv_DOI_11_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getMiscellaneousRule());
                    						}
                    						set(
                    							current,
                    							"DOI",
                    							lv_DOI_11_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:7199:3: (otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) ) )?
            int alt164=2;
            int LA164_0 = input.LA(1);

            if ( (LA164_0==77) ) {
                alt164=1;
            }
            switch (alt164) {
                case 1 :
                    // InternalTextualRCV.g:7200:4: otherlv_12= 'abstract' ( (lv_abstract_13_0= ruleEString ) )
                    {
                    otherlv_12=(Token)match(input,77,FOLLOW_20); 

                    				newLeafNode(otherlv_12, grammarAccess.getMiscellaneousAccess().getAbstractKeyword_10_0());
                    			
                    // InternalTextualRCV.g:7204:4: ( (lv_abstract_13_0= ruleEString ) )
                    // InternalTextualRCV.g:7205:5: (lv_abstract_13_0= ruleEString )
                    {
                    // InternalTextualRCV.g:7205:5: (lv_abstract_13_0= ruleEString )
                    // InternalTextualRCV.g:7206:6: lv_abstract_13_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getMiscellaneousAccess().getAbstractEStringParserRuleCall_10_1_0());
                    					
                    pushFollow(FOLLOW_94);
                    lv_abstract_13_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getMiscellaneousRule());
                    						}
                    						set(
                    							current,
                    							"abstract",
                    							lv_abstract_13_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_14=(Token)match(input,78,FOLLOW_91); 

            			newLeafNode(otherlv_14, grammarAccess.getMiscellaneousAccess().getWithAuthorVersionKeyword_11());
            		
            // InternalTextualRCV.g:7228:3: ( (lv_withAuthorVersion_15_0= ruleEBoolean ) )
            // InternalTextualRCV.g:7229:4: (lv_withAuthorVersion_15_0= ruleEBoolean )
            {
            // InternalTextualRCV.g:7229:4: (lv_withAuthorVersion_15_0= ruleEBoolean )
            // InternalTextualRCV.g:7230:5: lv_withAuthorVersion_15_0= ruleEBoolean
            {

            					newCompositeNode(grammarAccess.getMiscellaneousAccess().getWithAuthorVersionEBooleanParserRuleCall_12_0());
            				
            pushFollow(FOLLOW_150);
            lv_withAuthorVersion_15_0=ruleEBoolean();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getMiscellaneousRule());
            					}
            					set(
            						current,
            						"withAuthorVersion",
            						lv_withAuthorVersion_15_0,
            						"org.montex.researchcv.xtext.TextualRCV.EBoolean");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:7247:3: (otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}' )?
            int alt166=2;
            int LA166_0 = input.LA(1);

            if ( (LA166_0==79) ) {
                alt166=1;
            }
            switch (alt166) {
                case 1 :
                    // InternalTextualRCV.g:7248:4: otherlv_16= 'notes' otherlv_17= '{' ( (lv_notes_18_0= ruleEString ) ) (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )* otherlv_21= '}'
                    {
                    otherlv_16=(Token)match(input,79,FOLLOW_3); 

                    				newLeafNode(otherlv_16, grammarAccess.getMiscellaneousAccess().getNotesKeyword_13_0());
                    			
                    otherlv_17=(Token)match(input,16,FOLLOW_20); 

                    				newLeafNode(otherlv_17, grammarAccess.getMiscellaneousAccess().getLeftCurlyBracketKeyword_13_1());
                    			
                    // InternalTextualRCV.g:7256:4: ( (lv_notes_18_0= ruleEString ) )
                    // InternalTextualRCV.g:7257:5: (lv_notes_18_0= ruleEString )
                    {
                    // InternalTextualRCV.g:7257:5: (lv_notes_18_0= ruleEString )
                    // InternalTextualRCV.g:7258:6: lv_notes_18_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getMiscellaneousAccess().getNotesEStringParserRuleCall_13_2_0());
                    					
                    pushFollow(FOLLOW_40);
                    lv_notes_18_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getMiscellaneousRule());
                    						}
                    						add(
                    							current,
                    							"notes",
                    							lv_notes_18_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:7275:4: (otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) ) )*
                    loop165:
                    do {
                        int alt165=2;
                        int LA165_0 = input.LA(1);

                        if ( (LA165_0==24) ) {
                            alt165=1;
                        }


                        switch (alt165) {
                    	case 1 :
                    	    // InternalTextualRCV.g:7276:5: otherlv_19= ',' ( (lv_notes_20_0= ruleEString ) )
                    	    {
                    	    otherlv_19=(Token)match(input,24,FOLLOW_20); 

                    	    					newLeafNode(otherlv_19, grammarAccess.getMiscellaneousAccess().getCommaKeyword_13_3_0());
                    	    				
                    	    // InternalTextualRCV.g:7280:5: ( (lv_notes_20_0= ruleEString ) )
                    	    // InternalTextualRCV.g:7281:6: (lv_notes_20_0= ruleEString )
                    	    {
                    	    // InternalTextualRCV.g:7281:6: (lv_notes_20_0= ruleEString )
                    	    // InternalTextualRCV.g:7282:7: lv_notes_20_0= ruleEString
                    	    {

                    	    							newCompositeNode(grammarAccess.getMiscellaneousAccess().getNotesEStringParserRuleCall_13_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_40);
                    	    lv_notes_20_0=ruleEString();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getMiscellaneousRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"notes",
                    	    								lv_notes_20_0,
                    	    								"org.montex.researchcv.xtext.TextualRCV.EString");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop165;
                        }
                    } while (true);

                    otherlv_21=(Token)match(input,18,FOLLOW_102); 

                    				newLeafNode(otherlv_21, grammarAccess.getMiscellaneousAccess().getRightCurlyBracketKeyword_13_4());
                    			

                    }
                    break;

            }

            otherlv_22=(Token)match(input,86,FOLLOW_61); 

            			newLeafNode(otherlv_22, grammarAccess.getMiscellaneousAccess().getAuthorsKeyword_14());
            		
            otherlv_23=(Token)match(input,52,FOLLOW_20); 

            			newLeafNode(otherlv_23, grammarAccess.getMiscellaneousAccess().getLeftParenthesisKeyword_15());
            		
            // InternalTextualRCV.g:7313:3: ( ( ruleEString ) )
            // InternalTextualRCV.g:7314:4: ( ruleEString )
            {
            // InternalTextualRCV.g:7314:4: ( ruleEString )
            // InternalTextualRCV.g:7315:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getMiscellaneousRule());
            					}
            				

            					newCompositeNode(grammarAccess.getMiscellaneousAccess().getAuthorsPersonCrossReference_16_0());
            				
            pushFollow(FOLLOW_62);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:7329:3: (otherlv_25= ',' ( ( ruleEString ) ) )*
            loop167:
            do {
                int alt167=2;
                int LA167_0 = input.LA(1);

                if ( (LA167_0==24) ) {
                    alt167=1;
                }


                switch (alt167) {
            	case 1 :
            	    // InternalTextualRCV.g:7330:4: otherlv_25= ',' ( ( ruleEString ) )
            	    {
            	    otherlv_25=(Token)match(input,24,FOLLOW_20); 

            	    				newLeafNode(otherlv_25, grammarAccess.getMiscellaneousAccess().getCommaKeyword_17_0());
            	    			
            	    // InternalTextualRCV.g:7334:4: ( ( ruleEString ) )
            	    // InternalTextualRCV.g:7335:5: ( ruleEString )
            	    {
            	    // InternalTextualRCV.g:7335:5: ( ruleEString )
            	    // InternalTextualRCV.g:7336:6: ruleEString
            	    {

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getMiscellaneousRule());
            	    						}
            	    					

            	    						newCompositeNode(grammarAccess.getMiscellaneousAccess().getAuthorsPersonCrossReference_17_1_0());
            	    					
            	    pushFollow(FOLLOW_62);
            	    ruleEString();

            	    state._fsp--;


            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop167;
                }
            } while (true);

            otherlv_27=(Token)match(input,53,FOLLOW_103); 

            			newLeafNode(otherlv_27, grammarAccess.getMiscellaneousAccess().getRightParenthesisKeyword_18());
            		
            // InternalTextualRCV.g:7355:3: (otherlv_28= 'relatedProjects' otherlv_29= '(' ( ( ruleEString ) ) (otherlv_31= ',' ( ( ruleEString ) ) )* otherlv_33= ')' )?
            int alt169=2;
            int LA169_0 = input.LA(1);

            if ( (LA169_0==87) ) {
                alt169=1;
            }
            switch (alt169) {
                case 1 :
                    // InternalTextualRCV.g:7356:4: otherlv_28= 'relatedProjects' otherlv_29= '(' ( ( ruleEString ) ) (otherlv_31= ',' ( ( ruleEString ) ) )* otherlv_33= ')'
                    {
                    otherlv_28=(Token)match(input,87,FOLLOW_61); 

                    				newLeafNode(otherlv_28, grammarAccess.getMiscellaneousAccess().getRelatedProjectsKeyword_19_0());
                    			
                    otherlv_29=(Token)match(input,52,FOLLOW_20); 

                    				newLeafNode(otherlv_29, grammarAccess.getMiscellaneousAccess().getLeftParenthesisKeyword_19_1());
                    			
                    // InternalTextualRCV.g:7364:4: ( ( ruleEString ) )
                    // InternalTextualRCV.g:7365:5: ( ruleEString )
                    {
                    // InternalTextualRCV.g:7365:5: ( ruleEString )
                    // InternalTextualRCV.g:7366:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getMiscellaneousRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getMiscellaneousAccess().getRelatedProjectsGrantCrossReference_19_2_0());
                    					
                    pushFollow(FOLLOW_62);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:7380:4: (otherlv_31= ',' ( ( ruleEString ) ) )*
                    loop168:
                    do {
                        int alt168=2;
                        int LA168_0 = input.LA(1);

                        if ( (LA168_0==24) ) {
                            alt168=1;
                        }


                        switch (alt168) {
                    	case 1 :
                    	    // InternalTextualRCV.g:7381:5: otherlv_31= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_31=(Token)match(input,24,FOLLOW_20); 

                    	    					newLeafNode(otherlv_31, grammarAccess.getMiscellaneousAccess().getCommaKeyword_19_3_0());
                    	    				
                    	    // InternalTextualRCV.g:7385:5: ( ( ruleEString ) )
                    	    // InternalTextualRCV.g:7386:6: ( ruleEString )
                    	    {
                    	    // InternalTextualRCV.g:7386:6: ( ruleEString )
                    	    // InternalTextualRCV.g:7387:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getMiscellaneousRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getMiscellaneousAccess().getRelatedProjectsGrantCrossReference_19_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_62);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop168;
                        }
                    } while (true);

                    otherlv_33=(Token)match(input,53,FOLLOW_104); 

                    				newLeafNode(otherlv_33, grammarAccess.getMiscellaneousAccess().getRightParenthesisKeyword_19_4());
                    			

                    }
                    break;

            }

            otherlv_34=(Token)match(input,88,FOLLOW_9); 

            			newLeafNode(otherlv_34, grammarAccess.getMiscellaneousAccess().getDateKeyword_20());
            		
            // InternalTextualRCV.g:7411:3: ( (lv_date_35_0= rulePublishedDate ) )
            // InternalTextualRCV.g:7412:4: (lv_date_35_0= rulePublishedDate )
            {
            // InternalTextualRCV.g:7412:4: (lv_date_35_0= rulePublishedDate )
            // InternalTextualRCV.g:7413:5: lv_date_35_0= rulePublishedDate
            {

            					newCompositeNode(grammarAccess.getMiscellaneousAccess().getDatePublishedDateParserRuleCall_21_0());
            				
            pushFollow(FOLLOW_27);
            lv_date_35_0=rulePublishedDate();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getMiscellaneousRule());
            					}
            					set(
            						current,
            						"date",
            						lv_date_35_0,
            						"org.montex.researchcv.xtext.TextualRCV.PublishedDate");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_36=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_36, grammarAccess.getMiscellaneousAccess().getRightCurlyBracketKeyword_22());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMiscellaneous"


    // $ANTLR start "entryRuleEInt"
    // InternalTextualRCV.g:7438:1: entryRuleEInt returns [String current=null] : iv_ruleEInt= ruleEInt EOF ;
    public final String entryRuleEInt() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEInt = null;


        try {
            // InternalTextualRCV.g:7438:44: (iv_ruleEInt= ruleEInt EOF )
            // InternalTextualRCV.g:7439:2: iv_ruleEInt= ruleEInt EOF
            {
             newCompositeNode(grammarAccess.getEIntRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEInt=ruleEInt();

            state._fsp--;

             current =iv_ruleEInt.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEInt"


    // $ANTLR start "ruleEInt"
    // InternalTextualRCV.g:7445:1: ruleEInt returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= '-' )? this_INT_1= RULE_INT ) ;
    public final AntlrDatatypeRuleToken ruleEInt() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_INT_1=null;


        	enterRule();

        try {
            // InternalTextualRCV.g:7451:2: ( ( (kw= '-' )? this_INT_1= RULE_INT ) )
            // InternalTextualRCV.g:7452:2: ( (kw= '-' )? this_INT_1= RULE_INT )
            {
            // InternalTextualRCV.g:7452:2: ( (kw= '-' )? this_INT_1= RULE_INT )
            // InternalTextualRCV.g:7453:3: (kw= '-' )? this_INT_1= RULE_INT
            {
            // InternalTextualRCV.g:7453:3: (kw= '-' )?
            int alt170=2;
            int LA170_0 = input.LA(1);

            if ( (LA170_0==33) ) {
                alt170=1;
            }
            switch (alt170) {
                case 1 :
                    // InternalTextualRCV.g:7454:4: kw= '-'
                    {
                    kw=(Token)match(input,33,FOLLOW_68); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getEIntAccess().getHyphenMinusKeyword_0());
                    			

                    }
                    break;

            }

            this_INT_1=(Token)match(input,RULE_INT,FOLLOW_2); 

            			current.merge(this_INT_1);
            		

            			newLeafNode(this_INT_1, grammarAccess.getEIntAccess().getINTTerminalRuleCall_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEInt"


    // $ANTLR start "entryRuleSerialNumber"
    // InternalTextualRCV.g:7471:1: entryRuleSerialNumber returns [EObject current=null] : iv_ruleSerialNumber= ruleSerialNumber EOF ;
    public final EObject entryRuleSerialNumber() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSerialNumber = null;


        try {
            // InternalTextualRCV.g:7471:53: (iv_ruleSerialNumber= ruleSerialNumber EOF )
            // InternalTextualRCV.g:7472:2: iv_ruleSerialNumber= ruleSerialNumber EOF
            {
             newCompositeNode(grammarAccess.getSerialNumberRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSerialNumber=ruleSerialNumber();

            state._fsp--;

             current =iv_ruleSerialNumber; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSerialNumber"


    // $ANTLR start "ruleSerialNumber"
    // InternalTextualRCV.g:7478:1: ruleSerialNumber returns [EObject current=null] : (otherlv_0= 'SerialNumber' otherlv_1= '{' otherlv_2= 'value' ( (lv_value_3_0= ruleEString ) ) otherlv_4= 'kind' ( (lv_kind_5_0= ruleSerialNumberKind ) ) otherlv_6= '}' ) ;
    public final EObject ruleSerialNumber() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        AntlrDatatypeRuleToken lv_value_3_0 = null;

        Enumerator lv_kind_5_0 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:7484:2: ( (otherlv_0= 'SerialNumber' otherlv_1= '{' otherlv_2= 'value' ( (lv_value_3_0= ruleEString ) ) otherlv_4= 'kind' ( (lv_kind_5_0= ruleSerialNumberKind ) ) otherlv_6= '}' ) )
            // InternalTextualRCV.g:7485:2: (otherlv_0= 'SerialNumber' otherlv_1= '{' otherlv_2= 'value' ( (lv_value_3_0= ruleEString ) ) otherlv_4= 'kind' ( (lv_kind_5_0= ruleSerialNumberKind ) ) otherlv_6= '}' )
            {
            // InternalTextualRCV.g:7485:2: (otherlv_0= 'SerialNumber' otherlv_1= '{' otherlv_2= 'value' ( (lv_value_3_0= ruleEString ) ) otherlv_4= 'kind' ( (lv_kind_5_0= ruleSerialNumberKind ) ) otherlv_6= '}' )
            // InternalTextualRCV.g:7486:3: otherlv_0= 'SerialNumber' otherlv_1= '{' otherlv_2= 'value' ( (lv_value_3_0= ruleEString ) ) otherlv_4= 'kind' ( (lv_kind_5_0= ruleSerialNumberKind ) ) otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,114,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getSerialNumberAccess().getSerialNumberKeyword_0());
            		
            otherlv_1=(Token)match(input,16,FOLLOW_151); 

            			newLeafNode(otherlv_1, grammarAccess.getSerialNumberAccess().getLeftCurlyBracketKeyword_1());
            		
            otherlv_2=(Token)match(input,115,FOLLOW_20); 

            			newLeafNode(otherlv_2, grammarAccess.getSerialNumberAccess().getValueKeyword_2());
            		
            // InternalTextualRCV.g:7498:3: ( (lv_value_3_0= ruleEString ) )
            // InternalTextualRCV.g:7499:4: (lv_value_3_0= ruleEString )
            {
            // InternalTextualRCV.g:7499:4: (lv_value_3_0= ruleEString )
            // InternalTextualRCV.g:7500:5: lv_value_3_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getSerialNumberAccess().getValueEStringParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_152);
            lv_value_3_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSerialNumberRule());
            					}
            					set(
            						current,
            						"value",
            						lv_value_3_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,116,FOLLOW_153); 

            			newLeafNode(otherlv_4, grammarAccess.getSerialNumberAccess().getKindKeyword_4());
            		
            // InternalTextualRCV.g:7521:3: ( (lv_kind_5_0= ruleSerialNumberKind ) )
            // InternalTextualRCV.g:7522:4: (lv_kind_5_0= ruleSerialNumberKind )
            {
            // InternalTextualRCV.g:7522:4: (lv_kind_5_0= ruleSerialNumberKind )
            // InternalTextualRCV.g:7523:5: lv_kind_5_0= ruleSerialNumberKind
            {

            					newCompositeNode(grammarAccess.getSerialNumberAccess().getKindSerialNumberKindEnumRuleCall_5_0());
            				
            pushFollow(FOLLOW_27);
            lv_kind_5_0=ruleSerialNumberKind();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSerialNumberRule());
            					}
            					set(
            						current,
            						"kind",
            						lv_kind_5_0,
            						"org.montex.researchcv.xtext.TextualRCV.SerialNumberKind");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_6=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getSerialNumberAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSerialNumber"


    // $ANTLR start "entryRuleBudget"
    // InternalTextualRCV.g:7548:1: entryRuleBudget returns [EObject current=null] : iv_ruleBudget= ruleBudget EOF ;
    public final EObject entryRuleBudget() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBudget = null;


        try {
            // InternalTextualRCV.g:7548:47: (iv_ruleBudget= ruleBudget EOF )
            // InternalTextualRCV.g:7549:2: iv_ruleBudget= ruleBudget EOF
            {
             newCompositeNode(grammarAccess.getBudgetRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBudget=ruleBudget();

            state._fsp--;

             current =iv_ruleBudget; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBudget"


    // $ANTLR start "ruleBudget"
    // InternalTextualRCV.g:7555:1: ruleBudget returns [EObject current=null] : (otherlv_0= 'Budget' otherlv_1= '{' otherlv_2= 'amount' ( (lv_amount_3_0= ruleEDouble ) ) otherlv_4= 'currency' ( (lv_currency_5_0= ruleCurrency ) ) otherlv_6= '}' ) ;
    public final EObject ruleBudget() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        AntlrDatatypeRuleToken lv_amount_3_0 = null;

        AntlrDatatypeRuleToken lv_currency_5_0 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:7561:2: ( (otherlv_0= 'Budget' otherlv_1= '{' otherlv_2= 'amount' ( (lv_amount_3_0= ruleEDouble ) ) otherlv_4= 'currency' ( (lv_currency_5_0= ruleCurrency ) ) otherlv_6= '}' ) )
            // InternalTextualRCV.g:7562:2: (otherlv_0= 'Budget' otherlv_1= '{' otherlv_2= 'amount' ( (lv_amount_3_0= ruleEDouble ) ) otherlv_4= 'currency' ( (lv_currency_5_0= ruleCurrency ) ) otherlv_6= '}' )
            {
            // InternalTextualRCV.g:7562:2: (otherlv_0= 'Budget' otherlv_1= '{' otherlv_2= 'amount' ( (lv_amount_3_0= ruleEDouble ) ) otherlv_4= 'currency' ( (lv_currency_5_0= ruleCurrency ) ) otherlv_6= '}' )
            // InternalTextualRCV.g:7563:3: otherlv_0= 'Budget' otherlv_1= '{' otherlv_2= 'amount' ( (lv_amount_3_0= ruleEDouble ) ) otherlv_4= 'currency' ( (lv_currency_5_0= ruleCurrency ) ) otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,117,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getBudgetAccess().getBudgetKeyword_0());
            		
            otherlv_1=(Token)match(input,16,FOLLOW_154); 

            			newLeafNode(otherlv_1, grammarAccess.getBudgetAccess().getLeftCurlyBracketKeyword_1());
            		
            otherlv_2=(Token)match(input,118,FOLLOW_74); 

            			newLeafNode(otherlv_2, grammarAccess.getBudgetAccess().getAmountKeyword_2());
            		
            // InternalTextualRCV.g:7575:3: ( (lv_amount_3_0= ruleEDouble ) )
            // InternalTextualRCV.g:7576:4: (lv_amount_3_0= ruleEDouble )
            {
            // InternalTextualRCV.g:7576:4: (lv_amount_3_0= ruleEDouble )
            // InternalTextualRCV.g:7577:5: lv_amount_3_0= ruleEDouble
            {

            					newCompositeNode(grammarAccess.getBudgetAccess().getAmountEDoubleParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_155);
            lv_amount_3_0=ruleEDouble();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getBudgetRule());
            					}
            					set(
            						current,
            						"amount",
            						lv_amount_3_0,
            						"org.montex.researchcv.xtext.TextualRCV.EDouble");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,119,FOLLOW_20); 

            			newLeafNode(otherlv_4, grammarAccess.getBudgetAccess().getCurrencyKeyword_4());
            		
            // InternalTextualRCV.g:7598:3: ( (lv_currency_5_0= ruleCurrency ) )
            // InternalTextualRCV.g:7599:4: (lv_currency_5_0= ruleCurrency )
            {
            // InternalTextualRCV.g:7599:4: (lv_currency_5_0= ruleCurrency )
            // InternalTextualRCV.g:7600:5: lv_currency_5_0= ruleCurrency
            {

            					newCompositeNode(grammarAccess.getBudgetAccess().getCurrencyCurrencyParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_27);
            lv_currency_5_0=ruleCurrency();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getBudgetRule());
            					}
            					set(
            						current,
            						"currency",
            						lv_currency_5_0,
            						"org.montex.researchcv.xtext.TextualRCV.Currency");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_6=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getBudgetAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBudget"


    // $ANTLR start "entryRuleProject"
    // InternalTextualRCV.g:7625:1: entryRuleProject returns [EObject current=null] : iv_ruleProject= ruleProject EOF ;
    public final EObject entryRuleProject() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProject = null;


        try {
            // InternalTextualRCV.g:7625:48: (iv_ruleProject= ruleProject EOF )
            // InternalTextualRCV.g:7626:2: iv_ruleProject= ruleProject EOF
            {
             newCompositeNode(grammarAccess.getProjectRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleProject=ruleProject();

            state._fsp--;

             current =iv_ruleProject; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProject"


    // $ANTLR start "ruleProject"
    // InternalTextualRCV.g:7632:1: ruleProject returns [EObject current=null] : (otherlv_0= 'Project' otherlv_1= '{' (otherlv_2= 'URL' otherlv_3= '{' ( (lv_URL_4_0= ruleEString ) ) (otherlv_5= ',' ( (lv_URL_6_0= ruleEString ) ) )* otherlv_7= '}' )? otherlv_8= 'agency' ( (lv_agency_9_0= ruleEString ) ) (otherlv_10= 'program' ( (lv_program_11_0= ruleEString ) ) )? (otherlv_12= 'call' ( (lv_call_13_0= ruleEString ) ) )? otherlv_14= 'number' ( (lv_number_15_0= ruleEString ) ) (otherlv_16= 'acronym' ( (lv_acronym_17_0= ruleEString ) ) )? (otherlv_18= 'title' ( (lv_title_19_0= ruleEString ) ) )? (otherlv_20= 'relatedPublications' otherlv_21= '(' ( ( ruleEString ) ) (otherlv_23= ',' ( ( ruleEString ) ) )* otherlv_25= ')' )? (otherlv_26= 'budget' otherlv_27= '{' ( (lv_budget_28_0= ruleBudget ) ) (otherlv_29= ',' ( (lv_budget_30_0= ruleBudget ) ) )* otherlv_31= '}' )? otherlv_32= 'startDate' ( (lv_startDate_33_0= rulePublishedDate ) ) otherlv_34= 'endDate' ( (lv_endDate_35_0= rulePublishedDate ) ) otherlv_36= '}' ) ;
    public final EObject ruleProject() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_18=null;
        Token otherlv_20=null;
        Token otherlv_21=null;
        Token otherlv_23=null;
        Token otherlv_25=null;
        Token otherlv_26=null;
        Token otherlv_27=null;
        Token otherlv_29=null;
        Token otherlv_31=null;
        Token otherlv_32=null;
        Token otherlv_34=null;
        Token otherlv_36=null;
        AntlrDatatypeRuleToken lv_URL_4_0 = null;

        AntlrDatatypeRuleToken lv_URL_6_0 = null;

        AntlrDatatypeRuleToken lv_agency_9_0 = null;

        AntlrDatatypeRuleToken lv_program_11_0 = null;

        AntlrDatatypeRuleToken lv_call_13_0 = null;

        AntlrDatatypeRuleToken lv_number_15_0 = null;

        AntlrDatatypeRuleToken lv_acronym_17_0 = null;

        AntlrDatatypeRuleToken lv_title_19_0 = null;

        EObject lv_budget_28_0 = null;

        EObject lv_budget_30_0 = null;

        EObject lv_startDate_33_0 = null;

        EObject lv_endDate_35_0 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:7638:2: ( (otherlv_0= 'Project' otherlv_1= '{' (otherlv_2= 'URL' otherlv_3= '{' ( (lv_URL_4_0= ruleEString ) ) (otherlv_5= ',' ( (lv_URL_6_0= ruleEString ) ) )* otherlv_7= '}' )? otherlv_8= 'agency' ( (lv_agency_9_0= ruleEString ) ) (otherlv_10= 'program' ( (lv_program_11_0= ruleEString ) ) )? (otherlv_12= 'call' ( (lv_call_13_0= ruleEString ) ) )? otherlv_14= 'number' ( (lv_number_15_0= ruleEString ) ) (otherlv_16= 'acronym' ( (lv_acronym_17_0= ruleEString ) ) )? (otherlv_18= 'title' ( (lv_title_19_0= ruleEString ) ) )? (otherlv_20= 'relatedPublications' otherlv_21= '(' ( ( ruleEString ) ) (otherlv_23= ',' ( ( ruleEString ) ) )* otherlv_25= ')' )? (otherlv_26= 'budget' otherlv_27= '{' ( (lv_budget_28_0= ruleBudget ) ) (otherlv_29= ',' ( (lv_budget_30_0= ruleBudget ) ) )* otherlv_31= '}' )? otherlv_32= 'startDate' ( (lv_startDate_33_0= rulePublishedDate ) ) otherlv_34= 'endDate' ( (lv_endDate_35_0= rulePublishedDate ) ) otherlv_36= '}' ) )
            // InternalTextualRCV.g:7639:2: (otherlv_0= 'Project' otherlv_1= '{' (otherlv_2= 'URL' otherlv_3= '{' ( (lv_URL_4_0= ruleEString ) ) (otherlv_5= ',' ( (lv_URL_6_0= ruleEString ) ) )* otherlv_7= '}' )? otherlv_8= 'agency' ( (lv_agency_9_0= ruleEString ) ) (otherlv_10= 'program' ( (lv_program_11_0= ruleEString ) ) )? (otherlv_12= 'call' ( (lv_call_13_0= ruleEString ) ) )? otherlv_14= 'number' ( (lv_number_15_0= ruleEString ) ) (otherlv_16= 'acronym' ( (lv_acronym_17_0= ruleEString ) ) )? (otherlv_18= 'title' ( (lv_title_19_0= ruleEString ) ) )? (otherlv_20= 'relatedPublications' otherlv_21= '(' ( ( ruleEString ) ) (otherlv_23= ',' ( ( ruleEString ) ) )* otherlv_25= ')' )? (otherlv_26= 'budget' otherlv_27= '{' ( (lv_budget_28_0= ruleBudget ) ) (otherlv_29= ',' ( (lv_budget_30_0= ruleBudget ) ) )* otherlv_31= '}' )? otherlv_32= 'startDate' ( (lv_startDate_33_0= rulePublishedDate ) ) otherlv_34= 'endDate' ( (lv_endDate_35_0= rulePublishedDate ) ) otherlv_36= '}' )
            {
            // InternalTextualRCV.g:7639:2: (otherlv_0= 'Project' otherlv_1= '{' (otherlv_2= 'URL' otherlv_3= '{' ( (lv_URL_4_0= ruleEString ) ) (otherlv_5= ',' ( (lv_URL_6_0= ruleEString ) ) )* otherlv_7= '}' )? otherlv_8= 'agency' ( (lv_agency_9_0= ruleEString ) ) (otherlv_10= 'program' ( (lv_program_11_0= ruleEString ) ) )? (otherlv_12= 'call' ( (lv_call_13_0= ruleEString ) ) )? otherlv_14= 'number' ( (lv_number_15_0= ruleEString ) ) (otherlv_16= 'acronym' ( (lv_acronym_17_0= ruleEString ) ) )? (otherlv_18= 'title' ( (lv_title_19_0= ruleEString ) ) )? (otherlv_20= 'relatedPublications' otherlv_21= '(' ( ( ruleEString ) ) (otherlv_23= ',' ( ( ruleEString ) ) )* otherlv_25= ')' )? (otherlv_26= 'budget' otherlv_27= '{' ( (lv_budget_28_0= ruleBudget ) ) (otherlv_29= ',' ( (lv_budget_30_0= ruleBudget ) ) )* otherlv_31= '}' )? otherlv_32= 'startDate' ( (lv_startDate_33_0= rulePublishedDate ) ) otherlv_34= 'endDate' ( (lv_endDate_35_0= rulePublishedDate ) ) otherlv_36= '}' )
            // InternalTextualRCV.g:7640:3: otherlv_0= 'Project' otherlv_1= '{' (otherlv_2= 'URL' otherlv_3= '{' ( (lv_URL_4_0= ruleEString ) ) (otherlv_5= ',' ( (lv_URL_6_0= ruleEString ) ) )* otherlv_7= '}' )? otherlv_8= 'agency' ( (lv_agency_9_0= ruleEString ) ) (otherlv_10= 'program' ( (lv_program_11_0= ruleEString ) ) )? (otherlv_12= 'call' ( (lv_call_13_0= ruleEString ) ) )? otherlv_14= 'number' ( (lv_number_15_0= ruleEString ) ) (otherlv_16= 'acronym' ( (lv_acronym_17_0= ruleEString ) ) )? (otherlv_18= 'title' ( (lv_title_19_0= ruleEString ) ) )? (otherlv_20= 'relatedPublications' otherlv_21= '(' ( ( ruleEString ) ) (otherlv_23= ',' ( ( ruleEString ) ) )* otherlv_25= ')' )? (otherlv_26= 'budget' otherlv_27= '{' ( (lv_budget_28_0= ruleBudget ) ) (otherlv_29= ',' ( (lv_budget_30_0= ruleBudget ) ) )* otherlv_31= '}' )? otherlv_32= 'startDate' ( (lv_startDate_33_0= rulePublishedDate ) ) otherlv_34= 'endDate' ( (lv_endDate_35_0= rulePublishedDate ) ) otherlv_36= '}'
            {
            otherlv_0=(Token)match(input,120,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getProjectAccess().getProjectKeyword_0());
            		
            otherlv_1=(Token)match(input,16,FOLLOW_156); 

            			newLeafNode(otherlv_1, grammarAccess.getProjectAccess().getLeftCurlyBracketKeyword_1());
            		
            // InternalTextualRCV.g:7648:3: (otherlv_2= 'URL' otherlv_3= '{' ( (lv_URL_4_0= ruleEString ) ) (otherlv_5= ',' ( (lv_URL_6_0= ruleEString ) ) )* otherlv_7= '}' )?
            int alt172=2;
            int LA172_0 = input.LA(1);

            if ( (LA172_0==47) ) {
                alt172=1;
            }
            switch (alt172) {
                case 1 :
                    // InternalTextualRCV.g:7649:4: otherlv_2= 'URL' otherlv_3= '{' ( (lv_URL_4_0= ruleEString ) ) (otherlv_5= ',' ( (lv_URL_6_0= ruleEString ) ) )* otherlv_7= '}'
                    {
                    otherlv_2=(Token)match(input,47,FOLLOW_3); 

                    				newLeafNode(otherlv_2, grammarAccess.getProjectAccess().getURLKeyword_2_0());
                    			
                    otherlv_3=(Token)match(input,16,FOLLOW_20); 

                    				newLeafNode(otherlv_3, grammarAccess.getProjectAccess().getLeftCurlyBracketKeyword_2_1());
                    			
                    // InternalTextualRCV.g:7657:4: ( (lv_URL_4_0= ruleEString ) )
                    // InternalTextualRCV.g:7658:5: (lv_URL_4_0= ruleEString )
                    {
                    // InternalTextualRCV.g:7658:5: (lv_URL_4_0= ruleEString )
                    // InternalTextualRCV.g:7659:6: lv_URL_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getProjectAccess().getURLEStringParserRuleCall_2_2_0());
                    					
                    pushFollow(FOLLOW_40);
                    lv_URL_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getProjectRule());
                    						}
                    						add(
                    							current,
                    							"URL",
                    							lv_URL_4_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:7676:4: (otherlv_5= ',' ( (lv_URL_6_0= ruleEString ) ) )*
                    loop171:
                    do {
                        int alt171=2;
                        int LA171_0 = input.LA(1);

                        if ( (LA171_0==24) ) {
                            alt171=1;
                        }


                        switch (alt171) {
                    	case 1 :
                    	    // InternalTextualRCV.g:7677:5: otherlv_5= ',' ( (lv_URL_6_0= ruleEString ) )
                    	    {
                    	    otherlv_5=(Token)match(input,24,FOLLOW_20); 

                    	    					newLeafNode(otherlv_5, grammarAccess.getProjectAccess().getCommaKeyword_2_3_0());
                    	    				
                    	    // InternalTextualRCV.g:7681:5: ( (lv_URL_6_0= ruleEString ) )
                    	    // InternalTextualRCV.g:7682:6: (lv_URL_6_0= ruleEString )
                    	    {
                    	    // InternalTextualRCV.g:7682:6: (lv_URL_6_0= ruleEString )
                    	    // InternalTextualRCV.g:7683:7: lv_URL_6_0= ruleEString
                    	    {

                    	    							newCompositeNode(grammarAccess.getProjectAccess().getURLEStringParserRuleCall_2_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_40);
                    	    lv_URL_6_0=ruleEString();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getProjectRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"URL",
                    	    								lv_URL_6_0,
                    	    								"org.montex.researchcv.xtext.TextualRCV.EString");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop171;
                        }
                    } while (true);

                    otherlv_7=(Token)match(input,18,FOLLOW_157); 

                    				newLeafNode(otherlv_7, grammarAccess.getProjectAccess().getRightCurlyBracketKeyword_2_4());
                    			

                    }
                    break;

            }

            otherlv_8=(Token)match(input,121,FOLLOW_20); 

            			newLeafNode(otherlv_8, grammarAccess.getProjectAccess().getAgencyKeyword_3());
            		
            // InternalTextualRCV.g:7710:3: ( (lv_agency_9_0= ruleEString ) )
            // InternalTextualRCV.g:7711:4: (lv_agency_9_0= ruleEString )
            {
            // InternalTextualRCV.g:7711:4: (lv_agency_9_0= ruleEString )
            // InternalTextualRCV.g:7712:5: lv_agency_9_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getProjectAccess().getAgencyEStringParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_158);
            lv_agency_9_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getProjectRule());
            					}
            					set(
            						current,
            						"agency",
            						lv_agency_9_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:7729:3: (otherlv_10= 'program' ( (lv_program_11_0= ruleEString ) ) )?
            int alt173=2;
            int LA173_0 = input.LA(1);

            if ( (LA173_0==122) ) {
                alt173=1;
            }
            switch (alt173) {
                case 1 :
                    // InternalTextualRCV.g:7730:4: otherlv_10= 'program' ( (lv_program_11_0= ruleEString ) )
                    {
                    otherlv_10=(Token)match(input,122,FOLLOW_20); 

                    				newLeafNode(otherlv_10, grammarAccess.getProjectAccess().getProgramKeyword_5_0());
                    			
                    // InternalTextualRCV.g:7734:4: ( (lv_program_11_0= ruleEString ) )
                    // InternalTextualRCV.g:7735:5: (lv_program_11_0= ruleEString )
                    {
                    // InternalTextualRCV.g:7735:5: (lv_program_11_0= ruleEString )
                    // InternalTextualRCV.g:7736:6: lv_program_11_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getProjectAccess().getProgramEStringParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_159);
                    lv_program_11_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getProjectRule());
                    						}
                    						set(
                    							current,
                    							"program",
                    							lv_program_11_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:7754:3: (otherlv_12= 'call' ( (lv_call_13_0= ruleEString ) ) )?
            int alt174=2;
            int LA174_0 = input.LA(1);

            if ( (LA174_0==123) ) {
                alt174=1;
            }
            switch (alt174) {
                case 1 :
                    // InternalTextualRCV.g:7755:4: otherlv_12= 'call' ( (lv_call_13_0= ruleEString ) )
                    {
                    otherlv_12=(Token)match(input,123,FOLLOW_20); 

                    				newLeafNode(otherlv_12, grammarAccess.getProjectAccess().getCallKeyword_6_0());
                    			
                    // InternalTextualRCV.g:7759:4: ( (lv_call_13_0= ruleEString ) )
                    // InternalTextualRCV.g:7760:5: (lv_call_13_0= ruleEString )
                    {
                    // InternalTextualRCV.g:7760:5: (lv_call_13_0= ruleEString )
                    // InternalTextualRCV.g:7761:6: lv_call_13_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getProjectAccess().getCallEStringParserRuleCall_6_1_0());
                    					
                    pushFollow(FOLLOW_160);
                    lv_call_13_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getProjectRule());
                    						}
                    						set(
                    							current,
                    							"call",
                    							lv_call_13_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_14=(Token)match(input,124,FOLLOW_20); 

            			newLeafNode(otherlv_14, grammarAccess.getProjectAccess().getNumberKeyword_7());
            		
            // InternalTextualRCV.g:7783:3: ( (lv_number_15_0= ruleEString ) )
            // InternalTextualRCV.g:7784:4: (lv_number_15_0= ruleEString )
            {
            // InternalTextualRCV.g:7784:4: (lv_number_15_0= ruleEString )
            // InternalTextualRCV.g:7785:5: lv_number_15_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getProjectAccess().getNumberEStringParserRuleCall_8_0());
            				
            pushFollow(FOLLOW_161);
            lv_number_15_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getProjectRule());
            					}
            					set(
            						current,
            						"number",
            						lv_number_15_0,
            						"org.montex.researchcv.xtext.TextualRCV.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalTextualRCV.g:7802:3: (otherlv_16= 'acronym' ( (lv_acronym_17_0= ruleEString ) ) )?
            int alt175=2;
            int LA175_0 = input.LA(1);

            if ( (LA175_0==46) ) {
                alt175=1;
            }
            switch (alt175) {
                case 1 :
                    // InternalTextualRCV.g:7803:4: otherlv_16= 'acronym' ( (lv_acronym_17_0= ruleEString ) )
                    {
                    otherlv_16=(Token)match(input,46,FOLLOW_20); 

                    				newLeafNode(otherlv_16, grammarAccess.getProjectAccess().getAcronymKeyword_9_0());
                    			
                    // InternalTextualRCV.g:7807:4: ( (lv_acronym_17_0= ruleEString ) )
                    // InternalTextualRCV.g:7808:5: (lv_acronym_17_0= ruleEString )
                    {
                    // InternalTextualRCV.g:7808:5: (lv_acronym_17_0= ruleEString )
                    // InternalTextualRCV.g:7809:6: lv_acronym_17_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getProjectAccess().getAcronymEStringParserRuleCall_9_1_0());
                    					
                    pushFollow(FOLLOW_162);
                    lv_acronym_17_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getProjectRule());
                    						}
                    						set(
                    							current,
                    							"acronym",
                    							lv_acronym_17_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:7827:3: (otherlv_18= 'title' ( (lv_title_19_0= ruleEString ) ) )?
            int alt176=2;
            int LA176_0 = input.LA(1);

            if ( (LA176_0==49) ) {
                alt176=1;
            }
            switch (alt176) {
                case 1 :
                    // InternalTextualRCV.g:7828:4: otherlv_18= 'title' ( (lv_title_19_0= ruleEString ) )
                    {
                    otherlv_18=(Token)match(input,49,FOLLOW_20); 

                    				newLeafNode(otherlv_18, grammarAccess.getProjectAccess().getTitleKeyword_10_0());
                    			
                    // InternalTextualRCV.g:7832:4: ( (lv_title_19_0= ruleEString ) )
                    // InternalTextualRCV.g:7833:5: (lv_title_19_0= ruleEString )
                    {
                    // InternalTextualRCV.g:7833:5: (lv_title_19_0= ruleEString )
                    // InternalTextualRCV.g:7834:6: lv_title_19_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getProjectAccess().getTitleEStringParserRuleCall_10_1_0());
                    					
                    pushFollow(FOLLOW_163);
                    lv_title_19_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getProjectRule());
                    						}
                    						set(
                    							current,
                    							"title",
                    							lv_title_19_0,
                    							"org.montex.researchcv.xtext.TextualRCV.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalTextualRCV.g:7852:3: (otherlv_20= 'relatedPublications' otherlv_21= '(' ( ( ruleEString ) ) (otherlv_23= ',' ( ( ruleEString ) ) )* otherlv_25= ')' )?
            int alt178=2;
            int LA178_0 = input.LA(1);

            if ( (LA178_0==125) ) {
                alt178=1;
            }
            switch (alt178) {
                case 1 :
                    // InternalTextualRCV.g:7853:4: otherlv_20= 'relatedPublications' otherlv_21= '(' ( ( ruleEString ) ) (otherlv_23= ',' ( ( ruleEString ) ) )* otherlv_25= ')'
                    {
                    otherlv_20=(Token)match(input,125,FOLLOW_61); 

                    				newLeafNode(otherlv_20, grammarAccess.getProjectAccess().getRelatedPublicationsKeyword_11_0());
                    			
                    otherlv_21=(Token)match(input,52,FOLLOW_20); 

                    				newLeafNode(otherlv_21, grammarAccess.getProjectAccess().getLeftParenthesisKeyword_11_1());
                    			
                    // InternalTextualRCV.g:7861:4: ( ( ruleEString ) )
                    // InternalTextualRCV.g:7862:5: ( ruleEString )
                    {
                    // InternalTextualRCV.g:7862:5: ( ruleEString )
                    // InternalTextualRCV.g:7863:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getProjectRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getProjectAccess().getRelatedPublicationsPublicationCrossReference_11_2_0());
                    					
                    pushFollow(FOLLOW_62);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:7877:4: (otherlv_23= ',' ( ( ruleEString ) ) )*
                    loop177:
                    do {
                        int alt177=2;
                        int LA177_0 = input.LA(1);

                        if ( (LA177_0==24) ) {
                            alt177=1;
                        }


                        switch (alt177) {
                    	case 1 :
                    	    // InternalTextualRCV.g:7878:5: otherlv_23= ',' ( ( ruleEString ) )
                    	    {
                    	    otherlv_23=(Token)match(input,24,FOLLOW_20); 

                    	    					newLeafNode(otherlv_23, grammarAccess.getProjectAccess().getCommaKeyword_11_3_0());
                    	    				
                    	    // InternalTextualRCV.g:7882:5: ( ( ruleEString ) )
                    	    // InternalTextualRCV.g:7883:6: ( ruleEString )
                    	    {
                    	    // InternalTextualRCV.g:7883:6: ( ruleEString )
                    	    // InternalTextualRCV.g:7884:7: ruleEString
                    	    {

                    	    							if (current==null) {
                    	    								current = createModelElement(grammarAccess.getProjectRule());
                    	    							}
                    	    						

                    	    							newCompositeNode(grammarAccess.getProjectAccess().getRelatedPublicationsPublicationCrossReference_11_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_62);
                    	    ruleEString();

                    	    state._fsp--;


                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop177;
                        }
                    } while (true);

                    otherlv_25=(Token)match(input,53,FOLLOW_164); 

                    				newLeafNode(otherlv_25, grammarAccess.getProjectAccess().getRightParenthesisKeyword_11_4());
                    			

                    }
                    break;

            }

            // InternalTextualRCV.g:7904:3: (otherlv_26= 'budget' otherlv_27= '{' ( (lv_budget_28_0= ruleBudget ) ) (otherlv_29= ',' ( (lv_budget_30_0= ruleBudget ) ) )* otherlv_31= '}' )?
            int alt180=2;
            int LA180_0 = input.LA(1);

            if ( (LA180_0==126) ) {
                alt180=1;
            }
            switch (alt180) {
                case 1 :
                    // InternalTextualRCV.g:7905:4: otherlv_26= 'budget' otherlv_27= '{' ( (lv_budget_28_0= ruleBudget ) ) (otherlv_29= ',' ( (lv_budget_30_0= ruleBudget ) ) )* otherlv_31= '}'
                    {
                    otherlv_26=(Token)match(input,126,FOLLOW_3); 

                    				newLeafNode(otherlv_26, grammarAccess.getProjectAccess().getBudgetKeyword_12_0());
                    			
                    otherlv_27=(Token)match(input,16,FOLLOW_165); 

                    				newLeafNode(otherlv_27, grammarAccess.getProjectAccess().getLeftCurlyBracketKeyword_12_1());
                    			
                    // InternalTextualRCV.g:7913:4: ( (lv_budget_28_0= ruleBudget ) )
                    // InternalTextualRCV.g:7914:5: (lv_budget_28_0= ruleBudget )
                    {
                    // InternalTextualRCV.g:7914:5: (lv_budget_28_0= ruleBudget )
                    // InternalTextualRCV.g:7915:6: lv_budget_28_0= ruleBudget
                    {

                    						newCompositeNode(grammarAccess.getProjectAccess().getBudgetBudgetParserRuleCall_12_2_0());
                    					
                    pushFollow(FOLLOW_40);
                    lv_budget_28_0=ruleBudget();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getProjectRule());
                    						}
                    						add(
                    							current,
                    							"budget",
                    							lv_budget_28_0,
                    							"org.montex.researchcv.xtext.TextualRCV.Budget");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalTextualRCV.g:7932:4: (otherlv_29= ',' ( (lv_budget_30_0= ruleBudget ) ) )*
                    loop179:
                    do {
                        int alt179=2;
                        int LA179_0 = input.LA(1);

                        if ( (LA179_0==24) ) {
                            alt179=1;
                        }


                        switch (alt179) {
                    	case 1 :
                    	    // InternalTextualRCV.g:7933:5: otherlv_29= ',' ( (lv_budget_30_0= ruleBudget ) )
                    	    {
                    	    otherlv_29=(Token)match(input,24,FOLLOW_165); 

                    	    					newLeafNode(otherlv_29, grammarAccess.getProjectAccess().getCommaKeyword_12_3_0());
                    	    				
                    	    // InternalTextualRCV.g:7937:5: ( (lv_budget_30_0= ruleBudget ) )
                    	    // InternalTextualRCV.g:7938:6: (lv_budget_30_0= ruleBudget )
                    	    {
                    	    // InternalTextualRCV.g:7938:6: (lv_budget_30_0= ruleBudget )
                    	    // InternalTextualRCV.g:7939:7: lv_budget_30_0= ruleBudget
                    	    {

                    	    							newCompositeNode(grammarAccess.getProjectAccess().getBudgetBudgetParserRuleCall_12_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_40);
                    	    lv_budget_30_0=ruleBudget();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getProjectRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"budget",
                    	    								lv_budget_30_0,
                    	    								"org.montex.researchcv.xtext.TextualRCV.Budget");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop179;
                        }
                    } while (true);

                    otherlv_31=(Token)match(input,18,FOLLOW_166); 

                    				newLeafNode(otherlv_31, grammarAccess.getProjectAccess().getRightCurlyBracketKeyword_12_4());
                    			

                    }
                    break;

            }

            otherlv_32=(Token)match(input,127,FOLLOW_9); 

            			newLeafNode(otherlv_32, grammarAccess.getProjectAccess().getStartDateKeyword_13());
            		
            // InternalTextualRCV.g:7966:3: ( (lv_startDate_33_0= rulePublishedDate ) )
            // InternalTextualRCV.g:7967:4: (lv_startDate_33_0= rulePublishedDate )
            {
            // InternalTextualRCV.g:7967:4: (lv_startDate_33_0= rulePublishedDate )
            // InternalTextualRCV.g:7968:5: lv_startDate_33_0= rulePublishedDate
            {

            					newCompositeNode(grammarAccess.getProjectAccess().getStartDatePublishedDateParserRuleCall_14_0());
            				
            pushFollow(FOLLOW_167);
            lv_startDate_33_0=rulePublishedDate();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getProjectRule());
            					}
            					set(
            						current,
            						"startDate",
            						lv_startDate_33_0,
            						"org.montex.researchcv.xtext.TextualRCV.PublishedDate");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_34=(Token)match(input,128,FOLLOW_9); 

            			newLeafNode(otherlv_34, grammarAccess.getProjectAccess().getEndDateKeyword_15());
            		
            // InternalTextualRCV.g:7989:3: ( (lv_endDate_35_0= rulePublishedDate ) )
            // InternalTextualRCV.g:7990:4: (lv_endDate_35_0= rulePublishedDate )
            {
            // InternalTextualRCV.g:7990:4: (lv_endDate_35_0= rulePublishedDate )
            // InternalTextualRCV.g:7991:5: lv_endDate_35_0= rulePublishedDate
            {

            					newCompositeNode(grammarAccess.getProjectAccess().getEndDatePublishedDateParserRuleCall_16_0());
            				
            pushFollow(FOLLOW_27);
            lv_endDate_35_0=rulePublishedDate();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getProjectRule());
            					}
            					set(
            						current,
            						"endDate",
            						lv_endDate_35_0,
            						"org.montex.researchcv.xtext.TextualRCV.PublishedDate");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_36=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_36, grammarAccess.getProjectAccess().getRightCurlyBracketKeyword_17());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProject"


    // $ANTLR start "entryRuleEDouble"
    // InternalTextualRCV.g:8016:1: entryRuleEDouble returns [String current=null] : iv_ruleEDouble= ruleEDouble EOF ;
    public final String entryRuleEDouble() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEDouble = null;


        try {
            // InternalTextualRCV.g:8016:47: (iv_ruleEDouble= ruleEDouble EOF )
            // InternalTextualRCV.g:8017:2: iv_ruleEDouble= ruleEDouble EOF
            {
             newCompositeNode(grammarAccess.getEDoubleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEDouble=ruleEDouble();

            state._fsp--;

             current =iv_ruleEDouble.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEDouble"


    // $ANTLR start "ruleEDouble"
    // InternalTextualRCV.g:8023:1: ruleEDouble returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= '-' )? (this_INT_1= RULE_INT )? kw= '.' this_INT_3= RULE_INT ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT )? ) ;
    public final AntlrDatatypeRuleToken ruleEDouble() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_INT_1=null;
        Token this_INT_3=null;
        Token this_INT_7=null;


        	enterRule();

        try {
            // InternalTextualRCV.g:8029:2: ( ( (kw= '-' )? (this_INT_1= RULE_INT )? kw= '.' this_INT_3= RULE_INT ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT )? ) )
            // InternalTextualRCV.g:8030:2: ( (kw= '-' )? (this_INT_1= RULE_INT )? kw= '.' this_INT_3= RULE_INT ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT )? )
            {
            // InternalTextualRCV.g:8030:2: ( (kw= '-' )? (this_INT_1= RULE_INT )? kw= '.' this_INT_3= RULE_INT ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT )? )
            // InternalTextualRCV.g:8031:3: (kw= '-' )? (this_INT_1= RULE_INT )? kw= '.' this_INT_3= RULE_INT ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT )?
            {
            // InternalTextualRCV.g:8031:3: (kw= '-' )?
            int alt181=2;
            int LA181_0 = input.LA(1);

            if ( (LA181_0==33) ) {
                alt181=1;
            }
            switch (alt181) {
                case 1 :
                    // InternalTextualRCV.g:8032:4: kw= '-'
                    {
                    kw=(Token)match(input,33,FOLLOW_168); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getEDoubleAccess().getHyphenMinusKeyword_0());
                    			

                    }
                    break;

            }

            // InternalTextualRCV.g:8038:3: (this_INT_1= RULE_INT )?
            int alt182=2;
            int LA182_0 = input.LA(1);

            if ( (LA182_0==RULE_INT) ) {
                alt182=1;
            }
            switch (alt182) {
                case 1 :
                    // InternalTextualRCV.g:8039:4: this_INT_1= RULE_INT
                    {
                    this_INT_1=(Token)match(input,RULE_INT,FOLLOW_169); 

                    				current.merge(this_INT_1);
                    			

                    				newLeafNode(this_INT_1, grammarAccess.getEDoubleAccess().getINTTerminalRuleCall_1());
                    			

                    }
                    break;

            }

            kw=(Token)match(input,21,FOLLOW_68); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getEDoubleAccess().getFullStopKeyword_2());
            		
            this_INT_3=(Token)match(input,RULE_INT,FOLLOW_170); 

            			current.merge(this_INT_3);
            		

            			newLeafNode(this_INT_3, grammarAccess.getEDoubleAccess().getINTTerminalRuleCall_3());
            		
            // InternalTextualRCV.g:8059:3: ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT )?
            int alt185=2;
            int LA185_0 = input.LA(1);

            if ( ((LA185_0>=129 && LA185_0<=130)) ) {
                alt185=1;
            }
            switch (alt185) {
                case 1 :
                    // InternalTextualRCV.g:8060:4: (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT
                    {
                    // InternalTextualRCV.g:8060:4: (kw= 'E' | kw= 'e' )
                    int alt183=2;
                    int LA183_0 = input.LA(1);

                    if ( (LA183_0==129) ) {
                        alt183=1;
                    }
                    else if ( (LA183_0==130) ) {
                        alt183=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 183, 0, input);

                        throw nvae;
                    }
                    switch (alt183) {
                        case 1 :
                            // InternalTextualRCV.g:8061:5: kw= 'E'
                            {
                            kw=(Token)match(input,129,FOLLOW_9); 

                            					current.merge(kw);
                            					newLeafNode(kw, grammarAccess.getEDoubleAccess().getEKeyword_4_0_0());
                            				

                            }
                            break;
                        case 2 :
                            // InternalTextualRCV.g:8067:5: kw= 'e'
                            {
                            kw=(Token)match(input,130,FOLLOW_9); 

                            					current.merge(kw);
                            					newLeafNode(kw, grammarAccess.getEDoubleAccess().getEKeyword_4_0_1());
                            				

                            }
                            break;

                    }

                    // InternalTextualRCV.g:8073:4: (kw= '-' )?
                    int alt184=2;
                    int LA184_0 = input.LA(1);

                    if ( (LA184_0==33) ) {
                        alt184=1;
                    }
                    switch (alt184) {
                        case 1 :
                            // InternalTextualRCV.g:8074:5: kw= '-'
                            {
                            kw=(Token)match(input,33,FOLLOW_68); 

                            					current.merge(kw);
                            					newLeafNode(kw, grammarAccess.getEDoubleAccess().getHyphenMinusKeyword_4_1());
                            				

                            }
                            break;

                    }

                    this_INT_7=(Token)match(input,RULE_INT,FOLLOW_2); 

                    				current.merge(this_INT_7);
                    			

                    				newLeafNode(this_INT_7, grammarAccess.getEDoubleAccess().getINTTerminalRuleCall_4_2());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEDouble"


    // $ANTLR start "entryRuleECTS"
    // InternalTextualRCV.g:8092:1: entryRuleECTS returns [String current=null] : iv_ruleECTS= ruleECTS EOF ;
    public final String entryRuleECTS() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleECTS = null;


        try {
            // InternalTextualRCV.g:8092:44: (iv_ruleECTS= ruleECTS EOF )
            // InternalTextualRCV.g:8093:2: iv_ruleECTS= ruleECTS EOF
            {
             newCompositeNode(grammarAccess.getECTSRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleECTS=ruleECTS();

            state._fsp--;

             current =iv_ruleECTS.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleECTS"


    // $ANTLR start "ruleECTS"
    // InternalTextualRCV.g:8099:1: ruleECTS returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_INT_0= RULE_INT kw= '.' ( (this_DIGIT_2= RULE_DIGIT )? this_DIGIT_3= RULE_DIGIT )? ) ;
    public final AntlrDatatypeRuleToken ruleECTS() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_INT_0=null;
        Token kw=null;
        Token this_DIGIT_2=null;
        Token this_DIGIT_3=null;


        	enterRule();

        try {
            // InternalTextualRCV.g:8105:2: ( (this_INT_0= RULE_INT kw= '.' ( (this_DIGIT_2= RULE_DIGIT )? this_DIGIT_3= RULE_DIGIT )? ) )
            // InternalTextualRCV.g:8106:2: (this_INT_0= RULE_INT kw= '.' ( (this_DIGIT_2= RULE_DIGIT )? this_DIGIT_3= RULE_DIGIT )? )
            {
            // InternalTextualRCV.g:8106:2: (this_INT_0= RULE_INT kw= '.' ( (this_DIGIT_2= RULE_DIGIT )? this_DIGIT_3= RULE_DIGIT )? )
            // InternalTextualRCV.g:8107:3: this_INT_0= RULE_INT kw= '.' ( (this_DIGIT_2= RULE_DIGIT )? this_DIGIT_3= RULE_DIGIT )?
            {
            this_INT_0=(Token)match(input,RULE_INT,FOLLOW_169); 

            			current.merge(this_INT_0);
            		

            			newLeafNode(this_INT_0, grammarAccess.getECTSAccess().getINTTerminalRuleCall_0());
            		
            kw=(Token)match(input,21,FOLLOW_171); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getECTSAccess().getFullStopKeyword_1());
            		
            // InternalTextualRCV.g:8119:3: ( (this_DIGIT_2= RULE_DIGIT )? this_DIGIT_3= RULE_DIGIT )?
            int alt187=2;
            int LA187_0 = input.LA(1);

            if ( (LA187_0==RULE_DIGIT) ) {
                alt187=1;
            }
            switch (alt187) {
                case 1 :
                    // InternalTextualRCV.g:8120:4: (this_DIGIT_2= RULE_DIGIT )? this_DIGIT_3= RULE_DIGIT
                    {
                    // InternalTextualRCV.g:8120:4: (this_DIGIT_2= RULE_DIGIT )?
                    int alt186=2;
                    int LA186_0 = input.LA(1);

                    if ( (LA186_0==RULE_DIGIT) ) {
                        int LA186_1 = input.LA(2);

                        if ( (LA186_1==RULE_DIGIT) ) {
                            alt186=1;
                        }
                    }
                    switch (alt186) {
                        case 1 :
                            // InternalTextualRCV.g:8121:5: this_DIGIT_2= RULE_DIGIT
                            {
                            this_DIGIT_2=(Token)match(input,RULE_DIGIT,FOLLOW_172); 

                            					current.merge(this_DIGIT_2);
                            				

                            					newLeafNode(this_DIGIT_2, grammarAccess.getECTSAccess().getDIGITTerminalRuleCall_2_0());
                            				

                            }
                            break;

                    }

                    this_DIGIT_3=(Token)match(input,RULE_DIGIT,FOLLOW_2); 

                    				current.merge(this_DIGIT_3);
                    			

                    				newLeafNode(this_DIGIT_3, grammarAccess.getECTSAccess().getDIGITTerminalRuleCall_2_1());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleECTS"


    // $ANTLR start "entryRuleCurrency"
    // InternalTextualRCV.g:8141:1: entryRuleCurrency returns [String current=null] : iv_ruleCurrency= ruleCurrency EOF ;
    public final String entryRuleCurrency() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleCurrency = null;


        try {
            // InternalTextualRCV.g:8141:48: (iv_ruleCurrency= ruleCurrency EOF )
            // InternalTextualRCV.g:8142:2: iv_ruleCurrency= ruleCurrency EOF
            {
             newCompositeNode(grammarAccess.getCurrencyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCurrency=ruleCurrency();

            state._fsp--;

             current =iv_ruleCurrency.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCurrency"


    // $ANTLR start "ruleCurrency"
    // InternalTextualRCV.g:8148:1: ruleCurrency returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_EString_0= ruleEString ;
    public final AntlrDatatypeRuleToken ruleCurrency() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        AntlrDatatypeRuleToken this_EString_0 = null;



        	enterRule();

        try {
            // InternalTextualRCV.g:8154:2: (this_EString_0= ruleEString )
            // InternalTextualRCV.g:8155:2: this_EString_0= ruleEString
            {

            		newCompositeNode(grammarAccess.getCurrencyAccess().getEStringParserRuleCall());
            	
            pushFollow(FOLLOW_2);
            this_EString_0=ruleEString();

            state._fsp--;


            		current.merge(this_EString_0);
            	

            		afterParserOrEnumRuleCall();
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCurrency"


    // $ANTLR start "ruleSupervisionKind"
    // InternalTextualRCV.g:8168:1: ruleSupervisionKind returns [Enumerator current=null] : ( (enumLiteral_0= 'Master' ) | (enumLiteral_1= 'Bachelor' ) | (enumLiteral_2= 'PhD' ) | (enumLiteral_3= 'M.Quali' ) ) ;
    public final Enumerator ruleSupervisionKind() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;


        	enterRule();

        try {
            // InternalTextualRCV.g:8174:2: ( ( (enumLiteral_0= 'Master' ) | (enumLiteral_1= 'Bachelor' ) | (enumLiteral_2= 'PhD' ) | (enumLiteral_3= 'M.Quali' ) ) )
            // InternalTextualRCV.g:8175:2: ( (enumLiteral_0= 'Master' ) | (enumLiteral_1= 'Bachelor' ) | (enumLiteral_2= 'PhD' ) | (enumLiteral_3= 'M.Quali' ) )
            {
            // InternalTextualRCV.g:8175:2: ( (enumLiteral_0= 'Master' ) | (enumLiteral_1= 'Bachelor' ) | (enumLiteral_2= 'PhD' ) | (enumLiteral_3= 'M.Quali' ) )
            int alt188=4;
            switch ( input.LA(1) ) {
            case 131:
                {
                alt188=1;
                }
                break;
            case 132:
                {
                alt188=2;
                }
                break;
            case 133:
                {
                alt188=3;
                }
                break;
            case 134:
                {
                alt188=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 188, 0, input);

                throw nvae;
            }

            switch (alt188) {
                case 1 :
                    // InternalTextualRCV.g:8176:3: (enumLiteral_0= 'Master' )
                    {
                    // InternalTextualRCV.g:8176:3: (enumLiteral_0= 'Master' )
                    // InternalTextualRCV.g:8177:4: enumLiteral_0= 'Master'
                    {
                    enumLiteral_0=(Token)match(input,131,FOLLOW_2); 

                    				current = grammarAccess.getSupervisionKindAccess().getMASTEREnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getSupervisionKindAccess().getMASTEREnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalTextualRCV.g:8184:3: (enumLiteral_1= 'Bachelor' )
                    {
                    // InternalTextualRCV.g:8184:3: (enumLiteral_1= 'Bachelor' )
                    // InternalTextualRCV.g:8185:4: enumLiteral_1= 'Bachelor'
                    {
                    enumLiteral_1=(Token)match(input,132,FOLLOW_2); 

                    				current = grammarAccess.getSupervisionKindAccess().getBACHELOREnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getSupervisionKindAccess().getBACHELOREnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalTextualRCV.g:8192:3: (enumLiteral_2= 'PhD' )
                    {
                    // InternalTextualRCV.g:8192:3: (enumLiteral_2= 'PhD' )
                    // InternalTextualRCV.g:8193:4: enumLiteral_2= 'PhD'
                    {
                    enumLiteral_2=(Token)match(input,133,FOLLOW_2); 

                    				current = grammarAccess.getSupervisionKindAccess().getPHDEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getSupervisionKindAccess().getPHDEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalTextualRCV.g:8200:3: (enumLiteral_3= 'M.Quali' )
                    {
                    // InternalTextualRCV.g:8200:3: (enumLiteral_3= 'M.Quali' )
                    // InternalTextualRCV.g:8201:4: enumLiteral_3= 'M.Quali'
                    {
                    enumLiteral_3=(Token)match(input,134,FOLLOW_2); 

                    				current = grammarAccess.getSupervisionKindAccess().getPREMASTEREnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getSupervisionKindAccess().getPREMASTEREnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSupervisionKind"


    // $ANTLR start "ruleContactKind"
    // InternalTextualRCV.g:8211:1: ruleContactKind returns [Enumerator current=null] : ( (enumLiteral_0= 'private' ) | (enumLiteral_1= 'work' ) ) ;
    public final Enumerator ruleContactKind() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalTextualRCV.g:8217:2: ( ( (enumLiteral_0= 'private' ) | (enumLiteral_1= 'work' ) ) )
            // InternalTextualRCV.g:8218:2: ( (enumLiteral_0= 'private' ) | (enumLiteral_1= 'work' ) )
            {
            // InternalTextualRCV.g:8218:2: ( (enumLiteral_0= 'private' ) | (enumLiteral_1= 'work' ) )
            int alt189=2;
            int LA189_0 = input.LA(1);

            if ( (LA189_0==135) ) {
                alt189=1;
            }
            else if ( (LA189_0==136) ) {
                alt189=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 189, 0, input);

                throw nvae;
            }
            switch (alt189) {
                case 1 :
                    // InternalTextualRCV.g:8219:3: (enumLiteral_0= 'private' )
                    {
                    // InternalTextualRCV.g:8219:3: (enumLiteral_0= 'private' )
                    // InternalTextualRCV.g:8220:4: enumLiteral_0= 'private'
                    {
                    enumLiteral_0=(Token)match(input,135,FOLLOW_2); 

                    				current = grammarAccess.getContactKindAccess().getPRIVATEEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getContactKindAccess().getPRIVATEEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalTextualRCV.g:8227:3: (enumLiteral_1= 'work' )
                    {
                    // InternalTextualRCV.g:8227:3: (enumLiteral_1= 'work' )
                    // InternalTextualRCV.g:8228:4: enumLiteral_1= 'work'
                    {
                    enumLiteral_1=(Token)match(input,136,FOLLOW_2); 

                    				current = grammarAccess.getContactKindAccess().getWORKEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getContactKindAccess().getWORKEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleContactKind"


    // $ANTLR start "ruleSerialNumberKind"
    // InternalTextualRCV.g:8238:1: ruleSerialNumberKind returns [Enumerator current=null] : ( (enumLiteral_0= 'UNSPECIFIED' ) | (enumLiteral_1= 'PRINT' ) | (enumLiteral_2= 'ELECTRONIC' ) | (enumLiteral_3= 'CDROM' ) | (enumLiteral_4= 'ONDEMAND' ) | (enumLiteral_5= 'USB' ) ) ;
    public final Enumerator ruleSerialNumberKind() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;


        	enterRule();

        try {
            // InternalTextualRCV.g:8244:2: ( ( (enumLiteral_0= 'UNSPECIFIED' ) | (enumLiteral_1= 'PRINT' ) | (enumLiteral_2= 'ELECTRONIC' ) | (enumLiteral_3= 'CDROM' ) | (enumLiteral_4= 'ONDEMAND' ) | (enumLiteral_5= 'USB' ) ) )
            // InternalTextualRCV.g:8245:2: ( (enumLiteral_0= 'UNSPECIFIED' ) | (enumLiteral_1= 'PRINT' ) | (enumLiteral_2= 'ELECTRONIC' ) | (enumLiteral_3= 'CDROM' ) | (enumLiteral_4= 'ONDEMAND' ) | (enumLiteral_5= 'USB' ) )
            {
            // InternalTextualRCV.g:8245:2: ( (enumLiteral_0= 'UNSPECIFIED' ) | (enumLiteral_1= 'PRINT' ) | (enumLiteral_2= 'ELECTRONIC' ) | (enumLiteral_3= 'CDROM' ) | (enumLiteral_4= 'ONDEMAND' ) | (enumLiteral_5= 'USB' ) )
            int alt190=6;
            switch ( input.LA(1) ) {
            case 137:
                {
                alt190=1;
                }
                break;
            case 138:
                {
                alt190=2;
                }
                break;
            case 139:
                {
                alt190=3;
                }
                break;
            case 140:
                {
                alt190=4;
                }
                break;
            case 141:
                {
                alt190=5;
                }
                break;
            case 142:
                {
                alt190=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 190, 0, input);

                throw nvae;
            }

            switch (alt190) {
                case 1 :
                    // InternalTextualRCV.g:8246:3: (enumLiteral_0= 'UNSPECIFIED' )
                    {
                    // InternalTextualRCV.g:8246:3: (enumLiteral_0= 'UNSPECIFIED' )
                    // InternalTextualRCV.g:8247:4: enumLiteral_0= 'UNSPECIFIED'
                    {
                    enumLiteral_0=(Token)match(input,137,FOLLOW_2); 

                    				current = grammarAccess.getSerialNumberKindAccess().getUNSPECIFIEDEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getSerialNumberKindAccess().getUNSPECIFIEDEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalTextualRCV.g:8254:3: (enumLiteral_1= 'PRINT' )
                    {
                    // InternalTextualRCV.g:8254:3: (enumLiteral_1= 'PRINT' )
                    // InternalTextualRCV.g:8255:4: enumLiteral_1= 'PRINT'
                    {
                    enumLiteral_1=(Token)match(input,138,FOLLOW_2); 

                    				current = grammarAccess.getSerialNumberKindAccess().getPRINTEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getSerialNumberKindAccess().getPRINTEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalTextualRCV.g:8262:3: (enumLiteral_2= 'ELECTRONIC' )
                    {
                    // InternalTextualRCV.g:8262:3: (enumLiteral_2= 'ELECTRONIC' )
                    // InternalTextualRCV.g:8263:4: enumLiteral_2= 'ELECTRONIC'
                    {
                    enumLiteral_2=(Token)match(input,139,FOLLOW_2); 

                    				current = grammarAccess.getSerialNumberKindAccess().getELECTRONICEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getSerialNumberKindAccess().getELECTRONICEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalTextualRCV.g:8270:3: (enumLiteral_3= 'CDROM' )
                    {
                    // InternalTextualRCV.g:8270:3: (enumLiteral_3= 'CDROM' )
                    // InternalTextualRCV.g:8271:4: enumLiteral_3= 'CDROM'
                    {
                    enumLiteral_3=(Token)match(input,140,FOLLOW_2); 

                    				current = grammarAccess.getSerialNumberKindAccess().getCDROMEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getSerialNumberKindAccess().getCDROMEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;
                case 5 :
                    // InternalTextualRCV.g:8278:3: (enumLiteral_4= 'ONDEMAND' )
                    {
                    // InternalTextualRCV.g:8278:3: (enumLiteral_4= 'ONDEMAND' )
                    // InternalTextualRCV.g:8279:4: enumLiteral_4= 'ONDEMAND'
                    {
                    enumLiteral_4=(Token)match(input,141,FOLLOW_2); 

                    				current = grammarAccess.getSerialNumberKindAccess().getONDEMANDEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_4, grammarAccess.getSerialNumberKindAccess().getONDEMANDEnumLiteralDeclaration_4());
                    			

                    }


                    }
                    break;
                case 6 :
                    // InternalTextualRCV.g:8286:3: (enumLiteral_5= 'USB' )
                    {
                    // InternalTextualRCV.g:8286:3: (enumLiteral_5= 'USB' )
                    // InternalTextualRCV.g:8287:4: enumLiteral_5= 'USB'
                    {
                    enumLiteral_5=(Token)match(input,142,FOLLOW_2); 

                    				current = grammarAccess.getSerialNumberKindAccess().getUSBEnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_5, grammarAccess.getSerialNumberKindAccess().getUSBEnumLiteralDeclaration_5());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSerialNumberKind"


    // $ANTLR start "ruleCourseKind"
    // InternalTextualRCV.g:8297:1: ruleCourseKind returns [Enumerator current=null] : ( (enumLiteral_0= 'UNDERGRADUATE' ) | (enumLiteral_1= 'GRADUATE' ) | (enumLiteral_2= 'CONTINUING_EDUCATION' ) ) ;
    public final Enumerator ruleCourseKind() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;


        	enterRule();

        try {
            // InternalTextualRCV.g:8303:2: ( ( (enumLiteral_0= 'UNDERGRADUATE' ) | (enumLiteral_1= 'GRADUATE' ) | (enumLiteral_2= 'CONTINUING_EDUCATION' ) ) )
            // InternalTextualRCV.g:8304:2: ( (enumLiteral_0= 'UNDERGRADUATE' ) | (enumLiteral_1= 'GRADUATE' ) | (enumLiteral_2= 'CONTINUING_EDUCATION' ) )
            {
            // InternalTextualRCV.g:8304:2: ( (enumLiteral_0= 'UNDERGRADUATE' ) | (enumLiteral_1= 'GRADUATE' ) | (enumLiteral_2= 'CONTINUING_EDUCATION' ) )
            int alt191=3;
            switch ( input.LA(1) ) {
            case 143:
                {
                alt191=1;
                }
                break;
            case 144:
                {
                alt191=2;
                }
                break;
            case 145:
                {
                alt191=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 191, 0, input);

                throw nvae;
            }

            switch (alt191) {
                case 1 :
                    // InternalTextualRCV.g:8305:3: (enumLiteral_0= 'UNDERGRADUATE' )
                    {
                    // InternalTextualRCV.g:8305:3: (enumLiteral_0= 'UNDERGRADUATE' )
                    // InternalTextualRCV.g:8306:4: enumLiteral_0= 'UNDERGRADUATE'
                    {
                    enumLiteral_0=(Token)match(input,143,FOLLOW_2); 

                    				current = grammarAccess.getCourseKindAccess().getUNDERGRADUATEEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getCourseKindAccess().getUNDERGRADUATEEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalTextualRCV.g:8313:3: (enumLiteral_1= 'GRADUATE' )
                    {
                    // InternalTextualRCV.g:8313:3: (enumLiteral_1= 'GRADUATE' )
                    // InternalTextualRCV.g:8314:4: enumLiteral_1= 'GRADUATE'
                    {
                    enumLiteral_1=(Token)match(input,144,FOLLOW_2); 

                    				current = grammarAccess.getCourseKindAccess().getGRADUATEEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getCourseKindAccess().getGRADUATEEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalTextualRCV.g:8321:3: (enumLiteral_2= 'CONTINUING_EDUCATION' )
                    {
                    // InternalTextualRCV.g:8321:3: (enumLiteral_2= 'CONTINUING_EDUCATION' )
                    // InternalTextualRCV.g:8322:4: enumLiteral_2= 'CONTINUING_EDUCATION'
                    {
                    enumLiteral_2=(Token)match(input,145,FOLLOW_2); 

                    				current = grammarAccess.getCourseKindAccess().getCONTINUING_EDUCATIONEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getCourseKindAccess().getCONTINUING_EDUCATIONEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCourseKind"

    // Delegated rules


    protected DFA8 dfa8 = new DFA8(this);
    protected DFA39 dfa39 = new DFA39(this);
    static final String dfa_1s = "\13\uffff";
    static final String dfa_2s = "\2\7\1\25\1\7\2\uffff\1\7\1\25\2\7\1\26";
    static final String dfa_3s = "\1\41\1\7\1\u0086\1\41\2\uffff\1\7\1\u0086\1\41\1\7\1\u0086";
    static final String dfa_4s = "\4\uffff\1\2\1\1\5\uffff";
    static final String dfa_5s = "\13\uffff}>";
    static final String[] dfa_6s = {
            "\1\2\31\uffff\1\1",
            "\1\2",
            "\1\3\1\4\154\uffff\4\5",
            "\1\7\31\uffff\1\6",
            "",
            "",
            "\1\7",
            "\1\10\1\4\154\uffff\4\5",
            "\1\12\31\uffff\1\11",
            "\1\12",
            "\1\4\154\uffff\4\5"
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final char[] dfa_2 = DFA.unpackEncodedStringToUnsignedChars(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final short[] dfa_4 = DFA.unpackEncodedString(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[][] dfa_6 = unpackEncodedStringArray(dfa_6s);

    class DFA8 extends DFA {

        public DFA8(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 8;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_2;
            this.max = dfa_3;
            this.accept = dfa_4;
            this.special = dfa_5;
            this.transition = dfa_6;
        }
        public String getDescription() {
            return "382:3: ( ( (lv_dateEnd_0_0= rulePublishedDate ) ) | ( ( (lv_dasteStart_1_0= rulePublishedDate ) ) otherlv_2= '>' ( (lv_dateEnd_3_0= rulePublishedDate ) )? ) )";
        }
    }
    static final String dfa_7s = "\14\uffff";
    static final String dfa_8s = "\1\111\1\112\1\uffff\1\133\4\uffff\1\134\1\135\2\uffff";
    static final String dfa_9s = "\1\111\1\161\1\uffff\1\133\4\uffff\1\134\1\150\2\uffff";
    static final String dfa_10s = "\2\uffff\1\6\1\uffff\1\7\1\1\1\5\1\4\2\uffff\1\2\1\3";
    static final String dfa_11s = "\14\uffff}>";
    static final String[] dfa_12s = {
            "\1\1",
            "\1\5\17\uffff\1\3\20\uffff\1\7\1\6\1\uffff\1\2\2\uffff\1\4",
            "",
            "\1\10",
            "",
            "",
            "",
            "",
            "\1\11",
            "\1\12\12\uffff\1\13",
            "",
            ""
    };

    static final short[] dfa_7 = DFA.unpackEncodedString(dfa_7s);
    static final char[] dfa_8 = DFA.unpackEncodedStringToUnsignedChars(dfa_8s);
    static final char[] dfa_9 = DFA.unpackEncodedStringToUnsignedChars(dfa_9s);
    static final short[] dfa_10 = DFA.unpackEncodedString(dfa_10s);
    static final short[] dfa_11 = DFA.unpackEncodedString(dfa_11s);
    static final short[][] dfa_12 = unpackEncodedStringArray(dfa_12s);

    class DFA39 extends DFA {

        public DFA39(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 39;
            this.eot = dfa_7;
            this.eof = dfa_7;
            this.min = dfa_8;
            this.max = dfa_9;
            this.accept = dfa_10;
            this.special = dfa_11;
            this.transition = dfa_12;
        }
        public String getDescription() {
            return "1581:2: (this_Journal_0= ruleJournal | this_Conference_1= ruleConference | this_Workshop_2= ruleWorkshop | this_Book_3= ruleBook | this_BookChapter_4= ruleBookChapter | this_Report_5= ruleReport | this_Miscellaneous_6= ruleMiscellaneous )";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000080000030L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000080040030L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000200000080L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000200000082L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000210000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000040010L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000078L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000200000080L,0x0000000000000000L,0x0000000000000078L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000001010000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x000000002C040040L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000020040040L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000020040000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000040200090L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000040200092L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000000200002L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000000001000030L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000000000010030L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x000007FD00040000L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000074L,0x0000000000000180L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000000200040000L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x000007FC00040000L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0000000001040000L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x000007F800040000L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000200L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x000007F000040000L});
    public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x000007E000040000L});
    public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x000007C000040000L});
    public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0000078000040000L});
    public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x0000070000040000L});
    public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_51 = new BitSet(new long[]{0x0000060000040000L});
    public static final BitSet FOLLOW_52 = new BitSet(new long[]{0x0040000000000000L});
    public static final BitSet FOLLOW_53 = new BitSet(new long[]{0x0000040000040000L});
    public static final BitSet FOLLOW_54 = new BitSet(new long[]{0x1800000000000000L});
    public static final BitSet FOLLOW_55 = new BitSet(new long[]{0x0000100000000000L});
    public static final BitSet FOLLOW_56 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_57 = new BitSet(new long[]{0x0000800000040000L});
    public static final BitSet FOLLOW_58 = new BitSet(new long[]{0x0002000000000000L});
    public static final BitSet FOLLOW_59 = new BitSet(new long[]{0x000C000000040000L});
    public static final BitSet FOLLOW_60 = new BitSet(new long[]{0x0008000000040000L});
    public static final BitSet FOLLOW_61 = new BitSet(new long[]{0x0010000000000000L});
    public static final BitSet FOLLOW_62 = new BitSet(new long[]{0x0020000001000000L});
    public static final BitSet FOLLOW_63 = new BitSet(new long[]{0x0080000000000000L});
    public static final BitSet FOLLOW_64 = new BitSet(new long[]{0x0300000000000000L});
    public static final BitSet FOLLOW_65 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000038000L});
    public static final BitSet FOLLOW_66 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_67 = new BitSet(new long[]{0x0400000000000000L});
    public static final BitSet FOLLOW_68 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_69 = new BitSet(new long[]{0x1000000000000000L});
    public static final BitSet FOLLOW_70 = new BitSet(new long[]{0xE000000000000000L});
    public static final BitSet FOLLOW_71 = new BitSet(new long[]{0xC000000000000000L});
    public static final BitSet FOLLOW_72 = new BitSet(new long[]{0x8000000000000000L});
    public static final BitSet FOLLOW_73 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_74 = new BitSet(new long[]{0x0000000200200080L});
    public static final BitSet FOLLOW_75 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000002L});
    public static final BitSet FOLLOW_76 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
    public static final BitSet FOLLOW_77 = new BitSet(new long[]{0x0000000009000000L});
    public static final BitSet FOLLOW_78 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_79 = new BitSet(new long[]{0x0010000000040030L});
    public static final BitSet FOLLOW_80 = new BitSet(new long[]{0x0000000000000030L,0x0000000000000008L});
    public static final BitSet FOLLOW_81 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_82 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000008L});
    public static final BitSet FOLLOW_83 = new BitSet(new long[]{0x0020000000000000L});
    public static final BitSet FOLLOW_84 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
    public static final BitSet FOLLOW_85 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000020L});
    public static final BitSet FOLLOW_86 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_87 = new BitSet(new long[]{0x0000000000000032L});
    public static final BitSet FOLLOW_88 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000400L});
    public static final BitSet FOLLOW_89 = new BitSet(new long[]{0x0000800000000000L,0x0000000000000800L});
    public static final BitSet FOLLOW_90 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000800L});
    public static final BitSet FOLLOW_91 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000180L});
    public static final BitSet FOLLOW_92 = new BitSet(new long[]{0x0000000000000000L,0x0000000000007000L});
    public static final BitSet FOLLOW_93 = new BitSet(new long[]{0x0000000000000000L,0x0000000000006000L});
    public static final BitSet FOLLOW_94 = new BitSet(new long[]{0x0000000000000000L,0x0000000000004000L});
    public static final BitSet FOLLOW_95 = new BitSet(new long[]{0x0000000000000000L,0x0000000000018000L});
    public static final BitSet FOLLOW_96 = new BitSet(new long[]{0x0000000000000000L,0x0000000000010000L});
    public static final BitSet FOLLOW_97 = new BitSet(new long[]{0x0000000000000000L,0x00000000001E0000L});
    public static final BitSet FOLLOW_98 = new BitSet(new long[]{0x0000000000000000L,0x00000000001C0000L});
    public static final BitSet FOLLOW_99 = new BitSet(new long[]{0x0000000000000000L,0x0000000000180000L});
    public static final BitSet FOLLOW_100 = new BitSet(new long[]{0x0000000000000000L,0x0000000000100000L});
    public static final BitSet FOLLOW_101 = new BitSet(new long[]{0x0000000000000000L,0x0000000000200000L});
    public static final BitSet FOLLOW_102 = new BitSet(new long[]{0x0000000000000000L,0x0000000000400000L});
    public static final BitSet FOLLOW_103 = new BitSet(new long[]{0x0000000000000000L,0x0000000001800000L});
    public static final BitSet FOLLOW_104 = new BitSet(new long[]{0x0000000000000000L,0x0000000001000000L});
    public static final BitSet FOLLOW_105 = new BitSet(new long[]{0x0000000000040000L,0x0000000002000000L});
    public static final BitSet FOLLOW_106 = new BitSet(new long[]{0x0000000000000000L,0x0004000000000000L});
    public static final BitSet FOLLOW_107 = new BitSet(new long[]{0x0000000000000000L,0x0000000004000000L});
    public static final BitSet FOLLOW_108 = new BitSet(new long[]{0x0000000000000000L,0x0000000008000000L});
    public static final BitSet FOLLOW_109 = new BitSet(new long[]{0x0000000000000000L,0x0000000010000000L});
    public static final BitSet FOLLOW_110 = new BitSet(new long[]{0x0000000000000000L,0x0000000020000000L});
    public static final BitSet FOLLOW_111 = new BitSet(new long[]{0x0000000000000000L,0x0000000040008000L});
    public static final BitSet FOLLOW_112 = new BitSet(new long[]{0x0000000000000000L,0x0000000040000000L});
    public static final BitSet FOLLOW_113 = new BitSet(new long[]{0x0000000000000000L,0x00000001801E0000L});
    public static final BitSet FOLLOW_114 = new BitSet(new long[]{0x0000000000000000L,0x00000001801C0000L});
    public static final BitSet FOLLOW_115 = new BitSet(new long[]{0x0000000000000000L,0x00000001001C0000L});
    public static final BitSet FOLLOW_116 = new BitSet(new long[]{0x0000000000000000L,0x00000001000C0000L});
    public static final BitSet FOLLOW_117 = new BitSet(new long[]{0x0000000000000000L,0x0000000100080000L});
    public static final BitSet FOLLOW_118 = new BitSet(new long[]{0x0000000000000000L,0x0000000100000000L});
    public static final BitSet FOLLOW_119 = new BitSet(new long[]{0x0000000000000000L,0x0000000600000000L});
    public static final BitSet FOLLOW_120 = new BitSet(new long[]{0x0000000000000000L,0x0000000400000000L});
    public static final BitSet FOLLOW_121 = new BitSet(new long[]{0x0000000000000000L,0x0000001800400000L});
    public static final BitSet FOLLOW_122 = new BitSet(new long[]{0x0000000000000000L,0x0000001000400000L});
    public static final BitSet FOLLOW_123 = new BitSet(new long[]{0x0000000000000000L,0x0000002001800000L});
    public static final BitSet FOLLOW_124 = new BitSet(new long[]{0x0000000000000000L,0x0000002001000000L});
    public static final BitSet FOLLOW_125 = new BitSet(new long[]{0x0000000000040000L,0x000000C000000000L});
    public static final BitSet FOLLOW_126 = new BitSet(new long[]{0x0000000000040000L,0x0000008000000000L});
    public static final BitSet FOLLOW_127 = new BitSet(new long[]{0x0000000000000000L,0x0000010000000000L});
    public static final BitSet FOLLOW_128 = new BitSet(new long[]{0x0000000000000000L,0x0000061800400000L});
    public static final BitSet FOLLOW_129 = new BitSet(new long[]{0x0000000000000000L,0x0000061000400000L});
    public static final BitSet FOLLOW_130 = new BitSet(new long[]{0x0000000000000000L,0x0000060000400000L});
    public static final BitSet FOLLOW_131 = new BitSet(new long[]{0x0000000000000000L,0x0000040000400000L});
    public static final BitSet FOLLOW_132 = new BitSet(new long[]{0x0000000000000000L,0x0000080000000000L});
    public static final BitSet FOLLOW_133 = new BitSet(new long[]{0x0000000000000000L,0x0000000080528000L});
    public static final BitSet FOLLOW_134 = new BitSet(new long[]{0x0000000000000000L,0x0000000080520000L});
    public static final BitSet FOLLOW_135 = new BitSet(new long[]{0x0000000000000000L,0x0000000080500000L});
    public static final BitSet FOLLOW_136 = new BitSet(new long[]{0x0000000000000000L,0x0000000000500000L});
    public static final BitSet FOLLOW_137 = new BitSet(new long[]{0x0000000000040000L,0x0000004000000000L});
    public static final BitSet FOLLOW_138 = new BitSet(new long[]{0x0000000000000000L,0x0000100000000000L});
    public static final BitSet FOLLOW_139 = new BitSet(new long[]{0x0000000000000000L,0x00002000805E0000L});
    public static final BitSet FOLLOW_140 = new BitSet(new long[]{0x0000000000000000L,0x00002000805C0000L});
    public static final BitSet FOLLOW_141 = new BitSet(new long[]{0x0000000000000000L,0x00002000005C0000L});
    public static final BitSet FOLLOW_142 = new BitSet(new long[]{0x0000000000000000L,0x00002000004C0000L});
    public static final BitSet FOLLOW_143 = new BitSet(new long[]{0x0000000000000000L,0x0000200000480000L});
    public static final BitSet FOLLOW_144 = new BitSet(new long[]{0x0000000000000000L,0x0000200000400000L});
    public static final BitSet FOLLOW_145 = new BitSet(new long[]{0x0000000000000000L,0x0000400000000000L});
    public static final BitSet FOLLOW_146 = new BitSet(new long[]{0x0000000000000000L,0x0001800000408000L});
    public static final BitSet FOLLOW_147 = new BitSet(new long[]{0x0000000000000000L,0x0001800000400000L});
    public static final BitSet FOLLOW_148 = new BitSet(new long[]{0x0000000000000000L,0x0001000000400000L});
    public static final BitSet FOLLOW_149 = new BitSet(new long[]{0x0000000000000000L,0x0002000000000000L});
    public static final BitSet FOLLOW_150 = new BitSet(new long[]{0x0000000000000000L,0x0000000000408000L});
    public static final BitSet FOLLOW_151 = new BitSet(new long[]{0x0000000000000000L,0x0008000000000000L});
    public static final BitSet FOLLOW_152 = new BitSet(new long[]{0x0000000000000000L,0x0010000000000000L});
    public static final BitSet FOLLOW_153 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000007E00L});
    public static final BitSet FOLLOW_154 = new BitSet(new long[]{0x0000000000000000L,0x0040000000000000L});
    public static final BitSet FOLLOW_155 = new BitSet(new long[]{0x0000000000000000L,0x0080000000000000L});
    public static final BitSet FOLLOW_156 = new BitSet(new long[]{0x0000800000000000L,0x0200000000000000L});
    public static final BitSet FOLLOW_157 = new BitSet(new long[]{0x0000000000000000L,0x0200000000000000L});
    public static final BitSet FOLLOW_158 = new BitSet(new long[]{0x0000000000000000L,0x1C00000000000000L});
    public static final BitSet FOLLOW_159 = new BitSet(new long[]{0x0000000000000000L,0x1800000000000000L});
    public static final BitSet FOLLOW_160 = new BitSet(new long[]{0x0000000000000000L,0x1000000000000000L});
    public static final BitSet FOLLOW_161 = new BitSet(new long[]{0x0002400000000000L,0xE000000000000000L});
    public static final BitSet FOLLOW_162 = new BitSet(new long[]{0x0002000000000000L,0xE000000000000000L});
    public static final BitSet FOLLOW_163 = new BitSet(new long[]{0x0000000000000000L,0xE000000000000000L});
    public static final BitSet FOLLOW_164 = new BitSet(new long[]{0x0000000000000000L,0xC000000000000000L});
    public static final BitSet FOLLOW_165 = new BitSet(new long[]{0x0000000000000000L,0x0020000000000000L});
    public static final BitSet FOLLOW_166 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L});
    public static final BitSet FOLLOW_167 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_168 = new BitSet(new long[]{0x0000000000200080L});
    public static final BitSet FOLLOW_169 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_170 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000000000006L});
    public static final BitSet FOLLOW_171 = new BitSet(new long[]{0x0000000000000202L});
    public static final BitSet FOLLOW_172 = new BitSet(new long[]{0x0000000000000200L});

}