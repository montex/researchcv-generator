package org.montex.researchcv.xtext.linktoxmi

import org.eclipse.ui.plugin.AbstractUIPlugin
import org.eclipse.xtext.ui.LanguageSpecific
import org.eclipse.xtext.ui.resource.generic.EmfUiModule
import org.eclipse.xtext.ui.editor.IURIEditorOpener
import com.google.inject.Binder

class ResearchCVUiModule extends EmfUiModule {

    new(AbstractUIPlugin plugin) {
        super(plugin)
    }
    
    override configureLanguageSpecificURIEditorOpener(Binder binder) {
        binder.bind(IURIEditorOpener).annotatedWith(LanguageSpecific).to(ResearchCVEditorOpener)
    }
}