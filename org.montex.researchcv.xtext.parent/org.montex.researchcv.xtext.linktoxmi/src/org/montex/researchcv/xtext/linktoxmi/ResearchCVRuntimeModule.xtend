package org.montex.researchcv.xtext.linktoxmi

import org.eclipse.xtext.resource.generic.AbstractGenericResourceRuntimeModule
import org.eclipse.xtext.naming.SimpleNameProvider

class ResearchCVRuntimeModule extends AbstractGenericResourceRuntimeModule {
  
    override protected getFileExtensions() {
        'rcv'
    }
    
    override protected getLanguageName() {
        'org.montex.researchcv.ResearchCV'
    }

    override bindIQualifiedNameProvider() {
        SimpleNameProvider
    }

}