package org.montex.researchcv.xtext.linktoxmi

import org.montex.researchcv.presentation.ResearchCVEditor
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EReference
import org.eclipse.ui.IEditorPart
import org.eclipse.xtext.ui.editor.LanguageSpecificURIEditorOpener

class ResearchCVEditorOpener extends LanguageSpecificURIEditorOpener {
    
    override protected void selectAndReveal(
        IEditorPart openEditor, 
        URI uri, EReference crossReference, int indexInList, 
        boolean select) {
        if (uri.fragment !== null) {
            val ecoreEditor = openEditor.getAdapter(ResearchCVEditor)
            if (ecoreEditor instanceof ResearchCVEditor) {
                val eObject = ecoreEditor.editingDomain.resourceSet.getEObject(uri, true)
                ecoreEditor.setSelectionToViewer(#[eObject])
            }
        }
    }

    override protected String getEditorId() {
        return "io.typefox.xtextxmi.tree.presentation.TreeEditorID"
    }
}