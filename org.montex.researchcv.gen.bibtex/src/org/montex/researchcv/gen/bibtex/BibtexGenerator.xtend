package org.montex.researchcv.gen.bibtex

import org.montex.researchcv.Publication;

import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource

import org.montex.researchcv.Journal
import org.montex.researchcv.InCollection
import org.montex.researchcv.InProceedings
import org.montex.researchcv.BookChapter
import org.montex.researchcv.Person
import org.montex.researchcv.Researcher

import org.eclipse.xtend.lib.annotations.Accessors
import java.util.List
import org.montex.researchcv.Report

class BibtexGenerator {
    
    @Accessors(PUBLIC_GETTER, PUBLIC_SETTER) 
    boolean fullBibtex = true
    
    new() {
        this(true)
    }
    
    new(boolean fullBibtex) {
        this.fullBibtex = fullBibtex
    }
           
    def dispatch String generate(URI model) {
        var loader = new ModelLoader(model)

        return generate(loader.modelResource)
    }
    
    def dispatch String generate(Resource model) {
        return model.allContents.filter(Publication).toList.generate
    }

    def dispatch String generate(Researcher r) {
        return r.eAllContents.filter(Publication).toList.generate
    }
    
    def dispatch String generate(Publication[] publications) {
        return publications.toList.generate
    }
    
    def dispatch String generate(List<Publication> publications) {
        return publications.sortBy[date.toString].map[generate]
                    .join('''


''')
    }
    
    def dispatch String generate(Publication p) {
        return p.generateBibtex.toString
                   // In Bibtex we needto escape & 
                   .replace("&","\\&")
    }

    private def dispatch generateBibtex(Journal j)'''
        @article{«j.citekey»,
          author = {«j.authors.generateNames»},
          «j.generateTitle»
          journal = {«j.journalName»},
          «IF (fullBibtex && !j.publisher.isNullOrEmpty)»publisher = {«j.publisher»},«ENDIF»
          «IF (j.volume > 0)»volume = {«j.volume»},«ENDIF»
          «IF (j.issue > 0)»number = {«j.issue»},«ENDIF»
          pages = {«j.firstPage»-«j.lastPage»},
          «IF (j.date.month > 0)»month = {«j.date.month»},«ENDIF»
          «j.generateNote»
          «j.generateYear»
        }'''
    
    private def dispatch generateBibtex(InProceedings p)'''
        @inproceedings{«p.citekey»,
          author = {«p.authors.generateNames»},
          «p.generateTitle»
          booktitle = {«p.eventName» («p.eventShortName» «p.date.year»)},
          «p.generateAddress»
          «p.generateDates»
          «p.generatePages»
          «p.generateNote»
          «p.generateYear»
        }'''
        
    private def dispatch generateBibtex(BookChapter bc)'''
        @incollection{«bc.citekey»,
          author = {«bc.authors.generateNames»},
          «bc.generateTitle»
          «bc.generateBooktitle»
          «bc.generateEditors»
          «bc.generatePublisher»
          «bc.generatePages»
          «IF !bc.chapterNumber.nullOrEmpty»chapter = {«bc.chapterNumber»},«ENDIF»
          «bc.generateSeries»
          «bc.generateISBN»
          «bc.generateNote»
          «bc.generateYear»
        }'''

    private def dispatch generateBibtex(Report r)'''
        @incollection{«r.citekey»,
          author = {«r.authors.generateNames»},
          «r.generateTitle»
          «IF !r.institution.nullOrEmpty»institution = {«r.institution»},«ENDIF»
          number = {«r.reportNumber»},
          howpublished = {«r.URL»},
          «r.generateNote»
          «r.generateYear»
        }'''

    private def generateNames(Person[] people) {
        var String names;
        if(fullBibtex) {
            names = people.map[lastNames.join(" ") + ", " + firstNames.join(" ")].join(" and ")
        }else {
            names = people.map[lastNames.last + ", " + firstNames.head].join(" and ")
        }
        return names
    }
    
    private def generateTitle(Publication p)'''
        title = {{«p.title»}},'''

    private def generateSeries(InCollection ic)'''
        «IF !ic.series.nullOrEmpty»series = {«ic.series»},«ENDIF»'''
    
    private def generatePages(InCollection p)'''
        «IF !p.firstPage.nullOrEmpty»pages = {«p.firstPage»-«p.lastPage»},«ENDIF»'''

    private def generateNote(Publication p) {
        if(!p.published){
            p.notes.add('''\emph{To appear}''')
        }
        return '''«IF (!p.notes.empty)»note = {«p.notes.join(".")»},«ENDIF»'''
    }
    
    private def generateYear(Publication p)'''
        year = {«p.date.year»}'''
        
    private def generateBooktitle(InCollection p)'''
        booktitle = {«p.booktitle»},'''
        
    private def generateAddress(InProceedings p)'''
        «IF !p.venue.isNullOrEmpty»address = {«p.venue»},«ENDIF»'''
        
    private def generateDates(InProceedings p)'''
        date = {«p.date»«IF p.eventEndDate !== null»/«p.eventEndDate»«ENDIF»},'''
        
    private def generateEditors(BookChapter bc)'''
        «IF !bc.bookeditors.isEmpty»editors = {«bc.bookeditors.generateNames»},«ENDIF»'''
    
    private def generatePublisher(BookChapter bc)'''
        «IF !bc.publisher.nullOrEmpty»publisher = {«bc.publisher»},«ENDIF»'''
    
    private def generateISBN(BookChapter bc)'''
        «IF !bc.ISBN.isEmpty»isbn = {«bc.ISBN.head.value»},«ENDIF»''' 
}