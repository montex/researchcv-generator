package org.montex.researchcv.gen.bibtex

import java.io.IOException
import java.io.File
import java.io.FileWriter

import org.eclipse.emf.common.util.URI
import org.montex.researchcv.Publication

class BibtexWriter {
    
    val static CV_MODE_SWITCH = "-cv"
    
    var BibtexGenerator generator

    new(boolean cvMode) {
        generator = new BibtexGenerator(!cvMode)
    }

    def void write(String filePath, Publication[] publications) {
        var outFile = new File(filePath)
        
        println('''Writing «publications.length» entries on «outFile.absolutePath»''')
        
        var writer = new FileWriter(outFile);
        writer.write(generator.generate(publications))
        writer.close
    }
    
    def void write(String filePath, URI model) {
        var outFile = new File(filePath)
        
        println('''Writing «model.path» entries on «outFile.absolutePath»''')
        
        var writer = new FileWriter(outFile);
        writer.write(generator.generate(model))
        writer.close
    }
    
    def static void main(String[] args) {
        
        var cvMode = false;
        var arguments = args
         
        if(arguments.contains(CV_MODE_SWITCH)) {
            cvMode = true;
            arguments = arguments.reject[equals(CV_MODE_SWITCH)]
        }
                
        try {
            if (arguments.isEmpty) {
                printUsage
            } else {
                var model = URI.createFileURI(arguments.get(0));
                var File folder
                if(arguments.length > 1) {
                    folder = new File(arguments.get(1));
                }else{
                    folder = new File(".")
                }
                
                var loader = new ModelLoader(model)
                var bibWriter = new BibtexWriter(cvMode)

                
                if(cvMode) {
      
                    bibWriter.write(folder.absolutePath + "/journal.bib", loader.publicationsJournal)
                    bibWriter.write(folder.absolutePath + "/paper.bib", loader.publicationsConference)
                    bibWriter.write(folder.absolutePath + "/workshop.bib", loader.publicationsWorkshop)
                    bibWriter.write(folder.absolutePath + "/teaching.bib", loader.publicationsTeaching)
                    bibWriter.write(folder.absolutePath + "/other.bib", loader.publicationsOther)

                    
                }else{
                    var fileName = model.lastSegment.replace("." + model.fileExtension, ".bib")                
                    bibWriter.write(folder.absolutePath + "/" + fileName, model)
                }             
            }
        }catch(IOException e) {
            e.printStackTrace
        }
    }
    
    private def static void printUsage() {
        println('''
       ResearchCV Bibtex Generator
         (C) 2020 Leonardo Montecchi 
         leonardo@ic.unicamp.br
       
       Usage:
         java -jar bibwriter [-cv] MODEL [FOLDER]
       
           MODEL
           The instance of the ResearchCV metamodel
         
           FOLDER
           Output folder
         
           -cv:
           Curriculum mode. Omits some fields for a more compact bibtex
        ''')
    }
}