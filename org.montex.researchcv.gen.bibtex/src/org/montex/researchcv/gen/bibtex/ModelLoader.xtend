package org.montex.researchcv.gen.bibtex

import org.montex.researchcv.Researcher
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtend.lib.annotations.Accessors
import org.montex.researchcv.Journal
import org.montex.researchcv.Conference
import org.montex.researchcv.Workshop
import org.montex.researchcv.BookChapter
import org.montex.researchcv.impl.ResearchCVPackageImpl
import org.eclipse.emf.ecore.EPackage
import org.montex.researchcv.Publication
import org.eclipse.ocl.ecore.delegate.OCLDelegateDomain
import org.eclipse.ocl.ecore.OCL

class ModelLoader {
    
    @Accessors(PUBLIC_GETTER)
    Resource modelResource;
    
    @Accessors(PUBLIC_GETTER)
    Researcher researcher;
    
    new(URI model) {
        registerMetamodel
        load(model)
    }
    
    def private void registerMetamodel() {
        // Register ResearchCV metamodel
        var myPackage = ResearchCVPackageImpl.eINSTANCE;
        EPackage.Registry.INSTANCE.put(myPackage.getNsURI(), myPackage);
    }
    
    def private void load(URI model) {
        var rs = new ResourceSetImpl();
        rs.getResourceFactoryRegistry()
            .getExtensionToFactoryMap()
            .put("rcv", new XMIResourceFactoryImpl());

        // Initialize OCL metamodel and delegates
        OCL.initialize(rs);
        OCLDelegateDomain.initialize(rs);

        // Read a resource (XMI file)
        modelResource = rs.getResource(model, true);
        
        researcher = modelResource.allContents.filter(Researcher).head
    }
      
    def getPublicationsJournal() {
        researcher.publications.filter(Journal)
    }
    
    def getPublicationsConference() {
        researcher.publications.filter(Conference).map[it as Publication]
    }

    def getPublicationsWorkshop() {
        researcher.publications.filter(Workshop).map[it as Publication]
    }
    
    def getPublicationsBookChapter() {
        researcher.publications.filter(BookChapter).map[it as Publication]
    }
    
    def getPublicationsTeaching() {
        researcher.teachingMaterial
    }
    
    def getPublicationsOther() {
        researcher.otherMaterial
    }
}
