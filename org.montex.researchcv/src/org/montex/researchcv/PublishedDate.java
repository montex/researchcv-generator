/**
 */
package org.montex.researchcv;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Published Date</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.PublishedDate#getDay <em>Day</em>}</li>
 *   <li>{@link org.montex.researchcv.PublishedDate#getMonth <em>Month</em>}</li>
 *   <li>{@link org.montex.researchcv.PublishedDate#getYear <em>Year</em>}</li>
 *   <li>{@link org.montex.researchcv.PublishedDate#getMonthName <em>Month Name</em>}</li>
 *   <li>{@link org.montex.researchcv.PublishedDate#getMonthShortName <em>Month Short Name</em>}</li>
 * </ul>
 *
 * @see org.montex.researchcv.ResearchCVPackage#getPublishedDate()
 * @model
 * @generated
 */
public interface PublishedDate extends EObject {
	/**
	 * Returns the value of the '<em><b>Day</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Day</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Day</em>' attribute.
	 * @see #setDay(int)
	 * @see org.montex.researchcv.ResearchCVPackage#getPublishedDate_Day()
	 * @model default="1" required="true"
	 * @generated
	 */
	int getDay();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.PublishedDate#getDay <em>Day</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Day</em>' attribute.
	 * @see #getDay()
	 * @generated
	 */
	void setDay(int value);

	/**
	 * Returns the value of the '<em><b>Month</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Month</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Month</em>' attribute.
	 * @see #setMonth(int)
	 * @see org.montex.researchcv.ResearchCVPackage#getPublishedDate_Month()
	 * @model default="1" required="true"
	 * @generated
	 */
	int getMonth();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.PublishedDate#getMonth <em>Month</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Month</em>' attribute.
	 * @see #getMonth()
	 * @generated
	 */
	void setMonth(int value);

	/**
	 * Returns the value of the '<em><b>Year</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Year</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Year</em>' attribute.
	 * @see #setYear(int)
	 * @see org.montex.researchcv.ResearchCVPackage#getPublishedDate_Year()
	 * @model required="true"
	 * @generated
	 */
	int getYear();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.PublishedDate#getYear <em>Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Year</em>' attribute.
	 * @see #getYear()
	 * @generated
	 */
	void setYear(int value);

	/**
	 * Returns the value of the '<em><b>Month Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Month Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Month Name</em>' attribute.
	 * @see org.montex.researchcv.ResearchCVPackage#getPublishedDate_MonthName()
	 * @model transient="true" changeable="false" derived="true"
	 * @generated
	 */
	String getMonthName();

	/**
	 * Returns the value of the '<em><b>Month Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Month Short Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Month Short Name</em>' attribute.
	 * @see org.montex.researchcv.ResearchCVPackage#getPublishedDate_MonthShortName()
	 * @model transient="true" changeable="false" derived="true"
	 * @generated
	 */
	String getMonthShortName();

} // PublishedDate
