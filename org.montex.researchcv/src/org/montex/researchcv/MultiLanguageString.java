/**
 */
package org.montex.researchcv;

import java.util.Locale;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Multi Language String</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.MultiLanguageString#getText <em>Text</em>}</li>
 *   <li>{@link org.montex.researchcv.MultiLanguageString#getLocalizations <em>Localizations</em>}</li>
 *   <li>{@link org.montex.researchcv.MultiLanguageString#getBaseLanguage <em>Base Language</em>}</li>
 * </ul>
 *
 * @see org.montex.researchcv.ResearchCVPackage#getMultiLanguageString()
 * @model
 * @generated
 */
public interface MultiLanguageString extends EObject {
	/**
	 * Returns the value of the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Text</em>' attribute.
	 * @see org.montex.researchcv.ResearchCVPackage#getMultiLanguageString_Text()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL derivation='\n                                                 let first : LocalizedString = localizations-&gt;first() in\n                                            \n                                                if(first.oclIsInvalid()) then \n                                                    \'???\'\n                                                else first.text\n                                                endif'"
	 * @generated
	 */
	String getText();

	/**
	 * Returns the value of the '<em><b>Localizations</b></em>' containment reference list.
	 * The list contents are of type {@link org.montex.researchcv.LocalizedString}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Localizations</em>' containment reference list.
	 * @see org.montex.researchcv.ResearchCVPackage#getMultiLanguageString_Localizations()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<LocalizedString> getLocalizations();

	/**
	 * Returns the value of the '<em><b>Base Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Language</em>' attribute.
	 * @see org.montex.researchcv.ResearchCVPackage#getMultiLanguageString_BaseLanguage()
	 * @model dataType="org.montex.researchcv.LanguageCode" required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL derivation=' localizations-&gt;first().language'"
	 * @generated
	 */
	Locale getBaseLanguage();

} // MultiLanguageString
