/**
 */
package org.montex.researchcv;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Publication</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.Publication#getCitekey <em>Citekey</em>}</li>
 *   <li>{@link org.montex.researchcv.Publication#getTitle <em>Title</em>}</li>
 *   <li>{@link org.montex.researchcv.Publication#getAuthors <em>Authors</em>}</li>
 *   <li>{@link org.montex.researchcv.Publication#getURL <em>URL</em>}</li>
 *   <li>{@link org.montex.researchcv.Publication#isPublished <em>Published</em>}</li>
 *   <li>{@link org.montex.researchcv.Publication#isOpenAccess <em>Open Access</em>}</li>
 *   <li>{@link org.montex.researchcv.Publication#getDate <em>Date</em>}</li>
 *   <li>{@link org.montex.researchcv.Publication#getDOI <em>DOI</em>}</li>
 *   <li>{@link org.montex.researchcv.Publication#getAbstract <em>Abstract</em>}</li>
 *   <li>{@link org.montex.researchcv.Publication#isWithAuthorVersion <em>With Author Version</em>}</li>
 *   <li>{@link org.montex.researchcv.Publication#getNotes <em>Notes</em>}</li>
 *   <li>{@link org.montex.researchcv.Publication#getRelatedProjects <em>Related Projects</em>}</li>
 * </ul>
 *
 * @see org.montex.researchcv.ResearchCVPackage#getPublication()
 * @model abstract="true"
 * @generated
 */
public interface Publication extends EObject {
	/**
	 * Returns the value of the '<em><b>Citekey</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Citekey</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Citekey</em>' attribute.
	 * @see #setCitekey(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getPublication_Citekey()
	 * @model id="true" required="true"
	 * @generated
	 */
	String getCitekey();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Publication#getCitekey <em>Citekey</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Citekey</em>' attribute.
	 * @see #getCitekey()
	 * @generated
	 */
	void setCitekey(String value);

	/**
	 * Returns the value of the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Title</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Title</em>' attribute.
	 * @see #setTitle(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getPublication_Title()
	 * @model required="true"
	 * @generated
	 */
	String getTitle();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Publication#getTitle <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Title</em>' attribute.
	 * @see #getTitle()
	 * @generated
	 */
	void setTitle(String value);

	/**
	 * Returns the value of the '<em><b>Authors</b></em>' reference list.
	 * The list contents are of type {@link org.montex.researchcv.Person}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Authors</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Authors</em>' reference list.
	 * @see org.montex.researchcv.ResearchCVPackage#getPublication_Authors()
	 * @model required="true"
	 * @generated
	 */
	EList<Person> getAuthors();

	/**
	 * Returns the value of the '<em><b>URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>URL</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>URL</em>' attribute.
	 * @see #setURL(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getPublication_URL()
	 * @model
	 * @generated
	 */
	String getURL();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Publication#getURL <em>URL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>URL</em>' attribute.
	 * @see #getURL()
	 * @generated
	 */
	void setURL(String value);

	/**
	 * Returns the value of the '<em><b>Published</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Published</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Published</em>' attribute.
	 * @see #setPublished(boolean)
	 * @see org.montex.researchcv.ResearchCVPackage#getPublication_Published()
	 * @model default="true" required="true"
	 * @generated
	 */
	boolean isPublished();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Publication#isPublished <em>Published</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Published</em>' attribute.
	 * @see #isPublished()
	 * @generated
	 */
	void setPublished(boolean value);

	/**
	 * Returns the value of the '<em><b>Open Access</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Open Access</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Open Access</em>' attribute.
	 * @see #setOpenAccess(boolean)
	 * @see org.montex.researchcv.ResearchCVPackage#getPublication_OpenAccess()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isOpenAccess();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Publication#isOpenAccess <em>Open Access</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Open Access</em>' attribute.
	 * @see #isOpenAccess()
	 * @generated
	 */
	void setOpenAccess(boolean value);

	/**
	 * Returns the value of the '<em><b>Date</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Date</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Date</em>' containment reference.
	 * @see #setDate(PublishedDate)
	 * @see org.montex.researchcv.ResearchCVPackage#getPublication_Date()
	 * @model containment="true" required="true"
	 * @generated
	 */
	PublishedDate getDate();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Publication#getDate <em>Date</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Date</em>' containment reference.
	 * @see #getDate()
	 * @generated
	 */
	void setDate(PublishedDate value);

	/**
	 * Returns the value of the '<em><b>DOI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>DOI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>DOI</em>' attribute.
	 * @see #setDOI(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getPublication_DOI()
	 * @model
	 * @generated
	 */
	String getDOI();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Publication#getDOI <em>DOI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>DOI</em>' attribute.
	 * @see #getDOI()
	 * @generated
	 */
	void setDOI(String value);

	/**
	 * Returns the value of the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abstract</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abstract</em>' attribute.
	 * @see #setAbstract(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getPublication_Abstract()
	 * @model
	 * @generated
	 */
	String getAbstract();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Publication#getAbstract <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abstract</em>' attribute.
	 * @see #getAbstract()
	 * @generated
	 */
	void setAbstract(String value);

	/**
	 * Returns the value of the '<em><b>With Author Version</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>With Author Version</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>With Author Version</em>' attribute.
	 * @see #setWithAuthorVersion(boolean)
	 * @see org.montex.researchcv.ResearchCVPackage#getPublication_WithAuthorVersion()
	 * @model default="true" required="true"
	 * @generated
	 */
	boolean isWithAuthorVersion();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Publication#isWithAuthorVersion <em>With Author Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>With Author Version</em>' attribute.
	 * @see #isWithAuthorVersion()
	 * @generated
	 */
	void setWithAuthorVersion(boolean value);

	/**
	 * Returns the value of the '<em><b>Notes</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Notes</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Notes</em>' attribute list.
	 * @see org.montex.researchcv.ResearchCVPackage#getPublication_Notes()
	 * @model
	 * @generated
	 */
	EList<String> getNotes();

	/**
	 * Returns the value of the '<em><b>Related Projects</b></em>' reference list.
	 * The list contents are of type {@link org.montex.researchcv.Grant}.
	 * It is bidirectional and its opposite is '{@link org.montex.researchcv.Grant#getRelatedPublications <em>Related Publications</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Related Projects</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Related Projects</em>' reference list.
	 * @see org.montex.researchcv.ResearchCVPackage#getPublication_RelatedProjects()
	 * @see org.montex.researchcv.Grant#getRelatedPublications
	 * @model opposite="relatedPublications"
	 * @generated
	 */
	EList<Grant> getRelatedProjects();

} // Publication
