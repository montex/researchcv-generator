/**
 */
package org.montex.researchcv;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Institution Registry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.InstitutionRegistry#getInstitutions <em>Institutions</em>}</li>
 * </ul>
 *
 * @see org.montex.researchcv.ResearchCVPackage#getInstitutionRegistry()
 * @model
 * @generated
 */
public interface InstitutionRegistry extends RootElement {
	/**
	 * Returns the value of the '<em><b>Institutions</b></em>' containment reference list.
	 * The list contents are of type {@link org.montex.researchcv.Institution}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Institutions</em>' containment reference list.
	 * @see org.montex.researchcv.ResearchCVPackage#getInstitutionRegistry_Institutions()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Institution> getInstitutions();

} // InstitutionRegistry
