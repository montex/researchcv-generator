/**
 */
package org.montex.researchcv;

import java.util.Locale;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Localized String</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.LocalizedString#getText <em>Text</em>}</li>
 *   <li>{@link org.montex.researchcv.LocalizedString#getLanguage <em>Language</em>}</li>
 * </ul>
 *
 * @see org.montex.researchcv.ResearchCVPackage#getLocalizedString()
 * @model
 * @generated
 */
public interface LocalizedString extends EObject {
	/**
	 * Returns the value of the '<em><b>Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Language</em>' attribute.
	 * @see #setLanguage(Locale)
	 * @see org.montex.researchcv.ResearchCVPackage#getLocalizedString_Language()
	 * @model dataType="org.montex.researchcv.LanguageCode" required="true"
	 * @generated
	 */
	Locale getLanguage();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.LocalizedString#getLanguage <em>Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Language</em>' attribute.
	 * @see #getLanguage()
	 * @generated
	 */
	void setLanguage(Locale value);

	/**
	 * Returns the value of the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Text</em>' attribute.
	 * @see #setText(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getLocalizedString_Text()
	 * @model required="true"
	 * @generated
	 */
	String getText();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.LocalizedString#getText <em>Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Text</em>' attribute.
	 * @see #getText()
	 * @generated
	 */
	void setText(String value);

} // LocalizedString
