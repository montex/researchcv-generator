/**
 */
package org.montex.researchcv;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Institution Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.InstitutionUnit#getName <em>Name</em>}</li>
 *   <li>{@link org.montex.researchcv.InstitutionUnit#getSubunits <em>Subunits</em>}</li>
 *   <li>{@link org.montex.researchcv.InstitutionUnit#getFullName <em>Full Name</em>}</li>
 * </ul>
 *
 * @see org.montex.researchcv.ResearchCVPackage#getInstitutionUnit()
 * @model
 * @generated
 */
public interface InstitutionUnit extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getInstitutionUnit_Name()
	 * @model id="true" required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.InstitutionUnit#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Subunits</b></em>' containment reference list.
	 * The list contents are of type {@link org.montex.researchcv.InstitutionUnit}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subunits</em>' containment reference list.
	 * @see org.montex.researchcv.ResearchCVPackage#getInstitutionUnit_Subunits()
	 * @model containment="true"
	 * @generated
	 */
	EList<InstitutionUnit> getSubunits();

	/**
	 * Returns the value of the '<em><b>Full Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Full Name</em>' containment reference.
	 * @see #setFullName(MultiLanguageString)
	 * @see org.montex.researchcv.ResearchCVPackage#getInstitutionUnit_FullName()
	 * @model containment="true" required="true"
	 * @generated
	 */
	MultiLanguageString getFullName();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.InstitutionUnit#getFullName <em>Full Name</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Full Name</em>' containment reference.
	 * @see #getFullName()
	 * @generated
	 */
	void setFullName(MultiLanguageString value);

} // InstitutionUnit
