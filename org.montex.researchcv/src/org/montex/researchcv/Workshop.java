/**
 */
package org.montex.researchcv;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Workshop</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.Workshop#getMainEventName <em>Main Event Name</em>}</li>
 *   <li>{@link org.montex.researchcv.Workshop#getMainEventShortName <em>Main Event Short Name</em>}</li>
 * </ul>
 *
 * @see org.montex.researchcv.ResearchCVPackage#getWorkshop()
 * @model
 * @generated
 */
public interface Workshop extends InProceedings {

	/**
	 * Returns the value of the '<em><b>Main Event Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Main Event Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Main Event Name</em>' attribute.
	 * @see #setMainEventName(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getWorkshop_MainEventName()
	 * @model
	 * @generated
	 */
	String getMainEventName();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Workshop#getMainEventName <em>Main Event Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Main Event Name</em>' attribute.
	 * @see #getMainEventName()
	 * @generated
	 */
	void setMainEventName(String value);

	/**
	 * Returns the value of the '<em><b>Main Event Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Main Event Short Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Main Event Short Name</em>' attribute.
	 * @see #setMainEventShortName(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getWorkshop_MainEventShortName()
	 * @model
	 * @generated
	 */
	String getMainEventShortName();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Workshop#getMainEventShortName <em>Main Event Short Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Main Event Short Name</em>' attribute.
	 * @see #getMainEventShortName()
	 * @generated
	 */
	void setMainEventShortName(String value);
} // Workshop
