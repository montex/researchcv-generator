/**
 */
package org.montex.researchcv;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Participation Kind</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.montex.researchcv.ResearchCVPackage#getParticipationKind()
 * @model
 * @generated
 */
public enum ParticipationKind implements Enumerator {
	/**
	 * The '<em><b>PARTICIPANT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PARTICIPANT_VALUE
	 * @generated
	 * @ordered
	 */
	PARTICIPANT(0, "PARTICIPANT", "PARTICIPANT"),

	/**
	 * The '<em><b>PARTNER PI</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PARTNER_PI_VALUE
	 * @generated
	 * @ordered
	 */
	PARTNER_PI(1, "PARTNER_PI", "PARTNER_PI"),

	/**
	 * The '<em><b>PROJECT PI</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PROJECT_PI_VALUE
	 * @generated
	 * @ordered
	 */
	PROJECT_PI(2, "PROJECT_PI", "PROJECT_PI");

	/**
	 * The '<em><b>PARTICIPANT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PARTICIPANT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int PARTICIPANT_VALUE = 0;

	/**
	 * The '<em><b>PARTNER PI</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PARTNER_PI
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int PARTNER_PI_VALUE = 1;

	/**
	 * The '<em><b>PROJECT PI</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PROJECT_PI
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int PROJECT_PI_VALUE = 2;

	/**
	 * An array of all the '<em><b>Participation Kind</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ParticipationKind[] VALUES_ARRAY =
		new ParticipationKind[] {
			PARTICIPANT,
			PARTNER_PI,
			PROJECT_PI,
		};

	/**
	 * A public read-only list of all the '<em><b>Participation Kind</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<ParticipationKind> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Participation Kind</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ParticipationKind get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ParticipationKind result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Participation Kind</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ParticipationKind getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ParticipationKind result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Participation Kind</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ParticipationKind get(int value) {
		switch (value) {
			case PARTICIPANT_VALUE: return PARTICIPANT;
			case PARTNER_PI_VALUE: return PARTNER_PI;
			case PROJECT_PI_VALUE: return PROJECT_PI;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ParticipationKind(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //ParticipationKind
