/**
 */
package org.montex.researchcv;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course Offering</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.CourseOffering#getGroups <em>Groups</em>}</li>
 *   <li>{@link org.montex.researchcv.CourseOffering#getSpecificName <em>Specific Name</em>}</li>
 *   <li>{@link org.montex.researchcv.CourseOffering#getCourse <em>Course</em>}</li>
 * </ul>
 *
 * @see org.montex.researchcv.ResearchCVPackage#getCourseOffering()
 * @model
 * @generated
 */
public interface CourseOffering extends EObject {
	/**
	 * Returns the value of the '<em><b>Groups</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Groups</em>' attribute list.
	 * @see org.montex.researchcv.ResearchCVPackage#getCourseOffering_Groups()
	 * @model default=""
	 * @generated
	 */
	EList<String> getGroups();

	/**
	 * Returns the value of the '<em><b>Specific Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specific Name</em>' attribute.
	 * @see #setSpecificName(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getCourseOffering_SpecificName()
	 * @model
	 * @generated
	 */
	String getSpecificName();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.CourseOffering#getSpecificName <em>Specific Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Specific Name</em>' attribute.
	 * @see #getSpecificName()
	 * @generated
	 */
	void setSpecificName(String value);

	/**
	 * Returns the value of the '<em><b>Course</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course</em>' reference.
	 * @see #setCourse(Course)
	 * @see org.montex.researchcv.ResearchCVPackage#getCourseOffering_Course()
	 * @model required="true"
	 * @generated
	 */
	Course getCourse();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.CourseOffering#getCourse <em>Course</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course</em>' reference.
	 * @see #getCourse()
	 * @generated
	 */
	void setCourse(Course value);

} // CourseOffering
