/**
 */
package org.montex.researchcv;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Supervision</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.Supervision#getDasteStart <em>Daste Start</em>}</li>
 *   <li>{@link org.montex.researchcv.Supervision#getDateEnd <em>Date End</em>}</li>
 *   <li>{@link org.montex.researchcv.Supervision#getLevel <em>Level</em>}</li>
 *   <li>{@link org.montex.researchcv.Supervision#getStudent <em>Student</em>}</li>
 *   <li>{@link org.montex.researchcv.Supervision#getProjectTitle <em>Project Title</em>}</li>
 *   <li>{@link org.montex.researchcv.Supervision#getSupervisors <em>Supervisors</em>}</li>
 *   <li>{@link org.montex.researchcv.Supervision#getInstitutions <em>Institutions</em>}</li>
 *   <li>{@link org.montex.researchcv.Supervision#getMainInstitution <em>Main Institution</em>}</li>
 *   <li>{@link org.montex.researchcv.Supervision#getMainSupervisor <em>Main Supervisor</em>}</li>
 *   <li>{@link org.montex.researchcv.Supervision#isCompleted <em>Completed</em>}</li>
 *   <li>{@link org.montex.researchcv.Supervision#getFinalWorkURL <em>Final Work URL</em>}</li>
 *   <li>{@link org.montex.researchcv.Supervision#getFinalWorkDOI <em>Final Work DOI</em>}</li>
 * </ul>
 *
 * @see org.montex.researchcv.ResearchCVPackage#getSupervision()
 * @model
 * @generated
 */
public interface Supervision extends EObject {
	/**
	 * Returns the value of the '<em><b>Daste Start</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Daste Start</em>' containment reference.
	 * @see #setDasteStart(PublishedDate)
	 * @see org.montex.researchcv.ResearchCVPackage#getSupervision_DasteStart()
	 * @model containment="true"
	 * @generated
	 */
	PublishedDate getDasteStart();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Supervision#getDasteStart <em>Daste Start</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Daste Start</em>' containment reference.
	 * @see #getDasteStart()
	 * @generated
	 */
	void setDasteStart(PublishedDate value);

	/**
	 * Returns the value of the '<em><b>Date End</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Date End</em>' containment reference.
	 * @see #setDateEnd(PublishedDate)
	 * @see org.montex.researchcv.ResearchCVPackage#getSupervision_DateEnd()
	 * @model containment="true"
	 * @generated
	 */
	PublishedDate getDateEnd();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Supervision#getDateEnd <em>Date End</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Date End</em>' containment reference.
	 * @see #getDateEnd()
	 * @generated
	 */
	void setDateEnd(PublishedDate value);

	/**
	 * Returns the value of the '<em><b>Level</b></em>' attribute.
	 * The default value is <code>"MASTER"</code>.
	 * The literals are from the enumeration {@link org.montex.researchcv.SupervisionKind}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Level</em>' attribute.
	 * @see org.montex.researchcv.SupervisionKind
	 * @see #setLevel(SupervisionKind)
	 * @see org.montex.researchcv.ResearchCVPackage#getSupervision_Level()
	 * @model default="MASTER" required="true"
	 * @generated
	 */
	SupervisionKind getLevel();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Supervision#getLevel <em>Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Level</em>' attribute.
	 * @see org.montex.researchcv.SupervisionKind
	 * @see #getLevel()
	 * @generated
	 */
	void setLevel(SupervisionKind value);

	/**
	 * Returns the value of the '<em><b>Student</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Student</em>' reference.
	 * @see #setStudent(Person)
	 * @see org.montex.researchcv.ResearchCVPackage#getSupervision_Student()
	 * @model required="true"
	 * @generated
	 */
	Person getStudent();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Supervision#getStudent <em>Student</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Student</em>' reference.
	 * @see #getStudent()
	 * @generated
	 */
	void setStudent(Person value);

	/**
	 * Returns the value of the '<em><b>Project Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Project Title</em>' attribute.
	 * @see #setProjectTitle(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getSupervision_ProjectTitle()
	 * @model
	 * @generated
	 */
	String getProjectTitle();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Supervision#getProjectTitle <em>Project Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Project Title</em>' attribute.
	 * @see #getProjectTitle()
	 * @generated
	 */
	void setProjectTitle(String value);

	/**
	 * Returns the value of the '<em><b>Supervisors</b></em>' reference list.
	 * The list contents are of type {@link org.montex.researchcv.Person}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Supervisors</em>' reference list.
	 * @see org.montex.researchcv.ResearchCVPackage#getSupervision_Supervisors()
	 * @model required="true"
	 * @generated
	 */
	EList<Person> getSupervisors();

	/**
	 * Returns the value of the '<em><b>Institutions</b></em>' reference list.
	 * The list contents are of type {@link org.montex.researchcv.Institution}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Institutions</em>' reference list.
	 * @see org.montex.researchcv.ResearchCVPackage#getSupervision_Institutions()
	 * @model required="true"
	 * @generated
	 */
	EList<Institution> getInstitutions();

	/**
	 * Returns the value of the '<em><b>Main Institution</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Main Institution</em>' reference.
	 * @see org.montex.researchcv.ResearchCVPackage#getSupervision_MainInstitution()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL derivation='institutions-&gt;first()'"
	 * @generated
	 */
	Institution getMainInstitution();

	/**
	 * Returns the value of the '<em><b>Main Supervisor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Main Supervisor</em>' reference.
	 * @see org.montex.researchcv.ResearchCVPackage#getSupervision_MainSupervisor()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL derivation='supervisors-&gt;first()'"
	 * @generated
	 */
	Person getMainSupervisor();

	/**
	 * Returns the value of the '<em><b>Completed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Completed</em>' attribute.
	 * @see org.montex.researchcv.ResearchCVPackage#getSupervision_Completed()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL derivation='if dateEnd.oclIsUndefined() then false else not dateEnd.oclIsInvalid() endif'"
	 * @generated
	 */
	boolean isCompleted();

	/**
	 * Returns the value of the '<em><b>Final Work URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Final Work URL</em>' attribute.
	 * @see #setFinalWorkURL(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getSupervision_FinalWorkURL()
	 * @model
	 * @generated
	 */
	String getFinalWorkURL();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Supervision#getFinalWorkURL <em>Final Work URL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Final Work URL</em>' attribute.
	 * @see #getFinalWorkURL()
	 * @generated
	 */
	void setFinalWorkURL(String value);

	/**
	 * Returns the value of the '<em><b>Final Work DOI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Final Work DOI</em>' attribute.
	 * @see #setFinalWorkDOI(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getSupervision_FinalWorkDOI()
	 * @model
	 * @generated
	 */
	String getFinalWorkDOI();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Supervision#getFinalWorkDOI <em>Final Work DOI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Final Work DOI</em>' attribute.
	 * @see #getFinalWorkDOI()
	 * @generated
	 */
	void setFinalWorkDOI(String value);

} // Supervision
