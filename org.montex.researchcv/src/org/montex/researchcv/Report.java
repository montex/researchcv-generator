/**
 */
package org.montex.researchcv;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Report</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.Report#getReportNumber <em>Report Number</em>}</li>
 *   <li>{@link org.montex.researchcv.Report#getInstitution <em>Institution</em>}</li>
 * </ul>
 *
 * @see org.montex.researchcv.ResearchCVPackage#getReport()
 * @model
 * @generated
 */
public interface Report extends Publication {

	/**
	 * Returns the value of the '<em><b>Report Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Report Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Report Number</em>' attribute.
	 * @see #setReportNumber(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getReport_ReportNumber()
	 * @model
	 * @generated
	 */
	String getReportNumber();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Report#getReportNumber <em>Report Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Report Number</em>' attribute.
	 * @see #getReportNumber()
	 * @generated
	 */
	void setReportNumber(String value);

	/**
	 * Returns the value of the '<em><b>Institution</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Institution</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Institution</em>' attribute.
	 * @see #setInstitution(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getReport_Institution()
	 * @model
	 * @generated
	 */
	String getInstitution();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Report#getInstitution <em>Institution</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Institution</em>' attribute.
	 * @see #getInstitution()
	 * @generated
	 */
	void setInstitution(String value);
} // Report
