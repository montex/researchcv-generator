/**
 */
package org.montex.researchcv;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Serial Number</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.SerialNumber#getValue <em>Value</em>}</li>
 *   <li>{@link org.montex.researchcv.SerialNumber#getKind <em>Kind</em>}</li>
 * </ul>
 *
 * @see org.montex.researchcv.ResearchCVPackage#getSerialNumber()
 * @model
 * @generated
 */
public interface SerialNumber extends EObject {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getSerialNumber_Value()
	 * @model required="true"
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.SerialNumber#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Returns the value of the '<em><b>Kind</b></em>' attribute.
	 * The default value is <code>"UNSPECIFIED"</code>.
	 * The literals are from the enumeration {@link org.montex.researchcv.SerialNumberKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Kind</em>' attribute.
	 * @see org.montex.researchcv.SerialNumberKind
	 * @see #setKind(SerialNumberKind)
	 * @see org.montex.researchcv.ResearchCVPackage#getSerialNumber_Kind()
	 * @model default="UNSPECIFIED" required="true"
	 * @generated
	 */
	SerialNumberKind getKind();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.SerialNumber#getKind <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Kind</em>' attribute.
	 * @see org.montex.researchcv.SerialNumberKind
	 * @see #getKind()
	 * @generated
	 */
	void setKind(SerialNumberKind value);

} // SerialNumber
