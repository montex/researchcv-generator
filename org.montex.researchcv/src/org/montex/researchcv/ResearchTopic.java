/**
 */
package org.montex.researchcv;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Research Topic</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.ResearchTopic#getTitle <em>Title</em>}</li>
 *   <li>{@link org.montex.researchcv.ResearchTopic#getDescription <em>Description</em>}</li>
 *   <li>{@link org.montex.researchcv.ResearchTopic#getRelatedPapers <em>Related Papers</em>}</li>
 * </ul>
 *
 * @see org.montex.researchcv.ResearchCVPackage#getResearchTopic()
 * @model
 * @generated
 */
public interface ResearchTopic extends EObject {
	/**
	 * Returns the value of the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Title</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Title</em>' attribute.
	 * @see #setTitle(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getResearchTopic_Title()
	 * @model required="true"
	 * @generated
	 */
	String getTitle();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.ResearchTopic#getTitle <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Title</em>' attribute.
	 * @see #getTitle()
	 * @generated
	 */
	void setTitle(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getResearchTopic_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.ResearchTopic#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Related Papers</b></em>' reference list.
	 * The list contents are of type {@link org.montex.researchcv.Publication}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Related Papers</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Related Papers</em>' reference list.
	 * @see org.montex.researchcv.ResearchCVPackage#getResearchTopic_RelatedPapers()
	 * @model
	 * @generated
	 */
	EList<Publication> getRelatedPapers();

} // ResearchTopic
