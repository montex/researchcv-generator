/**
 */
package org.montex.researchcv;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Contact</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.Contact#getKind <em>Kind</em>}</li>
 * </ul>
 *
 * @see org.montex.researchcv.ResearchCVPackage#getContact()
 * @model abstract="true"
 * @generated
 */
public interface Contact extends EObject {
	/**
	 * Returns the value of the '<em><b>Kind</b></em>' attribute.
	 * The default value is <code>"PRIVATE"</code>.
	 * The literals are from the enumeration {@link org.montex.researchcv.ContactKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Kind</em>' attribute.
	 * @see org.montex.researchcv.ContactKind
	 * @see #setKind(ContactKind)
	 * @see org.montex.researchcv.ResearchCVPackage#getContact_Kind()
	 * @model default="PRIVATE" required="true"
	 * @generated
	 */
	ContactKind getKind();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Contact#getKind <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Kind</em>' attribute.
	 * @see org.montex.researchcv.ContactKind
	 * @see #getKind()
	 * @generated
	 */
	void setKind(ContactKind value);

} // Contact
