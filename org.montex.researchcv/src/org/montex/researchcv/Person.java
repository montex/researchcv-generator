/**
 */
package org.montex.researchcv;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Person</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.Person#getFirstNames <em>First Names</em>}</li>
 *   <li>{@link org.montex.researchcv.Person#getLastNames <em>Last Names</em>}</li>
 *   <li>{@link org.montex.researchcv.Person#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see org.montex.researchcv.ResearchCVPackage#getPerson()
 * @model
 * @generated
 */
public interface Person extends EObject {
	/**
	 * Returns the value of the '<em><b>First Names</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First Names</em>' attribute list.
	 * @see org.montex.researchcv.ResearchCVPackage#getPerson_FirstNames()
	 * @model unique="false" required="true"
	 * @generated
	 */
	EList<String> getFirstNames();

	/**
	 * Returns the value of the '<em><b>Last Names</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Names</em>' attribute list.
	 * @see org.montex.researchcv.ResearchCVPackage#getPerson_LastNames()
	 * @model unique="false" required="true"
	 * @generated
	 */
	EList<String> getLastNames();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getPerson_Name()
	 * @model id="true" required="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL derivation='lastNames-&gt;last() + \', \' + firstNames-&gt;first()'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Person#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // Person
