/**
 */
package org.montex.researchcv;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Root Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.montex.researchcv.ResearchCVPackage#getRootElement()
 * @model abstract="true"
 * @generated
 */
public interface RootElement extends EObject {
} // RootElement
