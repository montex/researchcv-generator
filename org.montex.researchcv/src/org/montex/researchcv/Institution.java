/**
 */
package org.montex.researchcv;

import java.util.Locale;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Institution</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.Institution#getName <em>Name</em>}</li>
 *   <li>{@link org.montex.researchcv.Institution#getUnits <em>Units</em>}</li>
 *   <li>{@link org.montex.researchcv.Institution#getCountry <em>Country</em>}</li>
 *   <li>{@link org.montex.researchcv.Institution#getFullName <em>Full Name</em>}</li>
 * </ul>
 *
 * @see org.montex.researchcv.ResearchCVPackage#getInstitution()
 * @model
 * @generated
 */
public interface Institution extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getInstitution_Name()
	 * @model id="true" required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Institution#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Units</b></em>' containment reference list.
	 * The list contents are of type {@link org.montex.researchcv.Institution}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Units</em>' containment reference list.
	 * @see org.montex.researchcv.ResearchCVPackage#getInstitution_Units()
	 * @model containment="true"
	 * @generated
	 */
	EList<Institution> getUnits();

	/**
	 * Returns the value of the '<em><b>Country</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Country</em>' attribute.
	 * @see #setCountry(Locale)
	 * @see org.montex.researchcv.ResearchCVPackage#getInstitution_Country()
	 * @model dataType="org.montex.researchcv.CountryCode"
	 * @generated
	 */
	Locale getCountry();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Institution#getCountry <em>Country</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Country</em>' attribute.
	 * @see #getCountry()
	 * @generated
	 */
	void setCountry(Locale value);

	/**
	 * Returns the value of the '<em><b>Full Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Full Name</em>' containment reference.
	 * @see #setFullName(MultiLanguageString)
	 * @see org.montex.researchcv.ResearchCVPackage#getInstitution_FullName()
	 * @model containment="true" required="true"
	 * @generated
	 */
	MultiLanguageString getFullName();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Institution#getFullName <em>Full Name</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Full Name</em>' containment reference.
	 * @see #getFullName()
	 * @generated
	 */
	void setFullName(MultiLanguageString value);

} // Institution
