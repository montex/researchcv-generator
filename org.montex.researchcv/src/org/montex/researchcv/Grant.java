/**
 */
package org.montex.researchcv;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Grant</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.Grant#getRelatedPublications <em>Related Publications</em>}</li>
 *   <li>{@link org.montex.researchcv.Grant#getURL <em>URL</em>}</li>
 *   <li>{@link org.montex.researchcv.Grant#getAgency <em>Agency</em>}</li>
 *   <li>{@link org.montex.researchcv.Grant#getProgram <em>Program</em>}</li>
 *   <li>{@link org.montex.researchcv.Grant#getCall <em>Call</em>}</li>
 *   <li>{@link org.montex.researchcv.Grant#getNumber <em>Number</em>}</li>
 *   <li>{@link org.montex.researchcv.Grant#getBudget <em>Budget</em>}</li>
 *   <li>{@link org.montex.researchcv.Grant#getStartDate <em>Start Date</em>}</li>
 *   <li>{@link org.montex.researchcv.Grant#getEndDate <em>End Date</em>}</li>
 *   <li>{@link org.montex.researchcv.Grant#getGrantID <em>Grant ID</em>}</li>
 *   <li>{@link org.montex.researchcv.Grant#getRole <em>Role</em>}</li>
 * </ul>
 *
 * @see org.montex.researchcv.ResearchCVPackage#getGrant()
 * @model abstract="true"
 * @generated
 */
public interface Grant extends EObject {

	/**
	 * Returns the value of the '<em><b>Related Publications</b></em>' reference list.
	 * The list contents are of type {@link org.montex.researchcv.Publication}.
	 * It is bidirectional and its opposite is '{@link org.montex.researchcv.Publication#getRelatedProjects <em>Related Projects</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Related Publications</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Related Publications</em>' reference list.
	 * @see org.montex.researchcv.ResearchCVPackage#getGrant_RelatedPublications()
	 * @see org.montex.researchcv.Publication#getRelatedProjects
	 * @model opposite="relatedProjects"
	 * @generated
	 */
	EList<Publication> getRelatedPublications();

	/**
	 * Returns the value of the '<em><b>URL</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>URL</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>URL</em>' attribute list.
	 * @see org.montex.researchcv.ResearchCVPackage#getGrant_URL()
	 * @model
	 * @generated
	 */
	EList<String> getURL();

	/**
	 * Returns the value of the '<em><b>Agency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Agency</em>' attribute.
	 * @see #setAgency(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getGrant_Agency()
	 * @model required="true"
	 * @generated
	 */
	String getAgency();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Grant#getAgency <em>Agency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Agency</em>' attribute.
	 * @see #getAgency()
	 * @generated
	 */
	void setAgency(String value);

	/**
	 * Returns the value of the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number</em>' attribute.
	 * @see #setNumber(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getGrant_Number()
	 * @model required="true"
	 * @generated
	 */
	String getNumber();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Grant#getNumber <em>Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number</em>' attribute.
	 * @see #getNumber()
	 * @generated
	 */
	void setNumber(String value);

	/**
	 * Returns the value of the '<em><b>Program</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Program</em>' attribute.
	 * @see #setProgram(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getGrant_Program()
	 * @model
	 * @generated
	 */
	String getProgram();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Grant#getProgram <em>Program</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Program</em>' attribute.
	 * @see #getProgram()
	 * @generated
	 */
	void setProgram(String value);

	/**
	 * Returns the value of the '<em><b>Call</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Call</em>' attribute.
	 * @see #setCall(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getGrant_Call()
	 * @model
	 * @generated
	 */
	String getCall();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Grant#getCall <em>Call</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Call</em>' attribute.
	 * @see #getCall()
	 * @generated
	 */
	void setCall(String value);

	/**
	 * Returns the value of the '<em><b>Grant ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Grant ID</em>' attribute.
	 * @see org.montex.researchcv.ResearchCVPackage#getGrant_GrantID()
	 * @model id="true" required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL derivation='agency + \n                        if program.oclIsUndefined() then \'\' else \'-\' + program endif +\n                        if call.oclIsUndefined() then \'\' else \'-\' + call endif + \n                        \'-\' + number'"
	 * @generated
	 */
	String getGrantID();

	/**
	 * Returns the value of the '<em><b>Role</b></em>' attribute.
	 * The literals are from the enumeration {@link org.montex.researchcv.ParticipationKind}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role</em>' attribute.
	 * @see org.montex.researchcv.ParticipationKind
	 * @see #setRole(ParticipationKind)
	 * @see org.montex.researchcv.ResearchCVPackage#getGrant_Role()
	 * @model required="true"
	 * @generated
	 */
	ParticipationKind getRole();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Grant#getRole <em>Role</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Role</em>' attribute.
	 * @see org.montex.researchcv.ParticipationKind
	 * @see #getRole()
	 * @generated
	 */
	void setRole(ParticipationKind value);

	/**
	 * Returns the value of the '<em><b>Budget</b></em>' containment reference list.
	 * The list contents are of type {@link org.montex.researchcv.Budget}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Budget</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Budget</em>' containment reference list.
	 * @see org.montex.researchcv.ResearchCVPackage#getGrant_Budget()
	 * @model containment="true"
	 * @generated
	 */
	EList<Budget> getBudget();

	/**
	 * Returns the value of the '<em><b>Start Date</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Date</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Date</em>' containment reference.
	 * @see #setStartDate(PublishedDate)
	 * @see org.montex.researchcv.ResearchCVPackage#getGrant_StartDate()
	 * @model containment="true" required="true"
	 * @generated
	 */
	PublishedDate getStartDate();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Grant#getStartDate <em>Start Date</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Date</em>' containment reference.
	 * @see #getStartDate()
	 * @generated
	 */
	void setStartDate(PublishedDate value);

	/**
	 * Returns the value of the '<em><b>End Date</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Date</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Date</em>' containment reference.
	 * @see #setEndDate(PublishedDate)
	 * @see org.montex.researchcv.ResearchCVPackage#getGrant_EndDate()
	 * @model containment="true" required="true"
	 * @generated
	 */
	PublishedDate getEndDate();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Grant#getEndDate <em>End Date</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Date</em>' containment reference.
	 * @see #getEndDate()
	 * @generated
	 */
	void setEndDate(PublishedDate value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true"
	 * @generated
	 */
	boolean isActive();
} // Grant
