/**
 */
package org.montex.researchcv;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Miscellaneous</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.montex.researchcv.ResearchCVPackage#getMiscellaneous()
 * @model
 * @generated
 */
public interface Miscellaneous extends Publication {

} // Miscellaneous
