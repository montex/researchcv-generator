/**
 */
package org.montex.researchcv;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>In Proceedings</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.InProceedings#getEventEndDate <em>Event End Date</em>}</li>
 *   <li>{@link org.montex.researchcv.InProceedings#getEventName <em>Event Name</em>}</li>
 *   <li>{@link org.montex.researchcv.InProceedings#getVenue <em>Venue</em>}</li>
 *   <li>{@link org.montex.researchcv.InProceedings#getEventShortName <em>Event Short Name</em>}</li>
 *   <li>{@link org.montex.researchcv.InProceedings#getEventStartDate <em>Event Start Date</em>}</li>
 *   <li>{@link org.montex.researchcv.InProceedings#getTrackName <em>Track Name</em>}</li>
 *   <li>{@link org.montex.researchcv.InProceedings#getTrackShortName <em>Track Short Name</em>}</li>
 *   <li>{@link org.montex.researchcv.InProceedings#isShortPaper <em>Short Paper</em>}</li>
 *   <li>{@link org.montex.researchcv.InProceedings#isToolPaper <em>Tool Paper</em>}</li>
 *   <li>{@link org.montex.researchcv.InProceedings#isInvitedPaper <em>Invited Paper</em>}</li>
 * </ul>
 *
 * @see org.montex.researchcv.ResearchCVPackage#getInProceedings()
 * @model abstract="true"
 * @generated
 */
public interface InProceedings extends InCollection {
	/**
	 * Returns the value of the '<em><b>Event End Date</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event End Date</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event End Date</em>' containment reference.
	 * @see #setEventEndDate(PublishedDate)
	 * @see org.montex.researchcv.ResearchCVPackage#getInProceedings_EventEndDate()
	 * @model containment="true"
	 * @generated
	 */
	PublishedDate getEventEndDate();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.InProceedings#getEventEndDate <em>Event End Date</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event End Date</em>' containment reference.
	 * @see #getEventEndDate()
	 * @generated
	 */
	void setEventEndDate(PublishedDate value);

	/**
	 * Returns the value of the '<em><b>Event Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event Name</em>' attribute.
	 * @see #setEventName(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getInProceedings_EventName()
	 * @model required="true"
	 * @generated
	 */
	String getEventName();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.InProceedings#getEventName <em>Event Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event Name</em>' attribute.
	 * @see #getEventName()
	 * @generated
	 */
	void setEventName(String value);

	/**
	 * Returns the value of the '<em><b>Venue</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Venue</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Venue</em>' attribute.
	 * @see #setVenue(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getInProceedings_Venue()
	 * @model
	 * @generated
	 */
	String getVenue();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.InProceedings#getVenue <em>Venue</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Venue</em>' attribute.
	 * @see #getVenue()
	 * @generated
	 */
	void setVenue(String value);

	/**
	 * Returns the value of the '<em><b>Event Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event Short Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event Short Name</em>' attribute.
	 * @see #setEventShortName(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getInProceedings_EventShortName()
	 * @model required="true"
	 * @generated
	 */
	String getEventShortName();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.InProceedings#getEventShortName <em>Event Short Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event Short Name</em>' attribute.
	 * @see #getEventShortName()
	 * @generated
	 */
	void setEventShortName(String value);

	/**
	 * Returns the value of the '<em><b>Event Start Date</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event Start Date</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event Start Date</em>' reference.
	 * @see org.montex.researchcv.ResearchCVPackage#getInProceedings_EventStartDate()
	 * @model required="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL derivation='date'"
	 * @generated
	 */
	PublishedDate getEventStartDate();

	/**
	 * Returns the value of the '<em><b>Track Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Track Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Track Name</em>' attribute.
	 * @see #setTrackName(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getInProceedings_TrackName()
	 * @model
	 * @generated
	 */
	String getTrackName();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.InProceedings#getTrackName <em>Track Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Track Name</em>' attribute.
	 * @see #getTrackName()
	 * @generated
	 */
	void setTrackName(String value);

	/**
	 * Returns the value of the '<em><b>Track Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Track Short Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Track Short Name</em>' attribute.
	 * @see #setTrackShortName(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getInProceedings_TrackShortName()
	 * @model
	 * @generated
	 */
	String getTrackShortName();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.InProceedings#getTrackShortName <em>Track Short Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Track Short Name</em>' attribute.
	 * @see #getTrackShortName()
	 * @generated
	 */
	void setTrackShortName(String value);

	/**
	 * Returns the value of the '<em><b>Short Paper</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Short Paper</em>' attribute.
	 * @see #setShortPaper(boolean)
	 * @see org.montex.researchcv.ResearchCVPackage#getInProceedings_ShortPaper()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isShortPaper();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.InProceedings#isShortPaper <em>Short Paper</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Short Paper</em>' attribute.
	 * @see #isShortPaper()
	 * @generated
	 */
	void setShortPaper(boolean value);

	/**
	 * Returns the value of the '<em><b>Tool Paper</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tool Paper</em>' attribute.
	 * @see #setToolPaper(boolean)
	 * @see org.montex.researchcv.ResearchCVPackage#getInProceedings_ToolPaper()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isToolPaper();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.InProceedings#isToolPaper <em>Tool Paper</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tool Paper</em>' attribute.
	 * @see #isToolPaper()
	 * @generated
	 */
	void setToolPaper(boolean value);

	/**
	 * Returns the value of the '<em><b>Invited Paper</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Invited Paper</em>' attribute.
	 * @see #setInvitedPaper(boolean)
	 * @see org.montex.researchcv.ResearchCVPackage#getInProceedings_InvitedPaper()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isInvitedPaper();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.InProceedings#isInvitedPaper <em>Invited Paper</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Invited Paper</em>' attribute.
	 * @see #isInvitedPaper()
	 * @generated
	 */
	void setInvitedPaper(boolean value);

} // InProceedings
