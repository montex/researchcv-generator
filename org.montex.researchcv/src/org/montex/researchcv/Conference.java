/**
 */
package org.montex.researchcv;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Conference</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.montex.researchcv.ResearchCVPackage#getConference()
 * @model
 * @generated
 */
public interface Conference extends InProceedings {
} // Conference
