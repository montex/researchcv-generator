/**
 */
package org.montex.researchcv;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Person Registry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.PersonRegistry#getPerson <em>Person</em>}</li>
 * </ul>
 *
 * @see org.montex.researchcv.ResearchCVPackage#getPersonRegistry()
 * @model
 * @generated
 */
public interface PersonRegistry extends RootElement {
	/**
	 * Returns the value of the '<em><b>Person</b></em>' containment reference list.
	 * The list contents are of type {@link org.montex.researchcv.Person}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Person</em>' containment reference list.
	 * @see org.montex.researchcv.ResearchCVPackage#getPersonRegistry_Person()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Person> getPerson();

} // PersonRegistry
