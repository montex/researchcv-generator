/**
 */
package org.montex.researchcv;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>In Collection</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.InCollection#getBooktitle <em>Booktitle</em>}</li>
 *   <li>{@link org.montex.researchcv.InCollection#getBookeditors <em>Bookeditors</em>}</li>
 *   <li>{@link org.montex.researchcv.InCollection#getPublisher <em>Publisher</em>}</li>
 *   <li>{@link org.montex.researchcv.InCollection#getSeries <em>Series</em>}</li>
 *   <li>{@link org.montex.researchcv.InCollection#getVolume <em>Volume</em>}</li>
 *   <li>{@link org.montex.researchcv.InCollection#getFirstPage <em>First Page</em>}</li>
 *   <li>{@link org.montex.researchcv.InCollection#getLastPage <em>Last Page</em>}</li>
 *   <li>{@link org.montex.researchcv.InCollection#getISBN <em>ISBN</em>}</li>
 * </ul>
 *
 * @see org.montex.researchcv.ResearchCVPackage#getInCollection()
 * @model abstract="true"
 * @generated
 */
public interface InCollection extends Publication {
	/**
	 * Returns the value of the '<em><b>Booktitle</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Booktitle</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Booktitle</em>' attribute.
	 * @see #setBooktitle(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getInCollection_Booktitle()
	 * @model required="true"
	 * @generated
	 */
	String getBooktitle();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.InCollection#getBooktitle <em>Booktitle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Booktitle</em>' attribute.
	 * @see #getBooktitle()
	 * @generated
	 */
	void setBooktitle(String value);

	/**
	 * Returns the value of the '<em><b>Bookeditors</b></em>' reference list.
	 * The list contents are of type {@link org.montex.researchcv.Person}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bookeditors</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bookeditors</em>' reference list.
	 * @see org.montex.researchcv.ResearchCVPackage#getInCollection_Bookeditors()
	 * @model
	 * @generated
	 */
	EList<Person> getBookeditors();

	/**
	 * Returns the value of the '<em><b>Publisher</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Publisher</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Publisher</em>' attribute.
	 * @see #setPublisher(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getInCollection_Publisher()
	 * @model
	 * @generated
	 */
	String getPublisher();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.InCollection#getPublisher <em>Publisher</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Publisher</em>' attribute.
	 * @see #getPublisher()
	 * @generated
	 */
	void setPublisher(String value);

	/**
	 * Returns the value of the '<em><b>Series</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Series</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Series</em>' attribute.
	 * @see #setSeries(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getInCollection_Series()
	 * @model
	 * @generated
	 */
	String getSeries();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.InCollection#getSeries <em>Series</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Series</em>' attribute.
	 * @see #getSeries()
	 * @generated
	 */
	void setSeries(String value);

	/**
	 * Returns the value of the '<em><b>Volume</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Volume</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Volume</em>' attribute.
	 * @see #setVolume(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getInCollection_Volume()
	 * @model
	 * @generated
	 */
	String getVolume();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.InCollection#getVolume <em>Volume</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Volume</em>' attribute.
	 * @see #getVolume()
	 * @generated
	 */
	void setVolume(String value);

	/**
	 * Returns the value of the '<em><b>ISBN</b></em>' containment reference list.
	 * The list contents are of type {@link org.montex.researchcv.SerialNumber}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ISBN</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ISBN</em>' containment reference list.
	 * @see org.montex.researchcv.ResearchCVPackage#getInCollection_ISBN()
	 * @model containment="true"
	 * @generated
	 */
	EList<SerialNumber> getISBN();

	/**
	 * Returns the value of the '<em><b>First Page</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First Page</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First Page</em>' attribute.
	 * @see #setFirstPage(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getInCollection_FirstPage()
	 * @model
	 * @generated
	 */
	String getFirstPage();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.InCollection#getFirstPage <em>First Page</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First Page</em>' attribute.
	 * @see #getFirstPage()
	 * @generated
	 */
	void setFirstPage(String value);

	/**
	 * Returns the value of the '<em><b>Last Page</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Page</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Page</em>' attribute.
	 * @see #setLastPage(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getInCollection_LastPage()
	 * @model
	 * @generated
	 */
	String getLastPage();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.InCollection#getLastPage <em>Last Page</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Page</em>' attribute.
	 * @see #getLastPage()
	 * @generated
	 */
	void setLastPage(String value);

} // InCollection
