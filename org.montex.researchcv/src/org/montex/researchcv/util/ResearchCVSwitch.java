/**
 */
package org.montex.researchcv.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;
import org.montex.researchcv.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.montex.researchcv.ResearchCVPackage
 * @generated
 */
public class ResearchCVSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ResearchCVPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResearchCVSwitch() {
		if (modelPackage == null) {
			modelPackage = ResearchCVPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ResearchCVPackage.PERSON: {
				Person person = (Person)theEObject;
				T result = casePerson(person);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.RESEARCHER: {
				Researcher researcher = (Researcher)theEObject;
				T result = caseResearcher(researcher);
				if (result == null) result = casePerson(researcher);
				if (result == null) result = caseRootElement(researcher);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.ADDRESS: {
				Address address = (Address)theEObject;
				T result = caseAddress(address);
				if (result == null) result = caseContact(address);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.CONTACT: {
				Contact contact = (Contact)theEObject;
				T result = caseContact(contact);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.EMAIL: {
				Email email = (Email)theEObject;
				T result = caseEmail(email);
				if (result == null) result = caseContact(email);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.PHONE: {
				Phone phone = (Phone)theEObject;
				T result = casePhone(phone);
				if (result == null) result = caseContact(phone);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.SOCIAL_PROFILE: {
				SocialProfile socialProfile = (SocialProfile)theEObject;
				T result = caseSocialProfile(socialProfile);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.WEBSITE: {
				Website website = (Website)theEObject;
				T result = caseWebsite(website);
				if (result == null) result = caseContact(website);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.PUBLICATION: {
				Publication publication = (Publication)theEObject;
				T result = casePublication(publication);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.JOURNAL: {
				Journal journal = (Journal)theEObject;
				T result = caseJournal(journal);
				if (result == null) result = casePublication(journal);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.CONFERENCE: {
				Conference conference = (Conference)theEObject;
				T result = caseConference(conference);
				if (result == null) result = caseInProceedings(conference);
				if (result == null) result = caseInCollection(conference);
				if (result == null) result = casePublication(conference);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.WORKSHOP: {
				Workshop workshop = (Workshop)theEObject;
				T result = caseWorkshop(workshop);
				if (result == null) result = caseInProceedings(workshop);
				if (result == null) result = caseInCollection(workshop);
				if (result == null) result = casePublication(workshop);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.IN_PROCEEDINGS: {
				InProceedings inProceedings = (InProceedings)theEObject;
				T result = caseInProceedings(inProceedings);
				if (result == null) result = caseInCollection(inProceedings);
				if (result == null) result = casePublication(inProceedings);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.IN_COLLECTION: {
				InCollection inCollection = (InCollection)theEObject;
				T result = caseInCollection(inCollection);
				if (result == null) result = casePublication(inCollection);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.BOOK: {
				Book book = (Book)theEObject;
				T result = caseBook(book);
				if (result == null) result = casePublication(book);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.BOOK_CHAPTER: {
				BookChapter bookChapter = (BookChapter)theEObject;
				T result = caseBookChapter(bookChapter);
				if (result == null) result = caseInCollection(bookChapter);
				if (result == null) result = casePublication(bookChapter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.REPORT: {
				Report report = (Report)theEObject;
				T result = caseReport(report);
				if (result == null) result = casePublication(report);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.MISCELLANEOUS: {
				Miscellaneous miscellaneous = (Miscellaneous)theEObject;
				T result = caseMiscellaneous(miscellaneous);
				if (result == null) result = casePublication(miscellaneous);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.PUBLISHED_DATE: {
				PublishedDate publishedDate = (PublishedDate)theEObject;
				T result = casePublishedDate(publishedDate);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.SERIAL_NUMBER: {
				SerialNumber serialNumber = (SerialNumber)theEObject;
				T result = caseSerialNumber(serialNumber);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.RESEARCH_GROUP: {
				ResearchGroup researchGroup = (ResearchGroup)theEObject;
				T result = caseResearchGroup(researchGroup);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.COURSE: {
				Course course = (Course)theEObject;
				T result = caseCourse(course);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.TAUGHT_COURSE: {
				TaughtCourse taughtCourse = (TaughtCourse)theEObject;
				T result = caseTaughtCourse(taughtCourse);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.RESEARCH_TOPIC: {
				ResearchTopic researchTopic = (ResearchTopic)theEObject;
				T result = caseResearchTopic(researchTopic);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.PROJECT: {
				Project project = (Project)theEObject;
				T result = caseProject(project);
				if (result == null) result = caseGrant(project);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.GRANT: {
				Grant grant = (Grant)theEObject;
				T result = caseGrant(grant);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.BUDGET: {
				Budget budget = (Budget)theEObject;
				T result = caseBudget(budget);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.ROOT_ELEMENT: {
				RootElement rootElement = (RootElement)theEObject;
				T result = caseRootElement(rootElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.PERSON_REGISTRY: {
				PersonRegistry personRegistry = (PersonRegistry)theEObject;
				T result = casePersonRegistry(personRegistry);
				if (result == null) result = caseRootElement(personRegistry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.COURSE_OFFERING: {
				CourseOffering courseOffering = (CourseOffering)theEObject;
				T result = caseCourseOffering(courseOffering);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.SUPERVISION: {
				Supervision supervision = (Supervision)theEObject;
				T result = caseSupervision(supervision);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.INSTITUTION_REGISTRY: {
				InstitutionRegistry institutionRegistry = (InstitutionRegistry)theEObject;
				T result = caseInstitutionRegistry(institutionRegistry);
				if (result == null) result = caseRootElement(institutionRegistry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.INSTITUTION: {
				Institution institution = (Institution)theEObject;
				T result = caseInstitution(institution);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.MULTI_LANGUAGE_STRING: {
				MultiLanguageString multiLanguageString = (MultiLanguageString)theEObject;
				T result = caseMultiLanguageString(multiLanguageString);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.LOCALIZED_STRING: {
				LocalizedString localizedString = (LocalizedString)theEObject;
				T result = caseLocalizedString(localizedString);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResearchCVPackage.SUPERVISIONS_REGISTRY: {
				SupervisionsRegistry supervisionsRegistry = (SupervisionsRegistry)theEObject;
				T result = caseSupervisionsRegistry(supervisionsRegistry);
				if (result == null) result = caseRootElement(supervisionsRegistry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Person</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Person</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePerson(Person object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Researcher</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Researcher</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResearcher(Researcher object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Address</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Address</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAddress(Address object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Contact</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Contact</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContact(Contact object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Email</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Email</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEmail(Email object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Phone</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Phone</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhone(Phone object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Social Profile</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Social Profile</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSocialProfile(SocialProfile object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Website</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Website</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWebsite(Website object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Publication</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Publication</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePublication(Publication object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Journal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Journal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseJournal(Journal object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Conference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Conference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConference(Conference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Workshop</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Workshop</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWorkshop(Workshop object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>In Proceedings</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>In Proceedings</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInProceedings(InProceedings object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>In Collection</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>In Collection</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInCollection(InCollection object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Book</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Book</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBook(Book object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Book Chapter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Book Chapter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBookChapter(BookChapter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Report</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Report</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReport(Report object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Miscellaneous</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Miscellaneous</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMiscellaneous(Miscellaneous object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Published Date</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Published Date</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePublishedDate(PublishedDate object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Serial Number</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Serial Number</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSerialNumber(SerialNumber object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Research Group</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Research Group</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResearchGroup(ResearchGroup object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Course</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Course</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCourse(Course object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Taught Course</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Taught Course</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTaughtCourse(TaughtCourse object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Research Topic</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Research Topic</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResearchTopic(ResearchTopic object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Project</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Project</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProject(Project object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Grant</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Grant</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGrant(Grant object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Budget</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Budget</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBudget(Budget object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Root Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Root Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRootElement(RootElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Person Registry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Person Registry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePersonRegistry(PersonRegistry object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Course Offering</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Course Offering</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCourseOffering(CourseOffering object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Supervision</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Supervision</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSupervision(Supervision object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Institution Registry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Institution Registry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInstitutionRegistry(InstitutionRegistry object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Institution</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Institution</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInstitution(Institution object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Multi Language String</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Multi Language String</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMultiLanguageString(MultiLanguageString object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Localized String</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Localized String</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLocalizedString(LocalizedString object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Supervisions Registry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Supervisions Registry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSupervisionsRegistry(SupervisionsRegistry object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //ResearchCVSwitch
