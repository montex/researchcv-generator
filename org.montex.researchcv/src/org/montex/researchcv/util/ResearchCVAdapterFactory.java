/**
 */
package org.montex.researchcv.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;
import org.montex.researchcv.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.montex.researchcv.ResearchCVPackage
 * @generated
 */
public class ResearchCVAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ResearchCVPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResearchCVAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = ResearchCVPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResearchCVSwitch<Adapter> modelSwitch =
		new ResearchCVSwitch<Adapter>() {
			@Override
			public Adapter casePerson(Person object) {
				return createPersonAdapter();
			}
			@Override
			public Adapter caseResearcher(Researcher object) {
				return createResearcherAdapter();
			}
			@Override
			public Adapter caseAddress(Address object) {
				return createAddressAdapter();
			}
			@Override
			public Adapter caseContact(Contact object) {
				return createContactAdapter();
			}
			@Override
			public Adapter caseEmail(Email object) {
				return createEmailAdapter();
			}
			@Override
			public Adapter casePhone(Phone object) {
				return createPhoneAdapter();
			}
			@Override
			public Adapter caseSocialProfile(SocialProfile object) {
				return createSocialProfileAdapter();
			}
			@Override
			public Adapter caseWebsite(Website object) {
				return createWebsiteAdapter();
			}
			@Override
			public Adapter casePublication(Publication object) {
				return createPublicationAdapter();
			}
			@Override
			public Adapter caseJournal(Journal object) {
				return createJournalAdapter();
			}
			@Override
			public Adapter caseConference(Conference object) {
				return createConferenceAdapter();
			}
			@Override
			public Adapter caseWorkshop(Workshop object) {
				return createWorkshopAdapter();
			}
			@Override
			public Adapter caseInProceedings(InProceedings object) {
				return createInProceedingsAdapter();
			}
			@Override
			public Adapter caseInCollection(InCollection object) {
				return createInCollectionAdapter();
			}
			@Override
			public Adapter caseBook(Book object) {
				return createBookAdapter();
			}
			@Override
			public Adapter caseBookChapter(BookChapter object) {
				return createBookChapterAdapter();
			}
			@Override
			public Adapter caseReport(Report object) {
				return createReportAdapter();
			}
			@Override
			public Adapter caseMiscellaneous(Miscellaneous object) {
				return createMiscellaneousAdapter();
			}
			@Override
			public Adapter casePublishedDate(PublishedDate object) {
				return createPublishedDateAdapter();
			}
			@Override
			public Adapter caseSerialNumber(SerialNumber object) {
				return createSerialNumberAdapter();
			}
			@Override
			public Adapter caseResearchGroup(ResearchGroup object) {
				return createResearchGroupAdapter();
			}
			@Override
			public Adapter caseCourse(Course object) {
				return createCourseAdapter();
			}
			@Override
			public Adapter caseTaughtCourse(TaughtCourse object) {
				return createTaughtCourseAdapter();
			}
			@Override
			public Adapter caseResearchTopic(ResearchTopic object) {
				return createResearchTopicAdapter();
			}
			@Override
			public Adapter caseProject(Project object) {
				return createProjectAdapter();
			}
			@Override
			public Adapter caseGrant(Grant object) {
				return createGrantAdapter();
			}
			@Override
			public Adapter caseBudget(Budget object) {
				return createBudgetAdapter();
			}
			@Override
			public Adapter caseRootElement(RootElement object) {
				return createRootElementAdapter();
			}
			@Override
			public Adapter casePersonRegistry(PersonRegistry object) {
				return createPersonRegistryAdapter();
			}
			@Override
			public Adapter caseCourseOffering(CourseOffering object) {
				return createCourseOfferingAdapter();
			}
			@Override
			public Adapter caseSupervision(Supervision object) {
				return createSupervisionAdapter();
			}
			@Override
			public Adapter caseInstitutionRegistry(InstitutionRegistry object) {
				return createInstitutionRegistryAdapter();
			}
			@Override
			public Adapter caseInstitution(Institution object) {
				return createInstitutionAdapter();
			}
			@Override
			public Adapter caseMultiLanguageString(MultiLanguageString object) {
				return createMultiLanguageStringAdapter();
			}
			@Override
			public Adapter caseLocalizedString(LocalizedString object) {
				return createLocalizedStringAdapter();
			}
			@Override
			public Adapter caseSupervisionsRegistry(SupervisionsRegistry object) {
				return createSupervisionsRegistryAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.Person <em>Person</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.Person
	 * @generated
	 */
	public Adapter createPersonAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.Researcher <em>Researcher</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.Researcher
	 * @generated
	 */
	public Adapter createResearcherAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.Address <em>Address</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.Address
	 * @generated
	 */
	public Adapter createAddressAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.Contact <em>Contact</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.Contact
	 * @generated
	 */
	public Adapter createContactAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.Email <em>Email</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.Email
	 * @generated
	 */
	public Adapter createEmailAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.Phone <em>Phone</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.Phone
	 * @generated
	 */
	public Adapter createPhoneAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.SocialProfile <em>Social Profile</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.SocialProfile
	 * @generated
	 */
	public Adapter createSocialProfileAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.Website <em>Website</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.Website
	 * @generated
	 */
	public Adapter createWebsiteAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.Publication <em>Publication</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.Publication
	 * @generated
	 */
	public Adapter createPublicationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.Journal <em>Journal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.Journal
	 * @generated
	 */
	public Adapter createJournalAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.Conference <em>Conference</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.Conference
	 * @generated
	 */
	public Adapter createConferenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.Workshop <em>Workshop</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.Workshop
	 * @generated
	 */
	public Adapter createWorkshopAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.InProceedings <em>In Proceedings</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.InProceedings
	 * @generated
	 */
	public Adapter createInProceedingsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.InCollection <em>In Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.InCollection
	 * @generated
	 */
	public Adapter createInCollectionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.Book <em>Book</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.Book
	 * @generated
	 */
	public Adapter createBookAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.BookChapter <em>Book Chapter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.BookChapter
	 * @generated
	 */
	public Adapter createBookChapterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.Report <em>Report</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.Report
	 * @generated
	 */
	public Adapter createReportAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.Miscellaneous <em>Miscellaneous</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.Miscellaneous
	 * @generated
	 */
	public Adapter createMiscellaneousAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.PublishedDate <em>Published Date</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.PublishedDate
	 * @generated
	 */
	public Adapter createPublishedDateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.SerialNumber <em>Serial Number</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.SerialNumber
	 * @generated
	 */
	public Adapter createSerialNumberAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.ResearchGroup <em>Research Group</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.ResearchGroup
	 * @generated
	 */
	public Adapter createResearchGroupAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.Course <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.Course
	 * @generated
	 */
	public Adapter createCourseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.TaughtCourse <em>Taught Course</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.TaughtCourse
	 * @generated
	 */
	public Adapter createTaughtCourseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.ResearchTopic <em>Research Topic</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.ResearchTopic
	 * @generated
	 */
	public Adapter createResearchTopicAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.Project <em>Project</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.Project
	 * @generated
	 */
	public Adapter createProjectAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.Grant <em>Grant</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.Grant
	 * @generated
	 */
	public Adapter createGrantAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.Budget <em>Budget</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.Budget
	 * @generated
	 */
	public Adapter createBudgetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.RootElement <em>Root Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.RootElement
	 * @generated
	 */
	public Adapter createRootElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.PersonRegistry <em>Person Registry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.PersonRegistry
	 * @generated
	 */
	public Adapter createPersonRegistryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.CourseOffering <em>Course Offering</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.CourseOffering
	 * @generated
	 */
	public Adapter createCourseOfferingAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.Supervision <em>Supervision</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.Supervision
	 * @generated
	 */
	public Adapter createSupervisionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.InstitutionRegistry <em>Institution Registry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.InstitutionRegistry
	 * @generated
	 */
	public Adapter createInstitutionRegistryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.Institution <em>Institution</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.Institution
	 * @generated
	 */
	public Adapter createInstitutionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.MultiLanguageString <em>Multi Language String</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.MultiLanguageString
	 * @generated
	 */
	public Adapter createMultiLanguageStringAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.LocalizedString <em>Localized String</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.LocalizedString
	 * @generated
	 */
	public Adapter createLocalizedStringAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.montex.researchcv.SupervisionsRegistry <em>Supervisions Registry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.montex.researchcv.SupervisionsRegistry
	 * @generated
	 */
	public Adapter createSupervisionsRegistryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //ResearchCVAdapterFactory
