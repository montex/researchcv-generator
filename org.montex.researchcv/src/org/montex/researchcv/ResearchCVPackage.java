/**
 */
package org.montex.researchcv;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.montex.researchcv.ResearchCVFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/OCL/Import ecore='http://www.eclipse.org/emf/2002/Ecore'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore invocationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL' settingDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL' validationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL'"
 * @generated
 */
public interface ResearchCVPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "researchcv";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.montex.org/researchcv";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "rcv";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ResearchCVPackage eINSTANCE = org.montex.researchcv.impl.ResearchCVPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.PersonImpl <em>Person</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.PersonImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getPerson()
	 * @generated
	 */
	int PERSON = 0;

	/**
	 * The feature id for the '<em><b>First Names</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__FIRST_NAMES = 0;

	/**
	 * The feature id for the '<em><b>Last Names</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__LAST_NAMES = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__NAME = 2;

	/**
	 * The number of structural features of the '<em>Person</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Person</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.ResearcherImpl <em>Researcher</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.ResearcherImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getResearcher()
	 * @generated
	 */
	int RESEARCHER = 1;

	/**
	 * The feature id for the '<em><b>First Names</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESEARCHER__FIRST_NAMES = PERSON__FIRST_NAMES;

	/**
	 * The feature id for the '<em><b>Last Names</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESEARCHER__LAST_NAMES = PERSON__LAST_NAMES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESEARCHER__NAME = PERSON__NAME;

	/**
	 * The feature id for the '<em><b>Coauthors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESEARCHER__COAUTHORS = PERSON_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Contacts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESEARCHER__CONTACTS = PERSON_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Socials</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESEARCHER__SOCIALS = PERSON_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Publications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESEARCHER__PUBLICATIONS = PERSON_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Teaching Material</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESEARCHER__TEACHING_MATERIAL = PERSON_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Other Material</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESEARCHER__OTHER_MATERIAL = PERSON_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Research Group</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESEARCHER__RESEARCH_GROUP = PERSON_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Topics</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESEARCHER__TOPICS = PERSON_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Grants</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESEARCHER__GRANTS = PERSON_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Courses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESEARCHER__COURSES = PERSON_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Taught Courses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESEARCHER__TAUGHT_COURSES = PERSON_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Supervisions Registry</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESEARCHER__SUPERVISIONS_REGISTRY = PERSON_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Supervisions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESEARCHER__SUPERVISIONS = PERSON_FEATURE_COUNT + 12;

	/**
	 * The number of structural features of the '<em>Researcher</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESEARCHER_FEATURE_COUNT = PERSON_FEATURE_COUNT + 13;

	/**
	 * The number of operations of the '<em>Researcher</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESEARCHER_OPERATION_COUNT = PERSON_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.ContactImpl <em>Contact</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.ContactImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getContact()
	 * @generated
	 */
	int CONTACT = 3;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTACT__KIND = 0;

	/**
	 * The number of structural features of the '<em>Contact</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTACT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Contact</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTACT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.AddressImpl <em>Address</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.AddressImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getAddress()
	 * @generated
	 */
	int ADDRESS = 2;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDRESS__KIND = CONTACT__KIND;

	/**
	 * The feature id for the '<em><b>Lines</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDRESS__LINES = CONTACT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Zip</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDRESS__ZIP = CONTACT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>City</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDRESS__CITY = CONTACT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Region</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDRESS__REGION = CONTACT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Country</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDRESS__COUNTRY = CONTACT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Building</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDRESS__BUILDING = CONTACT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Room</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDRESS__ROOM = CONTACT_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Address</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDRESS_FEATURE_COUNT = CONTACT_FEATURE_COUNT + 7;

	/**
	 * The number of operations of the '<em>Address</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDRESS_OPERATION_COUNT = CONTACT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.EmailImpl <em>Email</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.EmailImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getEmail()
	 * @generated
	 */
	int EMAIL = 4;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMAIL__KIND = CONTACT__KIND;

	/**
	 * The feature id for the '<em><b>Address</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMAIL__ADDRESS = CONTACT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Email</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMAIL_FEATURE_COUNT = CONTACT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Email</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMAIL_OPERATION_COUNT = CONTACT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.PhoneImpl <em>Phone</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.PhoneImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getPhone()
	 * @generated
	 */
	int PHONE = 5;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHONE__KIND = CONTACT__KIND;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHONE__NUMBER = CONTACT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Phone</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHONE_FEATURE_COUNT = CONTACT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Phone</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHONE_OPERATION_COUNT = CONTACT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.SocialProfileImpl <em>Social Profile</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.SocialProfileImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getSocialProfile()
	 * @generated
	 */
	int SOCIAL_PROFILE = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOCIAL_PROFILE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Url</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOCIAL_PROFILE__URL = 1;

	/**
	 * The number of structural features of the '<em>Social Profile</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOCIAL_PROFILE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Social Profile</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOCIAL_PROFILE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.WebsiteImpl <em>Website</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.WebsiteImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getWebsite()
	 * @generated
	 */
	int WEBSITE = 7;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WEBSITE__KIND = CONTACT__KIND;

	/**
	 * The feature id for the '<em><b>Url</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WEBSITE__URL = CONTACT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Website</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WEBSITE_FEATURE_COUNT = CONTACT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Website</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WEBSITE_OPERATION_COUNT = CONTACT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.PublicationImpl <em>Publication</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.PublicationImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getPublication()
	 * @generated
	 */
	int PUBLICATION = 8;

	/**
	 * The feature id for the '<em><b>Citekey</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLICATION__CITEKEY = 0;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLICATION__TITLE = 1;

	/**
	 * The feature id for the '<em><b>Authors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLICATION__AUTHORS = 2;

	/**
	 * The feature id for the '<em><b>URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLICATION__URL = 3;

	/**
	 * The feature id for the '<em><b>Published</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLICATION__PUBLISHED = 4;

	/**
	 * The feature id for the '<em><b>Open Access</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLICATION__OPEN_ACCESS = 5;

	/**
	 * The feature id for the '<em><b>Date</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLICATION__DATE = 6;

	/**
	 * The feature id for the '<em><b>DOI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLICATION__DOI = 7;

	/**
	 * The feature id for the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLICATION__ABSTRACT = 8;

	/**
	 * The feature id for the '<em><b>With Author Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLICATION__WITH_AUTHOR_VERSION = 9;

	/**
	 * The feature id for the '<em><b>Notes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLICATION__NOTES = 10;

	/**
	 * The feature id for the '<em><b>Related Projects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLICATION__RELATED_PROJECTS = 11;

	/**
	 * The number of structural features of the '<em>Publication</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLICATION_FEATURE_COUNT = 12;

	/**
	 * The number of operations of the '<em>Publication</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLICATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.JournalImpl <em>Journal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.JournalImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getJournal()
	 * @generated
	 */
	int JOURNAL = 9;

	/**
	 * The feature id for the '<em><b>Citekey</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOURNAL__CITEKEY = PUBLICATION__CITEKEY;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOURNAL__TITLE = PUBLICATION__TITLE;

	/**
	 * The feature id for the '<em><b>Authors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOURNAL__AUTHORS = PUBLICATION__AUTHORS;

	/**
	 * The feature id for the '<em><b>URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOURNAL__URL = PUBLICATION__URL;

	/**
	 * The feature id for the '<em><b>Published</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOURNAL__PUBLISHED = PUBLICATION__PUBLISHED;

	/**
	 * The feature id for the '<em><b>Open Access</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOURNAL__OPEN_ACCESS = PUBLICATION__OPEN_ACCESS;

	/**
	 * The feature id for the '<em><b>Date</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOURNAL__DATE = PUBLICATION__DATE;

	/**
	 * The feature id for the '<em><b>DOI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOURNAL__DOI = PUBLICATION__DOI;

	/**
	 * The feature id for the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOURNAL__ABSTRACT = PUBLICATION__ABSTRACT;

	/**
	 * The feature id for the '<em><b>With Author Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOURNAL__WITH_AUTHOR_VERSION = PUBLICATION__WITH_AUTHOR_VERSION;

	/**
	 * The feature id for the '<em><b>Notes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOURNAL__NOTES = PUBLICATION__NOTES;

	/**
	 * The feature id for the '<em><b>Related Projects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOURNAL__RELATED_PROJECTS = PUBLICATION__RELATED_PROJECTS;

	/**
	 * The feature id for the '<em><b>Journal Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOURNAL__JOURNAL_NAME = PUBLICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Publisher</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOURNAL__PUBLISHER = PUBLICATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>First Page</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOURNAL__FIRST_PAGE = PUBLICATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Last Page</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOURNAL__LAST_PAGE = PUBLICATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Volume</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOURNAL__VOLUME = PUBLICATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Issue</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOURNAL__ISSUE = PUBLICATION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>ISSN</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOURNAL__ISSN = PUBLICATION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Magazine</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOURNAL__MAGAZINE = PUBLICATION_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Journal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOURNAL_FEATURE_COUNT = PUBLICATION_FEATURE_COUNT + 8;

	/**
	 * The number of operations of the '<em>Journal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOURNAL_OPERATION_COUNT = PUBLICATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.InCollectionImpl <em>In Collection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.InCollectionImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getInCollection()
	 * @generated
	 */
	int IN_COLLECTION = 13;

	/**
	 * The feature id for the '<em><b>Citekey</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_COLLECTION__CITEKEY = PUBLICATION__CITEKEY;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_COLLECTION__TITLE = PUBLICATION__TITLE;

	/**
	 * The feature id for the '<em><b>Authors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_COLLECTION__AUTHORS = PUBLICATION__AUTHORS;

	/**
	 * The feature id for the '<em><b>URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_COLLECTION__URL = PUBLICATION__URL;

	/**
	 * The feature id for the '<em><b>Published</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_COLLECTION__PUBLISHED = PUBLICATION__PUBLISHED;

	/**
	 * The feature id for the '<em><b>Open Access</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_COLLECTION__OPEN_ACCESS = PUBLICATION__OPEN_ACCESS;

	/**
	 * The feature id for the '<em><b>Date</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_COLLECTION__DATE = PUBLICATION__DATE;

	/**
	 * The feature id for the '<em><b>DOI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_COLLECTION__DOI = PUBLICATION__DOI;

	/**
	 * The feature id for the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_COLLECTION__ABSTRACT = PUBLICATION__ABSTRACT;

	/**
	 * The feature id for the '<em><b>With Author Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_COLLECTION__WITH_AUTHOR_VERSION = PUBLICATION__WITH_AUTHOR_VERSION;

	/**
	 * The feature id for the '<em><b>Notes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_COLLECTION__NOTES = PUBLICATION__NOTES;

	/**
	 * The feature id for the '<em><b>Related Projects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_COLLECTION__RELATED_PROJECTS = PUBLICATION__RELATED_PROJECTS;

	/**
	 * The feature id for the '<em><b>Booktitle</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_COLLECTION__BOOKTITLE = PUBLICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Bookeditors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_COLLECTION__BOOKEDITORS = PUBLICATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Publisher</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_COLLECTION__PUBLISHER = PUBLICATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Series</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_COLLECTION__SERIES = PUBLICATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Volume</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_COLLECTION__VOLUME = PUBLICATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>First Page</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_COLLECTION__FIRST_PAGE = PUBLICATION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Last Page</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_COLLECTION__LAST_PAGE = PUBLICATION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>ISBN</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_COLLECTION__ISBN = PUBLICATION_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>In Collection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_COLLECTION_FEATURE_COUNT = PUBLICATION_FEATURE_COUNT + 8;

	/**
	 * The number of operations of the '<em>In Collection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_COLLECTION_OPERATION_COUNT = PUBLICATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.InProceedingsImpl <em>In Proceedings</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.InProceedingsImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getInProceedings()
	 * @generated
	 */
	int IN_PROCEEDINGS = 12;

	/**
	 * The feature id for the '<em><b>Citekey</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PROCEEDINGS__CITEKEY = IN_COLLECTION__CITEKEY;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PROCEEDINGS__TITLE = IN_COLLECTION__TITLE;

	/**
	 * The feature id for the '<em><b>Authors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PROCEEDINGS__AUTHORS = IN_COLLECTION__AUTHORS;

	/**
	 * The feature id for the '<em><b>URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PROCEEDINGS__URL = IN_COLLECTION__URL;

	/**
	 * The feature id for the '<em><b>Published</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PROCEEDINGS__PUBLISHED = IN_COLLECTION__PUBLISHED;

	/**
	 * The feature id for the '<em><b>Open Access</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PROCEEDINGS__OPEN_ACCESS = IN_COLLECTION__OPEN_ACCESS;

	/**
	 * The feature id for the '<em><b>Date</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PROCEEDINGS__DATE = IN_COLLECTION__DATE;

	/**
	 * The feature id for the '<em><b>DOI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PROCEEDINGS__DOI = IN_COLLECTION__DOI;

	/**
	 * The feature id for the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PROCEEDINGS__ABSTRACT = IN_COLLECTION__ABSTRACT;

	/**
	 * The feature id for the '<em><b>With Author Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PROCEEDINGS__WITH_AUTHOR_VERSION = IN_COLLECTION__WITH_AUTHOR_VERSION;

	/**
	 * The feature id for the '<em><b>Notes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PROCEEDINGS__NOTES = IN_COLLECTION__NOTES;

	/**
	 * The feature id for the '<em><b>Related Projects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PROCEEDINGS__RELATED_PROJECTS = IN_COLLECTION__RELATED_PROJECTS;

	/**
	 * The feature id for the '<em><b>Booktitle</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PROCEEDINGS__BOOKTITLE = IN_COLLECTION__BOOKTITLE;

	/**
	 * The feature id for the '<em><b>Bookeditors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PROCEEDINGS__BOOKEDITORS = IN_COLLECTION__BOOKEDITORS;

	/**
	 * The feature id for the '<em><b>Publisher</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PROCEEDINGS__PUBLISHER = IN_COLLECTION__PUBLISHER;

	/**
	 * The feature id for the '<em><b>Series</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PROCEEDINGS__SERIES = IN_COLLECTION__SERIES;

	/**
	 * The feature id for the '<em><b>Volume</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PROCEEDINGS__VOLUME = IN_COLLECTION__VOLUME;

	/**
	 * The feature id for the '<em><b>First Page</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PROCEEDINGS__FIRST_PAGE = IN_COLLECTION__FIRST_PAGE;

	/**
	 * The feature id for the '<em><b>Last Page</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PROCEEDINGS__LAST_PAGE = IN_COLLECTION__LAST_PAGE;

	/**
	 * The feature id for the '<em><b>ISBN</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PROCEEDINGS__ISBN = IN_COLLECTION__ISBN;

	/**
	 * The feature id for the '<em><b>Event End Date</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PROCEEDINGS__EVENT_END_DATE = IN_COLLECTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Event Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PROCEEDINGS__EVENT_NAME = IN_COLLECTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Venue</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PROCEEDINGS__VENUE = IN_COLLECTION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Event Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PROCEEDINGS__EVENT_SHORT_NAME = IN_COLLECTION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Event Start Date</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PROCEEDINGS__EVENT_START_DATE = IN_COLLECTION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Track Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PROCEEDINGS__TRACK_NAME = IN_COLLECTION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Track Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PROCEEDINGS__TRACK_SHORT_NAME = IN_COLLECTION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Short Paper</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PROCEEDINGS__SHORT_PAPER = IN_COLLECTION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Tool Paper</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PROCEEDINGS__TOOL_PAPER = IN_COLLECTION_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Invited Paper</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PROCEEDINGS__INVITED_PAPER = IN_COLLECTION_FEATURE_COUNT + 9;

	/**
	 * The number of structural features of the '<em>In Proceedings</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PROCEEDINGS_FEATURE_COUNT = IN_COLLECTION_FEATURE_COUNT + 10;

	/**
	 * The number of operations of the '<em>In Proceedings</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_PROCEEDINGS_OPERATION_COUNT = IN_COLLECTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.ConferenceImpl <em>Conference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.ConferenceImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getConference()
	 * @generated
	 */
	int CONFERENCE = 10;

	/**
	 * The feature id for the '<em><b>Citekey</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFERENCE__CITEKEY = IN_PROCEEDINGS__CITEKEY;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFERENCE__TITLE = IN_PROCEEDINGS__TITLE;

	/**
	 * The feature id for the '<em><b>Authors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFERENCE__AUTHORS = IN_PROCEEDINGS__AUTHORS;

	/**
	 * The feature id for the '<em><b>URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFERENCE__URL = IN_PROCEEDINGS__URL;

	/**
	 * The feature id for the '<em><b>Published</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFERENCE__PUBLISHED = IN_PROCEEDINGS__PUBLISHED;

	/**
	 * The feature id for the '<em><b>Open Access</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFERENCE__OPEN_ACCESS = IN_PROCEEDINGS__OPEN_ACCESS;

	/**
	 * The feature id for the '<em><b>Date</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFERENCE__DATE = IN_PROCEEDINGS__DATE;

	/**
	 * The feature id for the '<em><b>DOI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFERENCE__DOI = IN_PROCEEDINGS__DOI;

	/**
	 * The feature id for the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFERENCE__ABSTRACT = IN_PROCEEDINGS__ABSTRACT;

	/**
	 * The feature id for the '<em><b>With Author Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFERENCE__WITH_AUTHOR_VERSION = IN_PROCEEDINGS__WITH_AUTHOR_VERSION;

	/**
	 * The feature id for the '<em><b>Notes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFERENCE__NOTES = IN_PROCEEDINGS__NOTES;

	/**
	 * The feature id for the '<em><b>Related Projects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFERENCE__RELATED_PROJECTS = IN_PROCEEDINGS__RELATED_PROJECTS;

	/**
	 * The feature id for the '<em><b>Booktitle</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFERENCE__BOOKTITLE = IN_PROCEEDINGS__BOOKTITLE;

	/**
	 * The feature id for the '<em><b>Bookeditors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFERENCE__BOOKEDITORS = IN_PROCEEDINGS__BOOKEDITORS;

	/**
	 * The feature id for the '<em><b>Publisher</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFERENCE__PUBLISHER = IN_PROCEEDINGS__PUBLISHER;

	/**
	 * The feature id for the '<em><b>Series</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFERENCE__SERIES = IN_PROCEEDINGS__SERIES;

	/**
	 * The feature id for the '<em><b>Volume</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFERENCE__VOLUME = IN_PROCEEDINGS__VOLUME;

	/**
	 * The feature id for the '<em><b>First Page</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFERENCE__FIRST_PAGE = IN_PROCEEDINGS__FIRST_PAGE;

	/**
	 * The feature id for the '<em><b>Last Page</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFERENCE__LAST_PAGE = IN_PROCEEDINGS__LAST_PAGE;

	/**
	 * The feature id for the '<em><b>ISBN</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFERENCE__ISBN = IN_PROCEEDINGS__ISBN;

	/**
	 * The feature id for the '<em><b>Event End Date</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFERENCE__EVENT_END_DATE = IN_PROCEEDINGS__EVENT_END_DATE;

	/**
	 * The feature id for the '<em><b>Event Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFERENCE__EVENT_NAME = IN_PROCEEDINGS__EVENT_NAME;

	/**
	 * The feature id for the '<em><b>Venue</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFERENCE__VENUE = IN_PROCEEDINGS__VENUE;

	/**
	 * The feature id for the '<em><b>Event Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFERENCE__EVENT_SHORT_NAME = IN_PROCEEDINGS__EVENT_SHORT_NAME;

	/**
	 * The feature id for the '<em><b>Event Start Date</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFERENCE__EVENT_START_DATE = IN_PROCEEDINGS__EVENT_START_DATE;

	/**
	 * The feature id for the '<em><b>Track Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFERENCE__TRACK_NAME = IN_PROCEEDINGS__TRACK_NAME;

	/**
	 * The feature id for the '<em><b>Track Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFERENCE__TRACK_SHORT_NAME = IN_PROCEEDINGS__TRACK_SHORT_NAME;

	/**
	 * The feature id for the '<em><b>Short Paper</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFERENCE__SHORT_PAPER = IN_PROCEEDINGS__SHORT_PAPER;

	/**
	 * The feature id for the '<em><b>Tool Paper</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFERENCE__TOOL_PAPER = IN_PROCEEDINGS__TOOL_PAPER;

	/**
	 * The feature id for the '<em><b>Invited Paper</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFERENCE__INVITED_PAPER = IN_PROCEEDINGS__INVITED_PAPER;

	/**
	 * The number of structural features of the '<em>Conference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFERENCE_FEATURE_COUNT = IN_PROCEEDINGS_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Conference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFERENCE_OPERATION_COUNT = IN_PROCEEDINGS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.WorkshopImpl <em>Workshop</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.WorkshopImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getWorkshop()
	 * @generated
	 */
	int WORKSHOP = 11;

	/**
	 * The feature id for the '<em><b>Citekey</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP__CITEKEY = IN_PROCEEDINGS__CITEKEY;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP__TITLE = IN_PROCEEDINGS__TITLE;

	/**
	 * The feature id for the '<em><b>Authors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP__AUTHORS = IN_PROCEEDINGS__AUTHORS;

	/**
	 * The feature id for the '<em><b>URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP__URL = IN_PROCEEDINGS__URL;

	/**
	 * The feature id for the '<em><b>Published</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP__PUBLISHED = IN_PROCEEDINGS__PUBLISHED;

	/**
	 * The feature id for the '<em><b>Open Access</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP__OPEN_ACCESS = IN_PROCEEDINGS__OPEN_ACCESS;

	/**
	 * The feature id for the '<em><b>Date</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP__DATE = IN_PROCEEDINGS__DATE;

	/**
	 * The feature id for the '<em><b>DOI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP__DOI = IN_PROCEEDINGS__DOI;

	/**
	 * The feature id for the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP__ABSTRACT = IN_PROCEEDINGS__ABSTRACT;

	/**
	 * The feature id for the '<em><b>With Author Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP__WITH_AUTHOR_VERSION = IN_PROCEEDINGS__WITH_AUTHOR_VERSION;

	/**
	 * The feature id for the '<em><b>Notes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP__NOTES = IN_PROCEEDINGS__NOTES;

	/**
	 * The feature id for the '<em><b>Related Projects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP__RELATED_PROJECTS = IN_PROCEEDINGS__RELATED_PROJECTS;

	/**
	 * The feature id for the '<em><b>Booktitle</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP__BOOKTITLE = IN_PROCEEDINGS__BOOKTITLE;

	/**
	 * The feature id for the '<em><b>Bookeditors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP__BOOKEDITORS = IN_PROCEEDINGS__BOOKEDITORS;

	/**
	 * The feature id for the '<em><b>Publisher</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP__PUBLISHER = IN_PROCEEDINGS__PUBLISHER;

	/**
	 * The feature id for the '<em><b>Series</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP__SERIES = IN_PROCEEDINGS__SERIES;

	/**
	 * The feature id for the '<em><b>Volume</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP__VOLUME = IN_PROCEEDINGS__VOLUME;

	/**
	 * The feature id for the '<em><b>First Page</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP__FIRST_PAGE = IN_PROCEEDINGS__FIRST_PAGE;

	/**
	 * The feature id for the '<em><b>Last Page</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP__LAST_PAGE = IN_PROCEEDINGS__LAST_PAGE;

	/**
	 * The feature id for the '<em><b>ISBN</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP__ISBN = IN_PROCEEDINGS__ISBN;

	/**
	 * The feature id for the '<em><b>Event End Date</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP__EVENT_END_DATE = IN_PROCEEDINGS__EVENT_END_DATE;

	/**
	 * The feature id for the '<em><b>Event Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP__EVENT_NAME = IN_PROCEEDINGS__EVENT_NAME;

	/**
	 * The feature id for the '<em><b>Venue</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP__VENUE = IN_PROCEEDINGS__VENUE;

	/**
	 * The feature id for the '<em><b>Event Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP__EVENT_SHORT_NAME = IN_PROCEEDINGS__EVENT_SHORT_NAME;

	/**
	 * The feature id for the '<em><b>Event Start Date</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP__EVENT_START_DATE = IN_PROCEEDINGS__EVENT_START_DATE;

	/**
	 * The feature id for the '<em><b>Track Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP__TRACK_NAME = IN_PROCEEDINGS__TRACK_NAME;

	/**
	 * The feature id for the '<em><b>Track Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP__TRACK_SHORT_NAME = IN_PROCEEDINGS__TRACK_SHORT_NAME;

	/**
	 * The feature id for the '<em><b>Short Paper</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP__SHORT_PAPER = IN_PROCEEDINGS__SHORT_PAPER;

	/**
	 * The feature id for the '<em><b>Tool Paper</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP__TOOL_PAPER = IN_PROCEEDINGS__TOOL_PAPER;

	/**
	 * The feature id for the '<em><b>Invited Paper</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP__INVITED_PAPER = IN_PROCEEDINGS__INVITED_PAPER;

	/**
	 * The feature id for the '<em><b>Main Event Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP__MAIN_EVENT_NAME = IN_PROCEEDINGS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Main Event Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP__MAIN_EVENT_SHORT_NAME = IN_PROCEEDINGS_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Workshop</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP_FEATURE_COUNT = IN_PROCEEDINGS_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Workshop</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORKSHOP_OPERATION_COUNT = IN_PROCEEDINGS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.BookImpl <em>Book</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.BookImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getBook()
	 * @generated
	 */
	int BOOK = 14;

	/**
	 * The feature id for the '<em><b>Citekey</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK__CITEKEY = PUBLICATION__CITEKEY;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK__TITLE = PUBLICATION__TITLE;

	/**
	 * The feature id for the '<em><b>Authors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK__AUTHORS = PUBLICATION__AUTHORS;

	/**
	 * The feature id for the '<em><b>URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK__URL = PUBLICATION__URL;

	/**
	 * The feature id for the '<em><b>Published</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK__PUBLISHED = PUBLICATION__PUBLISHED;

	/**
	 * The feature id for the '<em><b>Open Access</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK__OPEN_ACCESS = PUBLICATION__OPEN_ACCESS;

	/**
	 * The feature id for the '<em><b>Date</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK__DATE = PUBLICATION__DATE;

	/**
	 * The feature id for the '<em><b>DOI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK__DOI = PUBLICATION__DOI;

	/**
	 * The feature id for the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK__ABSTRACT = PUBLICATION__ABSTRACT;

	/**
	 * The feature id for the '<em><b>With Author Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK__WITH_AUTHOR_VERSION = PUBLICATION__WITH_AUTHOR_VERSION;

	/**
	 * The feature id for the '<em><b>Notes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK__NOTES = PUBLICATION__NOTES;

	/**
	 * The feature id for the '<em><b>Related Projects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK__RELATED_PROJECTS = PUBLICATION__RELATED_PROJECTS;

	/**
	 * The feature id for the '<em><b>Publisher</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK__PUBLISHER = PUBLICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Series</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK__SERIES = PUBLICATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Volume</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK__VOLUME = PUBLICATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>ISBN</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK__ISBN = PUBLICATION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Book</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_FEATURE_COUNT = PUBLICATION_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Book</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_OPERATION_COUNT = PUBLICATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.BookChapterImpl <em>Book Chapter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.BookChapterImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getBookChapter()
	 * @generated
	 */
	int BOOK_CHAPTER = 15;

	/**
	 * The feature id for the '<em><b>Citekey</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_CHAPTER__CITEKEY = IN_COLLECTION__CITEKEY;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_CHAPTER__TITLE = IN_COLLECTION__TITLE;

	/**
	 * The feature id for the '<em><b>Authors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_CHAPTER__AUTHORS = IN_COLLECTION__AUTHORS;

	/**
	 * The feature id for the '<em><b>URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_CHAPTER__URL = IN_COLLECTION__URL;

	/**
	 * The feature id for the '<em><b>Published</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_CHAPTER__PUBLISHED = IN_COLLECTION__PUBLISHED;

	/**
	 * The feature id for the '<em><b>Open Access</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_CHAPTER__OPEN_ACCESS = IN_COLLECTION__OPEN_ACCESS;

	/**
	 * The feature id for the '<em><b>Date</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_CHAPTER__DATE = IN_COLLECTION__DATE;

	/**
	 * The feature id for the '<em><b>DOI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_CHAPTER__DOI = IN_COLLECTION__DOI;

	/**
	 * The feature id for the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_CHAPTER__ABSTRACT = IN_COLLECTION__ABSTRACT;

	/**
	 * The feature id for the '<em><b>With Author Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_CHAPTER__WITH_AUTHOR_VERSION = IN_COLLECTION__WITH_AUTHOR_VERSION;

	/**
	 * The feature id for the '<em><b>Notes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_CHAPTER__NOTES = IN_COLLECTION__NOTES;

	/**
	 * The feature id for the '<em><b>Related Projects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_CHAPTER__RELATED_PROJECTS = IN_COLLECTION__RELATED_PROJECTS;

	/**
	 * The feature id for the '<em><b>Booktitle</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_CHAPTER__BOOKTITLE = IN_COLLECTION__BOOKTITLE;

	/**
	 * The feature id for the '<em><b>Bookeditors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_CHAPTER__BOOKEDITORS = IN_COLLECTION__BOOKEDITORS;

	/**
	 * The feature id for the '<em><b>Publisher</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_CHAPTER__PUBLISHER = IN_COLLECTION__PUBLISHER;

	/**
	 * The feature id for the '<em><b>Series</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_CHAPTER__SERIES = IN_COLLECTION__SERIES;

	/**
	 * The feature id for the '<em><b>Volume</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_CHAPTER__VOLUME = IN_COLLECTION__VOLUME;

	/**
	 * The feature id for the '<em><b>First Page</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_CHAPTER__FIRST_PAGE = IN_COLLECTION__FIRST_PAGE;

	/**
	 * The feature id for the '<em><b>Last Page</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_CHAPTER__LAST_PAGE = IN_COLLECTION__LAST_PAGE;

	/**
	 * The feature id for the '<em><b>ISBN</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_CHAPTER__ISBN = IN_COLLECTION__ISBN;

	/**
	 * The feature id for the '<em><b>Chapter Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_CHAPTER__CHAPTER_NUMBER = IN_COLLECTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Book Chapter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_CHAPTER_FEATURE_COUNT = IN_COLLECTION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Book Chapter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_CHAPTER_OPERATION_COUNT = IN_COLLECTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.ReportImpl <em>Report</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.ReportImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getReport()
	 * @generated
	 */
	int REPORT = 16;

	/**
	 * The feature id for the '<em><b>Citekey</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT__CITEKEY = PUBLICATION__CITEKEY;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT__TITLE = PUBLICATION__TITLE;

	/**
	 * The feature id for the '<em><b>Authors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT__AUTHORS = PUBLICATION__AUTHORS;

	/**
	 * The feature id for the '<em><b>URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT__URL = PUBLICATION__URL;

	/**
	 * The feature id for the '<em><b>Published</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT__PUBLISHED = PUBLICATION__PUBLISHED;

	/**
	 * The feature id for the '<em><b>Open Access</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT__OPEN_ACCESS = PUBLICATION__OPEN_ACCESS;

	/**
	 * The feature id for the '<em><b>Date</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT__DATE = PUBLICATION__DATE;

	/**
	 * The feature id for the '<em><b>DOI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT__DOI = PUBLICATION__DOI;

	/**
	 * The feature id for the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT__ABSTRACT = PUBLICATION__ABSTRACT;

	/**
	 * The feature id for the '<em><b>With Author Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT__WITH_AUTHOR_VERSION = PUBLICATION__WITH_AUTHOR_VERSION;

	/**
	 * The feature id for the '<em><b>Notes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT__NOTES = PUBLICATION__NOTES;

	/**
	 * The feature id for the '<em><b>Related Projects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT__RELATED_PROJECTS = PUBLICATION__RELATED_PROJECTS;

	/**
	 * The feature id for the '<em><b>Report Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT__REPORT_NUMBER = PUBLICATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Institution</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT__INSTITUTION = PUBLICATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Report</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT_FEATURE_COUNT = PUBLICATION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Report</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORT_OPERATION_COUNT = PUBLICATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.MiscellaneousImpl <em>Miscellaneous</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.MiscellaneousImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getMiscellaneous()
	 * @generated
	 */
	int MISCELLANEOUS = 17;

	/**
	 * The feature id for the '<em><b>Citekey</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISCELLANEOUS__CITEKEY = PUBLICATION__CITEKEY;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISCELLANEOUS__TITLE = PUBLICATION__TITLE;

	/**
	 * The feature id for the '<em><b>Authors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISCELLANEOUS__AUTHORS = PUBLICATION__AUTHORS;

	/**
	 * The feature id for the '<em><b>URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISCELLANEOUS__URL = PUBLICATION__URL;

	/**
	 * The feature id for the '<em><b>Published</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISCELLANEOUS__PUBLISHED = PUBLICATION__PUBLISHED;

	/**
	 * The feature id for the '<em><b>Open Access</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISCELLANEOUS__OPEN_ACCESS = PUBLICATION__OPEN_ACCESS;

	/**
	 * The feature id for the '<em><b>Date</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISCELLANEOUS__DATE = PUBLICATION__DATE;

	/**
	 * The feature id for the '<em><b>DOI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISCELLANEOUS__DOI = PUBLICATION__DOI;

	/**
	 * The feature id for the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISCELLANEOUS__ABSTRACT = PUBLICATION__ABSTRACT;

	/**
	 * The feature id for the '<em><b>With Author Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISCELLANEOUS__WITH_AUTHOR_VERSION = PUBLICATION__WITH_AUTHOR_VERSION;

	/**
	 * The feature id for the '<em><b>Notes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISCELLANEOUS__NOTES = PUBLICATION__NOTES;

	/**
	 * The feature id for the '<em><b>Related Projects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISCELLANEOUS__RELATED_PROJECTS = PUBLICATION__RELATED_PROJECTS;

	/**
	 * The number of structural features of the '<em>Miscellaneous</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISCELLANEOUS_FEATURE_COUNT = PUBLICATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Miscellaneous</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISCELLANEOUS_OPERATION_COUNT = PUBLICATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.PublishedDateImpl <em>Published Date</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.PublishedDateImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getPublishedDate()
	 * @generated
	 */
	int PUBLISHED_DATE = 18;

	/**
	 * The feature id for the '<em><b>Day</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLISHED_DATE__DAY = 0;

	/**
	 * The feature id for the '<em><b>Month</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLISHED_DATE__MONTH = 1;

	/**
	 * The feature id for the '<em><b>Year</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLISHED_DATE__YEAR = 2;

	/**
	 * The feature id for the '<em><b>Month Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLISHED_DATE__MONTH_NAME = 3;

	/**
	 * The feature id for the '<em><b>Month Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLISHED_DATE__MONTH_SHORT_NAME = 4;

	/**
	 * The number of structural features of the '<em>Published Date</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLISHED_DATE_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Published Date</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLISHED_DATE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.SerialNumberImpl <em>Serial Number</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.SerialNumberImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getSerialNumber()
	 * @generated
	 */
	int SERIAL_NUMBER = 19;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERIAL_NUMBER__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERIAL_NUMBER__KIND = 1;

	/**
	 * The number of structural features of the '<em>Serial Number</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERIAL_NUMBER_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Serial Number</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERIAL_NUMBER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.ResearchGroupImpl <em>Research Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.ResearchGroupImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getResearchGroup()
	 * @generated
	 */
	int RESEARCH_GROUP = 20;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESEARCH_GROUP__NAME = 0;

	/**
	 * The feature id for the '<em><b>Acronym</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESEARCH_GROUP__ACRONYM = 1;

	/**
	 * The feature id for the '<em><b>URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESEARCH_GROUP__URL = 2;

	/**
	 * The number of structural features of the '<em>Research Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESEARCH_GROUP_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Research Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESEARCH_GROUP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.CourseImpl <em>Course</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.CourseImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getCourse()
	 * @generated
	 */
	int COURSE = 21;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CODE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__NAME = 1;

	/**
	 * The feature id for the '<em><b>Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__LEVEL = 2;

	/**
	 * The feature id for the '<em><b>Hours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__HOURS = 3;

	/**
	 * The feature id for the '<em><b>Credits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CREDITS = 4;

	/**
	 * The number of structural features of the '<em>Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.TaughtCourseImpl <em>Taught Course</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.TaughtCourseImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getTaughtCourse()
	 * @generated
	 */
	int TAUGHT_COURSE = 22;

	/**
	 * The feature id for the '<em><b>Year</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAUGHT_COURSE__YEAR = 0;

	/**
	 * The feature id for the '<em><b>Period</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAUGHT_COURSE__PERIOD = 1;

	/**
	 * The feature id for the '<em><b>Webinfo</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAUGHT_COURSE__WEBINFO = 2;

	/**
	 * The feature id for the '<em><b>Website</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAUGHT_COURSE__WEBSITE = 3;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAUGHT_COURSE__CODE = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAUGHT_COURSE__NAME = 5;

	/**
	 * The feature id for the '<em><b>Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAUGHT_COURSE__LEVEL = 6;

	/**
	 * The feature id for the '<em><b>Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAUGHT_COURSE__LANGUAGE = 7;

	/**
	 * The feature id for the '<em><b>Canceled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAUGHT_COURSE__CANCELED = 8;

	/**
	 * The feature id for the '<em><b>Responsibility</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAUGHT_COURSE__RESPONSIBILITY = 9;

	/**
	 * The feature id for the '<em><b>Offering</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAUGHT_COURSE__OFFERING = 10;

	/**
	 * The number of structural features of the '<em>Taught Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAUGHT_COURSE_FEATURE_COUNT = 11;

	/**
	 * The number of operations of the '<em>Taught Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAUGHT_COURSE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.ResearchTopicImpl <em>Research Topic</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.ResearchTopicImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getResearchTopic()
	 * @generated
	 */
	int RESEARCH_TOPIC = 23;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESEARCH_TOPIC__TITLE = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESEARCH_TOPIC__DESCRIPTION = 1;

	/**
	 * The feature id for the '<em><b>Related Papers</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESEARCH_TOPIC__RELATED_PAPERS = 2;

	/**
	 * The number of structural features of the '<em>Research Topic</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESEARCH_TOPIC_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Research Topic</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESEARCH_TOPIC_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.GrantImpl <em>Grant</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.GrantImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getGrant()
	 * @generated
	 */
	int GRANT = 25;

	/**
	 * The feature id for the '<em><b>Related Publications</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRANT__RELATED_PUBLICATIONS = 0;

	/**
	 * The feature id for the '<em><b>URL</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRANT__URL = 1;

	/**
	 * The feature id for the '<em><b>Agency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRANT__AGENCY = 2;

	/**
	 * The feature id for the '<em><b>Program</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRANT__PROGRAM = 3;

	/**
	 * The feature id for the '<em><b>Call</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRANT__CALL = 4;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRANT__NUMBER = 5;

	/**
	 * The feature id for the '<em><b>Budget</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRANT__BUDGET = 6;

	/**
	 * The feature id for the '<em><b>Start Date</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRANT__START_DATE = 7;

	/**
	 * The feature id for the '<em><b>End Date</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRANT__END_DATE = 8;

	/**
	 * The feature id for the '<em><b>Grant ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRANT__GRANT_ID = 9;

	/**
	 * The feature id for the '<em><b>Role</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRANT__ROLE = 10;

	/**
	 * The number of structural features of the '<em>Grant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRANT_FEATURE_COUNT = 11;

	/**
	 * The operation id for the '<em>Is Active</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRANT___IS_ACTIVE = 0;

	/**
	 * The number of operations of the '<em>Grant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRANT_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.ProjectImpl <em>Project</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.ProjectImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getProject()
	 * @generated
	 */
	int PROJECT = 24;

	/**
	 * The feature id for the '<em><b>Related Publications</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT__RELATED_PUBLICATIONS = GRANT__RELATED_PUBLICATIONS;

	/**
	 * The feature id for the '<em><b>URL</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT__URL = GRANT__URL;

	/**
	 * The feature id for the '<em><b>Agency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT__AGENCY = GRANT__AGENCY;

	/**
	 * The feature id for the '<em><b>Program</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT__PROGRAM = GRANT__PROGRAM;

	/**
	 * The feature id for the '<em><b>Call</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT__CALL = GRANT__CALL;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT__NUMBER = GRANT__NUMBER;

	/**
	 * The feature id for the '<em><b>Budget</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT__BUDGET = GRANT__BUDGET;

	/**
	 * The feature id for the '<em><b>Start Date</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT__START_DATE = GRANT__START_DATE;

	/**
	 * The feature id for the '<em><b>End Date</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT__END_DATE = GRANT__END_DATE;

	/**
	 * The feature id for the '<em><b>Grant ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT__GRANT_ID = GRANT__GRANT_ID;

	/**
	 * The feature id for the '<em><b>Role</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT__ROLE = GRANT__ROLE;

	/**
	 * The feature id for the '<em><b>Acronym</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT__ACRONYM = GRANT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT__TITLE = GRANT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT__SHORT_NAME = GRANT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Project</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT_FEATURE_COUNT = GRANT_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Is Active</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT___IS_ACTIVE = GRANT___IS_ACTIVE;

	/**
	 * The number of operations of the '<em>Project</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT_OPERATION_COUNT = GRANT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.BudgetImpl <em>Budget</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.BudgetImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getBudget()
	 * @generated
	 */
	int BUDGET = 26;

	/**
	 * The feature id for the '<em><b>Amount</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUDGET__AMOUNT = 0;

	/**
	 * The feature id for the '<em><b>Currency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUDGET__CURRENCY = 1;

	/**
	 * The number of structural features of the '<em>Budget</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUDGET_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Budget</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUDGET_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.RootElementImpl <em>Root Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.RootElementImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getRootElement()
	 * @generated
	 */
	int ROOT_ELEMENT = 27;

	/**
	 * The number of structural features of the '<em>Root Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT_ELEMENT_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Root Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOT_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.PersonRegistryImpl <em>Person Registry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.PersonRegistryImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getPersonRegistry()
	 * @generated
	 */
	int PERSON_REGISTRY = 28;

	/**
	 * The feature id for the '<em><b>Person</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_REGISTRY__PERSON = ROOT_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Person Registry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_REGISTRY_FEATURE_COUNT = ROOT_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Person Registry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_REGISTRY_OPERATION_COUNT = ROOT_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.CourseOfferingImpl <em>Course Offering</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.CourseOfferingImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getCourseOffering()
	 * @generated
	 */
	int COURSE_OFFERING = 29;

	/**
	 * The feature id for the '<em><b>Groups</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_OFFERING__GROUPS = 0;

	/**
	 * The feature id for the '<em><b>Specific Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_OFFERING__SPECIFIC_NAME = 1;

	/**
	 * The feature id for the '<em><b>Course</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_OFFERING__COURSE = 2;

	/**
	 * The number of structural features of the '<em>Course Offering</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_OFFERING_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Course Offering</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_OFFERING_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.SupervisionImpl <em>Supervision</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.SupervisionImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getSupervision()
	 * @generated
	 */
	int SUPERVISION = 30;

	/**
	 * The feature id for the '<em><b>Daste Start</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPERVISION__DASTE_START = 0;

	/**
	 * The feature id for the '<em><b>Date End</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPERVISION__DATE_END = 1;

	/**
	 * The feature id for the '<em><b>Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPERVISION__LEVEL = 2;

	/**
	 * The feature id for the '<em><b>Student</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPERVISION__STUDENT = 3;

	/**
	 * The feature id for the '<em><b>Project Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPERVISION__PROJECT_TITLE = 4;

	/**
	 * The feature id for the '<em><b>Supervisors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPERVISION__SUPERVISORS = 5;

	/**
	 * The feature id for the '<em><b>Institutions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPERVISION__INSTITUTIONS = 6;

	/**
	 * The feature id for the '<em><b>Main Institution</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPERVISION__MAIN_INSTITUTION = 7;

	/**
	 * The feature id for the '<em><b>Main Supervisor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPERVISION__MAIN_SUPERVISOR = 8;

	/**
	 * The feature id for the '<em><b>Completed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPERVISION__COMPLETED = 9;

	/**
	 * The feature id for the '<em><b>Final Work URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPERVISION__FINAL_WORK_URL = 10;

	/**
	 * The feature id for the '<em><b>Final Work DOI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPERVISION__FINAL_WORK_DOI = 11;

	/**
	 * The number of structural features of the '<em>Supervision</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPERVISION_FEATURE_COUNT = 12;

	/**
	 * The number of operations of the '<em>Supervision</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPERVISION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.InstitutionRegistryImpl <em>Institution Registry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.InstitutionRegistryImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getInstitutionRegistry()
	 * @generated
	 */
	int INSTITUTION_REGISTRY = 31;

	/**
	 * The feature id for the '<em><b>Institutions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTITUTION_REGISTRY__INSTITUTIONS = ROOT_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Institution Registry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTITUTION_REGISTRY_FEATURE_COUNT = ROOT_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Institution Registry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTITUTION_REGISTRY_OPERATION_COUNT = ROOT_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.InstitutionImpl <em>Institution</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.InstitutionImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getInstitution()
	 * @generated
	 */
	int INSTITUTION = 32;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTITUTION__NAME = 0;

	/**
	 * The feature id for the '<em><b>Units</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTITUTION__UNITS = 1;

	/**
	 * The feature id for the '<em><b>Country</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTITUTION__COUNTRY = 2;

	/**
	 * The feature id for the '<em><b>Full Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTITUTION__FULL_NAME = 3;

	/**
	 * The number of structural features of the '<em>Institution</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTITUTION_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Institution</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTITUTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.MultiLanguageStringImpl <em>Multi Language String</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.MultiLanguageStringImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getMultiLanguageString()
	 * @generated
	 */
	int MULTI_LANGUAGE_STRING = 33;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_STRING__TEXT = 0;

	/**
	 * The feature id for the '<em><b>Localizations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_STRING__LOCALIZATIONS = 1;

	/**
	 * The feature id for the '<em><b>Base Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_STRING__BASE_LANGUAGE = 2;

	/**
	 * The number of structural features of the '<em>Multi Language String</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_STRING_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Multi Language String</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_LANGUAGE_STRING_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.LocalizedStringImpl <em>Localized String</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.LocalizedStringImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getLocalizedString()
	 * @generated
	 */
	int LOCALIZED_STRING = 34;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCALIZED_STRING__TEXT = 0;

	/**
	 * The feature id for the '<em><b>Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCALIZED_STRING__LANGUAGE = 1;

	/**
	 * The number of structural features of the '<em>Localized String</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCALIZED_STRING_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Localized String</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCALIZED_STRING_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.impl.SupervisionsRegistryImpl <em>Supervisions Registry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.impl.SupervisionsRegistryImpl
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getSupervisionsRegistry()
	 * @generated
	 */
	int SUPERVISIONS_REGISTRY = 35;

	/**
	 * The feature id for the '<em><b>Supervisions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPERVISIONS_REGISTRY__SUPERVISIONS = ROOT_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Supervisions Registry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPERVISIONS_REGISTRY_FEATURE_COUNT = ROOT_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Supervisions Registry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUPERVISIONS_REGISTRY_OPERATION_COUNT = ROOT_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.ContactKind <em>Contact Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.ContactKind
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getContactKind()
	 * @generated
	 */
	int CONTACT_KIND = 36;


	/**
	 * The meta object id for the '{@link org.montex.researchcv.SerialNumberKind <em>Serial Number Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.SerialNumberKind
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getSerialNumberKind()
	 * @generated
	 */
	int SERIAL_NUMBER_KIND = 37;


	/**
	 * The meta object id for the '{@link org.montex.researchcv.CourseKind <em>Course Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.CourseKind
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getCourseKind()
	 * @generated
	 */
	int COURSE_KIND = 38;


	/**
	 * The meta object id for the '{@link org.montex.researchcv.SupervisionKind <em>Supervision Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.SupervisionKind
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getSupervisionKind()
	 * @generated
	 */
	int SUPERVISION_KIND = 39;

	/**
	 * The meta object id for the '{@link org.montex.researchcv.ParticipationKind <em>Participation Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.montex.researchcv.ParticipationKind
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getParticipationKind()
	 * @generated
	 */
	int PARTICIPATION_KIND = 40;

	/**
	 * The meta object id for the '<em>Currency</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.util.Currency
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getCurrency()
	 * @generated
	 */
	int CURRENCY = 41;


	/**
	 * The meta object id for the '<em>Language Code</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.util.Locale
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getLanguageCode()
	 * @generated
	 */
	int LANGUAGE_CODE = 42;

	/**
	 * The meta object id for the '<em>Country Code</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.util.Locale
	 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getCountryCode()
	 * @generated
	 */
	int COUNTRY_CODE = 43;

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.Person <em>Person</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Person</em>'.
	 * @see org.montex.researchcv.Person
	 * @generated
	 */
	EClass getPerson();

	/**
	 * Returns the meta object for the attribute list '{@link org.montex.researchcv.Person#getFirstNames <em>First Names</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>First Names</em>'.
	 * @see org.montex.researchcv.Person#getFirstNames()
	 * @see #getPerson()
	 * @generated
	 */
	EAttribute getPerson_FirstNames();

	/**
	 * Returns the meta object for the attribute list '{@link org.montex.researchcv.Person#getLastNames <em>Last Names</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Last Names</em>'.
	 * @see org.montex.researchcv.Person#getLastNames()
	 * @see #getPerson()
	 * @generated
	 */
	EAttribute getPerson_LastNames();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Person#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.montex.researchcv.Person#getName()
	 * @see #getPerson()
	 * @generated
	 */
	EAttribute getPerson_Name();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.Researcher <em>Researcher</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Researcher</em>'.
	 * @see org.montex.researchcv.Researcher
	 * @generated
	 */
	EClass getResearcher();

	/**
	 * Returns the meta object for the reference list '{@link org.montex.researchcv.Researcher#getCoauthors <em>Coauthors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Coauthors</em>'.
	 * @see org.montex.researchcv.Researcher#getCoauthors()
	 * @see #getResearcher()
	 * @generated
	 */
	EReference getResearcher_Coauthors();

	/**
	 * Returns the meta object for the containment reference list '{@link org.montex.researchcv.Researcher#getContacts <em>Contacts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Contacts</em>'.
	 * @see org.montex.researchcv.Researcher#getContacts()
	 * @see #getResearcher()
	 * @generated
	 */
	EReference getResearcher_Contacts();

	/**
	 * Returns the meta object for the containment reference list '{@link org.montex.researchcv.Researcher#getSocials <em>Socials</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Socials</em>'.
	 * @see org.montex.researchcv.Researcher#getSocials()
	 * @see #getResearcher()
	 * @generated
	 */
	EReference getResearcher_Socials();

	/**
	 * Returns the meta object for the containment reference list '{@link org.montex.researchcv.Researcher#getPublications <em>Publications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Publications</em>'.
	 * @see org.montex.researchcv.Researcher#getPublications()
	 * @see #getResearcher()
	 * @generated
	 */
	EReference getResearcher_Publications();

	/**
	 * Returns the meta object for the containment reference list '{@link org.montex.researchcv.Researcher#getTeachingMaterial <em>Teaching Material</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Teaching Material</em>'.
	 * @see org.montex.researchcv.Researcher#getTeachingMaterial()
	 * @see #getResearcher()
	 * @generated
	 */
	EReference getResearcher_TeachingMaterial();

	/**
	 * Returns the meta object for the containment reference list '{@link org.montex.researchcv.Researcher#getOtherMaterial <em>Other Material</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Other Material</em>'.
	 * @see org.montex.researchcv.Researcher#getOtherMaterial()
	 * @see #getResearcher()
	 * @generated
	 */
	EReference getResearcher_OtherMaterial();

	/**
	 * Returns the meta object for the containment reference '{@link org.montex.researchcv.Researcher#getResearchGroup <em>Research Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Research Group</em>'.
	 * @see org.montex.researchcv.Researcher#getResearchGroup()
	 * @see #getResearcher()
	 * @generated
	 */
	EReference getResearcher_ResearchGroup();

	/**
	 * Returns the meta object for the containment reference list '{@link org.montex.researchcv.Researcher#getTopics <em>Topics</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Topics</em>'.
	 * @see org.montex.researchcv.Researcher#getTopics()
	 * @see #getResearcher()
	 * @generated
	 */
	EReference getResearcher_Topics();

	/**
	 * Returns the meta object for the containment reference list '{@link org.montex.researchcv.Researcher#getGrants <em>Grants</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Grants</em>'.
	 * @see org.montex.researchcv.Researcher#getGrants()
	 * @see #getResearcher()
	 * @generated
	 */
	EReference getResearcher_Grants();

	/**
	 * Returns the meta object for the containment reference list '{@link org.montex.researchcv.Researcher#getCourses <em>Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Courses</em>'.
	 * @see org.montex.researchcv.Researcher#getCourses()
	 * @see #getResearcher()
	 * @generated
	 */
	EReference getResearcher_Courses();

	/**
	 * Returns the meta object for the containment reference list '{@link org.montex.researchcv.Researcher#getTaughtCourses <em>Taught Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Taught Courses</em>'.
	 * @see org.montex.researchcv.Researcher#getTaughtCourses()
	 * @see #getResearcher()
	 * @generated
	 */
	EReference getResearcher_TaughtCourses();

	/**
	 * Returns the meta object for the reference '{@link org.montex.researchcv.Researcher#getSupervisionsRegistry <em>Supervisions Registry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Supervisions Registry</em>'.
	 * @see org.montex.researchcv.Researcher#getSupervisionsRegistry()
	 * @see #getResearcher()
	 * @generated
	 */
	EReference getResearcher_SupervisionsRegistry();

	/**
	 * Returns the meta object for the reference list '{@link org.montex.researchcv.Researcher#getSupervisions <em>Supervisions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Supervisions</em>'.
	 * @see org.montex.researchcv.Researcher#getSupervisions()
	 * @see #getResearcher()
	 * @generated
	 */
	EReference getResearcher_Supervisions();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.Address <em>Address</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Address</em>'.
	 * @see org.montex.researchcv.Address
	 * @generated
	 */
	EClass getAddress();

	/**
	 * Returns the meta object for the attribute list '{@link org.montex.researchcv.Address#getLines <em>Lines</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Lines</em>'.
	 * @see org.montex.researchcv.Address#getLines()
	 * @see #getAddress()
	 * @generated
	 */
	EAttribute getAddress_Lines();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Address#getZip <em>Zip</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Zip</em>'.
	 * @see org.montex.researchcv.Address#getZip()
	 * @see #getAddress()
	 * @generated
	 */
	EAttribute getAddress_Zip();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Address#getCity <em>City</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>City</em>'.
	 * @see org.montex.researchcv.Address#getCity()
	 * @see #getAddress()
	 * @generated
	 */
	EAttribute getAddress_City();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Address#getRegion <em>Region</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Region</em>'.
	 * @see org.montex.researchcv.Address#getRegion()
	 * @see #getAddress()
	 * @generated
	 */
	EAttribute getAddress_Region();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Address#getCountry <em>Country</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Country</em>'.
	 * @see org.montex.researchcv.Address#getCountry()
	 * @see #getAddress()
	 * @generated
	 */
	EAttribute getAddress_Country();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Address#getBuilding <em>Building</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Building</em>'.
	 * @see org.montex.researchcv.Address#getBuilding()
	 * @see #getAddress()
	 * @generated
	 */
	EAttribute getAddress_Building();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Address#getRoom <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room</em>'.
	 * @see org.montex.researchcv.Address#getRoom()
	 * @see #getAddress()
	 * @generated
	 */
	EAttribute getAddress_Room();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.Contact <em>Contact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Contact</em>'.
	 * @see org.montex.researchcv.Contact
	 * @generated
	 */
	EClass getContact();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Contact#getKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind</em>'.
	 * @see org.montex.researchcv.Contact#getKind()
	 * @see #getContact()
	 * @generated
	 */
	EAttribute getContact_Kind();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.Email <em>Email</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Email</em>'.
	 * @see org.montex.researchcv.Email
	 * @generated
	 */
	EClass getEmail();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Email#getAddress <em>Address</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Address</em>'.
	 * @see org.montex.researchcv.Email#getAddress()
	 * @see #getEmail()
	 * @generated
	 */
	EAttribute getEmail_Address();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.Phone <em>Phone</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Phone</em>'.
	 * @see org.montex.researchcv.Phone
	 * @generated
	 */
	EClass getPhone();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Phone#getNumber <em>Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number</em>'.
	 * @see org.montex.researchcv.Phone#getNumber()
	 * @see #getPhone()
	 * @generated
	 */
	EAttribute getPhone_Number();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.SocialProfile <em>Social Profile</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Social Profile</em>'.
	 * @see org.montex.researchcv.SocialProfile
	 * @generated
	 */
	EClass getSocialProfile();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.SocialProfile#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.montex.researchcv.SocialProfile#getName()
	 * @see #getSocialProfile()
	 * @generated
	 */
	EAttribute getSocialProfile_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.SocialProfile#getUrl <em>Url</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Url</em>'.
	 * @see org.montex.researchcv.SocialProfile#getUrl()
	 * @see #getSocialProfile()
	 * @generated
	 */
	EAttribute getSocialProfile_Url();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.Website <em>Website</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Website</em>'.
	 * @see org.montex.researchcv.Website
	 * @generated
	 */
	EClass getWebsite();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Website#getUrl <em>Url</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Url</em>'.
	 * @see org.montex.researchcv.Website#getUrl()
	 * @see #getWebsite()
	 * @generated
	 */
	EAttribute getWebsite_Url();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.Publication <em>Publication</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Publication</em>'.
	 * @see org.montex.researchcv.Publication
	 * @generated
	 */
	EClass getPublication();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Publication#getCitekey <em>Citekey</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Citekey</em>'.
	 * @see org.montex.researchcv.Publication#getCitekey()
	 * @see #getPublication()
	 * @generated
	 */
	EAttribute getPublication_Citekey();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Publication#getTitle <em>Title</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Title</em>'.
	 * @see org.montex.researchcv.Publication#getTitle()
	 * @see #getPublication()
	 * @generated
	 */
	EAttribute getPublication_Title();

	/**
	 * Returns the meta object for the reference list '{@link org.montex.researchcv.Publication#getAuthors <em>Authors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Authors</em>'.
	 * @see org.montex.researchcv.Publication#getAuthors()
	 * @see #getPublication()
	 * @generated
	 */
	EReference getPublication_Authors();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Publication#getURL <em>URL</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>URL</em>'.
	 * @see org.montex.researchcv.Publication#getURL()
	 * @see #getPublication()
	 * @generated
	 */
	EAttribute getPublication_URL();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Publication#isPublished <em>Published</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Published</em>'.
	 * @see org.montex.researchcv.Publication#isPublished()
	 * @see #getPublication()
	 * @generated
	 */
	EAttribute getPublication_Published();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Publication#isOpenAccess <em>Open Access</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Open Access</em>'.
	 * @see org.montex.researchcv.Publication#isOpenAccess()
	 * @see #getPublication()
	 * @generated
	 */
	EAttribute getPublication_OpenAccess();

	/**
	 * Returns the meta object for the containment reference '{@link org.montex.researchcv.Publication#getDate <em>Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Date</em>'.
	 * @see org.montex.researchcv.Publication#getDate()
	 * @see #getPublication()
	 * @generated
	 */
	EReference getPublication_Date();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Publication#getDOI <em>DOI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>DOI</em>'.
	 * @see org.montex.researchcv.Publication#getDOI()
	 * @see #getPublication()
	 * @generated
	 */
	EAttribute getPublication_DOI();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Publication#getAbstract <em>Abstract</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abstract</em>'.
	 * @see org.montex.researchcv.Publication#getAbstract()
	 * @see #getPublication()
	 * @generated
	 */
	EAttribute getPublication_Abstract();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Publication#isWithAuthorVersion <em>With Author Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>With Author Version</em>'.
	 * @see org.montex.researchcv.Publication#isWithAuthorVersion()
	 * @see #getPublication()
	 * @generated
	 */
	EAttribute getPublication_WithAuthorVersion();

	/**
	 * Returns the meta object for the attribute list '{@link org.montex.researchcv.Publication#getNotes <em>Notes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Notes</em>'.
	 * @see org.montex.researchcv.Publication#getNotes()
	 * @see #getPublication()
	 * @generated
	 */
	EAttribute getPublication_Notes();

	/**
	 * Returns the meta object for the reference list '{@link org.montex.researchcv.Publication#getRelatedProjects <em>Related Projects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Related Projects</em>'.
	 * @see org.montex.researchcv.Publication#getRelatedProjects()
	 * @see #getPublication()
	 * @generated
	 */
	EReference getPublication_RelatedProjects();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.Journal <em>Journal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Journal</em>'.
	 * @see org.montex.researchcv.Journal
	 * @generated
	 */
	EClass getJournal();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Journal#getJournalName <em>Journal Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Journal Name</em>'.
	 * @see org.montex.researchcv.Journal#getJournalName()
	 * @see #getJournal()
	 * @generated
	 */
	EAttribute getJournal_JournalName();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Journal#getPublisher <em>Publisher</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Publisher</em>'.
	 * @see org.montex.researchcv.Journal#getPublisher()
	 * @see #getJournal()
	 * @generated
	 */
	EAttribute getJournal_Publisher();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Journal#getFirstPage <em>First Page</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>First Page</em>'.
	 * @see org.montex.researchcv.Journal#getFirstPage()
	 * @see #getJournal()
	 * @generated
	 */
	EAttribute getJournal_FirstPage();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Journal#getLastPage <em>Last Page</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Page</em>'.
	 * @see org.montex.researchcv.Journal#getLastPage()
	 * @see #getJournal()
	 * @generated
	 */
	EAttribute getJournal_LastPage();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Journal#getVolume <em>Volume</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Volume</em>'.
	 * @see org.montex.researchcv.Journal#getVolume()
	 * @see #getJournal()
	 * @generated
	 */
	EAttribute getJournal_Volume();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Journal#getIssue <em>Issue</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Issue</em>'.
	 * @see org.montex.researchcv.Journal#getIssue()
	 * @see #getJournal()
	 * @generated
	 */
	EAttribute getJournal_Issue();

	/**
	 * Returns the meta object for the containment reference list '{@link org.montex.researchcv.Journal#getISSN <em>ISSN</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>ISSN</em>'.
	 * @see org.montex.researchcv.Journal#getISSN()
	 * @see #getJournal()
	 * @generated
	 */
	EReference getJournal_ISSN();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Journal#isMagazine <em>Magazine</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Magazine</em>'.
	 * @see org.montex.researchcv.Journal#isMagazine()
	 * @see #getJournal()
	 * @generated
	 */
	EAttribute getJournal_Magazine();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.Conference <em>Conference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Conference</em>'.
	 * @see org.montex.researchcv.Conference
	 * @generated
	 */
	EClass getConference();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.Workshop <em>Workshop</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Workshop</em>'.
	 * @see org.montex.researchcv.Workshop
	 * @generated
	 */
	EClass getWorkshop();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Workshop#getMainEventName <em>Main Event Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Main Event Name</em>'.
	 * @see org.montex.researchcv.Workshop#getMainEventName()
	 * @see #getWorkshop()
	 * @generated
	 */
	EAttribute getWorkshop_MainEventName();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Workshop#getMainEventShortName <em>Main Event Short Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Main Event Short Name</em>'.
	 * @see org.montex.researchcv.Workshop#getMainEventShortName()
	 * @see #getWorkshop()
	 * @generated
	 */
	EAttribute getWorkshop_MainEventShortName();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.InProceedings <em>In Proceedings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>In Proceedings</em>'.
	 * @see org.montex.researchcv.InProceedings
	 * @generated
	 */
	EClass getInProceedings();

	/**
	 * Returns the meta object for the containment reference '{@link org.montex.researchcv.InProceedings#getEventEndDate <em>Event End Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Event End Date</em>'.
	 * @see org.montex.researchcv.InProceedings#getEventEndDate()
	 * @see #getInProceedings()
	 * @generated
	 */
	EReference getInProceedings_EventEndDate();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.InProceedings#getEventName <em>Event Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Event Name</em>'.
	 * @see org.montex.researchcv.InProceedings#getEventName()
	 * @see #getInProceedings()
	 * @generated
	 */
	EAttribute getInProceedings_EventName();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.InProceedings#getVenue <em>Venue</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Venue</em>'.
	 * @see org.montex.researchcv.InProceedings#getVenue()
	 * @see #getInProceedings()
	 * @generated
	 */
	EAttribute getInProceedings_Venue();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.InProceedings#getEventShortName <em>Event Short Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Event Short Name</em>'.
	 * @see org.montex.researchcv.InProceedings#getEventShortName()
	 * @see #getInProceedings()
	 * @generated
	 */
	EAttribute getInProceedings_EventShortName();

	/**
	 * Returns the meta object for the reference '{@link org.montex.researchcv.InProceedings#getEventStartDate <em>Event Start Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Event Start Date</em>'.
	 * @see org.montex.researchcv.InProceedings#getEventStartDate()
	 * @see #getInProceedings()
	 * @generated
	 */
	EReference getInProceedings_EventStartDate();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.InProceedings#getTrackName <em>Track Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Track Name</em>'.
	 * @see org.montex.researchcv.InProceedings#getTrackName()
	 * @see #getInProceedings()
	 * @generated
	 */
	EAttribute getInProceedings_TrackName();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.InProceedings#getTrackShortName <em>Track Short Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Track Short Name</em>'.
	 * @see org.montex.researchcv.InProceedings#getTrackShortName()
	 * @see #getInProceedings()
	 * @generated
	 */
	EAttribute getInProceedings_TrackShortName();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.InProceedings#isShortPaper <em>Short Paper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Short Paper</em>'.
	 * @see org.montex.researchcv.InProceedings#isShortPaper()
	 * @see #getInProceedings()
	 * @generated
	 */
	EAttribute getInProceedings_ShortPaper();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.InProceedings#isToolPaper <em>Tool Paper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tool Paper</em>'.
	 * @see org.montex.researchcv.InProceedings#isToolPaper()
	 * @see #getInProceedings()
	 * @generated
	 */
	EAttribute getInProceedings_ToolPaper();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.InProceedings#isInvitedPaper <em>Invited Paper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Invited Paper</em>'.
	 * @see org.montex.researchcv.InProceedings#isInvitedPaper()
	 * @see #getInProceedings()
	 * @generated
	 */
	EAttribute getInProceedings_InvitedPaper();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.InCollection <em>In Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>In Collection</em>'.
	 * @see org.montex.researchcv.InCollection
	 * @generated
	 */
	EClass getInCollection();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.InCollection#getBooktitle <em>Booktitle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Booktitle</em>'.
	 * @see org.montex.researchcv.InCollection#getBooktitle()
	 * @see #getInCollection()
	 * @generated
	 */
	EAttribute getInCollection_Booktitle();

	/**
	 * Returns the meta object for the reference list '{@link org.montex.researchcv.InCollection#getBookeditors <em>Bookeditors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Bookeditors</em>'.
	 * @see org.montex.researchcv.InCollection#getBookeditors()
	 * @see #getInCollection()
	 * @generated
	 */
	EReference getInCollection_Bookeditors();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.InCollection#getPublisher <em>Publisher</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Publisher</em>'.
	 * @see org.montex.researchcv.InCollection#getPublisher()
	 * @see #getInCollection()
	 * @generated
	 */
	EAttribute getInCollection_Publisher();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.InCollection#getSeries <em>Series</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Series</em>'.
	 * @see org.montex.researchcv.InCollection#getSeries()
	 * @see #getInCollection()
	 * @generated
	 */
	EAttribute getInCollection_Series();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.InCollection#getVolume <em>Volume</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Volume</em>'.
	 * @see org.montex.researchcv.InCollection#getVolume()
	 * @see #getInCollection()
	 * @generated
	 */
	EAttribute getInCollection_Volume();

	/**
	 * Returns the meta object for the containment reference list '{@link org.montex.researchcv.InCollection#getISBN <em>ISBN</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>ISBN</em>'.
	 * @see org.montex.researchcv.InCollection#getISBN()
	 * @see #getInCollection()
	 * @generated
	 */
	EReference getInCollection_ISBN();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.InCollection#getFirstPage <em>First Page</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>First Page</em>'.
	 * @see org.montex.researchcv.InCollection#getFirstPage()
	 * @see #getInCollection()
	 * @generated
	 */
	EAttribute getInCollection_FirstPage();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.InCollection#getLastPage <em>Last Page</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Page</em>'.
	 * @see org.montex.researchcv.InCollection#getLastPage()
	 * @see #getInCollection()
	 * @generated
	 */
	EAttribute getInCollection_LastPage();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.Book <em>Book</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Book</em>'.
	 * @see org.montex.researchcv.Book
	 * @generated
	 */
	EClass getBook();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Book#getPublisher <em>Publisher</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Publisher</em>'.
	 * @see org.montex.researchcv.Book#getPublisher()
	 * @see #getBook()
	 * @generated
	 */
	EAttribute getBook_Publisher();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Book#getSeries <em>Series</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Series</em>'.
	 * @see org.montex.researchcv.Book#getSeries()
	 * @see #getBook()
	 * @generated
	 */
	EAttribute getBook_Series();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Book#getVolume <em>Volume</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Volume</em>'.
	 * @see org.montex.researchcv.Book#getVolume()
	 * @see #getBook()
	 * @generated
	 */
	EAttribute getBook_Volume();

	/**
	 * Returns the meta object for the containment reference list '{@link org.montex.researchcv.Book#getISBN <em>ISBN</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>ISBN</em>'.
	 * @see org.montex.researchcv.Book#getISBN()
	 * @see #getBook()
	 * @generated
	 */
	EReference getBook_ISBN();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.BookChapter <em>Book Chapter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Book Chapter</em>'.
	 * @see org.montex.researchcv.BookChapter
	 * @generated
	 */
	EClass getBookChapter();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.BookChapter#getChapterNumber <em>Chapter Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Chapter Number</em>'.
	 * @see org.montex.researchcv.BookChapter#getChapterNumber()
	 * @see #getBookChapter()
	 * @generated
	 */
	EAttribute getBookChapter_ChapterNumber();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.Report <em>Report</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Report</em>'.
	 * @see org.montex.researchcv.Report
	 * @generated
	 */
	EClass getReport();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Report#getReportNumber <em>Report Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Report Number</em>'.
	 * @see org.montex.researchcv.Report#getReportNumber()
	 * @see #getReport()
	 * @generated
	 */
	EAttribute getReport_ReportNumber();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Report#getInstitution <em>Institution</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Institution</em>'.
	 * @see org.montex.researchcv.Report#getInstitution()
	 * @see #getReport()
	 * @generated
	 */
	EAttribute getReport_Institution();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.Miscellaneous <em>Miscellaneous</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Miscellaneous</em>'.
	 * @see org.montex.researchcv.Miscellaneous
	 * @generated
	 */
	EClass getMiscellaneous();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.PublishedDate <em>Published Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Published Date</em>'.
	 * @see org.montex.researchcv.PublishedDate
	 * @generated
	 */
	EClass getPublishedDate();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.PublishedDate#getDay <em>Day</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Day</em>'.
	 * @see org.montex.researchcv.PublishedDate#getDay()
	 * @see #getPublishedDate()
	 * @generated
	 */
	EAttribute getPublishedDate_Day();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.PublishedDate#getMonth <em>Month</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Month</em>'.
	 * @see org.montex.researchcv.PublishedDate#getMonth()
	 * @see #getPublishedDate()
	 * @generated
	 */
	EAttribute getPublishedDate_Month();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.PublishedDate#getYear <em>Year</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Year</em>'.
	 * @see org.montex.researchcv.PublishedDate#getYear()
	 * @see #getPublishedDate()
	 * @generated
	 */
	EAttribute getPublishedDate_Year();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.PublishedDate#getMonthName <em>Month Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Month Name</em>'.
	 * @see org.montex.researchcv.PublishedDate#getMonthName()
	 * @see #getPublishedDate()
	 * @generated
	 */
	EAttribute getPublishedDate_MonthName();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.PublishedDate#getMonthShortName <em>Month Short Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Month Short Name</em>'.
	 * @see org.montex.researchcv.PublishedDate#getMonthShortName()
	 * @see #getPublishedDate()
	 * @generated
	 */
	EAttribute getPublishedDate_MonthShortName();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.SerialNumber <em>Serial Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Serial Number</em>'.
	 * @see org.montex.researchcv.SerialNumber
	 * @generated
	 */
	EClass getSerialNumber();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.SerialNumber#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see org.montex.researchcv.SerialNumber#getValue()
	 * @see #getSerialNumber()
	 * @generated
	 */
	EAttribute getSerialNumber_Value();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.SerialNumber#getKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind</em>'.
	 * @see org.montex.researchcv.SerialNumber#getKind()
	 * @see #getSerialNumber()
	 * @generated
	 */
	EAttribute getSerialNumber_Kind();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.ResearchGroup <em>Research Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Research Group</em>'.
	 * @see org.montex.researchcv.ResearchGroup
	 * @generated
	 */
	EClass getResearchGroup();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.ResearchGroup#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.montex.researchcv.ResearchGroup#getName()
	 * @see #getResearchGroup()
	 * @generated
	 */
	EAttribute getResearchGroup_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.ResearchGroup#getAcronym <em>Acronym</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Acronym</em>'.
	 * @see org.montex.researchcv.ResearchGroup#getAcronym()
	 * @see #getResearchGroup()
	 * @generated
	 */
	EAttribute getResearchGroup_Acronym();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.ResearchGroup#getURL <em>URL</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>URL</em>'.
	 * @see org.montex.researchcv.ResearchGroup#getURL()
	 * @see #getResearchGroup()
	 * @generated
	 */
	EAttribute getResearchGroup_URL();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.Course <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course</em>'.
	 * @see org.montex.researchcv.Course
	 * @generated
	 */
	EClass getCourse();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Course#getCode <em>Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Code</em>'.
	 * @see org.montex.researchcv.Course#getCode()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Code();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Course#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.montex.researchcv.Course#getName()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Course#getLevel <em>Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Level</em>'.
	 * @see org.montex.researchcv.Course#getLevel()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Level();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Course#getHours <em>Hours</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hours</em>'.
	 * @see org.montex.researchcv.Course#getHours()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Hours();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Course#getCredits <em>Credits</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Credits</em>'.
	 * @see org.montex.researchcv.Course#getCredits()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Credits();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.TaughtCourse <em>Taught Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Taught Course</em>'.
	 * @see org.montex.researchcv.TaughtCourse
	 * @generated
	 */
	EClass getTaughtCourse();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.TaughtCourse#getYear <em>Year</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Year</em>'.
	 * @see org.montex.researchcv.TaughtCourse#getYear()
	 * @see #getTaughtCourse()
	 * @generated
	 */
	EAttribute getTaughtCourse_Year();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.TaughtCourse#getPeriod <em>Period</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Period</em>'.
	 * @see org.montex.researchcv.TaughtCourse#getPeriod()
	 * @see #getTaughtCourse()
	 * @generated
	 */
	EAttribute getTaughtCourse_Period();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.TaughtCourse#getWebinfo <em>Webinfo</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Webinfo</em>'.
	 * @see org.montex.researchcv.TaughtCourse#getWebinfo()
	 * @see #getTaughtCourse()
	 * @generated
	 */
	EAttribute getTaughtCourse_Webinfo();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.TaughtCourse#getWebsite <em>Website</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Website</em>'.
	 * @see org.montex.researchcv.TaughtCourse#getWebsite()
	 * @see #getTaughtCourse()
	 * @generated
	 */
	EAttribute getTaughtCourse_Website();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.TaughtCourse#getLanguage <em>Language</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Language</em>'.
	 * @see org.montex.researchcv.TaughtCourse#getLanguage()
	 * @see #getTaughtCourse()
	 * @generated
	 */
	EAttribute getTaughtCourse_Language();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.TaughtCourse#isCanceled <em>Canceled</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Canceled</em>'.
	 * @see org.montex.researchcv.TaughtCourse#isCanceled()
	 * @see #getTaughtCourse()
	 * @generated
	 */
	EAttribute getTaughtCourse_Canceled();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.TaughtCourse#getResponsibility <em>Responsibility</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Responsibility</em>'.
	 * @see org.montex.researchcv.TaughtCourse#getResponsibility()
	 * @see #getTaughtCourse()
	 * @generated
	 */
	EAttribute getTaughtCourse_Responsibility();

	/**
	 * Returns the meta object for the containment reference list '{@link org.montex.researchcv.TaughtCourse#getOffering <em>Offering</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Offering</em>'.
	 * @see org.montex.researchcv.TaughtCourse#getOffering()
	 * @see #getTaughtCourse()
	 * @generated
	 */
	EReference getTaughtCourse_Offering();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.TaughtCourse#getCode <em>Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Code</em>'.
	 * @see org.montex.researchcv.TaughtCourse#getCode()
	 * @see #getTaughtCourse()
	 * @generated
	 */
	EAttribute getTaughtCourse_Code();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.TaughtCourse#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.montex.researchcv.TaughtCourse#getName()
	 * @see #getTaughtCourse()
	 * @generated
	 */
	EAttribute getTaughtCourse_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.TaughtCourse#getLevel <em>Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Level</em>'.
	 * @see org.montex.researchcv.TaughtCourse#getLevel()
	 * @see #getTaughtCourse()
	 * @generated
	 */
	EAttribute getTaughtCourse_Level();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.ResearchTopic <em>Research Topic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Research Topic</em>'.
	 * @see org.montex.researchcv.ResearchTopic
	 * @generated
	 */
	EClass getResearchTopic();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.ResearchTopic#getTitle <em>Title</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Title</em>'.
	 * @see org.montex.researchcv.ResearchTopic#getTitle()
	 * @see #getResearchTopic()
	 * @generated
	 */
	EAttribute getResearchTopic_Title();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.ResearchTopic#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see org.montex.researchcv.ResearchTopic#getDescription()
	 * @see #getResearchTopic()
	 * @generated
	 */
	EAttribute getResearchTopic_Description();

	/**
	 * Returns the meta object for the reference list '{@link org.montex.researchcv.ResearchTopic#getRelatedPapers <em>Related Papers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Related Papers</em>'.
	 * @see org.montex.researchcv.ResearchTopic#getRelatedPapers()
	 * @see #getResearchTopic()
	 * @generated
	 */
	EReference getResearchTopic_RelatedPapers();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.Project <em>Project</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Project</em>'.
	 * @see org.montex.researchcv.Project
	 * @generated
	 */
	EClass getProject();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Project#getAcronym <em>Acronym</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Acronym</em>'.
	 * @see org.montex.researchcv.Project#getAcronym()
	 * @see #getProject()
	 * @generated
	 */
	EAttribute getProject_Acronym();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Project#getTitle <em>Title</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Title</em>'.
	 * @see org.montex.researchcv.Project#getTitle()
	 * @see #getProject()
	 * @generated
	 */
	EAttribute getProject_Title();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Project#getShortName <em>Short Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Short Name</em>'.
	 * @see org.montex.researchcv.Project#getShortName()
	 * @see #getProject()
	 * @generated
	 */
	EAttribute getProject_ShortName();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.Grant <em>Grant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Grant</em>'.
	 * @see org.montex.researchcv.Grant
	 * @generated
	 */
	EClass getGrant();

	/**
	 * Returns the meta object for the reference list '{@link org.montex.researchcv.Grant#getRelatedPublications <em>Related Publications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Related Publications</em>'.
	 * @see org.montex.researchcv.Grant#getRelatedPublications()
	 * @see #getGrant()
	 * @generated
	 */
	EReference getGrant_RelatedPublications();

	/**
	 * Returns the meta object for the attribute list '{@link org.montex.researchcv.Grant#getURL <em>URL</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>URL</em>'.
	 * @see org.montex.researchcv.Grant#getURL()
	 * @see #getGrant()
	 * @generated
	 */
	EAttribute getGrant_URL();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Grant#getAgency <em>Agency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Agency</em>'.
	 * @see org.montex.researchcv.Grant#getAgency()
	 * @see #getGrant()
	 * @generated
	 */
	EAttribute getGrant_Agency();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Grant#getNumber <em>Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number</em>'.
	 * @see org.montex.researchcv.Grant#getNumber()
	 * @see #getGrant()
	 * @generated
	 */
	EAttribute getGrant_Number();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Grant#getProgram <em>Program</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Program</em>'.
	 * @see org.montex.researchcv.Grant#getProgram()
	 * @see #getGrant()
	 * @generated
	 */
	EAttribute getGrant_Program();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Grant#getCall <em>Call</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Call</em>'.
	 * @see org.montex.researchcv.Grant#getCall()
	 * @see #getGrant()
	 * @generated
	 */
	EAttribute getGrant_Call();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Grant#getGrantID <em>Grant ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Grant ID</em>'.
	 * @see org.montex.researchcv.Grant#getGrantID()
	 * @see #getGrant()
	 * @generated
	 */
	EAttribute getGrant_GrantID();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Grant#getRole <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Role</em>'.
	 * @see org.montex.researchcv.Grant#getRole()
	 * @see #getGrant()
	 * @generated
	 */
	EAttribute getGrant_Role();

	/**
	 * Returns the meta object for the containment reference list '{@link org.montex.researchcv.Grant#getBudget <em>Budget</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Budget</em>'.
	 * @see org.montex.researchcv.Grant#getBudget()
	 * @see #getGrant()
	 * @generated
	 */
	EReference getGrant_Budget();

	/**
	 * Returns the meta object for the containment reference '{@link org.montex.researchcv.Grant#getStartDate <em>Start Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Start Date</em>'.
	 * @see org.montex.researchcv.Grant#getStartDate()
	 * @see #getGrant()
	 * @generated
	 */
	EReference getGrant_StartDate();

	/**
	 * Returns the meta object for the containment reference '{@link org.montex.researchcv.Grant#getEndDate <em>End Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>End Date</em>'.
	 * @see org.montex.researchcv.Grant#getEndDate()
	 * @see #getGrant()
	 * @generated
	 */
	EReference getGrant_EndDate();

	/**
	 * Returns the meta object for the '{@link org.montex.researchcv.Grant#isActive() <em>Is Active</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Active</em>' operation.
	 * @see org.montex.researchcv.Grant#isActive()
	 * @generated
	 */
	EOperation getGrant__IsActive();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.Budget <em>Budget</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Budget</em>'.
	 * @see org.montex.researchcv.Budget
	 * @generated
	 */
	EClass getBudget();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Budget#getAmount <em>Amount</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Amount</em>'.
	 * @see org.montex.researchcv.Budget#getAmount()
	 * @see #getBudget()
	 * @generated
	 */
	EAttribute getBudget_Amount();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Budget#getCurrency <em>Currency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Currency</em>'.
	 * @see org.montex.researchcv.Budget#getCurrency()
	 * @see #getBudget()
	 * @generated
	 */
	EAttribute getBudget_Currency();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.RootElement <em>Root Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Root Element</em>'.
	 * @see org.montex.researchcv.RootElement
	 * @generated
	 */
	EClass getRootElement();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.PersonRegistry <em>Person Registry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Person Registry</em>'.
	 * @see org.montex.researchcv.PersonRegistry
	 * @generated
	 */
	EClass getPersonRegistry();

	/**
	 * Returns the meta object for the containment reference list '{@link org.montex.researchcv.PersonRegistry#getPerson <em>Person</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Person</em>'.
	 * @see org.montex.researchcv.PersonRegistry#getPerson()
	 * @see #getPersonRegistry()
	 * @generated
	 */
	EReference getPersonRegistry_Person();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.CourseOffering <em>Course Offering</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course Offering</em>'.
	 * @see org.montex.researchcv.CourseOffering
	 * @generated
	 */
	EClass getCourseOffering();

	/**
	 * Returns the meta object for the attribute list '{@link org.montex.researchcv.CourseOffering#getGroups <em>Groups</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Groups</em>'.
	 * @see org.montex.researchcv.CourseOffering#getGroups()
	 * @see #getCourseOffering()
	 * @generated
	 */
	EAttribute getCourseOffering_Groups();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.CourseOffering#getSpecificName <em>Specific Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Specific Name</em>'.
	 * @see org.montex.researchcv.CourseOffering#getSpecificName()
	 * @see #getCourseOffering()
	 * @generated
	 */
	EAttribute getCourseOffering_SpecificName();

	/**
	 * Returns the meta object for the reference '{@link org.montex.researchcv.CourseOffering#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Course</em>'.
	 * @see org.montex.researchcv.CourseOffering#getCourse()
	 * @see #getCourseOffering()
	 * @generated
	 */
	EReference getCourseOffering_Course();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.Supervision <em>Supervision</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Supervision</em>'.
	 * @see org.montex.researchcv.Supervision
	 * @generated
	 */
	EClass getSupervision();

	/**
	 * Returns the meta object for the containment reference '{@link org.montex.researchcv.Supervision#getDasteStart <em>Daste Start</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Daste Start</em>'.
	 * @see org.montex.researchcv.Supervision#getDasteStart()
	 * @see #getSupervision()
	 * @generated
	 */
	EReference getSupervision_DasteStart();

	/**
	 * Returns the meta object for the containment reference '{@link org.montex.researchcv.Supervision#getDateEnd <em>Date End</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Date End</em>'.
	 * @see org.montex.researchcv.Supervision#getDateEnd()
	 * @see #getSupervision()
	 * @generated
	 */
	EReference getSupervision_DateEnd();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Supervision#getLevel <em>Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Level</em>'.
	 * @see org.montex.researchcv.Supervision#getLevel()
	 * @see #getSupervision()
	 * @generated
	 */
	EAttribute getSupervision_Level();

	/**
	 * Returns the meta object for the reference '{@link org.montex.researchcv.Supervision#getStudent <em>Student</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Student</em>'.
	 * @see org.montex.researchcv.Supervision#getStudent()
	 * @see #getSupervision()
	 * @generated
	 */
	EReference getSupervision_Student();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Supervision#getProjectTitle <em>Project Title</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Project Title</em>'.
	 * @see org.montex.researchcv.Supervision#getProjectTitle()
	 * @see #getSupervision()
	 * @generated
	 */
	EAttribute getSupervision_ProjectTitle();

	/**
	 * Returns the meta object for the reference list '{@link org.montex.researchcv.Supervision#getSupervisors <em>Supervisors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Supervisors</em>'.
	 * @see org.montex.researchcv.Supervision#getSupervisors()
	 * @see #getSupervision()
	 * @generated
	 */
	EReference getSupervision_Supervisors();

	/**
	 * Returns the meta object for the reference list '{@link org.montex.researchcv.Supervision#getInstitutions <em>Institutions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Institutions</em>'.
	 * @see org.montex.researchcv.Supervision#getInstitutions()
	 * @see #getSupervision()
	 * @generated
	 */
	EReference getSupervision_Institutions();

	/**
	 * Returns the meta object for the reference '{@link org.montex.researchcv.Supervision#getMainInstitution <em>Main Institution</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Main Institution</em>'.
	 * @see org.montex.researchcv.Supervision#getMainInstitution()
	 * @see #getSupervision()
	 * @generated
	 */
	EReference getSupervision_MainInstitution();

	/**
	 * Returns the meta object for the reference '{@link org.montex.researchcv.Supervision#getMainSupervisor <em>Main Supervisor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Main Supervisor</em>'.
	 * @see org.montex.researchcv.Supervision#getMainSupervisor()
	 * @see #getSupervision()
	 * @generated
	 */
	EReference getSupervision_MainSupervisor();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Supervision#isCompleted <em>Completed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Completed</em>'.
	 * @see org.montex.researchcv.Supervision#isCompleted()
	 * @see #getSupervision()
	 * @generated
	 */
	EAttribute getSupervision_Completed();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Supervision#getFinalWorkURL <em>Final Work URL</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Final Work URL</em>'.
	 * @see org.montex.researchcv.Supervision#getFinalWorkURL()
	 * @see #getSupervision()
	 * @generated
	 */
	EAttribute getSupervision_FinalWorkURL();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Supervision#getFinalWorkDOI <em>Final Work DOI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Final Work DOI</em>'.
	 * @see org.montex.researchcv.Supervision#getFinalWorkDOI()
	 * @see #getSupervision()
	 * @generated
	 */
	EAttribute getSupervision_FinalWorkDOI();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.InstitutionRegistry <em>Institution Registry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Institution Registry</em>'.
	 * @see org.montex.researchcv.InstitutionRegistry
	 * @generated
	 */
	EClass getInstitutionRegistry();

	/**
	 * Returns the meta object for the containment reference list '{@link org.montex.researchcv.InstitutionRegistry#getInstitutions <em>Institutions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Institutions</em>'.
	 * @see org.montex.researchcv.InstitutionRegistry#getInstitutions()
	 * @see #getInstitutionRegistry()
	 * @generated
	 */
	EReference getInstitutionRegistry_Institutions();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.Institution <em>Institution</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Institution</em>'.
	 * @see org.montex.researchcv.Institution
	 * @generated
	 */
	EClass getInstitution();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Institution#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.montex.researchcv.Institution#getName()
	 * @see #getInstitution()
	 * @generated
	 */
	EAttribute getInstitution_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link org.montex.researchcv.Institution#getUnits <em>Units</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Units</em>'.
	 * @see org.montex.researchcv.Institution#getUnits()
	 * @see #getInstitution()
	 * @generated
	 */
	EReference getInstitution_Units();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.Institution#getCountry <em>Country</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Country</em>'.
	 * @see org.montex.researchcv.Institution#getCountry()
	 * @see #getInstitution()
	 * @generated
	 */
	EAttribute getInstitution_Country();

	/**
	 * Returns the meta object for the containment reference '{@link org.montex.researchcv.Institution#getFullName <em>Full Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Full Name</em>'.
	 * @see org.montex.researchcv.Institution#getFullName()
	 * @see #getInstitution()
	 * @generated
	 */
	EReference getInstitution_FullName();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.MultiLanguageString <em>Multi Language String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Multi Language String</em>'.
	 * @see org.montex.researchcv.MultiLanguageString
	 * @generated
	 */
	EClass getMultiLanguageString();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.MultiLanguageString#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Text</em>'.
	 * @see org.montex.researchcv.MultiLanguageString#getText()
	 * @see #getMultiLanguageString()
	 * @generated
	 */
	EAttribute getMultiLanguageString_Text();

	/**
	 * Returns the meta object for the containment reference list '{@link org.montex.researchcv.MultiLanguageString#getLocalizations <em>Localizations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Localizations</em>'.
	 * @see org.montex.researchcv.MultiLanguageString#getLocalizations()
	 * @see #getMultiLanguageString()
	 * @generated
	 */
	EReference getMultiLanguageString_Localizations();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.MultiLanguageString#getBaseLanguage <em>Base Language</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Base Language</em>'.
	 * @see org.montex.researchcv.MultiLanguageString#getBaseLanguage()
	 * @see #getMultiLanguageString()
	 * @generated
	 */
	EAttribute getMultiLanguageString_BaseLanguage();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.LocalizedString <em>Localized String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Localized String</em>'.
	 * @see org.montex.researchcv.LocalizedString
	 * @generated
	 */
	EClass getLocalizedString();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.LocalizedString#getLanguage <em>Language</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Language</em>'.
	 * @see org.montex.researchcv.LocalizedString#getLanguage()
	 * @see #getLocalizedString()
	 * @generated
	 */
	EAttribute getLocalizedString_Language();

	/**
	 * Returns the meta object for class '{@link org.montex.researchcv.SupervisionsRegistry <em>Supervisions Registry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Supervisions Registry</em>'.
	 * @see org.montex.researchcv.SupervisionsRegistry
	 * @generated
	 */
	EClass getSupervisionsRegistry();

	/**
	 * Returns the meta object for the containment reference list '{@link org.montex.researchcv.SupervisionsRegistry#getSupervisions <em>Supervisions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Supervisions</em>'.
	 * @see org.montex.researchcv.SupervisionsRegistry#getSupervisions()
	 * @see #getSupervisionsRegistry()
	 * @generated
	 */
	EReference getSupervisionsRegistry_Supervisions();

	/**
	 * Returns the meta object for the attribute '{@link org.montex.researchcv.LocalizedString#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Text</em>'.
	 * @see org.montex.researchcv.LocalizedString#getText()
	 * @see #getLocalizedString()
	 * @generated
	 */
	EAttribute getLocalizedString_Text();

	/**
	 * Returns the meta object for enum '{@link org.montex.researchcv.ContactKind <em>Contact Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Contact Kind</em>'.
	 * @see org.montex.researchcv.ContactKind
	 * @generated
	 */
	EEnum getContactKind();

	/**
	 * Returns the meta object for enum '{@link org.montex.researchcv.SerialNumberKind <em>Serial Number Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Serial Number Kind</em>'.
	 * @see org.montex.researchcv.SerialNumberKind
	 * @generated
	 */
	EEnum getSerialNumberKind();

	/**
	 * Returns the meta object for enum '{@link org.montex.researchcv.CourseKind <em>Course Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Course Kind</em>'.
	 * @see org.montex.researchcv.CourseKind
	 * @generated
	 */
	EEnum getCourseKind();

	/**
	 * Returns the meta object for enum '{@link org.montex.researchcv.SupervisionKind <em>Supervision Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Supervision Kind</em>'.
	 * @see org.montex.researchcv.SupervisionKind
	 * @generated
	 */
	EEnum getSupervisionKind();

	/**
	 * Returns the meta object for enum '{@link org.montex.researchcv.ParticipationKind <em>Participation Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Participation Kind</em>'.
	 * @see org.montex.researchcv.ParticipationKind
	 * @generated
	 */
	EEnum getParticipationKind();

	/**
	 * Returns the meta object for data type '{@link java.util.Currency <em>Currency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Currency</em>'.
	 * @see java.util.Currency
	 * @model instanceClass="java.util.Currency"
	 * @generated
	 */
	EDataType getCurrency();

	/**
	 * Returns the meta object for data type '{@link java.util.Locale <em>Language Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Language Code</em>'.
	 * @see java.util.Locale
	 * @model instanceClass="java.util.Locale"
	 * @generated
	 */
	EDataType getLanguageCode();

	/**
	 * Returns the meta object for data type '{@link java.util.Locale <em>Country Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Country Code</em>'.
	 * @see java.util.Locale
	 * @model instanceClass="java.util.Locale"
	 * @generated
	 */
	EDataType getCountryCode();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ResearchCVFactory getResearchCVFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.PersonImpl <em>Person</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.PersonImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getPerson()
		 * @generated
		 */
		EClass PERSON = eINSTANCE.getPerson();

		/**
		 * The meta object literal for the '<em><b>First Names</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSON__FIRST_NAMES = eINSTANCE.getPerson_FirstNames();

		/**
		 * The meta object literal for the '<em><b>Last Names</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSON__LAST_NAMES = eINSTANCE.getPerson_LastNames();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSON__NAME = eINSTANCE.getPerson_Name();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.ResearcherImpl <em>Researcher</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.ResearcherImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getResearcher()
		 * @generated
		 */
		EClass RESEARCHER = eINSTANCE.getResearcher();

		/**
		 * The meta object literal for the '<em><b>Coauthors</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESEARCHER__COAUTHORS = eINSTANCE.getResearcher_Coauthors();

		/**
		 * The meta object literal for the '<em><b>Contacts</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESEARCHER__CONTACTS = eINSTANCE.getResearcher_Contacts();

		/**
		 * The meta object literal for the '<em><b>Socials</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESEARCHER__SOCIALS = eINSTANCE.getResearcher_Socials();

		/**
		 * The meta object literal for the '<em><b>Publications</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESEARCHER__PUBLICATIONS = eINSTANCE.getResearcher_Publications();

		/**
		 * The meta object literal for the '<em><b>Teaching Material</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESEARCHER__TEACHING_MATERIAL = eINSTANCE.getResearcher_TeachingMaterial();

		/**
		 * The meta object literal for the '<em><b>Other Material</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESEARCHER__OTHER_MATERIAL = eINSTANCE.getResearcher_OtherMaterial();

		/**
		 * The meta object literal for the '<em><b>Research Group</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESEARCHER__RESEARCH_GROUP = eINSTANCE.getResearcher_ResearchGroup();

		/**
		 * The meta object literal for the '<em><b>Topics</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESEARCHER__TOPICS = eINSTANCE.getResearcher_Topics();

		/**
		 * The meta object literal for the '<em><b>Grants</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESEARCHER__GRANTS = eINSTANCE.getResearcher_Grants();

		/**
		 * The meta object literal for the '<em><b>Courses</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESEARCHER__COURSES = eINSTANCE.getResearcher_Courses();

		/**
		 * The meta object literal for the '<em><b>Taught Courses</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESEARCHER__TAUGHT_COURSES = eINSTANCE.getResearcher_TaughtCourses();

		/**
		 * The meta object literal for the '<em><b>Supervisions Registry</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESEARCHER__SUPERVISIONS_REGISTRY = eINSTANCE.getResearcher_SupervisionsRegistry();

		/**
		 * The meta object literal for the '<em><b>Supervisions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESEARCHER__SUPERVISIONS = eINSTANCE.getResearcher_Supervisions();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.AddressImpl <em>Address</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.AddressImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getAddress()
		 * @generated
		 */
		EClass ADDRESS = eINSTANCE.getAddress();

		/**
		 * The meta object literal for the '<em><b>Lines</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ADDRESS__LINES = eINSTANCE.getAddress_Lines();

		/**
		 * The meta object literal for the '<em><b>Zip</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ADDRESS__ZIP = eINSTANCE.getAddress_Zip();

		/**
		 * The meta object literal for the '<em><b>City</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ADDRESS__CITY = eINSTANCE.getAddress_City();

		/**
		 * The meta object literal for the '<em><b>Region</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ADDRESS__REGION = eINSTANCE.getAddress_Region();

		/**
		 * The meta object literal for the '<em><b>Country</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ADDRESS__COUNTRY = eINSTANCE.getAddress_Country();

		/**
		 * The meta object literal for the '<em><b>Building</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ADDRESS__BUILDING = eINSTANCE.getAddress_Building();

		/**
		 * The meta object literal for the '<em><b>Room</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ADDRESS__ROOM = eINSTANCE.getAddress_Room();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.ContactImpl <em>Contact</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.ContactImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getContact()
		 * @generated
		 */
		EClass CONTACT = eINSTANCE.getContact();

		/**
		 * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONTACT__KIND = eINSTANCE.getContact_Kind();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.EmailImpl <em>Email</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.EmailImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getEmail()
		 * @generated
		 */
		EClass EMAIL = eINSTANCE.getEmail();

		/**
		 * The meta object literal for the '<em><b>Address</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EMAIL__ADDRESS = eINSTANCE.getEmail_Address();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.PhoneImpl <em>Phone</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.PhoneImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getPhone()
		 * @generated
		 */
		EClass PHONE = eINSTANCE.getPhone();

		/**
		 * The meta object literal for the '<em><b>Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHONE__NUMBER = eINSTANCE.getPhone_Number();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.SocialProfileImpl <em>Social Profile</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.SocialProfileImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getSocialProfile()
		 * @generated
		 */
		EClass SOCIAL_PROFILE = eINSTANCE.getSocialProfile();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOCIAL_PROFILE__NAME = eINSTANCE.getSocialProfile_Name();

		/**
		 * The meta object literal for the '<em><b>Url</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOCIAL_PROFILE__URL = eINSTANCE.getSocialProfile_Url();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.WebsiteImpl <em>Website</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.WebsiteImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getWebsite()
		 * @generated
		 */
		EClass WEBSITE = eINSTANCE.getWebsite();

		/**
		 * The meta object literal for the '<em><b>Url</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WEBSITE__URL = eINSTANCE.getWebsite_Url();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.PublicationImpl <em>Publication</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.PublicationImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getPublication()
		 * @generated
		 */
		EClass PUBLICATION = eINSTANCE.getPublication();

		/**
		 * The meta object literal for the '<em><b>Citekey</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PUBLICATION__CITEKEY = eINSTANCE.getPublication_Citekey();

		/**
		 * The meta object literal for the '<em><b>Title</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PUBLICATION__TITLE = eINSTANCE.getPublication_Title();

		/**
		 * The meta object literal for the '<em><b>Authors</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PUBLICATION__AUTHORS = eINSTANCE.getPublication_Authors();

		/**
		 * The meta object literal for the '<em><b>URL</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PUBLICATION__URL = eINSTANCE.getPublication_URL();

		/**
		 * The meta object literal for the '<em><b>Published</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PUBLICATION__PUBLISHED = eINSTANCE.getPublication_Published();

		/**
		 * The meta object literal for the '<em><b>Open Access</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PUBLICATION__OPEN_ACCESS = eINSTANCE.getPublication_OpenAccess();

		/**
		 * The meta object literal for the '<em><b>Date</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PUBLICATION__DATE = eINSTANCE.getPublication_Date();

		/**
		 * The meta object literal for the '<em><b>DOI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PUBLICATION__DOI = eINSTANCE.getPublication_DOI();

		/**
		 * The meta object literal for the '<em><b>Abstract</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PUBLICATION__ABSTRACT = eINSTANCE.getPublication_Abstract();

		/**
		 * The meta object literal for the '<em><b>With Author Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PUBLICATION__WITH_AUTHOR_VERSION = eINSTANCE.getPublication_WithAuthorVersion();

		/**
		 * The meta object literal for the '<em><b>Notes</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PUBLICATION__NOTES = eINSTANCE.getPublication_Notes();

		/**
		 * The meta object literal for the '<em><b>Related Projects</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PUBLICATION__RELATED_PROJECTS = eINSTANCE.getPublication_RelatedProjects();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.JournalImpl <em>Journal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.JournalImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getJournal()
		 * @generated
		 */
		EClass JOURNAL = eINSTANCE.getJournal();

		/**
		 * The meta object literal for the '<em><b>Journal Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JOURNAL__JOURNAL_NAME = eINSTANCE.getJournal_JournalName();

		/**
		 * The meta object literal for the '<em><b>Publisher</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JOURNAL__PUBLISHER = eINSTANCE.getJournal_Publisher();

		/**
		 * The meta object literal for the '<em><b>First Page</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JOURNAL__FIRST_PAGE = eINSTANCE.getJournal_FirstPage();

		/**
		 * The meta object literal for the '<em><b>Last Page</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JOURNAL__LAST_PAGE = eINSTANCE.getJournal_LastPage();

		/**
		 * The meta object literal for the '<em><b>Volume</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JOURNAL__VOLUME = eINSTANCE.getJournal_Volume();

		/**
		 * The meta object literal for the '<em><b>Issue</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JOURNAL__ISSUE = eINSTANCE.getJournal_Issue();

		/**
		 * The meta object literal for the '<em><b>ISSN</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JOURNAL__ISSN = eINSTANCE.getJournal_ISSN();

		/**
		 * The meta object literal for the '<em><b>Magazine</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JOURNAL__MAGAZINE = eINSTANCE.getJournal_Magazine();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.ConferenceImpl <em>Conference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.ConferenceImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getConference()
		 * @generated
		 */
		EClass CONFERENCE = eINSTANCE.getConference();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.WorkshopImpl <em>Workshop</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.WorkshopImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getWorkshop()
		 * @generated
		 */
		EClass WORKSHOP = eINSTANCE.getWorkshop();

		/**
		 * The meta object literal for the '<em><b>Main Event Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WORKSHOP__MAIN_EVENT_NAME = eINSTANCE.getWorkshop_MainEventName();

		/**
		 * The meta object literal for the '<em><b>Main Event Short Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WORKSHOP__MAIN_EVENT_SHORT_NAME = eINSTANCE.getWorkshop_MainEventShortName();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.InProceedingsImpl <em>In Proceedings</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.InProceedingsImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getInProceedings()
		 * @generated
		 */
		EClass IN_PROCEEDINGS = eINSTANCE.getInProceedings();

		/**
		 * The meta object literal for the '<em><b>Event End Date</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IN_PROCEEDINGS__EVENT_END_DATE = eINSTANCE.getInProceedings_EventEndDate();

		/**
		 * The meta object literal for the '<em><b>Event Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IN_PROCEEDINGS__EVENT_NAME = eINSTANCE.getInProceedings_EventName();

		/**
		 * The meta object literal for the '<em><b>Venue</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IN_PROCEEDINGS__VENUE = eINSTANCE.getInProceedings_Venue();

		/**
		 * The meta object literal for the '<em><b>Event Short Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IN_PROCEEDINGS__EVENT_SHORT_NAME = eINSTANCE.getInProceedings_EventShortName();

		/**
		 * The meta object literal for the '<em><b>Event Start Date</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IN_PROCEEDINGS__EVENT_START_DATE = eINSTANCE.getInProceedings_EventStartDate();

		/**
		 * The meta object literal for the '<em><b>Track Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IN_PROCEEDINGS__TRACK_NAME = eINSTANCE.getInProceedings_TrackName();

		/**
		 * The meta object literal for the '<em><b>Track Short Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IN_PROCEEDINGS__TRACK_SHORT_NAME = eINSTANCE.getInProceedings_TrackShortName();

		/**
		 * The meta object literal for the '<em><b>Short Paper</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IN_PROCEEDINGS__SHORT_PAPER = eINSTANCE.getInProceedings_ShortPaper();

		/**
		 * The meta object literal for the '<em><b>Tool Paper</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IN_PROCEEDINGS__TOOL_PAPER = eINSTANCE.getInProceedings_ToolPaper();

		/**
		 * The meta object literal for the '<em><b>Invited Paper</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IN_PROCEEDINGS__INVITED_PAPER = eINSTANCE.getInProceedings_InvitedPaper();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.InCollectionImpl <em>In Collection</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.InCollectionImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getInCollection()
		 * @generated
		 */
		EClass IN_COLLECTION = eINSTANCE.getInCollection();

		/**
		 * The meta object literal for the '<em><b>Booktitle</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IN_COLLECTION__BOOKTITLE = eINSTANCE.getInCollection_Booktitle();

		/**
		 * The meta object literal for the '<em><b>Bookeditors</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IN_COLLECTION__BOOKEDITORS = eINSTANCE.getInCollection_Bookeditors();

		/**
		 * The meta object literal for the '<em><b>Publisher</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IN_COLLECTION__PUBLISHER = eINSTANCE.getInCollection_Publisher();

		/**
		 * The meta object literal for the '<em><b>Series</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IN_COLLECTION__SERIES = eINSTANCE.getInCollection_Series();

		/**
		 * The meta object literal for the '<em><b>Volume</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IN_COLLECTION__VOLUME = eINSTANCE.getInCollection_Volume();

		/**
		 * The meta object literal for the '<em><b>ISBN</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IN_COLLECTION__ISBN = eINSTANCE.getInCollection_ISBN();

		/**
		 * The meta object literal for the '<em><b>First Page</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IN_COLLECTION__FIRST_PAGE = eINSTANCE.getInCollection_FirstPage();

		/**
		 * The meta object literal for the '<em><b>Last Page</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IN_COLLECTION__LAST_PAGE = eINSTANCE.getInCollection_LastPage();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.BookImpl <em>Book</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.BookImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getBook()
		 * @generated
		 */
		EClass BOOK = eINSTANCE.getBook();

		/**
		 * The meta object literal for the '<em><b>Publisher</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOK__PUBLISHER = eINSTANCE.getBook_Publisher();

		/**
		 * The meta object literal for the '<em><b>Series</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOK__SERIES = eINSTANCE.getBook_Series();

		/**
		 * The meta object literal for the '<em><b>Volume</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOK__VOLUME = eINSTANCE.getBook_Volume();

		/**
		 * The meta object literal for the '<em><b>ISBN</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOK__ISBN = eINSTANCE.getBook_ISBN();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.BookChapterImpl <em>Book Chapter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.BookChapterImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getBookChapter()
		 * @generated
		 */
		EClass BOOK_CHAPTER = eINSTANCE.getBookChapter();

		/**
		 * The meta object literal for the '<em><b>Chapter Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOK_CHAPTER__CHAPTER_NUMBER = eINSTANCE.getBookChapter_ChapterNumber();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.ReportImpl <em>Report</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.ReportImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getReport()
		 * @generated
		 */
		EClass REPORT = eINSTANCE.getReport();

		/**
		 * The meta object literal for the '<em><b>Report Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REPORT__REPORT_NUMBER = eINSTANCE.getReport_ReportNumber();

		/**
		 * The meta object literal for the '<em><b>Institution</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REPORT__INSTITUTION = eINSTANCE.getReport_Institution();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.MiscellaneousImpl <em>Miscellaneous</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.MiscellaneousImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getMiscellaneous()
		 * @generated
		 */
		EClass MISCELLANEOUS = eINSTANCE.getMiscellaneous();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.PublishedDateImpl <em>Published Date</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.PublishedDateImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getPublishedDate()
		 * @generated
		 */
		EClass PUBLISHED_DATE = eINSTANCE.getPublishedDate();

		/**
		 * The meta object literal for the '<em><b>Day</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PUBLISHED_DATE__DAY = eINSTANCE.getPublishedDate_Day();

		/**
		 * The meta object literal for the '<em><b>Month</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PUBLISHED_DATE__MONTH = eINSTANCE.getPublishedDate_Month();

		/**
		 * The meta object literal for the '<em><b>Year</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PUBLISHED_DATE__YEAR = eINSTANCE.getPublishedDate_Year();

		/**
		 * The meta object literal for the '<em><b>Month Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PUBLISHED_DATE__MONTH_NAME = eINSTANCE.getPublishedDate_MonthName();

		/**
		 * The meta object literal for the '<em><b>Month Short Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PUBLISHED_DATE__MONTH_SHORT_NAME = eINSTANCE.getPublishedDate_MonthShortName();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.SerialNumberImpl <em>Serial Number</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.SerialNumberImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getSerialNumber()
		 * @generated
		 */
		EClass SERIAL_NUMBER = eINSTANCE.getSerialNumber();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SERIAL_NUMBER__VALUE = eINSTANCE.getSerialNumber_Value();

		/**
		 * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SERIAL_NUMBER__KIND = eINSTANCE.getSerialNumber_Kind();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.ResearchGroupImpl <em>Research Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.ResearchGroupImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getResearchGroup()
		 * @generated
		 */
		EClass RESEARCH_GROUP = eINSTANCE.getResearchGroup();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESEARCH_GROUP__NAME = eINSTANCE.getResearchGroup_Name();

		/**
		 * The meta object literal for the '<em><b>Acronym</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESEARCH_GROUP__ACRONYM = eINSTANCE.getResearchGroup_Acronym();

		/**
		 * The meta object literal for the '<em><b>URL</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESEARCH_GROUP__URL = eINSTANCE.getResearchGroup_URL();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.CourseImpl <em>Course</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.CourseImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getCourse()
		 * @generated
		 */
		EClass COURSE = eINSTANCE.getCourse();

		/**
		 * The meta object literal for the '<em><b>Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__CODE = eINSTANCE.getCourse_Code();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__NAME = eINSTANCE.getCourse_Name();

		/**
		 * The meta object literal for the '<em><b>Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__LEVEL = eINSTANCE.getCourse_Level();

		/**
		 * The meta object literal for the '<em><b>Hours</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__HOURS = eINSTANCE.getCourse_Hours();

		/**
		 * The meta object literal for the '<em><b>Credits</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__CREDITS = eINSTANCE.getCourse_Credits();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.TaughtCourseImpl <em>Taught Course</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.TaughtCourseImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getTaughtCourse()
		 * @generated
		 */
		EClass TAUGHT_COURSE = eINSTANCE.getTaughtCourse();

		/**
		 * The meta object literal for the '<em><b>Year</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAUGHT_COURSE__YEAR = eINSTANCE.getTaughtCourse_Year();

		/**
		 * The meta object literal for the '<em><b>Period</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAUGHT_COURSE__PERIOD = eINSTANCE.getTaughtCourse_Period();

		/**
		 * The meta object literal for the '<em><b>Webinfo</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAUGHT_COURSE__WEBINFO = eINSTANCE.getTaughtCourse_Webinfo();

		/**
		 * The meta object literal for the '<em><b>Website</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAUGHT_COURSE__WEBSITE = eINSTANCE.getTaughtCourse_Website();

		/**
		 * The meta object literal for the '<em><b>Language</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAUGHT_COURSE__LANGUAGE = eINSTANCE.getTaughtCourse_Language();

		/**
		 * The meta object literal for the '<em><b>Canceled</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAUGHT_COURSE__CANCELED = eINSTANCE.getTaughtCourse_Canceled();

		/**
		 * The meta object literal for the '<em><b>Responsibility</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAUGHT_COURSE__RESPONSIBILITY = eINSTANCE.getTaughtCourse_Responsibility();

		/**
		 * The meta object literal for the '<em><b>Offering</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TAUGHT_COURSE__OFFERING = eINSTANCE.getTaughtCourse_Offering();

		/**
		 * The meta object literal for the '<em><b>Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAUGHT_COURSE__CODE = eINSTANCE.getTaughtCourse_Code();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAUGHT_COURSE__NAME = eINSTANCE.getTaughtCourse_Name();

		/**
		 * The meta object literal for the '<em><b>Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TAUGHT_COURSE__LEVEL = eINSTANCE.getTaughtCourse_Level();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.ResearchTopicImpl <em>Research Topic</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.ResearchTopicImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getResearchTopic()
		 * @generated
		 */
		EClass RESEARCH_TOPIC = eINSTANCE.getResearchTopic();

		/**
		 * The meta object literal for the '<em><b>Title</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESEARCH_TOPIC__TITLE = eINSTANCE.getResearchTopic_Title();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESEARCH_TOPIC__DESCRIPTION = eINSTANCE.getResearchTopic_Description();

		/**
		 * The meta object literal for the '<em><b>Related Papers</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESEARCH_TOPIC__RELATED_PAPERS = eINSTANCE.getResearchTopic_RelatedPapers();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.ProjectImpl <em>Project</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.ProjectImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getProject()
		 * @generated
		 */
		EClass PROJECT = eINSTANCE.getProject();

		/**
		 * The meta object literal for the '<em><b>Acronym</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROJECT__ACRONYM = eINSTANCE.getProject_Acronym();

		/**
		 * The meta object literal for the '<em><b>Title</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROJECT__TITLE = eINSTANCE.getProject_Title();

		/**
		 * The meta object literal for the '<em><b>Short Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROJECT__SHORT_NAME = eINSTANCE.getProject_ShortName();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.GrantImpl <em>Grant</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.GrantImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getGrant()
		 * @generated
		 */
		EClass GRANT = eINSTANCE.getGrant();

		/**
		 * The meta object literal for the '<em><b>Related Publications</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRANT__RELATED_PUBLICATIONS = eINSTANCE.getGrant_RelatedPublications();

		/**
		 * The meta object literal for the '<em><b>URL</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRANT__URL = eINSTANCE.getGrant_URL();

		/**
		 * The meta object literal for the '<em><b>Agency</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRANT__AGENCY = eINSTANCE.getGrant_Agency();

		/**
		 * The meta object literal for the '<em><b>Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRANT__NUMBER = eINSTANCE.getGrant_Number();

		/**
		 * The meta object literal for the '<em><b>Program</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRANT__PROGRAM = eINSTANCE.getGrant_Program();

		/**
		 * The meta object literal for the '<em><b>Call</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRANT__CALL = eINSTANCE.getGrant_Call();

		/**
		 * The meta object literal for the '<em><b>Grant ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRANT__GRANT_ID = eINSTANCE.getGrant_GrantID();

		/**
		 * The meta object literal for the '<em><b>Role</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRANT__ROLE = eINSTANCE.getGrant_Role();

		/**
		 * The meta object literal for the '<em><b>Budget</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRANT__BUDGET = eINSTANCE.getGrant_Budget();

		/**
		 * The meta object literal for the '<em><b>Start Date</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRANT__START_DATE = eINSTANCE.getGrant_StartDate();

		/**
		 * The meta object literal for the '<em><b>End Date</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRANT__END_DATE = eINSTANCE.getGrant_EndDate();

		/**
		 * The meta object literal for the '<em><b>Is Active</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation GRANT___IS_ACTIVE = eINSTANCE.getGrant__IsActive();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.BudgetImpl <em>Budget</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.BudgetImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getBudget()
		 * @generated
		 */
		EClass BUDGET = eINSTANCE.getBudget();

		/**
		 * The meta object literal for the '<em><b>Amount</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BUDGET__AMOUNT = eINSTANCE.getBudget_Amount();

		/**
		 * The meta object literal for the '<em><b>Currency</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BUDGET__CURRENCY = eINSTANCE.getBudget_Currency();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.RootElementImpl <em>Root Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.RootElementImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getRootElement()
		 * @generated
		 */
		EClass ROOT_ELEMENT = eINSTANCE.getRootElement();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.PersonRegistryImpl <em>Person Registry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.PersonRegistryImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getPersonRegistry()
		 * @generated
		 */
		EClass PERSON_REGISTRY = eINSTANCE.getPersonRegistry();

		/**
		 * The meta object literal for the '<em><b>Person</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERSON_REGISTRY__PERSON = eINSTANCE.getPersonRegistry_Person();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.CourseOfferingImpl <em>Course Offering</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.CourseOfferingImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getCourseOffering()
		 * @generated
		 */
		EClass COURSE_OFFERING = eINSTANCE.getCourseOffering();

		/**
		 * The meta object literal for the '<em><b>Groups</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_OFFERING__GROUPS = eINSTANCE.getCourseOffering_Groups();

		/**
		 * The meta object literal for the '<em><b>Specific Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE_OFFERING__SPECIFIC_NAME = eINSTANCE.getCourseOffering_SpecificName();

		/**
		 * The meta object literal for the '<em><b>Course</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_OFFERING__COURSE = eINSTANCE.getCourseOffering_Course();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.SupervisionImpl <em>Supervision</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.SupervisionImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getSupervision()
		 * @generated
		 */
		EClass SUPERVISION = eINSTANCE.getSupervision();

		/**
		 * The meta object literal for the '<em><b>Daste Start</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUPERVISION__DASTE_START = eINSTANCE.getSupervision_DasteStart();

		/**
		 * The meta object literal for the '<em><b>Date End</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUPERVISION__DATE_END = eINSTANCE.getSupervision_DateEnd();

		/**
		 * The meta object literal for the '<em><b>Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUPERVISION__LEVEL = eINSTANCE.getSupervision_Level();

		/**
		 * The meta object literal for the '<em><b>Student</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUPERVISION__STUDENT = eINSTANCE.getSupervision_Student();

		/**
		 * The meta object literal for the '<em><b>Project Title</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUPERVISION__PROJECT_TITLE = eINSTANCE.getSupervision_ProjectTitle();

		/**
		 * The meta object literal for the '<em><b>Supervisors</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUPERVISION__SUPERVISORS = eINSTANCE.getSupervision_Supervisors();

		/**
		 * The meta object literal for the '<em><b>Institutions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUPERVISION__INSTITUTIONS = eINSTANCE.getSupervision_Institutions();

		/**
		 * The meta object literal for the '<em><b>Main Institution</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUPERVISION__MAIN_INSTITUTION = eINSTANCE.getSupervision_MainInstitution();

		/**
		 * The meta object literal for the '<em><b>Main Supervisor</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUPERVISION__MAIN_SUPERVISOR = eINSTANCE.getSupervision_MainSupervisor();

		/**
		 * The meta object literal for the '<em><b>Completed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUPERVISION__COMPLETED = eINSTANCE.getSupervision_Completed();

		/**
		 * The meta object literal for the '<em><b>Final Work URL</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUPERVISION__FINAL_WORK_URL = eINSTANCE.getSupervision_FinalWorkURL();

		/**
		 * The meta object literal for the '<em><b>Final Work DOI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUPERVISION__FINAL_WORK_DOI = eINSTANCE.getSupervision_FinalWorkDOI();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.InstitutionRegistryImpl <em>Institution Registry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.InstitutionRegistryImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getInstitutionRegistry()
		 * @generated
		 */
		EClass INSTITUTION_REGISTRY = eINSTANCE.getInstitutionRegistry();

		/**
		 * The meta object literal for the '<em><b>Institutions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INSTITUTION_REGISTRY__INSTITUTIONS = eINSTANCE.getInstitutionRegistry_Institutions();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.InstitutionImpl <em>Institution</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.InstitutionImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getInstitution()
		 * @generated
		 */
		EClass INSTITUTION = eINSTANCE.getInstitution();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INSTITUTION__NAME = eINSTANCE.getInstitution_Name();

		/**
		 * The meta object literal for the '<em><b>Units</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INSTITUTION__UNITS = eINSTANCE.getInstitution_Units();

		/**
		 * The meta object literal for the '<em><b>Country</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INSTITUTION__COUNTRY = eINSTANCE.getInstitution_Country();

		/**
		 * The meta object literal for the '<em><b>Full Name</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INSTITUTION__FULL_NAME = eINSTANCE.getInstitution_FullName();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.MultiLanguageStringImpl <em>Multi Language String</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.MultiLanguageStringImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getMultiLanguageString()
		 * @generated
		 */
		EClass MULTI_LANGUAGE_STRING = eINSTANCE.getMultiLanguageString();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MULTI_LANGUAGE_STRING__TEXT = eINSTANCE.getMultiLanguageString_Text();

		/**
		 * The meta object literal for the '<em><b>Localizations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MULTI_LANGUAGE_STRING__LOCALIZATIONS = eINSTANCE.getMultiLanguageString_Localizations();

		/**
		 * The meta object literal for the '<em><b>Base Language</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MULTI_LANGUAGE_STRING__BASE_LANGUAGE = eINSTANCE.getMultiLanguageString_BaseLanguage();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.LocalizedStringImpl <em>Localized String</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.LocalizedStringImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getLocalizedString()
		 * @generated
		 */
		EClass LOCALIZED_STRING = eINSTANCE.getLocalizedString();

		/**
		 * The meta object literal for the '<em><b>Language</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOCALIZED_STRING__LANGUAGE = eINSTANCE.getLocalizedString_Language();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.impl.SupervisionsRegistryImpl <em>Supervisions Registry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.impl.SupervisionsRegistryImpl
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getSupervisionsRegistry()
		 * @generated
		 */
		EClass SUPERVISIONS_REGISTRY = eINSTANCE.getSupervisionsRegistry();

		/**
		 * The meta object literal for the '<em><b>Supervisions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUPERVISIONS_REGISTRY__SUPERVISIONS = eINSTANCE.getSupervisionsRegistry_Supervisions();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOCALIZED_STRING__TEXT = eINSTANCE.getLocalizedString_Text();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.ContactKind <em>Contact Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.ContactKind
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getContactKind()
		 * @generated
		 */
		EEnum CONTACT_KIND = eINSTANCE.getContactKind();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.SerialNumberKind <em>Serial Number Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.SerialNumberKind
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getSerialNumberKind()
		 * @generated
		 */
		EEnum SERIAL_NUMBER_KIND = eINSTANCE.getSerialNumberKind();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.CourseKind <em>Course Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.CourseKind
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getCourseKind()
		 * @generated
		 */
		EEnum COURSE_KIND = eINSTANCE.getCourseKind();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.SupervisionKind <em>Supervision Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.SupervisionKind
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getSupervisionKind()
		 * @generated
		 */
		EEnum SUPERVISION_KIND = eINSTANCE.getSupervisionKind();

		/**
		 * The meta object literal for the '{@link org.montex.researchcv.ParticipationKind <em>Participation Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.montex.researchcv.ParticipationKind
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getParticipationKind()
		 * @generated
		 */
		EEnum PARTICIPATION_KIND = eINSTANCE.getParticipationKind();

		/**
		 * The meta object literal for the '<em>Currency</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.util.Currency
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getCurrency()
		 * @generated
		 */
		EDataType CURRENCY = eINSTANCE.getCurrency();

		/**
		 * The meta object literal for the '<em>Language Code</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.util.Locale
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getLanguageCode()
		 * @generated
		 */
		EDataType LANGUAGE_CODE = eINSTANCE.getLanguageCode();

		/**
		 * The meta object literal for the '<em>Country Code</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.util.Locale
		 * @see org.montex.researchcv.impl.ResearchCVPackageImpl#getCountryCode()
		 * @generated
		 */
		EDataType COUNTRY_CODE = eINSTANCE.getCountryCode();

	}

} //ResearchCVPackage
