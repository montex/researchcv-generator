/**
 */
package org.montex.researchcv.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.montex.researchcv.BookChapter;
import org.montex.researchcv.ResearchCVPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Book Chapter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.impl.BookChapterImpl#getChapterNumber <em>Chapter Number</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BookChapterImpl extends InCollectionImpl implements BookChapter {
	/**
	 * The default value of the '{@link #getChapterNumber() <em>Chapter Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChapterNumber()
	 * @generated
	 * @ordered
	 */
	protected static final String CHAPTER_NUMBER_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getChapterNumber() <em>Chapter Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChapterNumber()
	 * @generated
	 * @ordered
	 */
	protected String chapterNumber = CHAPTER_NUMBER_EDEFAULT;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BookChapterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ResearchCVPackage.Literals.BOOK_CHAPTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getChapterNumber() {
		return chapterNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setChapterNumber(String newChapterNumber) {
		String oldChapterNumber = chapterNumber;
		chapterNumber = newChapterNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.BOOK_CHAPTER__CHAPTER_NUMBER, oldChapterNumber, chapterNumber));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ResearchCVPackage.BOOK_CHAPTER__CHAPTER_NUMBER:
				return getChapterNumber();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ResearchCVPackage.BOOK_CHAPTER__CHAPTER_NUMBER:
				setChapterNumber((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.BOOK_CHAPTER__CHAPTER_NUMBER:
				setChapterNumber(CHAPTER_NUMBER_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.BOOK_CHAPTER__CHAPTER_NUMBER:
				return CHAPTER_NUMBER_EDEFAULT == null ? chapterNumber != null : !CHAPTER_NUMBER_EDEFAULT.equals(chapterNumber);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (chapterNumber: ");
		result.append(chapterNumber);
		result.append(')');
		return result.toString();
	}

} //BookChapterImpl
