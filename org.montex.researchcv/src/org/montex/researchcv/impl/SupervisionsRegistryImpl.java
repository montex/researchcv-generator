/**
 */
package org.montex.researchcv.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.montex.researchcv.ResearchCVPackage;
import org.montex.researchcv.Supervision;
import org.montex.researchcv.SupervisionsRegistry;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Supervisions Registry</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.impl.SupervisionsRegistryImpl#getSupervisions <em>Supervisions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SupervisionsRegistryImpl extends RootElementImpl implements SupervisionsRegistry {
	/**
	 * The cached value of the '{@link #getSupervisions() <em>Supervisions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSupervisions()
	 * @generated
	 * @ordered
	 */
	protected EList<Supervision> supervisions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SupervisionsRegistryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ResearchCVPackage.Literals.SUPERVISIONS_REGISTRY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Supervision> getSupervisions() {
		if (supervisions == null) {
			supervisions = new EObjectContainmentEList<Supervision>(Supervision.class, this, ResearchCVPackage.SUPERVISIONS_REGISTRY__SUPERVISIONS);
		}
		return supervisions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ResearchCVPackage.SUPERVISIONS_REGISTRY__SUPERVISIONS:
				return ((InternalEList<?>)getSupervisions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ResearchCVPackage.SUPERVISIONS_REGISTRY__SUPERVISIONS:
				return getSupervisions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ResearchCVPackage.SUPERVISIONS_REGISTRY__SUPERVISIONS:
				getSupervisions().clear();
				getSupervisions().addAll((Collection<? extends Supervision>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.SUPERVISIONS_REGISTRY__SUPERVISIONS:
				getSupervisions().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.SUPERVISIONS_REGISTRY__SUPERVISIONS:
				return supervisions != null && !supervisions.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //SupervisionsRegistryImpl
