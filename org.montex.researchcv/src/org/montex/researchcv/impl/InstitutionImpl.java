/**
 */
package org.montex.researchcv.impl;

import java.util.Collection;

import java.util.Locale;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.montex.researchcv.Institution;
import org.montex.researchcv.MultiLanguageString;
import org.montex.researchcv.ResearchCVPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Institution</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.impl.InstitutionImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.InstitutionImpl#getUnits <em>Units</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.InstitutionImpl#getCountry <em>Country</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.InstitutionImpl#getFullName <em>Full Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InstitutionImpl extends MinimalEObjectImpl.Container implements Institution {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getUnits() <em>Units</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnits()
	 * @generated
	 * @ordered
	 */
	protected EList<Institution> units;

	/**
	 * The default value of the '{@link #getCountry() <em>Country</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCountry()
	 * @generated
	 * @ordered
	 */
	protected static final Locale COUNTRY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCountry() <em>Country</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCountry()
	 * @generated
	 * @ordered
	 */
	protected Locale country = COUNTRY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getFullName() <em>Full Name</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFullName()
	 * @generated
	 * @ordered
	 */
	protected MultiLanguageString fullName;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InstitutionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ResearchCVPackage.Literals.INSTITUTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.INSTITUTION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Institution> getUnits() {
		if (units == null) {
			units = new EObjectContainmentEList<Institution>(Institution.class, this, ResearchCVPackage.INSTITUTION__UNITS);
		}
		return units;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Locale getCountry() {
		return country;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCountry(Locale newCountry) {
		Locale oldCountry = country;
		country = newCountry;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.INSTITUTION__COUNTRY, oldCountry, country));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MultiLanguageString getFullName() {
		return fullName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFullName(MultiLanguageString newFullName, NotificationChain msgs) {
		MultiLanguageString oldFullName = fullName;
		fullName = newFullName;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ResearchCVPackage.INSTITUTION__FULL_NAME, oldFullName, newFullName);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFullName(MultiLanguageString newFullName) {
		if (newFullName != fullName) {
			NotificationChain msgs = null;
			if (fullName != null)
				msgs = ((InternalEObject)fullName).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ResearchCVPackage.INSTITUTION__FULL_NAME, null, msgs);
			if (newFullName != null)
				msgs = ((InternalEObject)newFullName).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ResearchCVPackage.INSTITUTION__FULL_NAME, null, msgs);
			msgs = basicSetFullName(newFullName, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.INSTITUTION__FULL_NAME, newFullName, newFullName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ResearchCVPackage.INSTITUTION__UNITS:
				return ((InternalEList<?>)getUnits()).basicRemove(otherEnd, msgs);
			case ResearchCVPackage.INSTITUTION__FULL_NAME:
				return basicSetFullName(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ResearchCVPackage.INSTITUTION__NAME:
				return getName();
			case ResearchCVPackage.INSTITUTION__UNITS:
				return getUnits();
			case ResearchCVPackage.INSTITUTION__COUNTRY:
				return getCountry();
			case ResearchCVPackage.INSTITUTION__FULL_NAME:
				return getFullName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ResearchCVPackage.INSTITUTION__NAME:
				setName((String)newValue);
				return;
			case ResearchCVPackage.INSTITUTION__UNITS:
				getUnits().clear();
				getUnits().addAll((Collection<? extends Institution>)newValue);
				return;
			case ResearchCVPackage.INSTITUTION__COUNTRY:
				setCountry((Locale)newValue);
				return;
			case ResearchCVPackage.INSTITUTION__FULL_NAME:
				setFullName((MultiLanguageString)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.INSTITUTION__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ResearchCVPackage.INSTITUTION__UNITS:
				getUnits().clear();
				return;
			case ResearchCVPackage.INSTITUTION__COUNTRY:
				setCountry(COUNTRY_EDEFAULT);
				return;
			case ResearchCVPackage.INSTITUTION__FULL_NAME:
				setFullName((MultiLanguageString)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.INSTITUTION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case ResearchCVPackage.INSTITUTION__UNITS:
				return units != null && !units.isEmpty();
			case ResearchCVPackage.INSTITUTION__COUNTRY:
				return COUNTRY_EDEFAULT == null ? country != null : !COUNTRY_EDEFAULT.equals(country);
			case ResearchCVPackage.INSTITUTION__FULL_NAME:
				return fullName != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", country: ");
		result.append(country);
		result.append(')');
		return result.toString();
	}

} //InstitutionImpl
