/**
 */
package org.montex.researchcv.impl;

import java.util.Currency;
import java.util.Locale;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.montex.researchcv.Address;
import org.montex.researchcv.Book;
import org.montex.researchcv.BookChapter;
import org.montex.researchcv.Budget;
import org.montex.researchcv.Conference;
import org.montex.researchcv.Contact;
import org.montex.researchcv.ContactKind;
import org.montex.researchcv.Course;
import org.montex.researchcv.CourseKind;
import org.montex.researchcv.CourseOffering;
import org.montex.researchcv.Email;
import org.montex.researchcv.Grant;
import org.montex.researchcv.InCollection;
import org.montex.researchcv.InProceedings;
import org.montex.researchcv.Institution;
import org.montex.researchcv.InstitutionRegistry;
import org.montex.researchcv.Journal;
import org.montex.researchcv.LocalizedString;
import org.montex.researchcv.Miscellaneous;
import org.montex.researchcv.MultiLanguageString;
import org.montex.researchcv.ParticipationKind;
import org.montex.researchcv.Person;
import org.montex.researchcv.PersonRegistry;
import org.montex.researchcv.Phone;
import org.montex.researchcv.Project;
import org.montex.researchcv.Publication;
import org.montex.researchcv.PublishedDate;
import org.montex.researchcv.Report;
import org.montex.researchcv.ResearchCVFactory;
import org.montex.researchcv.ResearchCVPackage;
import org.montex.researchcv.ResearchGroup;
import org.montex.researchcv.ResearchTopic;
import org.montex.researchcv.Researcher;
import org.montex.researchcv.RootElement;
import org.montex.researchcv.SerialNumber;
import org.montex.researchcv.SerialNumberKind;
import org.montex.researchcv.SocialProfile;
import org.montex.researchcv.Supervision;
import org.montex.researchcv.SupervisionKind;
import org.montex.researchcv.SupervisionsRegistry;
import org.montex.researchcv.TaughtCourse;
import org.montex.researchcv.Website;
import org.montex.researchcv.Workshop;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ResearchCVPackageImpl extends EPackageImpl implements ResearchCVPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass researcherEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass addressEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass contactEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass emailEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass phoneEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass socialProfileEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass websiteEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass publicationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass journalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass conferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass workshopEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass inProceedingsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass inCollectionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bookEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bookChapterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass reportEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass miscellaneousEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass publishedDateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serialNumberEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass researchGroupEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass courseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass taughtCourseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass researchTopicEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass projectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass grantEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass budgetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rootElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass personRegistryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass courseOfferingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass supervisionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass institutionRegistryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass institutionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass multiLanguageStringEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass localizedStringEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass supervisionsRegistryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum contactKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum serialNumberKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum courseKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum supervisionKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum participationKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType currencyEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType languageCodeEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType countryCodeEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.montex.researchcv.ResearchCVPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ResearchCVPackageImpl() {
		super(eNS_URI, ResearchCVFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link ResearchCVPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ResearchCVPackage init() {
		if (isInited) return (ResearchCVPackage)EPackage.Registry.INSTANCE.getEPackage(ResearchCVPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredResearchCVPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		ResearchCVPackageImpl theResearchCVPackage = registeredResearchCVPackage instanceof ResearchCVPackageImpl ? (ResearchCVPackageImpl)registeredResearchCVPackage : new ResearchCVPackageImpl();

		isInited = true;

		// Create package meta-data objects
		theResearchCVPackage.createPackageContents();

		// Initialize created meta-data
		theResearchCVPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theResearchCVPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ResearchCVPackage.eNS_URI, theResearchCVPackage);
		return theResearchCVPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPerson() {
		return personEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPerson_FirstNames() {
		return (EAttribute)personEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPerson_LastNames() {
		return (EAttribute)personEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPerson_Name() {
		return (EAttribute)personEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getResearcher() {
		return researcherEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getResearcher_Coauthors() {
		return (EReference)researcherEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getResearcher_Contacts() {
		return (EReference)researcherEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getResearcher_Socials() {
		return (EReference)researcherEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getResearcher_Publications() {
		return (EReference)researcherEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getResearcher_TeachingMaterial() {
		return (EReference)researcherEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getResearcher_OtherMaterial() {
		return (EReference)researcherEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getResearcher_ResearchGroup() {
		return (EReference)researcherEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getResearcher_Topics() {
		return (EReference)researcherEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getResearcher_Grants() {
		return (EReference)researcherEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getResearcher_Courses() {
		return (EReference)researcherEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getResearcher_TaughtCourses() {
		return (EReference)researcherEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getResearcher_SupervisionsRegistry() {
		return (EReference)researcherEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getResearcher_Supervisions() {
		return (EReference)researcherEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAddress() {
		return addressEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAddress_Lines() {
		return (EAttribute)addressEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAddress_Zip() {
		return (EAttribute)addressEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAddress_City() {
		return (EAttribute)addressEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAddress_Region() {
		return (EAttribute)addressEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAddress_Country() {
		return (EAttribute)addressEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAddress_Building() {
		return (EAttribute)addressEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAddress_Room() {
		return (EAttribute)addressEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getContact() {
		return contactEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getContact_Kind() {
		return (EAttribute)contactEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getEmail() {
		return emailEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEmail_Address() {
		return (EAttribute)emailEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPhone() {
		return phoneEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPhone_Number() {
		return (EAttribute)phoneEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSocialProfile() {
		return socialProfileEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSocialProfile_Name() {
		return (EAttribute)socialProfileEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSocialProfile_Url() {
		return (EAttribute)socialProfileEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getWebsite() {
		return websiteEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getWebsite_Url() {
		return (EAttribute)websiteEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPublication() {
		return publicationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPublication_Citekey() {
		return (EAttribute)publicationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPublication_Title() {
		return (EAttribute)publicationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPublication_Authors() {
		return (EReference)publicationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPublication_URL() {
		return (EAttribute)publicationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPublication_Published() {
		return (EAttribute)publicationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPublication_OpenAccess() {
		return (EAttribute)publicationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPublication_Date() {
		return (EReference)publicationEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPublication_DOI() {
		return (EAttribute)publicationEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPublication_Abstract() {
		return (EAttribute)publicationEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPublication_WithAuthorVersion() {
		return (EAttribute)publicationEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPublication_Notes() {
		return (EAttribute)publicationEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPublication_RelatedProjects() {
		return (EReference)publicationEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getJournal() {
		return journalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getJournal_JournalName() {
		return (EAttribute)journalEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getJournal_Publisher() {
		return (EAttribute)journalEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getJournal_FirstPage() {
		return (EAttribute)journalEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getJournal_LastPage() {
		return (EAttribute)journalEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getJournal_Volume() {
		return (EAttribute)journalEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getJournal_Issue() {
		return (EAttribute)journalEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getJournal_ISSN() {
		return (EReference)journalEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getJournal_Magazine() {
		return (EAttribute)journalEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getConference() {
		return conferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getWorkshop() {
		return workshopEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getWorkshop_MainEventName() {
		return (EAttribute)workshopEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getWorkshop_MainEventShortName() {
		return (EAttribute)workshopEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getInProceedings() {
		return inProceedingsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getInProceedings_EventEndDate() {
		return (EReference)inProceedingsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInProceedings_EventName() {
		return (EAttribute)inProceedingsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInProceedings_Venue() {
		return (EAttribute)inProceedingsEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInProceedings_EventShortName() {
		return (EAttribute)inProceedingsEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getInProceedings_EventStartDate() {
		return (EReference)inProceedingsEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInProceedings_TrackName() {
		return (EAttribute)inProceedingsEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInProceedings_TrackShortName() {
		return (EAttribute)inProceedingsEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInProceedings_ShortPaper() {
		return (EAttribute)inProceedingsEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInProceedings_ToolPaper() {
		return (EAttribute)inProceedingsEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInProceedings_InvitedPaper() {
		return (EAttribute)inProceedingsEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getInCollection() {
		return inCollectionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInCollection_Booktitle() {
		return (EAttribute)inCollectionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getInCollection_Bookeditors() {
		return (EReference)inCollectionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInCollection_Publisher() {
		return (EAttribute)inCollectionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInCollection_Series() {
		return (EAttribute)inCollectionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInCollection_Volume() {
		return (EAttribute)inCollectionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getInCollection_ISBN() {
		return (EReference)inCollectionEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInCollection_FirstPage() {
		return (EAttribute)inCollectionEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInCollection_LastPage() {
		return (EAttribute)inCollectionEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getBook() {
		return bookEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getBook_Publisher() {
		return (EAttribute)bookEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getBook_Series() {
		return (EAttribute)bookEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getBook_Volume() {
		return (EAttribute)bookEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBook_ISBN() {
		return (EReference)bookEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getBookChapter() {
		return bookChapterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getBookChapter_ChapterNumber() {
		return (EAttribute)bookChapterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getReport() {
		return reportEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getReport_ReportNumber() {
		return (EAttribute)reportEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getReport_Institution() {
		return (EAttribute)reportEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMiscellaneous() {
		return miscellaneousEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPublishedDate() {
		return publishedDateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPublishedDate_Day() {
		return (EAttribute)publishedDateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPublishedDate_Month() {
		return (EAttribute)publishedDateEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPublishedDate_Year() {
		return (EAttribute)publishedDateEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPublishedDate_MonthName() {
		return (EAttribute)publishedDateEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPublishedDate_MonthShortName() {
		return (EAttribute)publishedDateEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSerialNumber() {
		return serialNumberEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSerialNumber_Value() {
		return (EAttribute)serialNumberEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSerialNumber_Kind() {
		return (EAttribute)serialNumberEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getResearchGroup() {
		return researchGroupEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getResearchGroup_Name() {
		return (EAttribute)researchGroupEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getResearchGroup_Acronym() {
		return (EAttribute)researchGroupEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getResearchGroup_URL() {
		return (EAttribute)researchGroupEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCourse() {
		return courseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCourse_Code() {
		return (EAttribute)courseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCourse_Name() {
		return (EAttribute)courseEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCourse_Level() {
		return (EAttribute)courseEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCourse_Hours() {
		return (EAttribute)courseEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCourse_Credits() {
		return (EAttribute)courseEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTaughtCourse() {
		return taughtCourseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTaughtCourse_Year() {
		return (EAttribute)taughtCourseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTaughtCourse_Period() {
		return (EAttribute)taughtCourseEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTaughtCourse_Webinfo() {
		return (EAttribute)taughtCourseEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTaughtCourse_Website() {
		return (EAttribute)taughtCourseEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTaughtCourse_Language() {
		return (EAttribute)taughtCourseEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTaughtCourse_Canceled() {
		return (EAttribute)taughtCourseEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTaughtCourse_Responsibility() {
		return (EAttribute)taughtCourseEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTaughtCourse_Offering() {
		return (EReference)taughtCourseEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTaughtCourse_Code() {
		return (EAttribute)taughtCourseEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTaughtCourse_Name() {
		return (EAttribute)taughtCourseEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTaughtCourse_Level() {
		return (EAttribute)taughtCourseEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getResearchTopic() {
		return researchTopicEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getResearchTopic_Title() {
		return (EAttribute)researchTopicEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getResearchTopic_Description() {
		return (EAttribute)researchTopicEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getResearchTopic_RelatedPapers() {
		return (EReference)researchTopicEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getProject() {
		return projectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getProject_Acronym() {
		return (EAttribute)projectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getProject_Title() {
		return (EAttribute)projectEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getProject_ShortName() {
		return (EAttribute)projectEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGrant() {
		return grantEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getGrant_RelatedPublications() {
		return (EReference)grantEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGrant_URL() {
		return (EAttribute)grantEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGrant_Agency() {
		return (EAttribute)grantEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGrant_Number() {
		return (EAttribute)grantEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGrant_Program() {
		return (EAttribute)grantEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGrant_Call() {
		return (EAttribute)grantEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGrant_GrantID() {
		return (EAttribute)grantEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGrant_Role() {
		return (EAttribute)grantEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getGrant_Budget() {
		return (EReference)grantEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getGrant_StartDate() {
		return (EReference)grantEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getGrant_EndDate() {
		return (EReference)grantEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getGrant__IsActive() {
		return grantEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getBudget() {
		return budgetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getBudget_Amount() {
		return (EAttribute)budgetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getBudget_Currency() {
		return (EAttribute)budgetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRootElement() {
		return rootElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPersonRegistry() {
		return personRegistryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPersonRegistry_Person() {
		return (EReference)personRegistryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCourseOffering() {
		return courseOfferingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCourseOffering_Groups() {
		return (EAttribute)courseOfferingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCourseOffering_SpecificName() {
		return (EAttribute)courseOfferingEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCourseOffering_Course() {
		return (EReference)courseOfferingEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSupervision() {
		return supervisionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSupervision_DasteStart() {
		return (EReference)supervisionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSupervision_DateEnd() {
		return (EReference)supervisionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSupervision_Level() {
		return (EAttribute)supervisionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSupervision_Student() {
		return (EReference)supervisionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSupervision_ProjectTitle() {
		return (EAttribute)supervisionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSupervision_Supervisors() {
		return (EReference)supervisionEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSupervision_Institutions() {
		return (EReference)supervisionEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSupervision_MainInstitution() {
		return (EReference)supervisionEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSupervision_MainSupervisor() {
		return (EReference)supervisionEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSupervision_Completed() {
		return (EAttribute)supervisionEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSupervision_FinalWorkURL() {
		return (EAttribute)supervisionEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSupervision_FinalWorkDOI() {
		return (EAttribute)supervisionEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getInstitutionRegistry() {
		return institutionRegistryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getInstitutionRegistry_Institutions() {
		return (EReference)institutionRegistryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getInstitution() {
		return institutionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInstitution_Name() {
		return (EAttribute)institutionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getInstitution_Units() {
		return (EReference)institutionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getInstitution_Country() {
		return (EAttribute)institutionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getInstitution_FullName() {
		return (EReference)institutionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMultiLanguageString() {
		return multiLanguageStringEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMultiLanguageString_Text() {
		return (EAttribute)multiLanguageStringEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMultiLanguageString_Localizations() {
		return (EReference)multiLanguageStringEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getMultiLanguageString_BaseLanguage() {
		return (EAttribute)multiLanguageStringEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getLocalizedString() {
		return localizedStringEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLocalizedString_Language() {
		return (EAttribute)localizedStringEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSupervisionsRegistry() {
		return supervisionsRegistryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSupervisionsRegistry_Supervisions() {
		return (EReference)supervisionsRegistryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLocalizedString_Text() {
		return (EAttribute)localizedStringEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getContactKind() {
		return contactKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getSerialNumberKind() {
		return serialNumberKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getCourseKind() {
		return courseKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getSupervisionKind() {
		return supervisionKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getParticipationKind() {
		return participationKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getCurrency() {
		return currencyEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getLanguageCode() {
		return languageCodeEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getCountryCode() {
		return countryCodeEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResearchCVFactory getResearchCVFactory() {
		return (ResearchCVFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		personEClass = createEClass(PERSON);
		createEAttribute(personEClass, PERSON__FIRST_NAMES);
		createEAttribute(personEClass, PERSON__LAST_NAMES);
		createEAttribute(personEClass, PERSON__NAME);

		researcherEClass = createEClass(RESEARCHER);
		createEReference(researcherEClass, RESEARCHER__COAUTHORS);
		createEReference(researcherEClass, RESEARCHER__CONTACTS);
		createEReference(researcherEClass, RESEARCHER__SOCIALS);
		createEReference(researcherEClass, RESEARCHER__PUBLICATIONS);
		createEReference(researcherEClass, RESEARCHER__TEACHING_MATERIAL);
		createEReference(researcherEClass, RESEARCHER__OTHER_MATERIAL);
		createEReference(researcherEClass, RESEARCHER__RESEARCH_GROUP);
		createEReference(researcherEClass, RESEARCHER__TOPICS);
		createEReference(researcherEClass, RESEARCHER__GRANTS);
		createEReference(researcherEClass, RESEARCHER__COURSES);
		createEReference(researcherEClass, RESEARCHER__TAUGHT_COURSES);
		createEReference(researcherEClass, RESEARCHER__SUPERVISIONS_REGISTRY);
		createEReference(researcherEClass, RESEARCHER__SUPERVISIONS);

		addressEClass = createEClass(ADDRESS);
		createEAttribute(addressEClass, ADDRESS__LINES);
		createEAttribute(addressEClass, ADDRESS__ZIP);
		createEAttribute(addressEClass, ADDRESS__CITY);
		createEAttribute(addressEClass, ADDRESS__REGION);
		createEAttribute(addressEClass, ADDRESS__COUNTRY);
		createEAttribute(addressEClass, ADDRESS__BUILDING);
		createEAttribute(addressEClass, ADDRESS__ROOM);

		contactEClass = createEClass(CONTACT);
		createEAttribute(contactEClass, CONTACT__KIND);

		emailEClass = createEClass(EMAIL);
		createEAttribute(emailEClass, EMAIL__ADDRESS);

		phoneEClass = createEClass(PHONE);
		createEAttribute(phoneEClass, PHONE__NUMBER);

		socialProfileEClass = createEClass(SOCIAL_PROFILE);
		createEAttribute(socialProfileEClass, SOCIAL_PROFILE__NAME);
		createEAttribute(socialProfileEClass, SOCIAL_PROFILE__URL);

		websiteEClass = createEClass(WEBSITE);
		createEAttribute(websiteEClass, WEBSITE__URL);

		publicationEClass = createEClass(PUBLICATION);
		createEAttribute(publicationEClass, PUBLICATION__CITEKEY);
		createEAttribute(publicationEClass, PUBLICATION__TITLE);
		createEReference(publicationEClass, PUBLICATION__AUTHORS);
		createEAttribute(publicationEClass, PUBLICATION__URL);
		createEAttribute(publicationEClass, PUBLICATION__PUBLISHED);
		createEAttribute(publicationEClass, PUBLICATION__OPEN_ACCESS);
		createEReference(publicationEClass, PUBLICATION__DATE);
		createEAttribute(publicationEClass, PUBLICATION__DOI);
		createEAttribute(publicationEClass, PUBLICATION__ABSTRACT);
		createEAttribute(publicationEClass, PUBLICATION__WITH_AUTHOR_VERSION);
		createEAttribute(publicationEClass, PUBLICATION__NOTES);
		createEReference(publicationEClass, PUBLICATION__RELATED_PROJECTS);

		journalEClass = createEClass(JOURNAL);
		createEAttribute(journalEClass, JOURNAL__JOURNAL_NAME);
		createEAttribute(journalEClass, JOURNAL__PUBLISHER);
		createEAttribute(journalEClass, JOURNAL__FIRST_PAGE);
		createEAttribute(journalEClass, JOURNAL__LAST_PAGE);
		createEAttribute(journalEClass, JOURNAL__VOLUME);
		createEAttribute(journalEClass, JOURNAL__ISSUE);
		createEReference(journalEClass, JOURNAL__ISSN);
		createEAttribute(journalEClass, JOURNAL__MAGAZINE);

		conferenceEClass = createEClass(CONFERENCE);

		workshopEClass = createEClass(WORKSHOP);
		createEAttribute(workshopEClass, WORKSHOP__MAIN_EVENT_NAME);
		createEAttribute(workshopEClass, WORKSHOP__MAIN_EVENT_SHORT_NAME);

		inProceedingsEClass = createEClass(IN_PROCEEDINGS);
		createEReference(inProceedingsEClass, IN_PROCEEDINGS__EVENT_END_DATE);
		createEAttribute(inProceedingsEClass, IN_PROCEEDINGS__EVENT_NAME);
		createEAttribute(inProceedingsEClass, IN_PROCEEDINGS__VENUE);
		createEAttribute(inProceedingsEClass, IN_PROCEEDINGS__EVENT_SHORT_NAME);
		createEReference(inProceedingsEClass, IN_PROCEEDINGS__EVENT_START_DATE);
		createEAttribute(inProceedingsEClass, IN_PROCEEDINGS__TRACK_NAME);
		createEAttribute(inProceedingsEClass, IN_PROCEEDINGS__TRACK_SHORT_NAME);
		createEAttribute(inProceedingsEClass, IN_PROCEEDINGS__SHORT_PAPER);
		createEAttribute(inProceedingsEClass, IN_PROCEEDINGS__TOOL_PAPER);
		createEAttribute(inProceedingsEClass, IN_PROCEEDINGS__INVITED_PAPER);

		inCollectionEClass = createEClass(IN_COLLECTION);
		createEAttribute(inCollectionEClass, IN_COLLECTION__BOOKTITLE);
		createEReference(inCollectionEClass, IN_COLLECTION__BOOKEDITORS);
		createEAttribute(inCollectionEClass, IN_COLLECTION__PUBLISHER);
		createEAttribute(inCollectionEClass, IN_COLLECTION__SERIES);
		createEAttribute(inCollectionEClass, IN_COLLECTION__VOLUME);
		createEAttribute(inCollectionEClass, IN_COLLECTION__FIRST_PAGE);
		createEAttribute(inCollectionEClass, IN_COLLECTION__LAST_PAGE);
		createEReference(inCollectionEClass, IN_COLLECTION__ISBN);

		bookEClass = createEClass(BOOK);
		createEAttribute(bookEClass, BOOK__PUBLISHER);
		createEAttribute(bookEClass, BOOK__SERIES);
		createEAttribute(bookEClass, BOOK__VOLUME);
		createEReference(bookEClass, BOOK__ISBN);

		bookChapterEClass = createEClass(BOOK_CHAPTER);
		createEAttribute(bookChapterEClass, BOOK_CHAPTER__CHAPTER_NUMBER);

		reportEClass = createEClass(REPORT);
		createEAttribute(reportEClass, REPORT__REPORT_NUMBER);
		createEAttribute(reportEClass, REPORT__INSTITUTION);

		miscellaneousEClass = createEClass(MISCELLANEOUS);

		publishedDateEClass = createEClass(PUBLISHED_DATE);
		createEAttribute(publishedDateEClass, PUBLISHED_DATE__DAY);
		createEAttribute(publishedDateEClass, PUBLISHED_DATE__MONTH);
		createEAttribute(publishedDateEClass, PUBLISHED_DATE__YEAR);
		createEAttribute(publishedDateEClass, PUBLISHED_DATE__MONTH_NAME);
		createEAttribute(publishedDateEClass, PUBLISHED_DATE__MONTH_SHORT_NAME);

		serialNumberEClass = createEClass(SERIAL_NUMBER);
		createEAttribute(serialNumberEClass, SERIAL_NUMBER__VALUE);
		createEAttribute(serialNumberEClass, SERIAL_NUMBER__KIND);

		researchGroupEClass = createEClass(RESEARCH_GROUP);
		createEAttribute(researchGroupEClass, RESEARCH_GROUP__NAME);
		createEAttribute(researchGroupEClass, RESEARCH_GROUP__ACRONYM);
		createEAttribute(researchGroupEClass, RESEARCH_GROUP__URL);

		courseEClass = createEClass(COURSE);
		createEAttribute(courseEClass, COURSE__CODE);
		createEAttribute(courseEClass, COURSE__NAME);
		createEAttribute(courseEClass, COURSE__LEVEL);
		createEAttribute(courseEClass, COURSE__HOURS);
		createEAttribute(courseEClass, COURSE__CREDITS);

		taughtCourseEClass = createEClass(TAUGHT_COURSE);
		createEAttribute(taughtCourseEClass, TAUGHT_COURSE__YEAR);
		createEAttribute(taughtCourseEClass, TAUGHT_COURSE__PERIOD);
		createEAttribute(taughtCourseEClass, TAUGHT_COURSE__WEBINFO);
		createEAttribute(taughtCourseEClass, TAUGHT_COURSE__WEBSITE);
		createEAttribute(taughtCourseEClass, TAUGHT_COURSE__CODE);
		createEAttribute(taughtCourseEClass, TAUGHT_COURSE__NAME);
		createEAttribute(taughtCourseEClass, TAUGHT_COURSE__LEVEL);
		createEAttribute(taughtCourseEClass, TAUGHT_COURSE__LANGUAGE);
		createEAttribute(taughtCourseEClass, TAUGHT_COURSE__CANCELED);
		createEAttribute(taughtCourseEClass, TAUGHT_COURSE__RESPONSIBILITY);
		createEReference(taughtCourseEClass, TAUGHT_COURSE__OFFERING);

		researchTopicEClass = createEClass(RESEARCH_TOPIC);
		createEAttribute(researchTopicEClass, RESEARCH_TOPIC__TITLE);
		createEAttribute(researchTopicEClass, RESEARCH_TOPIC__DESCRIPTION);
		createEReference(researchTopicEClass, RESEARCH_TOPIC__RELATED_PAPERS);

		projectEClass = createEClass(PROJECT);
		createEAttribute(projectEClass, PROJECT__ACRONYM);
		createEAttribute(projectEClass, PROJECT__TITLE);
		createEAttribute(projectEClass, PROJECT__SHORT_NAME);

		grantEClass = createEClass(GRANT);
		createEReference(grantEClass, GRANT__RELATED_PUBLICATIONS);
		createEAttribute(grantEClass, GRANT__URL);
		createEAttribute(grantEClass, GRANT__AGENCY);
		createEAttribute(grantEClass, GRANT__PROGRAM);
		createEAttribute(grantEClass, GRANT__CALL);
		createEAttribute(grantEClass, GRANT__NUMBER);
		createEReference(grantEClass, GRANT__BUDGET);
		createEReference(grantEClass, GRANT__START_DATE);
		createEReference(grantEClass, GRANT__END_DATE);
		createEAttribute(grantEClass, GRANT__GRANT_ID);
		createEAttribute(grantEClass, GRANT__ROLE);
		createEOperation(grantEClass, GRANT___IS_ACTIVE);

		budgetEClass = createEClass(BUDGET);
		createEAttribute(budgetEClass, BUDGET__AMOUNT);
		createEAttribute(budgetEClass, BUDGET__CURRENCY);

		rootElementEClass = createEClass(ROOT_ELEMENT);

		personRegistryEClass = createEClass(PERSON_REGISTRY);
		createEReference(personRegistryEClass, PERSON_REGISTRY__PERSON);

		courseOfferingEClass = createEClass(COURSE_OFFERING);
		createEAttribute(courseOfferingEClass, COURSE_OFFERING__GROUPS);
		createEAttribute(courseOfferingEClass, COURSE_OFFERING__SPECIFIC_NAME);
		createEReference(courseOfferingEClass, COURSE_OFFERING__COURSE);

		supervisionEClass = createEClass(SUPERVISION);
		createEReference(supervisionEClass, SUPERVISION__DASTE_START);
		createEReference(supervisionEClass, SUPERVISION__DATE_END);
		createEAttribute(supervisionEClass, SUPERVISION__LEVEL);
		createEReference(supervisionEClass, SUPERVISION__STUDENT);
		createEAttribute(supervisionEClass, SUPERVISION__PROJECT_TITLE);
		createEReference(supervisionEClass, SUPERVISION__SUPERVISORS);
		createEReference(supervisionEClass, SUPERVISION__INSTITUTIONS);
		createEReference(supervisionEClass, SUPERVISION__MAIN_INSTITUTION);
		createEReference(supervisionEClass, SUPERVISION__MAIN_SUPERVISOR);
		createEAttribute(supervisionEClass, SUPERVISION__COMPLETED);
		createEAttribute(supervisionEClass, SUPERVISION__FINAL_WORK_URL);
		createEAttribute(supervisionEClass, SUPERVISION__FINAL_WORK_DOI);

		institutionRegistryEClass = createEClass(INSTITUTION_REGISTRY);
		createEReference(institutionRegistryEClass, INSTITUTION_REGISTRY__INSTITUTIONS);

		institutionEClass = createEClass(INSTITUTION);
		createEAttribute(institutionEClass, INSTITUTION__NAME);
		createEReference(institutionEClass, INSTITUTION__UNITS);
		createEAttribute(institutionEClass, INSTITUTION__COUNTRY);
		createEReference(institutionEClass, INSTITUTION__FULL_NAME);

		multiLanguageStringEClass = createEClass(MULTI_LANGUAGE_STRING);
		createEAttribute(multiLanguageStringEClass, MULTI_LANGUAGE_STRING__TEXT);
		createEReference(multiLanguageStringEClass, MULTI_LANGUAGE_STRING__LOCALIZATIONS);
		createEAttribute(multiLanguageStringEClass, MULTI_LANGUAGE_STRING__BASE_LANGUAGE);

		localizedStringEClass = createEClass(LOCALIZED_STRING);
		createEAttribute(localizedStringEClass, LOCALIZED_STRING__TEXT);
		createEAttribute(localizedStringEClass, LOCALIZED_STRING__LANGUAGE);

		supervisionsRegistryEClass = createEClass(SUPERVISIONS_REGISTRY);
		createEReference(supervisionsRegistryEClass, SUPERVISIONS_REGISTRY__SUPERVISIONS);

		// Create enums
		contactKindEEnum = createEEnum(CONTACT_KIND);
		serialNumberKindEEnum = createEEnum(SERIAL_NUMBER_KIND);
		courseKindEEnum = createEEnum(COURSE_KIND);
		supervisionKindEEnum = createEEnum(SUPERVISION_KIND);
		participationKindEEnum = createEEnum(PARTICIPATION_KIND);

		// Create data types
		currencyEDataType = createEDataType(CURRENCY);
		languageCodeEDataType = createEDataType(LANGUAGE_CODE);
		countryCodeEDataType = createEDataType(COUNTRY_CODE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		researcherEClass.getESuperTypes().add(this.getPerson());
		researcherEClass.getESuperTypes().add(this.getRootElement());
		addressEClass.getESuperTypes().add(this.getContact());
		emailEClass.getESuperTypes().add(this.getContact());
		phoneEClass.getESuperTypes().add(this.getContact());
		websiteEClass.getESuperTypes().add(this.getContact());
		journalEClass.getESuperTypes().add(this.getPublication());
		conferenceEClass.getESuperTypes().add(this.getInProceedings());
		workshopEClass.getESuperTypes().add(this.getInProceedings());
		inProceedingsEClass.getESuperTypes().add(this.getInCollection());
		inCollectionEClass.getESuperTypes().add(this.getPublication());
		bookEClass.getESuperTypes().add(this.getPublication());
		bookChapterEClass.getESuperTypes().add(this.getInCollection());
		reportEClass.getESuperTypes().add(this.getPublication());
		miscellaneousEClass.getESuperTypes().add(this.getPublication());
		projectEClass.getESuperTypes().add(this.getGrant());
		personRegistryEClass.getESuperTypes().add(this.getRootElement());
		institutionRegistryEClass.getESuperTypes().add(this.getRootElement());
		supervisionsRegistryEClass.getESuperTypes().add(this.getRootElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(personEClass, Person.class, "Person", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPerson_FirstNames(), ecorePackage.getEString(), "firstNames", null, 1, -1, Person.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPerson_LastNames(), ecorePackage.getEString(), "lastNames", null, 1, -1, Person.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPerson_Name(), ecorePackage.getEString(), "name", null, 1, 1, Person.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(researcherEClass, Researcher.class, "Researcher", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getResearcher_Coauthors(), this.getPerson(), null, "coauthors", null, 0, -1, Researcher.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED);
		initEReference(getResearcher_Contacts(), this.getContact(), null, "contacts", null, 0, -1, Researcher.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getResearcher_Socials(), this.getSocialProfile(), null, "socials", null, 0, -1, Researcher.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getResearcher_Publications(), this.getPublication(), null, "publications", null, 0, -1, Researcher.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getResearcher_TeachingMaterial(), this.getPublication(), null, "teachingMaterial", null, 0, -1, Researcher.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getResearcher_OtherMaterial(), this.getPublication(), null, "otherMaterial", null, 0, -1, Researcher.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getResearcher_ResearchGroup(), this.getResearchGroup(), null, "researchGroup", null, 0, 1, Researcher.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getResearcher_Topics(), this.getResearchTopic(), null, "topics", null, 0, -1, Researcher.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getResearcher_Grants(), this.getGrant(), null, "grants", null, 0, -1, Researcher.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getResearcher_Courses(), this.getCourse(), null, "courses", null, 0, -1, Researcher.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getResearcher_TaughtCourses(), this.getTaughtCourse(), null, "taughtCourses", null, 0, -1, Researcher.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getResearcher_SupervisionsRegistry(), this.getSupervisionsRegistry(), null, "supervisionsRegistry", null, 0, 1, Researcher.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getResearcher_Supervisions(), this.getSupervision(), null, "supervisions", null, 0, -1, Researcher.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(addressEClass, Address.class, "Address", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAddress_Lines(), ecorePackage.getEString(), "lines", null, 1, -1, Address.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAddress_Zip(), ecorePackage.getEString(), "zip", null, 1, 1, Address.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAddress_City(), ecorePackage.getEString(), "city", null, 1, 1, Address.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAddress_Region(), ecorePackage.getEString(), "region", null, 0, 1, Address.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAddress_Country(), ecorePackage.getEString(), "country", null, 1, 1, Address.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAddress_Building(), ecorePackage.getEString(), "building", null, 0, 1, Address.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAddress_Room(), ecorePackage.getEString(), "room", null, 0, 1, Address.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(contactEClass, Contact.class, "Contact", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getContact_Kind(), this.getContactKind(), "kind", "PRIVATE", 1, 1, Contact.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(emailEClass, Email.class, "Email", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEmail_Address(), ecorePackage.getEString(), "address", null, 1, 1, Email.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(phoneEClass, Phone.class, "Phone", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPhone_Number(), ecorePackage.getEString(), "number", null, 1, 1, Phone.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(socialProfileEClass, SocialProfile.class, "SocialProfile", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSocialProfile_Name(), ecorePackage.getEString(), "name", null, 1, 1, SocialProfile.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSocialProfile_Url(), ecorePackage.getEString(), "url", null, 1, 1, SocialProfile.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(websiteEClass, Website.class, "Website", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getWebsite_Url(), ecorePackage.getEString(), "url", null, 1, 1, Website.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(publicationEClass, Publication.class, "Publication", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPublication_Citekey(), ecorePackage.getEString(), "citekey", null, 1, 1, Publication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPublication_Title(), ecorePackage.getEString(), "title", null, 1, 1, Publication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPublication_Authors(), this.getPerson(), null, "authors", null, 1, -1, Publication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPublication_URL(), ecorePackage.getEString(), "URL", null, 0, 1, Publication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPublication_Published(), ecorePackage.getEBoolean(), "published", "true", 1, 1, Publication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPublication_OpenAccess(), ecorePackage.getEBoolean(), "openAccess", "false", 1, 1, Publication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPublication_Date(), this.getPublishedDate(), null, "date", null, 1, 1, Publication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPublication_DOI(), ecorePackage.getEString(), "DOI", null, 0, 1, Publication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPublication_Abstract(), ecorePackage.getEString(), "abstract", null, 0, 1, Publication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPublication_WithAuthorVersion(), ecorePackage.getEBoolean(), "withAuthorVersion", "true", 1, 1, Publication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPublication_Notes(), ecorePackage.getEString(), "notes", null, 0, -1, Publication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPublication_RelatedProjects(), this.getGrant(), this.getGrant_RelatedPublications(), "relatedProjects", null, 0, -1, Publication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(journalEClass, Journal.class, "Journal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getJournal_JournalName(), ecorePackage.getEString(), "journalName", null, 1, 1, Journal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getJournal_Publisher(), ecorePackage.getEString(), "publisher", null, 0, 1, Journal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getJournal_FirstPage(), ecorePackage.getEString(), "firstPage", null, 0, 1, Journal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getJournal_LastPage(), ecorePackage.getEString(), "lastPage", null, 0, 1, Journal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getJournal_Volume(), ecorePackage.getEInt(), "volume", null, 1, 1, Journal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getJournal_Issue(), ecorePackage.getEInt(), "issue", null, 1, 1, Journal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getJournal_ISSN(), this.getSerialNumber(), null, "ISSN", null, 0, -1, Journal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getJournal_Magazine(), ecorePackage.getEBoolean(), "magazine", "false", 1, 1, Journal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(conferenceEClass, Conference.class, "Conference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(workshopEClass, Workshop.class, "Workshop", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getWorkshop_MainEventName(), ecorePackage.getEString(), "mainEventName", null, 0, 1, Workshop.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getWorkshop_MainEventShortName(), ecorePackage.getEString(), "mainEventShortName", null, 0, 1, Workshop.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(inProceedingsEClass, InProceedings.class, "InProceedings", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInProceedings_EventEndDate(), this.getPublishedDate(), null, "eventEndDate", null, 0, 1, InProceedings.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInProceedings_EventName(), ecorePackage.getEString(), "eventName", null, 1, 1, InProceedings.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInProceedings_Venue(), ecorePackage.getEString(), "venue", null, 0, 1, InProceedings.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInProceedings_EventShortName(), ecorePackage.getEString(), "eventShortName", null, 1, 1, InProceedings.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInProceedings_EventStartDate(), this.getPublishedDate(), null, "eventStartDate", null, 1, 1, InProceedings.class, !IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getInProceedings_TrackName(), ecorePackage.getEString(), "trackName", null, 0, 1, InProceedings.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInProceedings_TrackShortName(), ecorePackage.getEString(), "trackShortName", null, 0, 1, InProceedings.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInProceedings_ShortPaper(), ecorePackage.getEBoolean(), "shortPaper", "false", 1, 1, InProceedings.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInProceedings_ToolPaper(), ecorePackage.getEBoolean(), "toolPaper", "false", 1, 1, InProceedings.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInProceedings_InvitedPaper(), ecorePackage.getEBoolean(), "invitedPaper", "false", 1, 1, InProceedings.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(inCollectionEClass, InCollection.class, "InCollection", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getInCollection_Booktitle(), ecorePackage.getEString(), "booktitle", null, 1, 1, InCollection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInCollection_Bookeditors(), this.getPerson(), null, "bookeditors", null, 0, -1, InCollection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInCollection_Publisher(), ecorePackage.getEString(), "publisher", null, 0, 1, InCollection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInCollection_Series(), ecorePackage.getEString(), "series", null, 0, 1, InCollection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInCollection_Volume(), ecorePackage.getEString(), "volume", null, 0, 1, InCollection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInCollection_FirstPage(), ecorePackage.getEString(), "firstPage", null, 0, 1, InCollection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInCollection_LastPage(), ecorePackage.getEString(), "lastPage", null, 0, 1, InCollection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInCollection_ISBN(), this.getSerialNumber(), null, "ISBN", null, 0, -1, InCollection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bookEClass, Book.class, "Book", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBook_Publisher(), ecorePackage.getEString(), "publisher", null, 0, 1, Book.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBook_Series(), ecorePackage.getEString(), "series", null, 0, 1, Book.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBook_Volume(), ecorePackage.getEString(), "volume", null, 0, 1, Book.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBook_ISBN(), this.getSerialNumber(), null, "ISBN", null, 0, -1, Book.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bookChapterEClass, BookChapter.class, "BookChapter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBookChapter_ChapterNumber(), ecorePackage.getEString(), "chapterNumber", null, 0, 1, BookChapter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(reportEClass, Report.class, "Report", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getReport_ReportNumber(), ecorePackage.getEString(), "reportNumber", null, 0, 1, Report.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getReport_Institution(), ecorePackage.getEString(), "institution", null, 0, 1, Report.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(miscellaneousEClass, Miscellaneous.class, "Miscellaneous", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(publishedDateEClass, PublishedDate.class, "PublishedDate", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPublishedDate_Day(), ecorePackage.getEInt(), "day", "1", 1, 1, PublishedDate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPublishedDate_Month(), ecorePackage.getEInt(), "month", "1", 1, 1, PublishedDate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPublishedDate_Year(), ecorePackage.getEInt(), "year", null, 1, 1, PublishedDate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPublishedDate_MonthName(), ecorePackage.getEString(), "monthName", null, 0, 1, PublishedDate.class, IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getPublishedDate_MonthShortName(), ecorePackage.getEString(), "monthShortName", null, 0, 1, PublishedDate.class, IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(serialNumberEClass, SerialNumber.class, "SerialNumber", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSerialNumber_Value(), ecorePackage.getEString(), "value", null, 1, 1, SerialNumber.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSerialNumber_Kind(), this.getSerialNumberKind(), "kind", "UNSPECIFIED", 1, 1, SerialNumber.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(researchGroupEClass, ResearchGroup.class, "ResearchGroup", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getResearchGroup_Name(), ecorePackage.getEString(), "name", null, 1, 1, ResearchGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getResearchGroup_Acronym(), ecorePackage.getEString(), "acronym", null, 1, 1, ResearchGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getResearchGroup_URL(), ecorePackage.getEString(), "URL", null, 0, 1, ResearchGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(courseEClass, Course.class, "Course", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCourse_Code(), ecorePackage.getEString(), "code", null, 1, 1, Course.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCourse_Name(), ecorePackage.getEString(), "name", null, 1, 1, Course.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCourse_Level(), this.getCourseKind(), "level", null, 0, 1, Course.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCourse_Hours(), ecorePackage.getEInt(), "hours", "60", 1, 1, Course.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCourse_Credits(), ecorePackage.getEFloat(), "credits", "4", 1, 1, Course.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(taughtCourseEClass, TaughtCourse.class, "TaughtCourse", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTaughtCourse_Year(), ecorePackage.getEInt(), "year", null, 1, 1, TaughtCourse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTaughtCourse_Period(), ecorePackage.getEInt(), "period", null, 1, 1, TaughtCourse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTaughtCourse_Webinfo(), ecorePackage.getEString(), "webinfo", null, 0, 1, TaughtCourse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTaughtCourse_Website(), ecorePackage.getEString(), "website", null, 0, 1, TaughtCourse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTaughtCourse_Code(), ecorePackage.getEString(), "code", null, 1, 1, TaughtCourse.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getTaughtCourse_Name(), ecorePackage.getEString(), "name", null, 1, 1, TaughtCourse.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getTaughtCourse_Level(), this.getCourseKind(), "level", null, 1, 1, TaughtCourse.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getTaughtCourse_Language(), this.getLanguageCode(), "language", "EN", 1, 1, TaughtCourse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTaughtCourse_Canceled(), ecorePackage.getEBoolean(), "canceled", "false", 1, 1, TaughtCourse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTaughtCourse_Responsibility(), ecorePackage.getEDouble(), "responsibility", "1.0", 1, 1, TaughtCourse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTaughtCourse_Offering(), this.getCourseOffering(), null, "offering", null, 1, -1, TaughtCourse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(researchTopicEClass, ResearchTopic.class, "ResearchTopic", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getResearchTopic_Title(), ecorePackage.getEString(), "title", null, 1, 1, ResearchTopic.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getResearchTopic_Description(), ecorePackage.getEString(), "description", null, 0, 1, ResearchTopic.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getResearchTopic_RelatedPapers(), this.getPublication(), null, "relatedPapers", null, 0, -1, ResearchTopic.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(projectEClass, Project.class, "Project", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getProject_Acronym(), ecorePackage.getEString(), "acronym", null, 0, 1, Project.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProject_Title(), ecorePackage.getEString(), "title", null, 0, 1, Project.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getProject_ShortName(), ecorePackage.getEString(), "shortName", null, 1, 1, Project.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(grantEClass, Grant.class, "Grant", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGrant_RelatedPublications(), this.getPublication(), this.getPublication_RelatedProjects(), "relatedPublications", null, 0, -1, Grant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGrant_URL(), ecorePackage.getEString(), "URL", null, 0, -1, Grant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGrant_Agency(), ecorePackage.getEString(), "agency", null, 1, 1, Grant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGrant_Program(), ecorePackage.getEString(), "program", null, 0, 1, Grant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGrant_Call(), ecorePackage.getEString(), "call", null, 0, 1, Grant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGrant_Number(), ecorePackage.getEString(), "number", null, 1, 1, Grant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGrant_Budget(), this.getBudget(), null, "budget", null, 0, -1, Grant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGrant_StartDate(), this.getPublishedDate(), null, "startDate", null, 1, 1, Grant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGrant_EndDate(), this.getPublishedDate(), null, "endDate", null, 1, 1, Grant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGrant_GrantID(), ecorePackage.getEString(), "grantID", null, 1, 1, Grant.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getGrant_Role(), this.getParticipationKind(), "role", null, 1, 1, Grant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getGrant__IsActive(), ecorePackage.getEBoolean(), "isActive", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(budgetEClass, Budget.class, "Budget", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBudget_Amount(), ecorePackage.getEDouble(), "amount", null, 1, 1, Budget.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBudget_Currency(), this.getCurrency(), "currency", null, 1, 1, Budget.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(rootElementEClass, RootElement.class, "RootElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(personRegistryEClass, PersonRegistry.class, "PersonRegistry", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPersonRegistry_Person(), this.getPerson(), null, "person", null, 1, -1, PersonRegistry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(courseOfferingEClass, CourseOffering.class, "CourseOffering", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCourseOffering_Groups(), ecorePackage.getEString(), "groups", "", 0, -1, CourseOffering.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCourseOffering_SpecificName(), ecorePackage.getEString(), "specificName", null, 0, 1, CourseOffering.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCourseOffering_Course(), this.getCourse(), null, "course", null, 1, 1, CourseOffering.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(supervisionEClass, Supervision.class, "Supervision", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSupervision_DasteStart(), this.getPublishedDate(), null, "dasteStart", null, 0, 1, Supervision.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSupervision_DateEnd(), this.getPublishedDate(), null, "dateEnd", null, 0, 1, Supervision.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSupervision_Level(), this.getSupervisionKind(), "level", "MASTER", 1, 1, Supervision.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSupervision_Student(), this.getPerson(), null, "student", null, 1, 1, Supervision.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSupervision_ProjectTitle(), ecorePackage.getEString(), "projectTitle", null, 0, 1, Supervision.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSupervision_Supervisors(), this.getPerson(), null, "supervisors", null, 1, -1, Supervision.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSupervision_Institutions(), this.getInstitution(), null, "institutions", null, 1, -1, Supervision.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSupervision_MainInstitution(), this.getInstitution(), null, "mainInstitution", null, 1, 1, Supervision.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getSupervision_MainSupervisor(), this.getPerson(), null, "mainSupervisor", null, 1, 1, Supervision.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getSupervision_Completed(), ecorePackage.getEBoolean(), "completed", null, 1, 1, Supervision.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getSupervision_FinalWorkURL(), ecorePackage.getEString(), "finalWorkURL", null, 0, 1, Supervision.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSupervision_FinalWorkDOI(), ecorePackage.getEString(), "finalWorkDOI", null, 0, 1, Supervision.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(institutionRegistryEClass, InstitutionRegistry.class, "InstitutionRegistry", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInstitutionRegistry_Institutions(), this.getInstitution(), null, "institutions", null, 1, -1, InstitutionRegistry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(institutionEClass, Institution.class, "Institution", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getInstitution_Name(), ecorePackage.getEString(), "name", null, 1, 1, Institution.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInstitution_Units(), this.getInstitution(), null, "units", null, 0, -1, Institution.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInstitution_Country(), this.getCountryCode(), "country", null, 0, 1, Institution.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInstitution_FullName(), this.getMultiLanguageString(), null, "fullName", null, 1, 1, Institution.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(multiLanguageStringEClass, MultiLanguageString.class, "MultiLanguageString", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMultiLanguageString_Text(), ecorePackage.getEString(), "text", null, 1, 1, MultiLanguageString.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMultiLanguageString_Localizations(), this.getLocalizedString(), null, "localizations", null, 1, -1, MultiLanguageString.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMultiLanguageString_BaseLanguage(), this.getLanguageCode(), "baseLanguage", null, 1, 1, MultiLanguageString.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(localizedStringEClass, LocalizedString.class, "LocalizedString", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLocalizedString_Text(), ecorePackage.getEString(), "text", null, 1, 1, LocalizedString.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLocalizedString_Language(), this.getLanguageCode(), "language", null, 1, 1, LocalizedString.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(supervisionsRegistryEClass, SupervisionsRegistry.class, "SupervisionsRegistry", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSupervisionsRegistry_Supervisions(), this.getSupervision(), null, "supervisions", null, 0, -1, SupervisionsRegistry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(contactKindEEnum, ContactKind.class, "ContactKind");
		addEEnumLiteral(contactKindEEnum, ContactKind.PRIVATE);
		addEEnumLiteral(contactKindEEnum, ContactKind.WORK);

		initEEnum(serialNumberKindEEnum, SerialNumberKind.class, "SerialNumberKind");
		addEEnumLiteral(serialNumberKindEEnum, SerialNumberKind.UNSPECIFIED);
		addEEnumLiteral(serialNumberKindEEnum, SerialNumberKind.PRINT);
		addEEnumLiteral(serialNumberKindEEnum, SerialNumberKind.ELECTRONIC);
		addEEnumLiteral(serialNumberKindEEnum, SerialNumberKind.CDROM);
		addEEnumLiteral(serialNumberKindEEnum, SerialNumberKind.ONDEMAND);
		addEEnumLiteral(serialNumberKindEEnum, SerialNumberKind.USB);

		initEEnum(courseKindEEnum, CourseKind.class, "CourseKind");
		addEEnumLiteral(courseKindEEnum, CourseKind.UNDERGRADUATE);
		addEEnumLiteral(courseKindEEnum, CourseKind.GRADUATE);
		addEEnumLiteral(courseKindEEnum, CourseKind.CONTINUING_EDUCATION);

		initEEnum(supervisionKindEEnum, SupervisionKind.class, "SupervisionKind");
		addEEnumLiteral(supervisionKindEEnum, SupervisionKind.BACHELOR);
		addEEnumLiteral(supervisionKindEEnum, SupervisionKind.MASTER);
		addEEnumLiteral(supervisionKindEEnum, SupervisionKind.PHD);
		addEEnumLiteral(supervisionKindEEnum, SupervisionKind.PREMASTER);

		initEEnum(participationKindEEnum, ParticipationKind.class, "ParticipationKind");
		addEEnumLiteral(participationKindEEnum, ParticipationKind.PARTICIPANT);
		addEEnumLiteral(participationKindEEnum, ParticipationKind.PARTNER_PI);
		addEEnumLiteral(participationKindEEnum, ParticipationKind.PROJECT_PI);

		// Initialize data types
		initEDataType(currencyEDataType, Currency.class, "Currency", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(languageCodeEDataType, Locale.class, "LanguageCode", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(countryCodeEDataType, Locale.class, "CountryCode", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/OCL/Import
		createImportAnnotations();
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
		// http://www.eclipse.org/emf/2002/Ecore/OCL
		createOCLAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/OCL/Import</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createImportAnnotations() {
		String source = "http://www.eclipse.org/OCL/Import";
		addAnnotation
		  (this,
		   source,
		   new String[] {
			   "ecore", "http://www.eclipse.org/emf/2002/Ecore"
		   });
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";
		addAnnotation
		  (this,
		   source,
		   new String[] {
			   "invocationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL",
			   "settingDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL",
			   "validationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL"
		   });
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore/OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore/OCL";
		addAnnotation
		  (getPerson_Name(),
		   source,
		   new String[] {
			   "derivation", "lastNames->last() + \', \' + firstNames->first()"
		   });
		addAnnotation
		  (getResearcher_Coauthors(),
		   source,
		   new String[] {
			   "derivation", "self.publications->collect(authors)->asSet()->excluding(self)"
		   });
		addAnnotation
		  (getResearcher_Supervisions(),
		   source,
		   new String[] {
			   "derivation", "supervisionsRegistry.supervisions"
		   });
		addAnnotation
		  (getInProceedings_EventStartDate(),
		   source,
		   new String[] {
			   "derivation", "date"
		   });
		addAnnotation
		  (getTaughtCourse_Code(),
		   source,
		   new String[] {
			   "derivation", "\n            if not offering->isEmpty() then offering->first().course.code else null endif"
		   });
		addAnnotation
		  (getTaughtCourse_Name(),
		   source,
		   new String[] {
			   "derivation", "\n            if not offering->isEmpty() then offering->first().course.name else null endif"
		   });
		addAnnotation
		  (getTaughtCourse_Level(),
		   source,
		   new String[] {
			   "derivation", "\n            if not offering->isEmpty() then offering->first().course.level else null endif"
		   });
		addAnnotation
		  (getProject_ShortName(),
		   source,
		   new String[] {
			   "derivation", "if acronym.oclIsUndefined() then self.grantID else acronym endif"
		   });
		addAnnotation
		  (getGrant_GrantID(),
		   source,
		   new String[] {
			   "derivation", "agency + \n                        if program.oclIsUndefined() then \'\' else \'-\' + program endif +\n                        if call.oclIsUndefined() then \'\' else \'-\' + call endif + \n                        \'-\' + number"
		   });
		addAnnotation
		  (getSupervision_MainInstitution(),
		   source,
		   new String[] {
			   "derivation", "institutions->first()"
		   });
		addAnnotation
		  (getSupervision_MainSupervisor(),
		   source,
		   new String[] {
			   "derivation", "supervisors->first()"
		   });
		addAnnotation
		  (getSupervision_Completed(),
		   source,
		   new String[] {
			   "derivation", "if dateEnd.oclIsUndefined() then false else not dateEnd.oclIsInvalid() endif"
		   });
		addAnnotation
		  (getMultiLanguageString_Text(),
		   source,
		   new String[] {
			   "derivation", "\n                                                 let first : LocalizedString = localizations->first() in\n                                            \n                                                if(first.oclIsInvalid()) then \n                                                    \'???\'\n                                                else first.text\n                                                endif"
		   });
		addAnnotation
		  (getMultiLanguageString_BaseLanguage(),
		   source,
		   new String[] {
			   "derivation", " localizations->first().language"
		   });
	}

} //ResearchCVPackageImpl
