/**
 */
package org.montex.researchcv.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.eclipse.emf.ecore.util.InternalEList;
import org.montex.researchcv.InCollection;
import org.montex.researchcv.Person;
import org.montex.researchcv.ResearchCVPackage;
import org.montex.researchcv.SerialNumber;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>In Collection</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.impl.InCollectionImpl#getBooktitle <em>Booktitle</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.InCollectionImpl#getBookeditors <em>Bookeditors</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.InCollectionImpl#getPublisher <em>Publisher</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.InCollectionImpl#getSeries <em>Series</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.InCollectionImpl#getVolume <em>Volume</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.InCollectionImpl#getFirstPage <em>First Page</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.InCollectionImpl#getLastPage <em>Last Page</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.InCollectionImpl#getISBN <em>ISBN</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class InCollectionImpl extends PublicationImpl implements InCollection {
	/**
	 * The default value of the '{@link #getBooktitle() <em>Booktitle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBooktitle()
	 * @generated
	 * @ordered
	 */
	protected static final String BOOKTITLE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBooktitle() <em>Booktitle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBooktitle()
	 * @generated
	 * @ordered
	 */
	protected String booktitle = BOOKTITLE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getBookeditors() <em>Bookeditors</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookeditors()
	 * @generated
	 * @ordered
	 */
	protected EList<Person> bookeditors;

	/**
	 * The default value of the '{@link #getPublisher() <em>Publisher</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPublisher()
	 * @generated
	 * @ordered
	 */
	protected static final String PUBLISHER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPublisher() <em>Publisher</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPublisher()
	 * @generated
	 * @ordered
	 */
	protected String publisher = PUBLISHER_EDEFAULT;

	/**
	 * The default value of the '{@link #getSeries() <em>Series</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSeries()
	 * @generated
	 * @ordered
	 */
	protected static final String SERIES_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSeries() <em>Series</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSeries()
	 * @generated
	 * @ordered
	 */
	protected String series = SERIES_EDEFAULT;

	/**
	 * The default value of the '{@link #getVolume() <em>Volume</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVolume()
	 * @generated
	 * @ordered
	 */
	protected static final String VOLUME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVolume() <em>Volume</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVolume()
	 * @generated
	 * @ordered
	 */
	protected String volume = VOLUME_EDEFAULT;

	/**
	 * The default value of the '{@link #getFirstPage() <em>First Page</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirstPage()
	 * @generated
	 * @ordered
	 */
	protected static final String FIRST_PAGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFirstPage() <em>First Page</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirstPage()
	 * @generated
	 * @ordered
	 */
	protected String firstPage = FIRST_PAGE_EDEFAULT;

	/**
	 * The default value of the '{@link #getLastPage() <em>Last Page</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastPage()
	 * @generated
	 * @ordered
	 */
	protected static final String LAST_PAGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLastPage() <em>Last Page</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastPage()
	 * @generated
	 * @ordered
	 */
	protected String lastPage = LAST_PAGE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getISBN() <em>ISBN</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getISBN()
	 * @generated
	 * @ordered
	 */
	protected EList<SerialNumber> isbn;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InCollectionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ResearchCVPackage.Literals.IN_COLLECTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getBooktitle() {
		return booktitle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBooktitle(String newBooktitle) {
		String oldBooktitle = booktitle;
		booktitle = newBooktitle;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.IN_COLLECTION__BOOKTITLE, oldBooktitle, booktitle));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Person> getBookeditors() {
		if (bookeditors == null) {
			bookeditors = new EObjectResolvingEList<Person>(Person.class, this, ResearchCVPackage.IN_COLLECTION__BOOKEDITORS);
		}
		return bookeditors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getPublisher() {
		return publisher;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPublisher(String newPublisher) {
		String oldPublisher = publisher;
		publisher = newPublisher;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.IN_COLLECTION__PUBLISHER, oldPublisher, publisher));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSeries() {
		return series;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSeries(String newSeries) {
		String oldSeries = series;
		series = newSeries;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.IN_COLLECTION__SERIES, oldSeries, series));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getVolume() {
		return volume;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVolume(String newVolume) {
		String oldVolume = volume;
		volume = newVolume;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.IN_COLLECTION__VOLUME, oldVolume, volume));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SerialNumber> getISBN() {
		if (isbn == null) {
			isbn = new EObjectContainmentEList<SerialNumber>(SerialNumber.class, this, ResearchCVPackage.IN_COLLECTION__ISBN);
		}
		return isbn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ResearchCVPackage.IN_COLLECTION__ISBN:
				return ((InternalEList<?>)getISBN()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getFirstPage() {
		return firstPage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFirstPage(String newFirstPage) {
		String oldFirstPage = firstPage;
		firstPage = newFirstPage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.IN_COLLECTION__FIRST_PAGE, oldFirstPage, firstPage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLastPage() {
		return lastPage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLastPage(String newLastPage) {
		String oldLastPage = lastPage;
		lastPage = newLastPage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.IN_COLLECTION__LAST_PAGE, oldLastPage, lastPage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ResearchCVPackage.IN_COLLECTION__BOOKTITLE:
				return getBooktitle();
			case ResearchCVPackage.IN_COLLECTION__BOOKEDITORS:
				return getBookeditors();
			case ResearchCVPackage.IN_COLLECTION__PUBLISHER:
				return getPublisher();
			case ResearchCVPackage.IN_COLLECTION__SERIES:
				return getSeries();
			case ResearchCVPackage.IN_COLLECTION__VOLUME:
				return getVolume();
			case ResearchCVPackage.IN_COLLECTION__FIRST_PAGE:
				return getFirstPage();
			case ResearchCVPackage.IN_COLLECTION__LAST_PAGE:
				return getLastPage();
			case ResearchCVPackage.IN_COLLECTION__ISBN:
				return getISBN();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ResearchCVPackage.IN_COLLECTION__BOOKTITLE:
				setBooktitle((String)newValue);
				return;
			case ResearchCVPackage.IN_COLLECTION__BOOKEDITORS:
				getBookeditors().clear();
				getBookeditors().addAll((Collection<? extends Person>)newValue);
				return;
			case ResearchCVPackage.IN_COLLECTION__PUBLISHER:
				setPublisher((String)newValue);
				return;
			case ResearchCVPackage.IN_COLLECTION__SERIES:
				setSeries((String)newValue);
				return;
			case ResearchCVPackage.IN_COLLECTION__VOLUME:
				setVolume((String)newValue);
				return;
			case ResearchCVPackage.IN_COLLECTION__FIRST_PAGE:
				setFirstPage((String)newValue);
				return;
			case ResearchCVPackage.IN_COLLECTION__LAST_PAGE:
				setLastPage((String)newValue);
				return;
			case ResearchCVPackage.IN_COLLECTION__ISBN:
				getISBN().clear();
				getISBN().addAll((Collection<? extends SerialNumber>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.IN_COLLECTION__BOOKTITLE:
				setBooktitle(BOOKTITLE_EDEFAULT);
				return;
			case ResearchCVPackage.IN_COLLECTION__BOOKEDITORS:
				getBookeditors().clear();
				return;
			case ResearchCVPackage.IN_COLLECTION__PUBLISHER:
				setPublisher(PUBLISHER_EDEFAULT);
				return;
			case ResearchCVPackage.IN_COLLECTION__SERIES:
				setSeries(SERIES_EDEFAULT);
				return;
			case ResearchCVPackage.IN_COLLECTION__VOLUME:
				setVolume(VOLUME_EDEFAULT);
				return;
			case ResearchCVPackage.IN_COLLECTION__FIRST_PAGE:
				setFirstPage(FIRST_PAGE_EDEFAULT);
				return;
			case ResearchCVPackage.IN_COLLECTION__LAST_PAGE:
				setLastPage(LAST_PAGE_EDEFAULT);
				return;
			case ResearchCVPackage.IN_COLLECTION__ISBN:
				getISBN().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.IN_COLLECTION__BOOKTITLE:
				return BOOKTITLE_EDEFAULT == null ? booktitle != null : !BOOKTITLE_EDEFAULT.equals(booktitle);
			case ResearchCVPackage.IN_COLLECTION__BOOKEDITORS:
				return bookeditors != null && !bookeditors.isEmpty();
			case ResearchCVPackage.IN_COLLECTION__PUBLISHER:
				return PUBLISHER_EDEFAULT == null ? publisher != null : !PUBLISHER_EDEFAULT.equals(publisher);
			case ResearchCVPackage.IN_COLLECTION__SERIES:
				return SERIES_EDEFAULT == null ? series != null : !SERIES_EDEFAULT.equals(series);
			case ResearchCVPackage.IN_COLLECTION__VOLUME:
				return VOLUME_EDEFAULT == null ? volume != null : !VOLUME_EDEFAULT.equals(volume);
			case ResearchCVPackage.IN_COLLECTION__FIRST_PAGE:
				return FIRST_PAGE_EDEFAULT == null ? firstPage != null : !FIRST_PAGE_EDEFAULT.equals(firstPage);
			case ResearchCVPackage.IN_COLLECTION__LAST_PAGE:
				return LAST_PAGE_EDEFAULT == null ? lastPage != null : !LAST_PAGE_EDEFAULT.equals(lastPage);
			case ResearchCVPackage.IN_COLLECTION__ISBN:
				return isbn != null && !isbn.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (booktitle: ");
		result.append(booktitle);
		result.append(", publisher: ");
		result.append(publisher);
		result.append(", series: ");
		result.append(series);
		result.append(", volume: ");
		result.append(volume);
		result.append(", firstPage: ");
		result.append(firstPage);
		result.append(", lastPage: ");
		result.append(lastPage);
		result.append(')');
		return result.toString();
	}

} //InCollectionImpl
