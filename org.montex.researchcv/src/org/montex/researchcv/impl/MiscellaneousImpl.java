/**
 */
package org.montex.researchcv.impl;

import org.eclipse.emf.ecore.EClass;
import org.montex.researchcv.Miscellaneous;
import org.montex.researchcv.ResearchCVPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Miscellaneous</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class MiscellaneousImpl extends PublicationImpl implements Miscellaneous {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MiscellaneousImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ResearchCVPackage.Literals.MISCELLANEOUS;
	}

} //MiscellaneousImpl
