/**
 */
package org.montex.researchcv.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.montex.researchcv.Institution;
import org.montex.researchcv.Person;
import org.montex.researchcv.PublishedDate;
import org.montex.researchcv.ResearchCVPackage;
import org.montex.researchcv.Supervision;
import org.montex.researchcv.SupervisionKind;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Supervision</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.impl.SupervisionImpl#getDasteStart <em>Daste Start</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.SupervisionImpl#getDateEnd <em>Date End</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.SupervisionImpl#getLevel <em>Level</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.SupervisionImpl#getStudent <em>Student</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.SupervisionImpl#getProjectTitle <em>Project Title</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.SupervisionImpl#getSupervisors <em>Supervisors</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.SupervisionImpl#getInstitutions <em>Institutions</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.SupervisionImpl#getMainInstitution <em>Main Institution</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.SupervisionImpl#getMainSupervisor <em>Main Supervisor</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.SupervisionImpl#isCompleted <em>Completed</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.SupervisionImpl#getFinalWorkURL <em>Final Work URL</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.SupervisionImpl#getFinalWorkDOI <em>Final Work DOI</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SupervisionImpl extends MinimalEObjectImpl.Container implements Supervision {
	/**
	 * The cached value of the '{@link #getDasteStart() <em>Daste Start</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDasteStart()
	 * @generated
	 * @ordered
	 */
	protected PublishedDate dasteStart;

	/**
	 * The cached value of the '{@link #getDateEnd() <em>Date End</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDateEnd()
	 * @generated
	 * @ordered
	 */
	protected PublishedDate dateEnd;

	/**
	 * The default value of the '{@link #getLevel() <em>Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLevel()
	 * @generated
	 * @ordered
	 */
	protected static final SupervisionKind LEVEL_EDEFAULT = SupervisionKind.MASTER;

	/**
	 * The cached value of the '{@link #getLevel() <em>Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLevel()
	 * @generated
	 * @ordered
	 */
	protected SupervisionKind level = LEVEL_EDEFAULT;

	/**
	 * The cached value of the '{@link #getStudent() <em>Student</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStudent()
	 * @generated
	 * @ordered
	 */
	protected Person student;

	/**
	 * The default value of the '{@link #getProjectTitle() <em>Project Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProjectTitle()
	 * @generated
	 * @ordered
	 */
	protected static final String PROJECT_TITLE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getProjectTitle() <em>Project Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProjectTitle()
	 * @generated
	 * @ordered
	 */
	protected String projectTitle = PROJECT_TITLE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSupervisors() <em>Supervisors</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSupervisors()
	 * @generated
	 * @ordered
	 */
	protected EList<Person> supervisors;

	/**
	 * The cached value of the '{@link #getInstitutions() <em>Institutions</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstitutions()
	 * @generated
	 * @ordered
	 */
	protected EList<Institution> institutions;

	/**
	 * The cached setting delegate for the '{@link #getMainInstitution() <em>Main Institution</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMainInstitution()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature.Internal.SettingDelegate MAIN_INSTITUTION__ESETTING_DELEGATE = ((EStructuralFeature.Internal)ResearchCVPackage.Literals.SUPERVISION__MAIN_INSTITUTION).getSettingDelegate();

	/**
	 * The cached setting delegate for the '{@link #getMainSupervisor() <em>Main Supervisor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMainSupervisor()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature.Internal.SettingDelegate MAIN_SUPERVISOR__ESETTING_DELEGATE = ((EStructuralFeature.Internal)ResearchCVPackage.Literals.SUPERVISION__MAIN_SUPERVISOR).getSettingDelegate();

	/**
	 * The cached setting delegate for the '{@link #isCompleted() <em>Completed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCompleted()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature.Internal.SettingDelegate COMPLETED__ESETTING_DELEGATE = ((EStructuralFeature.Internal)ResearchCVPackage.Literals.SUPERVISION__COMPLETED).getSettingDelegate();

	/**
	 * The default value of the '{@link #getFinalWorkURL() <em>Final Work URL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinalWorkURL()
	 * @generated
	 * @ordered
	 */
	protected static final String FINAL_WORK_URL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFinalWorkURL() <em>Final Work URL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinalWorkURL()
	 * @generated
	 * @ordered
	 */
	protected String finalWorkURL = FINAL_WORK_URL_EDEFAULT;

	/**
	 * The default value of the '{@link #getFinalWorkDOI() <em>Final Work DOI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinalWorkDOI()
	 * @generated
	 * @ordered
	 */
	protected static final String FINAL_WORK_DOI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFinalWorkDOI() <em>Final Work DOI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinalWorkDOI()
	 * @generated
	 * @ordered
	 */
	protected String finalWorkDOI = FINAL_WORK_DOI_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SupervisionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ResearchCVPackage.Literals.SUPERVISION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PublishedDate getDasteStart() {
		return dasteStart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDasteStart(PublishedDate newDasteStart, NotificationChain msgs) {
		PublishedDate oldDasteStart = dasteStart;
		dasteStart = newDasteStart;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ResearchCVPackage.SUPERVISION__DASTE_START, oldDasteStart, newDasteStart);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDasteStart(PublishedDate newDasteStart) {
		if (newDasteStart != dasteStart) {
			NotificationChain msgs = null;
			if (dasteStart != null)
				msgs = ((InternalEObject)dasteStart).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ResearchCVPackage.SUPERVISION__DASTE_START, null, msgs);
			if (newDasteStart != null)
				msgs = ((InternalEObject)newDasteStart).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ResearchCVPackage.SUPERVISION__DASTE_START, null, msgs);
			msgs = basicSetDasteStart(newDasteStart, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.SUPERVISION__DASTE_START, newDasteStart, newDasteStart));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PublishedDate getDateEnd() {
		return dateEnd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDateEnd(PublishedDate newDateEnd, NotificationChain msgs) {
		PublishedDate oldDateEnd = dateEnd;
		dateEnd = newDateEnd;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ResearchCVPackage.SUPERVISION__DATE_END, oldDateEnd, newDateEnd);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDateEnd(PublishedDate newDateEnd) {
		if (newDateEnd != dateEnd) {
			NotificationChain msgs = null;
			if (dateEnd != null)
				msgs = ((InternalEObject)dateEnd).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ResearchCVPackage.SUPERVISION__DATE_END, null, msgs);
			if (newDateEnd != null)
				msgs = ((InternalEObject)newDateEnd).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ResearchCVPackage.SUPERVISION__DATE_END, null, msgs);
			msgs = basicSetDateEnd(newDateEnd, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.SUPERVISION__DATE_END, newDateEnd, newDateEnd));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SupervisionKind getLevel() {
		return level;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLevel(SupervisionKind newLevel) {
		SupervisionKind oldLevel = level;
		level = newLevel == null ? LEVEL_EDEFAULT : newLevel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.SUPERVISION__LEVEL, oldLevel, level));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Person getStudent() {
		if (student != null && student.eIsProxy()) {
			InternalEObject oldStudent = (InternalEObject)student;
			student = (Person)eResolveProxy(oldStudent);
			if (student != oldStudent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ResearchCVPackage.SUPERVISION__STUDENT, oldStudent, student));
			}
		}
		return student;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Person basicGetStudent() {
		return student;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStudent(Person newStudent) {
		Person oldStudent = student;
		student = newStudent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.SUPERVISION__STUDENT, oldStudent, student));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getProjectTitle() {
		return projectTitle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setProjectTitle(String newProjectTitle) {
		String oldProjectTitle = projectTitle;
		projectTitle = newProjectTitle;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.SUPERVISION__PROJECT_TITLE, oldProjectTitle, projectTitle));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Person> getSupervisors() {
		if (supervisors == null) {
			supervisors = new EObjectResolvingEList<Person>(Person.class, this, ResearchCVPackage.SUPERVISION__SUPERVISORS);
		}
		return supervisors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Institution> getInstitutions() {
		if (institutions == null) {
			institutions = new EObjectResolvingEList<Institution>(Institution.class, this, ResearchCVPackage.SUPERVISION__INSTITUTIONS);
		}
		return institutions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Institution getMainInstitution() {
		return (Institution)MAIN_INSTITUTION__ESETTING_DELEGATE.dynamicGet(this, null, 0, true, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Institution basicGetMainInstitution() {
		return (Institution)MAIN_INSTITUTION__ESETTING_DELEGATE.dynamicGet(this, null, 0, false, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Person getMainSupervisor() {
		return (Person)MAIN_SUPERVISOR__ESETTING_DELEGATE.dynamicGet(this, null, 0, true, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Person basicGetMainSupervisor() {
		return (Person)MAIN_SUPERVISOR__ESETTING_DELEGATE.dynamicGet(this, null, 0, false, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isCompleted() {
		return (Boolean)COMPLETED__ESETTING_DELEGATE.dynamicGet(this, null, 0, true, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getFinalWorkURL() {
		return finalWorkURL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFinalWorkURL(String newFinalWorkURL) {
		String oldFinalWorkURL = finalWorkURL;
		finalWorkURL = newFinalWorkURL;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.SUPERVISION__FINAL_WORK_URL, oldFinalWorkURL, finalWorkURL));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getFinalWorkDOI() {
		return finalWorkDOI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFinalWorkDOI(String newFinalWorkDOI) {
		String oldFinalWorkDOI = finalWorkDOI;
		finalWorkDOI = newFinalWorkDOI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.SUPERVISION__FINAL_WORK_DOI, oldFinalWorkDOI, finalWorkDOI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ResearchCVPackage.SUPERVISION__DASTE_START:
				return basicSetDasteStart(null, msgs);
			case ResearchCVPackage.SUPERVISION__DATE_END:
				return basicSetDateEnd(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ResearchCVPackage.SUPERVISION__DASTE_START:
				return getDasteStart();
			case ResearchCVPackage.SUPERVISION__DATE_END:
				return getDateEnd();
			case ResearchCVPackage.SUPERVISION__LEVEL:
				return getLevel();
			case ResearchCVPackage.SUPERVISION__STUDENT:
				if (resolve) return getStudent();
				return basicGetStudent();
			case ResearchCVPackage.SUPERVISION__PROJECT_TITLE:
				return getProjectTitle();
			case ResearchCVPackage.SUPERVISION__SUPERVISORS:
				return getSupervisors();
			case ResearchCVPackage.SUPERVISION__INSTITUTIONS:
				return getInstitutions();
			case ResearchCVPackage.SUPERVISION__MAIN_INSTITUTION:
				if (resolve) return getMainInstitution();
				return basicGetMainInstitution();
			case ResearchCVPackage.SUPERVISION__MAIN_SUPERVISOR:
				if (resolve) return getMainSupervisor();
				return basicGetMainSupervisor();
			case ResearchCVPackage.SUPERVISION__COMPLETED:
				return isCompleted();
			case ResearchCVPackage.SUPERVISION__FINAL_WORK_URL:
				return getFinalWorkURL();
			case ResearchCVPackage.SUPERVISION__FINAL_WORK_DOI:
				return getFinalWorkDOI();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ResearchCVPackage.SUPERVISION__DASTE_START:
				setDasteStart((PublishedDate)newValue);
				return;
			case ResearchCVPackage.SUPERVISION__DATE_END:
				setDateEnd((PublishedDate)newValue);
				return;
			case ResearchCVPackage.SUPERVISION__LEVEL:
				setLevel((SupervisionKind)newValue);
				return;
			case ResearchCVPackage.SUPERVISION__STUDENT:
				setStudent((Person)newValue);
				return;
			case ResearchCVPackage.SUPERVISION__PROJECT_TITLE:
				setProjectTitle((String)newValue);
				return;
			case ResearchCVPackage.SUPERVISION__SUPERVISORS:
				getSupervisors().clear();
				getSupervisors().addAll((Collection<? extends Person>)newValue);
				return;
			case ResearchCVPackage.SUPERVISION__INSTITUTIONS:
				getInstitutions().clear();
				getInstitutions().addAll((Collection<? extends Institution>)newValue);
				return;
			case ResearchCVPackage.SUPERVISION__FINAL_WORK_URL:
				setFinalWorkURL((String)newValue);
				return;
			case ResearchCVPackage.SUPERVISION__FINAL_WORK_DOI:
				setFinalWorkDOI((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.SUPERVISION__DASTE_START:
				setDasteStart((PublishedDate)null);
				return;
			case ResearchCVPackage.SUPERVISION__DATE_END:
				setDateEnd((PublishedDate)null);
				return;
			case ResearchCVPackage.SUPERVISION__LEVEL:
				setLevel(LEVEL_EDEFAULT);
				return;
			case ResearchCVPackage.SUPERVISION__STUDENT:
				setStudent((Person)null);
				return;
			case ResearchCVPackage.SUPERVISION__PROJECT_TITLE:
				setProjectTitle(PROJECT_TITLE_EDEFAULT);
				return;
			case ResearchCVPackage.SUPERVISION__SUPERVISORS:
				getSupervisors().clear();
				return;
			case ResearchCVPackage.SUPERVISION__INSTITUTIONS:
				getInstitutions().clear();
				return;
			case ResearchCVPackage.SUPERVISION__FINAL_WORK_URL:
				setFinalWorkURL(FINAL_WORK_URL_EDEFAULT);
				return;
			case ResearchCVPackage.SUPERVISION__FINAL_WORK_DOI:
				setFinalWorkDOI(FINAL_WORK_DOI_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.SUPERVISION__DASTE_START:
				return dasteStart != null;
			case ResearchCVPackage.SUPERVISION__DATE_END:
				return dateEnd != null;
			case ResearchCVPackage.SUPERVISION__LEVEL:
				return level != LEVEL_EDEFAULT;
			case ResearchCVPackage.SUPERVISION__STUDENT:
				return student != null;
			case ResearchCVPackage.SUPERVISION__PROJECT_TITLE:
				return PROJECT_TITLE_EDEFAULT == null ? projectTitle != null : !PROJECT_TITLE_EDEFAULT.equals(projectTitle);
			case ResearchCVPackage.SUPERVISION__SUPERVISORS:
				return supervisors != null && !supervisors.isEmpty();
			case ResearchCVPackage.SUPERVISION__INSTITUTIONS:
				return institutions != null && !institutions.isEmpty();
			case ResearchCVPackage.SUPERVISION__MAIN_INSTITUTION:
				return MAIN_INSTITUTION__ESETTING_DELEGATE.dynamicIsSet(this, null, 0);
			case ResearchCVPackage.SUPERVISION__MAIN_SUPERVISOR:
				return MAIN_SUPERVISOR__ESETTING_DELEGATE.dynamicIsSet(this, null, 0);
			case ResearchCVPackage.SUPERVISION__COMPLETED:
				return COMPLETED__ESETTING_DELEGATE.dynamicIsSet(this, null, 0);
			case ResearchCVPackage.SUPERVISION__FINAL_WORK_URL:
				return FINAL_WORK_URL_EDEFAULT == null ? finalWorkURL != null : !FINAL_WORK_URL_EDEFAULT.equals(finalWorkURL);
			case ResearchCVPackage.SUPERVISION__FINAL_WORK_DOI:
				return FINAL_WORK_DOI_EDEFAULT == null ? finalWorkDOI != null : !FINAL_WORK_DOI_EDEFAULT.equals(finalWorkDOI);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (level: ");
		result.append(level);
		result.append(", projectTitle: ");
		result.append(projectTitle);
		result.append(", finalWorkURL: ");
		result.append(finalWorkURL);
		result.append(", finalWorkDOI: ");
		result.append(finalWorkDOI);
		result.append(')');
		return result.toString();
	}

} //SupervisionImpl
