/**
 */
package org.montex.researchcv.impl;

import org.eclipse.emf.ecore.EClass;
import org.montex.researchcv.Conference;
import org.montex.researchcv.ResearchCVPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Conference</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ConferenceImpl extends InProceedingsImpl implements Conference {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ResearchCVPackage.Literals.CONFERENCE;
	}

} //ConferenceImpl
