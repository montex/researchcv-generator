/**
 */
package org.montex.researchcv.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.montex.researchcv.Book;
import org.montex.researchcv.ResearchCVPackage;
import org.montex.researchcv.SerialNumber;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Book</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.impl.BookImpl#getPublisher <em>Publisher</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.BookImpl#getSeries <em>Series</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.BookImpl#getVolume <em>Volume</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.BookImpl#getISBN <em>ISBN</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BookImpl extends PublicationImpl implements Book {
	/**
	 * The default value of the '{@link #getPublisher() <em>Publisher</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPublisher()
	 * @generated
	 * @ordered
	 */
	protected static final String PUBLISHER_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getPublisher() <em>Publisher</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPublisher()
	 * @generated
	 * @ordered
	 */
	protected String publisher = PUBLISHER_EDEFAULT;
	/**
	 * The default value of the '{@link #getSeries() <em>Series</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSeries()
	 * @generated
	 * @ordered
	 */
	protected static final String SERIES_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getSeries() <em>Series</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSeries()
	 * @generated
	 * @ordered
	 */
	protected String series = SERIES_EDEFAULT;
	/**
	 * The default value of the '{@link #getVolume() <em>Volume</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVolume()
	 * @generated
	 * @ordered
	 */
	protected static final String VOLUME_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getVolume() <em>Volume</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVolume()
	 * @generated
	 * @ordered
	 */
	protected String volume = VOLUME_EDEFAULT;
	/**
	 * The cached value of the '{@link #getISBN() <em>ISBN</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getISBN()
	 * @generated
	 * @ordered
	 */
	protected EList<SerialNumber> isbn;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BookImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ResearchCVPackage.Literals.BOOK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getPublisher() {
		return publisher;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPublisher(String newPublisher) {
		String oldPublisher = publisher;
		publisher = newPublisher;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.BOOK__PUBLISHER, oldPublisher, publisher));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSeries() {
		return series;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSeries(String newSeries) {
		String oldSeries = series;
		series = newSeries;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.BOOK__SERIES, oldSeries, series));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getVolume() {
		return volume;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVolume(String newVolume) {
		String oldVolume = volume;
		volume = newVolume;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.BOOK__VOLUME, oldVolume, volume));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SerialNumber> getISBN() {
		if (isbn == null) {
			isbn = new EObjectContainmentEList<SerialNumber>(SerialNumber.class, this, ResearchCVPackage.BOOK__ISBN);
		}
		return isbn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ResearchCVPackage.BOOK__ISBN:
				return ((InternalEList<?>)getISBN()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ResearchCVPackage.BOOK__PUBLISHER:
				return getPublisher();
			case ResearchCVPackage.BOOK__SERIES:
				return getSeries();
			case ResearchCVPackage.BOOK__VOLUME:
				return getVolume();
			case ResearchCVPackage.BOOK__ISBN:
				return getISBN();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ResearchCVPackage.BOOK__PUBLISHER:
				setPublisher((String)newValue);
				return;
			case ResearchCVPackage.BOOK__SERIES:
				setSeries((String)newValue);
				return;
			case ResearchCVPackage.BOOK__VOLUME:
				setVolume((String)newValue);
				return;
			case ResearchCVPackage.BOOK__ISBN:
				getISBN().clear();
				getISBN().addAll((Collection<? extends SerialNumber>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.BOOK__PUBLISHER:
				setPublisher(PUBLISHER_EDEFAULT);
				return;
			case ResearchCVPackage.BOOK__SERIES:
				setSeries(SERIES_EDEFAULT);
				return;
			case ResearchCVPackage.BOOK__VOLUME:
				setVolume(VOLUME_EDEFAULT);
				return;
			case ResearchCVPackage.BOOK__ISBN:
				getISBN().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.BOOK__PUBLISHER:
				return PUBLISHER_EDEFAULT == null ? publisher != null : !PUBLISHER_EDEFAULT.equals(publisher);
			case ResearchCVPackage.BOOK__SERIES:
				return SERIES_EDEFAULT == null ? series != null : !SERIES_EDEFAULT.equals(series);
			case ResearchCVPackage.BOOK__VOLUME:
				return VOLUME_EDEFAULT == null ? volume != null : !VOLUME_EDEFAULT.equals(volume);
			case ResearchCVPackage.BOOK__ISBN:
				return isbn != null && !isbn.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (publisher: ");
		result.append(publisher);
		result.append(", series: ");
		result.append(series);
		result.append(", volume: ");
		result.append(volume);
		result.append(')');
		return result.toString();
	}

} //BookImpl
