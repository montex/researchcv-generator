/**
 */
package org.montex.researchcv.impl;

import java.util.Currency;
import java.util.Locale;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.montex.researchcv.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ResearchCVFactoryImpl extends EFactoryImpl implements ResearchCVFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ResearchCVFactory init() {
		try {
			ResearchCVFactory theResearchCVFactory = (ResearchCVFactory)EPackage.Registry.INSTANCE.getEFactory(ResearchCVPackage.eNS_URI);
			if (theResearchCVFactory != null) {
				return theResearchCVFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ResearchCVFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResearchCVFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ResearchCVPackage.PERSON: return createPerson();
			case ResearchCVPackage.RESEARCHER: return createResearcher();
			case ResearchCVPackage.ADDRESS: return createAddress();
			case ResearchCVPackage.EMAIL: return createEmail();
			case ResearchCVPackage.PHONE: return createPhone();
			case ResearchCVPackage.SOCIAL_PROFILE: return createSocialProfile();
			case ResearchCVPackage.WEBSITE: return createWebsite();
			case ResearchCVPackage.JOURNAL: return createJournal();
			case ResearchCVPackage.CONFERENCE: return createConference();
			case ResearchCVPackage.WORKSHOP: return createWorkshop();
			case ResearchCVPackage.BOOK: return createBook();
			case ResearchCVPackage.BOOK_CHAPTER: return createBookChapter();
			case ResearchCVPackage.REPORT: return createReport();
			case ResearchCVPackage.MISCELLANEOUS: return createMiscellaneous();
			case ResearchCVPackage.PUBLISHED_DATE: return createPublishedDate();
			case ResearchCVPackage.SERIAL_NUMBER: return createSerialNumber();
			case ResearchCVPackage.RESEARCH_GROUP: return createResearchGroup();
			case ResearchCVPackage.COURSE: return createCourse();
			case ResearchCVPackage.TAUGHT_COURSE: return createTaughtCourse();
			case ResearchCVPackage.RESEARCH_TOPIC: return createResearchTopic();
			case ResearchCVPackage.PROJECT: return createProject();
			case ResearchCVPackage.BUDGET: return createBudget();
			case ResearchCVPackage.PERSON_REGISTRY: return createPersonRegistry();
			case ResearchCVPackage.COURSE_OFFERING: return createCourseOffering();
			case ResearchCVPackage.SUPERVISION: return createSupervision();
			case ResearchCVPackage.INSTITUTION_REGISTRY: return createInstitutionRegistry();
			case ResearchCVPackage.INSTITUTION: return createInstitution();
			case ResearchCVPackage.MULTI_LANGUAGE_STRING: return createMultiLanguageString();
			case ResearchCVPackage.LOCALIZED_STRING: return createLocalizedString();
			case ResearchCVPackage.SUPERVISIONS_REGISTRY: return createSupervisionsRegistry();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case ResearchCVPackage.CONTACT_KIND:
				return createContactKindFromString(eDataType, initialValue);
			case ResearchCVPackage.SERIAL_NUMBER_KIND:
				return createSerialNumberKindFromString(eDataType, initialValue);
			case ResearchCVPackage.COURSE_KIND:
				return createCourseKindFromString(eDataType, initialValue);
			case ResearchCVPackage.SUPERVISION_KIND:
				return createSupervisionKindFromString(eDataType, initialValue);
			case ResearchCVPackage.PARTICIPATION_KIND:
				return createParticipationKindFromString(eDataType, initialValue);
			case ResearchCVPackage.CURRENCY:
				return createCurrencyFromString(eDataType, initialValue);
			case ResearchCVPackage.LANGUAGE_CODE:
				return createLanguageCodeFromString(eDataType, initialValue);
			case ResearchCVPackage.COUNTRY_CODE:
				return createCountryCodeFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case ResearchCVPackage.CONTACT_KIND:
				return convertContactKindToString(eDataType, instanceValue);
			case ResearchCVPackage.SERIAL_NUMBER_KIND:
				return convertSerialNumberKindToString(eDataType, instanceValue);
			case ResearchCVPackage.COURSE_KIND:
				return convertCourseKindToString(eDataType, instanceValue);
			case ResearchCVPackage.SUPERVISION_KIND:
				return convertSupervisionKindToString(eDataType, instanceValue);
			case ResearchCVPackage.PARTICIPATION_KIND:
				return convertParticipationKindToString(eDataType, instanceValue);
			case ResearchCVPackage.CURRENCY:
				return convertCurrencyToString(eDataType, instanceValue);
			case ResearchCVPackage.LANGUAGE_CODE:
				return convertLanguageCodeToString(eDataType, instanceValue);
			case ResearchCVPackage.COUNTRY_CODE:
				return convertCountryCodeToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Person createPerson() {
		PersonImpl person = new PersonImpl();
		return person;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Researcher createResearcher() {
		ResearcherImpl researcher = new ResearcherImpl();
		return researcher;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Address createAddress() {
		AddressImpl address = new AddressImpl();
		return address;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Email createEmail() {
		EmailImpl email = new EmailImpl();
		return email;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Phone createPhone() {
		PhoneImpl phone = new PhoneImpl();
		return phone;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SocialProfile createSocialProfile() {
		SocialProfileImpl socialProfile = new SocialProfileImpl();
		return socialProfile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Website createWebsite() {
		WebsiteImpl website = new WebsiteImpl();
		return website;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Journal createJournal() {
		JournalImpl journal = new JournalImpl();
		return journal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Conference createConference() {
		ConferenceImpl conference = new ConferenceImpl();
		return conference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Workshop createWorkshop() {
		WorkshopImpl workshop = new WorkshopImpl();
		return workshop;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Book createBook() {
		BookImpl book = new BookImpl();
		return book;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BookChapter createBookChapter() {
		BookChapterImpl bookChapter = new BookChapterImpl();
		return bookChapter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Report createReport() {
		ReportImpl report = new ReportImpl();
		return report;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Miscellaneous createMiscellaneous() {
		MiscellaneousImpl miscellaneous = new MiscellaneousImpl();
		return miscellaneous;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PublishedDate createPublishedDate() {
		PublishedDateImpl publishedDate = new PublishedDateImpl();
		return publishedDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SerialNumber createSerialNumber() {
		SerialNumberImpl serialNumber = new SerialNumberImpl();
		return serialNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResearchGroup createResearchGroup() {
		ResearchGroupImpl researchGroup = new ResearchGroupImpl();
		return researchGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Course createCourse() {
		CourseImpl course = new CourseImpl();
		return course;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TaughtCourse createTaughtCourse() {
		TaughtCourseImpl taughtCourse = new TaughtCourseImpl();
		return taughtCourse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResearchTopic createResearchTopic() {
		ResearchTopicImpl researchTopic = new ResearchTopicImpl();
		return researchTopic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Project createProject() {
		ProjectImpl project = new ProjectImpl();
		return project;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Budget createBudget() {
		BudgetImpl budget = new BudgetImpl();
		return budget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PersonRegistry createPersonRegistry() {
		PersonRegistryImpl personRegistry = new PersonRegistryImpl();
		return personRegistry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CourseOffering createCourseOffering() {
		CourseOfferingImpl courseOffering = new CourseOfferingImpl();
		return courseOffering;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Supervision createSupervision() {
		SupervisionImpl supervision = new SupervisionImpl();
		return supervision;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public InstitutionRegistry createInstitutionRegistry() {
		InstitutionRegistryImpl institutionRegistry = new InstitutionRegistryImpl();
		return institutionRegistry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Institution createInstitution() {
		InstitutionImpl institution = new InstitutionImpl();
		return institution;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MultiLanguageString createMultiLanguageString() {
		MultiLanguageStringImpl multiLanguageString = new MultiLanguageStringImpl();
		return multiLanguageString;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LocalizedString createLocalizedString() {
		LocalizedStringImpl localizedString = new LocalizedStringImpl();
		return localizedString;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SupervisionsRegistry createSupervisionsRegistry() {
		SupervisionsRegistryImpl supervisionsRegistry = new SupervisionsRegistryImpl();
		return supervisionsRegistry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ContactKind createContactKindFromString(EDataType eDataType, String initialValue) {
		ContactKind result = ContactKind.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertContactKindToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SerialNumberKind createSerialNumberKindFromString(EDataType eDataType, String initialValue) {
		SerialNumberKind result = SerialNumberKind.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSerialNumberKindToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseKind createCourseKindFromString(EDataType eDataType, String initialValue) {
		CourseKind result = CourseKind.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCourseKindToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SupervisionKind createSupervisionKindFromString(EDataType eDataType, String initialValue) {
		SupervisionKind result = SupervisionKind.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSupervisionKindToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParticipationKind createParticipationKindFromString(EDataType eDataType, String initialValue) {
		ParticipationKind result = ParticipationKind.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertParticipationKindToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * Customized conversion using java.util.Currency
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Currency createCurrencyFromString(EDataType eDataType, String initialValue) {
		return Currency.getInstance(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Customized conversion using java.util.Currency
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String convertCurrencyToString(EDataType eDataType, Object instanceValue) {
		String value = "";
		if(instanceValue != null) {
			value = instanceValue.toString();
		}
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Locale createLanguageCodeFromString(EDataType eDataType, String initialValue) {
		return (Locale)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLanguageCodeToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Locale createCountryCodeFromString(EDataType eDataType, String initialValue) {
		return (Locale)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCountryCodeToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResearchCVPackage getResearchCVPackage() {
		return (ResearchCVPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ResearchCVPackage getPackage() {
		return ResearchCVPackage.eINSTANCE;
	}

} //ResearchCVFactoryImpl
