/**
 */
package org.montex.researchcv.impl;

import java.lang.reflect.InvocationTargetException;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.GregorianCalendar;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.montex.researchcv.Budget;
import org.montex.researchcv.Grant;
import org.montex.researchcv.ParticipationKind;
import org.montex.researchcv.Publication;
import org.montex.researchcv.PublishedDate;
import org.montex.researchcv.ResearchCVPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Grant</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.impl.GrantImpl#getRelatedPublications <em>Related Publications</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.GrantImpl#getURL <em>URL</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.GrantImpl#getAgency <em>Agency</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.GrantImpl#getProgram <em>Program</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.GrantImpl#getCall <em>Call</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.GrantImpl#getNumber <em>Number</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.GrantImpl#getBudget <em>Budget</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.GrantImpl#getStartDate <em>Start Date</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.GrantImpl#getEndDate <em>End Date</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.GrantImpl#getGrantID <em>Grant ID</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.GrantImpl#getRole <em>Role</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class GrantImpl extends MinimalEObjectImpl.Container implements Grant {
	/**
	 * The cached value of the '{@link #getRelatedPublications() <em>Related Publications</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelatedPublications()
	 * @generated
	 * @ordered
	 */
	protected EList<Publication> relatedPublications;
	/**
	 * The cached value of the '{@link #getURL() <em>URL</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getURL()
	 * @generated
	 * @ordered
	 */
	protected EList<String> url;
	/**
	 * The default value of the '{@link #getAgency() <em>Agency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAgency()
	 * @generated
	 * @ordered
	 */
	protected static final String AGENCY_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getAgency() <em>Agency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAgency()
	 * @generated
	 * @ordered
	 */
	protected String agency = AGENCY_EDEFAULT;
	/**
	 * The default value of the '{@link #getProgram() <em>Program</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProgram()
	 * @generated
	 * @ordered
	 */
	protected static final String PROGRAM_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getProgram() <em>Program</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProgram()
	 * @generated
	 * @ordered
	 */
	protected String program = PROGRAM_EDEFAULT;
	/**
	 * The default value of the '{@link #getCall() <em>Call</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCall()
	 * @generated
	 * @ordered
	 */
	protected static final String CALL_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getCall() <em>Call</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCall()
	 * @generated
	 * @ordered
	 */
	protected String call = CALL_EDEFAULT;
	/**
	 * The default value of the '{@link #getNumber() <em>Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumber()
	 * @generated
	 * @ordered
	 */
	protected static final String NUMBER_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getNumber() <em>Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumber()
	 * @generated
	 * @ordered
	 */
	protected String number = NUMBER_EDEFAULT;
	/**
	 * The cached value of the '{@link #getBudget() <em>Budget</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBudget()
	 * @generated
	 * @ordered
	 */
	protected EList<Budget> budget;
	/**
	 * The cached value of the '{@link #getStartDate() <em>Start Date</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartDate()
	 * @generated
	 * @ordered
	 */
	protected PublishedDate startDate;
	/**
	 * The cached value of the '{@link #getEndDate() <em>End Date</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndDate()
	 * @generated
	 * @ordered
	 */
	protected PublishedDate endDate;
	/**
	 * The cached setting delegate for the '{@link #getGrantID() <em>Grant ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGrantID()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature.Internal.SettingDelegate GRANT_ID__ESETTING_DELEGATE = ((EStructuralFeature.Internal)ResearchCVPackage.Literals.GRANT__GRANT_ID).getSettingDelegate();

	/**
	 * The default value of the '{@link #getRole() <em>Role</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRole()
	 * @generated
	 * @ordered
	 */
	protected static final ParticipationKind ROLE_EDEFAULT = ParticipationKind.PARTICIPANT;
	/**
	 * The cached value of the '{@link #getRole() <em>Role</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRole()
	 * @generated
	 * @ordered
	 */
	protected ParticipationKind role = ROLE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GrantImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ResearchCVPackage.Literals.GRANT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Publication> getRelatedPublications() {
		if (relatedPublications == null) {
			relatedPublications = new EObjectWithInverseResolvingEList.ManyInverse<Publication>(Publication.class, this, ResearchCVPackage.GRANT__RELATED_PUBLICATIONS, ResearchCVPackage.PUBLICATION__RELATED_PROJECTS);
		}
		return relatedPublications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getURL() {
		if (url == null) {
			url = new EDataTypeUniqueEList<String>(String.class, this, ResearchCVPackage.GRANT__URL);
		}
		return url;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getAgency() {
		return agency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAgency(String newAgency) {
		String oldAgency = agency;
		agency = newAgency;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.GRANT__AGENCY, oldAgency, agency));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getNumber() {
		return number;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNumber(String newNumber) {
		String oldNumber = number;
		number = newNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.GRANT__NUMBER, oldNumber, number));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getProgram() {
		return program;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setProgram(String newProgram) {
		String oldProgram = program;
		program = newProgram;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.GRANT__PROGRAM, oldProgram, program));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCall() {
		return call;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCall(String newCall) {
		String oldCall = call;
		call = newCall;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.GRANT__CALL, oldCall, call));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getGrantID() {
		return (String)GRANT_ID__ESETTING_DELEGATE.dynamicGet(this, null, 0, true, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ParticipationKind getRole() {
		return role;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRole(ParticipationKind newRole) {
		ParticipationKind oldRole = role;
		role = newRole == null ? ROLE_EDEFAULT : newRole;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.GRANT__ROLE, oldRole, role));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Budget> getBudget() {
		if (budget == null) {
			budget = new EObjectContainmentEList<Budget>(Budget.class, this, ResearchCVPackage.GRANT__BUDGET);
		}
		return budget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PublishedDate getStartDate() {
		return startDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStartDate(PublishedDate newStartDate, NotificationChain msgs) {
		PublishedDate oldStartDate = startDate;
		startDate = newStartDate;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ResearchCVPackage.GRANT__START_DATE, oldStartDate, newStartDate);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStartDate(PublishedDate newStartDate) {
		if (newStartDate != startDate) {
			NotificationChain msgs = null;
			if (startDate != null)
				msgs = ((InternalEObject)startDate).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ResearchCVPackage.GRANT__START_DATE, null, msgs);
			if (newStartDate != null)
				msgs = ((InternalEObject)newStartDate).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ResearchCVPackage.GRANT__START_DATE, null, msgs);
			msgs = basicSetStartDate(newStartDate, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.GRANT__START_DATE, newStartDate, newStartDate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PublishedDate getEndDate() {
		return endDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEndDate(PublishedDate newEndDate, NotificationChain msgs) {
		PublishedDate oldEndDate = endDate;
		endDate = newEndDate;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ResearchCVPackage.GRANT__END_DATE, oldEndDate, newEndDate);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEndDate(PublishedDate newEndDate) {
		if (newEndDate != endDate) {
			NotificationChain msgs = null;
			if (endDate != null)
				msgs = ((InternalEObject)endDate).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ResearchCVPackage.GRANT__END_DATE, null, msgs);
			if (newEndDate != null)
				msgs = ((InternalEObject)newEndDate).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ResearchCVPackage.GRANT__END_DATE, null, msgs);
			msgs = basicSetEndDate(newEndDate, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.GRANT__END_DATE, newEndDate, newEndDate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * Returns true if the project is active, that is, if the end date is greater than the current date
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isActive() {
		ZonedDateTime now = ZonedDateTime.now();

		GregorianCalendar calendar = new GregorianCalendar();
		calendar.set(endDate.getYear(), endDate.getMonth(), endDate.getDay());
		ZonedDateTime end = calendar.toZonedDateTime();
		
		return now.isBefore(end);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ResearchCVPackage.GRANT__RELATED_PUBLICATIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getRelatedPublications()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ResearchCVPackage.GRANT__RELATED_PUBLICATIONS:
				return ((InternalEList<?>)getRelatedPublications()).basicRemove(otherEnd, msgs);
			case ResearchCVPackage.GRANT__BUDGET:
				return ((InternalEList<?>)getBudget()).basicRemove(otherEnd, msgs);
			case ResearchCVPackage.GRANT__START_DATE:
				return basicSetStartDate(null, msgs);
			case ResearchCVPackage.GRANT__END_DATE:
				return basicSetEndDate(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ResearchCVPackage.GRANT__RELATED_PUBLICATIONS:
				return getRelatedPublications();
			case ResearchCVPackage.GRANT__URL:
				return getURL();
			case ResearchCVPackage.GRANT__AGENCY:
				return getAgency();
			case ResearchCVPackage.GRANT__PROGRAM:
				return getProgram();
			case ResearchCVPackage.GRANT__CALL:
				return getCall();
			case ResearchCVPackage.GRANT__NUMBER:
				return getNumber();
			case ResearchCVPackage.GRANT__BUDGET:
				return getBudget();
			case ResearchCVPackage.GRANT__START_DATE:
				return getStartDate();
			case ResearchCVPackage.GRANT__END_DATE:
				return getEndDate();
			case ResearchCVPackage.GRANT__GRANT_ID:
				return getGrantID();
			case ResearchCVPackage.GRANT__ROLE:
				return getRole();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ResearchCVPackage.GRANT__RELATED_PUBLICATIONS:
				getRelatedPublications().clear();
				getRelatedPublications().addAll((Collection<? extends Publication>)newValue);
				return;
			case ResearchCVPackage.GRANT__URL:
				getURL().clear();
				getURL().addAll((Collection<? extends String>)newValue);
				return;
			case ResearchCVPackage.GRANT__AGENCY:
				setAgency((String)newValue);
				return;
			case ResearchCVPackage.GRANT__PROGRAM:
				setProgram((String)newValue);
				return;
			case ResearchCVPackage.GRANT__CALL:
				setCall((String)newValue);
				return;
			case ResearchCVPackage.GRANT__NUMBER:
				setNumber((String)newValue);
				return;
			case ResearchCVPackage.GRANT__BUDGET:
				getBudget().clear();
				getBudget().addAll((Collection<? extends Budget>)newValue);
				return;
			case ResearchCVPackage.GRANT__START_DATE:
				setStartDate((PublishedDate)newValue);
				return;
			case ResearchCVPackage.GRANT__END_DATE:
				setEndDate((PublishedDate)newValue);
				return;
			case ResearchCVPackage.GRANT__ROLE:
				setRole((ParticipationKind)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.GRANT__RELATED_PUBLICATIONS:
				getRelatedPublications().clear();
				return;
			case ResearchCVPackage.GRANT__URL:
				getURL().clear();
				return;
			case ResearchCVPackage.GRANT__AGENCY:
				setAgency(AGENCY_EDEFAULT);
				return;
			case ResearchCVPackage.GRANT__PROGRAM:
				setProgram(PROGRAM_EDEFAULT);
				return;
			case ResearchCVPackage.GRANT__CALL:
				setCall(CALL_EDEFAULT);
				return;
			case ResearchCVPackage.GRANT__NUMBER:
				setNumber(NUMBER_EDEFAULT);
				return;
			case ResearchCVPackage.GRANT__BUDGET:
				getBudget().clear();
				return;
			case ResearchCVPackage.GRANT__START_DATE:
				setStartDate((PublishedDate)null);
				return;
			case ResearchCVPackage.GRANT__END_DATE:
				setEndDate((PublishedDate)null);
				return;
			case ResearchCVPackage.GRANT__ROLE:
				setRole(ROLE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.GRANT__RELATED_PUBLICATIONS:
				return relatedPublications != null && !relatedPublications.isEmpty();
			case ResearchCVPackage.GRANT__URL:
				return url != null && !url.isEmpty();
			case ResearchCVPackage.GRANT__AGENCY:
				return AGENCY_EDEFAULT == null ? agency != null : !AGENCY_EDEFAULT.equals(agency);
			case ResearchCVPackage.GRANT__PROGRAM:
				return PROGRAM_EDEFAULT == null ? program != null : !PROGRAM_EDEFAULT.equals(program);
			case ResearchCVPackage.GRANT__CALL:
				return CALL_EDEFAULT == null ? call != null : !CALL_EDEFAULT.equals(call);
			case ResearchCVPackage.GRANT__NUMBER:
				return NUMBER_EDEFAULT == null ? number != null : !NUMBER_EDEFAULT.equals(number);
			case ResearchCVPackage.GRANT__BUDGET:
				return budget != null && !budget.isEmpty();
			case ResearchCVPackage.GRANT__START_DATE:
				return startDate != null;
			case ResearchCVPackage.GRANT__END_DATE:
				return endDate != null;
			case ResearchCVPackage.GRANT__GRANT_ID:
				return GRANT_ID__ESETTING_DELEGATE.dynamicIsSet(this, null, 0);
			case ResearchCVPackage.GRANT__ROLE:
				return role != ROLE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ResearchCVPackage.GRANT___IS_ACTIVE:
				return isActive();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (URL: ");
		result.append(url);
		result.append(", agency: ");
		result.append(agency);
		result.append(", program: ");
		result.append(program);
		result.append(", call: ");
		result.append(call);
		result.append(", number: ");
		result.append(number);
		result.append(", role: ");
		result.append(role);
		result.append(')');
		return result.toString();
	}

} //GrantImpl
