/**
 */
package org.montex.researchcv.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.montex.researchcv.ResearchCVPackage;
import org.montex.researchcv.Workshop;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Workshop</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.impl.WorkshopImpl#getMainEventName <em>Main Event Name</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.WorkshopImpl#getMainEventShortName <em>Main Event Short Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class WorkshopImpl extends InProceedingsImpl implements Workshop {
	/**
	 * The default value of the '{@link #getMainEventName() <em>Main Event Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMainEventName()
	 * @generated
	 * @ordered
	 */
	protected static final String MAIN_EVENT_NAME_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getMainEventName() <em>Main Event Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMainEventName()
	 * @generated
	 * @ordered
	 */
	protected String mainEventName = MAIN_EVENT_NAME_EDEFAULT;
	/**
	 * The default value of the '{@link #getMainEventShortName() <em>Main Event Short Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMainEventShortName()
	 * @generated
	 * @ordered
	 */
	protected static final String MAIN_EVENT_SHORT_NAME_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getMainEventShortName() <em>Main Event Short Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMainEventShortName()
	 * @generated
	 * @ordered
	 */
	protected String mainEventShortName = MAIN_EVENT_SHORT_NAME_EDEFAULT;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WorkshopImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ResearchCVPackage.Literals.WORKSHOP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getMainEventName() {
		return mainEventName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMainEventName(String newMainEventName) {
		String oldMainEventName = mainEventName;
		mainEventName = newMainEventName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.WORKSHOP__MAIN_EVENT_NAME, oldMainEventName, mainEventName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getMainEventShortName() {
		return mainEventShortName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMainEventShortName(String newMainEventShortName) {
		String oldMainEventShortName = mainEventShortName;
		mainEventShortName = newMainEventShortName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.WORKSHOP__MAIN_EVENT_SHORT_NAME, oldMainEventShortName, mainEventShortName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ResearchCVPackage.WORKSHOP__MAIN_EVENT_NAME:
				return getMainEventName();
			case ResearchCVPackage.WORKSHOP__MAIN_EVENT_SHORT_NAME:
				return getMainEventShortName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ResearchCVPackage.WORKSHOP__MAIN_EVENT_NAME:
				setMainEventName((String)newValue);
				return;
			case ResearchCVPackage.WORKSHOP__MAIN_EVENT_SHORT_NAME:
				setMainEventShortName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.WORKSHOP__MAIN_EVENT_NAME:
				setMainEventName(MAIN_EVENT_NAME_EDEFAULT);
				return;
			case ResearchCVPackage.WORKSHOP__MAIN_EVENT_SHORT_NAME:
				setMainEventShortName(MAIN_EVENT_SHORT_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.WORKSHOP__MAIN_EVENT_NAME:
				return MAIN_EVENT_NAME_EDEFAULT == null ? mainEventName != null : !MAIN_EVENT_NAME_EDEFAULT.equals(mainEventName);
			case ResearchCVPackage.WORKSHOP__MAIN_EVENT_SHORT_NAME:
				return MAIN_EVENT_SHORT_NAME_EDEFAULT == null ? mainEventShortName != null : !MAIN_EVENT_SHORT_NAME_EDEFAULT.equals(mainEventShortName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (mainEventName: ");
		result.append(mainEventName);
		result.append(", mainEventShortName: ");
		result.append(mainEventShortName);
		result.append(')');
		return result.toString();
	}

} //WorkshopImpl
