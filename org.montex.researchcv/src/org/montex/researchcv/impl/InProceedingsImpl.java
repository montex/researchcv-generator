/**
 */
package org.montex.researchcv.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.montex.researchcv.InProceedings;
import org.montex.researchcv.PublishedDate;
import org.montex.researchcv.ResearchCVPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>In Proceedings</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.impl.InProceedingsImpl#getEventEndDate <em>Event End Date</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.InProceedingsImpl#getEventName <em>Event Name</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.InProceedingsImpl#getVenue <em>Venue</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.InProceedingsImpl#getEventShortName <em>Event Short Name</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.InProceedingsImpl#getEventStartDate <em>Event Start Date</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.InProceedingsImpl#getTrackName <em>Track Name</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.InProceedingsImpl#getTrackShortName <em>Track Short Name</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.InProceedingsImpl#isShortPaper <em>Short Paper</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.InProceedingsImpl#isToolPaper <em>Tool Paper</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.InProceedingsImpl#isInvitedPaper <em>Invited Paper</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class InProceedingsImpl extends InCollectionImpl implements InProceedings {
	/**
	 * The cached value of the '{@link #getEventEndDate() <em>Event End Date</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventEndDate()
	 * @generated
	 * @ordered
	 */
	protected PublishedDate eventEndDate;

	/**
	 * The default value of the '{@link #getEventName() <em>Event Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventName()
	 * @generated
	 * @ordered
	 */
	protected static final String EVENT_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEventName() <em>Event Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventName()
	 * @generated
	 * @ordered
	 */
	protected String eventName = EVENT_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getVenue() <em>Venue</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVenue()
	 * @generated
	 * @ordered
	 */
	protected static final String VENUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVenue() <em>Venue</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVenue()
	 * @generated
	 * @ordered
	 */
	protected String venue = VENUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getEventShortName() <em>Event Short Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventShortName()
	 * @generated
	 * @ordered
	 */
	protected static final String EVENT_SHORT_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEventShortName() <em>Event Short Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventShortName()
	 * @generated
	 * @ordered
	 */
	protected String eventShortName = EVENT_SHORT_NAME_EDEFAULT;

	/**
	 * The cached setting delegate for the '{@link #getEventStartDate() <em>Event Start Date</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventStartDate()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature.Internal.SettingDelegate EVENT_START_DATE__ESETTING_DELEGATE = ((EStructuralFeature.Internal)ResearchCVPackage.Literals.IN_PROCEEDINGS__EVENT_START_DATE).getSettingDelegate();

	/**
	 * The default value of the '{@link #getTrackName() <em>Track Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTrackName()
	 * @generated
	 * @ordered
	 */
	protected static final String TRACK_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTrackName() <em>Track Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTrackName()
	 * @generated
	 * @ordered
	 */
	protected String trackName = TRACK_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getTrackShortName() <em>Track Short Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTrackShortName()
	 * @generated
	 * @ordered
	 */
	protected static final String TRACK_SHORT_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTrackShortName() <em>Track Short Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTrackShortName()
	 * @generated
	 * @ordered
	 */
	protected String trackShortName = TRACK_SHORT_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #isShortPaper() <em>Short Paper</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isShortPaper()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SHORT_PAPER_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isShortPaper() <em>Short Paper</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isShortPaper()
	 * @generated
	 * @ordered
	 */
	protected boolean shortPaper = SHORT_PAPER_EDEFAULT;

	/**
	 * The default value of the '{@link #isToolPaper() <em>Tool Paper</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isToolPaper()
	 * @generated
	 * @ordered
	 */
	protected static final boolean TOOL_PAPER_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isToolPaper() <em>Tool Paper</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isToolPaper()
	 * @generated
	 * @ordered
	 */
	protected boolean toolPaper = TOOL_PAPER_EDEFAULT;

	/**
	 * The default value of the '{@link #isInvitedPaper() <em>Invited Paper</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isInvitedPaper()
	 * @generated
	 * @ordered
	 */
	protected static final boolean INVITED_PAPER_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isInvitedPaper() <em>Invited Paper</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isInvitedPaper()
	 * @generated
	 * @ordered
	 */
	protected boolean invitedPaper = INVITED_PAPER_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InProceedingsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ResearchCVPackage.Literals.IN_PROCEEDINGS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PublishedDate getEventEndDate() {
		return eventEndDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEventEndDate(PublishedDate newEventEndDate, NotificationChain msgs) {
		PublishedDate oldEventEndDate = eventEndDate;
		eventEndDate = newEventEndDate;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ResearchCVPackage.IN_PROCEEDINGS__EVENT_END_DATE, oldEventEndDate, newEventEndDate);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEventEndDate(PublishedDate newEventEndDate) {
		if (newEventEndDate != eventEndDate) {
			NotificationChain msgs = null;
			if (eventEndDate != null)
				msgs = ((InternalEObject)eventEndDate).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ResearchCVPackage.IN_PROCEEDINGS__EVENT_END_DATE, null, msgs);
			if (newEventEndDate != null)
				msgs = ((InternalEObject)newEventEndDate).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ResearchCVPackage.IN_PROCEEDINGS__EVENT_END_DATE, null, msgs);
			msgs = basicSetEventEndDate(newEventEndDate, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.IN_PROCEEDINGS__EVENT_END_DATE, newEventEndDate, newEventEndDate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getEventName() {
		return eventName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEventName(String newEventName) {
		String oldEventName = eventName;
		eventName = newEventName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.IN_PROCEEDINGS__EVENT_NAME, oldEventName, eventName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getVenue() {
		return venue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVenue(String newVenue) {
		String oldVenue = venue;
		venue = newVenue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.IN_PROCEEDINGS__VENUE, oldVenue, venue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getEventShortName() {
		return eventShortName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEventShortName(String newEventShortName) {
		String oldEventShortName = eventShortName;
		eventShortName = newEventShortName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.IN_PROCEEDINGS__EVENT_SHORT_NAME, oldEventShortName, eventShortName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PublishedDate getEventStartDate() {
		return (PublishedDate)EVENT_START_DATE__ESETTING_DELEGATE.dynamicGet(this, null, 0, true, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PublishedDate basicGetEventStartDate() {
		return (PublishedDate)EVENT_START_DATE__ESETTING_DELEGATE.dynamicGet(this, null, 0, false, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getTrackName() {
		return trackName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTrackName(String newTrackName) {
		String oldTrackName = trackName;
		trackName = newTrackName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.IN_PROCEEDINGS__TRACK_NAME, oldTrackName, trackName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getTrackShortName() {
		return trackShortName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTrackShortName(String newTrackShortName) {
		String oldTrackShortName = trackShortName;
		trackShortName = newTrackShortName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.IN_PROCEEDINGS__TRACK_SHORT_NAME, oldTrackShortName, trackShortName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isShortPaper() {
		return shortPaper;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setShortPaper(boolean newShortPaper) {
		boolean oldShortPaper = shortPaper;
		shortPaper = newShortPaper;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.IN_PROCEEDINGS__SHORT_PAPER, oldShortPaper, shortPaper));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isToolPaper() {
		return toolPaper;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setToolPaper(boolean newToolPaper) {
		boolean oldToolPaper = toolPaper;
		toolPaper = newToolPaper;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.IN_PROCEEDINGS__TOOL_PAPER, oldToolPaper, toolPaper));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isInvitedPaper() {
		return invitedPaper;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setInvitedPaper(boolean newInvitedPaper) {
		boolean oldInvitedPaper = invitedPaper;
		invitedPaper = newInvitedPaper;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.IN_PROCEEDINGS__INVITED_PAPER, oldInvitedPaper, invitedPaper));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ResearchCVPackage.IN_PROCEEDINGS__EVENT_END_DATE:
				return basicSetEventEndDate(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ResearchCVPackage.IN_PROCEEDINGS__EVENT_END_DATE:
				return getEventEndDate();
			case ResearchCVPackage.IN_PROCEEDINGS__EVENT_NAME:
				return getEventName();
			case ResearchCVPackage.IN_PROCEEDINGS__VENUE:
				return getVenue();
			case ResearchCVPackage.IN_PROCEEDINGS__EVENT_SHORT_NAME:
				return getEventShortName();
			case ResearchCVPackage.IN_PROCEEDINGS__EVENT_START_DATE:
				if (resolve) return getEventStartDate();
				return basicGetEventStartDate();
			case ResearchCVPackage.IN_PROCEEDINGS__TRACK_NAME:
				return getTrackName();
			case ResearchCVPackage.IN_PROCEEDINGS__TRACK_SHORT_NAME:
				return getTrackShortName();
			case ResearchCVPackage.IN_PROCEEDINGS__SHORT_PAPER:
				return isShortPaper();
			case ResearchCVPackage.IN_PROCEEDINGS__TOOL_PAPER:
				return isToolPaper();
			case ResearchCVPackage.IN_PROCEEDINGS__INVITED_PAPER:
				return isInvitedPaper();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ResearchCVPackage.IN_PROCEEDINGS__EVENT_END_DATE:
				setEventEndDate((PublishedDate)newValue);
				return;
			case ResearchCVPackage.IN_PROCEEDINGS__EVENT_NAME:
				setEventName((String)newValue);
				return;
			case ResearchCVPackage.IN_PROCEEDINGS__VENUE:
				setVenue((String)newValue);
				return;
			case ResearchCVPackage.IN_PROCEEDINGS__EVENT_SHORT_NAME:
				setEventShortName((String)newValue);
				return;
			case ResearchCVPackage.IN_PROCEEDINGS__TRACK_NAME:
				setTrackName((String)newValue);
				return;
			case ResearchCVPackage.IN_PROCEEDINGS__TRACK_SHORT_NAME:
				setTrackShortName((String)newValue);
				return;
			case ResearchCVPackage.IN_PROCEEDINGS__SHORT_PAPER:
				setShortPaper((Boolean)newValue);
				return;
			case ResearchCVPackage.IN_PROCEEDINGS__TOOL_PAPER:
				setToolPaper((Boolean)newValue);
				return;
			case ResearchCVPackage.IN_PROCEEDINGS__INVITED_PAPER:
				setInvitedPaper((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.IN_PROCEEDINGS__EVENT_END_DATE:
				setEventEndDate((PublishedDate)null);
				return;
			case ResearchCVPackage.IN_PROCEEDINGS__EVENT_NAME:
				setEventName(EVENT_NAME_EDEFAULT);
				return;
			case ResearchCVPackage.IN_PROCEEDINGS__VENUE:
				setVenue(VENUE_EDEFAULT);
				return;
			case ResearchCVPackage.IN_PROCEEDINGS__EVENT_SHORT_NAME:
				setEventShortName(EVENT_SHORT_NAME_EDEFAULT);
				return;
			case ResearchCVPackage.IN_PROCEEDINGS__TRACK_NAME:
				setTrackName(TRACK_NAME_EDEFAULT);
				return;
			case ResearchCVPackage.IN_PROCEEDINGS__TRACK_SHORT_NAME:
				setTrackShortName(TRACK_SHORT_NAME_EDEFAULT);
				return;
			case ResearchCVPackage.IN_PROCEEDINGS__SHORT_PAPER:
				setShortPaper(SHORT_PAPER_EDEFAULT);
				return;
			case ResearchCVPackage.IN_PROCEEDINGS__TOOL_PAPER:
				setToolPaper(TOOL_PAPER_EDEFAULT);
				return;
			case ResearchCVPackage.IN_PROCEEDINGS__INVITED_PAPER:
				setInvitedPaper(INVITED_PAPER_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.IN_PROCEEDINGS__EVENT_END_DATE:
				return eventEndDate != null;
			case ResearchCVPackage.IN_PROCEEDINGS__EVENT_NAME:
				return EVENT_NAME_EDEFAULT == null ? eventName != null : !EVENT_NAME_EDEFAULT.equals(eventName);
			case ResearchCVPackage.IN_PROCEEDINGS__VENUE:
				return VENUE_EDEFAULT == null ? venue != null : !VENUE_EDEFAULT.equals(venue);
			case ResearchCVPackage.IN_PROCEEDINGS__EVENT_SHORT_NAME:
				return EVENT_SHORT_NAME_EDEFAULT == null ? eventShortName != null : !EVENT_SHORT_NAME_EDEFAULT.equals(eventShortName);
			case ResearchCVPackage.IN_PROCEEDINGS__EVENT_START_DATE:
				return EVENT_START_DATE__ESETTING_DELEGATE.dynamicIsSet(this, null, 0);
			case ResearchCVPackage.IN_PROCEEDINGS__TRACK_NAME:
				return TRACK_NAME_EDEFAULT == null ? trackName != null : !TRACK_NAME_EDEFAULT.equals(trackName);
			case ResearchCVPackage.IN_PROCEEDINGS__TRACK_SHORT_NAME:
				return TRACK_SHORT_NAME_EDEFAULT == null ? trackShortName != null : !TRACK_SHORT_NAME_EDEFAULT.equals(trackShortName);
			case ResearchCVPackage.IN_PROCEEDINGS__SHORT_PAPER:
				return shortPaper != SHORT_PAPER_EDEFAULT;
			case ResearchCVPackage.IN_PROCEEDINGS__TOOL_PAPER:
				return toolPaper != TOOL_PAPER_EDEFAULT;
			case ResearchCVPackage.IN_PROCEEDINGS__INVITED_PAPER:
				return invitedPaper != INVITED_PAPER_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (eventName: ");
		result.append(eventName);
		result.append(", venue: ");
		result.append(venue);
		result.append(", eventShortName: ");
		result.append(eventShortName);
		result.append(", trackName: ");
		result.append(trackName);
		result.append(", trackShortName: ");
		result.append(trackShortName);
		result.append(", shortPaper: ");
		result.append(shortPaper);
		result.append(", toolPaper: ");
		result.append(toolPaper);
		result.append(", invitedPaper: ");
		result.append(invitedPaper);
		result.append(')');
		return result.toString();
	}

} //InProceedingsImpl
