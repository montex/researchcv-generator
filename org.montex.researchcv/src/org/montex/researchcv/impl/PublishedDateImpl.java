/**
 */
package org.montex.researchcv.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.montex.researchcv.PublishedDate;
import org.montex.researchcv.ResearchCVPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Published Date</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.impl.PublishedDateImpl#getDay <em>Day</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.PublishedDateImpl#getMonth <em>Month</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.PublishedDateImpl#getYear <em>Year</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.PublishedDateImpl#getMonthName <em>Month Name</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.PublishedDateImpl#getMonthShortName <em>Month Short Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PublishedDateImpl extends MinimalEObjectImpl.Container implements PublishedDate {
	/**
	 * The default value of the '{@link #getDay() <em>Day</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDay()
	 * @generated
	 * @ordered
	 */
	protected static final int DAY_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getDay() <em>Day</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDay()
	 * @generated
	 * @ordered
	 */
	protected int day = DAY_EDEFAULT;

	/**
	 * The default value of the '{@link #getMonth() <em>Month</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMonth()
	 * @generated
	 * @ordered
	 */
	protected static final int MONTH_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getMonth() <em>Month</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMonth()
	 * @generated
	 * @ordered
	 */
	protected int month = MONTH_EDEFAULT;

	/**
	 * The default value of the '{@link #getYear() <em>Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getYear()
	 * @generated
	 * @ordered
	 */
	protected static final int YEAR_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getYear() <em>Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getYear()
	 * @generated
	 * @ordered
	 */
	protected int year = YEAR_EDEFAULT;

	/**
	 * The default value of the '{@link #getMonthName() <em>Month Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMonthName()
	 * @generated
	 * @ordered
	 */
	protected static final String MONTH_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMonthName() <em>Month Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMonthName()
	 * @generated
	 * @ordered
	 */
	protected String monthName = MONTH_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getMonthShortName() <em>Month Short Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMonthShortName()
	 * @generated
	 * @ordered
	 */
	protected static final String MONTH_SHORT_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMonthShortName() <em>Month Short Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMonthShortName()
	 * @generated
	 * @ordered
	 */
	protected String monthShortName = MONTH_SHORT_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 */
	protected static final String[] monthNames = {"January", "February", "March", "April", "May",
			"June", "July", "August", "September", "October", "November", "December"};

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 */
	protected static final String[] monthShortNames = {"Jan", "Feb", "Mar", "Apr", "May",
			"Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"};

	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PublishedDateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ResearchCVPackage.Literals.PUBLISHED_DATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getDay() {
		return day;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDay(int newDay) {
		int oldDay = day;
		day = newDay;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.PUBLISHED_DATE__DAY, oldDay, day));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getMonth() {
		return month;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMonth(int newMonth) {
		int oldMonth = month;
		month = newMonth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.PUBLISHED_DATE__MONTH, oldMonth, month));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getYear() {
		return year;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setYear(int newYear) {
		int oldYear = year;
		year = newYear;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.PUBLISHED_DATE__YEAR, oldYear, year));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getMonthName() {
		if(getMonth() >= 1 && getMonth() <= 12) {
			return monthNames[getMonth()-1];
		}else {
			return null;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getMonthShortName() {
		if(getMonth() >= 1 && getMonth() <= 12) {
			return monthShortNames[getMonth()-1];
		}else {
			return null;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ResearchCVPackage.PUBLISHED_DATE__DAY:
				return getDay();
			case ResearchCVPackage.PUBLISHED_DATE__MONTH:
				return getMonth();
			case ResearchCVPackage.PUBLISHED_DATE__YEAR:
				return getYear();
			case ResearchCVPackage.PUBLISHED_DATE__MONTH_NAME:
				return getMonthName();
			case ResearchCVPackage.PUBLISHED_DATE__MONTH_SHORT_NAME:
				return getMonthShortName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ResearchCVPackage.PUBLISHED_DATE__DAY:
				setDay((Integer)newValue);
				return;
			case ResearchCVPackage.PUBLISHED_DATE__MONTH:
				setMonth((Integer)newValue);
				return;
			case ResearchCVPackage.PUBLISHED_DATE__YEAR:
				setYear((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.PUBLISHED_DATE__DAY:
				setDay(DAY_EDEFAULT);
				return;
			case ResearchCVPackage.PUBLISHED_DATE__MONTH:
				setMonth(MONTH_EDEFAULT);
				return;
			case ResearchCVPackage.PUBLISHED_DATE__YEAR:
				setYear(YEAR_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.PUBLISHED_DATE__DAY:
				return day != DAY_EDEFAULT;
			case ResearchCVPackage.PUBLISHED_DATE__MONTH:
				return month != MONTH_EDEFAULT;
			case ResearchCVPackage.PUBLISHED_DATE__YEAR:
				return year != YEAR_EDEFAULT;
			case ResearchCVPackage.PUBLISHED_DATE__MONTH_NAME:
				return MONTH_NAME_EDEFAULT == null ? monthName != null : !MONTH_NAME_EDEFAULT.equals(monthName);
			case ResearchCVPackage.PUBLISHED_DATE__MONTH_SHORT_NAME:
				return MONTH_SHORT_NAME_EDEFAULT == null ? monthShortName != null : !MONTH_SHORT_NAME_EDEFAULT.equals(monthShortName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT;
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		String result = Integer.toString(getYear());
		if(getMonth() > 0) {
			result += "-" + String.format("%02d", getMonth());

			if(getDay() > 0) {
				result += "-" + String.format("%02d", getDay());
			}
		}
		return result;
	}

} //PublishedDateImpl
