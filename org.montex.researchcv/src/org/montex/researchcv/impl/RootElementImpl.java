/**
 */
package org.montex.researchcv.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.montex.researchcv.ResearchCVPackage;
import org.montex.researchcv.RootElement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Root Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class RootElementImpl extends MinimalEObjectImpl.Container implements RootElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RootElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ResearchCVPackage.Literals.ROOT_ELEMENT;
	}

} //RootElementImpl
