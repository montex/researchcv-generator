/**
 */
package org.montex.researchcv.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.montex.researchcv.Grant;
import org.montex.researchcv.Person;
import org.montex.researchcv.Publication;
import org.montex.researchcv.PublishedDate;
import org.montex.researchcv.ResearchCVPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Publication</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.impl.PublicationImpl#getCitekey <em>Citekey</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.PublicationImpl#getTitle <em>Title</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.PublicationImpl#getAuthors <em>Authors</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.PublicationImpl#getURL <em>URL</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.PublicationImpl#isPublished <em>Published</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.PublicationImpl#isOpenAccess <em>Open Access</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.PublicationImpl#getDate <em>Date</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.PublicationImpl#getDOI <em>DOI</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.PublicationImpl#getAbstract <em>Abstract</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.PublicationImpl#isWithAuthorVersion <em>With Author Version</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.PublicationImpl#getNotes <em>Notes</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.PublicationImpl#getRelatedProjects <em>Related Projects</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class PublicationImpl extends MinimalEObjectImpl.Container implements Publication {
	/**
	 * The default value of the '{@link #getCitekey() <em>Citekey</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCitekey()
	 * @generated
	 * @ordered
	 */
	protected static final String CITEKEY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCitekey() <em>Citekey</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCitekey()
	 * @generated
	 * @ordered
	 */
	protected String citekey = CITEKEY_EDEFAULT;

	/**
	 * The default value of the '{@link #getTitle() <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTitle()
	 * @generated
	 * @ordered
	 */
	protected static final String TITLE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTitle() <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTitle()
	 * @generated
	 * @ordered
	 */
	protected String title = TITLE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAuthors() <em>Authors</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAuthors()
	 * @generated
	 * @ordered
	 */
	protected EList<Person> authors;

	/**
	 * The default value of the '{@link #getURL() <em>URL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getURL()
	 * @generated
	 * @ordered
	 */
	protected static final String URL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getURL() <em>URL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getURL()
	 * @generated
	 * @ordered
	 */
	protected String url = URL_EDEFAULT;

	/**
	 * The default value of the '{@link #isPublished() <em>Published</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPublished()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PUBLISHED_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isPublished() <em>Published</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPublished()
	 * @generated
	 * @ordered
	 */
	protected boolean published = PUBLISHED_EDEFAULT;

	/**
	 * The default value of the '{@link #isOpenAccess() <em>Open Access</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOpenAccess()
	 * @generated
	 * @ordered
	 */
	protected static final boolean OPEN_ACCESS_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isOpenAccess() <em>Open Access</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOpenAccess()
	 * @generated
	 * @ordered
	 */
	protected boolean openAccess = OPEN_ACCESS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDate() <em>Date</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDate()
	 * @generated
	 * @ordered
	 */
	protected PublishedDate date;

	/**
	 * The default value of the '{@link #getDOI() <em>DOI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDOI()
	 * @generated
	 * @ordered
	 */
	protected static final String DOI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDOI() <em>DOI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDOI()
	 * @generated
	 * @ordered
	 */
	protected String doi = DOI_EDEFAULT;

	/**
	 * The default value of the '{@link #getAbstract() <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAbstract()
	 * @generated
	 * @ordered
	 */
	protected static final String ABSTRACT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAbstract() <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAbstract()
	 * @generated
	 * @ordered
	 */
	protected String abstract_ = ABSTRACT_EDEFAULT;

	/**
	 * The default value of the '{@link #isWithAuthorVersion() <em>With Author Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isWithAuthorVersion()
	 * @generated
	 * @ordered
	 */
	protected static final boolean WITH_AUTHOR_VERSION_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isWithAuthorVersion() <em>With Author Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isWithAuthorVersion()
	 * @generated
	 * @ordered
	 */
	protected boolean withAuthorVersion = WITH_AUTHOR_VERSION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getNotes() <em>Notes</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNotes()
	 * @generated
	 * @ordered
	 */
	protected EList<String> notes;

	/**
	 * The cached value of the '{@link #getRelatedProjects() <em>Related Projects</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelatedProjects()
	 * @generated
	 * @ordered
	 */
	protected EList<Grant> relatedProjects;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PublicationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ResearchCVPackage.Literals.PUBLICATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCitekey() {
		return citekey;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCitekey(String newCitekey) {
		String oldCitekey = citekey;
		citekey = newCitekey;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.PUBLICATION__CITEKEY, oldCitekey, citekey));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getTitle() {
		return title;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTitle(String newTitle) {
		String oldTitle = title;
		title = newTitle;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.PUBLICATION__TITLE, oldTitle, title));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Person> getAuthors() {
		if (authors == null) {
			authors = new EObjectResolvingEList<Person>(Person.class, this, ResearchCVPackage.PUBLICATION__AUTHORS);
		}
		return authors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getURL() {
		return url;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setURL(String newURL) {
		String oldURL = url;
		url = newURL;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.PUBLICATION__URL, oldURL, url));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isPublished() {
		return published;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPublished(boolean newPublished) {
		boolean oldPublished = published;
		published = newPublished;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.PUBLICATION__PUBLISHED, oldPublished, published));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isOpenAccess() {
		return openAccess;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOpenAccess(boolean newOpenAccess) {
		boolean oldOpenAccess = openAccess;
		openAccess = newOpenAccess;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.PUBLICATION__OPEN_ACCESS, oldOpenAccess, openAccess));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PublishedDate getDate() {
		return date;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDate(PublishedDate newDate, NotificationChain msgs) {
		PublishedDate oldDate = date;
		date = newDate;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ResearchCVPackage.PUBLICATION__DATE, oldDate, newDate);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDate(PublishedDate newDate) {
		if (newDate != date) {
			NotificationChain msgs = null;
			if (date != null)
				msgs = ((InternalEObject)date).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ResearchCVPackage.PUBLICATION__DATE, null, msgs);
			if (newDate != null)
				msgs = ((InternalEObject)newDate).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ResearchCVPackage.PUBLICATION__DATE, null, msgs);
			msgs = basicSetDate(newDate, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.PUBLICATION__DATE, newDate, newDate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDOI() {
		return doi;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDOI(String newDOI) {
		String oldDOI = doi;
		doi = newDOI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.PUBLICATION__DOI, oldDOI, doi));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getAbstract() {
		return abstract_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAbstract(String newAbstract) {
		String oldAbstract = abstract_;
		abstract_ = newAbstract;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.PUBLICATION__ABSTRACT, oldAbstract, abstract_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isWithAuthorVersion() {
		return withAuthorVersion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setWithAuthorVersion(boolean newWithAuthorVersion) {
		boolean oldWithAuthorVersion = withAuthorVersion;
		withAuthorVersion = newWithAuthorVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.PUBLICATION__WITH_AUTHOR_VERSION, oldWithAuthorVersion, withAuthorVersion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getNotes() {
		if (notes == null) {
			notes = new EDataTypeUniqueEList<String>(String.class, this, ResearchCVPackage.PUBLICATION__NOTES);
		}
		return notes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Grant> getRelatedProjects() {
		if (relatedProjects == null) {
			relatedProjects = new EObjectWithInverseResolvingEList.ManyInverse<Grant>(Grant.class, this, ResearchCVPackage.PUBLICATION__RELATED_PROJECTS, ResearchCVPackage.GRANT__RELATED_PUBLICATIONS);
		}
		return relatedProjects;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ResearchCVPackage.PUBLICATION__RELATED_PROJECTS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getRelatedProjects()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ResearchCVPackage.PUBLICATION__DATE:
				return basicSetDate(null, msgs);
			case ResearchCVPackage.PUBLICATION__RELATED_PROJECTS:
				return ((InternalEList<?>)getRelatedProjects()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ResearchCVPackage.PUBLICATION__CITEKEY:
				return getCitekey();
			case ResearchCVPackage.PUBLICATION__TITLE:
				return getTitle();
			case ResearchCVPackage.PUBLICATION__AUTHORS:
				return getAuthors();
			case ResearchCVPackage.PUBLICATION__URL:
				return getURL();
			case ResearchCVPackage.PUBLICATION__PUBLISHED:
				return isPublished();
			case ResearchCVPackage.PUBLICATION__OPEN_ACCESS:
				return isOpenAccess();
			case ResearchCVPackage.PUBLICATION__DATE:
				return getDate();
			case ResearchCVPackage.PUBLICATION__DOI:
				return getDOI();
			case ResearchCVPackage.PUBLICATION__ABSTRACT:
				return getAbstract();
			case ResearchCVPackage.PUBLICATION__WITH_AUTHOR_VERSION:
				return isWithAuthorVersion();
			case ResearchCVPackage.PUBLICATION__NOTES:
				return getNotes();
			case ResearchCVPackage.PUBLICATION__RELATED_PROJECTS:
				return getRelatedProjects();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ResearchCVPackage.PUBLICATION__CITEKEY:
				setCitekey((String)newValue);
				return;
			case ResearchCVPackage.PUBLICATION__TITLE:
				setTitle((String)newValue);
				return;
			case ResearchCVPackage.PUBLICATION__AUTHORS:
				getAuthors().clear();
				getAuthors().addAll((Collection<? extends Person>)newValue);
				return;
			case ResearchCVPackage.PUBLICATION__URL:
				setURL((String)newValue);
				return;
			case ResearchCVPackage.PUBLICATION__PUBLISHED:
				setPublished((Boolean)newValue);
				return;
			case ResearchCVPackage.PUBLICATION__OPEN_ACCESS:
				setOpenAccess((Boolean)newValue);
				return;
			case ResearchCVPackage.PUBLICATION__DATE:
				setDate((PublishedDate)newValue);
				return;
			case ResearchCVPackage.PUBLICATION__DOI:
				setDOI((String)newValue);
				return;
			case ResearchCVPackage.PUBLICATION__ABSTRACT:
				setAbstract((String)newValue);
				return;
			case ResearchCVPackage.PUBLICATION__WITH_AUTHOR_VERSION:
				setWithAuthorVersion((Boolean)newValue);
				return;
			case ResearchCVPackage.PUBLICATION__NOTES:
				getNotes().clear();
				getNotes().addAll((Collection<? extends String>)newValue);
				return;
			case ResearchCVPackage.PUBLICATION__RELATED_PROJECTS:
				getRelatedProjects().clear();
				getRelatedProjects().addAll((Collection<? extends Grant>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.PUBLICATION__CITEKEY:
				setCitekey(CITEKEY_EDEFAULT);
				return;
			case ResearchCVPackage.PUBLICATION__TITLE:
				setTitle(TITLE_EDEFAULT);
				return;
			case ResearchCVPackage.PUBLICATION__AUTHORS:
				getAuthors().clear();
				return;
			case ResearchCVPackage.PUBLICATION__URL:
				setURL(URL_EDEFAULT);
				return;
			case ResearchCVPackage.PUBLICATION__PUBLISHED:
				setPublished(PUBLISHED_EDEFAULT);
				return;
			case ResearchCVPackage.PUBLICATION__OPEN_ACCESS:
				setOpenAccess(OPEN_ACCESS_EDEFAULT);
				return;
			case ResearchCVPackage.PUBLICATION__DATE:
				setDate((PublishedDate)null);
				return;
			case ResearchCVPackage.PUBLICATION__DOI:
				setDOI(DOI_EDEFAULT);
				return;
			case ResearchCVPackage.PUBLICATION__ABSTRACT:
				setAbstract(ABSTRACT_EDEFAULT);
				return;
			case ResearchCVPackage.PUBLICATION__WITH_AUTHOR_VERSION:
				setWithAuthorVersion(WITH_AUTHOR_VERSION_EDEFAULT);
				return;
			case ResearchCVPackage.PUBLICATION__NOTES:
				getNotes().clear();
				return;
			case ResearchCVPackage.PUBLICATION__RELATED_PROJECTS:
				getRelatedProjects().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.PUBLICATION__CITEKEY:
				return CITEKEY_EDEFAULT == null ? citekey != null : !CITEKEY_EDEFAULT.equals(citekey);
			case ResearchCVPackage.PUBLICATION__TITLE:
				return TITLE_EDEFAULT == null ? title != null : !TITLE_EDEFAULT.equals(title);
			case ResearchCVPackage.PUBLICATION__AUTHORS:
				return authors != null && !authors.isEmpty();
			case ResearchCVPackage.PUBLICATION__URL:
				return URL_EDEFAULT == null ? url != null : !URL_EDEFAULT.equals(url);
			case ResearchCVPackage.PUBLICATION__PUBLISHED:
				return published != PUBLISHED_EDEFAULT;
			case ResearchCVPackage.PUBLICATION__OPEN_ACCESS:
				return openAccess != OPEN_ACCESS_EDEFAULT;
			case ResearchCVPackage.PUBLICATION__DATE:
				return date != null;
			case ResearchCVPackage.PUBLICATION__DOI:
				return DOI_EDEFAULT == null ? doi != null : !DOI_EDEFAULT.equals(doi);
			case ResearchCVPackage.PUBLICATION__ABSTRACT:
				return ABSTRACT_EDEFAULT == null ? abstract_ != null : !ABSTRACT_EDEFAULT.equals(abstract_);
			case ResearchCVPackage.PUBLICATION__WITH_AUTHOR_VERSION:
				return withAuthorVersion != WITH_AUTHOR_VERSION_EDEFAULT;
			case ResearchCVPackage.PUBLICATION__NOTES:
				return notes != null && !notes.isEmpty();
			case ResearchCVPackage.PUBLICATION__RELATED_PROJECTS:
				return relatedProjects != null && !relatedProjects.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (citekey: ");
		result.append(citekey);
		result.append(", title: ");
		result.append(title);
		result.append(", URL: ");
		result.append(url);
		result.append(", published: ");
		result.append(published);
		result.append(", openAccess: ");
		result.append(openAccess);
		result.append(", DOI: ");
		result.append(doi);
		result.append(", abstract: ");
		result.append(abstract_);
		result.append(", withAuthorVersion: ");
		result.append(withAuthorVersion);
		result.append(", notes: ");
		result.append(notes);
		result.append(')');
		return result.toString();
	}

} //PublicationImpl
