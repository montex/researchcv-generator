/**
 */
package org.montex.researchcv.impl;

import java.util.Collection;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.montex.researchcv.Person;
import org.montex.researchcv.ResearchCVPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Person</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.impl.PersonImpl#getFirstNames <em>First Names</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.PersonImpl#getLastNames <em>Last Names</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.PersonImpl#getName <em>Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PersonImpl extends MinimalEObjectImpl.Container implements Person {
	/**
	 * The cached value of the '{@link #getFirstNames() <em>First Names</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirstNames()
	 * @generated
	 * @ordered
	 */
	protected EList<String> firstNames;

	/**
	 * The cached value of the '{@link #getLastNames() <em>Last Names</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastNames()
	 * @generated
	 * @ordered
	 */
	protected EList<String> lastNames;

	/**
	 * The cached setting delegate for the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature.Internal.SettingDelegate NAME__ESETTING_DELEGATE = ((EStructuralFeature.Internal)ResearchCVPackage.Literals.PERSON__NAME).getSettingDelegate();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PersonImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ResearchCVPackage.Literals.PERSON;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getFirstNames() {
		if (firstNames == null) {
			firstNames = new EDataTypeEList<String>(String.class, this, ResearchCVPackage.PERSON__FIRST_NAMES);
		}
		return firstNames;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getLastNames() {
		if (lastNames == null) {
			lastNames = new EDataTypeEList<String>(String.class, this, ResearchCVPackage.PERSON__LAST_NAMES);
		}
		return lastNames;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return (String)NAME__ESETTING_DELEGATE.dynamicGet(this, null, 0, true, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		NAME__ESETTING_DELEGATE.dynamicSet(this, null, 0, newName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ResearchCVPackage.PERSON__FIRST_NAMES:
				return getFirstNames();
			case ResearchCVPackage.PERSON__LAST_NAMES:
				return getLastNames();
			case ResearchCVPackage.PERSON__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ResearchCVPackage.PERSON__FIRST_NAMES:
				getFirstNames().clear();
				getFirstNames().addAll((Collection<? extends String>)newValue);
				return;
			case ResearchCVPackage.PERSON__LAST_NAMES:
				getLastNames().clear();
				getLastNames().addAll((Collection<? extends String>)newValue);
				return;
			case ResearchCVPackage.PERSON__NAME:
				setName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.PERSON__FIRST_NAMES:
				getFirstNames().clear();
				return;
			case ResearchCVPackage.PERSON__LAST_NAMES:
				getLastNames().clear();
				return;
			case ResearchCVPackage.PERSON__NAME:
				NAME__ESETTING_DELEGATE.dynamicUnset(this, null, 0);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.PERSON__FIRST_NAMES:
				return firstNames != null && !firstNames.isEmpty();
			case ResearchCVPackage.PERSON__LAST_NAMES:
				return lastNames != null && !lastNames.isEmpty();
			case ResearchCVPackage.PERSON__NAME:
				return NAME__ESETTING_DELEGATE.dynamicIsSet(this, null, 0);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (firstNames: ");
		result.append(firstNames);
		result.append(", lastNames: ");
		result.append(lastNames);
		result.append(')');
		return result.toString();
	}

} //PersonImpl
