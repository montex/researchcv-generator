/**
 */
package org.montex.researchcv.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.montex.researchcv.Journal;
import org.montex.researchcv.ResearchCVPackage;
import org.montex.researchcv.SerialNumber;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Journal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.impl.JournalImpl#getJournalName <em>Journal Name</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.JournalImpl#getPublisher <em>Publisher</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.JournalImpl#getFirstPage <em>First Page</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.JournalImpl#getLastPage <em>Last Page</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.JournalImpl#getVolume <em>Volume</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.JournalImpl#getIssue <em>Issue</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.JournalImpl#getISSN <em>ISSN</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.JournalImpl#isMagazine <em>Magazine</em>}</li>
 * </ul>
 *
 * @generated
 */
public class JournalImpl extends PublicationImpl implements Journal {
	/**
	 * The default value of the '{@link #getJournalName() <em>Journal Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJournalName()
	 * @generated
	 * @ordered
	 */
	protected static final String JOURNAL_NAME_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getJournalName() <em>Journal Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJournalName()
	 * @generated
	 * @ordered
	 */
	protected String journalName = JOURNAL_NAME_EDEFAULT;
	/**
	 * The default value of the '{@link #getPublisher() <em>Publisher</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPublisher()
	 * @generated
	 * @ordered
	 */
	protected static final String PUBLISHER_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getPublisher() <em>Publisher</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPublisher()
	 * @generated
	 * @ordered
	 */
	protected String publisher = PUBLISHER_EDEFAULT;
	/**
	 * The default value of the '{@link #getFirstPage() <em>First Page</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirstPage()
	 * @generated
	 * @ordered
	 */
	protected static final String FIRST_PAGE_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getFirstPage() <em>First Page</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirstPage()
	 * @generated
	 * @ordered
	 */
	protected String firstPage = FIRST_PAGE_EDEFAULT;
	/**
	 * The default value of the '{@link #getLastPage() <em>Last Page</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastPage()
	 * @generated
	 * @ordered
	 */
	protected static final String LAST_PAGE_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getLastPage() <em>Last Page</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastPage()
	 * @generated
	 * @ordered
	 */
	protected String lastPage = LAST_PAGE_EDEFAULT;
	/**
	 * The default value of the '{@link #getVolume() <em>Volume</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVolume()
	 * @generated
	 * @ordered
	 */
	protected static final int VOLUME_EDEFAULT = 0;
	/**
	 * The cached value of the '{@link #getVolume() <em>Volume</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVolume()
	 * @generated
	 * @ordered
	 */
	protected int volume = VOLUME_EDEFAULT;
	/**
	 * The default value of the '{@link #getIssue() <em>Issue</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIssue()
	 * @generated
	 * @ordered
	 */
	protected static final int ISSUE_EDEFAULT = 0;
	/**
	 * The cached value of the '{@link #getIssue() <em>Issue</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIssue()
	 * @generated
	 * @ordered
	 */
	protected int issue = ISSUE_EDEFAULT;
	/**
	 * The cached value of the '{@link #getISSN() <em>ISSN</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getISSN()
	 * @generated
	 * @ordered
	 */
	protected EList<SerialNumber> issn;

	/**
	 * The default value of the '{@link #isMagazine() <em>Magazine</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isMagazine()
	 * @generated
	 * @ordered
	 */
	protected static final boolean MAGAZINE_EDEFAULT = false;
	/**
	 * The cached value of the '{@link #isMagazine() <em>Magazine</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isMagazine()
	 * @generated
	 * @ordered
	 */
	protected boolean magazine = MAGAZINE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JournalImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ResearchCVPackage.Literals.JOURNAL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getJournalName() {
		return journalName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setJournalName(String newJournalName) {
		String oldJournalName = journalName;
		journalName = newJournalName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.JOURNAL__JOURNAL_NAME, oldJournalName, journalName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getPublisher() {
		return publisher;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPublisher(String newPublisher) {
		String oldPublisher = publisher;
		publisher = newPublisher;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.JOURNAL__PUBLISHER, oldPublisher, publisher));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getFirstPage() {
		return firstPage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFirstPage(String newFirstPage) {
		String oldFirstPage = firstPage;
		firstPage = newFirstPage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.JOURNAL__FIRST_PAGE, oldFirstPage, firstPage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLastPage() {
		return lastPage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLastPage(String newLastPage) {
		String oldLastPage = lastPage;
		lastPage = newLastPage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.JOURNAL__LAST_PAGE, oldLastPage, lastPage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getVolume() {
		return volume;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVolume(int newVolume) {
		int oldVolume = volume;
		volume = newVolume;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.JOURNAL__VOLUME, oldVolume, volume));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getIssue() {
		return issue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIssue(int newIssue) {
		int oldIssue = issue;
		issue = newIssue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.JOURNAL__ISSUE, oldIssue, issue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SerialNumber> getISSN() {
		if (issn == null) {
			issn = new EObjectContainmentEList<SerialNumber>(SerialNumber.class, this, ResearchCVPackage.JOURNAL__ISSN);
		}
		return issn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isMagazine() {
		return magazine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMagazine(boolean newMagazine) {
		boolean oldMagazine = magazine;
		magazine = newMagazine;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.JOURNAL__MAGAZINE, oldMagazine, magazine));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ResearchCVPackage.JOURNAL__ISSN:
				return ((InternalEList<?>)getISSN()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ResearchCVPackage.JOURNAL__JOURNAL_NAME:
				return getJournalName();
			case ResearchCVPackage.JOURNAL__PUBLISHER:
				return getPublisher();
			case ResearchCVPackage.JOURNAL__FIRST_PAGE:
				return getFirstPage();
			case ResearchCVPackage.JOURNAL__LAST_PAGE:
				return getLastPage();
			case ResearchCVPackage.JOURNAL__VOLUME:
				return getVolume();
			case ResearchCVPackage.JOURNAL__ISSUE:
				return getIssue();
			case ResearchCVPackage.JOURNAL__ISSN:
				return getISSN();
			case ResearchCVPackage.JOURNAL__MAGAZINE:
				return isMagazine();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ResearchCVPackage.JOURNAL__JOURNAL_NAME:
				setJournalName((String)newValue);
				return;
			case ResearchCVPackage.JOURNAL__PUBLISHER:
				setPublisher((String)newValue);
				return;
			case ResearchCVPackage.JOURNAL__FIRST_PAGE:
				setFirstPage((String)newValue);
				return;
			case ResearchCVPackage.JOURNAL__LAST_PAGE:
				setLastPage((String)newValue);
				return;
			case ResearchCVPackage.JOURNAL__VOLUME:
				setVolume((Integer)newValue);
				return;
			case ResearchCVPackage.JOURNAL__ISSUE:
				setIssue((Integer)newValue);
				return;
			case ResearchCVPackage.JOURNAL__ISSN:
				getISSN().clear();
				getISSN().addAll((Collection<? extends SerialNumber>)newValue);
				return;
			case ResearchCVPackage.JOURNAL__MAGAZINE:
				setMagazine((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.JOURNAL__JOURNAL_NAME:
				setJournalName(JOURNAL_NAME_EDEFAULT);
				return;
			case ResearchCVPackage.JOURNAL__PUBLISHER:
				setPublisher(PUBLISHER_EDEFAULT);
				return;
			case ResearchCVPackage.JOURNAL__FIRST_PAGE:
				setFirstPage(FIRST_PAGE_EDEFAULT);
				return;
			case ResearchCVPackage.JOURNAL__LAST_PAGE:
				setLastPage(LAST_PAGE_EDEFAULT);
				return;
			case ResearchCVPackage.JOURNAL__VOLUME:
				setVolume(VOLUME_EDEFAULT);
				return;
			case ResearchCVPackage.JOURNAL__ISSUE:
				setIssue(ISSUE_EDEFAULT);
				return;
			case ResearchCVPackage.JOURNAL__ISSN:
				getISSN().clear();
				return;
			case ResearchCVPackage.JOURNAL__MAGAZINE:
				setMagazine(MAGAZINE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.JOURNAL__JOURNAL_NAME:
				return JOURNAL_NAME_EDEFAULT == null ? journalName != null : !JOURNAL_NAME_EDEFAULT.equals(journalName);
			case ResearchCVPackage.JOURNAL__PUBLISHER:
				return PUBLISHER_EDEFAULT == null ? publisher != null : !PUBLISHER_EDEFAULT.equals(publisher);
			case ResearchCVPackage.JOURNAL__FIRST_PAGE:
				return FIRST_PAGE_EDEFAULT == null ? firstPage != null : !FIRST_PAGE_EDEFAULT.equals(firstPage);
			case ResearchCVPackage.JOURNAL__LAST_PAGE:
				return LAST_PAGE_EDEFAULT == null ? lastPage != null : !LAST_PAGE_EDEFAULT.equals(lastPage);
			case ResearchCVPackage.JOURNAL__VOLUME:
				return volume != VOLUME_EDEFAULT;
			case ResearchCVPackage.JOURNAL__ISSUE:
				return issue != ISSUE_EDEFAULT;
			case ResearchCVPackage.JOURNAL__ISSN:
				return issn != null && !issn.isEmpty();
			case ResearchCVPackage.JOURNAL__MAGAZINE:
				return magazine != MAGAZINE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (journalName: ");
		result.append(journalName);
		result.append(", publisher: ");
		result.append(publisher);
		result.append(", firstPage: ");
		result.append(firstPage);
		result.append(", lastPage: ");
		result.append(lastPage);
		result.append(", volume: ");
		result.append(volume);
		result.append(", issue: ");
		result.append(issue);
		result.append(", magazine: ");
		result.append(magazine);
		result.append(')');
		return result.toString();
	}

} //JournalImpl
