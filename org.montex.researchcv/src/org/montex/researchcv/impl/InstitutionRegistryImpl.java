/**
 */
package org.montex.researchcv.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.montex.researchcv.Institution;
import org.montex.researchcv.InstitutionRegistry;
import org.montex.researchcv.ResearchCVPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Institution Registry</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.impl.InstitutionRegistryImpl#getInstitutions <em>Institutions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InstitutionRegistryImpl extends RootElementImpl implements InstitutionRegistry {
	/**
	 * The cached value of the '{@link #getInstitutions() <em>Institutions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstitutions()
	 * @generated
	 * @ordered
	 */
	protected EList<Institution> institutions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InstitutionRegistryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ResearchCVPackage.Literals.INSTITUTION_REGISTRY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Institution> getInstitutions() {
		if (institutions == null) {
			institutions = new EObjectContainmentEList<Institution>(Institution.class, this, ResearchCVPackage.INSTITUTION_REGISTRY__INSTITUTIONS);
		}
		return institutions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ResearchCVPackage.INSTITUTION_REGISTRY__INSTITUTIONS:
				return ((InternalEList<?>)getInstitutions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ResearchCVPackage.INSTITUTION_REGISTRY__INSTITUTIONS:
				return getInstitutions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ResearchCVPackage.INSTITUTION_REGISTRY__INSTITUTIONS:
				getInstitutions().clear();
				getInstitutions().addAll((Collection<? extends Institution>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.INSTITUTION_REGISTRY__INSTITUTIONS:
				getInstitutions().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.INSTITUTION_REGISTRY__INSTITUTIONS:
				return institutions != null && !institutions.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //InstitutionRegistryImpl
