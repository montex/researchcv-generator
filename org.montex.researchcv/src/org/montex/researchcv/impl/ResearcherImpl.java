/**
 */
package org.montex.researchcv.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.montex.researchcv.Contact;
import org.montex.researchcv.Course;
import org.montex.researchcv.Grant;
import org.montex.researchcv.Person;
import org.montex.researchcv.Publication;
import org.montex.researchcv.ResearchCVPackage;
import org.montex.researchcv.ResearchGroup;
import org.montex.researchcv.ResearchTopic;
import org.montex.researchcv.Researcher;
import org.montex.researchcv.SocialProfile;
import org.montex.researchcv.Supervision;
import org.montex.researchcv.SupervisionsRegistry;
import org.montex.researchcv.TaughtCourse;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Researcher</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.impl.ResearcherImpl#getCoauthors <em>Coauthors</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.ResearcherImpl#getContacts <em>Contacts</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.ResearcherImpl#getSocials <em>Socials</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.ResearcherImpl#getPublications <em>Publications</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.ResearcherImpl#getTeachingMaterial <em>Teaching Material</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.ResearcherImpl#getOtherMaterial <em>Other Material</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.ResearcherImpl#getResearchGroup <em>Research Group</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.ResearcherImpl#getTopics <em>Topics</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.ResearcherImpl#getGrants <em>Grants</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.ResearcherImpl#getCourses <em>Courses</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.ResearcherImpl#getTaughtCourses <em>Taught Courses</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.ResearcherImpl#getSupervisionsRegistry <em>Supervisions Registry</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.ResearcherImpl#getSupervisions <em>Supervisions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ResearcherImpl extends PersonImpl implements Researcher {
	/**
	 * The cached setting delegate for the '{@link #getCoauthors() <em>Coauthors</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCoauthors()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature.Internal.SettingDelegate COAUTHORS__ESETTING_DELEGATE = ((EStructuralFeature.Internal)ResearchCVPackage.Literals.RESEARCHER__COAUTHORS).getSettingDelegate();

	/**
	 * The cached value of the '{@link #getContacts() <em>Contacts</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContacts()
	 * @generated
	 * @ordered
	 */
	protected EList<Contact> contacts;

	/**
	 * The cached value of the '{@link #getSocials() <em>Socials</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSocials()
	 * @generated
	 * @ordered
	 */
	protected EList<SocialProfile> socials;

	/**
	 * The cached value of the '{@link #getPublications() <em>Publications</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPublications()
	 * @generated
	 * @ordered
	 */
	protected EList<Publication> publications;

	/**
	 * The cached value of the '{@link #getTeachingMaterial() <em>Teaching Material</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTeachingMaterial()
	 * @generated
	 * @ordered
	 */
	protected EList<Publication> teachingMaterial;

	/**
	 * The cached value of the '{@link #getOtherMaterial() <em>Other Material</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOtherMaterial()
	 * @generated
	 * @ordered
	 */
	protected EList<Publication> otherMaterial;

	/**
	 * The cached value of the '{@link #getResearchGroup() <em>Research Group</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResearchGroup()
	 * @generated
	 * @ordered
	 */
	protected ResearchGroup researchGroup;

	/**
	 * The cached value of the '{@link #getTopics() <em>Topics</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTopics()
	 * @generated
	 * @ordered
	 */
	protected EList<ResearchTopic> topics;

	/**
	 * The cached value of the '{@link #getGrants() <em>Grants</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGrants()
	 * @generated
	 * @ordered
	 */
	protected EList<Grant> grants;

	/**
	 * The cached value of the '{@link #getCourses() <em>Courses</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourses()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> courses;

	/**
	 * The cached value of the '{@link #getTaughtCourses() <em>Taught Courses</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaughtCourses()
	 * @generated
	 * @ordered
	 */
	protected EList<TaughtCourse> taughtCourses;

	/**
	 * The cached value of the '{@link #getSupervisionsRegistry() <em>Supervisions Registry</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSupervisionsRegistry()
	 * @generated
	 * @ordered
	 */
	protected SupervisionsRegistry supervisionsRegistry;

	/**
	 * The cached setting delegate for the '{@link #getSupervisions() <em>Supervisions</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSupervisions()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature.Internal.SettingDelegate SUPERVISIONS__ESETTING_DELEGATE = ((EStructuralFeature.Internal)ResearchCVPackage.Literals.RESEARCHER__SUPERVISIONS).getSettingDelegate();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResearcherImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ResearchCVPackage.Literals.RESEARCHER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public EList<Person> getCoauthors() {
		return (EList<Person>)COAUTHORS__ESETTING_DELEGATE.dynamicGet(this, null, 0, true, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Contact> getContacts() {
		if (contacts == null) {
			contacts = new EObjectContainmentEList<Contact>(Contact.class, this, ResearchCVPackage.RESEARCHER__CONTACTS);
		}
		return contacts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SocialProfile> getSocials() {
		if (socials == null) {
			socials = new EObjectContainmentEList<SocialProfile>(SocialProfile.class, this, ResearchCVPackage.RESEARCHER__SOCIALS);
		}
		return socials;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Publication> getPublications() {
		if (publications == null) {
			publications = new EObjectContainmentEList<Publication>(Publication.class, this, ResearchCVPackage.RESEARCHER__PUBLICATIONS);
		}
		return publications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Publication> getTeachingMaterial() {
		if (teachingMaterial == null) {
			teachingMaterial = new EObjectContainmentEList<Publication>(Publication.class, this, ResearchCVPackage.RESEARCHER__TEACHING_MATERIAL);
		}
		return teachingMaterial;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Publication> getOtherMaterial() {
		if (otherMaterial == null) {
			otherMaterial = new EObjectContainmentEList<Publication>(Publication.class, this, ResearchCVPackage.RESEARCHER__OTHER_MATERIAL);
		}
		return otherMaterial;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResearchGroup getResearchGroup() {
		return researchGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetResearchGroup(ResearchGroup newResearchGroup, NotificationChain msgs) {
		ResearchGroup oldResearchGroup = researchGroup;
		researchGroup = newResearchGroup;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ResearchCVPackage.RESEARCHER__RESEARCH_GROUP, oldResearchGroup, newResearchGroup);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setResearchGroup(ResearchGroup newResearchGroup) {
		if (newResearchGroup != researchGroup) {
			NotificationChain msgs = null;
			if (researchGroup != null)
				msgs = ((InternalEObject)researchGroup).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ResearchCVPackage.RESEARCHER__RESEARCH_GROUP, null, msgs);
			if (newResearchGroup != null)
				msgs = ((InternalEObject)newResearchGroup).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ResearchCVPackage.RESEARCHER__RESEARCH_GROUP, null, msgs);
			msgs = basicSetResearchGroup(newResearchGroup, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.RESEARCHER__RESEARCH_GROUP, newResearchGroup, newResearchGroup));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ResearchTopic> getTopics() {
		if (topics == null) {
			topics = new EObjectContainmentEList<ResearchTopic>(ResearchTopic.class, this, ResearchCVPackage.RESEARCHER__TOPICS);
		}
		return topics;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Grant> getGrants() {
		if (grants == null) {
			grants = new EObjectContainmentEList<Grant>(Grant.class, this, ResearchCVPackage.RESEARCHER__GRANTS);
		}
		return grants;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Course> getCourses() {
		if (courses == null) {
			courses = new EObjectContainmentEList<Course>(Course.class, this, ResearchCVPackage.RESEARCHER__COURSES);
		}
		return courses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<TaughtCourse> getTaughtCourses() {
		if (taughtCourses == null) {
			taughtCourses = new EObjectContainmentEList<TaughtCourse>(TaughtCourse.class, this, ResearchCVPackage.RESEARCHER__TAUGHT_COURSES);
		}
		return taughtCourses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SupervisionsRegistry getSupervisionsRegistry() {
		if (supervisionsRegistry != null && supervisionsRegistry.eIsProxy()) {
			InternalEObject oldSupervisionsRegistry = (InternalEObject)supervisionsRegistry;
			supervisionsRegistry = (SupervisionsRegistry)eResolveProxy(oldSupervisionsRegistry);
			if (supervisionsRegistry != oldSupervisionsRegistry) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ResearchCVPackage.RESEARCHER__SUPERVISIONS_REGISTRY, oldSupervisionsRegistry, supervisionsRegistry));
			}
		}
		return supervisionsRegistry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SupervisionsRegistry basicGetSupervisionsRegistry() {
		return supervisionsRegistry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSupervisionsRegistry(SupervisionsRegistry newSupervisionsRegistry) {
		SupervisionsRegistry oldSupervisionsRegistry = supervisionsRegistry;
		supervisionsRegistry = newSupervisionsRegistry;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.RESEARCHER__SUPERVISIONS_REGISTRY, oldSupervisionsRegistry, supervisionsRegistry));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public EList<Supervision> getSupervisions() {
		return (EList<Supervision>)SUPERVISIONS__ESETTING_DELEGATE.dynamicGet(this, null, 0, true, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ResearchCVPackage.RESEARCHER__CONTACTS:
				return ((InternalEList<?>)getContacts()).basicRemove(otherEnd, msgs);
			case ResearchCVPackage.RESEARCHER__SOCIALS:
				return ((InternalEList<?>)getSocials()).basicRemove(otherEnd, msgs);
			case ResearchCVPackage.RESEARCHER__PUBLICATIONS:
				return ((InternalEList<?>)getPublications()).basicRemove(otherEnd, msgs);
			case ResearchCVPackage.RESEARCHER__TEACHING_MATERIAL:
				return ((InternalEList<?>)getTeachingMaterial()).basicRemove(otherEnd, msgs);
			case ResearchCVPackage.RESEARCHER__OTHER_MATERIAL:
				return ((InternalEList<?>)getOtherMaterial()).basicRemove(otherEnd, msgs);
			case ResearchCVPackage.RESEARCHER__RESEARCH_GROUP:
				return basicSetResearchGroup(null, msgs);
			case ResearchCVPackage.RESEARCHER__TOPICS:
				return ((InternalEList<?>)getTopics()).basicRemove(otherEnd, msgs);
			case ResearchCVPackage.RESEARCHER__GRANTS:
				return ((InternalEList<?>)getGrants()).basicRemove(otherEnd, msgs);
			case ResearchCVPackage.RESEARCHER__COURSES:
				return ((InternalEList<?>)getCourses()).basicRemove(otherEnd, msgs);
			case ResearchCVPackage.RESEARCHER__TAUGHT_COURSES:
				return ((InternalEList<?>)getTaughtCourses()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ResearchCVPackage.RESEARCHER__COAUTHORS:
				return getCoauthors();
			case ResearchCVPackage.RESEARCHER__CONTACTS:
				return getContacts();
			case ResearchCVPackage.RESEARCHER__SOCIALS:
				return getSocials();
			case ResearchCVPackage.RESEARCHER__PUBLICATIONS:
				return getPublications();
			case ResearchCVPackage.RESEARCHER__TEACHING_MATERIAL:
				return getTeachingMaterial();
			case ResearchCVPackage.RESEARCHER__OTHER_MATERIAL:
				return getOtherMaterial();
			case ResearchCVPackage.RESEARCHER__RESEARCH_GROUP:
				return getResearchGroup();
			case ResearchCVPackage.RESEARCHER__TOPICS:
				return getTopics();
			case ResearchCVPackage.RESEARCHER__GRANTS:
				return getGrants();
			case ResearchCVPackage.RESEARCHER__COURSES:
				return getCourses();
			case ResearchCVPackage.RESEARCHER__TAUGHT_COURSES:
				return getTaughtCourses();
			case ResearchCVPackage.RESEARCHER__SUPERVISIONS_REGISTRY:
				if (resolve) return getSupervisionsRegistry();
				return basicGetSupervisionsRegistry();
			case ResearchCVPackage.RESEARCHER__SUPERVISIONS:
				return getSupervisions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ResearchCVPackage.RESEARCHER__COAUTHORS:
				getCoauthors().clear();
				getCoauthors().addAll((Collection<? extends Person>)newValue);
				return;
			case ResearchCVPackage.RESEARCHER__CONTACTS:
				getContacts().clear();
				getContacts().addAll((Collection<? extends Contact>)newValue);
				return;
			case ResearchCVPackage.RESEARCHER__SOCIALS:
				getSocials().clear();
				getSocials().addAll((Collection<? extends SocialProfile>)newValue);
				return;
			case ResearchCVPackage.RESEARCHER__PUBLICATIONS:
				getPublications().clear();
				getPublications().addAll((Collection<? extends Publication>)newValue);
				return;
			case ResearchCVPackage.RESEARCHER__TEACHING_MATERIAL:
				getTeachingMaterial().clear();
				getTeachingMaterial().addAll((Collection<? extends Publication>)newValue);
				return;
			case ResearchCVPackage.RESEARCHER__OTHER_MATERIAL:
				getOtherMaterial().clear();
				getOtherMaterial().addAll((Collection<? extends Publication>)newValue);
				return;
			case ResearchCVPackage.RESEARCHER__RESEARCH_GROUP:
				setResearchGroup((ResearchGroup)newValue);
				return;
			case ResearchCVPackage.RESEARCHER__TOPICS:
				getTopics().clear();
				getTopics().addAll((Collection<? extends ResearchTopic>)newValue);
				return;
			case ResearchCVPackage.RESEARCHER__GRANTS:
				getGrants().clear();
				getGrants().addAll((Collection<? extends Grant>)newValue);
				return;
			case ResearchCVPackage.RESEARCHER__COURSES:
				getCourses().clear();
				getCourses().addAll((Collection<? extends Course>)newValue);
				return;
			case ResearchCVPackage.RESEARCHER__TAUGHT_COURSES:
				getTaughtCourses().clear();
				getTaughtCourses().addAll((Collection<? extends TaughtCourse>)newValue);
				return;
			case ResearchCVPackage.RESEARCHER__SUPERVISIONS_REGISTRY:
				setSupervisionsRegistry((SupervisionsRegistry)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.RESEARCHER__COAUTHORS:
				getCoauthors().clear();
				return;
			case ResearchCVPackage.RESEARCHER__CONTACTS:
				getContacts().clear();
				return;
			case ResearchCVPackage.RESEARCHER__SOCIALS:
				getSocials().clear();
				return;
			case ResearchCVPackage.RESEARCHER__PUBLICATIONS:
				getPublications().clear();
				return;
			case ResearchCVPackage.RESEARCHER__TEACHING_MATERIAL:
				getTeachingMaterial().clear();
				return;
			case ResearchCVPackage.RESEARCHER__OTHER_MATERIAL:
				getOtherMaterial().clear();
				return;
			case ResearchCVPackage.RESEARCHER__RESEARCH_GROUP:
				setResearchGroup((ResearchGroup)null);
				return;
			case ResearchCVPackage.RESEARCHER__TOPICS:
				getTopics().clear();
				return;
			case ResearchCVPackage.RESEARCHER__GRANTS:
				getGrants().clear();
				return;
			case ResearchCVPackage.RESEARCHER__COURSES:
				getCourses().clear();
				return;
			case ResearchCVPackage.RESEARCHER__TAUGHT_COURSES:
				getTaughtCourses().clear();
				return;
			case ResearchCVPackage.RESEARCHER__SUPERVISIONS_REGISTRY:
				setSupervisionsRegistry((SupervisionsRegistry)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.RESEARCHER__COAUTHORS:
				return COAUTHORS__ESETTING_DELEGATE.dynamicIsSet(this, null, 0);
			case ResearchCVPackage.RESEARCHER__CONTACTS:
				return contacts != null && !contacts.isEmpty();
			case ResearchCVPackage.RESEARCHER__SOCIALS:
				return socials != null && !socials.isEmpty();
			case ResearchCVPackage.RESEARCHER__PUBLICATIONS:
				return publications != null && !publications.isEmpty();
			case ResearchCVPackage.RESEARCHER__TEACHING_MATERIAL:
				return teachingMaterial != null && !teachingMaterial.isEmpty();
			case ResearchCVPackage.RESEARCHER__OTHER_MATERIAL:
				return otherMaterial != null && !otherMaterial.isEmpty();
			case ResearchCVPackage.RESEARCHER__RESEARCH_GROUP:
				return researchGroup != null;
			case ResearchCVPackage.RESEARCHER__TOPICS:
				return topics != null && !topics.isEmpty();
			case ResearchCVPackage.RESEARCHER__GRANTS:
				return grants != null && !grants.isEmpty();
			case ResearchCVPackage.RESEARCHER__COURSES:
				return courses != null && !courses.isEmpty();
			case ResearchCVPackage.RESEARCHER__TAUGHT_COURSES:
				return taughtCourses != null && !taughtCourses.isEmpty();
			case ResearchCVPackage.RESEARCHER__SUPERVISIONS_REGISTRY:
				return supervisionsRegistry != null;
			case ResearchCVPackage.RESEARCHER__SUPERVISIONS:
				return SUPERVISIONS__ESETTING_DELEGATE.dynamicIsSet(this, null, 0);
		}
		return super.eIsSet(featureID);
	}

} //ResearcherImpl
