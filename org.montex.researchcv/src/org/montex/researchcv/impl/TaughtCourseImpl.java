/**
 */
package org.montex.researchcv.impl;

import java.util.Collection;

import java.util.Locale;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.montex.researchcv.CourseKind;
import org.montex.researchcv.CourseOffering;
import org.montex.researchcv.ResearchCVFactory;
import org.montex.researchcv.ResearchCVPackage;
import org.montex.researchcv.TaughtCourse;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Taught Course</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.impl.TaughtCourseImpl#getYear <em>Year</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.TaughtCourseImpl#getPeriod <em>Period</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.TaughtCourseImpl#getWebinfo <em>Webinfo</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.TaughtCourseImpl#getWebsite <em>Website</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.TaughtCourseImpl#getCode <em>Code</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.TaughtCourseImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.TaughtCourseImpl#getLevel <em>Level</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.TaughtCourseImpl#getLanguage <em>Language</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.TaughtCourseImpl#isCanceled <em>Canceled</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.TaughtCourseImpl#getResponsibility <em>Responsibility</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.TaughtCourseImpl#getOffering <em>Offering</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TaughtCourseImpl extends MinimalEObjectImpl.Container implements TaughtCourse {
	/**
	 * The default value of the '{@link #getYear() <em>Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getYear()
	 * @generated
	 * @ordered
	 */
	protected static final int YEAR_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getYear() <em>Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getYear()
	 * @generated
	 * @ordered
	 */
	protected int year = YEAR_EDEFAULT;

	/**
	 * The default value of the '{@link #getPeriod() <em>Period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPeriod()
	 * @generated
	 * @ordered
	 */
	protected static final int PERIOD_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getPeriod() <em>Period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPeriod()
	 * @generated
	 * @ordered
	 */
	protected int period = PERIOD_EDEFAULT;

	/**
	 * The default value of the '{@link #getWebinfo() <em>Webinfo</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWebinfo()
	 * @generated
	 * @ordered
	 */
	protected static final String WEBINFO_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getWebinfo() <em>Webinfo</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWebinfo()
	 * @generated
	 * @ordered
	 */
	protected String webinfo = WEBINFO_EDEFAULT;

	/**
	 * The default value of the '{@link #getWebsite() <em>Website</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWebsite()
	 * @generated
	 * @ordered
	 */
	protected static final String WEBSITE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getWebsite() <em>Website</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWebsite()
	 * @generated
	 * @ordered
	 */
	protected String website = WEBSITE_EDEFAULT;

	/**
	 * The cached setting delegate for the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature.Internal.SettingDelegate CODE__ESETTING_DELEGATE = ((EStructuralFeature.Internal)ResearchCVPackage.Literals.TAUGHT_COURSE__CODE).getSettingDelegate();

	/**
	 * The cached setting delegate for the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature.Internal.SettingDelegate NAME__ESETTING_DELEGATE = ((EStructuralFeature.Internal)ResearchCVPackage.Literals.TAUGHT_COURSE__NAME).getSettingDelegate();

	/**
	 * The cached setting delegate for the '{@link #getLevel() <em>Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLevel()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature.Internal.SettingDelegate LEVEL__ESETTING_DELEGATE = ((EStructuralFeature.Internal)ResearchCVPackage.Literals.TAUGHT_COURSE__LEVEL).getSettingDelegate();

	/**
	 * The default value of the '{@link #getLanguage() <em>Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLanguage()
	 * @generated
	 * @ordered
	 */
	protected static final Locale LANGUAGE_EDEFAULT = (Locale)ResearchCVFactory.eINSTANCE.createFromString(ResearchCVPackage.eINSTANCE.getLanguageCode(), "EN");

	/**
	 * The cached value of the '{@link #getLanguage() <em>Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLanguage()
	 * @generated
	 * @ordered
	 */
	protected Locale language = LANGUAGE_EDEFAULT;

	/**
	 * The default value of the '{@link #isCanceled() <em>Canceled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCanceled()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CANCELED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isCanceled() <em>Canceled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCanceled()
	 * @generated
	 * @ordered
	 */
	protected boolean canceled = CANCELED_EDEFAULT;

	/**
	 * The default value of the '{@link #getResponsibility() <em>Responsibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponsibility()
	 * @generated
	 * @ordered
	 */
	protected static final double RESPONSIBILITY_EDEFAULT = 1.0;

	/**
	 * The cached value of the '{@link #getResponsibility() <em>Responsibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponsibility()
	 * @generated
	 * @ordered
	 */
	protected double responsibility = RESPONSIBILITY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getOffering() <em>Offering</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOffering()
	 * @generated
	 * @ordered
	 */
	protected EList<CourseOffering> offering;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TaughtCourseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ResearchCVPackage.Literals.TAUGHT_COURSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getYear() {
		return year;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setYear(int newYear) {
		int oldYear = year;
		year = newYear;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.TAUGHT_COURSE__YEAR, oldYear, year));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getPeriod() {
		return period;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPeriod(int newPeriod) {
		int oldPeriod = period;
		period = newPeriod;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.TAUGHT_COURSE__PERIOD, oldPeriod, period));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getWebinfo() {
		return webinfo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setWebinfo(String newWebinfo) {
		String oldWebinfo = webinfo;
		webinfo = newWebinfo;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.TAUGHT_COURSE__WEBINFO, oldWebinfo, webinfo));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getWebsite() {
		return website;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setWebsite(String newWebsite) {
		String oldWebsite = website;
		website = newWebsite;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.TAUGHT_COURSE__WEBSITE, oldWebsite, website));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Locale getLanguage() {
		return language;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLanguage(Locale newLanguage) {
		Locale oldLanguage = language;
		language = newLanguage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.TAUGHT_COURSE__LANGUAGE, oldLanguage, language));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isCanceled() {
		return canceled;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCanceled(boolean newCanceled) {
		boolean oldCanceled = canceled;
		canceled = newCanceled;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.TAUGHT_COURSE__CANCELED, oldCanceled, canceled));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public double getResponsibility() {
		return responsibility;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setResponsibility(double newResponsibility) {
		double oldResponsibility = responsibility;
		responsibility = newResponsibility;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResearchCVPackage.TAUGHT_COURSE__RESPONSIBILITY, oldResponsibility, responsibility));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<CourseOffering> getOffering() {
		if (offering == null) {
			offering = new EObjectContainmentEList<CourseOffering>(CourseOffering.class, this, ResearchCVPackage.TAUGHT_COURSE__OFFERING);
		}
		return offering;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ResearchCVPackage.TAUGHT_COURSE__OFFERING:
				return ((InternalEList<?>)getOffering()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCode() {
		return (String)CODE__ESETTING_DELEGATE.dynamicGet(this, null, 0, true, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return (String)NAME__ESETTING_DELEGATE.dynamicGet(this, null, 0, true, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CourseKind getLevel() {
		return (CourseKind)LEVEL__ESETTING_DELEGATE.dynamicGet(this, null, 0, true, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ResearchCVPackage.TAUGHT_COURSE__YEAR:
				return getYear();
			case ResearchCVPackage.TAUGHT_COURSE__PERIOD:
				return getPeriod();
			case ResearchCVPackage.TAUGHT_COURSE__WEBINFO:
				return getWebinfo();
			case ResearchCVPackage.TAUGHT_COURSE__WEBSITE:
				return getWebsite();
			case ResearchCVPackage.TAUGHT_COURSE__CODE:
				return getCode();
			case ResearchCVPackage.TAUGHT_COURSE__NAME:
				return getName();
			case ResearchCVPackage.TAUGHT_COURSE__LEVEL:
				return getLevel();
			case ResearchCVPackage.TAUGHT_COURSE__LANGUAGE:
				return getLanguage();
			case ResearchCVPackage.TAUGHT_COURSE__CANCELED:
				return isCanceled();
			case ResearchCVPackage.TAUGHT_COURSE__RESPONSIBILITY:
				return getResponsibility();
			case ResearchCVPackage.TAUGHT_COURSE__OFFERING:
				return getOffering();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ResearchCVPackage.TAUGHT_COURSE__YEAR:
				setYear((Integer)newValue);
				return;
			case ResearchCVPackage.TAUGHT_COURSE__PERIOD:
				setPeriod((Integer)newValue);
				return;
			case ResearchCVPackage.TAUGHT_COURSE__WEBINFO:
				setWebinfo((String)newValue);
				return;
			case ResearchCVPackage.TAUGHT_COURSE__WEBSITE:
				setWebsite((String)newValue);
				return;
			case ResearchCVPackage.TAUGHT_COURSE__LANGUAGE:
				setLanguage((Locale)newValue);
				return;
			case ResearchCVPackage.TAUGHT_COURSE__CANCELED:
				setCanceled((Boolean)newValue);
				return;
			case ResearchCVPackage.TAUGHT_COURSE__RESPONSIBILITY:
				setResponsibility((Double)newValue);
				return;
			case ResearchCVPackage.TAUGHT_COURSE__OFFERING:
				getOffering().clear();
				getOffering().addAll((Collection<? extends CourseOffering>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.TAUGHT_COURSE__YEAR:
				setYear(YEAR_EDEFAULT);
				return;
			case ResearchCVPackage.TAUGHT_COURSE__PERIOD:
				setPeriod(PERIOD_EDEFAULT);
				return;
			case ResearchCVPackage.TAUGHT_COURSE__WEBINFO:
				setWebinfo(WEBINFO_EDEFAULT);
				return;
			case ResearchCVPackage.TAUGHT_COURSE__WEBSITE:
				setWebsite(WEBSITE_EDEFAULT);
				return;
			case ResearchCVPackage.TAUGHT_COURSE__LANGUAGE:
				setLanguage(LANGUAGE_EDEFAULT);
				return;
			case ResearchCVPackage.TAUGHT_COURSE__CANCELED:
				setCanceled(CANCELED_EDEFAULT);
				return;
			case ResearchCVPackage.TAUGHT_COURSE__RESPONSIBILITY:
				setResponsibility(RESPONSIBILITY_EDEFAULT);
				return;
			case ResearchCVPackage.TAUGHT_COURSE__OFFERING:
				getOffering().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.TAUGHT_COURSE__YEAR:
				return year != YEAR_EDEFAULT;
			case ResearchCVPackage.TAUGHT_COURSE__PERIOD:
				return period != PERIOD_EDEFAULT;
			case ResearchCVPackage.TAUGHT_COURSE__WEBINFO:
				return WEBINFO_EDEFAULT == null ? webinfo != null : !WEBINFO_EDEFAULT.equals(webinfo);
			case ResearchCVPackage.TAUGHT_COURSE__WEBSITE:
				return WEBSITE_EDEFAULT == null ? website != null : !WEBSITE_EDEFAULT.equals(website);
			case ResearchCVPackage.TAUGHT_COURSE__CODE:
				return CODE__ESETTING_DELEGATE.dynamicIsSet(this, null, 0);
			case ResearchCVPackage.TAUGHT_COURSE__NAME:
				return NAME__ESETTING_DELEGATE.dynamicIsSet(this, null, 0);
			case ResearchCVPackage.TAUGHT_COURSE__LEVEL:
				return LEVEL__ESETTING_DELEGATE.dynamicIsSet(this, null, 0);
			case ResearchCVPackage.TAUGHT_COURSE__LANGUAGE:
				return LANGUAGE_EDEFAULT == null ? language != null : !LANGUAGE_EDEFAULT.equals(language);
			case ResearchCVPackage.TAUGHT_COURSE__CANCELED:
				return canceled != CANCELED_EDEFAULT;
			case ResearchCVPackage.TAUGHT_COURSE__RESPONSIBILITY:
				return responsibility != RESPONSIBILITY_EDEFAULT;
			case ResearchCVPackage.TAUGHT_COURSE__OFFERING:
				return offering != null && !offering.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (year: ");
		result.append(year);
		result.append(", period: ");
		result.append(period);
		result.append(", webinfo: ");
		result.append(webinfo);
		result.append(", website: ");
		result.append(website);
		result.append(", language: ");
		result.append(language);
		result.append(", canceled: ");
		result.append(canceled);
		result.append(", responsibility: ");
		result.append(responsibility);
		result.append(')');
		return result.toString();
	}

} //TaughtCourseImpl
