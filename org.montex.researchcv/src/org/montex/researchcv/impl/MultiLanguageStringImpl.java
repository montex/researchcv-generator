/**
 */
package org.montex.researchcv.impl;

import java.util.Collection;

import java.util.Locale;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.montex.researchcv.LocalizedString;
import org.montex.researchcv.MultiLanguageString;
import org.montex.researchcv.ResearchCVPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Multi Language String</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.impl.MultiLanguageStringImpl#getText <em>Text</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.MultiLanguageStringImpl#getLocalizations <em>Localizations</em>}</li>
 *   <li>{@link org.montex.researchcv.impl.MultiLanguageStringImpl#getBaseLanguage <em>Base Language</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MultiLanguageStringImpl extends MinimalEObjectImpl.Container implements MultiLanguageString {
	/**
	 * The cached setting delegate for the '{@link #getText() <em>Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getText()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature.Internal.SettingDelegate TEXT__ESETTING_DELEGATE = ((EStructuralFeature.Internal)ResearchCVPackage.Literals.MULTI_LANGUAGE_STRING__TEXT).getSettingDelegate();

	/**
	 * The cached value of the '{@link #getLocalizations() <em>Localizations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalizations()
	 * @generated
	 * @ordered
	 */
	protected EList<LocalizedString> localizations;

	/**
	 * The cached setting delegate for the '{@link #getBaseLanguage() <em>Base Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseLanguage()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature.Internal.SettingDelegate BASE_LANGUAGE__ESETTING_DELEGATE = ((EStructuralFeature.Internal)ResearchCVPackage.Literals.MULTI_LANGUAGE_STRING__BASE_LANGUAGE).getSettingDelegate();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MultiLanguageStringImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ResearchCVPackage.Literals.MULTI_LANGUAGE_STRING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText() {
		return (String)TEXT__ESETTING_DELEGATE.dynamicGet(this, null, 0, true, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<LocalizedString> getLocalizations() {
		if (localizations == null) {
			localizations = new EObjectContainmentEList<LocalizedString>(LocalizedString.class, this, ResearchCVPackage.MULTI_LANGUAGE_STRING__LOCALIZATIONS);
		}
		return localizations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Locale getBaseLanguage() {
		return (Locale)BASE_LANGUAGE__ESETTING_DELEGATE.dynamicGet(this, null, 0, true, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ResearchCVPackage.MULTI_LANGUAGE_STRING__LOCALIZATIONS:
				return ((InternalEList<?>)getLocalizations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ResearchCVPackage.MULTI_LANGUAGE_STRING__TEXT:
				return getText();
			case ResearchCVPackage.MULTI_LANGUAGE_STRING__LOCALIZATIONS:
				return getLocalizations();
			case ResearchCVPackage.MULTI_LANGUAGE_STRING__BASE_LANGUAGE:
				return getBaseLanguage();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ResearchCVPackage.MULTI_LANGUAGE_STRING__LOCALIZATIONS:
				getLocalizations().clear();
				getLocalizations().addAll((Collection<? extends LocalizedString>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.MULTI_LANGUAGE_STRING__LOCALIZATIONS:
				getLocalizations().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ResearchCVPackage.MULTI_LANGUAGE_STRING__TEXT:
				return TEXT__ESETTING_DELEGATE.dynamicIsSet(this, null, 0);
			case ResearchCVPackage.MULTI_LANGUAGE_STRING__LOCALIZATIONS:
				return localizations != null && !localizations.isEmpty();
			case ResearchCVPackage.MULTI_LANGUAGE_STRING__BASE_LANGUAGE:
				return BASE_LANGUAGE__ESETTING_DELEGATE.dynamicIsSet(this, null, 0);
		}
		return super.eIsSet(featureID);
	}

} //MultiLanguageStringImpl
