/**
 */
package org.montex.researchcv;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Book Chapter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.BookChapter#getChapterNumber <em>Chapter Number</em>}</li>
 * </ul>
 *
 * @see org.montex.researchcv.ResearchCVPackage#getBookChapter()
 * @model
 * @generated
 */
public interface BookChapter extends InCollection {
	/**
	 * Returns the value of the '<em><b>Chapter Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Chapter Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Chapter Number</em>' attribute.
	 * @see #setChapterNumber(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getBookChapter_ChapterNumber()
	 * @model
	 * @generated
	 */
	String getChapterNumber();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.BookChapter#getChapterNumber <em>Chapter Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Chapter Number</em>' attribute.
	 * @see #getChapterNumber()
	 * @generated
	 */
	void setChapterNumber(String value);

} // BookChapter
