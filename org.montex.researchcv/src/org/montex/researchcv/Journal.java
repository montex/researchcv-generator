/**
 */
package org.montex.researchcv;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Journal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.Journal#getJournalName <em>Journal Name</em>}</li>
 *   <li>{@link org.montex.researchcv.Journal#getPublisher <em>Publisher</em>}</li>
 *   <li>{@link org.montex.researchcv.Journal#getFirstPage <em>First Page</em>}</li>
 *   <li>{@link org.montex.researchcv.Journal#getLastPage <em>Last Page</em>}</li>
 *   <li>{@link org.montex.researchcv.Journal#getVolume <em>Volume</em>}</li>
 *   <li>{@link org.montex.researchcv.Journal#getIssue <em>Issue</em>}</li>
 *   <li>{@link org.montex.researchcv.Journal#getISSN <em>ISSN</em>}</li>
 *   <li>{@link org.montex.researchcv.Journal#isMagazine <em>Magazine</em>}</li>
 * </ul>
 *
 * @see org.montex.researchcv.ResearchCVPackage#getJournal()
 * @model
 * @generated
 */
public interface Journal extends Publication {

	/**
	 * Returns the value of the '<em><b>Journal Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Journal Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Journal Name</em>' attribute.
	 * @see #setJournalName(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getJournal_JournalName()
	 * @model required="true"
	 * @generated
	 */
	String getJournalName();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Journal#getJournalName <em>Journal Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Journal Name</em>' attribute.
	 * @see #getJournalName()
	 * @generated
	 */
	void setJournalName(String value);

	/**
	 * Returns the value of the '<em><b>Publisher</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Publisher</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Publisher</em>' attribute.
	 * @see #setPublisher(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getJournal_Publisher()
	 * @model
	 * @generated
	 */
	String getPublisher();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Journal#getPublisher <em>Publisher</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Publisher</em>' attribute.
	 * @see #getPublisher()
	 * @generated
	 */
	void setPublisher(String value);

	/**
	 * Returns the value of the '<em><b>First Page</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First Page</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First Page</em>' attribute.
	 * @see #setFirstPage(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getJournal_FirstPage()
	 * @model
	 * @generated
	 */
	String getFirstPage();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Journal#getFirstPage <em>First Page</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First Page</em>' attribute.
	 * @see #getFirstPage()
	 * @generated
	 */
	void setFirstPage(String value);

	/**
	 * Returns the value of the '<em><b>Last Page</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Page</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Page</em>' attribute.
	 * @see #setLastPage(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getJournal_LastPage()
	 * @model
	 * @generated
	 */
	String getLastPage();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Journal#getLastPage <em>Last Page</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Page</em>' attribute.
	 * @see #getLastPage()
	 * @generated
	 */
	void setLastPage(String value);

	/**
	 * Returns the value of the '<em><b>Volume</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Volume</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Volume</em>' attribute.
	 * @see #setVolume(int)
	 * @see org.montex.researchcv.ResearchCVPackage#getJournal_Volume()
	 * @model required="true"
	 * @generated
	 */
	int getVolume();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Journal#getVolume <em>Volume</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Volume</em>' attribute.
	 * @see #getVolume()
	 * @generated
	 */
	void setVolume(int value);

	/**
	 * Returns the value of the '<em><b>Issue</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Issue</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Issue</em>' attribute.
	 * @see #setIssue(int)
	 * @see org.montex.researchcv.ResearchCVPackage#getJournal_Issue()
	 * @model required="true"
	 * @generated
	 */
	int getIssue();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Journal#getIssue <em>Issue</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Issue</em>' attribute.
	 * @see #getIssue()
	 * @generated
	 */
	void setIssue(int value);

	/**
	 * Returns the value of the '<em><b>ISSN</b></em>' containment reference list.
	 * The list contents are of type {@link org.montex.researchcv.SerialNumber}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ISSN</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ISSN</em>' containment reference list.
	 * @see org.montex.researchcv.ResearchCVPackage#getJournal_ISSN()
	 * @model containment="true"
	 * @generated
	 */
	EList<SerialNumber> getISSN();

	/**
	 * Returns the value of the '<em><b>Magazine</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Magazine</em>' attribute.
	 * @see #setMagazine(boolean)
	 * @see org.montex.researchcv.ResearchCVPackage#getJournal_Magazine()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isMagazine();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Journal#isMagazine <em>Magazine</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Magazine</em>' attribute.
	 * @see #isMagazine()
	 * @generated
	 */
	void setMagazine(boolean value);
} // Journal
