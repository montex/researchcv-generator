/**
 */
package org.montex.researchcv;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Course Kind</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.montex.researchcv.ResearchCVPackage#getCourseKind()
 * @model
 * @generated
 */
public enum CourseKind implements Enumerator {
	/**
	 * The '<em><b>UNDERGRADUATE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UNDERGRADUATE_VALUE
	 * @generated
	 * @ordered
	 */
	UNDERGRADUATE(0, "UNDERGRADUATE", "UNDERGRADUATE"),

	/**
	 * The '<em><b>GRADUATE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GRADUATE_VALUE
	 * @generated
	 * @ordered
	 */
	GRADUATE(0, "GRADUATE", "GRADUATE"), /**
	 * The '<em><b>CONTINUING EDUCATION</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CONTINUING_EDUCATION_VALUE
	 * @generated
	 * @ordered
	 */
	CONTINUING_EDUCATION(0, "CONTINUING_EDUCATION", "CONTINUING_EDUCATION");

	/**
	 * The '<em><b>UNDERGRADUATE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>UNDERGRADUATE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UNDERGRADUATE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int UNDERGRADUATE_VALUE = 0;

	/**
	 * The '<em><b>GRADUATE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>GRADUATE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GRADUATE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int GRADUATE_VALUE = 0;

	/**
	 * The '<em><b>CONTINUING EDUCATION</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CONTINUING_EDUCATION
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int CONTINUING_EDUCATION_VALUE = 0;

	/**
	 * An array of all the '<em><b>Course Kind</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final CourseKind[] VALUES_ARRAY =
		new CourseKind[] {
			UNDERGRADUATE,
			GRADUATE,
			CONTINUING_EDUCATION,
		};

	/**
	 * A public read-only list of all the '<em><b>Course Kind</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<CourseKind> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Course Kind</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static CourseKind get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			CourseKind result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Course Kind</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static CourseKind getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			CourseKind result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Course Kind</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static CourseKind get(int value) {
		switch (value) {
			case UNDERGRADUATE_VALUE: return UNDERGRADUATE;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private CourseKind(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //CourseKind
