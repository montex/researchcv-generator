/**
 */
package org.montex.researchcv;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Book</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.Book#getPublisher <em>Publisher</em>}</li>
 *   <li>{@link org.montex.researchcv.Book#getSeries <em>Series</em>}</li>
 *   <li>{@link org.montex.researchcv.Book#getVolume <em>Volume</em>}</li>
 *   <li>{@link org.montex.researchcv.Book#getISBN <em>ISBN</em>}</li>
 * </ul>
 *
 * @see org.montex.researchcv.ResearchCVPackage#getBook()
 * @model
 * @generated
 */
public interface Book extends Publication {

	/**
	 * Returns the value of the '<em><b>Publisher</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Publisher</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Publisher</em>' attribute.
	 * @see #setPublisher(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getBook_Publisher()
	 * @model
	 * @generated
	 */
	String getPublisher();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Book#getPublisher <em>Publisher</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Publisher</em>' attribute.
	 * @see #getPublisher()
	 * @generated
	 */
	void setPublisher(String value);

	/**
	 * Returns the value of the '<em><b>Series</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Series</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Series</em>' attribute.
	 * @see #setSeries(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getBook_Series()
	 * @model
	 * @generated
	 */
	String getSeries();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Book#getSeries <em>Series</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Series</em>' attribute.
	 * @see #getSeries()
	 * @generated
	 */
	void setSeries(String value);

	/**
	 * Returns the value of the '<em><b>Volume</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Volume</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Volume</em>' attribute.
	 * @see #setVolume(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getBook_Volume()
	 * @model
	 * @generated
	 */
	String getVolume();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Book#getVolume <em>Volume</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Volume</em>' attribute.
	 * @see #getVolume()
	 * @generated
	 */
	void setVolume(String value);

	/**
	 * Returns the value of the '<em><b>ISBN</b></em>' containment reference list.
	 * The list contents are of type {@link org.montex.researchcv.SerialNumber}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ISBN</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ISBN</em>' containment reference list.
	 * @see org.montex.researchcv.ResearchCVPackage#getBook_ISBN()
	 * @model containment="true"
	 * @generated
	 */
	EList<SerialNumber> getISBN();
} // Book
