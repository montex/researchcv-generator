/**
 */
package org.montex.researchcv;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Supervisions Registry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.SupervisionsRegistry#getSupervisions <em>Supervisions</em>}</li>
 * </ul>
 *
 * @see org.montex.researchcv.ResearchCVPackage#getSupervisionsRegistry()
 * @model
 * @generated
 */
public interface SupervisionsRegistry extends RootElement {
	/**
	 * Returns the value of the '<em><b>Supervisions</b></em>' containment reference list.
	 * The list contents are of type {@link org.montex.researchcv.Supervision}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Supervisions</em>' containment reference list.
	 * @see org.montex.researchcv.ResearchCVPackage#getSupervisionsRegistry_Supervisions()
	 * @model containment="true"
	 * @generated
	 */
	EList<Supervision> getSupervisions();

} // SupervisionsRegistry
