/**
 */
package org.montex.researchcv;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Researcher</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.Researcher#getCoauthors <em>Coauthors</em>}</li>
 *   <li>{@link org.montex.researchcv.Researcher#getContacts <em>Contacts</em>}</li>
 *   <li>{@link org.montex.researchcv.Researcher#getSocials <em>Socials</em>}</li>
 *   <li>{@link org.montex.researchcv.Researcher#getPublications <em>Publications</em>}</li>
 *   <li>{@link org.montex.researchcv.Researcher#getTeachingMaterial <em>Teaching Material</em>}</li>
 *   <li>{@link org.montex.researchcv.Researcher#getOtherMaterial <em>Other Material</em>}</li>
 *   <li>{@link org.montex.researchcv.Researcher#getResearchGroup <em>Research Group</em>}</li>
 *   <li>{@link org.montex.researchcv.Researcher#getTopics <em>Topics</em>}</li>
 *   <li>{@link org.montex.researchcv.Researcher#getGrants <em>Grants</em>}</li>
 *   <li>{@link org.montex.researchcv.Researcher#getCourses <em>Courses</em>}</li>
 *   <li>{@link org.montex.researchcv.Researcher#getTaughtCourses <em>Taught Courses</em>}</li>
 *   <li>{@link org.montex.researchcv.Researcher#getSupervisionsRegistry <em>Supervisions Registry</em>}</li>
 *   <li>{@link org.montex.researchcv.Researcher#getSupervisions <em>Supervisions</em>}</li>
 * </ul>
 *
 * @see org.montex.researchcv.ResearchCVPackage#getResearcher()
 * @model
 * @generated
 */
public interface Researcher extends Person, RootElement {
	/**
	 * Returns the value of the '<em><b>Coauthors</b></em>' reference list.
	 * The list contents are of type {@link org.montex.researchcv.Person}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Coauthors</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Coauthors</em>' reference list.
	 * @see org.montex.researchcv.ResearchCVPackage#getResearcher_Coauthors()
	 * @model transient="true" volatile="true" derived="true" ordered="false"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL derivation='self.publications-&gt;collect(authors)-&gt;asSet()-&gt;excluding(self)'"
	 * @generated
	 */
	EList<Person> getCoauthors();

	/**
	 * Returns the value of the '<em><b>Contacts</b></em>' containment reference list.
	 * The list contents are of type {@link org.montex.researchcv.Contact}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contacts</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contacts</em>' containment reference list.
	 * @see org.montex.researchcv.ResearchCVPackage#getResearcher_Contacts()
	 * @model containment="true"
	 * @generated
	 */
	EList<Contact> getContacts();

	/**
	 * Returns the value of the '<em><b>Socials</b></em>' containment reference list.
	 * The list contents are of type {@link org.montex.researchcv.SocialProfile}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Socials</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Socials</em>' containment reference list.
	 * @see org.montex.researchcv.ResearchCVPackage#getResearcher_Socials()
	 * @model containment="true"
	 * @generated
	 */
	EList<SocialProfile> getSocials();

	/**
	 * Returns the value of the '<em><b>Publications</b></em>' containment reference list.
	 * The list contents are of type {@link org.montex.researchcv.Publication}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Publications</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Publications</em>' containment reference list.
	 * @see org.montex.researchcv.ResearchCVPackage#getResearcher_Publications()
	 * @model containment="true"
	 * @generated
	 */
	EList<Publication> getPublications();

	/**
	 * Returns the value of the '<em><b>Teaching Material</b></em>' containment reference list.
	 * The list contents are of type {@link org.montex.researchcv.Publication}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Teaching Material</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Teaching Material</em>' containment reference list.
	 * @see org.montex.researchcv.ResearchCVPackage#getResearcher_TeachingMaterial()
	 * @model containment="true"
	 * @generated
	 */
	EList<Publication> getTeachingMaterial();

	/**
	 * Returns the value of the '<em><b>Other Material</b></em>' containment reference list.
	 * The list contents are of type {@link org.montex.researchcv.Publication}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Other Material</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Other Material</em>' containment reference list.
	 * @see org.montex.researchcv.ResearchCVPackage#getResearcher_OtherMaterial()
	 * @model containment="true"
	 * @generated
	 */
	EList<Publication> getOtherMaterial();

	/**
	 * Returns the value of the '<em><b>Research Group</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Research Group</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Research Group</em>' containment reference.
	 * @see #setResearchGroup(ResearchGroup)
	 * @see org.montex.researchcv.ResearchCVPackage#getResearcher_ResearchGroup()
	 * @model containment="true"
	 * @generated
	 */
	ResearchGroup getResearchGroup();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Researcher#getResearchGroup <em>Research Group</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Research Group</em>' containment reference.
	 * @see #getResearchGroup()
	 * @generated
	 */
	void setResearchGroup(ResearchGroup value);

	/**
	 * Returns the value of the '<em><b>Topics</b></em>' containment reference list.
	 * The list contents are of type {@link org.montex.researchcv.ResearchTopic}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Topics</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Topics</em>' containment reference list.
	 * @see org.montex.researchcv.ResearchCVPackage#getResearcher_Topics()
	 * @model containment="true"
	 * @generated
	 */
	EList<ResearchTopic> getTopics();

	/**
	 * Returns the value of the '<em><b>Grants</b></em>' containment reference list.
	 * The list contents are of type {@link org.montex.researchcv.Grant}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Grants</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Grants</em>' containment reference list.
	 * @see org.montex.researchcv.ResearchCVPackage#getResearcher_Grants()
	 * @model containment="true"
	 * @generated
	 */
	EList<Grant> getGrants();

	/**
	 * Returns the value of the '<em><b>Courses</b></em>' containment reference list.
	 * The list contents are of type {@link org.montex.researchcv.Course}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Courses</em>' containment reference list.
	 * @see org.montex.researchcv.ResearchCVPackage#getResearcher_Courses()
	 * @model containment="true"
	 * @generated
	 */
	EList<Course> getCourses();

	/**
	 * Returns the value of the '<em><b>Taught Courses</b></em>' containment reference list.
	 * The list contents are of type {@link org.montex.researchcv.TaughtCourse}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Taught Courses</em>' containment reference list.
	 * @see org.montex.researchcv.ResearchCVPackage#getResearcher_TaughtCourses()
	 * @model containment="true"
	 * @generated
	 */
	EList<TaughtCourse> getTaughtCourses();

	/**
	 * Returns the value of the '<em><b>Supervisions Registry</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Supervisions Registry</em>' reference.
	 * @see #setSupervisionsRegistry(SupervisionsRegistry)
	 * @see org.montex.researchcv.ResearchCVPackage#getResearcher_SupervisionsRegistry()
	 * @model
	 * @generated
	 */
	SupervisionsRegistry getSupervisionsRegistry();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.Researcher#getSupervisionsRegistry <em>Supervisions Registry</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Supervisions Registry</em>' reference.
	 * @see #getSupervisionsRegistry()
	 * @generated
	 */
	void setSupervisionsRegistry(SupervisionsRegistry value);

	/**
	 * Returns the value of the '<em><b>Supervisions</b></em>' reference list.
	 * The list contents are of type {@link org.montex.researchcv.Supervision}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Supervisions</em>' reference list.
	 * @see org.montex.researchcv.ResearchCVPackage#getResearcher_Supervisions()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL derivation='supervisionsRegistry.supervisions'"
	 * @generated
	 */
	EList<Supervision> getSupervisions();

} // Researcher
