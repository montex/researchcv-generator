/**
 */
package org.montex.researchcv;

import java.util.Locale;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Taught Course</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.montex.researchcv.TaughtCourse#getYear <em>Year</em>}</li>
 *   <li>{@link org.montex.researchcv.TaughtCourse#getPeriod <em>Period</em>}</li>
 *   <li>{@link org.montex.researchcv.TaughtCourse#getWebinfo <em>Webinfo</em>}</li>
 *   <li>{@link org.montex.researchcv.TaughtCourse#getWebsite <em>Website</em>}</li>
 *   <li>{@link org.montex.researchcv.TaughtCourse#getCode <em>Code</em>}</li>
 *   <li>{@link org.montex.researchcv.TaughtCourse#getName <em>Name</em>}</li>
 *   <li>{@link org.montex.researchcv.TaughtCourse#getLevel <em>Level</em>}</li>
 *   <li>{@link org.montex.researchcv.TaughtCourse#getLanguage <em>Language</em>}</li>
 *   <li>{@link org.montex.researchcv.TaughtCourse#isCanceled <em>Canceled</em>}</li>
 *   <li>{@link org.montex.researchcv.TaughtCourse#getResponsibility <em>Responsibility</em>}</li>
 *   <li>{@link org.montex.researchcv.TaughtCourse#getOffering <em>Offering</em>}</li>
 * </ul>
 *
 * @see org.montex.researchcv.ResearchCVPackage#getTaughtCourse()
 * @model
 * @generated
 */
public interface TaughtCourse extends EObject {
	/**
	 * Returns the value of the '<em><b>Year</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Year</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Year</em>' attribute.
	 * @see #setYear(int)
	 * @see org.montex.researchcv.ResearchCVPackage#getTaughtCourse_Year()
	 * @model required="true"
	 * @generated
	 */
	int getYear();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.TaughtCourse#getYear <em>Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Year</em>' attribute.
	 * @see #getYear()
	 * @generated
	 */
	void setYear(int value);

	/**
	 * Returns the value of the '<em><b>Period</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Period</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Period</em>' attribute.
	 * @see #setPeriod(int)
	 * @see org.montex.researchcv.ResearchCVPackage#getTaughtCourse_Period()
	 * @model required="true"
	 * @generated
	 */
	int getPeriod();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.TaughtCourse#getPeriod <em>Period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Period</em>' attribute.
	 * @see #getPeriod()
	 * @generated
	 */
	void setPeriod(int value);

	/**
	 * Returns the value of the '<em><b>Webinfo</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Webinfo</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Webinfo</em>' attribute.
	 * @see #setWebinfo(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getTaughtCourse_Webinfo()
	 * @model
	 * @generated
	 */
	String getWebinfo();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.TaughtCourse#getWebinfo <em>Webinfo</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Webinfo</em>' attribute.
	 * @see #getWebinfo()
	 * @generated
	 */
	void setWebinfo(String value);

	/**
	 * Returns the value of the '<em><b>Website</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Website</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Website</em>' attribute.
	 * @see #setWebsite(String)
	 * @see org.montex.researchcv.ResearchCVPackage#getTaughtCourse_Website()
	 * @model
	 * @generated
	 */
	String getWebsite();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.TaughtCourse#getWebsite <em>Website</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Website</em>' attribute.
	 * @see #getWebsite()
	 * @generated
	 */
	void setWebsite(String value);

	/**
	 * Returns the value of the '<em><b>Language</b></em>' attribute.
	 * The default value is <code>"EN"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Language</em>' attribute.
	 * @see #setLanguage(Locale)
	 * @see org.montex.researchcv.ResearchCVPackage#getTaughtCourse_Language()
	 * @model default="EN" dataType="org.montex.researchcv.LanguageCode" required="true"
	 * @generated
	 */
	Locale getLanguage();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.TaughtCourse#getLanguage <em>Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Language</em>' attribute.
	 * @see #getLanguage()
	 * @generated
	 */
	void setLanguage(Locale value);

	/**
	 * Returns the value of the '<em><b>Canceled</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Canceled</em>' attribute.
	 * @see #setCanceled(boolean)
	 * @see org.montex.researchcv.ResearchCVPackage#getTaughtCourse_Canceled()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isCanceled();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.TaughtCourse#isCanceled <em>Canceled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Canceled</em>' attribute.
	 * @see #isCanceled()
	 * @generated
	 */
	void setCanceled(boolean value);

	/**
	 * Returns the value of the '<em><b>Responsibility</b></em>' attribute.
	 * The default value is <code>"1.0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Responsibility</em>' attribute.
	 * @see #setResponsibility(double)
	 * @see org.montex.researchcv.ResearchCVPackage#getTaughtCourse_Responsibility()
	 * @model default="1.0" required="true"
	 * @generated
	 */
	double getResponsibility();

	/**
	 * Sets the value of the '{@link org.montex.researchcv.TaughtCourse#getResponsibility <em>Responsibility</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Responsibility</em>' attribute.
	 * @see #getResponsibility()
	 * @generated
	 */
	void setResponsibility(double value);

	/**
	 * Returns the value of the '<em><b>Offering</b></em>' containment reference list.
	 * The list contents are of type {@link org.montex.researchcv.CourseOffering}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Offering</em>' containment reference list.
	 * @see org.montex.researchcv.ResearchCVPackage#getTaughtCourse_Offering()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<CourseOffering> getOffering();

	/**
	 * Returns the value of the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code</em>' attribute.
	 * @see org.montex.researchcv.ResearchCVPackage#getTaughtCourse_Code()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL derivation='\n            if not offering-&gt;isEmpty() then offering-&gt;first().course.code else null endif'"
	 * @generated
	 */
	String getCode();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see org.montex.researchcv.ResearchCVPackage#getTaughtCourse_Name()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL derivation='\n            if not offering-&gt;isEmpty() then offering-&gt;first().course.name else null endif'"
	 * @generated
	 */
	String getName();

	/**
	 * Returns the value of the '<em><b>Level</b></em>' attribute.
	 * The literals are from the enumeration {@link org.montex.researchcv.CourseKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Level</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Level</em>' attribute.
	 * @see org.montex.researchcv.CourseKind
	 * @see org.montex.researchcv.ResearchCVPackage#getTaughtCourse_Level()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL derivation='\n            if not offering-&gt;isEmpty() then offering-&gt;first().course.level else null endif'"
	 * @generated
	 */
	CourseKind getLevel();

} // TaughtCourse
