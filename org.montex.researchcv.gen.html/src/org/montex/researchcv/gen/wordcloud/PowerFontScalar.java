package org.montex.researchcv.gen.wordcloud;

import com.kennycason.kumo.font.scale.FontScalar;

public class PowerFontScalar implements FontScalar {

    private final int minFont;
    private final int maxFont;
    private final int power;

    public PowerFontScalar(final int minFont, final int maxFont) {
        this(minFont, maxFont, 2);
    }
    public PowerFontScalar(final int minFont, final int maxFont, final int power) {
        this.minFont = minFont;
        this.maxFont = maxFont;
        this.power = power;
    }
    
    @Override
    public float scale(final int value, final int minValue, final int maxValue) {
        final double leftSpan = Math.pow(maxValue, power) - Math.pow(minValue, power);
        final double rightSpan = maxFont - minFont;

        // Convert the left range into a 0-1 range
        final double valueScaled = (Math.pow(value, power) - Math.pow(minValue, power)) / leftSpan;

        // Convert the 0-1 range into a value in the right range.
        return (float) (minFont + (valueScaled * rightSpan));
    }
}
