package org.montex.researchcv.gen.wordcloud;

import java.awt.Color;
import java.awt.Dimension;
import java.util.Arrays;
import java.util.List;

import com.kennycason.kumo.CollisionMode;
import com.kennycason.kumo.WordCloud;
import com.kennycason.kumo.WordFrequency;
import com.kennycason.kumo.font.scale.LinearFontScalar;
import com.kennycason.kumo.nlp.FrequencyAnalyzer;
import com.kennycason.kumo.palette.LinearGradientColorPalette;
import com.kennycason.kumo.placement.LinearWordPlacer;

public class WordCloudGenerator 
{
    
    public static void generateImage(String inputText, String stopWords, String outputFile) {
		final FrequencyAnalyzer frequencyAnalyzer = new FrequencyAnalyzer();
		frequencyAnalyzer.addNormalizer(new SingularNormalizer());
		frequencyAnalyzer.setWordFrequenciesToReturn(1000);
		frequencyAnalyzer.setMinWordLength(4);
		frequencyAnalyzer.setStopWords(Arrays.asList(stopWords.split("\\s")));

		final List<WordFrequency> wordFrequencies = frequencyAnalyzer.load(Arrays.asList(inputText.split("\\s")));
		final Dimension dimension = new Dimension(400, 200);
		final WordCloud wordCloud = new WordCloud(dimension, CollisionMode.PIXEL_PERFECT);
		wordCloud.setPadding(2);
		wordCloud.setWordPlacer(new LinearWordPlacer());
		wordCloud.setColorPalette(new LinearGradientColorPalette(new Color(0x1779ba), new Color(0x606060), 30));
		wordCloud.setFontScalar(new LinearFontScalar(3,60));
		wordCloud.setBackgroundColor(Color.WHITE);
		wordCloud.build(wordFrequencies);
		wordCloud.writeToFile(outputFile);
    }
}
