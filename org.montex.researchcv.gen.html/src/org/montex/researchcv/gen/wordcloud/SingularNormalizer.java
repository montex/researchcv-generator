package org.montex.researchcv.gen.wordcloud;

import com.kennycason.kumo.nlp.normalize.Normalizer;

public class SingularNormalizer implements Normalizer {
    @Override
    public String apply(final String text) {
        return Inflector.getInstance().singularize(text);
    }
}