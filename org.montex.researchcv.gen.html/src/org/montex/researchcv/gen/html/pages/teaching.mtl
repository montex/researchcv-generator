[comment encoding = UTF-8 /]
[module teaching('http://www.montex.org/researchcv')]
[import org::montex::researchcv::gen::html::common::staticElements /]
[import org::montex::researchcv::gen::html::common::commonQueries /]
[import org::montex::researchcv::gen::html::pages::publicationDetail/]
[import org::montex::researchcv::gen::html::common::formatDates/]

[template public teaching(r : Researcher)]
[header(
    'Teaching',
    'Personal page of Leonardo Montecchi, Associate Professor at IDI/NTNU',
    Sequence{'Leonardo Montecchi','dependability','model-driven engineering','assistant professor','Firenze','Campinas'},
    0)
/]

[topMenu(3,0)/]

   <div class="grid-container grid-container-padded">
      <div class="grid-x grid-padding-x">
        [r.nameWithContact()/]
         <div class="cell text-justify">
           [let tCourses : OrderedSet(TaughtCourse) = r.taughtCourses->select(year >= 2022 and level <> CourseKind::CONTINUING_EDUCATION and not canceled)]
           [for (y : Integer | tCourses->collect(c | c.year)->asOrderedSet()->reverse())]
           <h2>[y/]</h2>
           [let fall : OrderedSet(TaughtCourse) = tCourses->select(year = y and period = 2)]
           [if (not fall->isEmpty())]
           <h3>Fall Semester</h3>
           <ul class="courses">
           [for (c : TaughtCourse | fall)]
             [c.formatNewCourse()/]
           [/for]
           </ul>
           [/if]
           [/let]
           [let spring : OrderedSet(TaughtCourse) = tCourses->select(year = y and period = 1)]
           [if (not spring->isEmpty())]
           <h3>Spring Semester</h3>
           <ul class="courses">
           [for (c : TaughtCourse | spring)]
             [c.formatNewCourse()/]
           [/for]
           </ul>
           [/if]
           [/let]
           [/for]
           [/let]
         </div>
         <div class="cell text-justify">
           <h2>2017-2021</h2>
           <h3>Universidade Estadual de Campinas, Brazil</h3>
           [let tCourses : OrderedSet(TaughtCourse) = r.taughtCourses->select(year >= 2017 and year <= 2021 and level <> CourseKind::CONTINUING_EDUCATION and not canceled)]
           [let offerings : OrderedSet(CourseOffering) = tCourses->collect(offering->first())->asOrderedSet()]
           [let metaCourses : OrderedSet(Course) = offerings->collect(course)->asOrderedSet()]
           <ul class="courses">
           [for (c : Course | metaCourses)]
             <li>[c.code/] – [c.name/]
             ( [tCourses->select(offering->first().course = c).formatOldCourse()->sep(', ')/] )</li>
           [/for]
           </ul>
           [/let]
           [/let]
           [/let]
         </div>
       </div>

[footer(0)/]
[/template]

[template private formatNewCourse(c : TaughtCourse)]
<li><a href="[c.webinfo/]">[c.code/]</a> – [c.name/]</li> 
[/template]

[template private formatOldCourse(c : TaughtCourse)]
<a href="[c.webinfo/]">[c.year/]/[c.period/]</a>
[/template]

[query private sortingKey(c : TaughtCourse) : String =
if c.level = CourseKind::UNDERGRADUATE then 'A' else 'B' endif +
if c.canceled then 'A' else 'B' endif +
c.code
/]

[template private show(c : TaughtCourse)]
<li class="course"><span class="title">[if (c.canceled)]<strike>[/if][c.code/]/[c.groupsWithDefault()/] – [c.printName()/][if (c.canceled)]</strike>[/if]</span>
  [if (not c.webinfo.oclIsUndefined())]<a class="clear button" title="[c.code/] Info" href="[c.webinfo/]"><i class="fi-info"></i> </a>[/if]
  [if (not c.website.oclIsUndefined())]<a class="clear button" title="[c.code/] Teaching Platform" href="[c.website/]"><i class="fi-torsos-all"></i> </a>[/if]
</li>
[/template]

[template private printName(c : TaughtCourse)]
[c.name/][if (not c.offering->first().specificName.oclIsUndefined())]: <em>[c.offering->first().specificName/]</em>[/if]
[/template]

[query private groupsWithDefault(c : TaughtCourse) : OrderedSet(String) = 
    if c.offering->first().groups->isEmpty() then OrderedSet{'A'} else c.offering->first().groups endif
/]

