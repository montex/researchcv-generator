package org.montex.researchcv.gen.html.main;

/** This class has the only purpose to store the target path of generation
 *  and to pass it to the Acceleo generator via Java services.
 *  This is used only for the generation of the word cloud
 * @author Leonardo Montecchi
 */

public class GeneratorInformation {
	
	private static String outputFolder;
	
	public static void setOutputFolder(String path) {
		outputFolder = path;
	}
	
	public static String getOutputFolder() {
		return outputFolder;
	}
}
