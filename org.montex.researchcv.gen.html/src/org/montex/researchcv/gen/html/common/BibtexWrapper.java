package org.montex.researchcv.gen.html.common;

import org.montex.researchcv.Publication;
import org.montex.researchcv.gen.bibtex.BibtexGenerator;

public class BibtexWrapper {
	
	private static BibtexGenerator bib = new BibtexGenerator();

	public static String generate(Publication p) {
		
		return bib.generate(p);	
	}
}
